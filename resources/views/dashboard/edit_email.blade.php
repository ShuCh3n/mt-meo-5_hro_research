@extends('dashboard.layout.main')

@section('stylesheet')
    <link href="/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="/css/plugins/select2/select2.min.css" rel="stylesheet">

    <link href="/css/plugins/summernote/summernote.css" rel="stylesheet">
    <link href="/css/plugins/summernote/summernote-bs3.css" rel="stylesheet">
@stop

@section('javascript')
    <!-- iCheck -->
    <script src="/js/plugins/iCheck/icheck.min.js"></script>

    <!-- SUMMERNOTE -->
    <script src="/js/plugins/summernote/summernote.min.js"></script>

    <script>
        $(function(){

            $('.summernote').summernote();

            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });

            $('.email-body-input').summernote();

            $('.email-body-input').on('summernote.change', function(we, contents, $editable) {
                $('.summernote').each(function( index ) {
                    $(this).summernote("code", contents);
                });
            });

        });
    </script>
@stop

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>{{ trans('pages.inquiry_detail_header') }}</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('home') }}">{{ trans('menu.home') }}</a>
                </li>
                <li>
                    <a href="">{{ trans('menu.inquiries') }}</a>
                </li>
                <li class="active">
                    <strong>{{ trans('page.inquiry') }}</strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">
        {!! session()->has('success')? '<div class="alert alert-success alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>' . session('success') . '</div>' : '' !!}

        <form id="profileForm" method="post" class="form-horizontal">
            <div class="row">
                <div class="col-md-8">
                    <div class="ibox">

                        <div class="ibox-title">
                            <h5>New Email</h5>
                        </div>

                        <div class="ibox-content">
                            <div class="mb20"></div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Active</label>
                                        <div class="col-md-5">
                                            <label><input type="radio" class="form-control i-checks" name="active" value="1" checked> Enabled</label>
                                        </div>
                                        <div class="col-md-5">
                                            <label><input type="radio" class="form-control i-checks" name="active" value="0"> Disabled</label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Shop</label>

                                        <div class="col-md-10 form-static">
                                            <p class="form-control-static">{{ $email->shop->name }}</p>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Language</label>
                                        <div class="col-md-10">
                                            <select id="multi-language" class="form-control countries-select" name="languages">
                                                <option value="all" icon="United-Nations" selected>All</option>

                                                @foreach($languages as $language)
                                                    <option value="{{ $language->id }}" icon="{{ $language->icon }}">{{ $language->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Email Type</label>

                                        <div class="col-md-10">
                                            <select class="select2 form-control" name="email_type">
                                                @foreach($email_types as $type)
                                                    <option value="{{ $type->id }}" {{ ($type->id == $email->email_type_id)? "selected" : "" }}>{{ $type->info->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="ibox">

                        <div class="ibox-title">
                            <h5>Email Content</h5>
                        </div>

                        <div class="ibox-content">
                            <div class="mb20"></div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="multi-language-info-box multi-language-all">
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">Title</label>
                                            <div class="col-md-10">
                                                <input type="text" class="form-control all-language-input email-title-input" data-input="email-title-input" placeholder="Email Title">
                                                </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-2 control-label">Body</label>
                                            <div class="col-md-10">
                                                <textarea class="email-body-input"></textarea>
                                            </div>
                                        </div>
                                    </div>

                                    @foreach($languages as $language)
                                        <div class="multi-language-info-box multi-language-{{ $language->id}}">
                                            <div class="form-group">
                                                <label class="col-md-2 control-label">Title</label>
                                                <div class="col-md-10">
                                                    <input type="text" class="form-control email-title-input" name="email_title[{{ $language->id }}]" value="{{ @$email->infos->where('language_id', $language->id)->first()->title }}" placeholder="Email Title">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-2 control-label">Body</label>
                                                <div class="col-md-10">
                                                    <textarea class="summernote" name="email_body[{{ $language->id }}]">
                                                        {{ @$email_content[$language->id] }}
                                                    </textarea>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach

                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="col-md-4">
                    <div class="ibox">
                        <div class="ibox-title">
                            <h5>Action</h5>
                        </div>

                        <div class="ibox-content">

                            {{ csrf_field() }}
                            <button class="btn btn-block btn-outline btn-success" type="submit"><i class="fa fa-floppy-o"></i> Save</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>

        <div class="mb30"></div>
    </div>
@stop