@extends('dashboard.layout.main')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-6">
            <h2>{{ trans('pages.inquiry_detail_header') }}</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('home') }}">{{ trans('menu.home') }}</a>
                </li>
                <li>
                    <a href="">{{ trans('menu.inquiries') }}</a>
                </li>
                <li class="active">
                    <strong>{{ trans('page.inquiry') }}</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-6">
            <a class="btn btn-outline btn-primary pull-right mt30" href="{{ route('new_reward_program') }}"><i class="fa fa-plus-circle" aria-hidden="true"></i> New program</a>
        </div>
    </div>

	<div class="wrapper wrapper-content animated fadeInRight ecommerce">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-content">

                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>Shop</th>
                                <th>Total Orders</th>
                                <th>Total Payout</th>
                                <th class="text-right">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($reward_programs as $program)
                                    <tr>
                                        <td>
                                            <a href="{{ route('reward_program_orders', $program->id) }}">{{ $program->shop->name }}</a>
                                        </td>
                                        <td>
                                            {{ $program->orders->count() }}
                                        </td>
                                        <td>
                                            {{ price(0) }}
                                        </td>
                                        <td class="text-right">
                                            <div class="btn-group">
                                                <a class="btn-white btn btn-xs" href="{{ route('reward_program_orders', $program->id) }}">View</a>
                                                <a class="btn-white btn btn-xs" href="{{ route('edit_reward_program', $program->id) }}">Edit</a>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop