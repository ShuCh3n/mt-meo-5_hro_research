<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Commodity extends Model
{
    public function prices()
    {
    	return $this->hasMany(CommodityPrice::class);
    }

    public static function getStats($commodity, $from, $until)
    {
    	$commodity = self::where('name', $commodity)->with(['prices' => function($query) use ($from, $until){
    		$query->range($from, $until);
    	}])->first();

    	$stats = [];

		foreach($commodity->prices as $set)
		{
			$stats[] = array(
				(carbon()->createFromFormat('Y-m-d H:i:s', $set->created_at)->timestamp) * 1000,
				$set->price
			);
		}

        return collect($stats)->toJson();
    }
}