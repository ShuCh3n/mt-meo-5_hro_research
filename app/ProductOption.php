<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductOption extends Model
{
    public static function getOptionByName($name, $add_new = false)
    {
    	$option = ProductOption::where('name', $name)->first();
    	
    	if(!$option)
    	{
    		if($add_new)
    		{
    			$option = new ProductOption();
    			$option->name = $name;
    			$option->save();
    		}
    	}

    	return $option;
    }
}
