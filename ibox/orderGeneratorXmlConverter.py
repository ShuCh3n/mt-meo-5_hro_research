import random
import time
import json
import sys
import os


if not os.path.isdir('../storage/app/generated-orders/xml'):
    os.makedirs('../storage/app/generated-orders/xml')


if not os.path.isdir('../storage/app/generated-orders/errors'):
    os.makedirs('../storage/app/generated-orders/errors')


sourcefile = open('../storage/app/generated-orders/json/' + sys.argv[1], 'r')
source = json.loads(sourcefile.read())
sourcefile.close()


if not os.path.exists('../storage/app/generated-orders/settings.json'):
    logs = open('../storage/app/generated-orders/errors/' + time.strftime('%Y%m%d_%H%M%S') + '.log', 'w')
    logs.write('orderGeneratorXmlConverter >>> Fatal error: Settings file doesn\'t exist!')
    logs.close()


settingsfile = open('../storage/app/generated-orders/settings.json', 'r')
settings = json.loads(settingsfile.read())
settingsfile.close()


orders = {}

tab = str(chr(9))
newline = str(chr(13)) + str(chr(10))




def get_debtor_number(administration):
    debtor_number = None
    for name,organization in settings.items():
        if organization['admin'] == administration:
            debtor_number = organization['debno']
    if debtor_number == None:
        logs = open('../storage/app/generated-orders/errors/' + time.strftime('%Y%m%d_%H%M%S') + '.log', 'w')
        logs.write('orderGeneratorXmlConverter >>> Fatal error: Debtor number could\'nt be found for organization "' + str(name) + '"!')
        logs.close()
    else:
        return str(debtor_number)





def purchase(order):
    hash = str(random.getrandbits(128))
    xml  = '<?xml version="1.0" encoding="UTF-8"?>' + newline
    xml += '<!--' + newline + (tab*1) + 'Tyre Trading International B.V.' + newline + (tab*1) + 'Purchase Order Generator v1.0' + newline + (tab*1) + 'Author: Niek van Bennekom' + newline + (tab*1) + 'Email:  niek@tti.nl' + newline + '-->' + newline
    xml += '<AVXML>' + newline
    xml += (tab*1) + '<SIGNONMSGSRQ>' + newline
    xml += (tab*2) + '<SONRQ>' + newline
    xml += (tab*3) + '<ADMIN>' + str(order['inkoop admin']) + '</ADMIN>' + newline
    xml += (tab*3) + '<APPID>ACCVW</APPID>' + newline
    xml += (tab*3) + '<APPVER></APPVER>' + newline
    xml += (tab*2) + '</SONRQ>' + newline
    xml += (tab*1) + '</SIGNONMSGSRQ>' + newline
    xml += (tab*1) + '<EBUSMSGSRQ>' + newline
    xml += (tab*2) + '<EBUSTRNRQ>' + newline
    #xml += (tab*3) + '<TRNUID>INKOOP</TRNUID>' + newline
    xml += (tab*3) + '<BUSOBJ>PO1</BUSOBJ>' + newline
    xml += (tab*3) + '<ROWADDRQ>' + newline
    xml += (tab*4) + '<ORD_DATE>' + str(order['besteldatum']) + '</ORD_DATE>' + newline
    xml += (tab*4) + '<DEL_DATE>' + str(order['leverdatum']) + '</DEL_DATE>' + newline
    xml += (tab*4) + '<RPL_INV>' + str(order['crediteurnummer']) + '</RPL_INV>' + newline
    xml += (tab*4) + '<CUR_CODE>' + str(order['valuta']) + '</CUR_CODE>' + newline
    xml += (tab*4) + '<COMMENT1>' + str(order['referentie']) + '</COMMENT1>' + newline
    for line in order['regels']:
        xml += (tab*4) + '<PO_LINE>' + newline
        xml += (tab*5) + '<ART_CODE>' + str(line['artikelcode']) + '</ART_CODE>' + newline
        xml += (tab*5) + '<ART_QTY>' + str(line['aantal']) + '</ART_QTY>' + newline
        xml += (tab*5) + '<ART_PX>' + str((line['prijs'] + line['prijs marge']) + ((line['prijs'] + line['prijs marge']) * (line['procent marge'] / 100))) + '</ART_PX>' + newline
        xml += (tab*5) + '<GIP_PX>' + str(line['gip']) + '</GIP_PX>' + newline
        xml += (tab*5) + '<WHS_CODE>' + str(line['inkoop magazijn']) + '</WHS_CODE>' + newline
        xml += (tab*5) + '<LOC_CODE>' + str(line['inkoop locatie']) + '</LOC_CODE>' + newline
        xml += (tab*4) + '</PO_LINE>' + newline
    xml += (tab*3) + '</ROWADDRQ>' + newline
    xml += (tab*2) + '</EBUSTRNRQ>' + newline
    xml += (tab*1) + '</EBUSMSGSRQ>' + newline
    xml += '</AVXML>' + newline
    output = open('../storage/app/generated-orders/xml/' + hash + '_PO_admin[' + str(order['inkoop admin']) + ']_creditor[' + str(order['crediteurnummer']) + '].xml', 'w')
    output.write(xml)
    output.close()




def transfer(order):
    hash = str(random.getrandbits(128))
    xml  = '<?xml version="1.0" encoding="UTF-8"?>' + newline
    xml += '<!--' + newline + (tab*1) + 'Tyre Trading International B.V.' + newline + (tab*1) + 'Sales Order Generator v1.0' + newline + (tab*1) + 'Author: Niek van Bennekom' + newline + (tab*1) + 'Email:  niek@tti.nl' + newline + '-->' + newline
    xml += '<AVXML>' + newline
    xml += (tab*1) + '<SIGNONMSGSRQ>' + newline
    xml += (tab*2) + '<SONRQ>' + newline
    xml += (tab*3) + '<ADMIN>' + str(order['inkoop admin']) + '</ADMIN>' + newline
    xml += (tab*3) + '<APPID>ACCVW</APPID>' + newline
    xml += (tab*3) + '<APPVER></APPVER>' + newline
    xml += (tab*2) + '</SONRQ>' + newline
    xml += (tab*1) + '</SIGNONMSGSRQ>' + newline
    xml += (tab*1) + '<EBUSMSGSRQ>' + newline

    xml += (tab*2) + '<EBUSTRNRQ>' + newline
    xml += (tab*3) + '<TRNUID>VERKOOP</TRNUID>' + newline
    xml += (tab*3) + '<BUSOBJ>AR1</BUSOBJ>' + newline
    xml += (tab*3) + '<ROWMODRQ>' + newline
    xml += (tab*4) + '<RECID>' + get_debtor_number(order['eigenaar admin']) + '</RECID>' + newline
    xml += (tab*3) + '</ROWMODRQ>' + newline
    xml += (tab*2) + '</EBUSTRNRQ>' + newline

    xml += (tab*2) + '<EBUSTRNRQ>' + newline
    #xml += (tab*3) + '<TRNUID>VERKOOP</TRNUID>' + newline
    xml += (tab*3) + '<BUSOBJ>SO1</BUSOBJ>' + newline
    xml += (tab*3) + '<ROWADDRQ>' + newline
    xml += (tab*4) + '<RPL_DEL>' + get_debtor_number(order['eigenaar admin']) + '</RPL_DEL>' + newline
    xml += (tab*4) + '<RPL_INV>' + get_debtor_number(order['eigenaar admin']) + '</RPL_INV>' + newline
    xml += (tab*4) + '<SEL_CODE>ABCDEFGHIJKLMNOPQRSTUVWXYZ</SEL_CODE>' + newline
    xml += (tab*4) + '<COMMENT1>' + str(order['referentie']) + '</COMMENT1>' + newline
    for line in order['regels']:
        xml += (tab*4) + '<SO_LINE>' + newline
        xml += (tab*5) + '<ART_CODE>' + str(line['artikelcode']) + '</ART_CODE>' + newline
        xml += (tab*5) + '<ART_QTY>' + str(line['aantal']) + '</ART_QTY>' + newline
        xml += (tab*5) + '<ART_PX>' + str(line['prijs']) + '</ART_PX>' + newline
        #xml += (tab*5) + '<GIP_PX>' + str(line['gip']) + '</GIP_PX>' + newline
        xml += (tab*5) + '<WHS_CODE>' + str(line['inkoop magazijn']) + '</WHS_CODE>' + newline
        xml += (tab*5) + '<LOC_CODE>' + str(line['inkoop locatie']) + '</LOC_CODE>' + newline
        xml += (tab*4) + '</SO_LINE>' + newline
    xml += (tab*3) + '</ROWADDRQ>' + newline
    xml += (tab*2) + '</EBUSTRNRQ>' + newline

    xml += (tab*1) + '</EBUSMSGSRQ>' + newline
    xml += '</AVXML>' + newline
    output = open('../storage/app/generated-orders/xml/' + hash + '_SO_admin[' + str(order['inkoop admin']) + ']_newAdmin[' + str(order['eigenaar admin']) + '].xml', 'w')
    output.write(xml)
    output.close()


    xml  = '<?xml version="1.0" encoding="UTF-8"?>' + newline
    xml += '<!--' + newline + (tab*1) + 'Tyre Trading International B.V.' + newline + (tab*1) + 'Purchase Order Generator v1.0' + newline + (tab*1) + 'Author: Niek van Bennekom' + newline + (tab*1) + 'Email:  niek@tti.nl' + newline + '-->' + newline
    xml += '<AVXML>' + newline
    xml += (tab*1) + '<SIGNONMSGSRQ>' + newline
    xml += (tab*2) + '<SONRQ>' + newline
    xml += (tab*3) + '<ADMIN>' + str(order['eigenaar admin']) + '</ADMIN>' + newline
    xml += (tab*3) + '<APPID>ACCVW</APPID>' + newline
    xml += (tab*3) + '<APPVER></APPVER>' + newline
    xml += (tab*2) + '</SONRQ>' + newline
    xml += (tab*1) + '</SIGNONMSGSRQ>' + newline
    xml += (tab*1) + '<EBUSMSGSRQ>' + newline
    xml += (tab*2) + '<EBUSTRNRQ>' + newline
    #xml += (tab*3) + '<TRNUID>INKOOP</TRNUID>' + newline
    xml += (tab*3) + '<BUSOBJ>PO1</BUSOBJ>' + newline
    xml += (tab*3) + '<ROWADDRQ>' + newline
    xml += (tab*4) + '<ORD_DATE>' + str(order['besteldatum']) + '</ORD_DATE>' + newline
    xml += (tab*4) + '<DEL_DATE>' + str(order['leverdatum']) + '</DEL_DATE>' + newline
    xml += (tab*4) + '<RPL_INV>' + get_debtor_number(order['inkoop admin']) + '</RPL_INV>' + newline
    xml += (tab*4) + '<CUR_CODE>' + str(order['valuta']) + '</CUR_CODE>' + newline
    xml += (tab*4) + '<COMMENT1>' + str(order['referentie']) + '</COMMENT1>' + newline
    for line in order['regels']:
        xml += (tab*4) + '<PO_LINE>' + newline
        xml += (tab*5) + '<ART_CODE>' + str(line['artikelcode']) + '</ART_CODE>' + newline
        xml += (tab*5) + '<ART_QTY>' + str(line['aantal']) + '</ART_QTY>' + newline
        xml += (tab*5) + '<ART_PX>' + str((line['prijs'] + line['prijs marge']) + ((line['prijs'] + line['prijs marge']) * (line['procent marge'] / 100))) + '</ART_PX>' + newline
        xml += (tab*5) + '<GIP_PX>' + str(line['gip']) + '</GIP_PX>' + newline
        xml += (tab*5) + '<WHS_CODE>' + str(line['eigenaar magazijn']) + '</WHS_CODE>' + newline
        xml += (tab*5) + '<LOC_CODE>' + str(line['eigenaar locatie']) + '</LOC_CODE>' + newline
        xml += (tab*4) + '</PO_LINE>' + newline
    xml += (tab*3) + '</ROWADDRQ>' + newline
    xml += (tab*2) + '</EBUSTRNRQ>' + newline
    xml += (tab*1) + '</EBUSMSGSRQ>' + newline
    xml += '</AVXML>' + newline
    output = open('../storage/app/generated-orders/xml/' + hash + '_PO_admin[' + str(order['eigenaar admin']) + ']_oldAdmin[' + str(order['inkoop admin']) + '].xml', 'w')
    output.write(xml)
    output.close()




def purchase_transfer(order):
    hash = str(random.getrandbits(128))
    xml  = '<?xml version="1.0" encoding="UTF-8"?>' + newline
    xml += '<!--' + newline + (tab*1) + 'Tyre Trading International B.V.' + newline + (tab*1) + 'Purchase Order Generator v1.0' + newline + (tab*1) + 'Author: Niek van Bennekom' + newline + (tab*1) + 'Email:  niek@tti.nl' + newline + '-->' + newline
    xml += '<AVXML>' + newline
    xml += (tab*1) + '<SIGNONMSGSRQ>' + newline
    xml += (tab*2) + '<SONRQ>' + newline
    xml += (tab*3) + '<ADMIN>' + str(order['inkoop admin']) + '</ADMIN>' + newline
    xml += (tab*3) + '<APPID>ACCVW</APPID>' + newline
    xml += (tab*3) + '<APPVER></APPVER>' + newline
    xml += (tab*2) + '</SONRQ>' + newline
    xml += (tab*1) + '</SIGNONMSGSRQ>' + newline
    xml += (tab*1) + '<EBUSMSGSRQ>' + newline
    xml += (tab*2) + '<EBUSTRNRQ>' + newline
    #xml += (tab*3) + '<TRNUID>INKOOP</TRNUID>' + newline
    xml += (tab*3) + '<BUSOBJ>PO1</BUSOBJ>' + newline
    xml += (tab*3) + '<ROWADDRQ>' + newline
    xml += (tab*4) + '<ORD_DATE>' + str(order['besteldatum']) + '</ORD_DATE>' + newline
    xml += (tab*4) + '<DEL_DATE>' + str(order['leverdatum']) + '</DEL_DATE>' + newline
    xml += (tab*4) + '<RPL_INV>' + str(order['crediteurnummer']) + '</RPL_INV>' + newline
    xml += (tab*4) + '<CUR_CODE>' + str(order['valuta']) + '</CUR_CODE>' + newline
    xml += (tab*4) + '<COMMENT1>' + str(order['referentie']) + '</COMMENT1>' + newline
    for line in order['regels']:
        xml += (tab*4) + '<PO_LINE>' + newline
        xml += (tab*5) + '<ART_CODE>' + str(line['artikelcode']) + '</ART_CODE>' + newline
        xml += (tab*5) + '<ART_QTY>' + str(line['aantal']) + '</ART_QTY>' + newline
        xml += (tab*5) + '<ART_PX>' + str(line['prijs']) + '</ART_PX>' + newline
        xml += (tab*5) + '<GIP_PX>' + str(line['gip']) + '</GIP_PX>' + newline
        xml += (tab*5) + '<WHS_CODE>' + str(line['inkoop magazijn']) + '</WHS_CODE>' + newline
        xml += (tab*5) + '<LOC_CODE>' + str(line['inkoop locatie']) + '</LOC_CODE>' + newline
        xml += (tab*4) + '</PO_LINE>' + newline
    xml += (tab*3) + '</ROWADDRQ>' + newline
    xml += (tab*2) + '</EBUSTRNRQ>' + newline
    xml += (tab*1) + '</EBUSMSGSRQ>' + newline
    xml += '</AVXML>' + newline
    output = open('../storage/app/generated-orders/xml/' + hash + '_PO_admin[' + str(order['inkoop admin']) + ']_ownerAdmin[' + str(order['eigenaar admin']) + ']_creditor[' + str(order['crediteurnummer']) + '].xml', 'w')
    output.write(xml)
    output.close()


    xml  = '<?xml version="1.0" encoding="UTF-8"?>' + newline
    xml += '<!--' + newline + (tab*1) + 'Tyre Trading International B.V.' + newline + (tab*1) + 'Sales Order Generator v1.0' + newline + (tab*1) + 'Author: Niek van Bennekom' + newline + (tab*1) + 'Email:  niek@tti.nl' + newline + '-->' + newline
    xml += '<AVXML>' + newline
    xml += (tab*1) + '<SIGNONMSGSRQ>' + newline
    xml += (tab*2) + '<SONRQ>' + newline
    xml += (tab*3) + '<ADMIN>' + str(order['inkoop admin']) + '</ADMIN>' + newline
    xml += (tab*3) + '<APPID>ACCVW</APPID>' + newline
    xml += (tab*3) + '<APPVER></APPVER>' + newline
    xml += (tab*2) + '</SONRQ>' + newline
    xml += (tab*1) + '</SIGNONMSGSRQ>' + newline
    xml += (tab*1) + '<EBUSMSGSRQ>' + newline

    xml += (tab*2) + '<EBUSTRNRQ>' + newline
    #xml += (tab*3) + '<TRNUID>VERKOOP</TRNUID>' + newline
    xml += (tab*3) + '<BUSOBJ>AR1</BUSOBJ>' + newline
    xml += (tab*3) + '<ROWMODRQ>' + newline
    xml += (tab*4) + '<RECID>' + get_debtor_number(order['eigenaar admin']) + '</RECID>' + newline
    xml += (tab*3) + '</ROWMODRQ>' + newline
    xml += (tab*2) + '</EBUSTRNRQ>' + newline

    xml += (tab*2) + '<EBUSTRNRQ>' + newline
    xml += (tab*3) + '<TRNUID>VERKOOP</TRNUID>' + newline
    xml += (tab*3) + '<BUSOBJ>SO1</BUSOBJ>' + newline
    xml += (tab*3) + '<ROWADDRQ>' + newline
    xml += (tab*4) + '<RPL_DEL>' + get_debtor_number(order['eigenaar admin']) + '</RPL_DEL>' + newline
    xml += (tab*4) + '<RPL_INV>' + get_debtor_number(order['eigenaar admin']) + '</RPL_INV>' + newline
    xml += (tab*4) + '<SEL_CODE>ABCDEFGHIJKLMNOPQRSTUVWXYZ</SEL_CODE>' + newline
    xml += (tab*4) + '<COMMENT1>' + str(order['referentie']) + '</COMMENT1>' + newline
    for line in order['regels']:
        xml += (tab*4) + '<SO_LINE>' + newline
        xml += (tab*5) + '<ART_CODE>' + str(line['artikelcode']) + '</ART_CODE>' + newline
        xml += (tab*5) + '<ART_QTY>' + str(line['aantal']) + '</ART_QTY>' + newline
        xml += (tab*5) + '<ART_PX>' + str(line['prijs']) + '</ART_PX>' + newline
        #xml += (tab*5) + '<GIP_PX>' + str(line['gip']) + '</GIP_PX>' + newline
        xml += (tab*5) + '<WHS_CODE>' + str(line['inkoop magazijn']) + '</WHS_CODE>' + newline
        xml += (tab*5) + '<LOC_CODE>' + str(line['inkoop locatie']) + '</LOC_CODE>' + newline
        xml += (tab*4) + '</SO_LINE>' + newline
    xml += (tab*3) + '</ROWADDRQ>' + newline
    xml += (tab*2) + '</EBUSTRNRQ>' + newline

    xml += (tab*1) + '</EBUSMSGSRQ>' + newline
    xml += '</AVXML>' + newline
    output = open('../storage/app/generated-orders/xml/' + hash + '_SO_admin[' + str(order['inkoop admin']) + ']_newAdmin[' + str(order['eigenaar admin']) + ']_creditor[' + str(order['crediteurnummer']) + '].xml', 'w')
    output.write(xml)
    output.close()


    xml  = '<?xml version="1.0" encoding="UTF-8"?>' + newline
    xml += '<!--' + newline + (tab*1) + 'Tyre Trading International B.V.' + newline + (tab*1) + 'Purchase Order Generator v1.0' + newline + (tab*1) + 'Author: Niek van Bennekom' + newline + (tab*1) + 'Email:  niek@tti.nl' + newline + '-->' + newline
    xml += '<AVXML>' + newline
    xml += (tab*1) + '<SIGNONMSGSRQ>' + newline
    xml += (tab*2) + '<SONRQ>' + newline
    xml += (tab*3) + '<ADMIN>' + str(order['eigenaar admin']) + '</ADMIN>' + newline
    xml += (tab*3) + '<APPID>ACCVW</APPID>' + newline
    xml += (tab*3) + '<APPVER></APPVER>' + newline
    xml += (tab*2) + '</SONRQ>' + newline
    xml += (tab*1) + '</SIGNONMSGSRQ>' + newline
    xml += (tab*1) + '<EBUSMSGSRQ>' + newline
    xml += (tab*2) + '<EBUSTRNRQ>' + newline
    #xml += (tab*3) + '<TRNUID>INKOOP</TRNUID>' + newline
    xml += (tab*3) + '<BUSOBJ>PO1</BUSOBJ>' + newline
    xml += (tab*3) + '<ROWADDRQ>' + newline
    xml += (tab*4) + '<ORD_DATE>' + str(order['besteldatum']) + '</ORD_DATE>' + newline
    xml += (tab*4) + '<DEL_DATE>' + str(order['leverdatum']) + '</DEL_DATE>' + newline
    xml += (tab*4) + '<RPL_INV>' + get_debtor_number(order['inkoop admin']) + '</RPL_INV>' + newline
    xml += (tab*4) + '<CUR_CODE>' + str(order['valuta']) + '</CUR_CODE>' + newline
    xml += (tab*4) + '<COMMENT1>' + str(order['referentie']) + '</COMMENT1>' + newline
    for line in order['regels']:
        xml += (tab*4) + '<PO_LINE>' + newline
        xml += (tab*5) + '<ART_CODE>' + str(line['artikelcode']) + '</ART_CODE>' + newline
        xml += (tab*5) + '<ART_QTY>' + str(line['aantal']) + '</ART_QTY>' + newline
        xml += (tab*5) + '<ART_PX>' + str(line['prijs']) + '</ART_PX>' + newline
        xml += (tab*5) + '<GIP_PX>' + str(line['gip']) + '</GIP_PX>' + newline
        xml += (tab*5) + '<WHS_CODE>' + str(line['eigenaar magazijn']) + '</WHS_CODE>' + newline
        xml += (tab*5) + '<LOC_CODE>' + str(line['eigenaar locatie']) + '</LOC_CODE>' + newline
        xml += (tab*4) + '</PO_LINE>' + newline
    xml += (tab*3) + '</ROWADDRQ>' + newline
    xml += (tab*2) + '</EBUSTRNRQ>' + newline
    xml += (tab*1) + '</EBUSMSGSRQ>' + newline
    xml += '</AVXML>' + newline
    output = open('../storage/app/generated-orders/xml/' + hash + '_PO_admin[' + str(order['eigenaar admin']) + ']_oldAdmin[' + str(order['inkoop admin']) + ']_creditor[' + str(order['crediteurnummer']) + '].xml', 'w')
    output.write(xml)
    output.close()




for order in source:
    if order['bestelling id'] in orders:
        orders[order['bestelling id']]['regels'].append(order)
    else:
        orders[order['bestelling id']] = {
            'inkoop admin': order['inkoop admin'],
            'inkoop magazijn': order['inkoop magazijn'],
            'inkoop locatie': order['inkoop locatie'],
            'eigenaar admin': order['eigenaar admin'],
            'eigenaar magazijn': order['eigenaar magazijn'],
            'eigenaar locatie': order['eigenaar locatie'],
            'crediteurnummer': str(order['crediteurnummer']).replace('.0', ''),
            'besteldatum': order['besteldatum'],
            'leverdatum': order['leverdatum'],
            'valuta': order['valuta'],
            'referentie': order['referentie'],
            'regels': [order]
        }


for key,order in orders.items():
    if order['inkoop admin'] is not None and order['eigenaar admin'] is not None and (order['crediteurnummer'] is not None and order['crediteurnummer'] != 'None'):
        purchase_transfer(order)
    elif order['inkoop admin'] is not None and (order['eigenaar admin'] is None or order['eigenaar admin'] == 'None') and order['crediteurnummer'] is not None:
        purchase(order)
    elif order['inkoop admin'] is not None and order['eigenaar admin'] is not None and (order['crediteurnummer'] is None or order['crediteurnummer'] == 'None'):
        transfer(order)
    else:
        logs = open('../storage/app/generated-orders/errors/' + time.strftime('%Y%m%d_%H%M%S') + '.log', 'w')
        logs.write('orderGeneratorXmlConverter >>> Fatal error: Order type could\'nt be determined!\n\nStack:\n' + str(order))
        logs.close()
