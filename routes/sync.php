<?php
//products
Route::post('product', ["uses" => "ProductController@product"]);
Route::post('products', ["uses" => "ProductController@products"]);
Route::post('product/{sku}/group', ["uses" => "ProductController@productAttach"]);

//stock
Route::post('stock', ["uses" => "StockController@stock"]);

//orders
Route::get('orders', ["uses" => "OrderController@orders"]);
Route::get('order/newest', ["uses" => "OrderController@newestOrder"]);
Route::get('order/{orderid}', ["uses" => "OrderController@getOrder"]);

//ibox
Route::post('ibox/{type}', ["uses" => "iBoxController@ibox"]);

Route::get('shop/{shop_id}/category/{category_id}/products', ["uses" => "CategoryController@shopCategoryProducts"]);