<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CartRule extends Model
{
	public function countries()
	{
		return $this->belongsToMany(Country::class);
	}

    public function customer_groups()
    {
        return $this->belongsToMany(CustomerGroup::class);
    }

    public function customers()
	{
		return $this->belongsToMany(Customer::class);
	}

	public function products()
	{
		return $this->belongsToMany(Product::class);
	}

	public function feature_types()
	{
		return $this->belongsToMany(ProductFeatureType::class);
	}

	public function categories()
	{
		return $this->belongsToMany(Category::class);
	}

    public function setCartRule($request)
    {
    	$this->active = $request->active;
    	$this->giftcard = $request->giftcard;
    	$this->free_shipping = $request->free_shipping;
    	$this->name = $request->name;
    	$this->amount_or_percentage = $request->amount_or_percentage;
    	$this->amount = ($this->amount_or_percentage == 1)? $request->price_amount : $request->percentage_amount;
    	$this->voucher_code = strtoupper($request->voucher_code);
        $this->usage_limit = (!$this->giftcard && $request->usage_limit)? $request->usage_limit : null;
    	$this->valid_from = date('Y-m-d', strtotime($request->valid_from));
    	$this->valid_until = date('Y-m-d', strtotime($request->valid_until));
    	$this->save();
    }

    public function syncCountries($countries)
    {
    	$this->countries()->sync($countries);
    }

    public function syncCustomerGroups($customer_groups)
    {
        $this->customer_groups()->sync($customer_groups);
    }

    public function syncCustomers($customers)
    {
        $this->customers()->sync($customers);
    }

    public function syncProducts($products)
    {
    	$this->products()->sync($products);
    }

    public function syncFeatureTypes($feature_types)
    {
    	$this->feature_types()->sync($feature_types);
    }

    public function syncCategories($categories)
    {
    	$this->categories()->sync($categories);
    }
}
