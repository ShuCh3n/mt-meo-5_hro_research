@extends('dashboard.layout.main')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-6">
            <h2>{{ trans('pages.inquiry_detail_header') }}</h2>
            <ol class="breadcrumb">
                <li>
                    {{ trans('menu.home') }}
                </li>
                <li>
                    {{ trans('menu.inquiries') }}
                </li>
                <li class="active">
                    <strong>{{ trans('page.inquiry') }}</strong>
                </li>
            </ol>
        </div>

        <div class="mt30">
            <div class="col-lg-2 col-lg-offset-2">
                <product-csv-import-button uri="{{ route('product_sheet_upload') }}"></product-csv-import-button>
            </div>

            <div class="col-lg-2">
                <a class="btn btn-outline btn-primary btn-block" href="{{ route('new_product') }}"><i class="fa fa-plus-circle" aria-hidden="true"></i> New product</a>
            </div>
        </div>
    </div>

	<div class="wrapper wrapper-content animated fadeInRight ecommerce">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">

                    <div class="ibox-content">

                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>
                                        @lang('product.shop')
                                    </th>
                                    <th>
                                        @lang('product.products')
                                    </th>
                                    <th class="text-right" data-sort-ignore="true">@lang('product.action')</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        @lang('product.all')
                                    </td>
                                    <td>
                                        {{ $product_count }}
                                    </td>
                                    <td class="text-right">
                                        <div class="btn-group">
                                            <a href="{{ route('shop_products', "all") }}" class="btn-white btn btn-xs">@lang('product.view')</a>
                                        </div>
                                    </td>
                                </tr>
                                @foreach($shops as $shop)
                                    <tr>
                                        <td>
                                           {{ $shop->name }}
                                        </td>
                                        <td>
                                          {{ $shop->shop_products()->count() }}
                                        </td>
                                        <td class="text-right">
                                            <div class="btn-group">
                                                <a href="{{ route('shop_products', $shop->id) }}" class="btn-white btn btn-xs">@lang('product.view')</a>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
