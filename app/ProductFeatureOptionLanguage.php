<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductFeatureOptionLanguage extends Model
{
    protected $fillable = ['product_feature_option_id', 'language_id', 'name'];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    protected $touches = ['products', 'product_feature_option'];

    public function product_feature_option()
    {
    	return $this->belongsTo(ProductFeatureOption::class);
    }

    public function products()
    {
    	return $this->belongsToMany(Product::class, 'product_product_feature_option', 'product_feature_option_id', 'product_id');
    }

    public function scopeLanguage($query, $language_id)
    {
        return $query->where('language_id', $language_id);
    }

    public function scopeBackOffice($query)
    {
        return $query->where('language_id', auth()->user()->preferenceLanguage()->id);
    }

    public function scopeOption($query, $option)
    {
        return $query->where('product_feature_option_id', $option);
    }
}
