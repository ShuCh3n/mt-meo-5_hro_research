<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\ProductImage;
use App\Shop;
use App\Brand;
use App\Image;

use Storage;

class FileController extends Controller
{

    public function viewProductImage()
    {
    	if(is_numeric($this->request->id))
    	{
    		$image = Image::findOrFail($this->request->id);
			return redirect(env('CDN_URI').'/cdn/images/'.$image->filename.'.jpg');
    	}

    	return $this->noImage();
    }

	public function viewProductThumb()
    {
    	if(is_numeric($this->request->id))
    	{
    		$image = Image::findOrFail($this->request->id);
			return redirect(env('CDN_URI').'cdn/images/'.$image->filename.'_thumb.jpg');
    	}

    	return $this->noImage();
    }

    public function shopProductImage()
    {
        if(is_numeric($this->request->id))
        {
        	$shop = Shop::where('shop_link', $this->request->shop_name)->firstOrFail();
        	$product_image = ProductImage::findOrFail($this->request->id);

        	if($product_image->product->hasShopID($shop->id))
        	{
        		return $this->viewProductImage();
        	}
		}

		return $this->noImage();
    }

    public function viewBrandImage()
    {
        $brand = Brand::findOrFail($this->request->id);
        $file = Storage::get('brands/' . $brand->logo);
        return response($file)->header('Content-Type', 'image/jpg');
    }

    public function shopBrandImage()
    {
        $shop = Shop::where('shop_link', $this->request->shop_name)->firstOrFail();
        if($shop->brands()->contains('id', 3))
        {
            return $this->viewBrandImage();
        }

        return $this->noImage();
    }

    public function viewShopImage()
    {
        if(is_numeric($this->request->id))
        {
            $shop = Shop::findOrFail($this->request->id);
            $file = Storage::get('shops/' . $shop->logo);
            return response($file)->header('Content-Type', 'image/jpg');
        }

        return $this->noImage();
    }

    public function shopLogo()
    {
        $shop = Shop::where('shop_link', $this->request->shop_name)->firstOrFail();
        $file = Storage::get('shops/' . $shop->logo);

        return response($file)->header('Content-Type', 'image/jpg');
    }

    public function noImage()
    {
    	$file = Storage::get('assets/no_image_' . $this->request->size . '.jpg');
    	return response($file)->header('Content-Type', 'image/jpg');
    }
}
