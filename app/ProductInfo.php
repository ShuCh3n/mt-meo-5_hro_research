<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductInfo extends Model
{
    protected $fillable = ['language_id', 'product_id', 'name', 'short_description', 'description', 'specification', 'link'];

    public function setProductInfo($info)
    {
        $this->name = $info["product_name"];
        $this->short_description = $info["meta_description"];
        $this->description = $info["description"];
        $this->specification = (!isset($info["specification"]))?: $info["specification"];
        $this->link = $info["meta_link"];
        $this->save();
    }

    public static function getProductInfo($product, $language)
    {
    	return self::where('language_id', $language->id)->where('product_id', $product->id)->first();
    }

    public static function insertIfNotExist($product, $language, $info)
    {
    	$product_info = self::getProductInfo($product, $language);

    	if(!$product_info)
    	{
    		$product_info = new self;
    		$product_info->language_id = $language->id;
            $product_info->product_id = $product->id;
    	}

    	return $product_info;
    }
}
