<?php

Route::group(['middleware' => 'auth'], function () {
	//home
	Route::get('/', ["uses" => "DashboardController@home", "as" => "home"]);
	Route::get('script', ["uses" => "DashboardController@script"]);
    Route::get('lang/{language}', ["uses" => "FrontpageController@lang", "as" => "lang"]);
    
	//orders
	Route::get('orders', ["uses" => "OrderController@orders", "as" => "orders"]);
	Route::get('order/view/{id}', ["uses" => "OrderController@viewOrder", "as" => "view_order"]);
	Route::get('order/new', ["uses" => "OrderController@newOrder", "as" => "new_order"]);

	Route::post('order/new', ["uses" => "OrderController@actionNewOrder", "as" => "new_order"]);

	Route::get('reward-programs', ["uses" => "RewardProgramController@programs"])->name('reward_programs');
	Route::get('reward-program/new', ["uses" => "RewardProgramController@newProgram"])->name('new_reward_program');
	Route::get('reward-program/edit/{id}', ["uses" => "RewardProgramController@editProgram"])->name('edit_reward_program');
	Route::get('reward-program/view/{id}', ["uses" => "RewardProgramController@programOrders"])->name('reward_program_orders');
	Route::get('reward-program/{program_id}/order/{id}', ["uses" => "RewardProgramController@viewProgramOrder"])->name('view_program_order');

	Route::post('reward-program/new', ["uses" => "RewardProgramController@createProgram"]);
	Route::post('reward-program/edit/{id}', ["uses" => "RewardProgramController@modifyProgram"]);

	Route::get('generated-orders', ["uses" => "OrderController@generatedOrders", "as" => "generated_orders"]);
    Route::post('generate-order/upload', ["uses" => "OrderController@uploadGeneratedOrderFile"])->name('upload_generated_order_file');
    Route::get('generated-order/remove/{id}', ["uses" => "OrderController@removeFile"])->name('remove_generated_order_file');
    Route::get('generate-order/review/{id}', ["uses" => "OrderController@reviewGeneratedOrder"])->name('review_generated_order_file');
    Route::get('generate-order/process/{id}', ["uses" => "OrderController@processGeneratedOrder"])->name('process_generated_order_file');
    //Route::get('generate-order/process/{id}', ["uses" => "OrderController@updateGeneratedOrderSettings"])->name('update_generated_order_settings');

	//products
	Route::get('products', ["uses" => "ProductController@products", "as" => "products"]);
	Route::get('products/shop/{shop_id}', ["uses" => "ProductController@shopProducts", "as" => "shop_products"]);
	Route::get('product/edit/{id}', ["uses" => "ProductController@editProduct", "as" => "edit_product"]);
    Route::get('product/edit/sku/{sku}', ["uses" => "ProductController@editProductSku", "as" => "edit_sku"]);
	Route::get('product/new', ["uses" => "ProductController@newProduct", "as" => "new_product"]);
	Route::get('product/images/{id}/{size}/{filename}', ["uses" => "FileController@viewProductImage", "as" => "product_image"]);
	Route::get('product/edit/{product_id}/remove-image/{id}', ["uses" => "ProductController@removeImage"]);
	Route::get('product/view/{id}', ["uses" => "ProductController@viewProduct", "as" => "view_product"]);

	Route::post('product/new', ["uses" => "ProductController@actionNewProduct"]);
	Route::post('product/upload/images', ["uses" => "ProductController@uploadImage"]);
	Route::post('product/edit/{id}/reoder-image', ["uses" => "ProductController@reoderImage"]);
	Route::post('product/edit/{id}', ["uses" => "ProductController@actionEditProduct", "as" => "update_product"]);
	Route::post('product/edit/{id}/image-upload', ["uses" => "ProductController@actionUploadImage"]);
	Route::post('products/import', ["uses" => "ProductController@importFromFile"])->name('product_sheet_upload');
	Route::post('products/import/execute', ["uses" => "ProductController@importFromFileExecute", "as" => "product_importer_execute"]);
	Route::post('products/bulk-edit', ["uses" => "ProductController@bulkEditProduct", "as" => "product_bulk_edit"]);
	Route::post('products/bulk-edit/action', ["uses" => "ProductController@actionBulkEditProduct", "as" => "product_bulk_edit_action"]);

	Route::get('product/groups', ["uses" => "ProductGroupController@index", "as" => "product_groups"]);
	Route::get('product/group/new', ["uses" => "ProductGroupController@newProductGroup", "as" => "new_product_group"]);
	Route::get('product/group/edit/{id}', ["uses" => "ProductGroupController@editProductGroup", "as" => "edit_product_group"]);
	Route::get('product/group/{id}', ["uses" => "ProductGroupController@viewProductGroup", "as" => "product_group"]);

	Route::post('product/group/new', ["uses" => "ProductGroupController@actionNewProductGroup"]);
	Route::post('product/group/edit/{id}', ["uses" => "ProductGroupController@actionEditProductGroup"]);

	Route::get('product/attributes', ["uses" => "ProductAttributeController@index", "as" => "product_attributes"]);
	Route::get('product/attribute/new', ["uses" => "ProductAttributeController@newProductAttribute", "as" => "new_product_attribute"]);
	Route::get('product/attribute/{id}', ["uses" => "ProductAttributeController@productAttributeOptions", "as" => "product_attribute_options"]);
	Route::get('product/attribute/option/new', ["uses" => "ProductAttributeController@newProductAttributeOption", "as" => "new_product_attribute_option"]);
	Route::get('product/attribute/edit/{id}', ["uses" => "ProductAttributeController@editProductAttribute", "as" => "edit_product_attribute"]);
	Route::get('product/attribute/remove/{id}', ["uses" => "ProductAttributeController@removeProductAttribute", "as" => "remove_product_attribute"]);
	Route::get('product/attribute/{attribute_id}/option/edit/{id}', ["uses" => "ProductAttributeController@editProductAttributeOption", "as" => "edit_product_attribute_option"]);
	Route::get('product/attribute/{attribute_id}/option/remove/{id}', ["uses" => "ProductAttributeController@removeProductAttributeOption", "as" => "remove_product_attribute_option"]);

	Route::post('product/attribute/new', ["uses" => "ProductAttributeController@actionNewProductAttribute"]);
	Route::post('product/attribute/option/new', ["uses" => "ProductAttributeController@actionNewProductAttributeOption"]);
	Route::post('product/attribute/edit/{id}', ["uses" => "ProductAttributeController@actionEditProductAttribute"]);
	Route::post('product/attribute/{attribute_id}/option/edit/{id}', ["uses" => "ProductAttributeController@actionEditProductAttributeOption"]);

	Route::get('product/feature-types', ["uses" => "ProductFeatureController@index", "as" => "product_feature_type"]);
	Route::get('product/feature-type/new', ["uses" => "ProductFeatureController@newProductFeatureType", "as" => "new_product_feature_type"]);
	Route::get('product/feature-type/{id}', ["uses" => "ProductFeatureController@productFeatures", "as" => "product_features"]);
	Route::get('product/feature-type/edit/{id}', ["uses" => "ProductFeatureController@editProductFeatureType", "as" => "edit_product_feature_type"]);
	Route::get('product/feature-type/remove/{id}', ["uses" => "ProductFeatureController@removeProductFeatureType", "as" => "remove_product_feature_type"]);
	Route::get('product/feature-type/{id}/feature/new', ["uses" => "ProductFeatureController@newProductFeature", "as" => "new_product_feature"]);
	Route::get('product/feature-type/{feature_type_id}/edit/feature/{id}', ["uses" => "ProductFeatureController@editProductFeature", "as" => "edit_product_feature"]);
	Route::get('product/feature-type/{feature_id}/remove/feature/{id}', ["uses" => "ProductFeatureController@removeProductFeature", "as" => "remove_product_feature"]);
	Route::get('product/feature-option/transfer/{id}', ["uses" => "ProductFeatureController@transferOption", "as" => "transfer_feature_option"]);

	Route::post('product/feature-type/new', ["uses" => "ProductFeatureController@actionNewProductFeatureType"]);
	Route::post('product/feature-type/edit/{id}', ["uses" => "ProductFeatureController@actionEditProductFeatureType"]);
	Route::post('product/feature-type/{feature_type_id}/edit/feature/{id}', ["uses" => "ProductFeatureController@actionEditProductFeature"]);
	Route::post('product/feature-type/{id}/feature/new', ["uses" => "ProductFeatureController@actionNewProductFeature"]);
	Route::post('product/feature-option/transfer/{id}', ["uses" => "ProductFeatureController@actionTransferOption"]);

    Route::get('product/files/{product_id}', ["uses" => "ProductController@productFiles", "as" => "uploadFile"]);
	Route::post('product/files/upload/{product_id}', ["uses" => "ProductController@productFileUpload", "as" => "uploadFileAction"]);

    Route::get('product/images/bulk', ["uses" => "ImageController@bulkImage", "as" => "uploadImageBulk"]);
	Route::post('product/images/bulk/upload', ["uses" => "ImageController@bulkImageUpload", "as" => "uploadImageBulkAction"]);

	//price rules
	Route::get('price-rules/cart-rules', ["uses" => "PriceRuleController@cartRules", "as" => "cart_rules"]);
	Route::get('price-rules/cart-rule/new', ["uses" => "PriceRuleController@newCartRules", "as" => "new_cart_rule"]);
	Route::get('price-rules/cataglog-price-rules', ["uses" => "PriceRuleController@cataglogPriceRules", "as" => "catalog_price_rules"]);
	Route::get('price-rules/cataglog-price-rule/new', ["uses" => "PriceRuleController@newCatalogPriceRule", "as" => "new_catalog_price_rule"]);
	Route::get('price-rules/cataglog-price-rule/edit/{id}', ["uses" => "PriceRuleController@editCatalogPriceRule", "as" => "edit_catalog_price_rule"]);
	Route::get('price-rules/cataglog-price-rule/remove/{id}', ["uses" => "PriceRuleController@removeCatalogPriceRule", "as" => "remove_catalog_price_rule"]);
	Route::get('price-rules/bulk-price-rules', ["uses" => "PriceRuleController@bulkPriceRules", "as" => "bulk_price_rules"]);
	Route::get('price-rules/bulk-price-rule/new', ["uses" => "PriceRuleController@newBulkPriceRule", "as" => "new_bulk_price_rule"]);

	Route::get('price-rules/wholesale-price-rules', ["uses" => "PriceRuleController@wholesalePriceRules", "as" => "wholesale_price_rules"]);

	Route::post('price-rules/cart-rule/new', ["uses" => "PriceRuleController@actionNewCartRules"]);
	Route::post('price-rules/cataglog-price-rule/new', ["uses" => "PriceRuleController@actionNewCatalogPriceRule"]);
	Route::post('price-rules/cataglog-price-rule/edit/{id}', ["uses" => "PriceRuleController@actionEditCatalogPriceRule"]);
	Route::post('price-rules/bulk-price-rule/new', ["uses" => "PriceRuleController@actionNewBulkPriceRule"]);


	//brands
	Route::get('brands', ["uses" => "BrandController@brands", "as" => "brands"]);
	Route::get('brand/edit/{id}', ["uses" => "BrandController@editBrand", "as" => "edit_brand"]);
	Route::get('brand/new', ["uses" => "BrandController@newBrand", "as" => "new_brand"]);
	Route::get('brand/image/{id}/{brandname}', ["uses" => "FileController@viewBrandImage", "as" => "brand_image"]);

	Route::post('brand/new', ["uses" => "BrandController@actionNewBrand"]);
	Route::post('brand/edit/{id}', ["uses" => "BrandController@actionEditBrand", "as" => "edit_brand"]);

	//categories
	Route::get('categories', ["uses" => "CategoryController@categories", "as" => "categories"]);
	Route::get('categories/shop/{id}', ["uses" => "CategoryController@shopCategories", "as" => "shop_categories"]);
	Route::get('categories/shop/{shop_id}/category/{id}', ["uses" => "CategoryController@shopCategory", "as" => "shop_category"]);
	Route::get('category/new', ["uses" => "CategoryController@newCategory", "as" => "new_category"]);
	Route::get('category/edit/{id}', ["uses" => "CategoryController@editCategory"])->name('edit_category');

	Route::post('category/new', ["uses" => "CategoryController@actionNewCategory"]);
	Route::post('category/edit/{id}', ["uses" => "CategoryController@actionEditCategory"]);

	//shops
	Route::get('shops', ["uses" => "ShopController@shops", "as" => "shops"]);
	Route::get('shops/new', ["uses" => "ShopController@newShop", "as" => "new_shop"]);
	Route::get('shop/edit/{id}', ["uses" => "ShopController@editShop", "as" => "edit_shop"]);
	Route::get('shop/image/{id}/{shop_name}', ["uses" => "FileController@viewShopImage", "as" => "shop_image"]);

	Route::post('shops/new', ["uses" => "ShopController@actionNewShop"]);
	Route::post('shop/edit/{id}', ["uses" => "ShopController@actionEditShop"]);


	//customers
	Route::get('customers', ["uses" => "CustomerController@customers", "as" => "customers"]);
	Route::get('customer/view/{id}', ["uses" => "CustomerController@viewCustomer", "as" => "view_customer"]);
	Route::get('customer/new', ["uses" => "CustomerController@newCustomer", "as" => "new_customer"]);
	Route::get('customer/edit/{id}', ["uses" => "CustomerController@editCustomer", "as" => "edit_customer"]);
	Route::get('customers/addresses', ["uses" => "DashboardController@customersAddresses", "as" => "customer_addresses"]);
	Route::get('customers/address/edit/{id}', ["uses" => "CustomerController@editCustomerAddress", "as" => "edit_customer_address"]);
	Route::get('customers/groups', ["uses" => "CustomerController@customerGroups", "as" => "customer_groups"]);
	Route::get('customers/group/new', ["uses" => "CustomerController@newCustomerGroup", "as" => "new_customer_group"]);
	Route::get('customers/group/edit/{id}', ["uses" => "CustomerController@editCustomerGroup", "as" => "edit_customer_group"]);
	Route::get('customers/address/new', ["uses" => "CustomerController@newCustomerAddress", "as" => "new_customer_address"]);
	Route::get('customer/avatar/{id}', ["uses" => "CustomerController@viewAvatar", "as" => "customer_avatar"]);

	Route::get('customer/carts', ["uses" => "CartController@index", "as" => "carts"]);
	Route::get('customer/cart/{id}', ["uses" => "CartController@viewCart", "as" => "view_cart"]);
	Route::get('customer/cart/{id}/remove', ["uses" => "CartController@removeCart", "as" => "remove_cart"]);

	Route::post('customer/new', ["uses" => "CustomerController@actionNewCustomer"]);
	Route::post('customers/group/new', ["uses" => "CustomerController@actionNewCustomerGroup"]);
	Route::post('customers/group/edit/{id}', ["uses" => "CustomerController@modifyCustomerGroup"]);
	Route::post('customers/address/new', ["uses" => "CustomerController@actionNewCustomerAddress"]);
	Route::post('customers/address/edit/{id}', ["uses" => "CustomerController@actionEditCustomerAddress"]);
	Route::post('customer/edit/{id}', ["uses" => "CustomerController@actionEditCustomer", "as" => "action_edit_customer"]);


    // Customers - fetch from external source using creditor number
	Route::get('customer/resolve-creditor/{code}', ["uses" => "CustomerController@resolveCreditor", "as" => "resolve_creditor"]);


	//taxes
	Route::get('taxes/groups', ["uses" => "LocaleController@taxGroups", "as" => "tax_groups"]);
	Route::get('taxes/groups/new', ["uses" => "LocaleController@newTaxGroup", "as" => "new_tax_group"]);
	Route::get('taxes/group/edit/{id}', ["uses" => "LocaleController@editTaxGroup", "as" => "edit_tax_group"]);
	Route::get('taxes/group/{id}', ["uses" => "LocaleController@viewTaxGroup", "as" => "view_tax_group"]);
	Route::get('taxes/group/{id}/new', ["uses" => "LocaleController@newTax", "as" => "new_tax"]);
	Route::get('taxes/group/{group_id}/edit/{id}', ["uses" => "LocaleController@editTax", "as" => "edit_tax"]);

	Route::post('taxes/groups/new', ["uses" => "LocaleController@addTaxGroup"]);
	Route::post('taxes/group/edit/{id}', ["uses" => "LocaleController@editActionTaxGroup"]);
	Route::post('taxes/group/{group_id}/new', ["uses" => "LocaleController@newActionTax"]);
	Route::post('taxes/group/{group_id}/edit/{id}', ["uses" => "LocaleController@editActionTax"]);


	//shipments
	Route::get('shippings', ["uses" => "ShippingController@shippings", "as" => "shippings"]);
	Route::get('shipping/new', ["uses" => "ShippingController@newShipping", "as" => "new_shipping"]);


	//messages
	Route::get('messages', ["uses" => "MessageController@messages", "as" => "messages"]);
	Route::get('messages/closed', ["uses" => "MessageController@closedMessages", "as" => "closed_messages"]);
	Route::get('message/{id}', ["uses" => "MessageController@viewMessage", "as" => "view_messages"]);
	Route::get('message/{id}/close', ["uses" => "MessageController@closeTicket", "as" => "close_ticket"]);

	Route::post('message/{id}', ["uses" => "MessageController@actionUserReplyMessage"]);

	//cms pages
	Route::get('cms/pages', ["uses" => "CMSController@pages", "as" => "pages"]);
	Route::get('cms/page/new', ["uses" => "CMSController@newPage", "as" => "new_page"]);
	Route::get('cms/pages/{shop_id}', ["uses" => "CMSController@shopPages", "as" => "shop_pages"]);
	Route::get('cms/page/edit/{id}', ["uses" => "CMSController@editPage", "as" => "edit_page"]);

	Route::post('cms/page/new', ["uses" => "CMSController@actionNewPage"]);
	Route::post('cms/page/edit/{id}', ["uses" => "CMSController@actionEditPage"]);

	//cms emails
	Route::get('cms/emails', ["uses" => "CMSController@emails", "as" => "emails"]);
	Route::get('cms/emails/{id}', ["uses" => "CMSController@shopEmails", "as" => "shop_emails"]);
	Route::get('cms/email/new', ["uses" => "CMSController@newShopEmail", "as" => "new_email"]);
	Route::get('cms/email/edit/{id}', ["uses" => "CMSController@editShopEmail", "as" => "edit_email"]);

	Route::post('cms/email/new', ["uses" => "CMSController@actionNewShopEmail"]);
	Route::post('cms/email/edit/{id}', ["uses" => "CMSController@actionEditShopEmail"]);

	//cms blogs
	Route::get('cms/blogs', ["uses" => "CMSController@blogs", "as" => "blogs"]);
	Route::get('cms/blog/details/{id}', ["uses" => "CMSController@blog", "as" => "blog"]);
	Route::get('cms/blog/new', ["uses" => "CMSController@newBlog", "as" => "new_blog"]);
	Route::get('cms/blog/category/new', ["uses" => "CMSController@newBlogCategory", "as" => "new_blog_category"]);
	Route::get('cms/blog/shop/{shop_id}', ["uses" => "CMSController@shopBlogs", "as" => "shop_blog"]);
	Route::get('cms/blog/edit/{id}', ["uses" => "CMSController@editBlog", "as" => "edit_blog"]);
	Route::get('cms/blog/delete/{id}', ["uses" => "CMSController@deleteBlog", "as" => "delete_blog"]);

	Route::post('cms/blog/new', ["uses" => "CMSController@createBlog"]);
	Route::post('cms/blog/edit/{id}', ["uses" => "CMSController@updateBlog"]);
	Route::post('cms/blog/category/new', ["uses" => "CMSController@createBlogCategory"]);

	Route::get('cms/blog/article/{id}', ["uses" => "CMSController@blogArticle", "as" => "blog_article"]);
	Route::get('cms/blog/{blogid}/article/new/', ["uses" => "CMSController@newBlogArticle", "as" => "new_blog_article"]);
	Route::get('cms/blog/{blogid}/article/{id}/edit/', ["uses" => "CMSController@editBlogArticle", "as" => "edit_blog_article"]);
	Route::get('cms/blog/{blogid}/article/{id}/delete/', ["uses" => "CMSController@deleteBlogArticle", "as" => "delete_blog_article"]);

	Route::post('cms/blog/{blogid}/article/new/', ["uses" => "CMSController@createBlogArticle", "as" => "create_blog_article"]);
	Route::post('cms/blog/{blogid}/article/{id}/edit/', ["uses" => "CMSController@updateBlogArticle", "as" => "update_blog_article"]);

    Route::get('cms/blog/{blogid}/tags', ["uses" => "CMSController@editTags", "as" => "edit_blog_tags"]);
    Route::get('cms/blog/tag/{tagid}/edit', ["uses" => "CMSController@editTag", "as" => "edit_blog_tag"]);
    Route::get('cms/blog/tag/{tagid}/delete', ["uses" => "CMSController@deleteTag", "as" => "delete_blog_tag"]);
    Route::post('cms/blog/tag/{tagid}', ["uses" => "CMSController@updateTag", "as" => "update_blog_tag"]);
    Route::get('cms/blog/{blogid}/tag/new', ["uses" => "CMSController@editTag", "as" => "new_blog_tag"]);

	//warehouses
	Route::get('warehouses', ["uses" => "WarehouseController@index", "as" => "warehouses"]);
	Route::get('warehouse/new', ["uses" => "WarehouseController@newWarehouse", "as" => "new_warehouse"]);
	Route::get('warehouse/edit/{id}', ["uses" => "WarehouseController@editWarehouse", "as" => "edit_warehouse"]);

	Route::post('warehouse/new', ["uses" => "WarehouseController@actionNewWarehouse"]);
	Route::post('warehouse/edit/{id}', ["uses" => "WarehouseController@actionEditWarehouse"]);

	//images
	Route::get('cms/images', ["uses" => "ImageController@Index", "as" => "images"]);

	//vehicles
	Route::get('vehicles/motors', ["uses" => "VehicleController@motors", "as" => "motors"]);
	Route::get('vehicles/motor/new', ["uses" => "VehicleController@newMotor", "as" => "new_motor"]);
	Route::get('vehicles/motor/view/{id}', ["uses" => "VehicleController@viewMotor", "as" => "view_motor"]);
	Route::get('vehicles/motor/edit/{id}', ["uses" => "VehicleController@editMotor"])->name('edit_motor');
	Route::get('vehicles/motor/export/{id}', ["uses" => "VehicleController@exportMotor"])->name('export_motor');

	Route::post('vehicles/motor/new', ["uses" => "VehicleController@actionNewMotor"]);
	Route::post('vehicles/motor/view/{id}', ["uses" => "VehicleController@motorAddProduct"])->name('motor_add_product');
	Route::post('vehicles/motor/view/{id}/add-product', ["uses" => "VehicleController@motorAddProduct"])->name('motor_add_product');
	Route::post('vehicles/motor/edit/{id}', ["uses" => "VehicleController@actionEditMotor"]);

	//ibox
	Route::get('ibox/analist', ["uses" => "iBoxController@analist", "as" => "analist"]);
	Route::get('ibox/account-view', ["uses" => "iBoxController@accountView"])->name('account_view');
	Route::get('ibox/platforms', ["uses" => "iBoxController@platforms", "as" => "platforms"]);
	Route::get('ibox/platform/{id}/products', ["uses" => "iBoxController@platformProducts"])->name('platform_products');
	Route::get('ibox/platform/{id}/setting', ["uses" => "iBoxController@platformSettings"])->name('platform_setting');
	Route::get('ibox/platforms/sync/{type}', ["uses" => "iBoxController@platforms", "as" => "sync_ibox"]);
	Route::get('ibox/import', ["uses" => "iBoxController@import"]);
	Route::get('ibox/analist/remove/{id}', ["uses" => "iBoxController@removeFile"])->name('remove_analyse_file');
	Route::get('ibox/statistics', ["uses" => "iBoxController@statistics"])->name('ibox_statistics');

	Route::post('ibox/analist/analyze', ["uses" => "iBoxController@analyze", "as" => "analyze"]);
	Route::post('ibox/analist/upload', ["uses" => "iBoxController@uploadAnalistFile"])->name('upload_analist_file');
	Route::post('ibox/platform/{id}/setting', ["uses" => "iBoxController@actionPlatformSettings"]);
	Route::post('ibox/account-view', ["uses" => "iBoxController@actionAccountView"]);

	//management
	Route::get('management/employees', ["uses" => "ManagementController@employees", "as" => "employees"]);
	Route::get('management/employee/edit/{id}', ["uses" => "ManagementController@editEmployee", "as" => "edit_employee"]);
	Route::get('management/departments', ["uses" => "ManagementController@departments", "as" => "departments"]);
	Route::get('management/department/new', ["uses" => "ManagementController@newDepartment", "as" => "new_department"]);
	Route::get('management/department/edit/{id}', ["uses" => "ManagementController@editDepartment", "as" => "edit_department"]);

	Route::post('management/department/new', ["uses" => "ManagementController@actionNewDepartment"]);
	Route::post('management/department/edit/{id}', ["uses" => "ManagementController@actionEditDepartment"]);

	Route::get('search', ["uses" => "SearchController@index", "as" => "search"]);

	//misc
	Route::get('logout', ["uses" => "Auth\LoginController@logout", "as" => "logout"]);

	Route::group(['prefix' => 'ajax'], function () {
		Route::get('product/group/remove/{id}', ["uses" => "ProductGroupController@removeFromGroup"]);

		Route::post('product/graph/{id}', ["uses" => "ProductController@productGraph"]);
		Route::get('product/import/reset-categories', ["uses" => "CategoryController@resetCategories"]);
		Route::post('product/import', ["uses" => "ProductController@importProduct"]);

		Route::get('category/remove/{id}', ["uses" => "CategoryController@remove"]);

	    Route::get('shop/language', ["uses" => "ShopController@toggleLanguage", "as" => "toggle_shop_language"]);
	    Route::get('shop/currency', ["uses" => "ShopController@toggleCurrency", "as" => "toggle_shop_currency"]);
	    Route::get('shop/country', ["uses" => "ShopController@toggleCountry", "as" => "toggle_shop_country"]);

		Route::get('shop/language', ["uses" => "ShopController@toggleLanguage", "as" => "toggle_shop_language"]);
		Route::get('shop/currency', ["uses" => "ShopController@toggleCurrency", "as" => "toggle_shop_currency"]);
		Route::get('shop/country', ["uses" => "ShopController@toggleCountry", "as" => "toggle_shop_country"]);

		Route::get('feature-type/{id}/features', ["uses" => "ProductFeatureController@getFeatures"]);
		Route::get('feature-option/{id}/remove', ["uses" => "ProductFeatureController@removeProductFeatureOption"]);

		Route::get('attributes/{id}/options', ["uses" => "ProductAttributeController@getOptions"]);

		Route::get('tax_group/{id}/tax_rules', ["uses" => "LocaleController@getTaxRules"]);

		Route::post('customers/search', ["uses" => "CustomerController@findCustomer"]);
		Route::get('products/search', ["uses" => "ProductController@findProduct"]);

		Route::post('ibox/account-view/remove-formule', ["uses" => "iBoxController@removeAccountViewFormule"]);

		Route::get('statistics/commodity/{id}', ["uses" => "StatisticController@commodity"]);
		Route::get('statistics/weather/{iso}', ["uses" => "StatisticController@weather"]);

		//Images
		Route::get('imagetypes', ["uses" => "ImageController@imageTypes"]);
		Route::put('updateimagetype/{id}', ["uses" => "ImageController@UpdateImageType"]);
		Route::post('grouppic/{id}/{shops}', ["uses" => "ImageController@UploadGroupPic"]);
		Route::post('tyretypepic/{id}/{shops}', ["uses" => "ImageController@UploadTyreTypePic"]);
		Route::delete('grouppic/{id}/{shops}', ["uses" => "ImageController@DeleteGroupPic"]);
		Route::post('productpic/{id}/{shops}', ["uses" => "ImageController@UploadProductPic"]);
		Route::delete('productpic/{id}/{shops}', ["uses" => "ImageController@DeleteProductPic"]);

		Route::post('brandpic/{brand}/{shops}', ["uses" => "ImageController@UploadBrandPic"]);
		Route::delete('brandpic/{id}', ["uses" => "ImageController@DeleteBrandPic"]);


		Route::get('products/images', ["uses" => "ProductController@ProductsWithImages"]);

		Route::get('productgroups/images/', ["uses" => "ProductGroupController@ProductGroupsWithImages"]);

		Route::get('tyretypes/images/', ["uses" => "ImageController@getGroupImages"]);

		Route::get('brands/images', ["uses" => "BrandController@brandsImages"]);

		Route::get('vehicle/sku-check', ['uses' => 'VehicleController@skuCheck']);
	});

	Route::group(['prefix' => 'snippets'], function () {
		Route::get('bulk_rule_input', ["uses" => "SnippetController@bulkRuleInput"]);
		Route::get('product-feature-option-input', ["uses" => "SnippetController@productFeatureOptionInput"]);
	});
});


Route::get('login', ["uses"	=> "Auth\LoginController@showLoginForm", "as"	=> "login"]);
Route::post('login', 'Auth\LoginController@login');
