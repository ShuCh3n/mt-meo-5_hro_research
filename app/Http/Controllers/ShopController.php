<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Shop;
use App\Payment;
use App\Country;
use App\Currency;
use App\Language;
use App\Paynl;
use App\TaxGroup;

class ShopController extends Controller
{
    public function getShop()
	{
		return array(
			"status" => true,
			"shop" => $this->request->shop->load(['tax_rule' => function($query){
				$query->active();
			}])
		);
	}

    public function shops()
    {
        $shops = Shop::paginate(100);
        return view('dashboard.shops', compact('shops'));
    }

    public function newShop()
    {
        $payments = Payment::all();
        $countries = Country::all();
        $languages = Language::all();
        $currencies = Currency::all();
        $tax_groups = TaxGroup::all();

    	return view('dashboard.new_shop', compact('payments', 'countries', 'languages', 'currencies', 'tax_groups'));
    }

    public function editShop()
    {
    	$shop = Shop::findOrFail($this->request->id);
    	$payments = Payment::all();
    	$countries = Country::all();
        $languages = Language::all();
        $currencies = Currency::all();
        $tax_groups = TaxGroup::all();

    	return view('dashboard.edit_shop', compact('shop', 'payments', 'countries', 'currencies', 'languages', 'tax_groups'));
    }

    public function actionEditShop()
    {
        $shop = Shop::findOrFail($this->request->id);
        $shop->setShop($this->request);

        return redirect()->route('edit_shop', $shop->id)->with('success', trans('success.edited')); 
    }

    public function actionNewShop()
    {
        $shop = new Shop;
        $shop = $shop->setShop($this->request);

        return redirect()->route('shops'); 
    }
}
