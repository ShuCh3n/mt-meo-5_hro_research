@extends('dashboard.layout.main')

@section('content')

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-6">
            <h2>{{ trans('pages.inquiry_detail_header') }}</h2>
            <ol class="breadcrumb">
                <li>
                    {{ trans('menu.home') }}
                </li>
                <li>
                    {{ trans('menu.inquiries') }}
                </li>
                <li class="active">
                    <strong>{{ trans('page.inquiry') }}</strong>
                </li>
            </ol>
        </div>

        <div class="col-lg-6 text-right mt30">
            <a class="btn btn-primary pull-right" href="{{ route('new_blog') }}"><i class="fa fa-plus-circle" aria-hidden="true"></i> @lang('blog.new_blog')</a>
        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight ecommerce">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-content">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>
                                        @lang('blog.title')
                                    </th>
                                    <th>
                                        @lang('blog.category')
                                    </th>
                                    <th>
                                        @lang('blog.shop')
                                    </th>
                                    <th class="text-right">
                                        @lang('blog.action')
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($blogs as $blog)
                                    <tr>
                                        <td>
                                            {{ $blog->info()->first()->title }}
                                        </td>
                                        <td>
                                           {{ $blog->blog_category->info()->first()->name }}
                                        </td>
                                        <td>
                                            <a href="{{ route('blog', $blog->id) }}">{{ $blog->shop->name }}</a>
                                        </td>
                                        <td class="text-right">
                                            <div class="btn-group">
                                                <a href="{{ route('edit_blog_tags', $blog->id) }}" class="btn-white btn btn-sm"><i class="fa fa-tags"></i></a>
                                                <a href="{{ route('blog', $blog->id) }}" class="btn-white btn btn-sm"><i class="fa fa-eye"></i></a>
                                                <a href="{{ route('new_blog_article', $blog->id )}}" class="btn-white btn btn-sm"><i class="fa fa-plus-circle"></i></a>
                                                <a href="{{ route('edit_blog', $blog->id) }}" class="btn-white btn btn-sm"><i class="fa fa-pencil"></i></a>
                                                <a href="{{ route('delete_blog', $blog->id) }}" class="btn-white btn btn-sm"><i class="fa fa-trash-o"></i></a>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
