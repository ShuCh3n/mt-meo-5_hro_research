<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
	public function product_infos()
    {
    	return $this->hasMany(ProductInfo::class);
    }

    public function product_info($product_id)
    {
    	return $this->product_infos()->where('product_id', $product_id)->first();
    }

    public static function getLanguage($language)
    {
    	return Language::where('name', $language)->orWhere('iso', $language)->first();
    }
}
