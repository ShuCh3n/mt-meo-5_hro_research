@extends('dashboard.layout.main')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-6">
            <h2>{{ trans('pages.inquiry_detail_header') }}</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('home') }}">{{ trans('menu.home') }}</a>
                </li>
                <li>
                    <a href="">{{ trans('menu.inquiries') }}</a>
                </li>
                <li class="active">
                    <strong>{{ trans('page.inquiry') }}</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-6">
            <a class="btn btn-outline btn-primary pull-right mt30" href="{{ route('new_order') }}"><i class="fa fa-plus-circle" aria-hidden="true"></i> New order</a>
        </div>
    </div>

	<div class="wrapper wrapper-content animated fadeInRight ecommerce">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-content">

                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Reference</th>
                                <th>Customer</th>
                                <th>Total</th>
                                <th>Payment</th>
                                <th>Status</th>
                                <th>Delivery</th>
                                <th>Date</th>
                                <th class="text-right">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($orders as $order)
                                    <tr>
                                        <td>
                                          {{ $order->id }}
                                        </td>
                                        <td>
                                           {{ $order->reference }}
                                        </td>
                                        <td>
                                          {{ $order->customer->firstname . ' ' . $order->customer->lastname }}
                                        </td>
                                        <td>
                                          {{ price($order->total_price + $order->tax_price) }}
                                        </td>
                                        <td>
                                          {{ $order->payment->name }}
                                        </td>
                                        <td>
                                          {{ $order->order_status->name }}
                                        </td>
                                        <td>
                                            <img src="/images/flags/16/{{ $order->country->icon }}.png" /> {{ $order->country->iso_code_3 }}
                                        </td>
                                        <td>
                                            @if(carbon()->now()->diffInDays($order->created_at) > 0)
                                                <b>{{ $order->created_at->format('H:i') }}</b>
                                                {{ $order->created_at->format('d M Y') }}
                                            @else
                                                {{ str_replace('before', 'ago', $order->created_at->diffForHumans(carbon())) }}
                                            @endif
                                        </td>
                                        <td class="text-right">
                                            <a class="btn-white btn btn-xs" href="{{ route('view_order', $order->id) }}">View</a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop