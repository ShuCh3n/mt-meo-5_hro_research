<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Platform extends Model
{
    protected $fillable = ['name', 'code', 'settings'];

    public function products()
    {
    	return $this->belongsToMany(Product::class);
    }

    public function platform_product_prices()
    {
    	return $this->hasMany(PlatformProductPrice::class);
    }

    public function platform_product_price()
    {
    	return $this->hasOne(PlatformProductPrice::class);
    }

    public static function getPlatform($platform)
    {
    	$result = null;

    	if(is_numeric($platform))
    	{
    		$result = self::find($platform);
    	}

    	if(is_string($platform))
    	{
    		$result = self::where('name', $platform)->first();
    	}

    	return $result;
    }

    public function settings()
    {
        return json_decode($this->settings);
    }
}
