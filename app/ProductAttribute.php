<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductAttribute extends Model
{
	public function product_attribute_type()
	{
		return $this->belongsTo(ProductAttributeType::class);
	}

    public function options()
    {
    	return $this->hasMany(ProductAttributeOption::class);
    }

    public function infos()
    {
    	return $this->hasMany(ProductAttributeLanguage::class);
    }
}
