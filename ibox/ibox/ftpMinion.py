import ftplib
import socket
import time

class FTPMinion():
	def __init__(self):
		self.retryCount = 10
		self.retrySleep = 5

	def sync_server(self):
		self.ftp = ftplib.FTP('carbon.ttilabs.nl')

		for x in range(self.retryCount):
			try:
				self.ftp.login('cms', 'b7699127530525e653344ebbc3b0adae3c3cb855')
				self.ftp.cwd("ExportSessies")
				return self
				
			except ftplib.all_errors:
				print('retry connecting...')
				time.sleep(self.retrySleep)
			else:
				break

		return None

	def tti_server(self):
		self.ftp = ftplib.FTP('ftp.tti.nl')

		for x in range(self.retryCount):
			try:
				self.ftp.login('klei', '7KL!$3()')

				return self
			except ftplib.all_errors:
				print('retry connecting...')
				time.sleep(self.retrySleep)
			else:
				break

		return None

	def store(self, filename):
		for x in range(self.retryCount):
			try:
				self.ftp.storbinary('STOR shubox-data/' + filename, open('export/' + filename, 'rb'))
				return self
			except ftplib.all_errors:
				print('retry uploading ' + filename + '...')
				time.sleep(self.retrySleep)
			else:
				break
		
	def getFile(self, filename):
		for x in range(self.retryCount):
			try:
				self.ftp.retrbinary("RETR " + filename ,open('files/' + filename, 'wb').write)
				return self
			except ftplib.all_errors:
				print('retry downloading ' + filename + '...')
				time.sleep(self.retrySleep)
			else:
				break

	def close(self):
		self.ftp.quit()
