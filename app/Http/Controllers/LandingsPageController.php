<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class LandingsPageController extends Controller
{
    public function webwinkelsnel()
    {
    	return view('landing_pages.webwinkelsnel.page');
    }
}
