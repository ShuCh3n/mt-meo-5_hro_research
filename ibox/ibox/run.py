import minions
import sys
import ThirdPartySync as thridparty
import AccountViewSync as ac
import ttiMinion as tti
import time
import datetime

start = datetime.datetime.now()

# threads = [
# 	minions.Tyre24(),
# 	minions.O7zr(),
# 	minions.Allopneus(),
# 	minions.Boxenstopp(),
# 	minions.Deldo(),
# 	minions.Hanse(),
# 	minions.Hohl(),
# 	minions.Interpneus(),
# 	minions.Krieg(),
# 	minions.Krupp(),
# 	minions.Wolf(),
# 	minions.Seng(),
# 	minions.ShTyre(),
# 	minions.Straub(),
# 	minions.TyreDistribution(),
# 	minions.MuellerHammelburg()
# ]

# while any(thread.is_alive() for thread in threads):
# 	time.sleep(1)

# thridparty.ThirdPartyMinion()
ac.AccountView()
# tti.TTI()

print(str(datetime.datetime.now() - start))
