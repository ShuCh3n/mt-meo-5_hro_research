<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>CMS | Login | Always do your best. What you plant now, you will harvest later.</title>

    <link href="/css/zeus.css" rel="stylesheet">

</head>

<body class="gray-bg">

    <div class="middle-box text-center loginscreen animated fadeInDown">
        <div>
            <div>

                <h1 class="logo-name">ZEUS</h1>

            </div>
            <h3>Welcome to Zeus</h3>
            <form class="m-t" role="form" method="post">
                <div class="form-group">
                    <input type="email" class="form-control" placeholder="Email" name="email" required="">
                </div>
                <div class="form-group">
                    <input type="password" class="form-control" placeholder="Password" name="password" required="">
                </div>
                {{ csrf_field() }}
                <button type="submit" class="btn btn-primary block full-width m-b">Login</button>

                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
        
                <a href="#"><small>Forgot password?</small></a>
            </form>
        </div>
    </div>
</body>

</html>
