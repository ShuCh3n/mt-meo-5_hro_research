<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RewardProgramOrder extends Model
{
    protected $fillable = ['order_id', 'company', 'iban', 'total_price'];

    public function order()
    {
    	return $this->belongsTo(Order::class);
    }

    public function reward_program()
    {
    	return $this->belongsTo(RewardProgram::class);
    }

    public function lines()
    {
    	return $this->hasMany(RewardProgramOrderLine::class);
    }
}
