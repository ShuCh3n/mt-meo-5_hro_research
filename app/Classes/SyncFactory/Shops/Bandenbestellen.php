<?php
namespace App\Classes\SyncFactory\Shops;

use App\Classes\SyncFactory\Shops\ShopAbstract;
use App\Language;

class Bandenbestellen extends ShopAbstract
{
	public function __construct($shop)
	{
		$this->shop = $shop;
	}

	public function setOrder($order)
	{
		$this->order = $order;
		return $this;
	}

	public function getFormatedOrder()
	{
		$this->getServiceProducts()->getThirdPartyProducts();

		return $this->order;
	}

	public function getServiceProducts()
	{
		$products = array();

		foreach($this->order->lines as $line)
		{
			if($line->product->isFeatureType('Service', Language::getLanguage('Nederlands')))
			{
				$products[] = $line;
			}
		}

		$this->order->service_products = $products;

		return $this;
	}

	public function getThirdPartyProducts()
	{
		$products = array();

		foreach($this->order->lines as $line)
		{
			if($line->third_party_stock)
			{
				$products[] = $line;
			}
		}

		$this->order->third_party_tyres = $products;

		return $this;
	}
}