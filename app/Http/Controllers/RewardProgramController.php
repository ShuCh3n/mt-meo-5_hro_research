<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Shop;
use App\RewardProgram;
use App\RewardProgramOrder;

class RewardProgramController extends Controller
{
    public function programs()
    {
    	$reward_programs = RewardProgram::all();

    	return view('dashboard.reward_programs', compact('reward_programs'));
    }

    public function newProgram()
    {
    	$shops = Shop::all();
    	return view('dashboard.new_reward_program', compact('shops'));
    }

    public function editProgram()
    {
        $program = RewardProgram::findOrFail($this->request->id);
        $shops = Shop::all();

        return view('dashboard.edit_reward_program', compact('shops', 'program'));
    }

    public function createProgram()
    {
    	$program = new RewardProgram;
    	$program->shop_id = $this->request->shop;
    	$program->active = $this->request->active;
    	$program->save();

    	return redirect()->route('reward_programs');
    }

    public function modifyProgram()
    {
        $program = RewardProgram::findOrFail($this->request->id)->update([
            'shop_id'   => $this->request->shop,
            'active'    => $this->request->active
        ]);

        return redirect()->route('reward_programs');
    }

    public function programOrders()
    {
    	$reward_program = RewardProgram::with(['orders' => function($query){
            $query->latest();
        }])->findOrFail($this->request->id);

    	return view('dashboard.reward_program_orders', compact('reward_program'));
    }

    public function viewProgramOrder()
    {
        $program_order = RewardProgramOrder::findOrFail($this->request->id);

        return view('dashboard.view_program_order', compact('program_order'));
    }
}
