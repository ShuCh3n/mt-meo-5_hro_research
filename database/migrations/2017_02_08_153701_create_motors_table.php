<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMotorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicle_brands', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
        });

        Schema::create('vehicle_years', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('year');
            $table->timestamps();
        });

        Schema::create('vehicle_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
        });

        Schema::create('vehicle_category_infos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('vehicle_category_id')->unsigned();
            $table->integer('language_id')->unsigned();
            $table->string('name');
            $table->timestamps();

            $table->foreign('vehicle_category_id')
                ->references('id')
                ->on('vehicle_categories')
                ->onDelete('cascade');

            $table->foreign('language_id')
                ->references('id')
                ->on('languages')
                ->onDelete('cascade');
        });

        Schema::create('motors', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('vehicle_brand_id')->nullable()->unsigned();
            $table->integer('vehicle_category_id')->unsigned()->nullable();
            $table->string('code')->unique();
            $table->string('image')->nullable();
            $table->string('name');
            $table->string('model');
            $table->string('type');
            $table->integer('cc');
            $table->boolean('different_size');
    
            $table->timestamps();

            $table->foreign('vehicle_brand_id')
                ->references('id')
                ->on('vehicle_brands');

            $table->foreign('vehicle_category_id')
                ->references('id')
                ->on('vehicle_categories')
                ->onDelete('cascade');
        });

        Schema::create('motor_vehicle_year', function (Blueprint $table) {
            $table->integer('motor_id')->unsigned();
            $table->integer('vehicle_year_id')->unsigned();

            $table->integer('tyre_front_width');
            $table->integer('tyre_front_height');
            $table->integer('tyre_front_inch');
            $table->integer('tyre_rear_width');
            $table->integer('tyre_rear_height');
            $table->integer('tyre_rear_inch');
            
            $table->foreign('motor_id')
                ->references('id')
                ->on('motors')
                ->onDelete('cascade');

            $table->foreign('vehicle_year_id')
                ->references('id')
                ->on('vehicle_years')
                ->onDelete('cascade');

            $table->primary(['motor_id', 'vehicle_year_id']);
        });

        Schema::create('category_motor_product_vehicle_year', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('category_id')->unsigned();
            $table->integer('motor_id')->unsigned();
            $table->integer('product_id')->unsigned();
            $table->integer('vehicle_year_id')->unsigned();
            $table->integer('month')->nullable();
            $table->string('sku')->nullable();
            $table->integer('min_qty')->nullable();
            $table->timestamps();

            $table->foreign('category_id')
                ->references('id')
                ->on('categories')
                ->onDelete('cascade');

            $table->foreign('motor_id')
                ->references('id')
                ->on('motors')
                ->onDelete('cascade');

            $table->foreign('product_id')
                ->references('id')
                ->on('products')
                ->onDelete('cascade');

            $table->foreign('vehicle_year_id')
                ->references('id')
                ->on('vehicle_years')
                ->onDelete('cascade');

            //$table->primary(['category_id', 'motor_id', 'product_id', 'vehicle_year_id', 'sku'], 'category_motor_product_vehicle_year_sku');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('category_motor_product_vehicle_year');
        Schema::dropIfExists('motor_vehicle_year');
        Schema::dropIfExists('motors');
        Schema::dropIfExists('vehicle_category_infos');
        Schema::dropIfExists('vehicle_categories');
        Schema::dropIfExists('vehicle_years');
        Schema::dropIfExists('vehicle_brands');
    }
}
