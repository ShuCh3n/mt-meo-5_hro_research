<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Shipping;

class ShippingController extends Controller
{
    public function shippings()
    {
    	$shippings = Shipping::all();

    	return view('dashboard.shippings', compact('shippings'));
    }

    public function newShipping()
    {
    	return view('dashboard.new_shipping');
    }
}
