@extends('dashboard.layout.main')

@section('content')
	<div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-6">
            <h2>{{ trans('pages.inquiry_detail_header') }}</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('home') }}">{{ trans('menu.home') }}</a>
                </li>
                <li>
                    <a href="">{{ trans('menu.inquiries') }}</a>
                </li>
                <li class="active">
                    <strong>{{ trans('page.inquiry') }}</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-6">

        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight ecommerce">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-content">

                        <form action="{{ route('product_bulk_edit') }}" method="post">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th>ID</th>
                                    <th>Product Name</th>
                                    <th>SKU (Article Nr.)</th>
                                    <th>EAN</th>
                                    <th>Retail Price</th>
                                    <th>Active</th>
                                    <th class="text-right">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach($products as $product)
                                        <tr>
                                            <td>
                                                @if($product->allimages()->count() > 0)
                                                    <img class="img-responsive img-thumbnail" width="64" src="{{ route('product_image', [$product->allimages()->first()->id, "thumb", "filename"]) }}" />
                                                @else
                                                    <img class="img-responsive img-thumbnail" width="64" src="{{ route('product_image', ["no_image", "small", "filename"]) }}" />
                                                @endif
                                            </td>
                                            <td>
                                            	{{ $product->id }}
                                            </td>
                                            <td>
                                            	{{ $product->info->name }}
                                            </td>
                                            <td>
                                            	{{ $product->sku }}
                                            </td>
                                            <td>
                                            	{{ $product->ean }}
                                            </td>
                                            <td>
                                            	{{ price($product->product_price->retail_price ?? 0) }}
                                            </td>
                                            <td>
                                            	{!! ($product->active)? '<i class="fa fa-check text-navy" aria-hidden="true"></i>' : '<i class="fa fa-times text-danger" aria-hidden="true"></i>' !!}
                                            </td>
                                            <td class="text-right">
                                            	<div class="btn-group">
                                                    <a class="btn-white btn btn-xs" href="{{ route('view_product', $product->id) }}">View</a>
                                                    <a class="btn-white btn btn-xs" href="{{ route('edit_product', $product->id) }}">Edit</a>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>    
                        </form>

                        <div class="text-center">
                            {{ $products->links() }}
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@stop