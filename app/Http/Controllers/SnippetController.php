<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class SnippetController extends Controller
{
    public function bulkRuleInput()
    {
    	$random_string = str_random(10);
    	return view('dashboard.snippets.bulk_rule_input', compact('random_string'));
    }

    public function productFeatureOptionInput()
    {
    	return view('dashboard.snippets.product_feature_option_input');
    }
}
