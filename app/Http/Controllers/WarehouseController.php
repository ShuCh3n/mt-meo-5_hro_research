<?php

namespace App\Http\Controllers;

use App\Warehouse;
use App\Http\Requests;

class WarehouseController extends Controller
{
    public function index()
    {
    	$warehouses = Warehouse::all();

    	return view('dashboard.warehouses', compact('warehouses'));
    }

    public function newWarehouse()
    {
    	return view('dashboard.new_warehouse');
    }

    public function actionNewWarehouse()
    {
    	$warehouse = new Warehouse;
    	$warehouse->name = $this->request->name;
    	$warehouse->deltime = $this->request->del_time;
    	$warehouse->save();

    	return redirect()->route('warehouses');
    }

    public function editWarehouse()
    {
    	$warehouse = Warehouse::findOrFail($this->request->id);

    	return view('dashboard.edit_warehouse', compact('warehouse'));
    }

    public function actionEditWarehouse()
    {
    	$warehouse = Warehouse::findOrFail($this->request->id);
    	$warehouse->name = $this->request->name;
    	$warehouse->save();

    	return redirect()->route('warehouses');
    }
}
