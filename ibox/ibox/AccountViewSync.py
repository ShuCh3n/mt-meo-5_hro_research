from datetime import *

import databaseMinion
import pandas as pd
import sys
import datetime
import Platform as pf
import ftpMinion as ftp_minion
import json
import decimal

sys.path.append('..')
import zeus_scheme
sys.path.remove('..')

class AccountView():
	def __init__(self):
		self.database_minion = databaseMinion.DatabaseMinion()
		self.platforms = []
		self.products = self.database_minion.session.query(zeus_scheme.Product).all()
		self.header = []
		self.results = []

		self.setHeader()
		self.process()

		df = pd.DataFrame(self.results, columns = self.header)
		df.to_csv('export/av.csv', index=False, sep=';')

		# self.storeFTP()

	def process(self):
		for product in self.products:
			result = []
			add = False
			product_name = 'Product'
			purchasing_price = 0

			if len(product.infos) > 0:
				product_name = product.infos[0].name

			if product.product_prices.first():
				purchasing_price = product.product_prices.first().purchasing_price

			result.append(product.sku)
			result.append(product_name)
			result.append(str(purchasing_price).replace(".", ","))

			for platform in self.platforms:
				platform_name = next(iter(platform.keys()))
				platform_id = platform[platform_name]['platform'][0].id

				platform_price = self.database_minion.session.query(zeus_scheme.PlatformProductPrice).filter_by(platform_id = platform_id, product_id = product.id).order_by(zeus_scheme.PlatformProductPrice.created_at.desc()).first()

				if platform_price:
					add = True
					result.append(str(platform_price.price).replace(".", ","))

					for setting in platform[platform_name]['settings']:
						result.append(self.calculateSellingPrice(purchasing_price, platform_price, setting))

				else:
					result.append('-')

			if add:
				self.results.append(result)

	def setHeader(self):
		self.header.append('art_code')
		self.header.append('art_desc1')
		self.header.append('gip')

		settings = self.database_minion.session.query(zeus_scheme.AccountViewSetting).all();

		for setting in settings:
			if not any(setting.platform.name.lower() in s for s in self.platforms):
				platform = {}
				platform[setting.platform.name.lower()] = {}
				platform[setting.platform.name.lower()]['settings'] = []
				platform[setting.platform.name.lower()]['platform'] = [setting.platform]

				self.platforms.append(platform)

		for platform in self.platforms:
			platform_name = next(iter(platform.keys()))
			platform_id = platform[platform_name]['platform'][0].id

			platform_settings = self.database_minion.session.query(zeus_scheme.AccountViewSetting).filter(zeus_scheme.AccountViewSetting.platform_id == platform_id).all();

			self.header.append('t_' + platform_name)

			for platform_setting in platform_settings:
				platform[platform_name]['settings'].append(platform_setting)
				self.header.append('px_sell_' + platform_setting.country.iso_code_2.lower())

	def calculateSellingPrice(self, purchasing_price, platform_price, setting):
		if purchasing_price > 0:
			setting = json.loads(setting.settings)
			threshold = decimal.Decimal(setting['threshold'])
			amount = decimal.Decimal(setting['amount'])
			percentage = decimal.Decimal(setting['percentage'])

			threshold_percentage = (100 - percentage) / 100
			calculate_percentage = (100 + percentage) / 100

			calculated_price = (platform_price.price - threshold) * threshold_percentage

			if calculated_price > purchasing_price:
				return str(round(platform_price.price + amount, 2)).replace(".", ",")

			return str(round(((purchasing_price + threshold) * calculate_percentage) , 2)).replace(".", ",") 

		return None

	def storeFTP(self):
		ftp = ftp_minion.FTPMinion()
		ftp.tti_server()
		ftp.store('av.csv')
		ftp.close()
