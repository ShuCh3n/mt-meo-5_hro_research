<div class="ibox">
    <div class="ibox-title">
        <h5>{{ $heading ?? 'Default Heading' }}</h5>
    </div>

    <div class="ibox-content">
        <div class="mb20"></div>

        <div class="row m-b-lg">
            <div class="col-sm-12">
                <flot-graph 
                stats="{{ $stats ?? "[]" }}" 
                start="{{ (request()->from)? carbon()->createFromFormat('d M Y', request()->from) : carbon()->startOfMonth()  }}" 
                end="{{ (request()->from)? carbon()->createFromFormat('d M Y', request()->until) : carbon()->endOfMonth()  }}" 
                ></flot-graph>
            </div>
        </div>
    </div>
</div>



                   