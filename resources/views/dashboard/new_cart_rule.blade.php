@extends('dashboard.layout.main')

@section('stylesheet')
    <link href="/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="/css/plugins/touchspin/jquery.bootstrap-touchspin.min.css" rel="stylesheet">
    <link href="/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
    <style>
        .select2-result-repository__avatar {
            float: left;
            margin-right: 10px;
            width: 100px;
        }
        .select2-result-repository__title {
            color: black;
            font-weight: bold;
            line-height: 1.1;
            margin-bottom: 4px;
            word-wrap: break-word;
        }
        .select2-result-repository__forks, .select2-result-repository__stargazers, .select2-result-repository__watchers {
            color: #ddd;
            display: inline-block;
            font-size: 11px;
            margin-right: 10px;
        }
    </style>
@stop

@section('javascript')
    <!-- iCheck -->
    <script src="/js/plugins/iCheck/icheck.min.js"></script>

    <!-- TouchSpin -->
    <script src="/js/plugins/touchspin/jquery.bootstrap-touchspin.min.js"></script>

    <!-- Data picker -->
   <script src="/js/plugins/datapicker/bootstrap-datepicker.js"></script>

    <script>
        $(function(){
            $('#data_1 .input-group.date').datepicker({
                todayBtn: "linked",
                keyboardNavigation: false,
                forceParse: false,
                calendarWeeks: true,
                autoclose: true
            });

            $('#generate_voucher_code').click(function(){
                $('#voucher_code').val(randomString(16).toUpperCase());
            });

            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });

            $(".input-price").TouchSpin({
                max: 1000000000,
                min: 0,
                step: 1,
                decimals: 2,
                boostat: 5,
                maxboostedstep: 10,
                prefix: '€',
                buttondown_class: 'btn btn-white',
                buttonup_class: 'btn btn-white'
            });

            $(".usage-limit-input").TouchSpin({
                max: 1000000000,
                min: 0,
                step: 1,
                decimals: 0,
                boostat: 5,
                maxboostedstep: 10,
                buttondown_class: 'btn btn-white',
                buttonup_class: 'btn btn-white'
            });

            $(".input-percentage").TouchSpin({
                max: 1000000000,
                min: 0,
                step: 1,
                decimals: 2,
                boostat: 5,
                maxboostedstep: 10,
                postfix: '%',
                buttondown_class: 'btn btn-white',
                buttonup_class: 'btn btn-white'
            });

            $(".find-product").select2({
                ajax: {
                    url: "/ajax/products/search",
                    headers: { 'X-CSRF-Token': csrf_token },
                    dataType: 'json',
                    type: "post",
                    delay: 250,
                    data: function (params) {
                        return {
                            q: params.term, // search term
                            shop: $("#pick-shop").val(),
                            page: params.page
                        };
                    },
                    processResults: function (data, params) {
                        params.page = params.page || 1;
                        return {
                            results: data.items,
                            pagination: {
                                more: (params.page * 30) < data.total_count
                            }
                        };
                    },
                        cache: true
                    },
                    escapeMarkup: function (markup) { return markup; },
                    minimumInputLength: 1,
                    templateResult: formatProduct,
                    templateSelection: formatProductSelection
            });

            $(".find-customer").select2({
                ajax: {
                    url: "/ajax/customers/search",
                    headers: { 'X-CSRF-Token': csrf_token },
                    dataType: 'json',
                    type: "post",
                    delay: 250,
                    data: function (params) {
                        return {
                            q: params.term, // search term
                            shop: $("#pick-shop").val(),
                            page: params.page
                        };
                    },
                    processResults: function (data, params) {
                        params.page = params.page || 1;
                        return {
                            results: data.items,
                            pagination: {
                                more: (params.page * 30) < data.total_count
                            }
                        };
                    },
                        cache: true
                    },
                    escapeMarkup: function (markup) { return markup; },
                    minimumInputLength: 1,
                    templateResult: formatCustomer,
                    templateSelection: formatCustomerSelection
            });

            if($('.check-amount').attr('checked')) {
                $('.input-price-amount').show();
                $('.input-percentage-amount').hide();
            }

            if($('.check-percentage').attr('checked')) {
                $('.input-percentage-amount').show();
                $('.input-price-amount').hide();
            }

            $('.check-amount').on('ifChecked', function(){
                $('.input-price-amount').show();
                $('.input-percentage-amount').hide();
            });

            $('.check-percentage').on('ifChecked', function(){
                $('.input-percentage-amount').show();
                $('.input-price-amount').hide();
            });

            if($('.is_giftcard').attr('checked')) {
                $('.usage_limit').hide();
            }

            if($('.is_not_giftcard').attr('checked')) {
                $('.usage_limit').show();
            }
            
            $('.is_giftcard').on('ifChecked', function(){
                $('.usage_limit').hide();
            });

            $('.is_not_giftcard').on('ifChecked', function(){
                $('.usage_limit').show();
            });
        });


        function formatProduct (product) {
            if (product.loading) return product.text;

            var markup = "<div class='select2-result-repository clearfix'>";

            if(product.images.length > 0)
            {
                markup += "<div class='select2-result-repository__avatar'><img src='/product/images/" + product.images[0].id + "/small/filename' width='100' /></div>";
            }
            else
            {
                markup += "<div class='select2-result-repository__avatar'><img src='/product/images/no_image/small/filename' width='100' /></div>";
            }

            markup += "<div class='select2-result-repository__meta'><div class='select2-result-repository__title'>" + product.info.name + "</div>";

            if (product.info.short_description) {
                markup += "<div class='select2-result-repository__description'>" + product.info.short_description + "</div>";
            }

            markup += "<div class='select2-result-repository__statistics'>" +
            "<div class='select2-result-repository__forks'><i class='fa fa-hashtag'></i> " + product.id + "</div>" +
            "<div class='select2-result-repository__stargazers'><i class='fa fa-qrcode'></i> " + product.ean + "</div>" +
            "<div class='select2-result-repository__watchers'><i class='fa fa-shopping-bag'></i> " + product.sku + "</div>" +
            "</div>" +
            "</div></div>";

            return markup;
        }

        function formatProductSelection (product) {
            modal_product = product;

            if(product.info)
            {
                $("#modal-list-price").val(product.list_price);
                return product.info.name;
            }

            return product;         
        }

        function formatCustomer(customer)
        {
            if (customer.loading) return customer.text;

            var markup = "<div class='select2-result-repository clearfix'><div class='select2-result-repository__meta'>" + customer.id + " " + customer.firstname + " " + customer.lastname + "</div></div>";

            return markup;
        }

        function formatCustomerSelection(customer)
        {
            if(customer.firstname)
            {
                $(".customer_addresses").html("");

                for(i in customer.customer_addresses)
                {
                    $(".customer_addresses").append("<option value=" + customer.customer_addresses[i].id + ">" + customer.customer_addresses[i].alias + "</option>");
                }

                $(".customer_addresses").select2();

                return customer.id + " " + customer.firstname + " " + customer.lastname;
            }
            
            return customer;
        }
    </script>
@stop

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>{{ trans('pages.inquiry_detail_header') }}</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('home') }}">{{ trans('menu.home') }}</a>
                </li>
                <li>
                    <a href="">{{ trans('menu.inquiries') }}</a>
                </li>
                <li class="active">
                    <strong>{{ trans('page.inquiry') }}</strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">
        {!! session()->has('success')? '<div class="alert alert-success alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>' . session('success') . '</div>' : '' !!}

        <form id="profileForm" method="post" class="form-horizontal">
            <div class="row">
                <div class="col-md-8">
                    <div class="ibox">

                        <div class="ibox-title">
                            <h5>New Cart Rule</h5>
                        </div>

                        <div class="ibox-content">
                            <div class="mb20"></div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Active</label>
                                        <div class="col-md-5">
                                            <label><input type="radio" class="form-control i-checks" name="active" value="1" checked> Yes</label>
                                        </div>
                                        <div class="col-md-5">
                                            <label><input type="radio" class="form-control i-checks" name="active" value="0"> No</label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Name</label>
                                        <div class="col-md-10"><input type="text" class="form-control" name="name" placeholder="Cart Rule Name"></div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Giftcard</label>
                                        <div class="col-md-5">
                                            <label><input type="radio" class="form-control i-checks is_giftcard" name="giftcard" value="1" checked> Yes</label>
                                        </div>
                                        <div class="col-md-5">
                                            <label><input type="radio" class="form-control i-checks is_not_giftcard" name="giftcard" value="0"> No</label>
                                        </div>
                                    </div>

                                    <div class="form-group usage_limit">
                                        <label class="col-md-2 control-label">Usage Limit</label>
                                        <div class="col-md-10">
                                            <input type="text" class="usage-limit-input" name="usage_limit" placeholder="Max Usage" value="">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Amount / Percentage</label>
                                        <div class="col-md-5">
                                            <label><input type="radio" class="form-control i-checks check-amount" name="amount_or_percentage" value="1" checked> Amount</label>
                                        </div>
                                        <div class="col-md-5">
                                            <label><input type="radio" class="form-control i-checks check-percentage" name="amount_or_percentage" value="0"> Percentage</label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Amount</label>
                                        <div class="col-md-10 input-price-amount">
                                            <input type="text" class="input-price" name="price_amount" placeholder="Price Amount" value="">
                                        </div>
                                        <div class="col-md-10 input-percentage-amount">
                                            <input type="text" class="input-percentage" name="percentage_amount" placeholder="Percentage Amount" value="">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Voucher Code</label>
                                        <div class="col-md-10">
                                            <div class="input-group">
                                                <input type="text" class="form-control text-uppercase" name="voucher_code" id="voucher_code" value="{{ old('password') }}">
                                                <span class="input-group-btn"> 
                                                    <button class="btn btn-primary" type="button" id="generate_voucher_code">Generate</button> 
                                                </span>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="form-group" id="data_1">
                                        <label class="col-md-2 control-label">Valid Date</label>
                                        <div class="col-md-4">
                                            <div class="input-group date">
                                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" name="valid_from" class="form-control" placeholder="Valid from">
                                            </div>
                                        </div>
                                        <div class="col-md-2 text-center">
                                            Until
                                        </div>
                                        <div class="col-md-4">
                                            <div class="input-group date">
                                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" name="valid_until" class="form-control" placeholder="Valid until">
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="ibox">

                        <div class="ibox-title">
                            <h5>Cart Rule</h5>
                        </div>

                        <div class="ibox-content">
                            <div class="mb20"></div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Free Shipping</label>
                                        <div class="col-md-5">
                                            <label><input type="radio" class="form-control i-checks" name="free_shipping" value="1" checked> Yes</label>
                                        </div>
                                        <div class="col-md-5">
                                            <label><input type="radio" class="form-control i-checks" name="free_shipping" value="0"> No</label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Country</label>
                                        <div class="col-md-10">
                                            <select class="form-control countries-select" multiple="" name="countries[]">
                                                @foreach($countries as $country)
                                                    <option value="{{ $country->id }}" icon="{{ $country->icon }}">{{ $country->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Customer Group</label>
                                        <div class="col-md-10">
                                            <select class="form-control select2" multiple="" name="customer_groups[]">
                                                @foreach($customer_groups as $group)
                                                    <option value="{{ $group->id }}">{{ $group->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Customer</label>
                                        <div class="col-md-10">
                                            <select class="form-control find-customer" multiple="" name="customers[]"></select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Products</label>
                                        <div class="col-md-10">
                                            <select class="find-product form-control" multiple="" name="products[]"></select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Feature Type</label>
                                        <div class="col-md-10">
                                            <select class="select2 form-control" multiple="" name="feature_types[]">
                                                @foreach($product_feature_types as $type)
                                                    <option value="{{ $type->id }}">{{ $type->multilanguage->info()->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <hr />

                                    @foreach($shops as $shop)
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">{{ $shop->name }}</label>
                                            <div class="col-md-10">
                                                @if($shop->categories->count() > 0)
                                                    <select class="select2 form-control" multiple="multiple" name="categories[]">
                                                        @foreach($shop->categories as $category)
                                                            <option value="{{ $category->id }}">{{ $category->multilanguage->info()->name }}</option>
                                                        @endforeach
                                                    </select>

                                                @else
                                                    <p class="form-control-static">
                                                        <a href="{{ route('new_category', ["shop_id" => $shop->id]) }}">Create first category</a>
                                                    </p>
                                                @endif
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>

                <div class="col-md-4">
                    <div class="ibox">
                        <div class="ibox-title">
                            <h5>Action</h5>
                        </div>

                        <div class="ibox-content">
                            {{ csrf_field() }}
                            <button class="btn btn-block btn-outline btn-success" type="submit"><i class="fa fa-floppy-o"></i> Save</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>

        <div class="mb30"></div>
    </div>
@stop