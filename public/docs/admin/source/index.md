---
title: API Reference

language_tabs:
- bash
- javascript

includes:

search: true

toc_footers:
- <a href='http://github.com/mpociot/documentarian'>Documentation Powered by Documentarian</a>
---
<!-- START_INFO -->
# Info

Welcome to the generated API reference.
[Get Postman Collection](http://cms.dev/docs/admin/collection.json)
<!-- END_INFO -->

#Administration endpoints

For fetching data present in accountview administrations, the **AdminToken** header is required.
<!-- START_17ae86bec21f0361d1ecda668e4f68ad -->
## admin/v1/product/{sku}

> Example request:

```bash
curl -X GET "http://cms.dev/admin/v1/product/{sku}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://cms.dev/admin/v1/product/{sku}",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
null
```

### HTTP Request
`GET admin/v1/product/{sku}`

`HEAD admin/v1/product/{sku}`


<!-- END_17ae86bec21f0361d1ecda668e4f68ad -->

<!-- START_09e4023ac1fa516966aa08517d4373a1 -->
## admin/v1/products

> Example request:

```bash
curl -X GET "http://cms.dev/admin/v1/products" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://cms.dev/admin/v1/products",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "total": 83838,
    "per_page": 15,
    "current_page": 1,
    "last_page": 5590,
    "next_page_url": "http:\/\/localhostadmin\/v1\/products?page=2",
    "prev_page_url": null,
    "from": 1,
    "to": 15,
    "data": [
        {
            "id": 1,
            "brand_id": null,
            "product_feature_type_id": 1,
            "product_group_id": 135,
            "tax_rule_id": null,
            "supplier_sku": null,
            "ean": null,
            "upc": "upc",
            "sku": "BSMONTLM",
            "minimum_qty": 1,
            "stock_management": "1",
            "active_when_out_of_stock": "1",
            "deny_order_when_out_of_stock": "1",
            "width": "0.00",
            "height": "0.00",
            "depth": "0.00",
            "weight": "0.00",
            "additional_fee_per_item": "0.00",
            "active": "0",
            "imported": "0",
            "msdsFiles": [],
            "productimages": [],
            "motor_product": [],
            "motors": []
        },
        {
            "id": 2,
            "brand_id": null,
            "product_feature_type_id": 1,
            "product_group_id": 135,
            "tax_rule_id": null,
            "supplier_sku": null,
            "ean": null,
            "upc": "upc",
            "sku": "BSMONTST",
            "minimum_qty": 1,
            "stock_management": "1",
            "active_when_out_of_stock": "1",
            "deny_order_when_out_of_stock": "1",
            "width": "0.00",
            "height": "0.00",
            "depth": "0.00",
            "weight": "0.00",
            "additional_fee_per_item": "0.00",
            "active": "0",
            "imported": "0",
            "msdsFiles": [],
            "productimages": [],
            "motor_product": [],
            "motors": []
        },
        {
            "id": 3,
            "brand_id": null,
            "product_feature_type_id": 1,
            "product_group_id": 135,
            "tax_rule_id": null,
            "supplier_sku": null,
            "ean": null,
            "upc": "upc",
            "sku": "BSSTIK",
            "minimum_qty": 1,
            "stock_management": "1",
            "active_when_out_of_stock": "1",
            "deny_order_when_out_of_stock": "1",
            "width": "0.00",
            "height": "0.00",
            "depth": "0.00",
            "weight": "0.00",
            "additional_fee_per_item": "0.00",
            "active": "0",
            "imported": "0",
            "msdsFiles": [],
            "productimages": [],
            "motor_product": [],
            "motors": []
        },
        {
            "id": 4,
            "brand_id": null,
            "product_feature_type_id": 1,
            "product_group_id": 135,
            "tax_rule_id": null,
            "supplier_sku": null,
            "ean": null,
            "upc": "upc",
            "sku": "BSUITL",
            "minimum_qty": 1,
            "stock_management": "1",
            "active_when_out_of_stock": "1",
            "deny_order_when_out_of_stock": "1",
            "width": "0.00",
            "height": "0.00",
            "depth": "0.00",
            "weight": "0.00",
            "additional_fee_per_item": "0.00",
            "active": "0",
            "imported": "0",
            "msdsFiles": [],
            "productimages": [],
            "motor_product": [],
            "motors": []
        },
        {
            "id": 5,
            "brand_id": null,
            "product_feature_type_id": 1,
            "product_group_id": 135,
            "tax_rule_id": null,
            "supplier_sku": null,
            "ean": null,
            "upc": "upc",
            "sku": "BSOPSL",
            "minimum_qty": 1,
            "stock_management": "1",
            "active_when_out_of_stock": "1",
            "deny_order_when_out_of_stock": "1",
            "width": "0.00",
            "height": "0.00",
            "depth": "0.00",
            "weight": "0.00",
            "additional_fee_per_item": "0.00",
            "active": "0",
            "imported": "0",
            "msdsFiles": [],
            "productimages": [],
            "motor_product": [],
            "motors": []
        },
        {
            "id": 6,
            "brand_id": null,
            "product_feature_type_id": null,
            "product_group_id": null,
            "tax_rule_id": null,
            "supplier_sku": null,
            "ean": "12345",
            "upc": null,
            "sku": "test_product",
            "minimum_qty": 1,
            "stock_management": "0",
            "active_when_out_of_stock": "0",
            "deny_order_when_out_of_stock": "1",
            "width": null,
            "height": null,
            "depth": null,
            "weight": null,
            "additional_fee_per_item": null,
            "active": "1",
            "imported": "0",
            "msdsFiles": [],
            "productimages": [],
            "motor_product": [],
            "motors": []
        },
        {
            "id": 7,
            "brand_id": 1,
            "product_feature_type_id": 1,
            "product_group_id": null,
            "tax_rule_id": null,
            "supplier_sku": null,
            "ean": "54321",
            "upc": null,
            "sku": "test_tyre",
            "minimum_qty": 1,
            "stock_management": "0",
            "active_when_out_of_stock": "0",
            "deny_order_when_out_of_stock": "1",
            "width": null,
            "height": null,
            "depth": null,
            "weight": null,
            "additional_fee_per_item": null,
            "active": "1",
            "imported": "0",
            "msdsFiles": [],
            "productimages": [],
            "motor_product": [],
            "motors": []
        },
        {
            "id": 8,
            "brand_id": 2,
            "product_feature_type_id": 1,
            "product_group_id": 1,
            "tax_rule_id": null,
            "supplier_sku": "307739",
            "ean": "4250427408057",
            "upc": "upc",
            "sku": "FAZ307739",
            "minimum_qty": 1,
            "stock_management": "1",
            "active_when_out_of_stock": "1",
            "deny_order_when_out_of_stock": "1",
            "width": "0.00",
            "height": "0.00",
            "depth": "0.00",
            "weight": "8.00",
            "additional_fee_per_item": "0.00",
            "active": "0",
            "imported": "1",
            "msdsFiles": [],
            "productimages": [],
            "motor_product": [],
            "motors": []
        },
        {
            "id": 9,
            "brand_id": 3,
            "product_feature_type_id": 1,
            "product_group_id": 2,
            "tax_rule_id": null,
            "supplier_sku": "3220005494",
            "ean": "6959655424072",
            "upc": "upc",
            "sku": "RVZ3220005494",
            "minimum_qty": 1,
            "stock_management": "1",
            "active_when_out_of_stock": "1",
            "deny_order_when_out_of_stock": "1",
            "width": "0.00",
            "height": "0.00",
            "depth": "0.00",
            "weight": "8.00",
            "additional_fee_per_item": "0.00",
            "active": "0",
            "imported": "1",
            "msdsFiles": [],
            "productimages": [],
            "motor_product": [],
            "motors": []
        },
        {
            "id": 10,
            "brand_id": 4,
            "product_feature_type_id": 1,
            "product_group_id": 3,
            "tax_rule_id": null,
            "supplier_sku": "TQ3915",
            "ean": "6953913193915",
            "upc": "upc",
            "sku": "TQ3915",
            "minimum_qty": 1,
            "stock_management": "1",
            "active_when_out_of_stock": "1",
            "deny_order_when_out_of_stock": "1",
            "width": "0.00",
            "height": "0.00",
            "depth": "0.00",
            "weight": "0.00",
            "additional_fee_per_item": "0.00",
            "active": "0",
            "imported": "1",
            "msdsFiles": [],
            "productimages": [],
            "motor_product": [],
            "motors": []
        },
        {
            "id": 11,
            "brand_id": 5,
            "product_feature_type_id": 1,
            "product_group_id": 4,
            "tax_rule_id": null,
            "supplier_sku": "936568",
            "ean": "3528709365680",
            "upc": "upc",
            "sku": "MIW936568",
            "minimum_qty": 1,
            "stock_management": "1",
            "active_when_out_of_stock": "1",
            "deny_order_when_out_of_stock": "1",
            "width": "0.00",
            "height": "0.00",
            "depth": "0.00",
            "weight": "13.00",
            "additional_fee_per_item": "0.00",
            "active": "0",
            "imported": "1",
            "msdsFiles": [],
            "productimages": [
                {
                    "img": "https:\/\/storage.googleapis.com\/tti-cdn\/cdn\/images\/0a817604-d57c-4ba5-88b5-b1aafbd96ca5.png",
                    "thumb": "https:\/\/storage.googleapis.com\/tti-cdn\/cdn\/images\/0a817604-d57c-4ba5-88b5-b1aafbd96ca5_thumb.png"
                },
                {
                    "img": "https:\/\/storage.googleapis.com\/tti-cdn\/cdn\/images\/84dc8390-0ac6-4474-9185-9dd366706600.png",
                    "thumb": "https:\/\/storage.googleapis.com\/tti-cdn\/cdn\/images\/84dc8390-0ac6-4474-9185-9dd366706600_thumb.png"
                },
                {
                    "img": "https:\/\/storage.googleapis.com\/tti-cdn\/cdn\/images\/3e9646b7-13ea-4c42-b940-caaf1e5b93c9.png",
                    "thumb": "https:\/\/storage.googleapis.com\/tti-cdn\/cdn\/images\/3e9646b7-13ea-4c42-b940-caaf1e5b93c9_thumb.png"
                },
                {
                    "img": "https:\/\/storage.googleapis.com\/tti-cdn\/cdn\/images\/8387ea22-a11f-4405-a4d1-1091831f73de.png",
                    "thumb": "https:\/\/storage.googleapis.com\/tti-cdn\/cdn\/images\/8387ea22-a11f-4405-a4d1-1091831f73de_thumb.png"
                }
            ],
            "motor_product": [],
            "motors": []
        },
        {
            "id": 12,
            "brand_id": 4,
            "product_feature_type_id": 1,
            "product_group_id": 3,
            "tax_rule_id": null,
            "supplier_sku": "200T2031",
            "ean": "6953913190341",
            "upc": "upc",
            "sku": "TQ200T2031",
            "minimum_qty": 1,
            "stock_management": "1",
            "active_when_out_of_stock": "1",
            "deny_order_when_out_of_stock": "1",
            "width": "0.00",
            "height": "0.00",
            "depth": "0.00",
            "weight": "0.00",
            "additional_fee_per_item": "0.00",
            "active": "0",
            "imported": "1",
            "msdsFiles": [],
            "productimages": [],
            "motor_product": [],
            "motors": []
        },
        {
            "id": 13,
            "brand_id": 6,
            "product_feature_type_id": 1,
            "product_group_id": 5,
            "tax_rule_id": null,
            "supplier_sku": "251383",
            "ean": "6945080117030",
            "upc": "upc",
            "sku": "NEZ11703",
            "minimum_qty": 1,
            "stock_management": "1",
            "active_when_out_of_stock": "1",
            "deny_order_when_out_of_stock": "1",
            "width": "0.00",
            "height": "0.00",
            "depth": "0.00",
            "weight": "8.00",
            "additional_fee_per_item": "0.00",
            "active": "0",
            "imported": "1",
            "msdsFiles": [],
            "productimages": [
                {
                    "img": "https:\/\/storage.googleapis.com\/tti-cdn\/cdn\/images\/069edde3-2124-4a82-aa97-bbebd65556f0.png",
                    "thumb": "https:\/\/storage.googleapis.com\/tti-cdn\/cdn\/images\/069edde3-2124-4a82-aa97-bbebd65556f0_thumb.png"
                }
            ],
            "motor_product": [],
            "motors": []
        },
        {
            "id": 14,
            "brand_id": 7,
            "product_feature_type_id": 1,
            "product_group_id": 6,
            "tax_rule_id": null,
            "supplier_sku": "528390",
            "ean": "5452000654465",
            "upc": "upc",
            "sku": "GYZ528390",
            "minimum_qty": 1,
            "stock_management": "1",
            "active_when_out_of_stock": "1",
            "deny_order_when_out_of_stock": "1",
            "width": "0.00",
            "height": "0.00",
            "depth": "0.00",
            "weight": "8.00",
            "additional_fee_per_item": "0.00",
            "active": "0",
            "imported": "1",
            "msdsFiles": [],
            "productimages": [
                {
                    "img": "https:\/\/storage.googleapis.com\/tti-cdn\/cdn\/images\/a15c9111-1c44-4c46-9df3-e452a004f581.png",
                    "thumb": "https:\/\/storage.googleapis.com\/tti-cdn\/cdn\/images\/a15c9111-1c44-4c46-9df3-e452a004f581_thumb.png"
                },
                {
                    "img": "https:\/\/storage.googleapis.com\/tti-cdn\/cdn\/images\/8f25a774-c2e8-4cfd-b342-abd7efebd060.png",
                    "thumb": "https:\/\/storage.googleapis.com\/tti-cdn\/cdn\/images\/8f25a774-c2e8-4cfd-b342-abd7efebd060_thumb.png"
                }
            ],
            "motor_product": [],
            "motors": []
        },
        {
            "id": 15,
            "brand_id": 8,
            "product_feature_type_id": 1,
            "product_group_id": 7,
            "tax_rule_id": null,
            "supplier_sku": "AP20555016HSN5A00",
            "ean": "8714692297878",
            "upc": "upc",
            "sku": "VRW111753",
            "minimum_qty": 1,
            "stock_management": "1",
            "active_when_out_of_stock": "1",
            "deny_order_when_out_of_stock": "1",
            "width": "0.00",
            "height": "0.00",
            "depth": "0.00",
            "weight": "8.00",
            "additional_fee_per_item": "0.00",
            "active": "0",
            "imported": "1",
            "msdsFiles": [],
            "productimages": [
                {
                    "img": "https:\/\/storage.googleapis.com\/tti-cdn\/cdn\/images\/e96ceaeb-6b14-44fa-8fa5-8b4debdcddef.png",
                    "thumb": "https:\/\/storage.googleapis.com\/tti-cdn\/cdn\/images\/e96ceaeb-6b14-44fa-8fa5-8b4debdcddef_thumb.png"
                },
                {
                    "img": "https:\/\/storage.googleapis.com\/tti-cdn\/cdn\/images\/7476cdc2-32dc-46e3-9102-d5f9b5e5e3b0.png",
                    "thumb": "https:\/\/storage.googleapis.com\/tti-cdn\/cdn\/images\/7476cdc2-32dc-46e3-9102-d5f9b5e5e3b0_thumb.png"
                }
            ],
            "motor_product": [],
            "motors": []
        }
    ]
}
```

### HTTP Request
`GET admin/v1/products`

`HEAD admin/v1/products`


<!-- END_09e4023ac1fa516966aa08517d4373a1 -->

