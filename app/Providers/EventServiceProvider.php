<?php

namespace App\Providers;

use App\CategoryMotorProductVehicleYear;
use App\Motor;
use App\Observers\CategoryMotorProductVehicleYearObserver;
use App\Observers\MotorObserver;
use App\Observers\MotorProductObserver;
use App\Product;
use App\ProductFeatureOption;
use App\ProductFeatureOptionLanguage;
use App\Observers\ProductObserver;
use App\Observers\InheritProductObserver;

use Illuminate\Support\Facades\Event;
use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
//    protected $listen = [
//        'App\Events\ProductChangeEvent' => [
//            'App\Listeners\ProductChangeListener',
//        ],
//    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();
        Product::observe(new ProductObserver());
        ProductFeatureOption::observe(new InheritProductObserver());
        ProductFeatureOptionLanguage::observe(new InheritProductObserver());
        Motor::observe(new MotorObserver());
        CategoryMotorProductVehicleYear::observe(new MotorProductObserver());
    }
}
