@extends('dashboard.layout.main')

@section('stylesheet')
    <link href="/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="/css/plugins/colorpicker/bootstrap-colorpicker.min.css" rel="stylesheet">
@stop

@section('javascript')
    <!-- iCheck -->
    <script src="/js/plugins/iCheck/icheck.min.js"></script>

    <!-- Color picker -->
    <script src="/js/plugins/colorpicker/bootstrap-colorpicker.min.js"></script>

    <script>
        $(function(){
            $('.colorpicker').colorpicker();

            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });

            $('.add-option').click(function(){
                $.get('/dashboard/snippets/product-feature-option-input', function(data){
                    $(data).appendTo(".more_options");
                });
            });

        });
    </script>
@stop

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>{{ trans('pages.inquiry_detail_header') }}</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('home') }}">{{ trans('menu.home') }}</a>
                </li>
                <li>
                    <a href="">{{ trans('menu.inquiries') }}</a>
                </li>
                <li class="active">
                    <strong>{{ trans('page.inquiry') }}</strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">
        {!! session()->has('success')? '<div class="alert alert-success alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>' . session('success') . '</div>' : '' !!}

        <form id="profileForm" method="post" class="form-horizontal">
            <div class="row">
                <div class="col-md-8">
                    <div class="ibox">

                        <div class="ibox-title">
                            <h5>New Attribute</h5>
                        </div>

                        <div class="ibox-content">
                            <div class="mb20"></div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Attribute</label>
                                        <div class="col-md-10">
                                            <select class="select2 form-control" name="attribute">
                                                <option></option>
                                                @foreach($attributes as $attribute)
                                                    <option value="{{ $attribute->id }}" {{ ($selected_attribute == $attribute->id)? 'selected' : '' }}>{{ $attribute->multilanguage->info()->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Language</label>
                                        <div class="col-md-10">
                                            <select id="multi-language" class="form-control countries-select" name="languages">
                                                <option value="all" icon="United-Nations" selected>All</option>

                                                @foreach($languages as $language)
                                                    <option value="{{ $language->id }}" icon="{{ $language->icon }}">{{ $language->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="multi-language-info-box multi-language-all">
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">Name</label>
                                            <div class="col-md-10"><input type="text" class="form-control all-language-input feature-type-name-input" data-input="feature-type-name-input" placeholder="Attribute Name"></div>
                                        </div>
                                    </div>

                                    @foreach($languages as $language)
                                        <div class="multi-language-info-box multi-language-{{ $language->id}}">
                                            <div class="form-group">
                                                <label class="col-md-2 control-label">Name</label>
                                                <div class="col-md-10"><input type="text" class="form-control all-language-input feature-type-name-input" name="option[{{ $language->id }}][name]" placeholder="Attribute Name"></div>
                                            </div>
                                        </div>
                                    @endforeach

                                    @if($attribute->product_attribute_type->name == 'color')
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">Color</label>
                                            <div class="col-md-10">
                                                <input type="text" class="form-control colorpicker" name="color" placeholder="#5367ce" />
                                            </div>
                                        </div>
                                    @endif

                                </div>
                            </div>
                        </div>
                    </div> 
                </div>

                <div class="col-md-4">
                    <div class="ibox">
                        <div class="ibox-title">
                            <h5>Action</h5>
                        </div>

                        <div class="ibox-content">
                            {{ csrf_field() }}
                            <button class="btn btn-block btn-outline btn-success" type="submit"><i class="fa fa-floppy-o"></i> Save</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>

        <div class="mb30"></div>
    </div>
@stop