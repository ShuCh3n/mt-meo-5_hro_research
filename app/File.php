<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Nanigans\SingleTableInheritance\SingleTableInheritanceTrait;

class File extends Model
{
    use SoftDeletes;
    use SingleTableInheritanceTrait;

    protected $table = "files";
    protected static $singleTableTypeField = 'type';
    protected static $singleTableSubclasses = [ProductMaterialSafetyDataSheet::class, ProductCertificate::class];

    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];

    public function filable()
    {
        return $this->morphTo();
    }
}
