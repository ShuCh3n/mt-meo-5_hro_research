<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RewardProgramOrderLine extends Model
{
    protected $fillable = ['product_id', 'name', 'qty', 'price', 'credit'];
}
