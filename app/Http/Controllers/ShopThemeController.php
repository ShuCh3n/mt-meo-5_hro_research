<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Shop;
use App\Cart;
use App\CustomerSession;

class ShopThemeController extends Controller
{
	private $_request;
    private $_shop;

    public function __construct(Request $request)
    {
        $this->_request = $request;
        $this->_shop = Shop::where('shop_link', $request->shop_name)->firstOrFail();

        view()->composer('*', function ($view) {
            $view->with('shop', $this->_shop);
        });
    }

    public function home()
    {
    	return view('themes.' . $this->_shop->theme->folder . '.home');
    }

    public function category()
    {
        $category = $this->_shop->categories()->findOrFail($this->_request->category_id);
        
        return view('themes.' . $this->_shop->theme->folder . '.category', compact('category'));
    }

    public function cart()
    {
        $cart = customer_session(session('customer_session'))->cart;
        return view('themes.' . $this->_shop->theme->folder . '.cart', compact('cart'));
    }

    public function checkout()
    {
        return view('themes.' . $this->_shop->theme->folder . '.checkout');
    }

    public function product()
    {
        $product = $this->_shop->products()->findOrFail($this->_request->product_id);

        return view('themes.' . $this->_shop->theme->folder . '.product', compact('product'));
    }

    public function contact()
    {
        return view('themes.' . $this->_shop->theme->folder . '.contact');
    }

    public function page()
    {
        return view('themes.' . $this->_shop->theme->folder . '.page');
    }

    public function changeLanguage()
    {
        $language = $this->_shop->languages()->where('iso', $this->_request->language)->firstOrFail();

        session(['language' => $language]);

        return back();
    }

    public function changeCurrency()
    {
        $currency = $this->_shop->currencies()->where('symbol', $this->_request->currency)->firstOrFail();

        session(['currency' => $currency]);

        return back();
    }

    public function add_product()
    {
        $product = $this->_shop->products()->findOrFail($this->_request->product_id);
        $qty = 1;
        $static = false;

        if($this->_request->qty)
        {
            $qty = $this->_request->qty;
        }

        if($this->_request->static)
        {
            $static = $this->_request->static;
        }

        if(!customer_session(session('customer_session'))->cart)
        {
            $cart = new Cart;
            $cart->shop_id = $this->_shop->id;
            $cart->customer_session_id = customer_session(session('customer_session'))->id;
            $cart->save();
        }
        else
        {
            $cart = customer_session(session('customer_session'))->cart;
        }

        return $cart->addProduct($product, $qty, $static);
    }
}
