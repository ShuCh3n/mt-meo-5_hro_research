<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Shop;
use App\Category;
use App\CategoryInfo;
use App\Language;
use Illuminate\Pagination\LengthAwarePaginator;

class CategoryController extends Controller
{
    public function categories()
	{
		$categories = $this->request->shop->categories()->with(['info' => function($query){
			$query->language($this->request->language->id);
		}])->get();

        return $categories;
	}

    public function newCategory()
    {
        $shops = Shop::all();
        $languages = Language::all();

        return view('dashboard.new_category', compact('shops', 'languages'));
    }

    public function editCategory()
    {
        $category = Category::findOrFail($this->request->id);
        $shops = Shop::all();
        $languages = Language::all();

        return view('dashboard.edit_category', compact('category', 'shops', 'languages'));
    }

    public function shopCategories()
    {
        $categories = Category::where('shop_id', $this->request->id)->whereNull('parent_id')->get();

        return view('dashboard.shop_categories', compact('categories'));
    }

    public function shopCategory()
    {
        $category = Shop::findOrFail($this->request->shop_id)->categories()->findOrFail($this->request->id);
        return view('dashboard.shop_category', compact('category'));
    }

    public function actionNewCategory()
    {
    	$this->validate($this->request, [
            'shops' => 'required'
        ]);

    	foreach($this->request->shops as $shop)
    	{
    		$shop = Shop::findOrFail($shop);
    		$category = false;

    		foreach($this->request->category_info as $language_id => $info)
    		{
				if(!empty($info["category_name"]) && $language_id != 0)
    			{
    				if(!$category)
    				{
    					$category = new Category;
    					$category->parent_id = (!empty($this->request->parent_category[$shop->id]))? $this->request->parent_category[$shop->id] : null;
						$category->shop_id = $shop->id;
						$category->rank = 10;
						$category->save();
    				}

     				$category_info = new CategoryInfo;
    				$category_info->category_id = $category->id;
    				$category_info->language_id = $language_id;
    				$category_info->name = $info["category_name"];
    				$category_info->meta_description = $info["meta_description"];
    				$category_info->link = $info["meta_link"];
    				$category_info->description = $info["description"];
    				$category_info->save();
    			}
    		}
    	}
    	
    	return redirect()->route('categories');
    }

    public function actionEditCategory()
    {
        $this->validate($this->request, [
            'shops' => 'required'
        ]);

        $category = Category::findOrFail($this->request->id);
        $category->parent_id = (!empty($this->request->parent_category[$category->shop_id]))? $this->request->parent_category[$category->shop_id] : null;
        $category->rank = 10;
        $category->save();

        foreach($this->request->category_info as $language_id => $info)
        {
            if(!empty($info["category_name"]))
            {
                $category_info = $category->info()->language($language_id)->first();

                if(!$category_info)
                {
                    $category_info = new CategoryInfo;
                    $category_info->category_id = $category->id;
                    $category_info->language_id = $language_id;
                }
                
                $category_info->name = $info["category_name"];
                $category_info->meta_description = $info["meta_description"];
                $category_info->link = $info["meta_link"];
                $category_info->description = $info["description"];
                $category_info->save();
            }
        }

        return redirect()->route('shop_categories', $category->shop_id);
    }

    public function remove()
    {
        dd("Skip for now");
    }

    public function resetCategories()
    {
        $shop = Shop::name($this->request->shop)->firstOrFail();

        foreach($shop->categories as $category)
        {
            $category->products()->detach();
        }

        return array('status' => true);
    }

    public function categoryProducts()
	{
		$filters = null;
		$paginate = 32;
		$sort = null;
		$sort_type = null;
		$current_page = $this->request->page ?? 1;

		if($this->request->filters)
		{
			$filters = $this->request->filters;
		}

		if($this->request->paginate && $this->request->paginate <= 50)
		{
			$paginate = $this->request->paginate;
		}

		if($this->request->sort)
		{
			$sort = $this->request->sort;
			$sort_type = $this->request->sort_type;
		}

        $category = $this->request->shop->categories()->where('id', $this->request->id)->firstOrFail();
		$category->info = $category->info();

        $category_products = $this->getAllCategoryProducts($category, $filters);
        $num_products = $category_products->count();

        if($sort)
        {
            if($sort_type == "asc")
            {
                $category_products = $category_products->orderBy($sort, $sort_type);
            }

            if($sort_type == "desc")
            {
                $category_products = $category_products->orderBy($sort, $sort_type);
            }
        }

        $category->products = new LengthAwarePaginator($category_products->take($paginate)->skip(($current_page-1)*$paginate)->get(), $num_products, $paginate, $current_page);
		foreach($category->products as $product)
		{

            $prices = new ShopProductPrice($product, $this->request->shop);

            $product->prices = $prices;


			if($product->brand->image->first())
			{
				$product->logo = [
					'small' => env('CDN_URI') . '/images/brands/' . $product->brand->image->first()->filename . '_small.png',
					'medium' => env('CDN_URI') . '/images/brands/' . $product->brand->image->first()->filename . '_medium.png',
					'large' => env('CDN_URI') . '/images/brands/' . $product->brand->image->first()->filename . '_large.png'
				];
			}
			else
			{
				$product->logo = [];
			}

			$imagesSource = $product->allimages($this->request->shop->id);
	        $images = [];

	        foreach($imagesSource as $image)
	        {
	            $images[] = array(
	                'img' => env('CDN_URI') . '/images/' . $image->filename . '.png',
	                'thumb' => env('CDN_URI') . '/images/' . $image->filename . '_thumb.png'
	            );
	        }

        	$product->images = $images;

        	$paginate_product = $category->products->where('product_id', $product->id)->first(function($value, $key) use ($category, $product){
				$category->products[$key] = $product;
			});
		}
		return $category;
	}

	public function categoryFilters()
	{
		$category = $this->request->shop->categories()->where('id', $this->request->id)->first();
        $this->grabProductFilters();
		$filters = new ProductFilters($category->products()->Filter($this->filters)->featureInfo($this->request->language->id)->with('product_price'), $this->request->language->id, $this->request->shop->id);
		return array("filters" => $filters);
	}

	private function getAllCategoryProducts($category, $filters)
	{
		if(!$filters)
		{
			$filters = array();
		}

		$products = array();

		$category_products = $category->products()->featureInfo($this->request->language->id)->ProductFilter($this->request->shop->id)->shopFilter($filters, $this->request->shop->id)->shopInfo($this->request->shop->id, $this->request->language->id)->shopActive($this->request->shop->id)->with(['product_price'])->leftJoin('brands', 'brand_id', 'brands.id')->leftJoin('product_shop_prices', function ($join){
		    $join->on('products.id', 'product_shop_prices.product_id')->where('product_shop_prices.shop_id', $this->request->shop->id)->where('product_shop_prices.deleted_at','=', null);
        });

        return $category_products;
	}
}
