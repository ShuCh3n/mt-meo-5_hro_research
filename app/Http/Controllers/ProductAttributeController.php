<?php

namespace App\Http\Controllers;

use App\Http\Requests;

use App\Language;
use App\ProductAttribute;
use App\ProductAttributeType;
use App\ProductAttributeOption;
use App\ProductAttributeLanguage;
use App\ProductAttributeOptionLanguage;

class ProductAttributeController extends Controller
{
    public function index()
    {
    	$attributes = ProductAttribute::all();
    	return view('dashboard.product_attributes', compact('attributes'));
    }

    public function newProductAttribute()
    {
    	$attribute_types = ProductAttributeType::all();
    	$languages = Language::all();

    	return view('dashboard.new_product_attribute', compact('attribute_types', 'languages'));
    }

    public function actionNewProductAttribute()
    {
    	$attribute = new ProductAttribute;
    	$attribute->product_attribute_type_id = $this->request->attribute_type;
    	$attribute->save();

    	foreach($this->request->attribute as $language_id => $info)
    	{
			$attribute_language = new ProductAttributeLanguage;
			$attribute_language->product_attribute_id = $attribute->id;
			$attribute_language->language_id = $language_id;
			$attribute_language->name = $info["name"];
			$attribute_language->save();
    	}

    	return redirect()->route('product_attribute_options', $attribute->id);
    }

    public function productAttributeOptions()
    {
    	$attribute = ProductAttribute::findOrFail($this->request->id);
    	return view('dashboard.product_attribute_options', compact('attribute'));
    }

    public function newProductAttributeOption()
    {
    	$attributes = ProductAttribute::all();
    	$selected_attribute = ($this->request->attribute)? $this->request->attribute  : null;
    	$languages = Language::all();

    	return view('dashboard.new_product_attribute_option', compact('attributes', 'selected_attribute', 'languages'));
    }

    public function actionNewProductAttributeOption()
    {
    	$attribute = ProductAttribute::findOrFail($this->request->attribute);

		$attribute_option = new ProductAttributeOption;
		$attribute_option->product_attribute_id = $attribute->id;

		if($attribute->product_attribute_type->name == 'color')
		{
			$attribute_option->color = $this->request->color;
		}

		$attribute_option->save();

    	foreach($this->request->option as $language_id => $info)
    	{
			$attribute_language = new ProductAttributeOptionLanguage;
			$attribute_language->product_attribute_option_id = $attribute_option->id;
			$attribute_language->language_id = $language_id;
			$attribute_language->name = $info["name"];
			$attribute_language->save();
    	}

    	return redirect()->route('product_attribute_options', $attribute->id);
    }

    public function editProductAttribute()
    {
    	$attribute = ProductAttribute::findOrFail($this->request->id);
    	$attribute_types = ProductAttributeType::all();

    	return view('dashboard.edit_product_attribute', compact('attribute', 'attribute_types'));
    }

    public function actionEditProductAttribute()
    {
    	$attribute = ProductAttribute::findOrFail($this->request->id);
    	$attribute->product_attribute_type_id = $this->request->attribute_type;
    	$attribute->save();

		foreach($this->request->attribute as $language_id => $info)
		{
			$attribute_language = $attribute->info()->where('language_id', $language_id)->first();

			if(!$attribute_language)
			{
				$attribute_language = new ProductAttributeLanguage;
			}
			
			if(auth()->user()->client->hasLanguage($language_id))
			{
				$attribute_language->product_attribute_id = $attribute->id;
				$attribute_language->language_id = $language_id;
				$attribute_language->name = $info["name"];
				$attribute_language->save();
			}
		}

		return redirect()->route('dashboard.product_attribute_options', $attribute->id);
    }

    public function editProductAttributeOption()
    {
    	$attribute = ProductAttribute::findOrFail($this->request->attribute_id);
    	$attributes = ProductAttribute::all();
    	$attribute_option = ProductAttributeOption::findOrFail($this->request->id);

    	return view('dashboard.edit_product_attribute_option', compact('attributes', 'attribute', 'attribute_option'));
    }

    public function actionEditProductAttributeOption()
    {
    	$attribute = ProductAttribute::findOrFail($this->request->attribute_id);
    	$attribute_option = $attribute->options()->findOrFail($this->request->id);

    	$attribute_option->product_attribute_id = $this->request->attribute;

    	if($attribute->product_attribute_type->name == 'color')
		{
			$attribute_option->color = $this->request->color;
		}

		$attribute_option->save();

		foreach($this->request->option as $language_id => $info)
		{
			$attribute_option_language = $attribute_option->info()->where('language_id', $language_id)->first();

			if(!$attribute_option_language)
			{
				$attribute_option_language = new ProductAttributeOptionLanguage;
			}

			if(auth()->user()->client->hasLanguage($language_id))
			{
				$attribute_option_language->product_attribute_option_id = $attribute_option->id;
				$attribute_option_language->language_id = $language_id;
				$attribute_option_language->name = $info["name"];
				$attribute_option_language->save();
			}
		}

		return redirect()->route('dashboard.product_attribute_options', $attribute->id);
    }

    public function removeProductAttribute()
    {
    	$attribute = ProductAttribute::findOrFail($this->request->id);
    	$attribute->delete();

    	return redirect()->route('dashboard.product_attributes');
    }

    public function removeProductAttributeOption()
    {
    	$attribute = ProductAttribute::findOrFail($this->request->attribute_id);
    	$attribute_option = $attribute->options()->findOrFail($this->request->id);
    	$attribute_option->delete();
    	
    	return redirect()->route('dashboard.product_attribute_options', $attribute->id);
    }

    public function getOptions()
    {
        $attribute = ProductAttribute::with('options')->findOrFail($this->request->id);
        $attribute->name = $attribute->multilanguage->info()->name;

        foreach($attribute->options as $option)
        {
            $option->name = $option->multilanguage->info()->name;
        }

        return $attribute;
    }
}
