@extends('dashboard.layout.main')

@section('content')
	<div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-6">
            <h2>{{ trans('pages.inquiry_detail_header') }}</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('home') }}">{{ trans('menu.home') }}</a>
                </li>
                <li>
                    <a href="">{{ trans('menu.inquiries') }}</a>
                </li>
                <li class="active">
                    <strong>{{ trans('page.inquiry') }}</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-6">
            <a class="btn btn-outline btn-primary pull-right mt30" href="{{ route('new_product_group') }}"><i class="fa fa-plus-circle" aria-hidden="true"></i> New Employee</a>
        </div>
    </div>

	<div class="wrapper wrapper-content animated fadeInRight ecommerce">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-content">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th data-toggle="true">Name</th>
                                    <th data-toggle="true">Department</th>
                                    <th data-toggle="true">Super User</th>
                                    <th data-toggle="true">Status</th>
                                    <th class="text-right" data-sort-ignore="true">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            	@foreach($users as $user)
                                    <tr>
                                        <td>
                                           {{ $user->firstname }} {{ $user->lastname }}
                                        </td>
                                        <td>
                                           IT
                                        </td>
                                        <td>
                                           <i class="fa fa-check text-navy" aria-hidden="true"></i>
                                        </td>
                                        <td>
                                           <i class="fa fa-check text-navy" aria-hidden="true"></i>
                                        </td>
                                        <td class="text-right">
                                            <a href="{{ route('edit_employee', $user->id) }}" class="btn-white btn btn-xs">Edit</a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop