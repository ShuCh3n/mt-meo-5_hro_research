<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('parent_id')->nullable()->unsigned();
            $table->integer('shop_id')->unsigned();
            $table->integer('rank');
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('parent_id')
                ->references('id')
                ->on('categories')
                ->onDelete('cascade');

            $table->foreign('shop_id')
                ->references('id')
                ->on('shops')
                ->onDelete('cascade');
        });

        DB::table('categories')->insert([
            [
                'shop_id' => 3,
                'rank' => 1
            ]
        ]);

        Schema::create('category_infos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('category_id')->unsigned();
            $table->integer('language_id')->unsigned();
            $table->string('name');
            $table->string('meta_description')->nullable();
            $table->text('description')->nullable();
            $table->string('link')->nullable();
            $table->timestamps();

            $table->foreign('category_id')
                ->references('id')
                ->on('categories')
                ->onDelete('cascade');

            $table->foreign('language_id')
                ->references('id')
                ->on('languages')
                ->onDelete('cascade');
        });

        DB::table('category_infos')->insert([
            [
                'category_id' => 1,
                'language_id' => 2,
                'name' => "zomerbanden",
                'meta_description' => 'zomerbanden',
                'description' => 'zomerbanden',
                'link' => 'zomerbanden'
            ]
        ]);

        Schema::create('category_product', function (Blueprint $table) {
            $table->integer('category_id')->unsigned();
            $table->integer('product_id')->unsigned();

            $table->foreign('category_id')
                ->references('id')
                ->on('categories')
                ->onDelete('cascade');

            $table->foreign('product_id')
                ->references('id')
                ->on('products')
                ->onDelete('cascade');

            $table->primary(['category_id', 'product_id']);
        });

        DB::table('category_product')->insert([
            [
                'category_id' => 1,
                'product_id' => 7
            ]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('category_product');
        Schema::drop('category_infos');
        Schema::drop('categories');
    }
}