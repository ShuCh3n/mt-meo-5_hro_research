@extends('dashboard.layout.main')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>{{ trans('pages.inquiry_detail_header') }}</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('home') }}">{{ trans('menu.home') }}</a>
                </li>
                <li>
                    <a href="">{{ trans('menu.inquiries') }}</a>
                </li>
                <li class="active">
                    <strong>{{ trans('page.inquiry') }}</strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">

        <form method="post" class="form-horizontal" enctype="multipart/form-data">
            <div class="row">
                <div class="col-md-8">
                    <div class="ibox">

                        <div class="ibox-title">
                            <h5>New Shop</h5>
                        </div>

                        <div class="ibox-content">
                            <div class="mb20"></div>

                            <div class="row">
                                <div class="col-sm-4 col-md-3">
                                    <image-selector></image-selector>
                                </div>

                                <div class="col-sm-8 col-md-9">
                                
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Name</label>
                                        <div class="col-md-10"><input type="text" class="form-control" name="name" placeholder="Shop Name" value="{{ $shop->name }}"></div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Link</label>
                                        <div class="col-md-10"><input type="text" class="form-control" name="link" placeholder="Shop Link" value="{{ $shop->link }}"></div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Key</label>
                                        <div class="col-md-9"><p class="form-control-static">{{ $shop->key }}</p></div>
                                        <div class="col-md-1">
                                            <a class="btn btn-white btn-bitbucket pull-right">
                                                <i class="fa fa-refresh" aria-hidden="true"></i>
                                            </a>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Default Tax</label>
                                        @if($tax_groups->count() > 0)
                                            <tax-select tax_groups="{{ $tax_groups }}" selected_tax_group="{{ @$shop->tax_rule->tax_group->id }}"  selected_tax_rule="{{ @$shop->tax_rule->id }}"></tax-select>
                                        @else
                                            <div class="col-md-10">
                                                <p class="form-control-static">
                                                    <a href="{{ route('new_tax_group') }}">Create first tax group</a>
                                                </p>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="ibox">

                        <div class="ibox-title">
                            <h5>Pay.nl</h5>
                        </div>

                        <div class="ibox-content">
                            <div class="row">
                                <div class="col-md-12">
                                    
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Active<span class="text-danger">*</span></label>
                                        <div class="col-md-5">
                                            <label><input type="radio" class="form-control i-checks" name="paynl_active" value="1" {{ ($shop->paynl)? 'checked' : '' }}> Enabled</label>
                                        </div>
                                        <div class="col-md-5">
                                            <label><input type="radio" class="form-control i-checks" name="paynl_active" value="0" {{ (!$shop->paynl)? 'checked' : '' }}> Disabled</label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Mode<span class="text-danger">*</span></label>
                                        <div class="col-md-5">
                                            <label><input type="radio" class="form-control i-checks" name="paynl_mode" value="0" {{ ($shop->paynl)? (!$shop->paynl->test_mode)? 'checked' : '' : '' }}> Live</label>
                                        </div>
                                        <div class="col-md-5">
                                            <label><input type="radio" class="form-control i-checks" name="paynl_mode" value="1" {{ ($shop->paynl)? ($shop->paynl->test_mode)? 'checked' : '' : '' }}> Testing</label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Service ID</label>
                                        <div class="col-md-10"><input type="text" class="form-control" name="paynl_service_id" placeholder="Service ID" value="{{ ($shop->paynl)? $shop->paynl->service_id : '' }}"></div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Token</label>
                                        <div class="col-md-10"><input type="text" class="form-control" name="paynl_api_token" placeholder="API Token" value="{{ ($shop->paynl)? $shop->paynl->api_token : '' }}"></div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="ibox">

                        <div class="ibox-title">
                            <h5>Languages</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                            </div>
                        </div>

                        <div class="ibox-content" style="display: none;">
                            <div class="row">
                                <div class="col-md-12">
                                    @foreach($languages as $language)
                                        <div class="clearfix">
                                            <div class="col-md-6"><img src="/images/flags/16/{{ $language->icon }}.png" alt="flag"> {{ $language->name }}</div>
                                            <div class="col-md-6">
                                                <div class="switch pull-right">
                                                    <div class="onoffswitch">
                                                        <input name="languages[]" id="language_{{ $language->id }}" type="checkbox" class="onoffswitch-checkbox" value="{{ $language->id }}" {{ ($shop->hasLanguage($language->id))? 'checked' : '' }}>
                                                        <label class="onoffswitch-label" for="language_{{ $language->id }}">
                                                            <span class="onoffswitch-inner"></span>
                                                            <span class="onoffswitch-switch"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <hr />
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="ibox">

                        <div class="ibox-title">
                            <h5>Currencies</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                            </div>
                        </div>

                        <div class="ibox-content" style="display: none;">
                            <div class="row">
                                <div class="col-md-12">
                                    @foreach($currencies as $currency)
                                        <div class="clearfix">
                                            <div class="col-md-6"><img src="/images/flags/16/{{ $currency->icon }}.png" alt="flag"> {{ $currency->name }}</div>
                                            <div class="col-md-6">
                                                <div class="switch pull-right">
                                                    <div class="onoffswitch">
                                                        <input name="currencies[]" id="currency_{{ $currency->id }}" type="checkbox" class="onoffswitch-checkbox" value="{{ $currency->id }}" {{ ($shop->hasCurrency($currency->id))? 'checked' : '' }}>
                                                        <label class="onoffswitch-label" for="currency_{{ $currency->id }}">
                                                            <span class="onoffswitch-inner"></span>
                                                            <span class="onoffswitch-switch"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <hr />
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="ibox">

                        <div class="ibox-title">
                            <h5>Countries</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                            </div>
                        </div>

                        <div class="ibox-content" style="display: none;">
                            <div class="row">
                                <div class="col-md-12">
                                	@foreach($countries as $country)
                                        <div class="clearfix">
                                            <div class="col-md-6"><img src="/images/flags/16/{{ $country->icon }}.png" alt="flag"> {{ $country->name }}</div>
                                            <div class="col-md-6">
                                                <div class="switch pull-right">
                                                    <div class="onoffswitch">
                                                        <input name="countries[]" id="country_{{ $country->name }}" type="checkbox" class="onoffswitch-checkbox" value="{{ $country->id }}" {{ ($shop->hasCountry($country->id))? 'checked' : '' }}>
                                                        <label class="onoffswitch-label" for="country_{{ $country->name }}">
                                                            <span class="onoffswitch-inner"></span>
                                                            <span class="onoffswitch-switch"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <hr />
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>                    
                </div>

                <div class="col-md-4">
                    <div class="ibox">
                        <div class="ibox-title">
                            <h5>Action</h5>
                        </div>

                        <div class="ibox-content">
                            {{ csrf_field() }}
                            <button class="btn btn-block btn-outline btn-success" type="submit"><i class="fa fa-floppy-o"></i> Save</button>
                            <button class="btn btn-block btn-outline btn-warning" type="submit"><i class="fa fa-wrench" aria-hidden="true"></i> Maintenance</button>
                            <button class="btn btn-block btn-outline btn-danger" type="submit"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>

        <div class="mb30"></div>
    </div>
@stop