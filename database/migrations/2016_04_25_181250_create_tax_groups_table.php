<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\TaxGroup;

class CreateTaxGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tax_groups', function (Blueprint $table) {
            $table->increments('id');
            $table->string('declaration_section_code')->nullable();
            $table->string('name');
            $table->boolean('default');
            $table->timestamps();
        });

        DB::table('tax_groups')->insert([
            [
                'declaration_section_code'      => '1A',
                'name' => 'Belast met hoog tarief',
                'default' => true
            ],
            [
                'declaration_section_code'      => '1B',
                'name' => 'Belast met laag tarief',
                'default' => false
            ],
            [
                'declaration_section_code'      => '1C',
                'name' => 'Overige tarieven (behalve 0%)',
                'default' => false
            ],
            [
                'declaration_section_code'      => '1D',
                'name' => 'Prive-gebruik',
                'default' => false
            ],
            [
                'declaration_section_code'      => '1E',
                'name' => 'Belast met 0% (niet bij mij belast)',
                'default' => false
            ],
            [
                'declaration_section_code'      => '2A',
                'name' => 'Heffing verlegd naar mij',
                'default' => false
            ],
            [
                'declaration_section_code'      => '3A',
                'name' => 'Buiten de EU',
                'default' => false
            ],
            [
                'declaration_section_code'      => '3B',
                'name' => 'Binnen de EU',
                'default' => false
            ],
            [
                'declaration_section_code'      => '3B',
                'name' => 'Binnen de EU (diensten)',
                'default' => false
            ],
            [
                'declaration_section_code'      => '3C',
                'name' => 'Installatie binnen de EU',
                'default' => false
            ],
            [
                'declaration_section_code'      => '4A',
                'name' => 'Van buiten de EU',
                'default' => false
            ],
            [
                'declaration_section_code'      => '4B',
                'name' => 'Verwerving goederen uit de EU',
                'default' => false
            ],
            [
                'declaration_section_code'      => '5A',
                'name' => 'Subtotaal',
                'default' => false
            ],
            [
                'declaration_section_code'      => '5B',
                'name' => 'Voorbelasting',
                'default' => false
            ],
            [
                'declaration_section_code'      => '5C',
                'name' => 'Subtotaal',
                'default' => false
            ],
            [
                'declaration_section_code'      => '5D',
                'name' => 'Kleine ondernemers',
                'default' => false
            ],
            [
                'declaration_section_code'      => '5F',
                'name' => 'Schatting deze aangfite',
                'default' => false
            ],
            [
                'declaration_section_code'      => '5G',
                'name' => 'Totaal te betalen (terug te vragen)',
                'default' => false
            ],
            [
                'declaration_section_code'      => '6A',
                'name' => 'Wegens oninbare vorderingen',
                'default' => false
            ],
            [
                'declaration_section_code'      => '6B',
                'name' => 'Wegens herrekening en andere correctie',
                'default' => false
            ],
            [
                'declaration_section_code'      => '99',
                'name' => 'Wordt niet vermeld op BTW-aangifte',
                'default' => false
            ],
        ]);

        Schema::create('tax_rules', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tax_group_id')->unsigned();
            $table->boolean('enable');
            $table->string('code')->nullable();
            $table->string('name');
            $table->decimal('fixed_price', 10, 2)->nullable();
            $table->decimal('percentage', 10, 2)->nullable();
            $table->timestamps();

            $table->foreign('tax_group_id')
                ->references('id')
                ->on('tax_groups')
                ->onDelete('cascade');
        });

        DB::table('tax_rules')->insert([
            [
                'tax_group_id'  => TaxGroup::where('declaration_section_code', '1A')->first()->id,
                'enable'        => true,
                'code'          => '1C',
                'name'          => 'Te betalen BTW hoog VK-IC',
                'percentage'    => 21
            ],
            [
                'tax_group_id'  => TaxGroup::where('declaration_section_code', '1A')->first()->id,
                'enable'        => true,
                'code'          => '1E',
                'name'          => 'Te betalen BTW hoog VK-EXP',
                'percentage'    => 21
            ],
            [
                'tax_group_id'  => TaxGroup::where('declaration_section_code', '1A')->first()->id,
                'enable'        => true,
                'code'          => '1I',
                'name'          => 'Te betalen BTW hoog VK-INT',
                'percentage'    => 21
            ],
            [
                'tax_group_id'  => TaxGroup::where('declaration_section_code', '1A')->first()->id,
                'enable'        => true,
                'code'          => '1N',
                'name'          => 'Te betalen BTW hoog VK-NL',
                'percentage'    => 21
            ],
            [
                'tax_group_id'  => TaxGroup::where('declaration_section_code', '2A')->first()->id,
                'enable'        => true,
                'code'          => '21',
                'name'          => 'BTW binnenland aankoop verlegd',
                'percentage'    => 21
            ],
            [
                'tax_group_id'  => TaxGroup::where('declaration_section_code', '3A')->first()->id,
                'enable'        => true,
                'code'          => '31',
                'name'          => 'BTW 0-tarief Export buiten EU',
                'percentage'    => 0
            ],
            [
                'tax_group_id'  => TaxGroup::where('declaration_section_code', '3B')->first()->id,
                'enable'        => true,
                'code'          => '3E',
                'name'          => 'BTW 0-tarief ICL binnen EU VK-EXP',
                'percentage'    => 0
            ],
            [
                'tax_group_id'  => TaxGroup::where('declaration_section_code', '3B')->first()->id,
                'enable'        => true,
                'code'          => '3I',
                'name'          => 'BTW 0-tarief ICL binnen EU VK-INT',
                'percentage'    => 0
            ],
            [
                'tax_group_id'  => TaxGroup::where('declaration_section_code', '3B')->first()->id,
                'enable'        => true,
                'code'          => '3N',
                'name'          => 'BTW 0-tarief ICL binnen EU VK-NL',
                'percentage'    => 0
            ],
            [
                'tax_group_id'  => TaxGroup::where('declaration_section_code', '4A')->first()->id,
                'enable'        => true,
                'code'          => '41',
                'name'          => 'BTW 0-tarief Import (buiten EU)',
                'percentage'    => 21
            ],
            [
                'tax_group_id'  => TaxGroup::where('declaration_section_code', '4B')->first()->id,
                'enable'        => true,
                'code'          => '42',
                'name'          => 'BTW 0-tarief ICV (binnen EU)',
                'percentage'    => 21
            ],
            [
                'tax_group_id'  => TaxGroup::where('declaration_section_code', '5B')->first()->id,
                'enable'        => true,
                'code'          => '51',
                'name'          => 'Te vorderen BTW 0-tarief',
                'percentage'    => 0
            ],
            [
                'tax_group_id'  => TaxGroup::where('declaration_section_code', '5B')->first()->id,
                'enable'        => true,
                'code'          => '52',
                'name'          => 'Te vorderen BTW laag',
                'percentage'    => 6
            ],
            [
                'tax_group_id'  => TaxGroup::where('declaration_section_code', '5B')->first()->id,
                'enable'        => true,
                'code'          => '53',
                'name'          => 'Te vorderen BTW hoog',
                'percentage'    => 21
            ],
            [
                'tax_group_id'  => TaxGroup::where('declaration_section_code', '99')->first()->id,
                'enable'        => true,
                'code'          => '91',
                'name'          => 'Te betalen BTW 0 (DPD)',
                'percentage'    => 0
            ],
            [
                'tax_group_id'  => TaxGroup::where('declaration_section_code', '99')->first()->id,
                'enable'        => true,
                'code'          => '92',
                'name'          => 'Te vorderen Belgische BTW',
                'percentage'    => 21
            ],
            [
                'tax_group_id'  => TaxGroup::where('declaration_section_code', '99')->first()->id,
                'enable'        => true,
                'code'          => '10',
                'name'          => 'Te vorderen MwSt.',
                'percentage'    => 19
            ]
        ]);

        Schema::create('country_tax_rule', function (Blueprint $table) {
            $table->integer('country_id')->unsigned();
            $table->integer('tax_rule_id')->unsigned();

            $table->foreign('country_id')
                ->references('id')
                ->on('countries')
                ->onDelete('cascade');

            $table->foreign('tax_rule_id')
                ->references('id')
                ->on('tax_rules')
                ->onDelete('cascade');

            $table->primary(['country_id', 'tax_rule_id']);
        });

        Schema::create('state_tax_rule', function (Blueprint $table) {
            $table->integer('state_id')->unsigned();
            $table->integer('tax_rule_id')->unsigned();

            $table->foreign('state_id')
                ->references('id')
                ->on('states')
                ->onDelete('cascade');

            $table->foreign('tax_rule_id')
                ->references('id')
                ->on('tax_rules')
                ->onDelete('cascade');

            $table->primary(['state_id', 'tax_rule_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('state_tax_rule');
        Schema::drop('country_tax_rule');
        Schema::drop('tax_rules');
        Schema::drop('tax_groups');
    }
}
