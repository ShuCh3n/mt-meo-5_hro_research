<?php

namespace App\Jobs;


use App\CategoryMotorProductVehicleYear;
use App\Classes\WebHooks\WebHookFactory;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class UpdateCategoryMotorProductVehicleYear implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $model;
    protected $webhook;
    protected $event;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(UpdateCategoryMotorProductVehicleYear $model, $webhook, $event = 'create')
    {
        $this->model = $model;
        $this->webhook = $webhook;
        $this->event = $event;
    }
    /**

     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $webhook = WebHookFactory::create($this->model, $this->webhook, $this->event);
        $webhook->execute();
    }
}