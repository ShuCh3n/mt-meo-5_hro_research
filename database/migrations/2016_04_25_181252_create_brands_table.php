<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBrandsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('brands', function (Blueprint $table) {
           $table->increments('id');
            $table->string('logo')->nullable();
            $table->string('name');
            $table->json('segments')->nullable();
            $table->timestamps();
        });

        DB::table('brands')->insert([
            [
                'name' => 'test brand'
            ]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('brands');
    }
}
