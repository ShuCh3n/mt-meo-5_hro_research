<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductGroup extends Model
{
	use SubFilter;
	
    protected $fillable = ['name'];
    
	public function products()
	{
		return $this->hasMany(Product::class);
	}

    public function setGroup($request)
    {
    	$this->name = $request->name;
    	$this->save();
    }
	
	public function images()
	{
    	return $this->morphMany(Image::class, 'imagable');
	}
	
	public function shopImages($shop)
	{
    	return $this->morphMany(Image::class, 'imagable')->where('shop_id', $shop)->get();
	}

    public static function getGroup($group)
    {
    	if(is_numeric($group))
    	{
    		$product_group = ProductGroup::find($group);

    		if($product_group)
    		{
    			return $product_group;
    		}
    	}

    	return ProductGroup::where('name', $group)->first();
    }

    public function productFeatureType()
    {
        return $this->belongsTo(ProductFeatureType::class);
    }

    public static function insertIfNotExist($group)
    {
    	$product_group = self::getGroup($group);

    	if(!$product_group)
    	{
    		$product_group = new self;
    		$product_group->name = $group;
    		$product_group->save();
    	}

    	return $product_group;
    }
}
