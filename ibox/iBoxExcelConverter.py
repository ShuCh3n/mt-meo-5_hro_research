import pandas
import numpy
import sys

file = pandas.read_excel('../storage/app/ibox/analist/excel/' + sys.argv[1], encoding='utf8')

f = open('../storage/app/ibox/analist/json/' + sys.argv[1] + '.json', 'w')
f.write(file.to_json(orient='records'))