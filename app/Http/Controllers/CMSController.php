<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Shop;
use App\Page;
use App\PageInfo;
use App\Blog;
use App\BlogCategory;
use App\BlogTags;
use App\BlogCategoryInfo;
use App\BlogInfo;

use App\Language;
use App\Email;
use App\EmailInfo;
use App\EmailType;

use Storage;
use File;
use Carbon\Carbon;

class CMSController extends Controller
{
    //pages
    public function pages()
    {
        $shops = Shop::all();
        return view('dashboard.pages', compact('shops'));
    }

    public function shopPages()
    {
        $shop = Shop::findOrFail($this->request->shop_id);
        return view('dashboard.shop_pages', compact('shop'));
    }

    public function newPage()
    {
        $shops = Shop::all();
        $languages = Language::all();
        return view('dashboard.new_page', compact('shops', 'languages'));
    }

    public function editPage()
    {
        $page = Page::findOrFail($this->request->id);
        return view('dashboard.edit_page', compact('page'));
    }

    public function actionNewPage()
    {
        $this->validate($this->request, [
            'shops' => 'required'
        ]);

        foreach($this->request->shops as $shop_id)
        {
            if(auth()->user()->client->hasShop($shop_id))
            {
                $page = new Page;
                $page->active = $this->request->active;
                $page->shop_id = $shop_id;
                $page->rank = 10;
                $page->save();

                $this->setPageInfo($page);
            }
        }

        if($this->request->shop_id)
        {
            return redirect()->route('shop_pages', $this->request->shop_id);
        }

        return redirect()->route('pages');
    }

    public function actionEditPage()
    {
        $page = Page::findOrFail($this->request->id);
        $page->active = $this->request->active;
        $page->save();

        $this->setPageInfo($page);

        return redirect()->route('edit_page', $page->shop->id)->with('success', trans('success.edited'));
    }

    public function setPageInfo(Page $page)
    {
        foreach($this->request->page_info as $language_id => $info)
        {
            $page_info = $page->page_infos()->where('language_id', $language_id)->first();

            if(!$page_info)
            {
                $page_info = new PageInfo;
            }

            $page_info->page_id = $page->id;
            $page_info->language_id = $language_id;
            $page_info->title = $info["title"];
            $page_info->meta_description = $info["meta_description"];
            $page_info->link = $info["meta_link"];
            $page_info->body = $info["body"];
            $page_info->save();
        }
    }

    public function blogs()
    {

        $blogs = Blog::latest()->get();
        return view('dashboard.blogs', compact('blogs'));
    }

    public function newBlog()
    {
        $languages = Language::all();
        $categories = BlogCategory::get();
        $allTags = BlogTags::with('tagContent')->get();
        $shops = Shop::oldest('name')->get();

        return view('dashboard.new_blog', compact('languages', 'categories', 'allTags', 'shops'));
    }

    public function createBlog()
    {
        $blog = null;

        foreach($this->request->language as $language_id => $content)
        {
            if($language_id)
            {
                $content = (object) $content;

                if($content->title)
                {
                    if(!$blog)
                    {
                        $blog = new Blog;

                        if($this->request->cover_image)
                        {
                            $blog->cover_image = str_random(40) . "." . $this->request->cover_image->getClientOriginalExtension();
                            Storage::put('blogs/' . $blog->cover_image, File::get($this->request->cover_image), 'public');
                        }

                        $blog->shop_id = $this->request->shop;
                        $blog->blog_category_id = $this->request->category;
                        $blog->active = $this->request->active;
                        $blog->published_at = Carbon::now();
                        $blog->save();
                    }

                    $blog_info = new BlogInfo;
                    $blog_info->language_id = $language_id;
                    $blog_info->blog_id = $blog->id;
                    $blog_info->title = $content->title;
                    $blog_info->short = $content->short;
                    $blog_info->body = $content->body;
                    $blog_info->save();
                }
            }
        }

        return redirect()->route('blogs');
    }

    public function newBlogCategory()
    {
        $languages = Language::all();
        return view('dashboard.new_blog_category', compact('languages'));
    }

    public function createBlogCategory()
    {
        $blog_category = null;

        foreach($this->request->language as $language_id => $content)
        {
            if($language_id)
            {
                $content = (object) $content;

                if($content->name)
                { 
                    if(!$blog_category)
                    {
                        $blog_category = new BlogCategory;

                        if($this->request->cover_image)
                        {
                            $blog_category->cover_image = str_random(40) . "." . $this->request->cover_image->getClientOriginalExtension();
                            Storage::put('blogs/categories/' . $blog_category->cover_image, File::get($this->request->cover_image), 'public');
                        }

                        $blog_category->save();
                    }

                    $category_info = new BlogCategoryInfo;
                    $category_info->language_id = $language_id;
                    $category_info->blog_category_id = $blog_category->id;
                    $category_info->name = $content->name;
                    $category_info->save();
                }
            }
        }

        return redirect()->route('blogs');
    }

    public function deleteBlog()
    {
        $blog = Blog::findOrFail($this->request->id)->delete();

        return redirect()->route('blogs');
    }

    public function editBlog(){
        $blog = Blog::findOrFail($this->request->id);

        $languages = Language::all();
        $categories = BlogCategory::get();
        $allTags = BlogTags::with('tagContent')->get();

        return view('dashboard.edit_blog', compact('shops', 'blog', 'languages', 'categories', 'allTags'));
    }

    public function updateBlog()
    {
        // $blog = Blog::findOrFail($this->request->id);
        // $blog->title = $this->request->title;
        // $blog->shop_id = $this->request->shop;
        // $blog->active = $this->request->active;
        // $blog->update();
        return redirect()->route('blogs');
    }

    //emails
    public function emails()
    {
        $shops = Shop::with('emails')->get();
        return view('dashboard.emails', compact('shops'));
    }

    public function shopEmails()
    {
        $emails = Shop::findOrFail($this->request->id)->emails()->with(['info' => function($query){
            $query->backOffice();
        }])->get();

        return view('dashboard.shop_emails', compact('emails'));
    }

    public function newShopEmail()
    {
        $shops = Shop::all();
        $languages = Language::all();
        $email_types = EmailType::with(['info' => function($query){
            $query->backOffice();
        }])->get();

        return view('dashboard.new_email', compact('shops', 'email_types', 'languages'));
    }

    public function actionNewShopEmail()
    {
        $email = new Email;
        $email->shop_id = $this->request->shop;
        $email->email_type_id = $this->request->email_type;
        $email->active = $this->request->active;
        $email->save();

        $languages = Language::all();

        foreach($languages as $language)
        {
            if(!empty($this->request->email_title[$language->id]))
            {
                $view_file = $this->request->email_title[$language->id] . '_' . str_random(5) . '_' . $language->iso;

                $email_info = new EmailInfo;
                $email_info->email_id = $email->id;
                $email_info->language_id = $language->id;
                $email_info->title = $this->request->email_title[$language->id];
                $email_info->view_file = $view_file;
                $email_info->save();

                if(!File::exists(resource_path('views/emails/' . strtolower($email->shop->name))))
                {
                    File::makeDirectory(resource_path('views/emails/' . strtolower($email->shop->name)));
                }

                File::put(resource_path('views/emails/' . strtolower($email->shop->name) . '/' . $email_info->view_file . '.blade.php'), $this->request->email_body[$language->id]);
            }
        }

        return redirect()->route('shop_emails', $this->request->shop);
    }

    public function editShopEmail()
    {
        $email = Email::with('infos')->with('shop')->findOrFail($this->request->id);
        $languages = Language::all();
        $shops = Shop::all();
        $email_types = EmailType::with(['info' => function($query){
            $query->backOffice();
        }])->get();

        $email_content = array();

        foreach($languages as $language)
        {
            if($email->infos->where('language_id', $language->id)->count() > 0)
            {
                $email_content[$language->id] = File::get(resource_path('views/emails/' . strtolower($email->shop->name) . '/' . $email->info()->where('language_id', $language->id)->first()->view_file . '.blade.php'));
            }
        }

        return view('dashboard.edit_email', compact('email', 'email_types', 'email_content', 'languages', 'shops'));
    }

    public function actionEditShopEmail()
    {
        $email = Email::with('infos')->findOrFail($this->request->id);

        $email->email_type_id = $this->request->email_type;
        $email->active = $this->request->active;
        $email->save();

        $languages = Language::all();

        foreach($languages as $language)
        {
            if(!empty($this->request->email_title[$language->id]))
            {
                if(!$email->infos->where('language_id', $language->id)->first())
                {
                    $email_info = new EmailInfo;
                    $email_info->view_file = $this->request->email_title[$language->id] . '_' . str_random(5) . '_' . $language->iso;
                    $email_info->language_id = $language->id;
                    $email_info->email_id = $email->id;
                }
                else
                {
                    $email_info = $email->infos()->where('language_id', $language->id)->first();
                }

                $email_info->title = $this->request->email_title[$language->id];
                $email_info->save();

                if(!File::exists(resource_path('views/emails/' . strtolower($email->shop->name))))
                {
                    File::makeDirectory(resource_path('views/emails/' . strtolower($email->shop->name)));
                }

                File::put(resource_path('views/emails/' . strtolower($email->shop->name) . '/' . $email_info->view_file . '.blade.php'), $this->request->email_body[$language->id]);
            }
        }

        return redirect()->route('shop_emails', $email->shop_id);
    }

    public function editTags(){
        $languages = Language::All();
        $blog = Blog::findOrFail($this->request->blogid);
        $tags = BlogTags::where('blogs_id', $this->request->blogid)->get();
        return view('dashboard.edit_tags', compact('tags', 'blog'));
    }


    public function updateTag(){
        $tag = BlogTags::findOrFail($this->request->tagid);

        $tags = $this->request->tag;
        foreach ($tags as $languageid => $tagname){
            $taglang = BlogTagLanguages::firstOrNew(['blog_tags_id' => $tag->id, 'languages_id' => $languageid]);
            $taglang->name = $tagname;
            $taglang->save();
        }
        return redirect()->route('edit_blog_tags', $tag->blogs_id);
    }

    public function deleteTag(){
        $tag = BlogTags::findOrFail($this->request->tagid);
        $blog = $tag->blogs_id;
        $tag->delete();
        return redirect()->route('edit_blog_tags', $blog);
    }

    public function editTag(){
        $languages = Language::All();

        if($this->request->tagid){
            $tag = BlogTags::findOrFail($this->request->tagid);
        }else{
            $tag = new BlogTags();
            $tag->blogs_id =$this->request->blogid;
            $tag->save();
        }

        $tagnames = $tag->tagContent()->get();
        return view('dashboard.edit_tag', compact('tag', 'tagnames', 'languages'));
    }


}
