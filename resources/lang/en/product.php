<?php
/*************************************************************************
 Generated via "php artisan localization:missing" at 2017/04/10 16:46:28 
*************************************************************************/

return array (
  //============================== New strings to translate ==============================//
  // Defined in file /var/www/cms/resources/views/dashboard/shop_products.blade.php
  'bulk_edit' => 'bulk_edit',
  //==================================== Translations ====================================//
  'action' => 'action',
  'active' => 'active',
  'all' => 'all',
  'ean' => 'ean',
  'edit' => 'edit',
  'id' => 'id',
  'new_product' => 'new_product',
  'productname' => 'productname',
  'products' => 'products',
  'retailprice' => 'retailprice',
  'shop' => 'shop',
  'sku' => 'sku',
  'view' => 'view',
);