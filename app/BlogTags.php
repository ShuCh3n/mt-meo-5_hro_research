<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlogTags extends Model
{
    public function tagContent(){
        return $this->hasMany(BlogTagLanguages::class);
    }

    public function tagContentLang($language_id){
        return $this->hasMany(BlogTagLanguages::class)->where('languages_id', $language_id);
    }

    public function posts(){
        return $this->belongsToMany(BlogPost::class, 'blog_post_blog_tags');
    }
}
