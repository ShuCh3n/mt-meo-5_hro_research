<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCartRulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cart_rules', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('active');
            $table->boolean('giftcard');
            $table->boolean('free_shipping');
            $table->string('name');
            $table->boolean('amount_or_percentage');
            $table->decimal('amount', 10, 2);
            $table->string('voucher_code');
            $table->integer('usage_limit')->nullable();
            $table->date('valid_from')->nullable();
            $table->date('valid_until')->nullable();
            $table->timestamps();
        });

        Schema::create('cart_rule_country', function (Blueprint $table) {
            $table->integer('cart_rule_id')->unsigned();
            $table->integer('country_id')->unsigned();

            $table->foreign('cart_rule_id')
                ->references('id')
                ->on('cart_rules')
                ->onDelete('cascade');

            $table->foreign('country_id')
                ->references('id')
                ->on('countries')
                ->onDelete('cascade');

            $table->primary(['cart_rule_id', 'country_id']);
        });

        Schema::create('cart_rule_product', function (Blueprint $table) {
            $table->integer('cart_rule_id')->unsigned();
            $table->integer('product_id')->unsigned();

            $table->foreign('cart_rule_id')
                ->references('id')
                ->on('cart_rules')
                ->onDelete('cascade');

            $table->foreign('product_id')
                ->references('id')
                ->on('products')
                ->onDelete('cascade');

            $table->primary(['cart_rule_id', 'product_id']);
        });

        Schema::create('cart_rule_product_feature_type', function (Blueprint $table) {
            $table->integer('cart_rule_id')->unsigned();
            $table->integer('product_feature_type_id')->unsigned();

            $table->foreign('cart_rule_id')
                ->references('id')
                ->on('cart_rules')
                ->onDelete('cascade');

            $table->foreign('product_feature_type_id')
                ->references('id')
                ->on('product_feature_types')
                ->onDelete('cascade');

            $table->primary(['cart_rule_id', 'product_feature_type_id'], 'cridpftid');
        });

        Schema::create('cart_rule_shop', function (Blueprint $table) {
            $table->integer('cart_rule_id')->unsigned();
            $table->integer('shop_id')->unsigned();

            $table->foreign('cart_rule_id')
                ->references('id')
                ->on('cart_rules')
                ->onDelete('cascade');

            $table->foreign('shop_id')
                ->references('id')
                ->on('shops')
                ->onDelete('cascade');

            $table->primary(['cart_rule_id', 'shop_id']);
        });

        Schema::create('cart_rule_category', function (Blueprint $table) {
            $table->integer('cart_rule_id')->unsigned();
            $table->integer('category_id')->unsigned();

            $table->foreign('cart_rule_id')
                ->references('id')
                ->on('cart_rules')
                ->onDelete('cascade');

            $table->foreign('category_id')
                ->references('id')
                ->on('categories')
                ->onDelete('cascade');

            $table->primary(['cart_rule_id', 'category_id']);
        });

        Schema::create('cart_rule_customer', function (Blueprint $table) {
            $table->integer('cart_rule_id')->unsigned();
            $table->integer('customer_id')->unsigned();

            $table->foreign('cart_rule_id')
                ->references('id')
                ->on('cart_rules')
                ->onDelete('cascade');

            $table->foreign('customer_id')
                ->references('id')
                ->on('customers')
                ->onDelete('cascade');

            $table->primary(['cart_rule_id', 'customer_id']);
        });

        Schema::create('cart_rule_customer_group', function (Blueprint $table) {
            $table->integer('cart_rule_id')->unsigned();
            $table->integer('customer_group_id')->unsigned();

            $table->foreign('cart_rule_id')
                ->references('id')
                ->on('cart_rules')
                ->onDelete('cascade');

            $table->foreign('customer_group_id')
                ->references('id')
                ->on('customer_groups')
                ->onDelete('cascade');

            $table->primary(['cart_rule_id', 'customer_group_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cart_rule_customer_group');
        Schema::drop('cart_rule_customer');
        Schema::drop('cart_rule_category');
        Schema::drop('cart_rule_shop');
        Schema::drop('cart_rule_product_feature_type');
        Schema::drop('cart_rule_product');
        Schema::drop('cart_rule_country');
        Schema::drop('cart_rules');
    }
}
