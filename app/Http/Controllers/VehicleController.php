<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Motor;
use App\VehicleBrand;
use App\VehicleYear;
use App\Product;
use App\Shop;
use App\Category;
use Excel;

class VehicleController extends Controller
{
    public function motors()
    {
        $brands = VehicleBrand::oldest('name')->get();
    	$motors = Motor::with('vehicle_brand');
        $vehicle_years = VehicleYear::oldest('year')->get();
        $ccs = Motor::select('cc')->groupBy('cc')->oldest('cc')->get();

        if($this->request->q)
        {
            $query = $this->request->q;

            $motors = $motors->orWhere('code', 'like', '%' . $query . '%')
                        ->orWhere('name', 'like', '%' . $query . '%')
                        ->orWhere('model', 'like', '%' . $query . '%')
                        ->orWhere('type', 'like', '%' . $query . '%');
        }

        if($this->request->filter)
        {
            if(isset($this->request->filter['brand']))
            {
                $motors = $motors->whereHas('vehicle_brand', function($query){
                    $query->whereIn('id', $this->request->filter['brand']);
                });
            }

            if(isset($this->request->filter['year']))
            {
                $motors = $motors->whereHas('vehicle_years', function($query){
                    $query->whereIn('year', $this->request->filter['year']);
                });
            }

            if(isset($this->request->filter['cc']))
            {
                $motors = $motors->whereIn('cc', $this->request->filter['cc']);
            }
        }

        $motors = $motors->latest()->paginate(100)->appends($this->request->input());
    	return view('dashboard.motors', compact('motors', 'vehicle_years', 'ccs', 'brands'));
    }

    public function viewMotor()
    {
    	$motor = Motor::with(['vehicle_brand', 'vehicle_years', 'products'])->findOrFail($this->request->id);
        $categories = Shop::getShop('Motoria')->categories()->with('info')->whereNull('parent_id')->get();
        $subcategories = Shop::getShop('Motoria')->categories()->with(['parent', 'info'])->whereNotNull('parent_id')->get();

        $motor_oldest_year = $motor->vehicle_years()->oldest('year')->first()->year;
        $motor_newest_year = $motor->vehicle_years()->latest('year')->first()->year;

    	return view('dashboard.view_motor', compact('motor', 'products', 'categories', 'subcategories', 'motor_oldest_year', 'motor_newest_year'));
    }

    public function newMotor()
    {
        $vehicle_brands = VehicleBrand::oldest('name')->get();
        return view('dashboard.new_motor', compact('vehicle_brands'));
    }

    public function actionNewMotor()
    {
        $motor = Motor::create([
            "code"              => strtoupper(str_random(8)),
            "vehicle_brand_id"  => VehicleBrand::findOrFail($this->request->vehicle_brand)->id,
            "name"              => $this->request->name,
            "model"             => $this->request->model,
            "type"              => $this->request->type,
            "cc"                => $this->request->cc,
            "different_size"    => ($this->request->different_tyre_sizes)? true : false
        ]);
        
        for($i=$this->request->from_year;$i<=$this->request->until_year;$i++)
        {
            if(!$this->request->different_tyre_sizes)
            {
                $front_sizes = tyre_sizes($this->request->size_front);
                $rear_sizes = tyre_sizes($this->request->size_rear);
            }
            else
            {
                $front_sizes = tyre_sizes($this->request->custom_size_front[$i]);
                $rear_sizes = tyre_sizes($this->request->custom_size_front[$i]);
            }

            $motor->vehicle_years()->attach(VehicleYear::getByYear($i), [
                'tyre_front_width' => $front_sizes->width, 
                'tyre_front_height' => $front_sizes->height,
                'tyre_front_inch' => $front_sizes->inch,
                'tyre_rear_width' => $rear_sizes->width,
                'tyre_rear_height' => $rear_sizes->height,
                'tyre_rear_inch' => $rear_sizes->inch
            ]);
        }

        return redirect()->route('view_motor', $motor->id);
    }

    public function motorAddProduct()
    {
        $motor = Motor::findOrFail($this->request->id);
        $motor->products()->detach();

        foreach($this->request->categories as $category_id => $products)
        {
            foreach($products as $year => $sku)
            {
                if($sku)
                {
                    $product = Product::where('sku', $sku)->first();
                    $year = VehicleYear::getByYear($year);
                    $category = Category::findOrFail($category_id);

                    $month = isset($this->request->month[$category_id][$year->year])? carbon()->createFromFormat('M', $this->request->month[$category_id][$year->year])->format('m') : null;
                    $min_qty = $this->request->min_qty[$category_id][$year->year] ?? null;

                    $motor->categories()->attach($category, [
                        'product_id'        => $product->id ?? null,
                        'vehicle_year_id'   => $year->id, 
                        'month'             => $month,
                        'sku'               => $sku,
                        'min_qty'           => $min_qty
                    ]);
                }
            }
        }

        return redirect()->route('view_motor', $motor->id); 
    }

    public function editMotor()
    {
        $motor = Motor::findOrFail($this->request->id);
        $vehicle_brands = VehicleBrand::oldest('name')->get();

        $motor_oldest_year = $motor->vehicle_years()->oldest('year')->first()->year;
        $motor_newest_year = $motor->vehicle_years()->latest('year')->first()->year;

        return view('dashboard.edit_motor', compact('motor', 'vehicle_brands', 'motor_oldest_year', 'motor_newest_year'));
    }

    public function actionEditMotor()
    {
        $motor = Motor::findOrFail($this->request->id);

        $motor->code = $this->request->code;
        $motor->vehicle_brand_id = VehicleBrand::findOrFail($this->request->vehicle_brand)->id;
        $motor->name = $this->request->name;
        $motor->model = $this->request->model;
        $motor->type = $this->request->type;
        $motor->cc = $this->request->cc;
        $motor->different_size = ($this->request->different_tyre_sizes)? true : false;
        $motor->save();

        $motor->vehicle_years()->detach();

        for($i=$this->request->from_year;$i<=$this->request->until_year;$i++)
        {
            if(!$this->request->different_tyre_sizes)
            {
                $front_sizes = tyre_sizes($this->request->size_front);
                $rear_sizes = tyre_sizes($this->request->size_rear);
            }
            else
            {
                $front_sizes = tyre_sizes($this->request->custom_size_front[$i]);
                $rear_sizes = tyre_sizes($this->request->custom_size_front[$i]);
            }

            $motor->vehicle_years()->attach(VehicleYear::getByYear($i), [
                'tyre_front_width' => $front_sizes->width, 
                'tyre_front_height' => $front_sizes->height,
                'tyre_front_inch' => $front_sizes->inch,
                'tyre_rear_width' => $rear_sizes->width,
                'tyre_rear_height' => $rear_sizes->height,
                'tyre_rear_inch' => $rear_sizes->inch
            ]);
        }

        return redirect()->route('view_motor', $motor->id);
    }

    public function skuCheck()
    {
        $product = Product::where('sku', $this->request->sku)->first();

        if($product)
        {
            return array('status'   => true);
        }

        return array('status'   => false);
    }

    public function exportMotor()
    {
        $motor = Motor::findOrFail($this->request->id);
        $motor->oldest_year = $motor->vehicle_years()->oldest('year')->first()->year;
        $motor->motor_newest_year = $motor->vehicle_years()->latest('year')->first()->year;

        $subcategories = Shop::getShop('Motoria')->categories()->with(['parent', 'info'])->whereNotNull('parent_id')->get();

        return Excel::create('Filename', function($excel) use($motor, $subcategories) {
            $excel->setCreator('CMS')->setCompany('TTI B.V.');

            $header[] = 'ID';
            $header[] = 'Category';

            for($i=$motor->oldest_year;$i<=$motor->motor_newest_year;$i++)
            {
                $header[] = $i;
                $header[] = $i . "[month]";
                $header[] = $i . "[min_qty]";
            }

            $excel->sheet('Analyzer', function($sheet) use ($header, $motor, $subcategories){
                $sheet->row(1, $header);

                foreach($subcategories->sortBy('info.name') as $category)
                {
                    $row['id'] = $category->id;
                    $row['category'] = $category->info->name;

                    for($i=$motor->oldest_year;$i<=$motor->motor_newest_year;$i++)
                    {
                        $row[] = $motor->categories->where('id', $category->id)->where('pivot.vehicle_year_id', vehicle_year($i)->id)->first()->pivot->sku ?? '';
                        $row[] = $motor->categories->where('id', $category->id)->where('pivot.vehicle_year_id', vehicle_year($i)->id)->first()->pivot->month ?? '';
                        $row[] = $motor->categories->where('id', $category->id)->where('pivot.vehicle_year_id', vehicle_year($i)->id)->first()->pivot->min_qty ?? '';
                    }

                    $sheet->appendRow($row);

                    $row = array();
                }
            });

        })->download('xls');
    }
}