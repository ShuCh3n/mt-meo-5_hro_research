<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWarehousesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('warehouses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->boolean('third_party')->nullable()->default(false);
            $table->integer('deltime')->nullable()->default(0);
            $table->timestamps();
        });

        DB::table('warehouses')->insert([
            [
                'name' => 'TTI',
            ],
            [
                'name' => 'Motoria',
            ],
            [
                'name' => 'Third party',
            ]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('warehouses');
    }
}
