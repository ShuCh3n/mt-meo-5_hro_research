<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Shop;
use App\Product;
use App\Language;
use App\Supplier;
use App\Category;
use App\Customer;
use App\Order;
use App\CustomerAddress;
use App\Country;
use Storage;

use App\ProductFeature;

class DashboardController extends Controller
{
    private $_request;

    public function __construct(Request $request)
    {
        $this->_request = $request;
    }

    public function home()
    {
        return view('dashboard.home');
    }

    
    public function customersAddresses()
    {
        $customer_addresses = CustomerAddress::orderBy('created_at', 'desc')->paginate(100);
        $request = $this->_request;

        return view('dashboard.customers_addresses', compact('customer_addresses', 'request'));
    }    

    public function orders()
    {
        $orders = Order::orderBy('created_at', 'desc')->paginate(100);
        $request = $this->_request;

        return view('dashboard.orders', compact('orders', 'request'));
    }

    public function script()
    {
        $shop_products = \App\ShopProduct::where('shop_id', 3)->get();

        foreach($shop_products as $shop_product)
        {
            if($shop_product->product->product_price && $shop_product->product->product_price->retail_price > 0)
            {
                $retail_price = $shop_product->product->product_price->retail_price * 1.15;
                $shop_product->product->insertShopPrice($shop_product->shop, $retail_price);
                $shop_product->active = 1;
                $shop_product->save();
            }
        }
        dd('done');
        //FEATURE CLEANUP
        // $features = ProductFeature::with(['infos', 'options.infos', 'options.products'])->whereHas('info', function($query){
        //     $query->where('name', 'Dot');
        // })->where('id', '!=', 7)->get();
 
        // foreach($features as $feature)
        // {
        //     foreach($feature->options as $option)
        //     {
        //         foreach($option->infos as $option_info)
        //         {
        //             $option_info->delete();
        //         }
        //         $option->products()->detach();
        //         $option->delete();
        //     }
 
        //     foreach($feature->infos as $lang)
        //     {
        //         $lang->delete();
        //     }
 
        //     $feature->delete();
        // }
    }
}
