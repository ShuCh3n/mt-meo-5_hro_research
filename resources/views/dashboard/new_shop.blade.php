@extends('dashboard.layout.main')

@section('stylesheet')
    <link href="/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="/css/plugins/select2/select2.min.css" rel="stylesheet">

    <link href="/css/plugins/summernote/summernote.css" rel="stylesheet">
    <link href="/css/plugins/summernote/summernote-bs3.css" rel="stylesheet">

    <style>
        .changable .image {
            display: block;
            overflow: hidden;
            width: 200px;
            height: 150px;
        }
        .changable .image img {
            width: 100%;
        }
        .changable .after{
            color: #fff;
            cursor: pointer;
            display: none;
            padding-top: 75px;
            position: absolute;
            top: 0;
            width: 200px;
            height: 150px;
        }
        .changable:hover .after, .changable:hover .after {
            background: rgba(0, 0, 0, 0.5) none repeat scroll 0 0;
            display: block;
        }
        input[type="file"] {
            display: none;
        }
    </style>
@stop

@section('javascript')
    <!-- iCheck -->
    <script src="/js/plugins/iCheck/icheck.min.js"></script>

    <!-- Select2 -->
    <script src="/js/plugins/select2/select2.full.min.js"></script>

    <!-- SUMMERNOTE -->
    <script src="/js/plugins/summernote/summernote.min.js"></script>

    <script>
        $(function(){
            $(".select2").select2({
                placeholder: "Select"
            });

            $('.summernote').summernote();

            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });

            $(".select2.tax_groups").on("select2:select", function (e) { 
                $.get('/ajax/tax_group/' + $(this).val() + '/tax_rules', function(result){

                    $('.select2.tax_rules').html('');

                    for(var i = 0, len = result.length; i < len; i++)
                    {
                        $('.select2.tax_rules').append($("<option>").val(result[i].id).html(result[i].name));
                    }

                    $('.select2.tax_rules').select2();
                });
            });

             $("#logo-file-input").change(function(){
                if (this.files && this.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('#logo-img').attr('src', e.target.result);
                    }

                    reader.readAsDataURL(this.files[0]);
                }
            });
        });
    </script>
@stop

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>{{ trans('pages.inquiry_detail_header') }}</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('home') }}">{{ trans('menu.home') }}</a>
                </li>
                <li>
                    <a href="">{{ trans('menu.inquiries') }}</a>
                </li>
                <li class="active">
                    <strong>{{ trans('page.inquiry') }}</strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">
        {!! session()->has('success')? '<div class="alert alert-success alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>' . session('success') . '</div>' : '' !!}

        <form method="post" class="form-horizontal" enctype="multipart/form-data">
            <div class="row">
                <div class="col-md-8">
                    <div class="ibox">

                        <div class="ibox-title">
                            <h5>New Shop</h5>
                        </div>

                        <div class="ibox-content">
                            <div class="mb20"></div>

                            <div class="row">
                                <div class="col-sm-4 col-md-3">
                                    <div class="m-b-sm">
                                        <div class="row text-center">
                                            <div class="changable">
                                                <label for="logo-file-input">
                                                    <div class="image" v-else>
                                                        <img id="logo-img" src="" alt="image">
                                                        <div class="after">Change</div>
                                                    </div>
                                                </label>

                                                <input id="logo-file-input" type="file" name="logo" />
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="col-sm-8 col-md-9">
                                
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Name</label>
                                        <div class="col-md-10"><input type="text" class="form-control" name="name" placeholder="Shop Name"></div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Link</label>
                                        <div class="col-md-10"><input type="text" class="form-control" name="link" placeholder="Shop Link"></div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Default Tax</label>
                                        @if($tax_groups->count() > 0)
                                            <div class="col-md-5">
                                                <select class="select2 tax_groups form-control" name="tax_group">
                                                    <option value="">Select</option>
                                                    @foreach($tax_groups as $tax_group)
                                                        <option value="{{ $tax_group->id }}">{{ $tax_group->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-md-5">
                                                <select class="select2 tax_rules form-control" name="tax_rule">
                                                    <option value="">Select</option>
                                                </select>
                                            </div>
                                        @else
                                            <div class="col-md-10">
                                                <p class="form-control-static">
                                                    <a href="{{ route('new_tax_group') }}">Create first tax group</a>
                                                </p>
                                            </div>
                                        @endif
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="ibox">

                        <div class="ibox-title">
                            <h5>Pay.nl</h5>
                        </div>

                        <div class="ibox-content">
                            <div class="row">
                                <div class="col-md-12">
                                    
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Active<span class="text-danger">*</span></label>
                                        <div class="col-md-5">
                                            <label><input type="radio" class="form-control i-checks" name="paynl_active" value="1"> Enabled</label>
                                        </div>
                                        <div class="col-md-5">
                                            <label><input type="radio" class="form-control i-checks" name="paynl_active" value="0" checked> Disabled</label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Service ID</label>
                                        <div class="col-md-10"><input type="text" class="form-control" name="paynl_service_id" placeholder="Service ID" value=""></div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Token</label>
                                        <div class="col-md-10"><input type="text" class="form-control" name="paynl_api_token" placeholder="API Token" value=""></div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="ibox">

                        <div class="ibox-title">
                            <h5>Languages</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                            </div>
                        </div>

                        <div class="ibox-content" style="display: none;">
                            <div class="row">
                                <div class="col-md-12">
                                    <table class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th data-toggle="true">Language</th>
                                                <th class="text-right">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($languages as $language)
                                                <tr>
                                                    <td><img src="/img/flags/16/{{ $language->icon }}.png" alt="flag"> {{ $language->name }}</td>
                                                    <td>
                                                        <div class="switch pull-right">
                                                            <div class="onoffswitch">
                                                                <input type="checkbox"  class="onoffswitch-checkbox language" lang_id="{{ $language->id }}" id="activate_{{ $language->name }}">
                                                                <label class="onoffswitch-label" for="activate_{{ $language->name }}">
                                                                    <span class="onoffswitch-inner"></span>
                                                                    <span class="onoffswitch-switch"></span>
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="ibox">

                        <div class="ibox-title">
                            <h5>Currencies</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                            </div>
                        </div>

                        <div class="ibox-content" style="display: none;">
                            <div class="row">
                                <div class="col-md-12">
                                    <table class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th data-toggle="true">Currencies</th>
                                                <th class="text-right">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($currencies as $currency)
                                                <tr>
                                                    <td><img src="/img/flags/16/{{ $currency->icon }}.png" alt="flag"> {{ $currency->name }}</td>
                                                    <td>
                                                        <div class="switch pull-right">
                                                            <div class="onoffswitch">
                                                                <input type="checkbox" class="onoffswitch-checkbox currency" currency_id="{{ $currency->id }}" id="activate_{{ $currency->name }}">
                                                                <label class="onoffswitch-label" for="activate_{{ $currency->name }}">
                                                                    <span class="onoffswitch-inner"></span>
                                                                    <span class="onoffswitch-switch"></span>
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="ibox">

                        <div class="ibox-title">
                            <h5>Countries</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                            </div>
                        </div>

                        <div class="ibox-content" style="display: none;">
                            <div class="row">
                                <div class="col-md-12">
                                    <table class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th data-toggle="true">Country</th>
                                                <th class="text-right">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($countries as $country)
                                                <tr>
                                                    <td><img src="/img/flags/16/{{ $country->icon }}.png" alt="flag"> {{ $country->name }}</td>
                                                    <td>
                                                        <div class="switch pull-right">
                                                            <div class="onoffswitch">
                                                                <input type="checkbox" checked class="onoffswitch-checkbox" id="activate_{{ $country->name }}">
                                                                <label class="onoffswitch-label" for="activate_{{ $country->name }}">
                                                                    <span class="onoffswitch-inner"></span>
                                                                    <span class="onoffswitch-switch"></span>
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>

                                </div>
                            </div>
                        </div>
                    </div>

                    
                </div>

                <div class="col-md-4">
                    <div class="ibox">
                        <div class="ibox-title">
                            <h5>Action</h5>
                        </div>

                        <div class="ibox-content">
                            {{ csrf_field() }}
                            <button class="btn btn-block btn-outline btn-success" type="submit"><i class="fa fa-floppy-o"></i> Save</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>

        <div class="mb30"></div>
    </div>
@stop