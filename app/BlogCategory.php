<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlogCategory extends Model
{
    public function infos()
    {
    	return $this->hasMany(BlogCategoryInfo::class);
    }

    public function info()
    {
    	return $this->hasOne(BlogCategoryInfo::class);
    }
}
