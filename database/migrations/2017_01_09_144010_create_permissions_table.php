<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePermissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permissions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('parent_id')->unsigned()->nullable();
            $table->string('name');
            $table->timestamps();

            $table->foreign('parent_id')
                ->references('id')
                ->on('permissions')
                ->onDelete('cascade');
        });

        Schema::create('permission_languages', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('permission_id')->unsigned();
            $table->integer('language_id')->unsigned();
            $table->string('name');
            $table->timestamps();

            $table->foreign('permission_id')
                ->references('id')
                ->on('permissions')
                ->onDelete('cascade');

            $table->foreign('language_id')
                ->references('id')
                ->on('languages')
                ->onDelete('cascade');
        });

        DB::table('permissions')->insert([
            [
                "parent_id" => null,
                "name"  => "orders"
            ],
            [
                "parent_id" => 1,
                "name"  => "orders"
            ],
            [
                "parent_id" => 1,
                "name"  => "invoices"
            ],
            [
                "parent_id" => 1,
                "name"  => "credit_slips"
            ],
            [
                "parent_id" => 1,
                "name"  => "delivery_slips"
            ],
            [
                "parent_id" => 1,
                "name"  => "shopping_carts"
            ]
        ]);

        DB::table('permission_languages')->insert([
            [
                "permission_id" => 1,
                "language_id"   => 2,
                "name"          => "Bestellingen"
            ],
            [
                "permission_id" => 2,
                "language_id"   => 2,
                "name"          => "Bestellingen"
            ],
            [
                "permission_id" => 3,
                "language_id"   => 2,
                "name"          => "Facturen"
            ],
            [
                "permission_id" => 4,
                "language_id"   => 2,
                "name"          => "Kredieten"
            ],
            [
                "permission_id" => 5,
                "language_id"   => 2,
                "name"          => "Afleveringen"
            ],
            [
                "permission_id" => 6,
                "language_id"   => 2,
                "name"          => "Winkelwagens"
            ],
        ]);

        Schema::create('permission_user', function (Blueprint $table) {
            $table->integer('permission_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->json('options');

            $table->foreign('permission_id')
                ->references('id')
                ->on('permissions')
                ->onDelete('cascade');

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->primary(['permission_id', 'user_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('permission_user');
        Schema::dropIfExists('permission_languages');
        Schema::dropIfExists('permissions');
    }
}
