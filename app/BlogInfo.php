<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlogInfo extends Model
{
    public function scopeLanguage($query, $language_id)
    {
    	$query->where('language_id', $language_id);
    }
}
