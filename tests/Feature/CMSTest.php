<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CMSTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testHomeTest()
    {
        $response = $this->get('/');
        $response->assertStatus(302);
        $response->assertRedirect('login');

        $this->be(\Auth::loginUsingId(1));

        $response = $this->get('/');
        $response->assertStatus(200);

        $response = $this->get('brand/edit/'.\App\Brand::inRandomOrder()->first()->id);
        $response->assertStatus(200);

        $response = $this->get('brand/new');
        $response->assertStatus(200);

        $response = $this->get('brands');
        $response->assertStatus(200);

        $response = $this->get('categories');
        $response->assertStatus(200);

        $response = $this->get('categories/shop/'.\App\Shop::inRandomOrder()->first()->id);
        $response->assertStatus(200);

        $shop = \App\Shop::inRandomOrder()->first();
        if($shop->categories()->first()){
            $response = $this->get('categories/shop/'.$shop->id.'/category/'.$shop->categories()->first()->id);
            $response->assertStatus(200);
        }

        $response = $this->get('category/new');
        $response->assertStatus(200);

        $response = $this->get('cms/blogs');
        $response->assertStatus(200);

        $b = \App\Blog::inRandomOrder()->first();
        $bp = $b->blogposts()->inRandomOrder()->first();
        $response = $this->get('cms/blog/'.$b->id.'/article/'.$bp->id.'/edit');
        $response->assertStatus(200);
        
    }

    public function testApiTest()
    {
        $shop = \App\Shop::inRandomOrder()->first();

        $headers = ["ShopToken" => $shop->key, "Accept-Language" => "application/json", "Language" => "nl"];
        $response = $this->getJson('api/v1/blogs/', $headers);
        if($response->status() == 200){
            $response->assertStatus(200);
        }else{
            $response->assertStatus(404);
        }

        $response = $this->getJson('api/v1/brand/'.$shop->brands()->first()->id, $headers);
        $response->assertStatus(200);

        $response = $this->getJson('api/v1/cart', $headers);
        $response->assertStatus(200);

        $response = $this->getJson('api/v1/categories', $headers);
        $response->assertStatus(200);

        $response = $this->getJson('api/v1/products', $headers);
        $response->assertStatus(200);

        $response = $this->getJson('api/v1/products/featured', $headers);
        $response->assertStatus(200);

        $response = $this->getJson('api/v1/products/most-sold', $headers);
        $response->assertStatus(200);

        $response = $this->getJson('api/v1/products/offers', $headers);
        $response->assertStatus(200);

        $response = $this->getJson('api/v1/shop', $headers);
        $response->assertStatus(200);        
    }
}
