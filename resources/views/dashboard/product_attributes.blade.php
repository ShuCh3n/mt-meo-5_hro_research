@extends('dashboard.layout.main')

@section('content')
	<div class="wrapper wrapper-content animated fadeInRight ecommerce">
            <div class="ibox-content m-b-sm border-bottom">
                <div class="row">
                    <div class="col-xs-12">
                        <a class="btn btn-primary pull-right" href="{{ route('new_product_attribute') }}"><i class="fa fa-plus-circle" aria-hidden="true"></i> New Attribute</a>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox">
                        <div class="ibox-content">

                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th data-toggle="true">
                                            <a href="">
                                                Attribute
                                            </a>
                                        </th>
                                        <th class="text-right" data-sort-ignore="true">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($attributes as $attribute)
                                        <tr>
                                            <td>
                                               <a href="{{ route('product_attribute_options', $attribute->id) }}">{{ $attribute->multilanguage->info()->name }}</a>
                                            </td>
                                            <td class="text-right">
                                                <div class="btn-group">
                                                    <a href="{{ route('edit_product_attribute', $attribute->id) }}" class="btn-white btn btn-xs">Edit</a>
                                                    <a href="{{ route('remove_product_attribute', $attribute->id) }}" class="btn-white btn btn-xs">Delete</a>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@stop