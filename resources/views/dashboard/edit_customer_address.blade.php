@extends('dashboard.layout.main')

@section('stylesheet')
    <link href="/css/plugins/iCheck/custom.css" rel="stylesheet">
@stop

@section('javascript')
    <!-- iCheck -->
    <script src="/js/plugins/iCheck/icheck.min.js"></script>

    <script>
        $(function(){
            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });
        });
    </script>
@stop

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>{{ trans('pages.inquiry_detail_header') }}</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('home') }}">{{ trans('menu.home') }}</a>
                </li>
                <li>
                    <a href="">{{ trans('menu.inquiries') }}</a>
                </li>
                <li class="active">
                    <strong>{{ trans('page.inquiry') }}</strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">
        {!! session()->has('success')? '<div class="alert alert-success alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>' . session('success') . '</div>' : '' !!}

        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <form id="profileForm" method="post" class="form-horizontal">
            <div class="row">
                <div class="col-md-8">
                    <div class="ibox">

                        <div class="ibox-title">
                            <h5>New Address</h5>
                        </div>

                        <div class="ibox-content">
                            <div class="mb20"></div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Customer</label>
                                        <div class="col-md-10">
                                            <p class="form-control-static">
                                                <a href="{{ route('view_customer', $customer_address->customer->id) }}">{{ $customer_address->customer->firstname }} {{ $customer_address->customer->lastname }}</a>
                                            </p>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Name</label>
                                        <div class="col-md-10"><input type="text" class="form-control" name="name" placeholder="Address Label Name" value="{{ $customer_address->alias }}"></div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Default Address</label>
                                        <div class="col-md-5">
                                            <label><input type="radio" class="form-control i-checks" name="default_address" value="1" {{ ($customer_address->default == 1)? "checked" : "" }}> Yes</label>
                                        </div>
                                        <div class="mb5-sm"></div>
                                        <div class="col-md-5">
                                            <label><input type="radio" class="form-control i-checks" name="default_address" value="0" {{ ($customer_address->default == 0)? "checked" : "" }}> No</label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Company</label>
                                        <div class="col-md-10">
                                            <input type="text" class="form-control" name="company" placeholder="Company" value="{{ $customer_address->company }}">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Name</label>
                                        <div class="col-md-5">
                                            <input type="text" class="form-control" name="firstname" placeholder="Firstname" value="{{ $customer_address->firstname }}">
                                        </div>
                                        <div class="mb5-sm"></div>
                                        <div class="col-md-5">
                                            <input type="text" class="form-control" name="lastname" placeholder="Lastname" value="{{ $customer_address->lastname }}">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Phonenumber</label>
                                        <div class="col-md-5">
                                            <input type="text" class="form-control" name="mobile_phone" placeholder="Mobile Phone" value="{{ $customer_address->mobile_phone }}">
                                        </div>
                                        <div class="mb5-sm"></div>
                                        <div class="col-md-5">
                                            <input type="text" class="form-control" name="home_phone" placeholder="Home Phone" value="{{ $customer_address->home_phone }}">
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="ibox">

                        <div class="ibox-title">
                            <h5>Shipping Address</h5>
                        </div>

                        <div class="ibox-content">
                            <div class="mb20"></div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Street</label>
                                        <div class="col-md-5">
                                            <input type="text" class="form-control" name="street" placeholder="Street" value="{{ $customer_address->street }}">
                                        </div>
                                        <div class="mb5-sm"></div>
                                        <div class="col-md-5">
                                            <input type="text" class="form-control" name="housenumber" placeholder="Housenumber" value="{{ $customer_address->housenumber }}">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">City / Zipcode</label>
                                        <div class="col-md-5">
                                            <input type="text" class="form-control" name="city" placeholder="City" value="{{ $customer_address->city }}">
                                        </div>
                                        <div class="mb5-sm"></div>
                                        <div class="col-md-5">
                                            <input type="text" class="form-control" name="zipcode" placeholder="Zipcode" value="{{ $customer_address->zipcode }}">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Country</label>
                                        <div class="col-md-10">
                                            <select class="form-control countries-select" name="country">
                                                <option value=""></option>
                                                @foreach($countries as $country)
                                                    <option value="{{ $country->id }}" icon="{{ $country->icon }}" {{ ($customer_address->country_id == $country->id)? 'selected' : '' }}>{{ $country->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">State</label>
                                        <div class="col-md-10">
                                            <input type="text" class="form-control" name="state" placeholder="State" value="{{ $customer_address->state }}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="ibox">
                        <div class="ibox-title">
                            <h5>Action</h5>
                        </div>

                        <div class="ibox-content">
                            {{ csrf_field() }}
                            <button class="btn btn-block btn-outline btn-success" type="submit"><i class="fa fa-floppy-o"></i> Save</button>
                            <button class="btn btn-block btn-outline btn-danger" type="submit"><i class="fa fa-trash-o"></i> Delete</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>

        <div class="mb30"></div>
    </div>
@stop