from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm import joinedload
from sqlalchemy import *
from datetime import *
import sys
import os
import CSVMinion as csv

sys.path.append('..')
import tti_scheme
sys.path.remove('..')

class TTI():
	def __init__(self):
		self.engine = create_engine('mysql://root:root@127.0.0.1/webshoptti_datab')
		Session = sessionmaker(bind=self.engine)
		self.session = Session()
		self.csv = csv.CSVMinion('export')
		self.syncFiles()

	def syncFiles(self):

		for file in os.listdir("export"):
			if file != 'full.csv':
				data = self.csv.getData(',', file)

				for index, row in data.iterrows():
					product = self.session.query(tti_scheme.Artikelen).filter_by(artikel = row['ArticleCode']).first()

					if product:
						if product.voorraad == 0:
							product.vrd3 = 1
							product.av_vrd3 = row['Stock']
							product.av_vrd_prijs = row['PricePP']

							self.session.commit()

							print(row['ArticleCode'] + ' updated')
