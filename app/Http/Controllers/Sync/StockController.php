<?php

namespace App\Http\Controllers\Sync;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Product;
use App\Warehouse;
use App\ProductStock;

class StockController extends Controller
{
	 public function stock(){
        $product = Product::where('sku', $this->request->sku)->firstOrFail();

        foreach($this->request->warehouses as $warehouseInfo)
        {
            $warehouse = Warehouse::firstOrNew(['name' => $warehouseInfo['name']]);
            $warehouse->third_party = $warehouseInfo['third_party'];
            $warehouse->deltime = $warehouseInfo['del_time'];
            $warehouse->save();

            ProductStock::where('product_id', $product->id)->where('warehouse_id', $warehouse->id)->delete();

            $product_stock = new ProductStock;
            $product_stock->warehouse_id = $warehouse->id;
            $product_stock->product_id = $product->id;
            $product_stock->amount = $warehouseInfo['amount'];
            $product_stock->price = $warehouseInfo['price'];
            $product_stock->save();
        }
        
        return $product->product_stocks;
    }
}