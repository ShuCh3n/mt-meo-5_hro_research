@extends('dashboard.layout.main')

@section('content')
	<div class="wrapper wrapper-content animated fadeInRight ecommerce">
            <div class="ibox-content m-b-sm border-bottom">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="btn-group pull-right">
                            <a class="btn btn-primary" href="{{ route('new_product_attribute') }}"><i class="fa fa-plus-circle" aria-hidden="true"></i> New Attribute</a>
                            <a class="btn btn-warning pull-right" href="{{ route('new_product_attribute_option', ["attribute" => $attribute->id]) }}"><i class="fa fa-plus-circle" aria-hidden="true"></i> New Option</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox">
                        <div class="ibox-content">

                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th data-toggle="true">
                                            <a href="">
                                                Attribute Option
                                            </a>
                                        </th>
                                        <th class="text-right" data-sort-ignore="true">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($attribute->options as $option)
                                        <tr>
                                            <td>
                                               {{ $option->multilanguage->info()->name }}
                                            </td>
                                            <td class="text-right">
                                                <div class="btn-group">
                                                    <a href="{{ route('edit_product_attribute_option', [$attribute->id, $option->id]) }}" class="btn-white btn btn-xs">Edit</a>
                                                    <a href="{{ route('remove_product_attribute_option', [$attribute->id, $option->id]) }}" class="btn-white btn btn-xs">Delete</a>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@stop