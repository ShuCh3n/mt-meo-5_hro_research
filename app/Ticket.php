<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\TicketMessage;

class Ticket extends Model
{
    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function ticket_department()
    {
        return $this->belongsTo(TicketDepartment::class);
    }

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    public function ticket_messages()
    {
        return $this->hasMany(TicketMessage::class);
    }

	public function scopeClosed($query)
    {
    	$query->where("closed", true);
    }

    public function scopeNotClosed($query)
    {
    	$query->where("closed", false);
    }

    public function addMessage($request, $customer_message = false)
    {
    	$message = new TicketMessage;
    	$message->ticket_id = $this->id;
    	$message->customer = $customer_message;
    	$message->message = $request->message;
    	$message->ip = $request->ip();

    	if(!$customer_message)
    	{
    		$message->user_id = auth()->user()->id;
    	}

    	$message->save();
    }

    public function isUnread()
    {
        if($this->ticket_messages()->where('customer', true)->where('read', false)->first())
        {
            return true;
        }

        return false;
    }

    public function markedReadMessage($customer = true)
    {
        $messages = $this->ticket_messages()->where('customer', $customer)->where('read', false)->get();

        foreach($messages as $message)
        {
            $message->read = true;
            $message->save();
        }

        return $this;
    }

    public function close()
    {
        $this->closed = true;
        $this->save();
    }
}
