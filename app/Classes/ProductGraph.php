<?php

namespace App\Classes;
use App\Platform;

class ProductGraph {

	private $months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
	private $labels = [];

	public function __construct($years, $product, $platforms = [])
	{
		$this->years = collect($years);
		$this->product = $product;
		$this->platforms = $platforms;

		$this->labels();
	}

	private function labels()
	{
		foreach($this->years as $year)
		{
			foreach($this->months as $month)
	        {
	            $this->labels[] = $year . ' ' . $month;
	        }
	    }

	    return $this;
	}

	private function reorder_prices($prices, $meta = "")
	{
		$serie = [];

		foreach($this->years as $year)
		{
			for($i=1;$i<=12;$i++)
	        {
	            $start = carbon()->year($year)->month($i)->startOfMonth();
	            $end = carbon()->year($year)->month($i)->endOfMonth();

	            $results = $prices->where('created_at', '>', $start)->where('created_at', '<', $end);
	            $price = 0;

	            if($results->isNotEmpty())
	            {
	            	$price = $results->avg('price');
	            }

	            $serie[] = ["meta" => $meta, "value" => round($price, 2)];
	        }
	    }

	    return $serie;
	}

	public function product_prices()
	{
		return [
			[
				"meta"	=> "Product",
				"value"	=> 1
			],
			[
				"meta"	=> "Product",
				"value"	=> 2
			],
			[
				"meta"	=> "Product",
				"value"	=> 3
			],
			[
				"meta"	=> "Product",
				"value"	=> 4
			],
			[
				"meta"	=> "Product",
				"value"	=> 5
			],
		];
	}

	public function platform_prices($id)
	{
		$platform = Platform::find($id);

		$prices = $this->product->platform_product_prices()
			->where('platform_id', $id)
			->whereBetween('created_at', [carbon()->year($this->years->first())->startOfYear(), carbon()->year($this->years->first())->endOfYear()])
			->get();

		return $this->reorder_prices($prices, $platform->name);
	}

	public function series()
	{
		$series = [];

		$series[] = $this->product_prices();

		if($this->platforms)
		{
			foreach($this->platforms as $platform_id)
			{
				$series[] = $this->platform_prices($platform_id);
			}
		}

	    return $series;
	}

	public function result()
	{
        return array(
            "labels" => $this->labels,
            "series" => $this->series()
        );
	}
}