<?php

namespace App\Classes\iBox;

use App\Product;
use App\Platform;
use App\PlatformProductPrice;
use App\Classes\iBox\Product as iBoxProduct;
use Excel;

class Analyzer
{
	public $file;
	public $items;
	public $platforms;

	public function __construct($analist_file, $formula, $eq_products, $segments)
	{
		$this->analist_file = $analist_file;
		$this->file_path = 'storage/app/ibox/analist/excel/' . $analist_file->filename;
		$this->formula = $formula;
		$this->eq_products = $eq_products;
		$this->segments = $segments;
		$this->platforms = collect();

		$this->loadFile();
		$this->mergeSheet();
		$this->analyze();
	}

	private function loadFile()
	{
		$this->file = Excel::load($this->file_path)->select(['ean_code', 'prod_code', 'article_code', 'price', 'description', 'currency', 'qty'])->limit(2000)->get();
		return $this;
	}

	private function mergeSheet()
	{
		$classname = explode("\\", get_class($this->file));
        $classname = end($classname);

        if($classname == "SheetCollection")
        {
            $items = array();

            foreach($this->file as $sheet)
            {
                foreach($sheet as $row)
                {
                    $items[] = $row;
                }
            }

            $this->items = $item;

            return $this;
        }
        
        $this->items = $this->file;

        return $this;
	}

	private function analyzable($item)
	{
		if(!isset($item->price))
        {
            return false;
        }

        if(!isset($item->ean_code) && !isset($item->prod_code) && !isset($item->article_code))
        {
            return false;
        }

		return true;
	}

	private function findProduct($item)
	{
		$item->product = false;

		if($item->prod_code)
        {
            $item->product = Product::where("supplier_sku", $item->prod_code)->first();
        }

        if($item->ean_code && !$item->product)
        {
            $item->product = Product::where("ean", $item->ean_code)->first();
        }

        if($item->article_code && !$item->product)
        {
            $item->product = Product::where("sku", $item->article_code)->first();
        }

		return $this;
	}

	public function appendPlatform($platform)
	{
		if(!$this->platforms->where('id', $platform->id)->first())
		{
			$this->platforms[] = $platform;
		}

		return $this;
	}

	private function getCheapestPrice($item)
	{
		return $item->platforms->sortBy('price')->first();
	}

	private function rePercentage($value)
    {
        return (100 - $value) / 100;
    }

    private function calculateTargetPrice($item, $tyre24)
    {
    	$item->target_price = ($this->formula['t24_fee'])? $tyre24->price + $this->formula['t24_fee_value'] : $tyre24->price - $this->formula['t24_fee_value'];
        $item->target_price = ($this->formula['margin'])? $item->target_price * $this->rePercentage($this->formula['margin_value']) : $item->target_price / $this->rePercentage($this->formula['t24_fee_value']);

        if($this->formula['import_fee_value'] > 0)
        {
            $item->target_price = ($this->formula['import_fee'])? $item->target_price * $this->rePercentage($this->formula['import_fee_value']) : $item->target_price / $this->rePercentage($this->formula['import_fee_value']);
        }

        return $this;
    }

	private function dealOrNoDeal($item)
	{
		//Tyre24 ID = 14
		$tyre24 = $item->product->platform_product_price()->platform(14)->latest()->first();

		if($tyre24)
		{
			$this->calculateTargetPrice($item, $tyre24);
            
            if($item->target_price > $item->price)
            {
            	return 1;
            }

            return 2;
		}

		return 3;
	}

	private function percentageDifference($item)
    {
    	if($item->deal == 1 || $item->deal == 2)
    	{
    		$difference = (($item->price - $item->target_price) / $item->target_price) * 100;
        	return number_format(abs($difference), 2);
    	}
        
        return null;
    }

	private function analyseProduct($item)
	{
		$item->platforms = collect();
		
		foreach($item->product->platforms as $platform)
		{
			$this->appendPlatform($platform);
			$platform->price = $item->product->platform_product_price()->platform($platform->id)->latest()->first()->price ?? null;
			$item->platforms[] = $platform;
		}

		$item->cheapest_price = $this->getCheapestPrice($item);
		$item->deal = $this->dealOrNoDeal($item);
		$item->percentage_difference = $this->percentageDifference($item);
	}

	private function analyze()
	{
		foreach($this->items as $item)
        {
            if($this->analyzable($item))
            {
            	$this->findProduct($item);

            	if($item->product)
            	{
            		$this->analyseProduct($item);
            	}
            }
        }

        return $this;
	}

	private function filterPlatforms()
	{
		$this->filtered_platforms = $this->platforms->filter(function ($value, $key) {
			if($value->name != '07zr' && $value->name != 'Tyre24' )
			{
				return $value;
			}
		});

		return $this;
	}

	private function excelHeaders()
	{
		$header = array();
		$header[] = 'EAN';
        $header[] = 'Product Code';
        $header[] = 'Article Code';
        $header[] = 'Name';
        $header[] = 'Supplier Price';
        $header[] = 'Supplier QTY';
        $header[] = 'Difference';
        $header[] = 'Target';
        $header[] = 'Tyre24';
        $header[] = '07zr';
        $header[] = 'Cheapest';

        foreach($this->filtered_platforms as $platform)
        {
        	$header[] = $platform->name;
        }

        $header[] = 'TTI';
        $header[] = 'Purchasing Price';
        $header[] = 'Price Status';

        return $header;
	}

	private function excelRow($item)
	{
		$row = array();
		$row["ean"] = $item->product->ean ?? '-';
        $row["product_code"] = $item->product->supplier_sku ?? '-';
        $row["article_code"] = $item->product->sku ?? '-';
        $row["name"] = $item->name ?? $item->product->info->name;
        $row["supplier_price"] = price($item->price ?? 0);
        $row["supplier_qty"] = $item->qty;
        $row["difference"] = ($item->percentage_difference)? $item->percentage_difference . '%'  : '-';
        $row["target"] = price($item->target_price ?? 0);
        $row["tyre24"] = price($item->platforms->where('name', 'Tyre24')->first()->price ?? 0);
        $row["07zr"] = price($item->platforms->where('name', '07zr')->first()->price ?? 0);
        $row["cheapest"] = price($item->cheapest_price->price ?? 0);

		foreach($this->filtered_platforms as $platform)
		{
			$row[$platform->name] = price($item->platforms->where('id', $platform->id)->first()->price ?? 0);
		}

		$row["tti"] = price($item->product->product_price()->latest()->first()->retail_price ?? 0);
		$row["purchasing_price"] = price($item->product->product_price()->latest()->first()->purchasing_price ?? 0);

		switch ($item->deal) {
			case 1:
				$row["status"] = 'Deal';
				break;
			case 2:
				$row["status"] = 'No Deal';
				break;
			case 3:
				$row["status"] = 'No Info';
				break;
		}

		return $row;
	}

	public function resultInExcel()
	{
		$filename = pathinfo(carbon()->format('Y-m-d_H:i:s') . '_' . $this->analist_file->filename, PATHINFO_FILENAME);

		$this->filterPlatforms();

		return Excel::create($filename, function($excel){
            $excel->setCreator('Internet Box')->setCompany('TTI B.V.');

            $excel->sheet('Analyzer', function($sheet){
                $sheet->row(1, $this->excelHeaders());

                foreach($this->items as $item)
                {
					$sheet->appendRow($this->excelRow($item));

                    // if(isset($result_product["equivalent_products"]))
                    // {
                    //     foreach($result_product["equivalent_products"] as $equivalent)
                    //     {
                    //         $equivalent_product = new AnalyzerProduct();
                    //         $equivalent_product->product = $equivalent;
                    //         $equivalent_product->ean = $equivalent->product->ean;
                    //         $equivalent_product->product_code = $equivalent->product->product_code;
                    //         $equivalent_product->article_code = $equivalent->product->articleCode();
                    //         $equivalent_product->name = $equivalent->product->name;
                    //         $equivalent_product->currency_symbol = ($result_product["currency_symbol"])? $result_product["currency_symbol"] : false;
                    //         $equivalent_product->original_price = ($result_product["original_price"])? $result_product["original_price"] : false;
                    //         $equivalent_product->supplier_price = $result_product["supplier_price"];
                    //         $equivalent_product->supplier_qty = $result_product["supplier_qty"];
                    //         $equivalent_product->tyre24 = $equivalent->product->shopPrice(Shop::getShopByName("Tyre24")->id);
                    //         $equivalent_product->zero7zr = $equivalent->product->shopPrice(Shop::getShopByName("07zr")->id);
                    //         $equivalent_product->shops = $available_shops;
                            
                    //         $equivalent_row = $this->getExcelBaseColumn($equivalent_product);

                    //         $equivalent_row["tti_price"] = ($equivalent->product->shopPrice(Shop::getShopByName("TTI")->id))? "€ " . price($equivalent->product->shopPrice(Shop::getShopByName("TTI")->id)) : "-";

                    //         $equivalent_row["purchase_price"] = "-";
                    //         $equivalent_row["dealornodeal"] = "-";

                    //         $sheet->appendRow($equivalent_row);

                    //         $sheet->row($sheet->getHighestRow(), function($row) {
                    //             $row->setBackground('#0E4585')->setFontColor('#ffffff');
                    //         });
                    //     }
                    // }
                }
            });

        })->download('xlsx');
	}
}