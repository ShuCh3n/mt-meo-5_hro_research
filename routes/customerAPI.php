<?php

use Illuminate\Http\Request;

Route::group(['middleware' => 'customerAPI', 'as' => 'customerAPI.'], function () {
	Route::get('health', ["uses" => "HealthCheck@check"]);
	
	Route::get('product/{id}', ["uses" => "ProductController@getProduct"]);
	Route::post('products/search', ["uses" => "ProductController@searchProduct"]);

	Route::get('categories', ["uses" => "CategoryController@categories"]);
	Route::get('category/{id}/products', ["uses" => "CategoryController@categoryProducts"]);

    Route::get('shop', ["uses" => "ShopController@getShop"]);

    Route::get('vehicle/{vehicle}', ["uses" => "VehicleController@vehicle"]);
    Route::get('vehicle/{vehicle}/{type}', ["uses" => "VehicleController@vehicle"]);
    Route::get('vehicle/{vehicle}/{type}/{id}', ["uses" => "VehicleController@vehicle"]);
});
