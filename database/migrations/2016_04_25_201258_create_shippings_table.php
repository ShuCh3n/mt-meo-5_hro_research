<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShippingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shippings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('logo');
            $table->string('av_code');
            $table->decimal('base_fee', 10, 2);
            $table->timestamps();
        });

        Schema::create('shipping_infos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('shipping_id')->unsigned();
            $table->integer('language_id')->unsigned();
            $table->string('name');
            $table->text('description');
            $table->timestamps();

            $table->foreign('shipping_id')
                ->references('id')
                ->on('shippings')
                ->onDelete('cascade');

            $table->foreign('language_id')
                ->references('id')
                ->on('languages')
                ->onDelete('cascade');
        });


        Schema::create('shipping_shop', function (Blueprint $table) {
            $table->integer('shipping_id')->unsigned();
            $table->integer('shop_id')->unsigned();

            $table->foreign('shipping_id')
                ->references('id')
                ->on('shippings')
                ->onDelete('cascade');

            $table->foreign('shop_id')
                ->references('id')
                ->on('shops')
                ->onDelete('cascade');

            $table->primary(['shipping_id', 'shop_id']);
        });

        Schema::create('shipping_rules', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('shipping_id')->unsigned();
            $table->integer('country_id')->unsigned();
            $table->decimal('from_price', 10, 2)->nullable();
            $table->decimal('till_price', 10, 2)->nullable();
            $table->decimal('additional_fee', 10, 2)->nullable();
            $table->boolean('free_shipping')->nullable();

            $table->foreign('shipping_id')
                ->references('id')
                ->on('shippings')
                ->onDelete('cascade');

            $table->foreign('country_id')
                ->references('id')
                ->on('countries')
                ->onDelete('cascade');
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('shipping_rules');
        Schema::drop('shipping_shop');
        Schema::drop('shipping_infos');
        Schema::drop('shippings');
    }
}
