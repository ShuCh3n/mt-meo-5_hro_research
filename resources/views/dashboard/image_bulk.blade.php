@extends('dashboard.layout.main')

@section('content')
      <h2>Image bulk uploads</h2>
	<form action="{{route('uploadImageBulkAction')}}"
      class="dropzone">
      <select name="selectedshops[]" id="selectedshop" class="selectpicker" multiple data-live-search="true">
        @foreach ($shops as $shop)
        <option value="{{$shop->id}}">{{$shop->name}}</option>
        @endforeach
      </select>
            <input name="file" type="file" multiple />
            {{ csrf_field() }}
      </form>
@stop
