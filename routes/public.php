<?php

use Illuminate\Http\Request;

Route::group(['middleware' => 'public', 'as' => 'public.'], function () {
    Route::get('health', ["uses" => "HealthCheck@check"]);
    Route::get('thumbnail/{sku}', ["uses" => "PublicController@getThumbnail"]);
    Route::get('thumbnail/{sku}/shop/{shop_id}', ["uses" => "PublicController@getThumbnail"]);
    Route::get('image/{sku}', ["uses" => "PublicController@getImage"]);
    Route::get('image/{sku}/shop/{shop_id}', ["uses" => "PublicController@getImage"]);
    Route::get('image/{sku}/shop/{shop_id}/available', ["uses" => "PublicController@getImageAvailability"]);
});
