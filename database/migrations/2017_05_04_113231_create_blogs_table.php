<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blog_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('cover_image')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('blog_category_infos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('language_id')->unsigned();
            $table->integer('blog_category_id')->unsigned();
            $table->string('name');
            $table->timestamps();

            $table->foreign('language_id')
                ->references('id')
                ->on('languages')
                ->onDelete('cascade');

            $table->foreign('blog_category_id')
                ->references('id')
                ->on('blog_categories')
                ->onDelete('cascade');
        });

        Schema::create('blogs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('shop_id')->unsigned();
            $table->integer('blog_category_id')->unsigned();

            $table->string('cover_image')->nullable();

            $table->boolean('active');
            $table->dateTime('published_at');
            $table->softDeletes();
            $table->timestamps();
            
            $table->foreign('shop_id')
                ->references('id')
                ->on('shops')
                ->onDelete('cascade');

            $table->foreign('blog_category_id')
                ->references('id')
                ->on('blog_categories')
                ->onDelete('cascade');
        });

        Schema::create('blog_infos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('language_id')->unsigned();
            $table->integer('blog_id')->unsigned();
            $table->string('title');
            $table->text('short');
            $table->text('body');
            $table->timestamps();

            $table->foreign('language_id')
                ->references('id')
                ->on('languages')
                ->onDelete('cascade');

            $table->foreign('blog_id')
                ->references('id')
                ->on('blogs')
                ->onDelete('cascade');
        });

        Schema::create('blog_tags', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
        });

        Schema::create('blog_post_blog_tag', function (Blueprint $table) {
            $table->integer('blog_id')->unsigned();
            $table->integer('blog_tag_id')->unsigned();

            $table->foreign('blog_id')
                ->references('id')
                ->on('blogs')
                ->onDelete('cascade');

            $table->foreign('blog_tag_id')
                ->references('id')
                ->on('blog_tags')
                ->onDelete('cascade');

            $table->primary(['blog_id', 'blog_tag_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blog_post_blog_tag');
        Schema::dropIfExists('blog_tags');
        Schema::dropIfExists('blog_infos');
        Schema::dropIfExists('blogs');
        Schema::dropIfExists('blog_category_infos');
        Schema::dropIfExists('blog_categories');
    }
}
