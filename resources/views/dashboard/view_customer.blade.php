@extends('dashboard.layout.main')

@section('stylesheet')
<style>
	.profile_picture .after,
	.passport_picture .after {
	    position: absolute;
	    top: 0;
	    width: 120px;
	    height: 120px;
	    display: none;
	    color: #FFF;
	    cursor: pointer;
	    padding-top: 50px;
	}
	.profile_picture:hover .after,
	.passport_picture:hover .after{
	  display: block;
	  background: rgba(0,0,0,0.5);
	}
	.vue_upload > input{
	  display: none;
	}
	.profile_picture .picture{
	  width: 120px;
	  height: 120px;
	  border-radius: 50%;
	  overflow: hidden;
	  display: block;
	}
	.profile_picture .picture img{
	  height: 120px;
	}
</style>
@stop

@section('javascript')
    <script>
      var map;
      function initMap() {
        map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: -34.397, lng: 150.644},
          zoom: 8
        });
      }
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBMbFNHoBSbK7mqpQU_hvytLZk4BhXdo0Q&callback=initMap" async defer></script>
@stop

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>{{ trans('pages.student_header') }}</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ URL::route('home') }}">{{ trans('menu.home') }}</a>
                </li>
                <li>
                    <a href="{{ URL::route('customers') }}">{{ trans('menu.students') }}</a>
                </li>
                <li class="active">
                    <strong>{{ $customer->firstname . ' ' . $customer->lastname }}</strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="form-horizontal">

            <div class="row">
                <div class="col-md-8">
                    <div class="ibox">

                        <div class="ibox-title">
                            <h5>#{{ $customer->id . ' ' . $customer->firstname . ' ' . $customer->lastname }}</h5>
                        </div>

                        <div class="ibox-content">
                            <div class="mb20"></div>

                            <div class="row m-b-lg">
                                <div class="col-sm-4 col-md-3">
                                    <div class="m-b-sm">
                                        <div class="row text-center">
                                        	<div class="profile_picture">
                                        		<label>
													<div class="picture">
														<img alt="image" src="{{ route('customer_avatar', $customer->id) }}">
													</div>
												</label>
											</div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-8 col-md-9">

                                	<div class="form-group">
                                        <label class="col-md-2 control-label">Active</label>
                                        <div class="col-md-10">
                                        	<p class="form-control-static">
                                        		{!! ($customer->active)? '<i class="fa fa-check text-navy" aria-hidden="true"></i>' : '<i class="fa fa-times text-danger" aria-hidden="true"></i>' !!}
                                        	</p>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Name</label>
                                        <div class="col-md-5"><p class="form-control-static">{{ $customer->firstname }}</p></div>
                                        <div class="mb5-sm"></div>
                                        <div class="col-md-5"><p class="form-control-static">{{ $customer->lastname }}</p></div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Phonenumber</label>
                                        <div class="col-md-10"><p class="form-control-static">{{ $customer->home_phone }}</p></div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Mobile</label>
                                        <div class="col-md-10"><p class="form-control-static">{{ $customer->mobile_phone }}</p></div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Email</label>
                                        <div class="col-md-10"><p class="form-control-static"><a href="mailto:{{ $customer->email }}">{{ $customer->email }}</a></p></div>
                                    </div>

                                    <div class="form-group" id="dob">
                                        <label class="col-md-2 control-label">Date of birth</label>

                                        <div class="col-md-10">
                                            <p class="form-control-static">{{ date('d M Y', strtotime($customer->dob)) }}</p>
                                        </div>
                                    </div>

                                    <div class="hr-line-dashed"></div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Newsletter</label>
                                        <div class="col-md-10">
                                        	<p class="form-control-static">
                                        		{!! ($customer->newsletter)? '<i class="fa fa-check text-navy" aria-hidden="true"></i>' : '<i class="fa fa-times text-danger" aria-hidden="true"></i>' !!}
                                        	</p>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Opt-in</label>
                                        <div class="col-md-10">
                                        	<p class="form-control-static">
                                        		{!! ($customer->optin)? '<i class="fa fa-check text-navy" aria-hidden="true"></i>' : '<i class="fa fa-times text-danger" aria-hidden="true"></i>' !!}
                                        	</p>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="ibox ">
                        <div class="ibox-title">
                            <h5>Shipping Addresses</h5>
                        </div>
                        <div class="ibox-content">
                            <div class="google-map" id="map"></div>
                        </div>
                    </div>

                    <div class="ibox">

                        <div class="ibox-title">
                            <h5>Orders</h5>
                        </div>

                        <div class="ibox-content">
                            <div class="mb20"></div>

                            <div class="row m-b-lg">
                                <div class="col-xs-12">
									@if($customer->orders->count() > 0)
										@foreach($customer->orders as $order)
											sdjfosijf
										@endforeach
									@else
										<i>This customer has no order</i>
									@endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="ibox">
                        <div class="ibox-title">
                            <h5>Action</h5>
                        </div>

                        <div class="ibox-content">
                            @if($customer->note)
                                <strong>Note</strong>

                                <p class="form-control-static">
                                	{!! nl2br($customer->note) !!}
                                </p>

                                <hr>
                            @endif

                            <a class="btn btn-block btn-outline btn-success" href="{{ route('edit_customer', $customer->id) }}"><i class="fa fa-pencil-square-o"></i> Edit</a>
                            <button class="btn btn-block btn-outline btn-primary" type="button"><i class="fa fa-credit-card-alt"></i> Add Order</button>
                            <button class="btn btn-block btn-outline btn-warning" type="button"><i class="fa fa-map-marker"></i> Add Address</button>
                            <button class="btn btn-block btn-outline btn-info" type="button"><i class="fa fa-envelope-o"></i> Send message</button>
                            <button class="btn btn-block btn-outline btn-danger" type="button"><i class="fa fa-power-off"></i> Deactivate</button>
                            <hr>

                            <div class="student-detail hidden-xs hidden-sm">
                                <div class="full-height-scroll">
                                    <strong>Timeline activity</strong>
                                    <div id="vertical-timeline" class="vertical-container dark-timeline">
                                        <div class="vertical-timeline-block">
                                            <div class="vertical-timeline-icon gray-bg">
                                                <i class="fa fa-briefcase"></i>
                                            </div>
                                            <div class="vertical-timeline-content">
                                                <p>Many desktop publishing packages and web page editors now use Lorem.</p>
                                                <span class="vertical-date small text-muted"> 4:20 pm - 10.05.2014 </span>
                                            </div>
                                        </div>
                                        <div class="vertical-timeline-block">
                                            <div class="vertical-timeline-icon gray-bg">
                                                <i class="fa fa-bolt"></i>
                                            </div>
                                            <div class="vertical-timeline-content">
                                                <p>There are many variations of passages of Lorem Ipsum available.</p>
                                                <span class="vertical-date small text-muted"> 06:10 pm - 11.03.2014 </span>
                                            </div>
                                        </div>
                                        <div class="vertical-timeline-block">
                                            <div class="vertical-timeline-icon navy-bg">
                                                <i class="fa fa-warning"></i>
                                            </div>
                                            <div class="vertical-timeline-content">
                                                <p>The generated Lorem Ipsum is therefore.
                                                </p>
                                                <span class="vertical-date small text-muted"> 02:50 pm - 03.10.2014 </span>
                                            </div>
                                        </div>
                                        <div class="vertical-timeline-block">
                                            <div class="vertical-timeline-icon gray-bg">
                                                <i class="fa fa-coffee"></i>
                                            </div>
                                            <div class="vertical-timeline-content">
                                                <p>Conference on the sales results for the previous year.
                                                </p>
                                                <span class="vertical-date small text-muted"> 2:10 pm - 12.06.2014 </span>
                                            </div>
                                        </div>
                                        <div class="vertical-timeline-block">
                                            <div class="vertical-timeline-icon gray-bg">
                                                <i class="fa fa-briefcase"></i>
                                            </div>
                                            <div class="vertical-timeline-content">
                                                <p>Many desktop publishing packages and web page editors now use Lorem.
                                                </p>
                                                <span class="vertical-date small text-muted"> 4:20 pm - 10.05.2014 </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop