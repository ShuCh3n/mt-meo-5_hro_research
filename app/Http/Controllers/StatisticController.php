<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Country;
use App\Commodity;

class StatisticController extends Controller
{
	private $months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

    public function commodity()
    {
    	$this->years = collect([ date('Y') ]);
    	$series = [];

    	if($this->request->years)
		{
			$this->years = collect($this->request->years);
		}

		$rubber = Commodity::where('name', 'Rubber')->with('prices')->first();

		foreach($this->years as $year)
		{
			for($i=1;$i<=12;$i++)
	        {

	            $start = carbon()->year($year)->month($i)->startOfMonth();
	            $end = carbon()->year($year)->month($i)->endOfMonth();

	            $results = $rubber->prices->where('created_at', '>', $start)->where('created_at', '<', $end);
	            $price = 0;

	            if($results->isNotEmpty())
	            {
	            	$price = $results->avg('price');
	            }

	            $series[] = ["meta" => $rubber->name, "value" => round($price, 2)];
	        }
	    }

    	return array(
            "labels" => $this->labels(),
            "series" => [$series]
        );
    }

    private function labels()
	{
		$labels = [];

		foreach($this->years as $year)
		{
			foreach($this->months as $month)
	        {
	            $labels[] = $year . ' ' . $month;
	        }
	    }

	    return $labels;
	}

    public function weather()
    {
    	$this->years = collect([ date('Y') ]);
    	$series = [];

		if($this->request->years)
		{
			$this->years = collect($this->request->years);
		}

    	$country = Country::getCountry($this->request->iso)->load(['weathers' => function($query){
    		$query->whereBetween('created_at', [carbon()->year($this->years->first())->startOfYear(), carbon()->year($this->years->first())->endOfYear()]);
    	}]);

    	foreach($this->years as $year)
		{
			for($i=1;$i<=12;$i++)
	        {
	            $start = carbon()->year($year)->month($i)->startOfMonth();
	            $end = carbon()->year($year)->month($i)->endOfMonth();

	            $results = $country->weathers->where('created_at', '>', $start)->where('created_at', '<', $end);
	            $degree = 0;

	            if($results->isNotEmpty())
	            {
	            	$degree = $results->avg('degree');
	            }

	            $series[] = ["meta" => $country->name, "value" => round($degree, 1)];
	        }
	    }

    	return array(
            "labels" => $this->labels(),
            "series" => [$series]
        );
    }
}
