from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm import joinedload
from sqlalchemy import *
from datetime import *

import sys
import datetime

sys.path.append('..')
import zeus_scheme
sys.path.remove('..')

class DatabaseMinion():
	def __init__(self):
		self.engine = create_engine('mysql://root:root@localhost/cms-dev')
		Session = sessionmaker(bind=self.engine)
		self.session = Session()

	def getPlatform(self, value):
		return self.session.query(zeus_scheme.Platform).filter(zeus_scheme.Platform.name == value).first()

	def findProduct(self, platform, values, platform_id=None):
		product = None

		if platform_id:
			platform_product = self.session.query(zeus_scheme.PlatformProduct).filter(zeus_scheme.PlatformProduct.platform_id == platform.id).filter(zeus_scheme.PlatformProduct.article_code == str(platform_id)).first()
			
			if platform_product:
				return platform_product.product

		for v in values:
			platform_product = self.session.query(zeus_scheme.PlatformProduct).filter(zeus_scheme.PlatformProduct.platform_id == platform.id).filter(zeus_scheme.PlatformProduct.article_code == str(v)).first()
			
			if platform_product:
				return platform_product.product

			product = self.session.query(zeus_scheme.Product).filter(or_(zeus_scheme.Product.sku == str(v), zeus_scheme.Product.ean == str(v), zeus_scheme.Product.supplier_sku == str(v))).first()

			if product:
				return product

		return product

	def insertPrice(self, platform, product, price, article_code):
		now = str(datetime.datetime.now()).split('.')[0]

		platform_product_price = zeus_scheme.PlatformProductPrice()
		platform_product_price.platform_id = platform.id
		platform_product_price.product_id = product.id
		platform_product_price.price = price
		platform_product_price.created_at = now
		platform_product_price.updated_at = now

		self.session.add(platform_product_price)
		self.session.commit()

		assigned_platform = self.session.query(zeus_scheme.PlatformProduct).filter(zeus_scheme.PlatformProduct.platform_id == platform.id).filter(zeus_scheme.PlatformProduct.product_id == product.id).first()
				
		if not assigned_platform:
			assigned_platform = zeus_scheme.PlatformProduct()
			assigned_platform.platform_id = platform.id
			assigned_platform.product_id = product.id
			assigned_platform.article_code = article_code

			self.session.add(assigned_platform)
			self.session.commit()


	def insertStock(self, name, product, availability):
		now = str(datetime.datetime.now()).split('.')[0]

		warehouse = self.getWarehouse(name.title())

		stocks = zeus_scheme.ProductStock()
		stocks.warehouse_id = warehouse.id
		stocks.product_id = product.id
		stocks.amount = availability
		stocks.created_at = now
		stocks.updated_at = now

		self.session.add(stocks)
		self.session.commit()

	def getWarehouse(self, name):
		
		warehouse = self.session.query(zeus_scheme.Warehouse).filter_by(name = name).first()
		
		if not warehouse:
			now = str(datetime.datetime.now()).split('.')[0]

			warehouse = zeus_scheme.Warehouse()
			warehouse.name = name
			warehouse.third_party = True
			warehouse.created_at = now
			warehouse.updated_at = now

			self.session.add(warehouse)
			self.session.commit()

		return warehouse