<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateCategoryMotorProductVehicleYear extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('category_motor_product_vehicle_year', function (Blueprint $table) {
            $table->dropPrimary();
            $table->increments('id');
            $table->timestamps();

            $table->primary(['id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('category_motor_product_vehicle_year', function (Blueprint $table) {
            //
        });
    }
}
