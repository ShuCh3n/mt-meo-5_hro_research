<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Classes\API\Vehicle\Motors\Motor;

class VehicleController extends Controller
{
	public function vehicle()
    {
    	if($this->request->vehicle == 'motors' || $this->request->vehicle == 'motor')
    	{
    		$vehicle = new Motor($this->request);

    		switch ($this->request->type) {
    			case 'products':
    				return $vehicle->products();
    				break;
    			case 'view':
    				return $vehicle->view_motor();
    				break;
    			case '':
    				return $vehicle->motors();
    				break;
    		}
    	}
    }
}