<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Multilanguage;

class ProductFeature extends Model
{
    protected $hidden = [
        'created_at', 
        'updated_at'
    ];

    protected $fillable = ['product_feature_type_id'];

	public function product_feature_type()
	{
		return $this->belongsTo(ProductFeatureType::class);
	}

    public function options()
    {
    	return $this->hasMany(ProductFeatureOption::class);
    }

    public function infos()
    {
    	return $this->hasMany(ProductFeatureLanguage::class);
    }

    public function info()
    {
        return $this->hasOne(ProductFeatureLanguage::class);
    }

    public function info_model()
    {
        return new ProductFeatureLanguage;
    }

    public function appendName($language_id, $name)
    {
        $feature_language = new ProductFeatureLanguage;
        $feature_language->product_feature_id = $this->id;
        $feature_language->language_id = $language_id;
        $feature_language->name = ucfirst(strtolower($name));
        $feature_language->save();
    }

    public static function getFeature($feature_type, $feature_name)
    {
        $product_features = self::where('product_feature_type_id', $feature_type->id)->get();

        foreach($product_features as $feature)
        {
            $find_feature = $feature->infos()->where('language_id', 2)->where(function($query) use ($feature_name){
                $query->where('name', $feature_name)->orWhere('name', ucfirst(strtolower($feature_name)));
            })->first();

            if($find_feature)
            {
                return $feature;
            }
        }

        return null;
    }

    public static function firstOrCreateFeature($feature_type, $name, $language_id)
    {
        $product_feature = ProductFeature::where('product_feature_type_id', $feature_type->id)->whereHas('infos', function($query) use ($name, $language_id){
            $query->where('language_id', $language_id)->where('name', ucfirst(strtolower($name)));
        })->firstOrCreate(["product_feature_type_id" => $feature_type->id]);

        if($product_feature->wasRecentlyCreated)
        {
            $product_feature->appendName($language_id, $name);
        }

        return $product_feature;
    }
}
