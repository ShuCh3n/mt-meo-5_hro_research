@extends('dashboard.layout.main')

@section('stylesheet')

@stop

@section('javascript')

@stop

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-6">
            <h2>{{ trans('pages.inquiry_detail_header') }}</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('home') }}">{{ trans('menu.home') }}</a>
                </li>
                <li>
                    <a href="">{{ trans('menu.inquiries') }}</a>
                </li>
                <li class="active">
                    <strong>{{ trans('page.inquiry') }}</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-6">
            <a class="btn btn-outline btn-primary pull-right mt30" href="{{ route('new_department') }}"><i class="fa fa-plus-circle" aria-hidden="true"></i> New Department</a>
        </div>
    </div>

	<div class="wrapper wrapper-content animated fadeInRight ecommerce">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-content">

                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th data-toggle="true">Department</th>
                                    <th data-hide="phone">Users</th>
                                    <th class="text-right" data-sort-ignore="true">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($departments as $department)
                                    <tr>
                                        <td>
                                           {{ $department->name }}
                                        </td>
                                        <td>
                                          {{ $department->users->count() }}  
                                        </td>
                                        <td class="text-right">
                                            <a class="btn-white btn btn-xs" href="{{ route('edit_department', $department->id) }}">Edit</a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <td colspan="6">
                                    <ul class="pagination pull-right"></ul>
                                </td>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop