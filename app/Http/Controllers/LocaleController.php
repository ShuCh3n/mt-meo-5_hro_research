<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Language;
use App\TaxGroup;
use App\TaxRule;
use App\Country;

class LocaleController extends Controller
{
    public function languages()
    {
        $languages = Language::all();
        return view('dashboard.languages', compact('languages'));
    }

    public function taxGroups()
    {
    	$tax_groups = TaxGroup::all();

    	return view('dashboard.tax_groups', compact('tax_groups'));
    }

    public function addTaxGroup()
    {
		$this->validate($this->request, [
			'name' => 'required',
		]);

		$tax_group = new TaxGroup;
		$tax_group->setTaxGroup($this->request);

		return redirect()->route('tax_groups');
    }

    public function editActionTaxGroup()
    {
    	$this->validate($this->request, [
			'name' => 'required',
		]);

    	$tax_group = TaxGroup::where('id', $this->request->id)->first();
        $tax_group->setTaxGroup($this->request);

		return redirect()->route('tax_groups');
    }

    public function newActionTax()
    {
        $this->validate($this->request, [
            'name' => 'required',
            'countries' => 'required'
        ]);

        $tax_rule = New TaxRule;
        $tax_rule->tax_group_id = $this->request->group_id;
        $tax_rule->setTaxRule($this->request);

        return redirect()->route('view_tax_group', $this->request->group_id);
    }

    public function editActionTax()
    {
        $this->validate($this->request, [
            'name' => 'required',
            'countries' => 'required'
        ]);

        $tax_rule = TaxRule::findOrfail($this->request->id);
        $tax_rule->setTaxRule($this->request);

        return redirect()->route('view_tax_group', $this->request->group_id);
    }

    public function taxes()
    {
        return view('dashboard.taxes');
    }

    public function newTaxGroup()
    {
    	return view('dashboard.new_tax_group');
    }

    public function editTaxGroup()
    {
    	$tax_group = TaxGroup::findOrFail($this->request->id);

    	return view('dashboard.edit_tax_group', compact('tax_group'));
    }

    public function viewTaxGroup()
    {
        $tax_group = TaxGroup::findOrFail($this->request->id);
    	return view('dashboard.view_tax_group', compact('tax_group'));
    }

    public function newTax()
    {
        $countries = Country::all();

    	return view('dashboard.new_tax', compact('countries'));
    }

    public function editTax()
    {
        $tax_rule = TaxRule::findOrFail($this->request->id);
        $countries = Country::all();

        return view('dashboard.edit_tax', compact('tax_rule', 'countries'));
    }

    public function getTaxRules()
    {
        $tax_group = TaxGroup::findOrFail($this->request->id);

        return $tax_group->tax_rules;
    }
}
