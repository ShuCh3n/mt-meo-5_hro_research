<div>
	<div class="form-group">
	    <div class="col-md-5 col-md-offset-2">
	        <input type="text" data-identifier="{{ $random_string }}" class="form-control input-qty" name="bulk_rule_qty[]" placeholder="QTY">
	    </div>
	    <div class="col-md-5">
	        <input type="text" data-identifier="{{ $random_string }}" class="form-control input-price" name="bulk_rule_retail_price[]" placeholder="Retail Price">
	    </div>
	</div>
</div>