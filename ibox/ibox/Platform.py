import pandas as pd
import ftpMinion as ftp_minion

class Platform():
	def __init__(self, platform):

		self.results = []

		if isinstance(platform, str):
			self.platform = None
			self.name = platform
		else:
			self.platform = platform
			self.name = platform.name
			self.code = platform.code

	def setFTP(self, ftp):
		self.ftp = ftp
		return self

	def export(self):
		df = pd.DataFrame(self.results, columns = ['ProductCode', 'ArticleCode', 'ProductDescription', 'ProductEANCode', 'Stock', 'OriginalPrice', 'PricePP', 'LeveranciersArtikelCode', 'LeverancierCode', 'Leverancier'])
		df.to_csv('export/' + str(self.name) + '.csv', index=False)

		return self

	def exportB2C(self):
		df = pd.DataFrame(self.results, columns = ['ProductCode', 'Leverancier'])
		df.to_csv('export/' + str(self.name) + '.csv', index=False)

		return self


	def store(self):
		self.ftp.store(self.name + '.csv')
