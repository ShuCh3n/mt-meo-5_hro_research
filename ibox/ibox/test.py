import databaseMinion
import sys

sys.path.append('..')
import zeus_scheme
sys.path.remove('..')

database_minion = databaseMinion.DatabaseMinion()

#platform_price = database_minion.session.query(zeus_scheme.PlatformProductPrice).filter_by(platform_id = 14, product_id = 8).order_by(zeus_scheme.PlatformProductPrice.created_at.desc()).first()
platform_price = 39.5

purchasing_price = 35.12
threshold = 3.5
amount = -0.05
percentage = 6

threshold_percentage = (100 - percentage) / 100
calculate_percentage = (100 + percentage) / 100

calculated_price = (platform_price - threshold) * threshold_percentage

if calculated_price > purchasing_price:
	print(str(round(platform_price + amount, 2)).replace(".", ","))
	exit()

print(str(round((purchasing_price + threshold) * calculate_percentage , 2)).replace(".", ","))
