<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductShopPrice extends Model
{
    use SubFilter;
    use SoftDeletes;
    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];
    //
}
