<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Shop;
use App\Product;
use App\Language;
use App\Supplier;
use App\Category;
use Storage;

class FrontpageController extends Controller
{
    public function home()
    {
        return view('site.home');
    }

    public function lang($language){
        session()->put('locale', $language);
        return redirect()->route('home');
    }
}
