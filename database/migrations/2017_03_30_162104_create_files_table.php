<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('files', function (Blueprint $table) {
            $table->integer('shop_id')->nullable()->unsigned();
            $table->string('filename');
            $table->integer('filable_id');
            $table->string('filable_type');
            $table->string('extension');
            $table->string('type');
            $table->softDeletes();
            $table->timestamps();

            $table->index('filable_id');
            $table->index('filable_type');

            $table->foreign('shop_id')
                ->references('id')
                ->on('shops')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('files');
    }
}
