@extends('dashboard.layout.main')

@section('content')
    <div class="wrapper wrapper-content animated fadeInRight ecommerce">
            <div class="ibox-content m-b-sm border-bottom">
                <div class="row">
                    <div class="col-xs-12">
                        <a class="btn btn-primary pull-right" href="{{ route('new_category') }}"><i class="fa fa-plus-circle" aria-hidden="true"></i> New Category</a>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox">
                        <div class="ibox-content">

                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>Shop</th>
                                        <th>Categories</th>
                                        <th class="text-right">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($shops as $shop)
                                        <tr>
                                            <td>
                                                <a href="{{ route('shop_categories', $shop->id) }}">{{ $shop->name }}</a>
                                            </td>
                                            <td>
                                              {{ $shop->categories->count() }}
                                            </td>
                                            <td class="text-right">
                                                <div class="btn-group">
                                                    <a href="{{ route('shop_categories', $shop->id) }}" class="btn-white btn btn-xs">View</a>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@stop