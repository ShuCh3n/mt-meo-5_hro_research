@extends('dashboard.layout.main')

@section('content')
	<div class="wrapper wrapper-content animated fadeInRight ecommerce">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Upload</h5>
                    </div>
                    <div class="ibox-content">
                        <analist-uploader uri="{{ route('upload_analist_file') }}"></analist-uploader>
                    </div>
                </div>

                <div class="ibox">
                    <div class="ibox-title">
                        <h5>History</h5>
                    </div>
                    <div class="ibox-content">
                        <analist-history ref="history" histories="{{ json_encode($histories) }}" segments="{{ json_encode(range('A', 'F')) }}"></analist-history>

                        <div class="text-center">
                            {{ $histories->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop