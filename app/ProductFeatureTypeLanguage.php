<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductFeatureTypeLanguage extends Model
{
	protected $fillable = ['product_feature_type_id', 'language_id', 'name'];
	
    public function product_feature_type()
    {
    	return $this->belongsTo(ProductFeatureType::class);
    }

    public function scopeLanguage($query, $language_id)
    {
        return $query->where('language_id', $language_id);
    }

    public function scopeBackOffice($query)
    {
    	$query->where('language_id', auth()->user()->preferenceLanguage()->id);
    }
}
