<?php

namespace App\Http\Controllers\Sync;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Classes\SyncFactory\SyncFactory;
use App\Product;
use App\Group;
use App\Image;

class ProductController extends Controller
{
    public function products()
    {
        foreach ($this->request->input() as $productRequest) {
            $this->product((object)$productRequest);
            \Log::info('Bulk added: '.((object)$productRequest)->sku);
        }
        return ['bulk' => 'transferred'];
    }

    public function product($product = null)
    {
        if(!$product){
            $product = $this->request;
        }

        $sync = SyncFactory::create('product')
                    ->set('request', $product)
                    ->sync();

        return $sync->product;
    }
    
    public function productAttach($sku){
        $product = Product::where('sku', $sku)->firstOrFail();
        if ($this->request->group){
            if ($this->request->shop){
                $group = Group::firstOrCreate(['name' => $this->request->group, 'shop_id' => $this->request->shop]);
            }else{
                $group = Group::firstOrCreate(['name' => $this->request->group]);
            }
            if($this->request->images){
                if(is_array($this->request->images)){
                    foreach($this->request->images as $image_name){
                        $image = Image::firstOrNew(['filename' => $image_name, 'imagable_type' => 'App\Group', 'imagable_id' => $group->id]);
                        $image->image_type_id = 1;
                        if($this->request->shop){
                            $image->shop_id = $this->request->shop;
                        }
                        $image->save();
                    }
                }else{
                        $image = Image::firstOrNew(['filename' => $this->request->images, 'imagable_type' => 'App\Group', 'imagable_id' => $group->id]);
                        $image->image_type_id = 1;
                        if($this->request->shop){
                            $image->shop_id = $this->request->shop;
                        }
                        $image->save();
                }
                
            }
            $product->groups()->sync([$group->id]);
        }
        return $product->load('groups');
    }
}