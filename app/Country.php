<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
	public function states()
	{
		return $this->hasMany(State::class);
	}

    public function taxRule()
    {
    	return $this->hasMany(TaxRule::class);
    }

    public function weathers()
    {
        return $this->hasMany(Weather::class);
    }

    public function getDefaultTaxRule()
    {
    	return $this->taxRule()->where('default', true)->first();
    }

    public static function getCountry($country)
    {
        if(is_string($country))
        {
            return self::where('name', $country)->orWhere('iso_code_2', strtoupper($country))->orWhere('iso_code_3', strtoupper($country))->first();
        }

        if(is_numeric($country))
        {
            return self::findOrFail($country);
        }

        return null;
    }
}
