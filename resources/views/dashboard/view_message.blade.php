@extends('dashboard.layout.main')

@section('content')
<div class="wrapper wrapper-content animated fadeInRight">
	<div class="row">
		<div class="col-lg-3">
                <div class="ibox float-e-margins">
                    <div class="ibox-content mailbox-content">
                        <div class="file-manager">
                            <a class="btn btn-block btn-primary compose-mail" href="mail_compose.html"><i class="fa fa-ticket" aria-hidden="true"></i> Compose Ticket</a>
                            <div class="space-25"></div>
                            <h5>Tickets</h5>
                            <ul class="folder-list m-b-md" style="padding: 0">
                                <li><a href="{{ route('messages') }}"> <i class="fa fa-inbox "></i> Open <span class="label label-warning pull-right">{{-- $total_unreaded_messages->count() --}}</span> </a></li>
                                <li><a href="{{ route('closed_messages') }}"> <i class="fa fa-envelope-o"></i> Closed</a></li>
                            </ul>
                            <h5>Department</h5>
                            <ul class="category-list" style="padding: 0">
                                @foreach($departments as $department)
                                    <li><a href="#"> <i class="fa fa-circle" style="color:#{{ $department->color }}"></i> {{ $department->name }}</a></li>
                                @endforeach
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
		<div class="col-lg-6">
			<div class="ibox">
				<div class="ibox-title">
					<h5>Ticket #{{ $ticket->reference }}</h5>
				</div>

				<div class="ibox-content">
					<div class="row">
						<div class="col-md-12">
							<div class="chat-activity-list">
								@foreach($ticket->ticket_messages as $message)
									<div class="chat-element {{ (!$message->customer)? 'right' : ''  }}">
										@if($message->customer)
											<a href="#" class="pull-left">
												<img alt="image" class="img-circle" src="https://lh3.googleusercontent.com/pj0jrgFCXjNtAkktmst6H1qaU4F9nmOE5b7_kAcwgYfoQfF7NQXQo3uu2lteXqFrEsg=w300">
											</a>
										@else
											<a href="#" class="pull-right">
												<img alt="image" class="img-circle" src="https://pbs.twimg.com/profile_images/602729491916435458/hSu0UjMC.jpg">
											</a>
										@endif

										<div class="media-body {{ (!$message->customer)? 'text-right' : ''  }}">
											@if($ticket->customer)
												<strong>{{ auth()->user()->firstname . ' ' . auth()->user()->lastname }}</strong>
											@else
												<strong>{{ $ticket->first_name . ' ' . $ticket->last_name }}</strong>
											@endif

											<p class="m-b-xs">{{ $message->message }}</p>

											<small class="text-muted">{{ date('H:i - d M Y', strtotime($message->created_at)) }}</small>
										</div>
									</div>
								@endforeach
							</div>

							<div class="mb20"></div>

							<div class="chat-form">
								<form role="form" method="post">
									<div class="form-group">
										<textarea class="form-control" placeholder="Message" name="message"></textarea>
									</div>

									<div class="col-xs-6 text-left">
										<div class="row">
											<a class="btn btn-sm btn-danger m-t-n-xs" href="{{ route('close_ticket', $ticket->id) }}">
												<i class="fa fa-times" aria-hidden="true"></i>
												<strong>Close Ticket</strong>
											</a>
										</div>
									</div>

									<div class="col-xs-6 text-right">
										<div class="row">
											{{ csrf_field() }}
											<button type="submit" class="btn btn-sm btn-primary m-t-n-xs">
												<strong>Send message</strong>
												<i class="fa fa-paper-plane" aria-hidden="true"></i>
											</button>
										</div>
									</div>

								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="col-lg-3">
			<div class="ibox">
				<div class="ibox-title">
					<h5>Order {{ ($ticket->order)? '#' . $ticket->order->ref : '' }}</h5>
				</div>

				<div class="ibox-content">
					@if($ticket->order)

					@else
						<div class="text-center">
							<i>No order is related to this ticket.</i>
						</div>
					@endif
				</div>
			</div>
		</div>
	</div>
</div>
@stop
