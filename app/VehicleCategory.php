<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VehicleCategory extends Model
{
    public function infos()
    {
        return $this->hasMany(VehicleCategoryInfo::class);
    }

    public function info()
    {
        return $this->hasOne(VehicleCategoryInfo::class);
    }

    public function motors()
    {
        return $this->belongsToMany(Motors::class);
    }

}
