<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Classes\Search;

class SearchController extends Controller
{
    public function index()
    {
    	$search = (new Search($this->request->q))->all();

    	return view('dashboard.search_result', compact('search'));
    }
}
