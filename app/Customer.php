<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Customer extends Authenticatable
{
    protected $hidden = [
        'password', 'created_at', 'updated_at', 'deleted_at', 'active', 'note', 'optin', 'tax_rule_id'
    ];

    protected $dates = ['dob'];

    public function shop()
    {
    	return $this->belongsTo(Shop::class);
    }

    public function billing_country()
    {
        return $this->belongsTo(Country::class);
    }

    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    public function customer_addresses()
    {
    	return $this->hasMany(CustomerAddress::class);
    }

    public function customer_address()
    {
        return $this->hasOne(CustomerAddress::class);
    }

    public function customer_sessions()
    {
    	return $this->hasMany(CustomerSession::class);
    }

    public function customer_password_reset()
    {
        return $this->hasOne(CustomerPasswordReset::class)->latest();
    }

    public function lastVisit()
    {
    	$last_session = $this->customer_sessions()->orderBy('created_at', 'desc')->first();

    	if($last_session)
    	{
    		return $last_session->updated_at;
    	}

    	return null;
    }

    public function scopeEmail($query, $email)
    {
        return $query->where('email', $email);
    }

    public function avatar($size = 'medium')
    {
    	$avatar = EmailAvatar::where('email', $this->email)->first();
    	
    	if($avatar)
    	{
    		return $avatar;
    	}
    	
    	return false;
    }
}
