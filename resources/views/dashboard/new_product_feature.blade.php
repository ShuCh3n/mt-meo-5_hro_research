@extends('dashboard.layout.main')

@section('javascript')
    <script>
        $(function(){
            $('.add-option').click(function(){
                $.get('/snippets/product-feature-option-input', function(data){
                    var random_string = randomString(10);

                    $('.more_options').each(function( index ) {
                        var form_group = $(data).find('div.form-group');

                        if($(this).attr('data-language'))
                        {
                            var input = form_group.find('input').attr('name', 'option_name[' + random_string + '][' + $(this).attr('data-language') + ']');
                            $(form_group).appendTo(".more_options.language-" + $(this).attr('data-language'));
                        }
                        else
                        {
                            var input = form_group.find('input').attr('name', 'option_name[' + random_string + '][all]');
                            $(form_group).appendTo(".more_options.more-options-all-language");
                        }
                    });
                });
            });

            $(document).on('keyup', '.more-options-all-language .extra-option', function(){
                $('.extra-option[data-identifier="' + $(this).attr('data-identifier') + '"]').val($(this).val());
            });
        });
    </script>
@stop

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>{{ trans('pages.inquiry_detail_header') }}</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('home') }}">{{ trans('menu.home') }}</a>
                </li>
                <li>
                    <a href="">{{ trans('menu.inquiries') }}</a>
                </li>
                <li class="active">
                    <strong>{{ trans('page.inquiry') }}</strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">
        {!! session()->has('success')? '<div class="alert alert-success alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>' . session('success') . '</div>' : '' !!}

        <form id="profileForm" method="post" class="form-horizontal">
            <div class="row">
                <div class="col-md-8">
                    <div class="ibox">

                        <div class="ibox-title">
                            <h5>New Feature</h5>
                        </div>

                        <div class="ibox-content">
                            <div class="mb20"></div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Language</label>
                                        <div class="col-md-10">
                                            <select id="multi-language" class="form-control countries-select">
                                                <option value="all" icon="United-Nations" selected>All</option>
                                                @foreach($languages as $language)
                                                    <option value="{{ $language->id }}" icon="{{ $language->icon }}">{{ $language->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="multi-language-info-box multi-language-all">
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">Name</label>
                                            <div class="col-md-10"><input type="text" class="form-control all-language-input feature-name-input" name="feature_name[all]" data-input="feature-name-input" placeholder="Feature Name"></div>
                                        </div>
                                    </div>
                                    @foreach($languages as $language)
                                        <div class="multi-language-info-box multi-language-{{ $language->id}}">
                                            <div class="form-group">
                                                <label class="col-md-2 control-label">Name</label>
                                                <div class="col-md-10"><input type="text" class="form-control feature-name-input" name="feature_name[{{ $language->id }}]" placeholder="Feature Name"></div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div> 

                    <div class="ibox">

                        <div class="ibox-title">
                            <h5>Feature Options</h5>
                        </div>

                        <div class="ibox-content">
                            <div class="mb20"></div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="multi-language-info-box multi-language-all">
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">Option</label>
                                            <div class="col-md-10"><input type="text" class="form-control all-language-input feature-option-input" data-input="feature-option-input" name="option_name[first][all]" placeholder="Option Name"></div>
                                        </div>

                                        <div class="more_options more-options-all-language"></div>
                                    </div>

                                    @foreach($languages as $language)
                                        <div class="multi-language-info-box multi-language-{{ $language->id}}">
                                            <div class="form-group">
                                                <label class="col-md-2 control-label">Option</label>
                                                <div class="col-md-10"><input type="text" class="form-control feature-option-input" name="option_name[first][{{ $language->id }}]" placeholder="Option Name"></div>
                                            </div>

                                            <div class="more_options language-{{ $language->id }}" data-language="{{ $language->id }}"></div>
                                        </div>
                                    @endforeach

                                    <button class="btn btn-w-m btn-primary pull-right add-option" type="button">
                                        <i class="fa fa-plus" aria-hidden="true"></i> Add Option
                                    </button>

                                </div>
                            </div>
                        </div>
                    </div> 

                </div>

                <div class="col-md-4">
                    <div class="ibox">
                        <div class="ibox-title">
                            <h5>Action</h5>
                        </div>

                        <div class="ibox-content">
                            {{ csrf_field() }}
                            <button class="btn btn-block btn-outline btn-success" type="submit"><i class="fa fa-floppy-o"></i> Save</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>

        <div class="mb30"></div>
    </div>
@stop