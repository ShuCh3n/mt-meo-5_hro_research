<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductShopInfo extends Model
{
    protected $fillable = ['shop_id', 'product_id', 'language_id'];
    
    protected $hidden = [
        'created_at', 
        'updated_at'
    ];

    public function shop()
    {
    	return $this->belongsTo(Shop::class);
    }
}
