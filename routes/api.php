<?php
Route::get('health', ["uses" => "HealthCheck@check"]);
Route::get('products', ["uses" => "ProductController@products"]);
Route::get('products/profiles', ["uses" => "ProductController@productsProfiles"]);
Route::get('products/filters', ["uses" => "ProductController@productsFilters"]);

Route::get('product/{id}', ["uses" => "ProductController@getProduct"]);
Route::get('products/featured', ["uses" => "ProductController@getFeaturedProduct"]);
Route::get('products/offers', ["uses" => "ProductController@getProductOffers"]);
Route::get('products/most-sold', ["uses" => "ProductController@getMostSoldProducts"]);

Route::post('products/search', ["uses" => "ProductController@searchProduct"]);

Route::get('categories', ["uses" => "CategoryController@categories"]);
Route::get('category/{id}/products', ["uses" => "CategoryController@categoryProducts"]);
Route::get('category/{id}/filters', ["uses" => "CategoryController@categoryFilters"]);

Route::get('cart', ["uses" => "CartController@cart"]);
Route::get('cart/clear', ["uses" => "CartController@initCart"]);
Route::post('cart/add', ["uses" => "CartController@addToCart"]);

Route::get('customer/register/check-email', ["uses" => "CustomerController@checkEmail"]);
Route::get('customer/orders', ["uses" => "CustomerController@orders"]);
Route::get('customer/order/{reference}', ["uses" => "CustomerController@order"]);

Route::post('customer/init', ["uses" => "CustomerController@initApp"]);
Route::post('customer/register', ["uses" => "CustomerController@customerRegister"]);
Route::post('customer/register/validate', ["uses" => "CustomerController@customerValidate"]);
Route::post('customer/update', ["uses" => "CustomerController@customerUpdate"]);
Route::post('customer/login', ["uses" => "CustomerController@customerLogin"]);
Route::post('customer/forgot-password', ["uses" => "CustomerController@forgotPassword"]);
Route::post('customer/new-password', ["uses" => "CustomerController@newPassword"]);

Route::get('payments', ["uses" => "PaymentController@payments"]);

Route::post('pay', ["uses" => "PaymentController@pay"]);
Route::post('pay/{provider}/response', ["uses" => "PaymentController@paymentResponse"]);

Route::get('blogs', ["uses" => "BlogController@blogs"]);
Route::get('blog/{id}', ["uses" => "BlogController@blog"]);

Route::get('brand/{id}', ["uses" => "BrandController@getBrand"]);

Route::post('ticket/new', ["uses" => "TicketController@newMessage"]);
Route::post('ticket/find', ["uses" => "TicketController@findTicket"]);
Route::post('ticket/{ticket_reference}/message/new', ["uses" => "TicketController@newTicketMessage"]);

Route::get('shop', ["uses" => "ShopController@getShop"]);

Route::get('vehicle/{vehicle}', ["uses" => "VehicleController@vehicle"]);
Route::get('vehicle/{vehicle}/{type}', ["uses" => "VehicleController@vehicle"]);
Route::get('vehicle/{vehicle}/{type}/{id}', ["uses" => "VehicleController@vehicle"]);
