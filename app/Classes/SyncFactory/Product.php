<?php
namespace App\Classes\SyncFactory;

use App\Brand;
use App\Group;
use App\ProductPrice;
use App\ProductGroup;
use App\ProductInfo;
use App\Language;
use App\ProductFeatureType;
use App\ProductFeature;
use App\ProductFeatureOption;
use App\ProductFeatureTypeLanguage;
use App\ProductFeatureLanguage;
use App\ProductFeatureOptionLanguage;
use App\Warehouse;
use App\ProductStock;
use App\Product as ProductModel;

class Product
{
	public function set($property, $content)
	{
		$this->$property = $content;
		return $this;
	}

	public function sync()
	{
		$this->product = ProductModel::updateOrCreate(
			['sku'	=> $this->request->sku ],
			[
	            'brand_id'                      => $this->getBrand(),
	            'product_group_id'              => $this->getProductGroup(),
	            'active'                        => $this->request->active=='True'?true:false,
	            'supplier_sku'                  => $this->request->supplier_sku,
	            'ean'                           => $this->request->ean,
	            'upc'                           => $this->request->upc,
	            'minimum_qty'                   => $this->request->minimum_qty,
	            'stock_management'              => $this->request->stock_management?true:false,
	            'deny_order_when_out_of_stock'  => $this->request->deny_order ?? true,
	            'width'                         => (float)($this->request->package_width ?? 0),
	            'height'                        => (float)($this->request->package_height ?? 0),
	            'depth'                         => (float)($this->request->package_depth ?? 0),
	            'weight'                        => (float)($this->request->package_weight ?? 0),
	            'additional_fee_per_item'       => $this->request->additional_fee_per_item ?? 0
        	]
        );
		// removed
		$this->insertPrice()->setProductInfo()->setShop()->setFeatures()->setQty();

		if (isset($this->request->group)){
			$group = Group::firstOrCreate(['name' => $this->request->group]);
			$this->product->groups()->sync([$group->id]);
		}


		return $this;
	}

	private function getBrand()
	{
		if($this->request->brand){
            $brand = Brand::firstOrCreate(["name" => title_case($this->request->brand)]);

            return $brand->id;
        }

        return null;
	}

	private function getProductGroup($get_model = false)
	{
		if($this->request->product_group)
        {
            $product_group = ProductGroup::where(['name' => $this->request->product_group])->firstOrFail();
            if($get_model){
                return $product_group;
            }
            return $product_group->id;
        }

        return null;
	}

	private function insertPrice()
	{
        ProductPrice::where('product_id', $this->product->id)->delete();
		ProductPrice::create([
            'product_id'        => $this->product->id,
            'purchasing_price'  => $this->request->purchasing_price,
            'retail_price'      => $this->request->retail_price,
            'list_price'        => $this->request->list_price,
            'unit_name'         => $this->request->unit_name,
            'unit_price'        => $this->request->unit_price,
        ]);

		return $this;
	}

	private function setProductInfo()
	{
		foreach($this->request->product_infos as $info)
        {
            $product_info = ProductInfo::updateOrCreate([
                'language_id'       => (Language::getLanguage($info["language"]))->id,
                'product_id'        => $this->product->id,
                'name'              => $info["product_name"],
                'short_description' => $info["meta_description"] ?? '',
                'description'       => $info["description"] ?? '',
                'specification'     => $info["specification"] ?? '',
                'link'              => $info["meta_link"] ?? ''
            ]);

            if($info["tags"])
            {
                foreach(explode(",", $info["tags"]) as $tag)
                {
                    $product_tag = ProductTag::firstOrCreate(['language_id' => $product_info->language_id, 'name' => $tag]);
                    $product = Product::find($this->product->id);
                    $product->product_tags()->attach($product_tag->id);
                }
            }
        }

        return $this;
	}

	private function setFeatures()
	{
		$this->product->product_feature_options()->detach();
		$this->product->product_feature_type()->associate(null)->save();

		if($this->request->features && isset($this->request->features["type"]))
        {
        	$this->setFeatureType();

	    	foreach($this->request->features["options"] as $feature_name => $value)
	    	{

	            if($value)
	            {
	            	$this->setFeature($feature_name, $value);
	            }
	    	}

	    	if(isset($this->feature_options))
	    	{
	    		$this->product->product_feature_options()->sync($this->feature_options);
	    	}

        }

		return $this;
	}

	private function setFeatureType()
	{
        $productGroup = $this->getProductGroup(true);
        $feature_type = null;
        if($productGroup){
            $feature_type = $productGroup->productFeatureType;
        }

		$this->product->product_feature_type()->associate($feature_type)->save();

		$this->set('feature_type', $feature_type);

		return $this;
	}

	private function setFeature($name, $value)
	{
		$product_feature = ProductFeature::whereHas('info', function($query) use ($name){
			$query->language(2)->where('name', title_case($name));
		})->first();
		if(!$product_feature)
		{
			$product_feature = ProductFeature::create([
				"product_feature_type_id"   => $this->feature_type->id
			]);

			ProductFeatureLanguage::create([
				"product_feature_id"    => $product_feature->id,
				"language_id"           => 2,
				"name"                  => title_case($name)
			]);
		}

		$feature_option = ProductFeatureOption::whereHas('info', function($query) use ($value){
	                            $query->language(2)->where('name', title_case($value));
	                        })->first();
		if(!$feature_option)
		{
			$feature_option = ProductFeatureOption::create([
	            "product_feature_id"    => $product_feature->id
	        ]);

	        ProductFeatureOptionLanguage::create([
	            "product_feature_option_id" => $feature_option->id,
	            "language_id"               => 2,
	            "name"                      => title_case($value)
	        ]);
		}

		$this->feature_options[] = $feature_option->id;

		return $this;
	}

	private function setQty()
	{
		if($this->request->quantities)
		{
			foreach($this->request->quantities["warehouses"] as $warehouse)
			{
				$warehouse_model = Warehouse::name($warehouse['name'])->first();

				if($warehouse_model)
				{
                    ProductStock::where("warehouse_id", $warehouse_model->id)->where("product_id", $this->product->id)->delete();
					ProductStock::create([
						"warehouse_id"	=> $warehouse_model->id,
						"product_id"	=> $this->product->id,
						"amount"		=> $warehouse['amount']
					]);
				}
			}
		}
	}

	private function setShop()
	{
		if(isset($this->request->shops))
		{
			foreach($this->request->shops as $shop)
			{
				$shop = Shop::firstOrCreate([
					'name'	=> $shop['name']
				]);

				$shop->products()->attach($this->product->id);
			}
		}

		return $this;
	}
}
