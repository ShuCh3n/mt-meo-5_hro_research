<?php
namespace App\Classes\WebHooks;

use App\VehicleYear;
use App\Product;
use App\Motor;

class WooCommerce extends WebHook
{
    public function __construct($model, $webhook, $event)
    {
        $this->model = $model;
        $this->webhook = $webhook;
        $this->event = $event;
        $this->headers = ['base_uri' => $this->webhook->url, 'verify' => false , 'headers' => [ 'content-type' => 'application/json']];

        $auth = [$webhook->username,$webhook->password];

        $client =  new \GuzzleHttp\Client($this->headers);

        switch (get_class($model)) {
            case 'App\Product':
                $this->setTemplate('product');
                $this->model = $model->load('info');
                try {
                    $response = $client->get('/wp-json/wc/v1/products?sku=' . $model->sku, ['verify' => false, 'auth' => $auth]);
                } catch (\GuzzleHttp\Exception\ClientException $e) {
                    return;
                }
                $product = collect(json_decode($response->getBody()))->first();
                $this->setUrl('/wp-json/wc/v1/products/'.$product->id);
                $this->setAditional($product);
                break;
            case 'App\Motor':
                $this->setTemplate('motor');
                $this->setUrl('/wp-json/vehicles/v1/vehicle');
                break;
            case 'App\CategoryMotorProductVehicleYear':
                $product = Product::findOrFail($model->product_id);
                $this->setTemplate('motor_product');
                $this->setUrl('/wp-json/vehicles/v1/tag/'.$product->sku);
                break;
        }

        $this->setAuth($auth);
    }

    public function format($template = null)
    {

        switch ($template) {
            case 'product':
                return collect([
                    "sku" => (string) $this->model->sku,
                    "name" => (string) $this->model->info->name,
                    "reviews_allowed"=> "false",
                    "price" => (string) $this->model->prices->retail_price,
                    "regular_price" => (string) $this->model->prices->retail_price,
                    "manage_stock" => (string) true,
                    "short_description" => (string) $this->model->info->short_description,
                    "description" => (string) $this->model->info->description,
                    "stock_quantity" => (string) $this->model->total_stocks(),
                ]);
                break;
            case 'motor':
                return collect([
                    "sku" => (string) $this->model->code,
                    "brand" => (string) $this->model->vehicle_brand->name,
                    "vehicle_name" => (string) $this->model->name,
                    "vehicle_type" => (string) $this->model->type,
                    "build_year_from" => (string) $this->model->vehicle_years->min('year'),
                    "build_year_to" => (string) $this->model->vehicle_years->max('year'),
                    "cc" => (string) $this->model->cc
                ]);
                break;
            case 'motor_product':
                $product = Product::findOrFail($this->model->product_id);
                $motor = Motor::findOrFail($this->model->motor_id);
                $year = VehicleYear::findOrFail($this->model->vehicle_year_id);
                return collect([
                    "sku" => (string) $product->sku,
                    "tag" => (string) $motor->code . '_' . $year->year
                ]);
                break;
        }
    }
}
