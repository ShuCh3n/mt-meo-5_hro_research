<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

use App\Http\Requests;

use App\Shop;
use App\Brand;
use App\Product;
use App\Language;
use App\Supplier;
use App\TaxGroup;
use App\Warehouse;
use App\ProductTag;
use App\ShopProduct;
use App\ProductInfo;
use App\ProductImage;
use App\ProductPrice;
use App\ProductStock;
use App\ProductFeature;
use App\ProductShopInfo;
use App\ProductBulkPrice;
use App\ProductAttribute;
use App\ProductFeatureType;
use App\ProductCombination;
use App\ProductFeatureOption;
use App\Platform;
use App\Classes\ProductGraph;
use App\Classes\ProductImport;
use App\Classes\ShopProductPrice;

use File;
use Storage;
use Image;
use Log;
use DB;
use Excel;
use Uuid;

class ProductController extends Controller
{
    public function grabProductFilters()
    {
        $this->filters = null;
        $this->sort = null;
        $this->sort_type = null;
        $this->per_page = 100;

        if($this->request->filters)
        {
            $this->filters = $this->request->filters;
        }

        if($this->request->per_page)
        {
            $this->per_page = $this->request->per_page;
        }

        if($this->request->sort)
        {
            $this->sort = $this->request->sort;
            $this->sort_type = $this->request->sort_type;
        }
    }

    public function ProductsWithImages()
    {
        $products = Product::with('images');
        if($this->request->input('filter')){
            $products->where('sku', 'LIKE', '%'.$this->request->input('filter').'%');
        }
        return $products->limit(100)->get();
    }

    public function products()
    {
        $shops = Shop::all();
        $product_count = Product::count();

        return view('dashboard.products', compact('shops', 'product_count'));
    }

    public function shopProducts()
    {
        $shop = null;
        $this->grabProductFilters();

        if(is_numeric($this->request->shop_id))
        {
            $shop = Shop::findOrFail($this->request->shop_id);

            $products = Product::whereHas('shop_products', function($query) use ($shop){
                $query->where('shop_id', $shop->id);
                })->with(['shop_prices' => function($query) use ($shop){
                    $query->where('shop_id', $shop->id)->latest()->first();
                }])->with('shop_products')->ProductFilter($shop->id);

                $currentPage = \Illuminate\Pagination\LengthAwarePaginator::resolveCurrentPage();
                $products = new \Illuminate\Pagination\LengthAwarePaginator($products->skip(($currentPage-1)*100)->take(100)->get(), 2000,100, $currentPage);
                $products->setPath($shop->id);
        }
        else
        {
            $products = Product::with('product_price')->filter($this->filters)->orderBy('created_at', 'desc')->paginate($this->per_page);
        }

        return view('dashboard.shop_products', compact('shop', 'products'));
    }

    public function newProduct()
    {
        $shops = Shop::paginate(100);
        $brands = Brand::all();
        $tax_groups = TaxGroup::all();
        $warehouses = Warehouse::all();

        $product_feature_types = ProductFeatureType::with(['info' => function($query){
            $query->backOffice();
        }])->get();

        $languages = Language::all();
        $attributes = ProductAttribute::all();

        return view('dashboard.new_product', compact('shops', 'brands', 'languages', 'product_feature_types', 'warehouses', 'attributes', 'tax_groups'));
    }

    public function editProductSku(){
        $product = Product::where('sku', $this->request->sku)->firstOrFail();
        return $this->editProduct($product->id);
    }

    public function editProduct($product_id = null)
    {
        $shop = null;
        $brands = null;

        if($this->request->shop)
        {
            $shop = Shop::findOrFail($this->request->shop);

            $product = Product::with(['shop_product' => function($query) use ($shop){
                $query->where('shop_id', $shop->id)->with('tax_rule');
            }])->with(['shop_price' => function($query) use ($shop){
                $query->where('shop_id', $shop->id)->latest()->first();
            }])->with(['shop_info' => function($query) use ($shop){
                $query->where('shop_id', $shop->id)->first();
            }])->featureInfo(auth()->user()->preferenceLanguage())->findOrFail($this->request->id);
        }
        else
        {
            if(!$product_id){
                $product = Product::findOrFail($this->request->id);
            }else{
                $product = Product::findOrFail($product_id);
            }
            $brands = Brand::all();
        }

        $product->tags();
        $product->getStocks();
        $product->getSelectedFeatureOptions();

        $shops = Shop::all();

        $tax_groups = TaxGroup::all();
        $warehouses = Warehouse::all();
        $product_feature_types = ProductFeatureType::all();
        $languages = Language::all();
        $attributes = ProductAttribute::all();

        return view('dashboard.edit_product', compact('shops', 'product', 'tax_groups', 'brands', 'warehouses', 'product_feature_types', 'languages', 'attributes', 'shop'));
    }
    
    public function productFiles($product_id)
    {
        $product = Product::findOrFail($product_id);
        return view('dashboard.product_files', compact('product'));
    }

    public function productFileUpload($product_id)
    {

        if ($this->request->hasFile('file')) {
            $file = $this->request->file('file');
            $msds = new \App\ProductMaterialSafetyDataSheet();
            $uuid = Uuid::generate(4);
            $msds->filename = $uuid;
            $msds->filable_id = $product_id;
            $msds->filable_type = 'App\Product';
            $msds->extension = 'unknown';
            $msds->extension = $file->extension();
            //Storage::put('cdn/files/'.$uuid.'.'.$file->extension(), $file, 'public');
            $file->storePubliclyAs('cdn/files', $uuid.'.'.$file->extension(), 'gcs', 'public');
            $msds->save();
            return 'true';
        }
    }

    public function productFileUploadSku()
    {

        if ($this->request->hasFile('file')) {
            $file = $this->request->file('file');
            $sku = explode('_', $file->getClientOriginalName())[0];
            $product = Product::where('sku', $sku)->first();
            if($product){
                $msds = new \App\ProductMaterialSafetyDataSheet();
                $uuid = Uuid::generate(4);
                $msds->filename = $uuid;
                $msds->filable_id = $product->id;
                $msds->filable_type = 'App\Product';
                $msds->extension = 'unknown';
                $msds->extension = $file->extension();
                //Storage::put('cdn/files/'.$uuid.'.'.$file->extension(), $file, 'public');
                $file->storePubliclyAs('cdn/files', $uuid.'.'.$file->extension(), 'gcs', 'public');
                $msds->save();
                return 'true';
            }
            return 'false';

        }
    }

    public function actionNewProduct()
    {
    	$product = new Product;
	    safeFill($product, $this->request->input());

        $tag_ids = array();
        foreach($this->request->product_info as $language_id => $info)
        {
            if(is_numeric($language_id))
            {
                $product_info = new ProductInfo;
                $product_info->language_id = $language_id;
                $product_info->product_id = $product->id;

                $product_info->setProductInfo($info);

                if($info["tags"])
                {
                    foreach(explode(",", $info["tags"]) as $tag)
                    {
                        $product_tag = ProductTag::firstOrCreate(['language_id' => $language_id, 'name' => $tag]);
                        $tag_ids[] = $product_tag->id;
                    }
                }
            }
        }

        $product->assignTag($tag_ids);

        $product_price = new ProductPrice;
        $product_price->setProductPrice($product, $this->request);

        if($this->request->product_features)
        {
            $ids = array();

            foreach($this->request->product_features as $feature_option_id)
            {
                if(!empty($feature_option_id))
                {
                    $feature_option = ProductFeatureOption::findOrFail($feature_option_id);

                    if($feature_option->product_feature->product_feature_type->id == $product->product_feature_type_id)
                    {
                        $ids[] = $feature_option->id;
                    }
                }
            }

            $product->assignFeature($ids);
        }

        $product->assignShop($this->request->shops);

        foreach($this->request->category as $categories)
        {
            $product->assignCategory($categories);
        }

        if($product->stock_management && $this->request->stock)
        {
            foreach($this->request->stock as $warehouse_id => $stock)
            {
                $warehouse = Warehouse::findOrFail($warehouse_id);

                $product_stock = new ProductStock;
                $product_stock->setProductStock($product, $warehouse, $stock ?? 0);
            }
        }

        if($this->request->combinations)
        {
            foreach($this->request->combinations as $combination_info)
            {
                $combination = new ProductCombination;
                $combination->setProductCombination($product, json_decode($combination_info));
            }
        }

    	return redirect()->route('edit_product', $product->id);
    }

    public function actionEditProduct()
    {
        $product = Product::findOrFail($this->request->id);

        if($this->request->shop)
        {
            $shop_product = Shop::findOrFail($this->request->shop)->shop_products()->where('product_id', $product->id)->first();

            if(!$shop_product)
            {
                $shop_product = new ShopProduct;
            }

            $shop_product->setProduct($this->request);

            return redirect()->route('edit_product', [$product->id, "shop" => $this->request->shop]);
        }

        safeFill($product, $this->request->input());
        $tag_ids = array();

        foreach($this->request->product_info as $language_id => $info)
        {
            if(is_numeric($language_id))
            {
                $product_info = new ProductInfo;
                $product_info->language_id = $language_id;
                $product_info->product_id = $product->id;

                $product_info->setProductInfo($info);

                if($info["tags"])
                {
                    foreach(explode(",", $info["tags"]) as $tag)
                    {
                        $product_tag = ProductTag::firstOrCreate(['language_id' => $language_id, 'name' => $tag]);
                        $tag_ids[] = $product_tag->id;
                    }
                }
            }
        }

        $product->assignTag($tag_ids);

        $product_price = new ProductPrice;
        $product_price->setProductPrice($product, $this->request);

        if($this->request->product_features)
        {
            $ids = array();

            foreach($this->request->product_features as $feature_option_id)
            {
                if(!empty($feature_option_id))
                {
                    $feature_option = ProductFeatureOption::findOrFail($feature_option_id);

                    if($feature_option->product_feature->product_feature_type->id == $product->product_feature_type_id)
                    {
                        $ids[] = $feature_option->id;
                    }
                }
            }

            $product->assignFeature($ids);
        }
        if($this->request->shops)
        {
            foreach($this->request->shops as $shop){
                $shopProduct = ShopProduct::firstOrNew(['shop_id' => $shop ,'product_id' => $product->id]);
                $shopProduct->tax_rule_id = $product->tax_rule_id;
                $shopProduct->minimum_qty = $product->minimum_qty;
                $shopProduct->stock_management = $product->stock_management;
                $shopProduct->active_when_out_of_stock = $product->active_when_out_of_stock;
                $shopProduct->deny_order_when_out_of_stock = $product->deny_order_when_out_of_stock;
                $shopProduct->additional_fee_per_item = $product->additional_fee_per_item;
                $shopProduct->active = true;
                $shopProduct->save();

                $info = $product->infos->where('language_id', 2)->first();

                $shopProductInfo = ProductShopInfo::firstOrNew(['shop_id' => $shop ,'product_id' => $product->id]);
                $shopProductInfo->language_id = 2;
                $shopProductInfo->name = $info->name;
                $shopProductInfo->short_description = $info->short_description;
                $shopProductInfo->description = $info->description;
                $shopProductInfo->specification = $info->specification;
                $shopProductInfo->link = $info->link;
                $shopProductInfo->save();

                //$product->assignShop($this->request->shops);
            }
        }

        if($this->request->category)
        {
            foreach($this->request->category as $categories)
            {
                $product->assignCategory($categories);
            }
        }

        if($product->stock_management && $this->request->stock)
        {
            foreach($this->request->stock as $warehouse_id => $stock)
            {
                $warehouse = Warehouse::findOrFail($warehouse_id);

                $product_stock = new ProductStock;
                $product_stock->setProductStock($product, $warehouse, $stock ?? 0);
            }
        }

        if($this->request->combinations)
        {
            foreach($this->request->combinations as $combination_info)
            {
                $combination = new ProductCombination;
                $combination->setProductCombination($product, json_decode($combination_info));
            }
        }

        return redirect()->route('edit_product', $product->id);
    }

    public function actionUploadImage()
    {
        $product = Product::findOrFail($this->request->id);
        $rank = $product->getImageRank();

        $i = $product->images->count() + 1;
        $filename = $product->id . '_' . $i;
        $extension = 'jpg';

        if($this->request->file)
        {
            $file = File::get($this->request->file);

            $image = Image::make($file);
            $large_file = Image::make($file)->fit(600, 600)->stream($extension, 100)->__toString();
            $medium_file = Image::make($file)->fit(300, 300)->stream($extension, 75)->__toString();
            $small_file = Image::make($file)->fit(140, 140)->stream($extension, 75)->__toString();

            Storage::put('products/original/' . $filename . '.' . $extension, $image->stream($extension, 100)->__toString(), 'public');
            Storage::put('products/' . $filename . '_large.' . $extension, $large_file, 'public');
            Storage::put('products/' . $filename . '_medium.' . $extension, $medium_file, 'public');
            Storage::put('products/' . $filename . '_small.' . $extension, $small_file, 'public');

            $product_image = new ProductImage;
            $product_image->product_id = $product->id;
            $product_image->image_link = $filename;
            $product_image->rank = $rank;
            $product_image->save();

            return array(
                "status"    => true,
                "file_id"   => $product_image->id,
                "name"      => $product->sku . ".jpg"
            );
        }
    }

    public function reoderImage()
    {
        $product = Product::findOrFail($this->request->id);
        $rank = 10;

        foreach(json_decode($this->request->order) as $image)
        {
            $image = $product->images()->where('id', $image->id)->first();
            $image->rank = $rank;
            $image->save();

            $rank = $rank + 10;
        }

        return array("status" => true);
    }

    public function findProduct()
    {
        $products = Product::orWhere('id', 'like', '%' . $this->request->q . '%')
                    ->orWhere('ean', 'like', '%' . $this->request->q . '%')
                    ->orWhere('sku', 'like', '%' . $this->request->q .'%')
                    ->orWhereHas('infos', function($query){
                        $query->where('name', 'like', '%' . $this->request->q . '%');
                    })
                    ->with(['images', 'shops', 'info'])
                    ->get();

        return array('results' => $products);
    }

    public function removeImage()
    {
        $product_image = Product::findOrFail($this->request->product_id)->images()->where('id', $this->request->id);
        $product_image->delete();
    }

    public function importFromFile()
    {
        $products = $this->request->import;
        $total_products = count(json_decode($products));

        return view('dashboard.product_importer_execute', compact('products', 'total_products'));
    }

    public function importProduct()
    {
        $import = new ProductImport((object) $this->request->product);
        $import->setShop(Shop::getShop($import->request->shop));
        $import->setLanguage(Language::getLanguage($import->request->language));

        $import->setProduct(Product::where('sku', $import->request->sku)->featureInfo($import->language->id)->with('brand')->with(['shop_info' => function($query) use ($import){
            $query->where('shop_id', $import->shop->id);
        }])->first());

        if($import->isImportable())
        {
            $import->attachCategory();
            $import->importFeatures();
            $import->importBrand();
            $import->addShopProduct();
            $import->importProductShopInfo();
            $import->updateShopPrice();
        }

        return array("status" => true);
    }

    public function bulkEditProduct()
    {
        $product_feature_types = ProductFeatureType::all();
        $edit_products = $this->request->product;

        return view('dashboard.product_bulk_edit', compact('product_feature_types', 'edit_products'));
    }

    public function actionBulkEditProduct()
    {
        $products = Product::whereIn('id', $this->request->products)->get();

        foreach($products as $product)
        {
            if($this->request->active)
            {
                $product->active = $this->request->active;
            }

            if(!empty($this->request->feature))
            {
                foreach($this->request->feature as $feature_id)
                {
                    $option_id = $this->request->product_feature_options[$feature_id];

                    $product->syncFeatureOption($option_id);
                }
            }
        }

        return redirect()->route('products');
    }

    public function viewProduct()
    {
        $product = Product::with('platforms')->findOrFail($this->request->id);
        $platforms = Platform::all();
        //$productChartData = $product->chartData();

        return view('dashboard.view_product', compact('product', 'platforms'));
    }

    public function productGraph()
    {
        $product = Product::with('platforms')->findOrFail($this->request->id);
        $platforms = $this->request->platforms;
        $graph = new ProductGraph(array(2016, 2017), $product, $platforms);

        return $graph->result();
    }

    public function searchProduct()
    {
        $paginate = 32;

        $filter = array();
        $brand_array = array();
        $skus = array();

        $search_query = $this->request["query"];
        $brands = $this->request["brands"];
        $categories_ids = $this->request["category_ids"];

        if($this->request["features"])
        {
           $filter["options"] = $this->request["features"];
           if(!is_array($filter["options"])){
           	$filter["options"] = json_decode($filter["options"]);
           }
        }

        if($this->request["prices"])
        {
           $filter["price"] = $this->request["prices"];
        }

        $sort = false;

        if($this->request->sort)
        {
            $sort = $this->request->sort;
            $sort_type = $this->request->sort_type;
        }

        if($this->request["skus"])
        {
            $skus = $this->request["skus"];
        }

        if($this->request["paginate"] && $this->request["paginate"] <= 50)
        {
            $paginate = $this->request->paginate;
        }

        if($brands)
        {
            if(!is_array($brands)){
                $brands = json_decode($brands);
            }

            foreach($brands as $brand)
            {
                $get_brand = Brand::getBrand($brand);

                if($get_brand)
                {
                    $brand_array[] = $get_brand->id;
                }
            }
        }

        $result = $this->request->shop->shop_products()->active()->whereHas('product', function($query) use ($brand_array, $search_query, $filter, $categories_ids, $skus){
            $query->ProductFilter($this->request->shop->id);

            if(!empty($brand_array))
            {
                $query->whereIn('brand_id', $brand_array);
            }

            if($search_query)
            {
                $query->whereHas('shop_infos', function($q) use ($search_query){
                    $q->where('shop_id', $this->request->shop->id)->where('language_id', $this->request->language->id)->where('name', 'like', '%' . $search_query . '%');
                });
            }

            if(!empty($filter))
            {
                $query->filter($filter);
            }

            if(!empty($categories_ids))
            {
                if(!is_array($categories_ids)){
                    $categories_ids = json_decode($categories_ids);
                }
                foreach($categories_ids as $category_id)
                {
                    $query->whereHas('categories', function($q) use ($category_id){
                        $q->where('id', $category_id);
                    });
                }
            }

            if(!empty($skus))
            {
                $query->whereIn('sku', $skus);
            }

        })->with(['product' => function($query){
            $query->with(['product_price', 'product_stocks', 'categories'])->shopInfo($this->request->shop->id, $this->request->language->id)->featureInfo($this->request->language->id);
         }])->leftJoin('product_shop_prices', function ($join){
             $join->on('shop_products.product_id', 'product_shop_prices.product_id')->where('product_shop_prices.shop_id', $this->request->shop->id)->where('product_shop_prices.deleted_at','=', null);
         })->select('shop_products.*');

        if($sort)
        {
            if($sort_type == "asc")
            {
                $result = $result->orderBy($sort, $sort_type);
            }

            if($sort_type == "desc")
            {
                $result = $result->orderBy($sort, $sort_type);
            }
        }
        //dd($result->toSql());
        $result = $result->paginate($paginate);
        foreach($result as $r)
        {
            $r->product->prices = new ShopProductPrice($r->product, $this->request->shop);
            $r->product->logo = [];
            $r->product->total_stocks = $r->product->total_stocks();

            if(isset($r->product->brand) && $r->product->brand->image->first())
            {
                $r->product->logo = [
                    'small' => env('CDN_URI') . '/images/brands/' . $r->product->brand->image->first()->filename . '_small.png',
                    'medium' => env('CDN_URI') . '/images/brands/' . $r->product->brand->image->first()->filename . '_medium.png',
                    'large' => env('CDN_URI') . '/images/brands/' . $r->product->brand->image->first()->filename . '_large.png'
                ];
            }

            $imagesSource = $r->product->allimages($this->request->shop->id);

            $images = [];

            foreach($imagesSource as $image){
                $images[] = array(
                    'img' => env('CDN_URI') . '/images/' . $image->filename . '.png',
                    'thumb' => env('CDN_URI') . '/images/' . $image->filename . '_thumb.png'
                );
            }

            $r->product->images = $images;
        }

        return $result;
    }

    public function getProduct()
    {
        $shop_product = ShopProduct::where('shop_id', $this->request->shop->id)
                        ->where('product_id', $this->request->id)
                        ->with(['product' => function($query){
                            $query->shopInfo($this->request->shop->id, $this->request->language->id)->featureInfo($this->request->language->id);
                            $query->with('images');
                        }])
                        ->firstOrFail();

        $shop_product->product->prices = new ShopProductPrice($shop_product->product, $this->request->shop);

        $imagesSource = $shop_product->product->allimages($this->request->shop->id);

        if($shop_product->product->brand && $shop_product->product->brand->image->count() > 0)
        {
			$shop_product->product->logo = [
				'small' => env('CDN_URI') . '/images/brands/' . $shop_product->product->brand->image->first()->filename . '_small.png',
				'medium' => env('CDN_URI') . '/images/brands/' . $shop_product->product->brand->image->first()->filename . '_medium.png',
				'large' => env('CDN_URI') . '/images/brands/' . $shop_product->product->brand->image->first()->filename . '_large.png'
			];
		}
        else
        {
			$shop_product->product->logo = [];
		}

        $images = [];

        foreach($imagesSource as $image){
            $images[] = array(
                'img' => env('CDN_URI') . '/images/' . $image->filename . '.png',
                'thumb' => env('CDN_URI') . '/images/' . $image->filename . '_thumb.png'
            );
        }
        $shop_product->images = $images;

        $shop_product->product->category_id = $shop_product->product->categories->where('shop_id', $this->request->shop->id)->first()?$shop_product->product->categories->where('shop_id', $this->request->shop->id)->first()->id:0;

        return $shop_product;
    }
}
