<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use DB;
use App\Product;
use App\Classes\ProductFilters;
use App\Classes\ShopProductPrice;
use Illuminate\Pagination\LengthAwarePaginator;

class CategoryController extends Controller{

	public function categories()
	{
		$categories = $this->request->shop->categories()->with(['info' => function($query){
			$query->language($this->request->language->id);
		}])->get();

        return $categories;
	}

	public function categoryProducts()
	{
		$filters = null;
		$paginate = 32;
		$sort = null;
		$sort_type = null;
		$current_page = $this->request->page ?? 1;

		if($this->request->filters)
		{
			$filters = $this->request->filters;
		}

		if($this->request->paginate && $this->request->paginate <= 50)
		{
			$paginate = $this->request->paginate;
		}

		if($this->request->sort)
		{
			$sort = $this->request->sort;
			$sort_type = $this->request->sort_type;
		}

        $category = $this->request->shop->categories()->where('id', $this->request->id)->first();
		$category->info = $category->multilanguage->language($this->request->language->id)->info();

        $category_products = $this->getAllCategoryProducts($category, $filters);
        $num_products = $category_products->count();

        if($sort)
        {
            if($sort_type == "asc")
            {
                $category_products = $category_products->orderBy($sort, $sort_type);
            }

            if($sort_type == "desc")
            {
                $category_products = $category_products->orderBy($sort, $sort_type);
            }
        }

        $category->products = new LengthAwarePaginator($category_products->take($paginate)->skip(($current_page-1)*$paginate)->get(), $num_products, $paginate, $current_page);
		foreach($category->products as $product)
		{

            $prices = new ShopProductPrice($product, $this->request->shop);

            $product->prices = $prices;


			if($product->brand->image->first())
			{
				$product->logo = [
					'small' => env('CDN_URI') . '/images/brands/' . $product->brand->image->first()->filename . '_small.png',
					'medium' => env('CDN_URI') . '/images/brands/' . $product->brand->image->first()->filename . '_medium.png',
					'large' => env('CDN_URI') . '/images/brands/' . $product->brand->image->first()->filename . '_large.png'
				];
			}
			else
			{
				$product->logo = [];
			}

			$imagesSource = $product->allimages($this->request->shop->id);
	        $images = [];

	        foreach($imagesSource as $image)
	        {
	            $images[] = array(
	                'img' => env('CDN_URI') . '/images/' . $image->filename . '.png',
	                'thumb' => env('CDN_URI') . '/images/' . $image->filename . '_thumb.png'
	            );
	        }

        	$product->images = $images;

        	$paginate_product = $category->products->where('product_id', $product->id)->first(function($value, $key) use ($category, $product){
				$category->products[$key] = $product;
			});
		}
		return $category;
	}

	public function categoryFilters()
	{
		$category = $this->request->shop->categories()->where('id', $this->request->id)->first();
        $this->grabProductFilters();
		$filters = new ProductFilters($category->products()->Filter($this->filters)->featureInfo($this->request->language->id)->with('product_price'), $this->request->language->id, $this->request->shop->id);
		return array("filters" => $filters);
	}

	private function getAllCategoryProducts($category, $filters)
	{
		if(!$filters)
		{
			$filters = array();
		}

		$products = array();

		$category_products = $category->products()->featureInfo($this->request->language->id)->ProductFilter($this->request->shop->id)->shopFilter($filters, $this->request->shop->id)->shopInfo($this->request->shop->id, $this->request->language->id)->shopActive($this->request->shop->id)->with(['product_price'])->leftJoin('brands', 'brand_id', 'brands.id')->leftJoin('product_shop_prices', function ($join){
		    $join->on('products.id', 'product_shop_prices.product_id')->where('product_shop_prices.shop_id', $this->request->shop->id)->where('product_shop_prices.deleted_at','=', null);
        });

        return $category_products;
	}
}
