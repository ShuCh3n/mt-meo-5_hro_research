<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryMotorProductVehicleYear extends Model
{
    protected $table = 'category_motor_product_vehicle_year';

    public function vehicle_year()
    {
        return $this->belongsTo(VehicleYear::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function motor()
    {
        return $this->belongsTo(Motor::class);
    }
}
