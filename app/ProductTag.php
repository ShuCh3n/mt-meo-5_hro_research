<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductTag extends Model
{
	protected $fillable = ['language_id', 'name'];
	
	public function products()
	{
		return $this->belongsToMany(Product::class);
	}

    public function assignProduct($product)
    {
    	return $this->products()->save($product);
    }
}
