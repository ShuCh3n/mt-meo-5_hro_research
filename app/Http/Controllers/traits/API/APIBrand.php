<?php

namespace App\Http\Controllers\traits\API;

use App\Brand;

trait APIBrand {
    public function getBrand(){
        $brand = Brand::with('image')->findOrFail($this->request->id);
        $imagesColl = $brand->image->unique('filename');
        unset($brand->image);
        $images = [];
        foreach($imagesColl as $image){
            $images[] = array(
                'small' => env('CDN_URI') . '/images/brands/' . $image->filename . '_small.png',
                'medium' => env('CDN_URI') . '/images/brands/' . $image->filename . 'medium.png'
            );
        }
        $brand->images = $images;
        return $brand;
    }
}
