<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AnalistFile extends Model
{
    public function user()
    {
    	return $this->belongsTo(User::class);
    }
}
