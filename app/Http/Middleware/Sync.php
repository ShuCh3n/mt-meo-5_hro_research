<?php

namespace App\Http\Middleware;

use Closure;

class Sync
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(env('APP_ENV') != 'production' || $request->header('SyncToken') ==  "EewsZm9MuJLEc4ZgBJfqAkIeIMEdz3RDQZKsmVgG")
        {
            return $next($request);
        }
        
        return response(array('status' => false));
    }
}
