@extends('dashboard.layout.main')

@section('stylesheet')
    <link href="/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
@stop

@section('javascript')
    <!-- iCheck -->
    <script src="/js/plugins/iCheck/icheck.min.js"></script>

    <!-- Data picker -->
   <script src="/js/plugins/datapicker/bootstrap-datepicker.js"></script>

    <script>
        $(function(){
            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });

            $('#generate_password').click(function(){
            	$('#new_password').val(randomString(8));
            });

			$('#dob .input-group.date').datepicker({
				startView: 2,
				todayBtn: "linked",
				keyboardNavigation: false,
				forceParse: false,
				autoclose: true
			});

            $(".select2.tax_groups").on("select2:select", function (e) { 
                $.get('/ajax/tax_group/' + $(this).val() + '/tax_rules', function(result){

                    $('.select2.tax_rules').html('');

                    for(var i = 0, len = result.length; i < len; i++)
                    {
                        $('.select2.tax_rules').append($("<option>").val(result[i].id).html(result[i].name));
                    }

                    $('.select2.tax_rules').select2();
                });
            });
        });
    </script>
@stop

@section('content')
	<div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>{{ trans('pages.inquiry_detail_header') }}</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('home') }}">{{ trans('menu.home') }}</a>
                </li>
                <li>
                    <a href="">{{ trans('menu.inquiries') }}</a>
                </li>
                <li class="active">
                    <strong>{{ trans('page.inquiry') }}</strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">
        {!! session()->has('success')? '<div class="alert alert-success alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>' . session('success') . '</div>' : '' !!}

        @if (count($errors) > 0)
            <div class="alert alert-danger alert-dismissable">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <form id="profileForm" method="post" class="form-horizontal">
            <div class="row">
                <div class="col-md-8">
                    <div class="ibox">

                        <div class="ibox-title">
                            <h5>New Customer</h5>
                        </div>

                        <div class="ibox-content">
                            <div class="mb20"></div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Shop<span class="text-danger">*</span></label>
                                        <div class="col-md-10">
                                            <select class="select2 form-control" name="shop">
                                                <option value="">Select shop</option>
                                                @foreach($shops as $shop)
                                                    <option value="{{ $shop->id }}" {{ (old('shop') == $shop->id)? "selected" : "" }}>{{ $shop->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Active<span class="text-danger">*</span></label>
                                        <div class="col-md-5">
                                            <label><input type="radio" class="form-control i-checks" name="active" value="1" checked> Enabled</label>
                                        </div>
                                        <div class="col-md-5">
                                            <label><input type="radio" class="form-control i-checks" name="active" value="0"> Disabled</label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Social title<span class="text-danger">*</span></label>
                                        <div class="col-md-5">
                                            <label><input type="radio" class="form-control i-checks" name="gender" value="1" {{ (old('gender') == 1)? "checked" : "" }}> Mr.</label>
                                        </div>
                                        <div class="col-md-5">
                                            <label><input type="radio" class="form-control i-checks" name="gender" value="0" {{ (old('gender') == 0)? "checked" : "" }}> Mrs.</label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Name</label>
                                        <div class="col-md-5">
                                            <input type="text" class="form-control" name="firstname" placeholder="Firstname" value="{{ old('firstname') }}">
                                        </div>
                                        <div class="col-md-5">
                                            <input type="text" class="form-control" name="lastname" placeholder="Lastname" value="{{ old('lastname') }}">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Phonenumber</label>
                                        <div class="col-md-10">
                                            <input type="text" class="form-control" name="phone" placeholder="Phone" value="{{ old('phone') }}">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Mobile number</label>
                                        <div class="col-md-10">
                                            <input type="text" class="form-control" name="mobile_phone" placeholder="Mobile Phone" value="{{ old('mobile_phone') }}">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Account<span class="text-danger">*</span></label>
                                        <div class="col-md-5">
                                            <input type="text" class="form-control" name="email" placeholder="Email" value="{{ old('email') }}">
                                        </div>
                                        <div class="col-md-5">
											<div class="input-group">
												<input type="text" class="form-control" name="password" id="new_password" value="{{ old('password') }}">
												<span class="input-group-btn"> 
													<button class="btn btn-primary" type="button" id="generate_password">Generate</button> 
												</span>
                                        	</div>
                                        </div>
                                    </div>

                                    <div class="form-group" id="dob">
                                        <label class="col-md-2 control-label">Date of birth</label>

                                        <div class="col-md-10">
                                            <div class="input-group date dob">
                                                <span class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </span>
                                                <input type="text" placeholder="Date of birth" name="dob" class="form-control" value="{{ old('dob') }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="ibox">
                        <div class="ibox-title">
                            <h5>Billing Address</h5>
							<div class="ibox-tools">
								<label><input type="checkbox" class="form-control i-checks" value="1" name="same_as_shipping" checked> Same as shipping address</label>
							</div>
                        </div>

                        <div class="ibox-content">
                            <div class="mb20"></div>

                            <div class="row">
                                <div class="col-md-12">
                                	<div class="form-group">
                                        <label class="col-md-2 control-label">Company</label>
                                        <div class="col-md-5">
                                            <input type="text" class="form-control" name="company" placeholder="Company">
                                        </div>
                                        <div class="mb5-sm"></div>
										<div class="col-md-5">
											<input type="text" class="form-control" name="vat_number" placeholder="VAT Number" value="">
										</div>
                                    </div>

                                    <div class="form-group">
										<label class="col-md-2 control-label">Street<span class="text-danger">*</span></label>
										<div class="col-md-5">
											<input type="text" class="form-control" name="street" placeholder="Street" value="">
										</div>
										<div class="mb5-sm"></div>
										<div class="col-md-5">
											<input type="text" class="form-control" name="housenumber" placeholder="Housenumber" value="">
										</div>
									</div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">City / Zipcode<span class="text-danger">*</span></label>
                                        <div class="col-md-5">
                                            <input type="text" class="form-control" name="city" placeholder="City" value="">
                                        </div>
                                        <div class="mb5-sm"></div>
                                        <div class="col-md-5">
                                            <input type="text" class="form-control" name="zipcode" placeholder="Zipcode" value="">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Country<span class="text-danger">*</span></label>
                                        <div class="col-md-10">
                                            <select class="form-control countries-select" name="country">
                                                <option value=""></option>
                                                @foreach($countries as $country)
                                                    <option value="{{ $country->id }}" icon="{{ $country->icon }}">{{ $country->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">State<span class="text-danger">*</span></label>
                                        <div class="col-md-10">
                                            <input type="text" class="form-control" name="state" placeholder="State" value="">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Tax</label>
                                        @if($tax_groups->count() > 0)
                                            <div class="col-md-5">
                                                <select class="select2 tax_groups form-control" name="tax_group">
                                                    <option value="">Select</option>
                                                    @foreach($tax_groups as $tax_group)
                                                        <option value="{{ $tax_group->id }}">{{ $tax_group->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-md-5">
                                                <select class="select2 tax_rules form-control" name="tax_rule">
                                                    <option value="">Select</option>
                                                </select>
                                            </div>
                                        @else
                                            <div class="col-md-10">
                                                <p class="form-control-static">
                                                    <a href="{{ route('new_tax_group') }}">Create first tax group</a>
                                                </p>
                                            </div>
                                        @endif
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="ibox">

                        <div class="ibox-title">
                            <h5>Extra info</h5>
                        </div>

                        <div class="ibox-content">
                            <div class="mb20"></div>

                            <div class="row">
                                <div class="col-md-12">

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Opt-in<span class="text-danger">*</span></label>
                                        <div class="col-md-5">
                                            <label><input type="radio" class="form-control i-checks" name="optin" value="1" {{ (old('optin') == 0)? "checked" : "" }}> Enabled</label>
                                        </div>
                                        <div class="col-md-5">
                                            <label><input type="radio" class="form-control i-checks" name="optin" value="0" {{ (old('optin') == 0)? "checked" : "" }}> Disabled</label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Group access</label>
                                        <div class="col-md-10">
                                            @if($customer_groups->count() > 0)
                                                <select class="select2 form-control" name="group">
                                                    <option value=""></option>
                                                    @foreach($customer_groups as $group)
                                                        <option value="{{ $group->id }}">{{ $group->name }}</option>
                                                    @endforeach
                                                </select>
                                            @else
                                                <p class="form-control-static">
                                                    <a href="{{ route('new_customer_group') }}">Create first customer group</a>
                                                </p>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="ibox">
                        <div class="ibox-title">
                            <h5>Action</h5>
                        </div>

                        <div class="ibox-content">
                        	<strong>Note</strong>
                        	<textarea class="form-control" placeholder="Private notes" style="height: 200px;" name="note">{{ old('note') }}</textarea>
                        	<div class="mb20"></div>
                        	<label><input type="checkbox" class="i-checks" checked> Notify Customer</label>
                        	<div class="mb20"></div>

                            {{ csrf_field() }}

                            <button class="btn btn-block btn-outline btn-success" type="submit"><i class="fa fa-floppy-o"></i> Save</button>
                            <a class="btn btn-block btn-outline btn-warning" type="submit"><i class="fa fa-envelope-o" aria-hidden="true"></i> Send message</a>
                        </div>
                    </div>
                </div>
            </div>
        </form>    
    </div>
@stop