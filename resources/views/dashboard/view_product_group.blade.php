@extends('dashboard.layout.main')

@section('stylesheet')
    <!-- Sweet Alert -->
    <link href="/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
@stop

@section('javascript')
    <!-- Sweet alert -->
    <script src="/js/plugins/sweetalert/sweetalert.min.js"></script>

    <script>
        $('.remove-from-group').click(function () {
            var delete_button = $(this);

            swal({
                title: "Are you sure?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, remove it!",
                cancelButtonText: "No, cancel please!",
                closeOnConfirm: false,
                closeOnCancel: true 
            },
            function (isConfirm) {
                if (isConfirm) {
                    $.get('/ajax/product/group/remove/' + delete_button.attr('data-product-id'), function(response){
                        if(response.status)
                        {
                            swal("Deleted!", "Product is removed from group", "success");
                            delete_button.closest('tr').remove();
                        }
                        else 
                        {
                            swal("Cancelled", "Something went wrong, please try again!", "error");
                        }
                    });
                }
            });
        });
    </script>
@stop

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-6">
            <h2>{{ trans('pages.inquiry_detail_header') }}</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('home') }}">{{ trans('menu.home') }}</a>
                </li>
                <li>
                    <a href="">{{ trans('menu.inquiries') }}</a>
                </li>
                <li class="active">
                    <strong>{{ trans('page.inquiry') }}</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-6">
            <a class="btn btn-outline btn-primary pull-right mt30" href="{{ route('new_product_group') }}"><i class="fa fa-plus-circle" aria-hidden="true"></i> New Product Group</a>
        </div>
    </div>

	<div class="wrapper wrapper-content animated fadeInRight ecommerce">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-content">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>ID</th>
                                    <th>Product Name</th>
                                    <th>SKU (Article Nr.)</th>
                                    <th>EAN</th>
                                    <th>Retail Price</th>
                                    <th>Active</th>
                                    <th class="text-right" data-sort-ignore="true">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($products as $product)
                                    <tr>
                                        <td>
                                            @if($product->images->count() > 0)
                                                <img class="img-responsive img-thumbnail" width="64" src="{{ route('product_image', [$product->images[0]->id, "small", "filname"]) }}" />
                                            @else
                                                <img class="img-responsive img-thumbnail" width="64" src="{{ route('product_image', ["no_image", "small", "filename"]) }}" />
                                            @endif
                                        </td>
                                        <td>
                                            {{ $product->id }}
                                        </td>
                                        <td>
                                            {{ $product->info->name }}
                                        </td>
                                        <td>
                                            {{ $product->sku }}
                                        </td>
                                        <td>
                                            {{ $product->ean }}
                                        </td>
                                        <td>
                                            € {{ $product->product_price->retail_price }}
                                        </td>
                                        <td>
                                            {!! ($product->active)? '<i class="fa fa-check text-navy" aria-hidden="true"></i>' : '<i class="fa fa-times text-danger" aria-hidden="true"></i>' !!}
                                        </td>
                                        <td class="text-right">
                                            <div class="btn-group">
                                                <button class="btn-white btn btn-xs remove-from-group" data-product-id="{{ $product->id }}">Remove</button>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="6">
                                        <ul class="pagination pull-right"></ul>
                                    </td>
                                </tr>
                            </tfoot>
                        </table>

                        <div class="text-center">
                            {{ $products->links() }}
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@stop