<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryInfo extends Model
{
    protected $hidden = [
        'created_at', 'deleted_at', 'updated_at'
    ];

    public function scopeLanguage($query, $language_id)
    {
        $query->where('language_id', $language_id);
    }

    public function scopeName($query, $name)
    {
        return $query->where('name', $name);
    }
}
