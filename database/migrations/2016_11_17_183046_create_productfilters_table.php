<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductFiltersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('product_filters', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('shop_id')->unsigned();
            $table->integer('product_filter_group_id')->unsigned();
            $table->enum('operator', ['less', 'equal', 'more', 'not', 'like']);
            $table->enum('clause', ['or', 'and']);
            $table->string('field');
            $table->string('value');
            $table->enum('value_type', ['int', 'string', 'float', 'double']);
            $table->integer('weight')->default(0);
            $table->boolean('shop_val')->default(false);
            $table->timestamps();

            $table->foreign('shop_id')
               ->references('id')
                ->on('shops')
                ->onDelete('cascade');
            
            $table->foreign('product_filter_group_id')
               ->references('id')
                ->on('product_filter_groups')
                ->onDelete('cascade');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('product_filters');
	}

}
