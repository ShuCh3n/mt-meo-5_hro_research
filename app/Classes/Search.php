<?php
namespace App\Classes;

use Illuminate\Pagination\LengthAwarePaginator;
use Oefenweb\DamerauLevenshtein\DamerauLevenshtein;

use App\Product;
use App\Classes\SearchResult;
use App\Customer;
use App\Motor;

class Search {
	public $results;
    public $total;
    public $search_query;
    public $search_points;
    private $paginate;

    public function __construct($query)
    {
    	$this->results = array();
    	$this->total = 0;
        $this->search_points = 0;
    	$this->search_query = strtolower($query);
        $this->paginate = 30;
    }

    public function all()
    {
    	$this->products()->customers()->motors();
    	$this->toCollection()->total()->paginate();

    	return $this;
    }

    private function total()
    {
    	$this->total = $this->results->count();
    	return $this;
    }

    private function products()
    {
        $products = new Product;

        foreach(explode(' ', $this->search_query) as $q)
        {
            $products = $products->orWhere('sku', 'REGEXP', $q)
                ->orWhere('supplier_sku', 'REGEXP', $q)
                ->orWhere('ean', 'REGEXP', $q)
                ->orWhereHas('brand', function($query) use ($q){
                    $query->where('name', 'REGEXP', $q);
                })
                ->orWhereHas('infos', function($query) use ($q){
                    $query->where('name', 'REGEXP', $q)
                            ->orWhere('short_description', 'REGEXP', $q)
                            ->orWhere('description', 'REGEXP', $q)
                            ->orWhere('specification', 'REGEXP', $q);
                });
        }

        $products = $products->with(['info', 'brand', 'infos'])->get();

        foreach($products as $product)
        {
            $result = new SearchResult($this->search_query);
            $result->category = 'products';
            $result->title = ($product->info)? $product->info->name : '';
            $result->link = route('edit_product', $product->id);
            $result->description = ($product->info)? $product->info->description : '';

            $result->getSimilarity([
                $product->sku,
                $product->id,
                ($product->brand)? $product->brand->name : '',
                ($product->info)? $product->info->name : '',
                ($product->info)? $product->info->short_description : '',
                ($product->info)? $product->info->description : '',
                ($product->info)? $product->info->specification : '',
            ]);

            $this->search_points += $result->points;

            $this->results[] = $result;
        }

    	return $this;
    }

    private function customers()
    {
        $customers = new Customer;

        foreach(explode(' ', $this->search_query) as $q)
        {
            $customers = $customers->orWhere('av_deb', 'REGEXP', $this->search_query)
                ->orWhere('company_name', 'REGEXP', $this->search_query)
                ->orWhere('firstname', 'REGEXP', $this->search_query)
                ->orWhere('lastname', 'REGEXP', $this->search_query)
                ->orWhere('email', 'REGEXP', $this->search_query)
                ->orWhere('mobile_phone', 'REGEXP', $this->search_query)
                ->orWhere('home_phone', 'REGEXP', $this->search_query)
                ->orWhere('billing_company', 'REGEXP', $this->search_query)
                ->orWhere('billing_vat_number', 'REGEXP', $this->search_query)
                ->orWhere('billing_firstname', 'REGEXP', $this->search_query)
                ->orWhere('billing_lastname', 'REGEXP', $this->search_query)
                ->orWhere('billing_street', 'REGEXP', $this->search_query)
                ->orWhere('billing_housenumber', 'REGEXP', $this->search_query)
                ->orWhere('billing_city', 'REGEXP', $this->search_query)
                ->orWhere('billing_state', 'REGEXP', $this->search_query);
        }

        $customers = $customers->get();

        foreach($customers as $customer)
        {
            $result = new SearchResult($this->search_query);
            $result->category = 'customers';
            $result->title = $customer->firstname . ' ' . $customer->lastname;
            $result->link = route('view_customer', $customer->id);
            $result->description = '';

            $result->getSimilarity([
                $customer->av_deb,
                $customer->id,
                $customer->company_name,
                $customer->firstname,
                $customer->lastname,
                $customer->email,
                $customer->mobile_phone,
                $customer->home_phone,
                $customer->billing_company,
                $customer->billing_vat_number,
                $customer->billing_firstname,
                $customer->billing_lastname,
                $customer->billing_street,
                $customer->billing_housenumber,
                $customer->billing_city,
                $customer->billing_state
            ]);

            $this->search_points += $result->points;

            $this->results[] = $result;
        }

        return $this;
    }

    private function motors()
    {
        $motors = new Motor;

        foreach(explode(' ', $this->search_query) as $q)
        {
            $motors = $motors->orWhere('code', 'REGEXP', $this->search_query)
                ->orWhere('name', 'REGEXP', $this->search_query)
                ->orWhere('model', 'REGEXP', $this->search_query)
                ->orWhere('type', 'REGEXP', $this->search_query)
                ->orWhereHas('vehicle_brand', function($query){
                    $query->where('name', 'REGEXP', $this->search_query);
                });
        }

        $motors = $motors->with('vehicle_brand')->get();

        foreach($motors as $motor)
        {
            $result = new SearchResult($this->search_query);
            $result->category = 'motors';
            $result->title = $motor->name . ' ' . $motor->model . ' ' . $motor->type;
            $result->link = route('view_motor', $motor->id);
            $result->description = '';

            $result->getSimilarity([
                $motor->code,
                $motor->name,
                $motor->model,
                $motor->type,
                $motor->cc,
                $motor->vehicle_brand->name
            ]);

            $this->search_points += $result->points;

            $this->results[] = $result;
        }

        return $this;
    }

    private function toCollection()
    {
        $this->results = collect($this->results)->sortBy('points');

        return $this;
    }

    private function paginate()
    {

        $currentPage = LengthAwarePaginator::resolveCurrentPage();
        $currentPageSearchResults = $this->results->slice(($currentPage - 1) * $this->paginate, $this->paginate)->all();
        $this->results = new LengthAwarePaginator($currentPageSearchResults, $this->total, $this->paginate, $currentPage, ['path' => route('search'), 'query' => request()->query()]);

        return $this;
    }
}
