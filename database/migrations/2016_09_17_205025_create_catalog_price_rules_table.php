<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCatalogPriceRulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('catalog_price_rules', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('active');
            $table->string('name');
            $table->boolean('add_or_subtract');
            $table->boolean('amount_or_percentage');
            $table->decimal('amount', 10, 2);
            $table->integer('priority')->default(0)->nullable();
            $table->date('valid_from')->nullable();
            $table->date('valid_until')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('catalog_price_rule_country', function (Blueprint $table) {
            $table->integer('catalog_price_rule_id')->unsigned();
            $table->integer('country_id')->unsigned();

            $table->foreign('catalog_price_rule_id', 'fk_cprid2')
                ->references('id')
                ->on('catalog_price_rules')
                ->onDelete('cascade');

            $table->foreign('country_id')
                ->references('id')
                ->on('countries')
                ->onDelete('cascade');

            $table->primary(['catalog_price_rule_id', 'country_id'], 'cpircid');
        });

        Schema::create('catalog_price_rule_product', function (Blueprint $table) {
            $table->integer('catalog_price_rule_id')->unsigned();
            $table->integer('product_id')->unsigned();

            $table->foreign('catalog_price_rule_id', 'fk_cprid3')
                ->references('id')
                ->on('catalog_price_rules')
                ->onDelete('cascade');

            $table->foreign('product_id', 'fk_prid2')
                ->references('id')
                ->on('products')
                ->onDelete('cascade');

            $table->primary(['catalog_price_rule_id', 'product_id'], 'cpridpid2');
        });

        Schema::create('catalog_price_rule_product_feature_type', function (Blueprint $table) {
            $table->integer('catalog_price_rule_id')->unsigned();
            $table->integer('product_feature_type_id')->unsigned();

            $table->foreign('catalog_price_rule_id', 'fk_cprid4')
                ->references('id')
                ->on('catalog_price_rules')
                ->onDelete('cascade');

            $table->foreign('product_feature_type_id', 'fk_pftid')
                ->references('id')
                ->on('product_feature_types')
                ->onDelete('cascade');

            $table->primary(['catalog_price_rule_id', 'product_feature_type_id'], 'cpridpftid');
        });

        Schema::create('catalog_price_rule_shop', function (Blueprint $table) {
            $table->integer('catalog_price_rule_id')->unsigned();
            $table->integer('shop_id')->unsigned();

            $table->foreign('catalog_price_rule_id')
                ->references('id')
                ->on('catalog_price_rules')
                ->onDelete('cascade');

            $table->foreign('shop_id')
                ->references('id')
                ->on('shops')
                ->onDelete('cascade');

            $table->primary(['catalog_price_rule_id', 'shop_id']);
        });

        Schema::create('catalog_price_rule_category', function (Blueprint $table) {
            $table->integer('catalog_price_rule_id')->unsigned();
            $table->integer('category_id')->unsigned();

            $table->foreign('catalog_price_rule_id')
                ->references('id')
                ->on('catalog_price_rules')
                ->onDelete('cascade');

            $table->foreign('category_id')
                ->references('id')
                ->on('categories')
                ->onDelete('cascade');

            $table->primary(['catalog_price_rule_id', 'category_id'], 'cpridcid5');
        });

        Schema::create('catalog_price_rule_customer_group', function (Blueprint $table) {
            $table->integer('catalog_price_rule_id')->unsigned();
            $table->integer('customer_group_id')->unsigned();

            $table->foreign('catalog_price_rule_id')
                ->references('id')
                ->on('catalog_price_rules')
                ->onDelete('cascade');

            $table->foreign('customer_group_id')
                ->references('id')
                ->on('customer_groups')
                ->onDelete('cascade');

            $table->primary(['catalog_price_rule_id', 'customer_group_id'], 'cptidcgid2');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('catalog_price_rule_customer_group');
        Schema::drop('catalog_price_rule_category');
        Schema::drop('catalog_price_rule_shop');
        Schema::drop('catalog_price_rule_product_feature_type');
        Schema::drop('catalog_price_rule_product');
        Schema::drop('catalog_price_rule_country');
        Schema::drop('catalog_price_rules');
    }
}
