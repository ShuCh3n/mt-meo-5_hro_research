@extends('dashboard.layout.main')

@section('stylesheet')
    <link href="/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
@stop

@section('javascript')
    <!-- iCheck -->
    <script src="/js/plugins/iCheck/icheck.min.js"></script>

    <!-- Data picker -->
   <script src="/js/plugins/datapicker/bootstrap-datepicker.js"></script>

    <script>
        $(function(){
            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });

            $('#generate_password').click(function(){
                $('#new_password').val(randomString(8));
            });

            $('#dob .input-group.date').datepicker({
                startView: 2,
                todayBtn: "linked",
                keyboardNavigation: false,
                forceParse: false,
                autoclose: true
            });
        });

        function randomString(length) {
            return Math.round((Math.pow(36, length + 1) - Math.random() * Math.pow(36, length))).toString(36).slice(1);
        }
    </script>
@stop

@section('content')
	<div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-6">
            <h2>{{ trans('pages.inquiry_detail_header') }}</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('home') }}">{{ trans('menu.home') }}</a>
                </li>
                <li>
                    <a href="">{{ trans('menu.inquiries') }}</a>
                </li>
                <li class="active">
                    <strong>{{ trans('page.inquiry') }}</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-6">
            <a class="btn btn-outline btn-primary pull-right mt30" href="{{ route('new_product_group') }}"><i class="fa fa-plus-circle" aria-hidden="true"></i> New Employee</a>
        </div>
    </div>

	<div class="wrapper wrapper-content animated fadeInRight">
        {!! session()->has('success')? '<div class="alert alert-success alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>' . session('success') . '</div>' : '' !!}

        <form id="profileForm" method="post" class="form-horizontal">
            <div class="row">
                <div class="col-md-8">
                    <div class="ibox">

                        <div class="ibox-title">
                            <h5>Edit {{ $user->firstname }} {{ $user->lastname }}</h5>
                        </div>

                        <div class="ibox-content">
                            <div class="mb20"></div>

                            <div class="row">
                                <div class="col-md-12">
                                    
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Active<span class="text-danger">*</span></label>
                                        <div class="col-md-5">
                                            <label><input type="radio" class="form-control i-checks" name="active" value="1" {{ ($user->active == 1)? "checked" : "" }}> Enabled</label>
                                        </div>
                                        <div class="col-md-5">
                                            <label><input type="radio" class="form-control i-checks" name="active" value="0" {{ ($user->active == 0)? "checked" : "" }}> Disabled</label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Super User<span class="text-danger">*</span></label>
                                        <div class="col-md-5">
                                            <label><input type="radio" class="form-control i-checks" name="super_user" value="1" {{ ($user->super_user == 1)? "checked" : "" }}> Yes</label>
                                        </div>
                                        <div class="col-md-5">
                                            <label><input type="radio" class="form-control i-checks" name="super_user" value="0" {{ ($user->super_user == 0)? "checked" : "" }}> No</label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Name</label>
                                        <div class="col-md-5"><input type="text" class="form-control" name="name" placeholder="First name" value="{{ $user->firstname }}"></div>
                                        <div class="col-md-5"><input type="text" class="form-control" name="name" placeholder="Last name" value="{{ $user->lastname }}"></div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Account<span class="text-danger">*</span></label>
                                        <div class="col-md-5"><input type="text" class="form-control" name="name" placeholder="Email" value="{{ $user->email }}"></div>
                                        <div class="col-md-5">
                                            <div class="input-group">
                                                <input class="form-control" name="password" id="new_password" value="" type="text" placeholder="New Password">
                                                <span class="input-group-btn"> 
                                                    <button class="btn btn-primary" type="button" id="generate_password">Generate</button> 
                                                </span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Date of birth</label>
                                        <div class="col-md-10">
                                            <div class="input-group date dob">
                                                <span class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </span>
                                                <input type="text" placeholder="Date of birth" name="dob" class="form-control" value="{{ date("m/d/Y", strtotime($user->dob)) }}">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Language</label>
                                        <div class="col-md-10">
                                            <select class="select2 form-control" name="language">
                                                @foreach($languages as $language)
                                                    <option value="{{ $language->id }}">{{ $language->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Department</label>
                                        <div class="col-md-10">
                                            <select class="select2 form-control" name="department">
                                                @foreach($departments as $department)
                                                    <option value="{{ $department->id }}">{{ $department->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="ibox">

                        <div class="ibox-title">
                            <h5>Permission</h5>
                        </div>

                        <div class="ibox-content">
                            <div class="mb20"></div>

                            <div class="row">
                                <div class="col-md-12">
                                
                                    <div class="clearfix">
                                        <label class="col-xs-2 col-xs-push-2"> 
                                            View
                                        </label>
                                        <label class="col-xs-2 col-xs-push-2"> 
                                            Add
                                        </label>
                                        <label class="col-xs-2 col-xs-push-2"> 
                                            Edit
                                        </label>
                                        <label class="col-xs-2 col-xs-push-2"> 
                                            Delete
                                        </label>
                                        <label class="col-xs-2 col-xs-push-2"> 
                                            All
                                        </label>
                                    </div>

                                    <div class="mb5"></div>

                                    @foreach($permissions->where('parent_id', null) as $permission)
                                        <label class="col-xs-12">{{ $permission->info->name}}</label>

                                        @foreach($permissions->where('parent_id', $permission->id) as $sub_permission)
                                            <div class="clearfix">
                                                <div class="col-xs-2"> 
                                                    {{ $sub_permission->info->name }}
                                                </div>
                                                <div class="col-xs-2"> 
                                                    <input type="checkbox" class="i-checks">
                                                </div>
                                                <div class="col-xs-2"> 
                                                    <input type="checkbox" class="i-checks">
                                                </div>
                                                <div class="col-xs-2"> 
                                                    <input type="checkbox" class="i-checks">
                                                </div>
                                                <div class="col-xs-2"> 
                                                    <input type="checkbox" class="i-checks">
                                                </div>
                                                <div class="col-xs-2"> 
                                                    <input type="checkbox" class="i-checks">
                                                </div>
                                            </div>
                                            <div class="mb5"></div>
                                        @endforeach

                                        <hr>
                                    @endforeach

                                </div>
                            </div>
                        </div>
                    </div> 
                </div>

                <div class="col-md-4">
                    <div class="ibox">
                        <div class="ibox-title">
                            <h5>Action</h5>
                        </div>

                        <div class="ibox-content">
                            {{ csrf_field() }}
                            <button class="btn btn-block btn-outline btn-success" type="submit"><i class="fa fa-floppy-o"></i> Save</button>
                            <button class="btn btn-block btn-outline btn-danger" type="submit"><i class="fa fa-trash" aria-hidden="true"></i> Remove</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>

        <div class="mb30"></div>
    </div>
@stop