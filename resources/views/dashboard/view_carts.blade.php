@extends('dashboard.layout.main')

@section('content')
	<div class="row wrapper border-bottom white-bg page-heading">
	    <div class="col-lg-12">
	        <h2>{{ trans('pages.inquiry_detail_header') }}</h2>
	        <ol class="breadcrumb">
	            <li>
	                <a href="{{ route('home') }}">{{ trans('menu.home') }}</a>
	            </li>
	            <li>
	                <a href="">{{ trans('menu.inquiries') }}</a>
	            </li>
	            <li class="active">
	                <strong>{{ trans('page.inquiry') }}</strong>
	            </li>
	        </ol>
	    </div>
	</div>

	<div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-md-8">

                <div class="ibox">
                    <div class="ibox-title">
                        <span class="pull-right">(<strong>{{ $cart->cart_products->count() }}</strong>) items</span>
                        <h5>Items in your cart</h5>
                    </div>
                    <div class="ibox-content">

                        <div class="table-responsive">
                            @if($cart->cart_products->count() > 0)
                                @foreach($cart->cart_products as $cart_product)
                                    <table class="table shoping-cart-table">

                                        <tbody>
                                        <tr>
                                            <td width="90">
                                                <div class="cart-product-imitation">
                                                </div>
                                            </td>
                                            <td class="desc">
                                                <h3>
                                                <a href="#">
                                                    <a href="{{ route('edit_product', $cart_product->product->id) }}">{{ $cart_product->product->shop_infos->first()->name }}</a>
                                                </a>
                                                </h3>
                                                <p class="small">
                                                    {{ $cart_product->product->shop_infos->first()->short_description }}
                                                </p>

                                                <div class="m-t-sm">
                                                    <a href="#" class="text-muted"><i class="fa fa-trash"></i> Remove item</a>
                                                </div>
                                            </td>

                                            <td>
                                                {{ price($cart_product->product->prices->retail_price) }}
                                                <s class="small text-muted">{{ price($cart_product->product->prices->retail_price) }}</s>
                                            </td>
                                            <td width="65">
                                                <input type="text" class="form-control" placeholder="1" value="{{ $cart_product->qty }}">
                                            </td>
                                            <td>
                                                <h4>
                                                    {{ price($cart_product->product->prices->retail_price * $cart_product->qty) }}
                                                </h4>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                @endforeach
                            @else
                                <table class="table shoping-cart-table">
                                    <tbody>
                                        <tr>
                                            <td class="desc">
                                                <h3 class="text-center">Cart is empty</h3>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            @endif
                        </div>

                    </div>
                </div>
            </div>

            <div class="col-md-4">

                <div class="ibox">
                    <div class="ibox-title">
                        <h5>Cart Summary</h5>
                    </div>
                    <div class="ibox-content">
                        <span>
                            Total
                        </span>
                        <h2 class="font-bold">
                            {{ price($cart->total_price) }}
                        </h2>

                        <hr/>

                        <div class="m-t-sm">
                            @if($cart->hasCustomer())
                                <button class="btn btn-block btn-outline btn-warning" type="button"><i class="fa fa-bell-o" aria-hidden="true"></i> Send Reminder</button>
                            @endif
                            <a class="btn btn-block btn-outline btn-danger" href="{{ route("remove_cart", $cart->id) }}"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="mb30"></div>
    </div>
@stop