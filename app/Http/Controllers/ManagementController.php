<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Language;
use App\Department;
use App\Permission;

class ManagementController extends Controller
{
    public function employees()
    {
    	$users = User::all();
    	return view('dashboard.employees', compact('users'));
    }

    public function editEmployee()
    {
    	$user = User::findOrFail($this->request->id);
    	$languages = Language::all();
    	$departments = Department::all();
        $permissions = Permission::with('info')->get();

    	return view('dashboard.edit_employee', compact('user', 'languages', 'departments', 'permissions'));
    }

    public function departments()
    {
    	$departments = Department::all();
    	return view('dashboard.departments', compact('departments'));
    }

    public function newDepartment()
    {
    	return view('dashboard.new_department');
    }

    public function actionNewDepartment()
    {
    	$department = new Department;
    	$department->name = $this->request->name;
    	$department->save();

    	return redirect()->route('departments');
    }

    public function editDepartment()
    {
    	$department = Department::findOrFail($this->request->id);
    	return view('dashboard.edit_department', compact('department'));
    }

    public function actionEditDepartment()
    {
    	$department = Department::findOrFail($this->request->id);
    	$department->name = $this->request->name;
    	$department->save();

    	return redirect()->route('departments');
    }
}
