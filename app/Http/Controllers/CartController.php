<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Cart;
use App\Classes\ShopProductPrice;

class CartController extends Controller
{
    public function index()
    {
    	$carts = Cart::with(['customer_session' => function($query){
    		$query->with('customer');
    	}])
    	->whereHas('cart_products')
    	->with(['cart_products' => function($query){
    		$query->with(['product' => function($q){
    			$q->with('shop_price')->with('product_price')->with('catalog_price_rules')->with('shop_products')->with('shop_infos');
    		}]);
    	}])->with(['shop' => function($query){
    		$query->with('tax_rule');
    	}])->latest()->paginate(100);

    	foreach($carts as $cart)
    	{
    		$cart = $cart->getCartTotalPrice();
    	}

    	return view('dashboard.carts', compact('carts'));
    }

    public function viewCart()
    {
    	$cart = Cart::findOrFail($this->request->id)->getCartTotalPrice();

    	return view('dashboard.view_carts', compact('cart'));
    }

    public function removeCart()
    {
    	$cart = Cart::findOrFail($this->request->id)->delete();

    	return redirect()->route('carts');
    }
}
