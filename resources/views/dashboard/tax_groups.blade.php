@extends('dashboard.layout.main')

@section('stylesheet')
    <link href="/css/plugins/select2/select2.min.css" rel="stylesheet">
@stop

@section('javascript')
    <!-- Select2 -->
    <script src="/js/plugins/select2/select2.full.min.js"></script>

    <script>
        $(function(){
            $(".shop_list").select2();
        });
    </script>
@stop

@section('content')
	<div class="wrapper wrapper-content animated fadeInRight ecommerce">
            <form method="get">
                <div class="ibox-content m-b-sm border-bottom">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="btn-group pull-right">
                                <a class="btn btn-warning" href="{{ route('new_tax_group') }}"><i class="fa fa-plus-circle" aria-hidden="true"></i> New Group</a>
                            </div>
                        </div>
                    </div>
                </div>
            </form>

            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox">
                        <div class="ibox-content">

                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th data-toggle="true">Section Code</th>
                                        <th data-toggle="true">Group Name</th>
                                        <th data-toggle="true">Default</th>
                                        <th class="text-right" data-sort-ignore="true">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($tax_groups as $tax_group)
                                        <tr>
                                            <td>
                                                {{ $tax_group->declaration_section_code }}
                                            </td>
                                            <td>
                                                <a href="{{ route('view_tax_group', $tax_group->id) }}">{{ $tax_group->name }}</a>
                                            </td>
                                            <td>
                                                {!! ($tax_group->default)? '<i class="fa fa-check text-navy" aria-hidden="true"></i>' : '<i class="fa fa-times text-danger" aria-hidden="true"></i>' !!}
                                            </td>
                                            <td class="text-right">
                                                <div class="btn-group">
                                                    <a href="{{ route('view_tax_group', $tax_group->id) }}" class="btn-white btn btn-xs">View</a>
                                                    <a href="{{ route('edit_tax_group', $tax_group->id) }}" class="btn-white btn btn-xs">Edit</a>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@stop