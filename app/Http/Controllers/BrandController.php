<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Brand;
use File;
use Storage;

class BrandController extends Controller
{
    public function brands()
    {
    	$brands = Brand::with('products')->with('image')->paginate(100);
        $request = $this->request;

    	return view('dashboard.brands', compact('brands', 'request'));
    }

    public function newBrand()
    {
    	return view('dashboard.new_brand');
    }

    public function actionNewBrand()
    {
        $this->validate($this->request, [
            "name"  => "required",
            "logo"   => "image|max:5000"
        ]);

        $filename = null;

        if($this->request->logo)
        {
            $filename = str_random(40) . "." . $this->request->logo->getClientOriginalExtension();
            Storage::put('brands/' . $filename, File::get($this->request->logo), 'public');
        }

        $brand = new Brand;
        $brand->logo = $filename;
        $brand->name = $this->request->name;
        $brand->save();

        return redirect()->route('brands');
    }

    public function editBrand()
    {
        $brand = Brand::findOrFail($this->request->id);

        return view('dashboard.edit_brand', compact('brand'));
    }

    public function brandsImages(){
        $brands = Brand::with('image');
        return $brands->get();
    }

    public function actionEditBrand()
    {

        $brand = Brand::findOrFail($this->request->id);
        $brand->name = $this->request->name;

        if($this->request->logo)
        {
            $filename = str_random(40) . "." . $this->request->logo->getClientOriginalExtension();
            Storage::put('brands/' . $filename, File::get($this->request->logo), 'public');
            $brand->logo = $filename;
        }

        $brand->save();

        return redirect()->route('brands');
    }
}
