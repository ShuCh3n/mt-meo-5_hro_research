<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_groups', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('mandant')->nullable();
            $table->integer('discount');
            $table->boolean('tax');
            $table->boolean('show_prices');
            $table->timestamps();
        });

        DB::table('customer_groups')->insert([
            [
                "name" => "Debiteuren",
                "discount" => 0,
                "tax" => false,
                "show_prices" => true
            ],
            [
                "name" => "Klanten",
                "discount" => 0,
                "tax" => true,
                "show_prices" => true
            ]
        ]);

        Schema::create('customers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('av_deb')->nullable();
            $table->string('mand')->default('TTI');
            $table->string('company_name')->nullable();
            $table->integer('shop_id')->unsigned();
            $table->integer('customer_group_id')->unsigned()->nullable();
            $table->boolean('gender')->nullable();
            $table->string('firstname')->nullable();
            $table->string('lastname')->nullable();
            $table->date('dob')->nullable();
            $table->string('email');
            $table->string('password');
            $table->string('mobile_phone')->nullable();
            $table->string('home_phone')->nullable();
            $table->string('billing_company')->nullable();
            $table->string('billing_vat_number')->nullable();
            $table->string('billing_firstname')->nullable();
            $table->string('billing_lastname')->nullable();
            $table->string('billing_street');
            $table->string('billing_housenumber');
            $table->string('billing_city');
            $table->string('billing_zipcode');
            $table->string('billing_state')->nullable();
            $table->integer('billing_state_id')->nullable()->unsigned();
            $table->integer('billing_country_id')->unsigned();
            $table->integer('tax_rule_id')->unsigned()->nullable();
            $table->boolean('optin');
            $table->text('note')->nullable();
            $table->boolean('active');
            $table->boolean('av_sync')->default(0);
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('shop_id')
                ->references('id')
                ->on('shops')
                ->onDelete('cascade');

            $table->foreign('customer_group_id')
                ->references('id')
                ->on('customer_groups')
                ->onDelete('cascade');

            $table->foreign('billing_country_id')
                ->references('id')
                ->on('countries')
                ->onDelete('cascade');

            $table->foreign('billing_state_id')
                ->references('id')
                ->on('states')
                ->onDelete('cascade');

            $table->foreign('tax_rule_id')
                ->references('id')
                ->on('tax_rules')
                ->onDelete('cascade');
        });

        // DB::table('customers')->insert([
        //     [
        //         'id' => 1,
        //         'av_deb' => '00000-BS',
        //         'mand' => 'TTI',
        //         'company_name' => 'Bubba gump shrimp Co.',
        //         'shop_id' => 1,
        //         'firstname' => 'bubba',
        //         'lastname' => 'gump',
        //         'email' => 'info@bubbagumpshrimp.com',
        //         'password' => 'bbg',
        //         'mobile_phone' => '0666983367',
        //         'home_phone' => '0666983368',
        //         'billing_company' => 'daans motorclub',
        //         'billing_vat_number' => '00000',
        //         'billing_firstname' => 'Daan',
        //         'billing_lastname' => 'Van der werf',
        //         'billing_street' => 'Baumstrasse',
        //         'billing_housenumber' => '13',
        //         'billing_city' => 'Darmstadt',
        //         'billing_zipcode' => '66445',
        //         'billing_country_id' => 215,
        //         'optin' => 1,
        //         'active' => 1
        //     ]
        // ]);

        Schema::create('customer_addresses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('customer_id')->unsigned();
            $table->string('alias');
            $table->string('company')->nullable();
            $table->string('firstname')->nullable();
            $table->string('lastname')->nullable();
            $table->string('mobile_phone')->nullable();
            $table->string('home_phone')->nullable();
            $table->string('street');
            $table->string('housenumber');
            $table->string('city');
            $table->string('zipcode');
            $table->string('state')->nullable();
            $table->integer('state_id')->nullable()->unsigned();
            $table->integer('country_id')->unsigned();
            $table->boolean('default');
            $table->text('note')->nullable();
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('customer_id')
                ->references('id')
                ->on('customers')
                ->onDelete('cascade');

            $table->foreign('state_id')
                ->references('id')
                ->on('states')
                ->onDelete('cascade');

            $table->foreign('country_id')
                ->references('id')
                ->on('countries')
                ->onDelete('cascade');
        });

        Schema::create('customer_sessions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('customer_id')->nullable()->unsigned();
            $table->string('session_token')->unique();
            $table->string('ip');
            $table->boolean('checked_out');
            $table->timestamps();

            $table->foreign('customer_id')
                ->references('id')
                ->on('customers')
                ->onDelete('cascade');
        });

        Schema::create('customer_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('customer_id')->unsigned();
            $table->string('message');
            $table->integer('level');
            $table->timestamps();

            $table->foreign('customer_id')
                ->references('id')
                ->on('customers')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('customer_logs');
        Schema::drop('customer_sessions');
        Schema::drop('customer_addresses');
        Schema::drop('customers');
        Schema::drop('customer_groups');
    }
}
