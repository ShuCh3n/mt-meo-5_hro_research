<?php

namespace App\Classes\API\Vehicle\Motors;

use App\Classes\API\Vehicle\Vehicle;
use App\Motor as MotorModel;
use App\Product;
use App\motorProduct;

class Motor extends Vehicle
{
	public function view_motor()
	{
		return MotorModel::with('products')->findOrFail($this->request->id);
	}

	public function motors()
    {
    	$motors = new MotorModel;

    	if(isset($this->request->filter['motor_brand']))
	    {
    		$motors = $motors->filterVehicleBrand($this->request->filter['motor_brand']);
    	}

        if(isset($this->request->filter['motor_name']))
        {
            $motors = $motors->scopeFilterVehicleName($this->request->filter['motor_name']);
        }

    	if(isset($this->request->filter['year']))
	    {
    		$motors = $motors->filterVehicleYear($this->request->filter['year']);
    	}

    	if(isset($this->request->filter['cc']))
	    {
    		$motors = $motors->whereIn('cc', $this->request->filter['cc']);
    	}

    	return $motors->paginate($this->paginate)->appends($this->request->input());
    }

    public function products()
    {
    	$products = new Product;

    	if($this->request->id)
    	{
    		$products = $products->where('id', $this->request->id);
    	}

        //$products = $this->product_filter($products);
        $products = $products->with(['motorProduct' => function($q){
            $q->with(['vehicle_year', 'category.info', 'motor.vehicle_brand']);
        }])
        ->whereHas('motors');
        return $products->paginate($this->paginate)->appends($this->request->input());
    }

    private function product_filter($product)
    {
        if(isset($this->request->filter['sku']))
        {
            $products = $products->where('sku', $this->request->filter['sku']);
        }

        if(isset($this->request->filter['motor_brand']))
        {
            $products = $products->filterMotorVehicleBrand($this->request->filter['motor_brand']);
        }

        if(isset($this->request->filter['cc']))
        {
            $products = $products->whereHas('motors', function($query){
                $query->whereIn('cc', $this->request->filter['cc']);
            });
        }

        if(isset($this->request->filter['year']))
        {
            $products = $products->filterMotorVehicleYear($this->request->filter['year']);
        }

        return $product;
    }
}
