<?php
namespace App\Classes\WebHooks;

use App\Classes\ShopProductPrice;
use App\Shop;
use Illuminate\Support\Facades\View;

class WebHook
{
    protected $model;
    protected $webhook;
    protected $template;
    protected $request;
    protected $event;
    protected $url;
    protected $auth;
    protected $headers;
    protected $additional;

    public function __construct($model, $webhook, $event)
    {
        $this->model = $model;
        $this->webhook = $webhook;
        $this->event = $event;
        switch (get_class($model)) {
            case 'App\Product':
                $this->setTemplate('product');
                $this->setUrl('products');
                break;
            case 'App\Motor':
                $this->setTemplate('motor');
                $this->setUrl('motor');
                break;
            case 'App\CategoryMotorProductVehicleYear':
                $this->setTemplate('motor_product');
                $this->setUrl('motor_product');
                break;
        }

        $this->additional = [];
        $this->headers = ['base_uri' => $this->webhook->url, 'verify' => false ,'headers' => ['content-type' => 'application/json']];
        if ($this->webhook->username) {
            $this->setAuth([$this->webhook->username, $this->webhook->password]);
        }
    }
     
    public function createRequest()
    {
        $this->request = new \GuzzleHttp\Client($this->headers);
    }
     
    public function setTemplate($template)
    {
        $model = $this->model;
        if ($this->webhook->shop_id && $template == 'product') {
            $model->prices = new ShopProductPrice($this->model, Shop::find($this->webhook->shop_id));
        }

        $this->template = $this->format($template);//View::make('webHooks.'.$template, ['model' => $model, 'additional' => $this->additional])->render();
    }

    public function format($template = null)
    {
        return $this->model;
    }

    public function setUrl($url)
    {
        $this->url = $url;
    }

    public function setAditional($additional)
    {
        $this->additional = $additional;
    }

    public function setAuth($auth)
    {
        $this->headers = array_merge($this->headers, ['auth' => $auth]);
        $this->auth = $auth;
    }

    public function doRequest()
    {
        switch ($this->event) {
            case 'created':
                $method = 'POST';
                break;
            case 'updated':
                $method = 'PUT';
                break;
            case 'deleted':
                $method = 'DELETE';
                break;
        }
        $body =  ['body' => $this->template];
        return $this->request->request($method, $this->url, $body);
    }
     
    public function execute()
    {
        $this->createRequest();
        $this->output();
    }

    public function output()
    {
        $response = $this->doRequest();
        var_dump((string) $response->getBody());
    }
}
