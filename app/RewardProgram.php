<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RewardProgram extends Model
{
    protected $fillable = ['shop_id', 'active'];
    
    public function shop()
    {
    	return $this->belongsTo(Shop::class);
    }

    public function orders()
    {
    	return $this->hasMany(RewardProgramOrder::class);
    }

    public function scopeGetShop($query, $shop_id)
    {
    	return $query->where('shop_id', $shop_id);
    }

    public function scopeActive($query)
    {
    	return $query->where('active', true);
    }
}
