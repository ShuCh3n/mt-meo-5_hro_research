<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductFeatureOption extends Model
{
    use SubFilter;
    protected $hidden = [
        'created_at', 
        'updated_at'
    ];

    protected $touches = ['products'];
    protected $fillable = ['product_feature_id'];
    
	public function images()
	{
		return $this->morphMany(Image::class, 'imagable');
	}

    public function product_feature()
    {
    	return $this->belongsTo(ProductFeature::class);
    }

    public function infos()
    {
    	return $this->hasMany(ProductFeatureOptionLanguage::class);
    }

    public function info()
    {
        return $this->hasOne(ProductFeatureOptionLanguage::class);
    }

    public function featureType()
    {
        return $this->belongsTo(ProductFeatureTypeLanguage::class);
    }

    public function products()
    {
        return $this->belongsToMany(Product::class);
    }

    public function appendName($language_id, $name)
    {
        $feature_option_language = new ProductFeatureOptionLanguage;
        $feature_option_language->product_feature_option_id = $this->id;
        $feature_option_language->language_id = $language_id;
        $feature_option_language->name = ucfirst(strtolower($name));
        $feature_option_language->save();
    }

    public static function getFeatureOption($product_feature, $option_name)
    {
        
        foreach($product_feature->options as $option)
        {
            $find_option = $option->infos()->where('language_id', 2)->where('name', $option_name)->first();

            if($find_option)
            {
                return $find_option->product_feature_option;
            }
        }

        return null;
    }

    public static function firstOrCreateFeatureOption($feature, $name, $language_id)
    {
        $product_feature_option = ProductFeatureOption::where('product_feature_id', $feature->id)->whereHas('infos', function($query) use ($name, $language_id){
            $query->where('language_id', $language_id)->where('name', ucfirst(strtolower($name)));
        })->firstOrCreate(["product_feature_id" => $feature->id]);

        if($product_feature_option->wasRecentlyCreated)
        {
            $product_feature_option->appendName($language_id, $name);
        }

        return $product_feature_option;
    }
}
