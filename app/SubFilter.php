<?php

namespace App;

trait SubFilter {
    
    public function scopeSubCheckRelations($q, $relations, $column, $value, $shop_val, $shop_id, $operator, $and){
        if(count($relations) > 2){
            array_shift($relations);
            $q->subFilter($relations, $column, $value, $shop_val, $shop_id, $operator, $and);
        }else{  
            $q->where($column, $operator, $value);
            if($shop_val){
                $q->where('shop_id', $shop_id);
            }
        }
    }

    public function scopeSubFilter ($q, $relations, $column, $value, $shop_val, $shop_id, $operator, $and = true) {
        if(!is_array($relations)){
            $q->where($column, $operator, $value);
            if($shop_val){
                $q->where('shop_id', $shop_id);
            }
        }else{
            if($and == 'and'){
                $q->whereHas($relations[0], function($q) use($relations, $column, $value, $shop_val, $shop_id, $operator, $and) {
                    $q->subCheckRelations($relations, $column, $value, $shop_val, $shop_id, $operator, $and);
                });
            }else{
                $q->orWhereHas($relations[0], function($q) use($relations, $column, $value, $shop_val, $shop_id, $operator, $and) {    
                    $q->subCheckRelations($relations, $column, $value, $shop_val, $shop_id, $operator, $and);
                });
            }
        }
    }
}