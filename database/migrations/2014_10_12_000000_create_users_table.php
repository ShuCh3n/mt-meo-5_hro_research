<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('department_id')->unsigned();
            $table->boolean('super_user')->default(false);
            $table->string('firstname');
            $table->string('lastname');
            $table->string('email')->unique();
            $table->string('password');
            $table->date('dob')->nullable();
            $table->json('options');
            $table->boolean('active');
            $table->rememberToken();
            $table->timestamps();

            $table->foreign('department_id')
                ->references('id')
                ->on('departments')
                ->onDelete('cascade');
        });
 
        DB::table('users')->insert([
            [
                "department_id" => 1,
                "super_user"    => true,
                "firstname"     => "Stenn",
                "lastname"      => "Kool",
                "email"         => "stenn@tti.nl",
                "password"      => Hash::make('1212'),
                "options"       => "{\"language_id\": 2}",
                "active"        => 1
            ],
            [
                "department_id" => 1,
                "super_user"    => true,
                "firstname"     => "Shu",
                "lastname"      => "Chen",
                "email"         => "shu@tti.nl",
                "password"      => Hash::make('shuchen1990'),
                "options"       => "{\"language_id\": 2}",
                "active"        => 1
            ],
            [
                "department_id" => 1,
                "super_user"    => true,
                "firstname"     => "Daan",
                "lastname"      => "vd Werf",
                "email"         => "daan@tti.nl",
                "password"      => Hash::make('ttiiscool'),
                "options"       => "{\"language_id\": 2}",
                "active"        => 1
            ]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
