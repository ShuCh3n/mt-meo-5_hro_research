<?php
/*************************************************************************
 Generated via "php artisan localization:missing" at 2017/04/10 17:38:45
*************************************************************************/

return array (
  //==================================== Translations ====================================//
  'brands' => 'Merken',
  'drag and drop to upload' => 'Sleep een afbeelding of klik om te uplaoden',
  'groups' => 'Groep',
  'image type' => 'Afbeeldings type',
  'products' => 'Producten',
  'refresh manually' => 'Handmatig verieuwen',
  'select a shop' => 'Selecteer een winkel',
);
