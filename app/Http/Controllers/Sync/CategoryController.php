<?php

namespace App\Http\Controllers\Sync;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Language;
use App\Shop;
use App\Product;
use App\Classes\ShopProductPrice;

class CategoryController extends Controller
{
	public function shopCategoryProducts()
    {
        $this->request->language = Language::getLanguage('nl');

        if($this->request->locale)
        {
            $this->request->language = Language::getLanguage($this->request->locale);
        }

        $shop = Shop::with('tax_rule')->findOrFail($this->request->shop_id);

        $category = $shop->categories()
                ->where('id', $this->request->category_id)
                ->with(['products' => function($query){
                    $query->featureInfo($this->request->language->id)
                            ->ProductFilter($this->request->shop_id)
                            ->shopInfo($this->request->shop_id, $this->request->language->id)
                            ->with(['product_price', 'shop_price']);
                }])
                ->first();


        foreach($category->products as $product)
        {
            $product->images = $product->allimages($shop->id);
        }

        $category->products = Product::reformatFeatures($category->products);
        $category->products = ShopProductPrice::calculatePrice($category->products, $shop);

        return $category;
    }
}