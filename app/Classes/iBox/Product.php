<?php

namespace App\Classes\iBox;

use App\Product as CMSProduct;
use App\Platform;
use App\PlatformProductPrice;

class Product
{
	public $product;
	public $currency;
	public $status;
	public $message;
	public $platform_prices;
	public $caculated_price;
	public $cheapest_platform;
	public $row;
	public $percentage_difference;
	public $purchasing_price;

	private $formula;
	private $eq_products;
	private $segments;

	public function __construct($row)
	{
		$this->row = $row;
	}

	public function analyze($formula, $eq_products, $segments)
	{
		$this->formula = $formula;
		$this->eq_products = $eq_products;
		$this->segments = $segments;

		if(isset($this->row->price) && (isset($this->row->ean_code) || isset($this->row->upc) || isset($this->row->prod_code) || isset($this->row->article_code )))
		{
			if(isset($this->row->currency))
	        {
	            $this->currency = Currency::getSymbol($row->currency);
	        }

	        $this->product = $this->findProduct();

	        if($this->product)
	        {
	        	$this->analyseProduct();
	        }
		}

		return $this;
	}

	public function findProduct()
	{
		$queries = array();

		if(isset($this->row->prod_code))
		{
			$queries[] = $this->row->prod_code;
		}
		
		if(isset($this->row->ean_code))
		{
			$queries[] = $this->row->ean_code;
		}

		if(isset($this->row->article_code))
		{
			$queries[] = $this->row->article_code;
		}

		if(isset($this->row->upc))
		{
			$queries[] = $this->row->upc;
		}

		foreach($queries as $q)
		{
			$product = CMSProduct::with('platforms')
						->with('product_price')
						->where('supplier_sku', $q)
						->orWhere('ean', $q)
						->orWhere('sku', $q)
						->orWhere('upc', $q)
						->first();

			if($product)
			{
				return $product;
			}
		}
	}

	public function analyseProduct()
	{
		$this->dealOrNoDeal();

		$this->platform_prices = array();

		foreach($this->product->platforms as $platform)
		{
			$platform_price = PlatformProductPrice::where('platform_id', $platform->id)->where('product_id', $this->product->id)->with('platform')->latest()->first();

			if($platform_price)
			{
				$this->platform_prices[] = $platform_price;
				$this->cheapestPlatform($platform, $platform_price);
			}
		}

		if($this->status == 1 || $this->status == 2)
		{
			$this->percentageDifference();
		}

		$this->getPurchasingPrice();

		return $this;
	}

	private function cheapestPlatform($platform, $platform_price)
	{
		if($this->cheapest_platform && $platform_price > $this->cheapest_platform['price'])
		{
			return $this;
		}

		$this->cheapest_platform['platform'] = $platform->name;
		$this->cheapest_platform['price'] = $platform_price;

		return $this;
	}

	private function percentageDifference()
    {
        $difference = (($this->row->price - $this->caculated_price) / $this->caculated_price) * 100;

        $this->percentage_difference = number_format(abs($difference), 2);

        return $this;
    }

	public function dealOrNoDeal()
	{
		$tyre24 = $this->product->platforms->where('name', 'Tyre24')->first();

        if($tyre24)
        {
        	$tyre24->product_price = PlatformProductPrice::where('platform_id', $tyre24->id)->where('product_id', $this->product->id)->latest()->first()->price;

        	$this->tyre24Fee($tyre24)->margin()->importFee()->transportFee();

            if($this->caculated_price > $this->row->price)
            {
                $this->status = 1;
                return $this;
            }

            $this->status = 2;  
			return $this;
        }

        $this->status = 3;
		return $this;
	}

	private function tyre24Fee($t24)
	{
		if($this->formula['t24_fee'])
		{
			$this->caculated_price = $t24->product_price + $this->formula['t24_fee_value'];

			return $this;
		}

		$this->caculated_price = $t24->product_price - $this->formula['t24_fee_value'];

		return $this;
	}

	private function margin()
	{
		if($this->formula['margin'])
		{
			$this->caculated_price = $this->caculated_price * $this->rePercentage($this->formula['margin_value']);

			return $this;
		}

		$this->caculated_price = $this->caculated_price / $this->rePercentage($this->formula['t24_fee_value']);

		return $this;
	}

	private function importFee()
	{
		if($this->formula['import_fee_value'] > 0)
        {
        	if($this->formula['import_fee'])
        	{
        		$this->caculated_price = $this->caculated_price * $this->rePercentage($this->formula['import_fee_value']);
        		return $this;
        	}

        	$this->caculated_price = $this->caculated_price / $this->rePercentage($this->formula['import_fee_value']);
        }

        return $this;
	}

	private function transportFee()
	{
		if($this->formula['transport_fee'])
		{
			$this->caculated_price = $this->caculated_price + $this->formula['transport_fee_value'];

			return $this;
		}

		$this->caculated_price = $this->caculated_price -  $this->formula['transport_fee_value'];

		return $this;
	}

	private function rePercentage($value)
    {
        $percentage = "0." . (100 - $value);
        return floatval($percentage);
    }

    private function getPurchasingPrice()
    {
    	$this->purchasing_price = $this->product->product_price->purchasing_price;

    	return $this;
    }
}