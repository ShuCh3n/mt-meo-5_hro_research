<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AccountViewSetting extends Model
{
	use SoftDeletes;
	
    protected $fillable = ['platform_id', 'country_id', 'settings'];
}
