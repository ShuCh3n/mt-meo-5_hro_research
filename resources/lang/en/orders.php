<?php

return array (
    //==================================== Translations ====================================//
    'average_purchase_price' => 'APP',
    'average_purchase_price_abbr' => 'Average purchase price',
    'buyer' => 'Buyer',
    'buyer' => 'Buyer',
    'generate_from_file' => 'Generate from imported file',
    'generate_orders_title' => 'Generate Orders',
    'generate_orders' => 'Generate orders',
    'import_excel' => 'Import Excel files',
    'margin_in' => 'Margin in',
    'order_date' => 'Order date',
    'delivery_date' => 'Delivery date',
    'owner' => 'Owner',
    'processed' => 'Processed',
    'product_not_found' => 'This product couldn\'t be found in the CMS',
    'products_not_found' => 'Some products couldn\'t be found in the CMS',
    'purchase_order' => 'Purchase order',
    'purchase_and_transfer' => 'Purchase and transfer',
    'reference' => 'Reference',
    'seller' => 'Seller',
    'transfer_order' => 'Transfer order',
    'transfer_price' => 'Transfer price',
    'already_been_processed' => 'These orders already have been processed',
    'error_in_file_format' => 'There is an error in the format of the selected file! Verify the selected file and try uploading it again...',
    'connect_admin_to_debno' => 'Connect AccountView administrations to the corresponding debtor number',
    'unknown_product' => 'Unknown product',
    'unknown_creditor' => 'Unknown creditor',
    'creditor_not_found' => 'This creditor couldn\'t be found in the CMS! Click on the button next to the creditor number to search for this creditor.',
    'search_for_creditor' => 'Search for this creditor in AccountView',
);
