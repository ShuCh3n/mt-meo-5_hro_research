<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShopsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shops', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tax_rule_id')->unsigned()->nullable();
            $table->string('logo')->nullable();
            $table->string('name');
            $table->string('AV_admin');
            $table->string('link');
            $table->string('key')->unique();
            $table->boolean('maintenance')->default(false);
            $table->integer('default_image_width');
            $table->integer('default_image_height');
            $table->timestamps();

            $table->foreign('tax_rule_id')
                ->references('id')
                ->on('tax_rules')
                ->onDelete('cascade');
        });

        Schema::create('shop_maintenance_excluded_ips', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('shop_id')->unsigned();
            $table->string('ip');
            $table->timestamps();

            $table->foreign('shop_id')
                ->references('id')
                ->on('shops')
                ->onDelete('cascade');
        });

        Schema::create('language_shop', function (Blueprint $table) {
            $table->integer('shop_id')->unsigned();
            $table->integer('language_id')->unsigned();

            $table->foreign('shop_id')
                ->references('id')
                ->on('shops')
                ->onDelete('cascade');

            $table->foreign('language_id')
                ->references('id')
                ->on('languages')
                ->onDelete('cascade');

            $table->primary(['shop_id', 'language_id']);
        });

        Schema::create('country_shop', function(Blueprint $table){
            $table->integer('country_id')->unsigned();
            $table->integer('shop_id')->unsigned();

            $table->foreign('country_id')
                ->references('id')
                ->on('countries')
                ->onDelete('cascade');

            $table->foreign('shop_id')
                ->references('id')
                ->on('shops')
                ->onDelete('cascade');

            $table->primary(['country_id', 'shop_id']);
        });

        Schema::create('currency_shop', function (Blueprint $table) {
            $table->integer('shop_id')->unsigned();
            $table->integer('currency_id')->unsigned();

            $table->foreign('shop_id')
                ->references('id')
                ->on('shops')
                ->onDelete('cascade');

            $table->foreign('currency_id')
                ->references('id')
                ->on('currencies')
                ->onDelete('cascade');

            $table->primary(['shop_id', 'currency_id']);
        });

        DB::table('shops')->insert([
            [
                "name"          => "TTI Webshop",
                "link"          => "http://shop.tti.nl",
                "AV_admin"      =>  "TTI-2016",
                "key"           => str_random(40),
                "maintenance"   => 0
            ],
            [
                "name"          => "Motoria",
                "link"          => "http://webshop.motoria.nl",
                "AV_admin"      =>  "MOTORIA-16",
                "key"           => str_random(40),
                "maintenance"   => 0
            ],
            [
                "name"          => "Banden Bestellen",
                "link"          => "http://bandenbestellen.nl",
                "AV_admin"      =>  "TTI-2016",
                "key"           => "If0J3VLlxVhFt0Maw8ze86Qz1SxJRzMvyeoygAX4",
                "maintenance"   => 0
            ]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('currency_shop');
        Schema::drop('country_shop');
        Schema::drop('language_shop');
        Schema::drop('shop_maintenance_excluded_ips');
        Schema::drop('shops');
    }
}
