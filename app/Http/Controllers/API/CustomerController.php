<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use Auth;

use App\CustomerSession;
use App\Customer;
use App\Country;
use App\CustomerAddress;
use App\CustomerPasswordReset;

use App\Http\Controllers\MailController;

class CustomerController extends Controller
{
	public function initApp()
	{
		return array('status' => true);
	}

	public function customerLogin()
	{
		if(auth()->guard('customers')->attempt(["email" => $this->request->email, "password" => $this->request->password]))
		{
			if($this->request->customer_session->customer_id != auth()->guard('customers')->user()->id)
			{
				
				$this->request->customer_session->customer_id = auth()->guard('customers')->user()->id;
				$this->request->customer_session->save();

				//Merge previous cart that the customer didn't checkout yet
				//$this->mergeCarts();
			}

			return array("status" => true, "user" => auth()->guard('customers')->user()->load('billing_country'));
		}
		return array("status" => false, "message" => "invalid_login");
	}

    public function customerValidate()
    {
        if ($this->request->shop->customers()->where('email', $this->request->email)->first()) {
            return array("status" => false, "message" => "already_registered");
        }else{
            return ['status' => true];
        }
    }

	public function customerRegister()
	{
		if($this->request->shop->customers()->where('email', $this->request->email)->first())
		{
			return array("status" => false, "message" => "already_registered");
		}

		$customer = new Customer;
		$customer->shop_id = $this->request->shop->id;
		$customer->gender = $this->request->gender;
		$customer->firstname = $this->request->firstname;
		$customer->lastname = $this->request->lastname;
		$customer->dob = ($this->request->dob)? date("Y-m-d", strtotime($this->request->dob)) : null;
		$customer->email = $this->request->email;
		$customer->mand = $this->request->mand ?? '';
		$customer->password = bcrypt($this->request->password);
		$customer->mobile_phone = $this->request->mobile_phone;
		$customer->home_phone = $this->request->home_phone;
		$customer->billing_company = $this->request->billing_company ?? "";
		$customer->billing_vat_number = $this->request->billing_vat_number ?? "";
		$customer->billing_street = $this->request->billing_street ?? $this->request->street;
		$customer->billing_housenumber = $this->request->billing_housenumber ?? $this->request->housenumber;
		$customer->billing_city = $this->request->billing_city ?? $this->request->city;
		$customer->billing_zipcode = $this->request->billing_zipcode ?? $this->request->zipcode;

		if(is_numeric($this->request->billing_state))
		{
			$customer->billing_state_id = ($this->request->billing_state)? $this->request->billing_state : $this->request->state;
		}
		
		$customer->billing_country_id = ($this->request->billing_country)? Country::getCountry($this->request->billing_country)->id : Country::getCountry($this->request->country)->id;
		$customer->optin = $this->request->optin;
		$customer->active = true;
		$customer->save();

		$customer->load('billing_country');

		$customer_address = new CustomerAddress;
		$customer_address->customer_id = $customer->id;
		$customer_address->default = 1;
		$customer_address->alias = "Address " . rand(10, 1688);
		$customer_address->company = ($this->request->shipping_company)? $this->request->shipping_company : ($customer->billing_company)? $customer->billing_company : "";
		$customer_address->firstname = ($this->request->shipping_firstname)? $this->request->shipping_firstname : $customer->firstname;
		$customer_address->lastname = ($this->request->shipping_lastname)? $this->request->shipping_lastname : $customer->lastname;
		$customer_address->mobile_phone = ($this->request->shipping_mobile_phone)? $this->request->shipping_mobile_phone : $customer->mobile_phone;
		$customer_address->home_phone = ($this->request->shipping_home_phone)? $this->request->shipping_home_phone : $customer->home_phone;
		$customer_address->street = ($this->request->shipping_street)? $this->request->shipping_street : $customer->billing_street;
		$customer_address->housenumber = ($this->request->shipping_housenumber)? $this->request->shipping_housenumber : $customer->billing_housenumber;
		$customer_address->city = ($this->request->shipping_city)? $this->request->shipping_city : $customer->billing_city;
		$customer_address->zipcode = ($this->request->shipping_zipcode)? $this->request->shipping_zipcode : $customer->billing_zipcode;
		$customer_address->state_id = ($this->request->shipping_state)? $this->request->shipping_state : $customer->billing_state_id;
		$customer_address->country_id = ($this->request->shipping_country)? Country::getCountry($this->request->shipping_country)->id : $customer->billing_country_id;
		$customer_address->save();

		$this->request->customer_session->customer_id =  $customer->id;
		$this->request->customer_session->save();

		return array("status" => true, "user" => $customer);
	}

    public function customerUpdate()
    {
        $customer = Customer::findOrFail($this->request->customer_session->customer_id);
        $customer->gender = $this->request->gender;
        $customer->firstname = $this->request->firstname;
        $customer->lastname = $this->request->lastname;
        $customer->home_phone = $this->request->home_phone;
        $customer->billing_company = ($this->request->billing_company)? $this->request->billing_company : "";
        $customer->billing_vat_number = ($this->request->billing_vat_number)? $this->request->billing_vat_number : "";
        $customer->billing_firstname = ($this->request->billing_firstname)? $this->request->billing_firstname : $this->request->firstname;
        $customer->billing_lastname = ($this->request->billing_lastname)? $this->request->billing_lastname : $this->request->lastname;
        $customer->billing_street = ($this->request->billing_street)? $this->request->billing_street : $this->request->street;
        $customer->billing_housenumber = ($this->request->billing_housenumber)? $this->request->billing_housenumber : $this->request->housenumber;
        $customer->billing_city = ($this->request->billing_city)? $this->request->billing_city : $this->request->city;
        $customer->billing_zipcode = ($this->request->billing_zipcode)? $this->request->billing_zipcode : $this->request->zipcode;
        
        if($this->request->password){
            $customer->password = bcrypt($this->request->password);
        }
        $customer->save();

        $customer->load('billing_country');

        $customer_address = CustomerAddress::where('customer_id', $customer->id)->firstOrFail();
        $customer_address->firstname = ($this->request->shipping_firstname)? $this->request->shipping_firstname : $this->request->firstname;
        $customer_address->lastname = ($this->request->shipping_lastname)? $this->request->shipping_lastname : $this->request->lastname;
        $customer_address->home_phone = ($this->request->shipping_home_phone)? $this->request->shipping_home_phone : $this->request->home_phone;
        $customer_address->street = ($this->request->shipping_street)? $this->request->shipping_street : $this->request->street;
        $customer_address->housenumber = ($this->request->shipping_housenumber)? $this->request->shipping_housenumber : $this->request->housenumber;
        $customer_address->city = ($this->request->shipping_city)? $this->request->shipping_city : $this->request->city;
        $customer_address->zipcode = ($this->request->shipping_zipcode)? $this->request->shipping_zipcode : $this->request->zipcode;
        $customer_address->save();

        $this->request->customer_session->customer_id =  $customer->id;
        $this->request->customer_session->save();


        return array("status" => true, "user" => $customer);
    }

	public function forgotPassword()
	{
		$email = $this->request->email;
		$customer = $this->request->shop->customers()->where('email', $email)->first();

		if($customer)
		{
			$password_reset = new CustomerPasswordReset;
			$password_reset->shop_id = $this->request->shop->id;
			$password_reset->customer_id = $customer->id;
			$password_reset->token = str_random(40);
			$password_reset->active = true;
			$password_reset->save();

			return ['status' => true, 'token' => $password_reset->token];
		}

		return ["status" => false];
	}

	public function newPassword()
	{
		$customer = $this->request->shop->customers()->where('email', $this->request->email)->whereHas('customer_password_reset', function($query){
				$query->where('token', $this->request->token)->active();
			})->with('customer_password_reset')->first();

		if($customer)
		{
			$customer->password = bcrypt($this->request->password);
			$customer->save();
			$customer->customer_password_reset->deactivate();

			return array("user" => $customer, "status" => true);
		}

		return array("status" => false);
	}

	private function mergeCarts()
	{
		$previous_customer_session = CustomerSession::where('customer_id', $this->request->customer_session->customer_id)->where('checked_out', false)->whereHas('cart')->latest()->first();

		if($previous_customer_session)
		{
			foreach($previous_customer_session->cart->cart_products as $cart_product)
			{
				if(!$this->cart()->cart_products->contains($cart_product->product_id))
				{
					//Reimport from old cart to new cart
					$this->request->products = array([
						"product_id" 	=> $cart_product->product_id,
						"qty"			=> $cart_product->qty,
						"static" 		=> false
					]);

					$this->addToCart();
				}
			}
		}
	}

	public function checkEmail()
	{
		$customer = $this->request->shop->customer()->email($this->request->email)->first();

		if($customer)
		{
			return array('status' => true);
		}

		return array('status' => false);
	}

	public function orders()
	{
		if($this->request->customer_session->customer)
		{
			return array('status' => true, 'orders' => $this->request->customer_session->customer->orders()->latest()->get());
		}

		return array('status' => false);
	}

	public function order()
	{
		if($this->request->customer_session->customer)
		{
			$order = $this->request->customer_session->customer->orders()->reference($this->request->reference)->with(['payment', 'customer', 'lines'])->first();

			return array('status' => true, 'order' => $order);
		}

		return array('status' => false);
	}
}
