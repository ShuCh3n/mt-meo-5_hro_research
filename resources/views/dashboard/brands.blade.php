@extends('dashboard.layout.main')

@section('content')
	<div class="wrapper wrapper-content animated fadeInRight ecommerce">
        <div class="ibox-content m-b-sm border-bottom">
            <div class="row">
                <div class="col-xs-12">
                    <a class="btn btn-primary pull-right" href="{{ route('new_brand') }}"><i class="fa fa-plus-circle" aria-hidden="true"></i> New brand</a>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-content">

                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>Brand</th>
                                    <th>Products</th>
                                    <th class="text-right" data-sort-ignore="true">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($brands as $brand)
                                    <tr>
                                        <td>
                                           {{ $brand->name }}
                                        </td>
                                        <td>
                                           {{ $brand->products->count() }}
                                        </td>
                                        <td class="text-right">
                                            <div class="btn-group">
                                                <a href="{{ route('edit_brand', $brand->id) }}" class="btn-white btn btn-xs">Edit</a>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>

                        <div class="text-center">
                            {{ $brands->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop