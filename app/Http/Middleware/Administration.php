<?php

namespace App\Http\Middleware;

use Closure;
use App\AdminUsers;

class Administration
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $adminUser = AdminUsers::where('token', $request->header('AdminToken'))->first();

        if($adminUser)
        {
            return $next($request);
        }

        return response(['status' => false, 'message' => 'Incorrect token or token not set']);
    }
}
