<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerAddress extends Model
{
    public function customer()
    {
    	return $this->belongsTo(Customer::class);
    }

    public function country()
    {
    	return $this->belongsTo(Country::class);
    }

    public function scopeDefault($query)
    {
        return $query->where('default', true);
    }

    public function makeDefault()
    {
    	$this_customer_addresses = self::where('customer_id', $this->customer_id)->get();

    	foreach($this_customer_addresses as $address)
    	{
    		$address->default = 0;
    		$address->save();
    	}
    	
    	$this->default = 1;
    	$this->save();
    }
}
