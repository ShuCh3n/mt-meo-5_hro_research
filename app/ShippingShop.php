<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShippingShop extends Model
{
    protected $table = "shipping_shop";
}
