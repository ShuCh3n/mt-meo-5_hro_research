@extends('dashboard.layout.main')

@section('content')
	<div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>{{ trans('pages.inquiry_detail_header') }}</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('home') }}">{{ trans('menu.home') }}</a>
                </li>
                <li>
                    <a href="">{{ trans('menu.inquiries') }}</a>
                </li>
                <li class="active">
                    <strong>{{ trans('page.inquiry') }}</strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">

        <form method="post" class="form-horizontal">
            <div class="row">
                <div class="col-md-8">
                    <div class="ibox">
                        <div class="ibox-title">
                            <h5>New Customer</h5>
                        </div>

                        <div class="ibox-content">
                            <div class="mb20"></div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Shop<span class="text-danger">*</span></label>
                                        <div class="col-md-10">
                                            <select2 class="form-control" name="shop">
                                                <option value="">Select shop</option>

                                                @foreach($shops as $shop)
                                                    <option value="{{ $shop->id }}" {{ ($customer->shop_id == $shop->id)? 'selected' : '' }}>{{ $shop->name }}</option>
                                                @endforeach
                                            </select2>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Active<span class="text-danger">*</span></label>
                                        <div class="col-md-5">
                                            <label><input type="radio" class="form-control i-checks" name="active" value="1" {{ ($customer->active == 1)? "checked" : "" }}> Enabled</label>
                                        </div>
                                        <div class="col-md-5">
                                            <label><input type="radio" class="form-control i-checks" name="active" value="0" {{ ($customer->active == 0)? "checked" : "" }}> Disabled</label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Social title<span class="text-danger">*</span></label>
                                        <div class="col-md-5">
                                            <label><input type="radio" class="form-control i-checks" name="gender" value="1" {{ ($customer->gender == 1)? "checked" : "" }}> Mr.</label>
                                        </div>
                                        <div class="col-md-5">
                                            <label><input type="radio" class="form-control i-checks" name="gender" value="0" {{ ($customer->gender == 0)? "checked" : "" }}> Mrs.</label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Name</label>
                                        <div class="col-md-5">
                                            <input type="text" class="form-control" name="firstname" placeholder="Firstname" value="{{ $customer->firstname }}">
                                        </div>
                                        <div class="col-md-5">
                                            <input type="text" class="form-control" name="lastname" placeholder="Lastname" value="{{ $customer->lastname }}">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Phonenumber</label>
                                        <div class="col-md-10">
                                            <phone-input 
                                            countries="{{ $countries }}" 
                                            selected_country="{{ country('us') }}" 
                                            name_country="phone_country" 
                                            name="phone" 
                                            placeholder="Phone" 
                                            value="{{ old('phone') ?? $customer->home_phone }}"
                                            ></phone-input>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Mobile number</label>
                                        <div class="col-md-10">
                                            <phone-input 
                                            countries="{{ $countries }}" 
                                            selected_country="{{ country('us') }}" 
                                            name_country="mobile_phone_country" 
                                            name="mobile_phone" 
                                            placeholder="Mobile Phone" 
                                            value="{{ old('mobile_phone') ?? $customer->mobile_phone }}"
                                            ></phone-input>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Account<span class="text-danger">*</span></label>
                                        <div class="col-md-5">
                                            <input type="text" class="form-control" name="email" placeholder="Email" value="{{ $customer->email }}">
                                        </div>
                                        <div class="col-md-5">
                                            <password-input name="password" placeholder="Leave blank to keep old password"></password-input>
                                        </div>
                                    </div>

                                    <div id="dob" class="form-group" id="dob">
                                        <label class="col-md-2 control-label">Date of birth</label>

                                        <div class="col-md-10">
                                            <datepicker placeholder="Date of birth" name="dob" value="{{ ($customer->dob)? $customer->dob->format('d M Y') : '' }}"></datepicker>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="ibox">
                        <div class="ibox-title">
                            <h5>Billing Address</h5>
                        </div>

                        <div class="ibox-content">
                            <div class="mb20"></div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Company</label>
                                        <div class="col-md-5">
                                            <input type="text" class="form-control" name="company" placeholder="Company" value="{{ $customer->billing_company }}">
                                        </div>
                                        <div class="mb5-sm"></div>
                                        <div class="col-md-5">
                                            <input type="text" class="form-control" name="vat_number" placeholder="VAT Number" value="{{ $customer->billing_vat_number }}">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Street<span class="text-danger">*</span></label>
                                        <div class="col-md-5">
                                            <input type="text" class="form-control" name="street" placeholder="Street" value="{{ $customer->billing_street }}">
                                        </div>
                                        <div class="mb5-sm"></div>
                                        <div class="col-md-5">
                                            <input type="text" class="form-control" name="housenumber" placeholder="Housenumber" value="{{ $customer->billing_housenumber }}">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">City / Zipcode<span class="text-danger">*</span></label>
                                        <div class="col-md-5">
                                            <input type="text" class="form-control" name="city" placeholder="City" value="{{ $customer->billing_city }}">
                                        </div>
                                        <div class="mb5-sm"></div>
                                        <div class="col-md-5">
                                            <input type="text" class="form-control" name="zipcode" placeholder="Zipcode" value="{{ $customer->billing_zipcode }}">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Country<span class="text-danger">*</span></label>
                                        <div class="col-md-10">
                                            <country-select name="country">
                                                <option value=""></option>
                                                @foreach($countries as $country)
                                                    <option value="{{ $country->id }}" icon="{{ $country->icon }}" {{ ($customer->billing_country_id == $country->id)? 'selected' : '' }}>{{ $country->name }}</option>
                                                @endforeach
                                            </country-select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">State<span class="text-danger">*</span></label>
                                        <div class="col-md-10">
                                            <input type="text" class="form-control" name="state" placeholder="State" value="{{ $customer->billing_state }}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="ibox">
                        <div class="ibox-title">
                            <h5>Extra info</h5>
                        </div>

                        <div class="ibox-content">
                            <div class="mb20"></div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Newsletter<span class="text-danger">*</span></label>
                                        <div class="col-md-5">
                                            <label><input type="radio" class="form-control i-checks" name="newsletter" value="1" {{ ($customer->newsletter == 1)? "checked" : "" }}> Enabled</label>
                                        </div>
                                        <div class="col-md-5">
                                            <label><input type="radio" class="form-control i-checks" name="newsletter" value="0" {{ ($customer->newsletter == 0)? "checked" : "" }}> Disabled</label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Opt-in<span class="text-danger">*</span></label>
                                        <div class="col-md-5">
                                            <label><input type="radio" class="form-control i-checks" name="optin" value="1" {{ ($customer->optin == 1)? "checked" : "" }}> Enabled</label>
                                        </div>
                                        <div class="col-md-5">
                                            <label><input type="radio" class="form-control i-checks" name="optin" value="0" {{ ($customer->optin == 0)? "checked" : "" }}> Disabled</label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Group access</label>
                                        <div class="col-xs-9">
                                            @if($customer_groups->count() > 0)
                                                <select2 class="form-control group" name="group">
                                                    @foreach($customer_groups as $group)
                                                        <option value="{{ $group->id }}" {{ ($customer->customer_group_id == $group->id)? 'selected' : '' }}>{{ $group->name }}</option>
                                                    @endforeach
                                                </select2>
                                            @else
                                                <p class="form-control-static">
                                                    <a href="{{ route('new_customer_group') }}">Create first customer group</a>
                                                </p>
                                            @endif
                                        </div>
                                        <div class="col-xs-1">
                                            <a class="btn btn-warning erase-group"><i class="fa fa-eraser" aria-hidden="true"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="ibox">
                        <div class="ibox-title">
                            <h5>Action</h5>
                        </div>

                        <div class="ibox-content">
                            <strong>Note</strong>
                            <textarea class="form-control" placeholder="Private notes" style="height: 200px;" name="note">{{ old('note') }}</textarea>
                            <div class="mb20"></div>
                            <label><input type="checkbox" class="i-checks"> Notify Customer</label>
                            <div class="mb20"></div>

                            {{ csrf_field() }}

                            <button class="btn btn-block btn-outline btn-success" type="submit"><i class="fa fa-floppy-o"></i> Save</button>
                            <a class="btn btn-block btn-outline btn-primary" href="{{ route('new_customer_address', ["customer_id" => $customer->id ]) }}"><i class="fa fa-map-marker" aria-hidden="true"></i> Add Address</a>
                            <a class="btn btn-block btn-outline btn-warning" type="submit"><i class="fa fa-envelope-o" aria-hidden="true"></i> Send message</a>
                        </div>
                    </div>
                </div>
            </div>
        </form>    
    </div>
@stop