<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Warehouse extends Model
{
    protected $fillable = ["name"];

    public function scopeName($query, $name)
    {
    	return $query->where('name', $name);
    }
}
