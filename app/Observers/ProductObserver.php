<?php

namespace App\Observers;

use App\Jobs\UpdateProduct;
use App\WebHook;

/**
 * Observes the product
 */
class ProductObserver 
{
    /**
     * Function will be triggered when an product is updated
     * 
     * @param $product
     */
    public function created($model)
    {
        $this->dispatch($model);
    }

    public function updated($model)
    {
        $this->dispatch($model, 'updated');
    }

    public function deleted($model)
    {
        $this->dispatch($model, 'deleted');
    }

    public function dispatch($model, $event = 'created'){
        foreach(WebHook::all() as $webhook){
            if($webhook->shop_id){
                foreach($model->shops as $shopProduct){
                    if($webhook->shop_id == $shopProduct->shop_id){
                        dispatch(new UpdateProduct($model, $webhook, $event));
                    }
                }
            }else{
                dispatch(new UpdateProduct($model, $webhook, $event));
            }
        }
    }
}
