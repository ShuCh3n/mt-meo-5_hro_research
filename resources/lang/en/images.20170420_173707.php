<?php
/*************************************************************************
 Generated via "php artisan localization:missing" at 2017/04/10 17:38:45 
*************************************************************************/

return array (
  //==================================== Translations ====================================//
  'brands' => 'brands',
  'drag and drop to upload' => 'drag and drop to upload',
  'groups' => 'groups',
  'image type' => 'image type',
  'products' => 'products',
  'refresh manually' => 'refresh manually',
  'select a shop' => 'select a shop',
  //================================== Obsolete strings ==================================//
  'LLH:obsolete' => 
  array (
    'profiles' => 'profiles',
  ),
);