<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\BlogTags;
use App\Language;

class BlogController extends Controller
{
	public function blogs()
    {
        $blogs = $this->request->shop->blogs()->with(['info' => function($query){
            $query->language($this->request->language->id);
        }])->published()->active()->latest()->get();

        return $blogs;
    }

    public function blog()
    {
        $blog = $this->request->shop->blogs()->post($this->request->id)->with(['info' => function($query){
            $query->language($this->request->language->id);
        }])->published()->active()->firstOrFail();

        return $blog;
    }
}