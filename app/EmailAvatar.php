<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use GuzzleHttp\Client;
use Storage;
use Carbon\Carbon;

class EmailAvatar extends Model
{
    public static function retrieveGravatar($email)
    {
    	$avatar = self::where('email', $email)->first();

    	if($avatar)
    	{
    		$updated = new Carbon($avatar->updated_at);
    		$now = Carbon::now();

    		if($updated->diff($now)->days < 30)
    		{
    			return true;
    		}
    	}

		$small_size_uri = "https://www.gravatar.com/avatar/" . md5( strtolower( trim( $email ) ) ) . "?d=identicon&s=128";
		$medium_size_uri = "https://www.gravatar.com/avatar/" . md5( strtolower( trim( $email ) ) ) . "?d=identicon&s=256";
		$large_size_uri = "https://www.gravatar.com/avatar/" . md5( strtolower( trim( $email ) ) ) . "?d=identicon&s=1024";

		$client = new Client;

		$result = $client->get($small_size_uri, ['http_errors' => false]);

		if($result->getStatusCode() == 200)
		{
			$filename = str_random(40) . '.jpg';
			Storage::put('email_avatars/small/' . $filename, file_get_contents($small_size_uri), 'public');
			Storage::put('email_avatars/medium/' . $filename, file_get_contents($medium_size_uri), 'public');
			Storage::put('email_avatars/large/' . $filename, file_get_contents($large_size_uri), 'public');

			if(!$avatar)
			{
				$avatar = new self;
			}
			
			$avatar->email = $email;
			$avatar->filename = $filename;
			$avatar->save();
		}

		return true;
    }
}
