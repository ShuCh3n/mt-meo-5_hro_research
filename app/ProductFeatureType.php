<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductFeatureType extends Model
{
    public function product_features()
    {
    	return $this->hasMany(ProductFeature::class);
    }

    public function infos()
    {
    	return $this->hasMany(ProductFeatureTypeLanguage::class);
    }

    public function info()
    {
        return $this->hasOne(ProductFeatureTypeLanguage::class);
    }

    public function appendName($language_id, $name)
    {
        $feature_type_language = new ProductFeatureTypeLanguage;
        $feature_type_language->language_id = $language_id;
        $feature_type_language->product_feature_type_id = $this->id;
        $feature_type_language->name = ucfirst(strtolower($name));
        $feature_type_language->save();
    }

    public static function getFeatureType($feature_type)
    {
        if(is_numeric($feature_type))
        {
            $product_feature_type = ProductFeatureType::find($feature_type);
            
            if($product_feature_type)
            {
                return $product_feature_type;
            }
        }

        $product_feature_type_language = ProductFeatureTypeLanguage::where('name', $feature_type)->orWhere('name', ucfirst(strtolower($feature_type)))->where('language_id', 2)->first();

        if($product_feature_type_language)
        {
            return $product_feature_type_language->product_feature_type;
        }
        
        return null;
    }

    public static function firstOrCreateFeatureType($name, $language_id)
    {
        $feature_type = self::whereHas('infos', function($query) use ($name, $language_id){
            $query->where('language_id', $language_id)->where('name', ucfirst(strtolower($name)));
        })->firstOrCreate([]);
        
        if($feature_type->wasRecentlyCreated)
        {
            $feature_type->appendName($language_id, $name);
        }

        return $feature_type;
    }
}
