<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    use SubFilter;

	protected $fillable = ['name'];

	protected $hidden = [
        'created_at',
        'updated_at'
    ];

    public function products()
    {
    	return $this->hasMany(Product::class);
    }

	public function image()
	{
    	return $this->morphMany(Image::class, 'imagable');
	}

	public static function getBrand($brand)
	{
		if(is_int($brand))
		{
			$find_brand = self::findOrFail($brand);
		}

		if(is_string($brand))
		{
			$find_brand = self::where('name', $brand)->first();
		}

		return $find_brand;
	}
}
