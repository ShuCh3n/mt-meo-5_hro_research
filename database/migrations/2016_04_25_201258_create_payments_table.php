<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('image')->nullable();
            $table->string('name');
            $table->text('description')->nullable();
            $table->decimal('fee', 10, 2)->nullable();
            $table->decimal('percentage_fee', 10, 2)->nullable();
            $table->timestamps();
        });


        DB::table('payments')->insert([
            [
                "name"  => "ideal"
            ],
            [
                "name"  => "credit_card"
            ],
            [
                "name"  => "giropay"
            ],
            [
                "name"  => "bancontact"
            ],
            [
                "name"  => "sofort_banking"
            ],
            [
                "name"  => "maestro"
            ],
            [
                "name"  => "cash"
            ],
            [
                "name"  => "visa"
            ],
            [
                "name"  => "mastercard"
            ],
            [
                "name"  => "postepay"
            ],
            [
                "name"  => "cart_bleue"
            ],
            [
                "name"  => "after_pay"
            ],
            [
                "name"  => "paypal"
            ],
            [
                "name"  => "paysafecard"
            ],
            [
                "name"  => "vvv_giftcard"
            ],
            [
                "name"  => "fashioncheque"
            ],
            [
                "name"  => "fashion_giftcard"
            ],
            [
                "name"  => "wijncadeau"
            ],
            [
                "name"  => "gezondheidsbon"
            ],
            [
                "name"  => "podium_cadeaukaart"
            ],
            [
                "name"  => "yourgift_card"
            ],
            [
                "name"  => "webshop_giftcard"
            ],
            [
                "name"  => "instore_v-pay"
            ],
            [
                "name"  => "instore_maestro"
            ],
            [
                "name"  => "instore_visa"
            ],
            [
                "name"  => "instore_mastercard"
            ],
            [
                "name"  => "instore_unionpay"
            ],
            [
                "name"  => "instore_jcb"
            ],
            [
                "name"  => "instore_diners"
            ],
            [
                "name"  => "instore_discover"
            ],
            [
                "name"  => "instore_american_express"
            ]
        ]);

        Schema::create('payment_shop', function(Blueprint $table){
            $table->integer('payment_id')->unsigned();
            $table->integer('shop_id')->unsigned();

            $table->foreign('payment_id')
                ->references('id')
                ->on('payments')
                ->onDelete('cascade');

            $table->foreign('shop_id')
                ->references('id')
                ->on('shops')
                ->onDelete('cascade');

            $table->primary(['payment_id', 'shop_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('payment_shop');
        Schema::drop('payments');
    }
}
