@extends('dashboard.layout.main')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>{{ trans('pages.inquiry_detail_header') }}</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('home') }}">{{ trans('menu.home') }}</a>
                </li>
                <li>
                    <a href="">{{ trans('menu.inquiries') }}</a>
                </li>
                <li class="active">
                    <strong>{{ trans('page.inquiry') }}</strong>
                </li>
            </ol>
        </div>
    </div>

    @if($shop)
        <div class="row">
            <div class="alert alert-warning">
                Warning! You're editing the shop product of <strong>{{ $shop->name }}</strong>.
                <a href="{{ route('edit_product', $product->id) }}" class="btn btn-outline btn-warning pull-right btn-xs"><i class="fa fa-pencil" aria-hidden="true"></i> Edit General</a>
            </div>
        </div>
    @endif

    <div class="wrapper wrapper-content animated fadeInRight">
        <form method="post" class="form-horizontal" action="{{route('update_product', $product->id)}}">
            <div class="mb20"></div>

            <div class="row">
                <div class="col-md-8">
                    <div class="tabs-container">
                        <div class="tabs-left">
                            <ul class="nav nav-tabs">
                                <li class="active"><a data-toggle="tab" href="#tab-general"><i class="fa fa-certificate" aria-hidden="true"></i> <span class="hidden-xs">General</span></a></li>
                                <li class=""><a data-toggle="tab" href="#tab-information"><i class="fa fa-info-circle" aria-hidden="true"></i> <span class="hidden-xs">Information</span></a></li>
                                <li class=""><a data-toggle="tab" href="#tab-features"><i class="fa fa-check-square-o" aria-hidden="true"></i> <span class="hidden-xs">Features</span></a></li>
                                <li class=""><a data-toggle="tab" href="#tab-prices"><i class="fa fa-usd" aria-hidden="true"></i> <span class="hidden-xs">Prices</span></a></li>
                                <li class=""><a data-toggle="tab" href="#tab-associations"><i class="fa fa-sitemap" aria-hidden="true"></i> <span class="hidden-xs">Associations</span></a></li>
                                <li class=""><a data-toggle="tab" href="#tab-combinations"><i class="fa fa-share-alt" aria-hidden="true"></i> <span class="hidden-xs">Combinations</span></a></li>
                                <li class=""><a data-toggle="tab" href="#tab-quantities"><i class="fa fa-cubes" aria-hidden="true"></i> <span class="hidden-xs">Quantities</span></a></li>
                                <li class=""><a data-toggle="tab" href="#tab-shipping"><i class="fa fa-truck" aria-hidden="true"></i> <span class="hidden-xs">Shipping</span></a></li>
                            </ul>
                            <div class="tab-content ">
                                <div id="tab-general" class="tab-pane active">
                                    <div class="panel-body">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="col-md-2 control-label">Active</label>
                                                <div class="col-md-5">
                                                    <label><input type="radio" class="form-control i-checks" name="active" value="1" checked> Enabled</label>
                                                </div>
                                                <div class="col-md-5">
                                                    <label><input type="radio" class="form-control i-checks" name="active" value="0"> Disabled</label>
                                                </div>
                                            </div>

                                            @if($shop)
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">Featured</label>
                                                    <div class="col-md-5">
                                                        <label><input type="radio" class="form-control i-checks" name="featured" value="1" checked> Yes</label>
                                                    </div>
                                                    <div class="col-md-5">
                                                        <label><input type="radio" class="form-control i-checks" name="featured" value="0"> No</label>
                                                    </div>
                                                </div>
                                            @endif

                                            <div class="form-group">
                                                <label class="col-md-2 control-label">Supplier SKU</label>
                                                @if($shop)
                                                    <div class="col-md-3"><p class="form-control-static">{{ $product->supplier_sku }}</p></div>
                                                    <div class="col-md-4"><p class="form-control-static">EAN: {{ $product->ean }}</p></div>
                                                    <div class="col-md-3"><p class="form-control-static">UPC: {{ $product->upc }}</p></div>
                                                @else
                                                    <div class="col-md-3"><input type="text" class="form-control" name="supplier_sku" placeholder="Supplier SKU" value="{{ $product->supplier_sku }}"></div>
                                                    <div class="col-md-4"><input type="text" class="form-control" name="ean" placeholder="EAN-13 or JAN barcode" value="{{ $product->ean }}"></div>
                                                    <div class="col-md-3"><input type="text" class="form-control" name="upc" placeholder="UPC barcode" value="{{ $product->upc }}"></div>
                                                @endif
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-2 control-label">SKU</label>
                                                @if($shop)
                                                    <div class="col-md-10"><p class="form-control-static">{{ $product->sku }}</p></div>
                                                @else
                                                    <div class="col-md-10"><input type="text" class="form-control" name="sku" placeholder="Own SKU" value="{{ $product->sku }}"></div>
                                                @endif
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-2 control-label">Minimum QTY</label>

                                                <div class="col-md-10">
                                                    @if($shop)
                                                        <integer-input name="minimum_qty" placeholder="Minimum QTY" value="{{ $product->shop_products->first()->minimum_qty }}"></integer-input>
                                                    @else
                                                        <integer-input name="minimum_qty" placeholder="Minimum QTY" value="{{ $product->minimum_qty }}"></integer-input>
                                                    @endif
                                                </div>

                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-2 control-label">Brand</label>
                                                <div class="col-md-10">
                                                    @if($shop)
                                                        {{ @$product->brand->name }}
                                                    @else
                                                        @if($brands->count() > 0)
                                                            <select2 name="brand" select="{{ $product->brand_id }}">
                                                                <option value="">Select</option>
                                                                @foreach($brands as $brand)
                                                                    <option value="{{ $brand->id }}">{{ $brand->name }}</option>
                                                                @endforeach
                                                            </select2>
                                                        @else
                                                            <p class="form-control-static">
                                                                <a href="{{ route('new_brand') }}">Create first brand</a>
                                                            </p>
                                                        @endif
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-2 control-label">Tax</label>
                                                @if($tax_groups->count() > 0)
                                                    <tax-select tax_groups="{{ $tax_groups }}" selected_tax_group="{{ $product->shop_product->tax_rule->tax_group_id }}"  selected_tax_rule="{{ $product->shop_product->tax_rule->id }}"></tax-select>
                                                @else
                                                    <div class="col-md-10">
                                                        <p class="form-control-static">
                                                            <a href="{{ route('new_tax_group') }}">Create first tax group</a>
                                                        </p>
                                                    </div>
                                                @endif

                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <div id="tab-information" class="tab-pane">
                                    <div class="panel-body">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="col-md-2 control-label">Language</label>
                                                <div class="col-md-10">
                                                    <select id="multi-language" class="form-control countries-select">
                                                        <option value="all" icon="United-Nations" selected>All</option>

                                                        @foreach($languages as $language)
                                                            <option value="{{ $language->id }}" icon="{{ $language->icon }}" {{ (auth()->user()->options()->language_id == $language->id)? 'selected' : '' }}>{{ $language->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="multi-language-info-box multi-language-all">
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">Product Name</label>
                                                    <div class="col-md-10"><input type="text" class="form-control all-language-input product-name-input" data-input="product-name-input" placeholder="Product Name" value="{{ old('product_info.all.product_name') }}"></div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">Meta Description</label>
                                                    <div class="col-md-10"><input type="text" class="form-control all-language-input product-meta-description-input" name="product_info[all][meta_description]" data-input="product-meta-description-input" placeholder="Describe short" value="{{ old('product_info.all.meta_description') }}"></div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">Meta Link</label>
                                                    <div class="col-md-10"><input type="text" class="form-control all-language-input product-meta-link-input" data-input="product-meta-link-input" name="product_info[all][meta_link]" placeholder="Custom Link" value="{{ old('product_info.all.meta_link') }}"></div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">Tags</label>
                                                    <div class="col-md-10"><input class="all-language-tagsinput form-control" type="text" placeholder="Product Tags" value=""/></div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">Description</label>
                                                    <div class="col-md-10">
                                                        <textarea class="all-language-summernote"></textarea>
                                                    </div>
                                                </div>
                                            </div>

                                            @foreach($languages as $language)
                                                <div class="multi-language-info-box multi-language-{{ $language->id }}">
                                                    @if($shop)
                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label">Product Name</label>
                                                            <div class="col-md-10"><input type="text" class="form-control product-name-input" name="product_info[{{ $language->id }}][product_name]" placeholder="Product Name" value="{{ @$product->shop_infos->where('language_id', $language->id)->first()->name }}"></div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label">Meta Description</label>
                                                            <div class="col-md-10"><input type="text" class="form-control product-meta-description-input" name="product_info[{{ $language->id }}][meta_description]" placeholder="Describe short" value="{{ @$product->shop_infos->where('language_id', $language->id)->first()->short_description }}"></div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label">Meta Link</label>
                                                            <div class="col-md-10"><input type="text" class="form-control product-meta-link-input" name="product_info[{{ $language->id }}][meta_link]" placeholder="Custom Link" value="{{ @$product->shop_infos->where('language_id', $language->id)->first()->link }}"></div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label">Tags</label>
                                                            <div class="col-md-10"><input class="tagsinput form-control" value="{{ ($product->tags)? $product->tags[$language->id] : '' }}" type="text" placeholder="Product Tags" name="product_info[{{ $language->id }}][tags]" /></div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label">Description</label>
                                                            <div class="col-md-10">
                                                                <textarea class="summernote" name="product_info[{{ $language->id }}][description]">
                                                                    {{ @$product->shop_infos->where('language_id', $language->id)->first()->description }}
                                                                </textarea>
                                                            </div>
                                                        </div>
                                                    @else
                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label">Product Name</label>
                                                            <div class="col-md-10"><input type="text" class="form-control product-name-input" name="product_info[{{ $language->id }}][product_name]" placeholder="Product Name" value="{{ $product->info->name }}"></div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label">Meta Description</label>
                                                            <div class="col-md-10"><input type="text" class="form-control product-meta-description-input" name="product_info[{{ $language->id }}][meta_description]" placeholder="Describe short" value="{{ $product->info->short_description }}"></div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label">Meta Link</label>
                                                            <div class="col-md-10"><input type="text" class="form-control product-meta-link-input" name="product_info[{{ $language->id }}][meta_link]" placeholder="Custom Link" value="{{ $product->info->link }}"></div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label">Tags</label>
                                                            <div class="col-md-10"><input class="tagsinput form-control" value="{{ ($product->tags)? $product->tags[$language->id] : '' }}" type="text" placeholder="Product Tags" name="product_info[{{ $language->id }}][tags]" /></div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label">Description</label>
                                                            <div class="col-md-10">
                                                                <textarea class="summernote" name="product_info[{{ $language->id }}][description]">
                                                                    {{ $product->info->description }}
                                                                </textarea>
                                                            </div>
                                                        </div>
                                                    @endif
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>

                                <div id="tab-features" class="tab-pane">
                                    <div class="panel-body">
                                        <div class="col-md-12">

                                            <div class="form-group">
                                                <label class="col-md-2 control-label">Feature Types</label>
                                                <div class="col-md-10">
                                                    @if($shop)
                                                        <p class="form-control-static">{{ $product->info->name }}</p>
                                                    @else
                                                        <select2 name="feature_type" select="{{ $product->product_feature_type_id }}">
                                                            <option value="null">No Feature</option>
                                                            @foreach($product_feature_types as $feature_type)
                                                                <option value="{{ $feature_type->id }}">{{$feature_type->info->name}}</option>
                                                            @endforeach
                                                        </select2>
                                                     @endif
                                                </div>
                                            </div>

                                            <hr>

                                            @if($shop)
                                                @foreach($product->product_feature_options as $option)
                                                    <div class="form-group">
                                                        <label class="col-md-2 control-label">{{ collect($option->product_feature->infos)->first()->name }}</label>
                                                        <div class="col-md-10"><p class="form-control-static">{{ collect($option->infos)->first()->name }}</p></div>
                                                    </div>
                                                @endforeach
                                            @else
                                                <div class="features" data-selected="{{ $product->feature_options }}"></div>
                                             @endif
                                        </div>
                                    </div>
                                </div>

                                <div id="tab-prices" class="tab-pane">
                                    <div class="panel-body">
                                        <div class="col-md-12">

                                            @if($shop)
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">Purchase Price</label>
                                                    <div class="col-md-10">
                                                        <p class="form-control-static">{{ $product->product_price->purchasing_price }}</p>
                                                    </div>
                                                </div>

                                                @if($product->shop_prices->count() > 0)
                                                    <div class="form-group">
                                                        <label class="col-md-2 control-label">Price</label>
                                                        <div class="col-md-5">
                                                            <input type="text" class="price-input" name="retail_price" placeholder="Retail Price" value="{{ collect($product->shop_prices)->first()->retail_price }}">
                                                        </div>
                                                        <div class="col-md-5">
                                                            <input type="text" class="price-input" name="list_price" placeholder="List Price" value="{{ collect($product->shop_prices)->first()->list_price }}">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-md-2 control-label">Unit Price</label>
                                                        <div class="col-md-4">
                                                            <input type="text" class="price-input" name="unit_price" placeholder="Unit Price" value="{{ collect($product->shop_prices)->first()->unit_price }}">
                                                        </div>
                                                        <div class="col-md-2 text-center">
                                                            per
                                                        </div>
                                                        <div class="col-md-4">
                                                            <input type="text" class="form-control" name="unit_name" placeholder="Gram, Bottle, Pound" value="{{ collect($product->shop_prices)->first()->retail_price }}">
                                                        </div>
                                                    </div>
                                                @else
                                                    <div class="form-group">
                                                        <label class="col-md-2 control-label">Price</label>
                                                        <div class="col-md-5">
                                                            <input type="text" class="price-input" name="retail_price" placeholder="Retail Price" value="{{ $product->product_price->retail_price }}">
                                                        </div>
                                                        <div class="col-md-5">
                                                            <input type="text" class="price-input" name="list_price" placeholder="List Price" value="{{ $product->product_price->list_price }}">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-md-2 control-label">Unit Price</label>
                                                        <div class="col-md-4">
                                                            <input type="text" class="price-input" name="unit_price" placeholder="Unit Price" value="{{ $product->product_price->unit_qty }}">
                                                        </div>
                                                        <div class="col-md-2 text-center">
                                                            per
                                                        </div>
                                                        <div class="col-md-4">
                                                            <input type="text" class="form-control" name="unit_name" placeholder="Gram, Bottle, Pound" value="{{ $product->product_price->unit_price }}">
                                                        </div>
                                                    </div>
                                                @endif
                                            @else
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">Purchase Price</label>
                                                    <div class="col-md-10">
                                                        <input type="text" class="price-input" name="purchasing_price" placeholder="Purchasing Price" value="{{ $product->product_price->purchasing_price ?? 0 }}">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">Price</label>
                                                    <div class="col-md-5">
                                                        <input type="text" class="price-input" name="retail_price" placeholder="Retail Price" value="{{ $product->product_price->retail_price ?? 0 }}">
                                                    </div>
                                                    <div class="col-md-5">
                                                        <input type="text" class="price-input" name="list_price" placeholder="List Price" value="{{ $product->product_price->list_price ?? 0 }}">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">Unit Price</label>
                                                    <div class="col-md-4">
                                                        <input type="text" class="qty-input" name="unit_qty" placeholder="Unit Price" value="{{ $product->product_price->unit_qty ?? 0 }}">
                                                    </div>
                                                    <div class="col-md-2 text-center">
                                                        per
                                                    </div>
                                                    <div class="col-md-4">
                                                        <input type="text" class="price-input" name="unit_price" placeholder="Gram, Bottle, Pound" value="{{ $product->product_price->unit_price ?? 0 }}">
                                                    </div>
                                                </div>
                                            @endif

                                        </div>
                                    </div>
                                </div>

                                <div id="tab-associations" class="tab-pane">
                                    <div class="panel-body">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="col-md-2 control-label">Shops</label>

                                                <div class="col-md-10">
                                                    @foreach($shops as $shop)
                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                <label>
                                                                    <input class="i-checks" type="checkbox" name="shops[]" value="{{ $shop->id }}" {{ ($product->hasShopID($shop->id))? 'checked' : '' }} /> {{ $shop->name }}
                                                                </label>
                                                            </div>

                                                            <div class="col-md-9">
                                                                @if($shop->categories->count() > 0)
                                                                    <select class="select2 form-control" multiple="multiple" name="category[{{ $shop->id }}][]">
                                                                        @foreach($shop->categories as $category)
                                                                            <option value="{{ $category->id }}" {{ ($product->hasCategoryID($category->id))? 'selected' : '' }}>{{ $category->info->name }}</option>
                                                                        @endforeach
                                                                    </select>

                                                                @else
                                                                    <p class="form-control-static">
                                                                        <a href="{{ route('new_category', ["shop_id" => $shop->id]) }}">Create first category</a>
                                                                    </p>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="mb15"></div>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div id="tab-combinations" class="tab-pane">
                                    <div class="panel-body">
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-striped">
                                                <thead>
                                                <tr>
                                                    <th>
                                                        Attribute - Value Pair
                                                    </th>
                                                    <th>
                                                        Impact on price
                                                    </th>
                                                    <th>
                                                        Impact on weight
                                                    </th>
                                                    <th>
                                                        Reference
                                                    </th>
                                                    <th>
                                                        Action
                                                    </th>
                                                </tr>
                                                </thead>
                                                <tbody class="combinations-table">
                                                    @foreach($product->product_combinations as $combination)
                                                        <tr id="{{ $combination->id }}">
                                                            <td>
                                                                @foreach($combination->product_attribute_options as $option)
                                                                    {{ $option->product_attribute->info->name }} : {{ $option->info->name }},
                                                                @endforeach
                                                            </td>
                                                            <td>
                                                                <i aria-hidden="true" class="fa fa-{{ ($combination->impact_price_type)? 'plus' : 'minus' }}"></i> € 3.00
                                                            </td>
                                                            <td>
                                                                <i aria-hidden="true" class="fa fa-{{ ($combination->impact_weight_type)? 'plus' : 'minus' }}"></i> 0.30 KG
                                                            </td>
                                                            <td>{{ $combination->supplier_sku }}</td>
                                                            <td class="text-center">
                                                                <div class="btn-group">
                                                                    <button data-target="#addCombination" data-toggle="modal" data-edit-combination="{{ $combination->id }}" class="btn-white btn btn-sm edit-combination" type="button">
                                                                        <i aria-hidden="true" class="fa fa-pencil-square-o"></i>
                                                                    </button>
                                                                    <button type="button" data-remove-combination="{{ $combination->id }}" class="btn-white btn btn-sm remove-combination">
                                                                        <i aria-hidden="true" class="fa fa-times"></i>
                                                                    </button>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <input type="hidden" value="{{ $combination->getInJson() }}" data-combination="{{ $combination->id }}" name="combinations[][{{ $combination->id }}]">
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>

                                        <button type="button" class="btn btn-primary pull-right add-combination-modal" data-toggle="modal" data-target="#addCombination">
                                            <i class="fa fa-plus-circle" aria-hidden="true"></i> Add Combination
                                        </button>

                                        <div class="modal inmodal fade" id="addCombination" role="dialog"  aria-hidden="true">
                                            <div class="modal-dialog modal-lg">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                        <h4 class="modal-title">Add Combination</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="form-horizontal">
                                                            <div class="form-group">
                                                                <label class="col-md-2 control-label">Default</label>
                                                                <div class="col-md-5">
                                                                    <label><input type="radio" name="default-combination" class="form-control i-checks default-combination" value="1"> Yes</label>
                                                                </div>
                                                                <div class="col-md-5">
                                                                    <label><input type="radio" name="default-combination" class="form-control i-checks default-combination" value="0" checked> No</label>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="col-md-2 control-label">Attribute</label>
                                                                <div class="col-md-5">
                                                                    <select class="form-control select2 combination-attribute">
                                                                        <option selected="selected" value="">Choose an Attribute</option>
                                                                        @foreach($attributes as $attribute)
                                                                            <option value="{{ $attribute->id }}">{{ $attribute->info->name }}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                                <div class="col-md-5">
                                                                    <select class="form-control select2 combination-attribute-options">
                                                                        <option selected="selected" value="">Choose an Option</option>
                                                                    </select>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <div class="col-md-5 col-md-offset-2">
                                                                    <button class="btn btn-block btn-danger remove-combination-attribute" type="button"><i aria-hidden="true" class="fa fa-minus-circle"></i> Remove</button>
                                                                </div>
                                                                <div class="col-md-5">
                                                                    <button class="btn btn-block btn-primary add-combination" type="button"><i aria-hidden="true" class="fa fa-plus-circle"></i> Add</button>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <div class="col-md-10 col-md-offset-2">
                                                                    <select id="selected-options" class="selected-options form-control" multiple=""></select>
                                                                </div>
                                                            </div>

                                                            <hr>

                                                            <div class="form-group">
                                                                <label class="col-md-2 control-label">Supplier SKU</label>
                                                                <div class="col-md-3"><input id="combination-supplier-sku" type="text" class="form-control" placeholder="Supplier SKU"></div>
                                                                <div class="col-md-4"><input id="combination-ean" type="text" class="form-control" placeholder="EAN-13 or JAN barcode"></div>
                                                                <div class="col-md-3"><input id="combination-upc" type="text" class="form-control" placeholder="UPC barcode"></div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="col-md-2 control-label">Own SKU</label>
                                                                <div class="col-md-10"><input id="combination-sku" type="text" class="form-control" placeholder="Own SKU"></div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="col-md-2 control-label">Minimum QTY</label>
                                                                <div class="col-md-10"><input id="combination-minimum-qty" type="text" class="form-control qty-input" placeholder="Minimum QTY"></div>
                                                            </div>

                                                            <hr>

                                                            @if($warehouses->count() > 0)
                                                                @foreach($warehouses as $warehouse)
                                                                    <div class="form-group">
                                                                        <label class="col-md-2 control-label">{{ $warehouse->name }}</label>
                                                                        <div class="col-md-10">
                                                                            <input type="text" class="form-control qty-input warehouse-stock" data-warehouse="{{ $warehouse->id }}" name="stock[{{ $warehouse->id }}]" placeholder="Stock">
                                                                        </div>
                                                                    </div>
                                                                @endforeach
                                                            @else
                                                                <div class="form-group">
                                                                    <label class="col-md-2 control-label">Stock</label>
                                                                    <div class="col-md-10">
                                                                        <p class="form-control-static">
                                                                            <a href="{{ route('new_warehouse') }}">Create first warehouse</a>
                                                                        </p>
                                                                    </div>
                                                                </div>
                                                            @endif

                                                            <hr>

                                                            <div class="form-group">
                                                                <label class="col-md-2 control-label">Impact on price</label>
                                                                <div class="col-md-5">
                                                                    <select class="select2" id="impact-price">
                                                                        <option value="null">None</option>
                                                                        <option value="1">Increase</option>
                                                                        <option value="0">Reduce</option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-md-5">
                                                                    <input id="impact-price-value" class="price-input" type="text" placeholder="Impact on price">
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="col-md-2 control-label">Impact on weight</label>
                                                                <div class="col-md-5">
                                                                    <select class="select2" id="impact-weight">
                                                                        <option value="null">None</option>
                                                                        <option value="1">Increase</option>
                                                                        <option value="0">Reduce</option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-md-5">
                                                                    <input id="impact-weight-value" class="weight-input" type="text" placeholder="Impact on weight">
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="col-md-2 control-label">Impact on unit price</label>
                                                                <div class="col-md-5">
                                                                    <select class="select2" id="impact-unit-price">
                                                                        <option value="null">None</option>
                                                                        <option value="1">Increase</option>
                                                                        <option value="0">Reduce</option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-md-5">
                                                                    <input type="text" id="impact-unit-price-value" placeholder="Impact on unit price">
                                                                </div>
                                                            </div>

                                                            <hr>

                                                            <div class="form-group">
                                                                <label class="col-md-2 control-label">Images</label>
                                                                <div class="col-md-10">
                                                                    <div class="combination_images">
                                                                        <div>
                                                                            <label class="text-center">
                                                                                <img class="img-lg" src="http://www.rd.com/wp-content/uploads/sites/2/2016/04/01-cat-wants-to-tell-you-laptop.jpg"><br />
                                                                                <div class="mb10"></div>
                                                                                <input type="checkbox" class="form-control i-checks combination-image" value="1">
                                                                            </label>
                                                                        </div>
                                                                        <div>
                                                                            <label class="text-center">
                                                                                <img class="img-lg" src="http://www.rd.com/wp-content/uploads/sites/2/2016/04/01-cat-wants-to-tell-you-laptop.jpg"><br />
                                                                                <div class="mb10"></div>
                                                                                <input type="checkbox" class="form-control i-checks combination-image" value="2">
                                                                            </label>
                                                                        </div>
                                                                        <div>
                                                                            <label class="text-center">
                                                                                <img class="img-lg" src="http://www.rd.com/wp-content/uploads/sites/2/2016/04/01-cat-wants-to-tell-you-laptop.jpg"><br />
                                                                                <div class="mb10"></div>
                                                                                <input type="checkbox" class="form-control i-checks combination-image" value="3">
                                                                            </label>
                                                                        </div>
                                                                        <div>
                                                                            <label class="text-center">
                                                                                <img class="img-lg" src=""><br />
                                                                                <div class="mb10"></div>
                                                                                <input type="checkbox" class="form-control i-checks combination-image" value="4">
                                                                            </label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>

                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                                                        <button type="button" class="btn btn-primary add" id="save-combination" data-dismiss="modal">Add</button>
                                                        <button type="button" class="btn btn-primary edit" id="save-edit-combination" data-dismiss="modal">Edit</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div id="tab-quantities" class="tab-pane">
                                    <div class="panel-body">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="col-md-2 control-label">Stock management</label>
                                                <div class="col-md-5">
                                                    <label><input type="radio" class="form-control i-checks" name="stock_management" value="1" {{ ($product->stock_management)? 'checked' : '' }}> Yes</label>
                                                </div>
                                                <div class="col-md-5">
                                                    <label><input type="radio" class="form-control i-checks" name="stock_management" value="0" {{ (!$product->stock_management)? 'checked' : '' }}> No</label>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-2 control-label">Active when out of stock</label>
                                                <div class="col-md-5">
                                                    <label><input type="radio" class="form-control i-checks" name="deny_order" value="1" {{ ($product->deny_order_when_out_of_stock)? 'checked' : '' }}> Active</label>
                                                </div>
                                                <div class="col-md-5">
                                                    <label><input type="radio" class="form-control i-checks" name="deny_order" value="0" {{ (!$product->deny_order_when_out_of_stock)? 'checked' : '' }}> Disabled</label>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-2 control-label">When out of stock</label>
                                                <div class="col-md-5">
                                                    <label><input type="radio" class="form-control i-checks" name="deny_order" value="1" {{ ($product->deny_order_when_out_of_stock)? 'checked' : '' }}> Deny orders</label>
                                                </div>
                                                <div class="col-md-5">
                                                    <label><input type="radio" class="form-control i-checks" name="deny_order" value="0" {{ (!$product->deny_order_when_out_of_stock)? 'checked' : '' }}> Allow orders</label>
                                                </div>
                                            </div>

                                            @if($warehouses->count() > 0)
                                                @foreach($warehouses as $warehouse)
                                                    <div class="form-group">
                                                        <label class="col-md-2 control-label">{{ $warehouse->name }}</label>
                                                        <div class="col-md-10">
                                                            <input type="text" class="form-control qty-input" name="stock[{{ $warehouse->id }}]" placeholder="Stock" value="{{@ $product->stocks[$warehouse->id] }}">
                                                        </div>
                                                    </div>
                                                @endforeach
                                            @else
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">Warehouse</label>
                                                    <div class="col-md-10">
                                                        <p class="form-control-static">
                                                            <a href="{{ route('new_warehouse') }}">Create first warehouse</a>
                                                        </p>
                                                    </div>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div id="tab-shipping" class="tab-pane">
                                    <div class="panel-body">
                                        <div class="col-md-12">

                                            @if($shop)
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">Width</label>
                                                    <div class="col-md-10">
                                                        <p class="form-control-static">{{ $product->width }} CM</p>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">Height</label>
                                                    <div class="col-md-10">
                                                        <p class="form-control-static">{{ $product->height }} CM</p>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">Depth</label>
                                                    <div class="col-md-10">
                                                        <p class="form-control-static">{{ $product->depth }} CM</p>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">Weight</label>
                                                    <div class="col-md-10">
                                                        <p class="form-control-static">{{ $product->weight }} KG</p>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">Additional Fee</label>
                                                    <div class="col-md-10">
                                                        <input type="text" class="form-control price-input" name="additional_fee_per_item" placeholder="Additional fee per item" value="{{ @collect($product->shop_products)->first()->additional_fee_per_item }}">
                                                    </div>
                                                </div>
                                            @else
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">Width</label>
                                                    <div class="col-md-10">
                                                        <input type="text" class="size-input" name="package_width" placeholder="Package width" value="{{ $product->width }}">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">Height</label>
                                                    <div class="col-md-10">
                                                        <input type="text" class="size-input" name="package_height" placeholder="Package height" value="{{ $product->height }}">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">Depth</label>
                                                    <div class="col-md-10">
                                                        <input type="text" class="size-input" name="package_depth" placeholder="Package depth" value="{{ $product->depth }}">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">Weight</label>
                                                    <div class="col-md-10">
                                                        <input type="text" class="form-control weight-input" name="package_weight" placeholder="Package weight" value="{{ $product->weight }}">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">Additional Fee</label>
                                                    <div class="col-md-10">
                                                        <input type="text" class="form-control price-input" name="additional_fee_per_item" placeholder="Additional fee per item" value="{{ $product->additional_fee_per_item }}">
                                                    </div>
                                                </div>
                                            @endif

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="ibox">
                        <div class="ibox-title">
                            <h5>Action</h5>
                        </div>

                        <div class="ibox-content">

                            {{ csrf_field() }}
                            <button class="btn btn-block btn-outline btn-success" type="submit"><i class="fa fa-floppy-o"></i> Save</button>
                        </div>
                        <div class="ibox-content">
                            <a href="{{route('uploadFile', $product->id)}}" class="btn btn-block btn-outline btn-success"><i class="fa fa-file-o"></i> Edit safety sheets</a>
                        </div>

                    </div>
                </div>
            </div>
        </form>

        <div class="mb30"></div>
    </div>
@stop
