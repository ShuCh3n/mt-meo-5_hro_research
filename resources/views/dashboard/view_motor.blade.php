@extends('dashboard.layout.main')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>{{ trans('pages.inquiry_detail_header') }}</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('home') }}">{{ trans('menu.home') }}</a>
                </li>
                <li>
                    <a href="">{{ trans('menu.inquiries') }}</a>
                </li>
                <li class="active">
                    <strong>{{ trans('page.inquiry') }}</strong>
                </li>
            </ol>
        </div>
    </div>

    <script type="text/x-template" id="page-content">
        <div class="wrapper wrapper-content animated fadeInRight">

            <form method="post" class="form-horizontal">
                <div class="mb20"></div>

                <div class="row">
                    <div class="col-md-12">

                        <div class="row">
                            <div class="col-md-8">
                                <div class="ibox">
                                    <div class="ibox-title">
                                        <h5>@lang('motor.motor'): {{ $motor->vehicle_brand->name }} {{ $motor->name }} {{ $motor->model }}</h5>
                                    </div>

                                    <div class="ibox-content">
                                        <div class="mb20"></div>

                                        <div class="row m-b-lg">
                                            <div class="col-sm-6 col-md-8">

                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">@lang('motor.motor')</label>
                                                    <div class="col-md-10">
                                                        <p class="form-control-static">
                                                            {{ $motor->vehicle_brand->name }} {{ $motor->name }} {{ $motor->model }}
                                                        </p>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">@lang('motor.type')</label>
                                                    <div class="col-md-10">
                                                        <p class="form-control-static">
                                                            {{ $motor->type }}
                                                        </p>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">@lang('motor.cc')</label>
                                                    <div class="col-md-10">
                                                        <p class="form-control-static">
                                                            {{ $motor->cc }}
                                                        </p>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">@lang('motor.years')</label>
                                                    <div class="col-md-10">
                                                        <p class="form-control-static">
                                                            {{ $motor_oldest_year }} - {{ $motor_newest_year }}
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-sm-6 col-md-4">
                                                <img class="img-responsive" src="https://azcdubmedia.azureedge.net/media/themes/fab-four/article-content-images/motorbike-insurance/triumph-daytona-phantom-black-main.jpg?la=en-GB">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="ibox">
                                    <div class="ibox-title">
                                        <h5>@lang('motor.action')</h5>
                                    </div>

                                    <div class="ibox-content">
                                        <div class="mb20"></div>

                                        {{ csrf_field() }}
                                        <button class="btn btn-block btn-outline btn-success" type="submit"><i class="fa fa-floppy-o"></i> @lang('motor.save')</button>
                                        <a class="btn btn-block btn-outline btn-primary" href="{{ route('edit_motor', $motor->id) }}"><i class="fa fa-pencil-square-o"></i> @lang('motor.edit')</a>
                                        <a class="btn btn-block btn-primary" href="{{ route('export_motor', $motor->id) }}"><i class="fa fa-file-excel-o" aria-hidden="true"></i> @lang('motor.export_excel')</a>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="ibox">
                            <div class="ibox-title">
                                <h5>@lang('motor.filters')</h5>
                            </div>

                            <div class="ibox-content">
                                <div class="mb20"></div>

                                <div class="row m-b-lg">
                                    <div class="col-md-12">

                                        <div class="form-group">
                                            <label class="col-md-2 control-label">@lang('motor.years')</label>
                                            <div class="col-md-10">
                                                <motor-year-filter start='{{ $motor_oldest_year }}' end='{{ $motor_newest_year }}'></motor-year-filter>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-2 control-label">@lang('motor.category')</label>
                                            <div class="col-md-10">
                                                <motor-category-filter categories="{{ $categories->sortBy('info.name') }}"></motor-category-filter>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="ibox">
                            <div class="ibox-title">
                                <h5>@lang('motor.product')</h5>

                                <div class="ibox-tools">
                                    <input type="checkbox" class="i-checks" checked id="auto_fill"> @lang('motor.auto_fill')
                                </div>
                            </div>

                            <div class="ibox-content">

                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th>@lang('motor.category')</th>

                                            @for($i = $motor_oldest_year; $i <= $motor_newest_year; $i++)
                                                <th class="column_{{ $i }}">{{ $i }}</th>
                                            @endfor
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($subcategories->sortBy('info.name') as $category)
                                            <tr class="category_row {{ $category->parent->id }}">
                                                <td>
                                                    {{ @ $category->info->name }}
                                                </td>

                                                @for($i = $motor_oldest_year; $i <= $motor_newest_year; $i++)
                                                    <td class="column_{{ $i }}">
                                                        <motor-sku-input
                                                        placeholder="SKU"
                                                        ref="{{ $category->id . '_' . $i }}"
                                                        category="{{ $category->id }}"
                                                        year="{{ $i }}"
                                                        name="categories[{{ $category->id }}][{{ $i }}]"
                                                        month_name="month[{{ $category->id }}][{{ $i }}]"
                                                        qty_name="min_qty[{{ $category->id }}][{{ $i }}]"
                                                        value="{{ $motor->categories->where('id', $category->id)->where('pivot.vehicle_year_id', vehicle_year($i)->id)->first()->pivot->sku ?? '' }}"
                                                        valid="{{ $motor->categories->where('id', $category->id)->where('pivot.vehicle_year_id', vehicle_year($i)->id)->first()->pivot->product_id ?? '' }}"
                                                        month_value="{{ (isset($motor->products->where('pivot.category_id', $category->id)->where('pivot.vehicle_year_id', vehicle_year($i)->id)->first()->pivot->month))? carbon()->createFromFormat('m', $motor->products->where('pivot.category_id', $category->id)->where('pivot.vehicle_year_id', vehicle_year($i)->id)->first()->pivot->month)->format('M') : '' }}"
                                                        qty_value="{{ $motor->products->where('pivot.category_id', $category->id)->where('pivot.vehicle_year_id', vehicle_year($i)->id)->first()->pivot->min_qty ?? '' }}"
                                                        ></motor-sku-input>
                                                    </td>
                                                @endfor

                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </form>

            <div class="mb30"></div>
        </div>
    </script>

    <view-motor-page first_year="{{ $motor_oldest_year }}" last_year="{{ $motor_newest_year }}"></view-motor-page>
@stop
