@extends('dashboard.layout.main')

@section('stylesheet')
    <link href="/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="/css/plugins/select2/select2.min.css" rel="stylesheet">

    <link href="/css/plugins/summernote/summernote.css" rel="stylesheet">
    <link href="/css/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css" rel="stylesheet">

    <link href="/css/plugins/owlCarousel/owl.carousel.css" rel="stylesheet">
    <link href="/css/plugins/owlCarousel/owl.theme.css" rel="stylesheet">
    <link href="/css/plugins/owlCarousel/owl.transitions.css" rel="stylesheet">

    <link href="/css/plugins/touchspin/jquery.bootstrap-touchspin.min.css" rel="stylesheet">

    <style type="text/css">
        .tab-pane .select2{
            width: 100% !important;
        }
    </style>
@stop

@section('javascript')
    <!-- Owl carousel-->
    <script src="/js/plugins/owlCarousel/owl.carousel.min.js"></script>

    <!-- iCheck -->
    <script src="/js/plugins/iCheck/icheck.min.js"></script>

    <!-- Select2 -->
    <script src="/js/plugins/select2/select2.full.min.js"></script>

    <!-- SUMMERNOTE -->
    <script src="/js/plugins/summernote/summernote.min.js"></script>

    <!-- Tags Input -->
    <script src="/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>

    <!-- TouchSpin -->
    <script src="/js/plugins/touchspin/jquery.bootstrap-touchspin.min.js"></script>

    <script>
        $(function(){
            $('.tagsinput, .all-language-tagsinput').tagsinput({
                tagClass: 'label label-primary'
            });

            $('.summernote, .all-language-summernote').summernote();

            $('.all-language-tagsinput').on('itemAdded', function(event) {
                $('.tagsinput').val($(this).val());

                $('.tagsinput').tagsinput('destroy');

                $('.tagsinput').tagsinput({
                    tagClass: 'label label-primary'
                });
            });

            $('.all-language-summernote').on('summernote.change', function(we, contents, $editable) {
                $('.summernote').each(function( index ) {
                    $(this).summernote("code", contents);
                });
            });
            
            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });

            if($(".select2.feature-type").val() != "null")
            {
                getFeatures($(".select2.feature-type").val());
            }

            if($(".select2.combination-attribute").val())
            {
                getAttributeOptions($(".select2.combination-attribute").val());
            }

            $(".select2.tax_groups").on("select2:select", function (e) { 
                $.get('/ajax/tax_group/' + $(this).val() + '/tax_rules', function(result){

                    $('.select2.tax_rules').html('');

                    for(var i = 0, len = result.length; i < len; i++)
                    {
                        $('.select2.tax_rules').append($("<option>").val(result[i].id).html(result[i].name));
                    }
                });
            });

            $(".features").on('click', '.reset-feature', function(){
                $("#feature-option-" + $(this).attr('data-feature-id')).select2("val", "");
            });

            $(".select2.feature-type").on("select2:select", function (e) { 
                if($(this).val() != "null")
                {
                    getFeatures($(this).val());
                }
                else
                {
                    $('.features').html("");
                }
            });

            $(".combination_images").owlCarousel({
                items : 4,
            });

            $(".combination-attribute").on("select2:select", function(e){
                getAttributeOptions($(this).val());
            });

            $(".add-combination").click(function(){
                var attribute = $(".combination-attribute option:selected").text();
                var option = $(".combination-attribute-options option:selected");

                $(".selected-options").append($("<option>").val(option.val()).html(attribute + ": " + option.text()));
            });

            $(".remove-combination-attribute").click(function(){
                $(".selected-options option:selected").remove();
            });

            $("#save-combination").click(function(){
                var table_row = getCombinationValues();

                $(".combinations-table").append(table_row);

                emptyCombinationModalForm();
            });

            $("#save-edit-combination").click(function(){
                $("#" + $(this).val()).remove();

                var table_row = getCombinationValues();

                $(".combinations-table").append(table_row);

                emptyCombinationModalForm();
            });
            
            $(document).on('click', '.remove-combination', function(){
                $("#" + $(this).attr('data-remove-combination')).remove();
            });

            $(".add-combination-modal").click(function(){
                $(".modal-footer .edit").hide();
                $(".modal-footer .add").show();
            });

            $(document).on('click', '.edit-combination', function(){
                $(".modal-footer .add").hide();
                $(".modal-footer .edit").show();

                var id = $(this).attr('data-edit-combination');
                var json = JSON.parse($('input[data-combination="' + id + '"]').val());

                $('input[name="default-combination"][value="' + json.default + '"]').iCheck('check');

                for(var i = 0, len = json.options.length; i < len; i++)
                {
                    $('.selected-options').append($("<option>").val(json.options[i]).html(json.options_text[i]));
                }

                $('#combination-supplier-sku').val(json.supplier_sku);
                $('#combination-ean').val(json.ean);
                $('#combination-upc').val(json.upc);
                $('#combination-sku').val(json.sku);
                $('#combination-minimum-qty').val(json.minimum_qty);
                $('#impact-price-value').val(json.impact_price.value);
                $('#impact-weight-value').val(json.impact_weight.value);
                $('#impact-unit-price-value').val(json.impact_unit_price.value);
                $('#impact-price').select2('val', json.impact_price.type);
                $('#impact-weight').select2('val', json.impact_weight.type);
                $('#impact-unit-price').select2('val', json.impact_unit_price.type);

                for(var i = 0, len = json.images.length; i < len; i++)
                {
                    $('.combination-image[value="' + json.images[i] + '"]').iCheck('check');
                }

                $(".modal-footer .edit").val($(this).attr('data-edit-combination'));
            });

            $(".price-input").TouchSpin({
                max: 1000000000,
                min: 0,
                step: 1,
                decimals: 2,
                boostat: 5,
                maxboostedstep: 10,
                prefix: '€',
                buttondown_class: 'btn btn-white',
                buttonup_class: 'btn btn-white'
            });

            $(".weight-input").TouchSpin({
                max: 1000000000,
                min: 0,
                step: 0.1,
                decimals: 2,
                boostat: 5,
                maxboostedstep: 10,
                postfix: 'KG',
                buttondown_class: 'btn btn-white',
                buttonup_class: 'btn btn-white'
            });

            $(".size-input").TouchSpin({
                max: 1000000000,
                min: 0,
                step: 0.1,
                decimals: 2,
                boostat: 5,
                maxboostedstep: 10,
                postfix: 'CM',
                buttondown_class: 'btn btn-white',
                buttonup_class: 'btn btn-white'
            });

            $("#impact-unit-price-value").TouchSpin({
                max: 1000000000,
                min: 0,
                step: 1,
                decimals: 2,
                boostat: 5,
                maxboostedstep: 10,
                prefix: '€ /',
                buttondown_class: 'btn btn-white',
                buttonup_class: 'btn btn-white'
            });

            $(".qty-input").TouchSpin({
                max: 1000000000,
                min: 0,
                step: 1,
                decimals: 0,
                boostat: 5,
                maxboostedstep: 10,
                buttondown_class: 'btn btn-white',
                buttonup_class: 'btn btn-white'
            });
        });

        function getAttributeOptions(id)
        {
            $.get('/ajax/attributes/' + id + '/options', function(result){
                for(var i = 0, len = result.options.length; i < len; i++)
                {
                    $('.select2.combination-attribute-options').append($("<option>").val(result.options[i].id).html(result.options[i].name));
                    initSelect2();
                }
            });
        }

        function getFeatures(id)
        {
            $.get('/ajax/feature-type/' + id + '/features', function(result){
                 $('.features').html("");

                for(var i = 0, len = result.product_features.length; i < len; i++)
                {
                    var body = '<div class="form-group"><label class="col-md-2 control-label">' + result.product_features[i].name + '</label>';
                    body += '<div class="col-md-8"><select name="product_features[' + result.product_features[i].id + ']" class="form-control select2 features-options" id="feature-option-' + result.product_features[i].id + '"><option value="">Select</option>';

                    for(var j = 0, jlen = result.product_features[i].options.length; j < jlen; j++)
                    {
                        body += '<option value="' + result.product_features[i].options[j].id + '">' + result.product_features[i].options[j].name + '</option>';
                    }

                    body += '</select></div><div class="col-md-2"><button type="button" class="btn btn-warning reset-feature" data-feature-id="' + result.product_features[i].id + '"><i class="fa fa-eraser" aria-hidden="true"></i> Reset</button></div></div>';

                    $('.features').append(body);
                }

                initSelect2();
            });
        }

        function initSelect2()
        {
            $(".features-options").select2({
                placeholder: "Select"
            });
        }

        function getCombinationValues()
        {
            var combination = {};

            var default_combination = $(".default-combination:checked").val();

            var selected_options = [];
            var selected_options_text = [];
            $("#selected-options option").each(function(){
                selected_options.push($(this).val());
                selected_options_text.push($(this).text());
            });

            var combination_supplier_sku = $("#combination-supplier-sku").val();
            var combination_ean = $("#combination-ean").val();
            var combination_upc =  $("#combination-upc").val();
            var combination_sku =  $("#combination-sku").val();
            var combination_minimum_qty =  $("#combination-minimum-qty").val();
            var impact_price = { "type": $("#impact-price").val() };
            var impact_weight = { "type": $("#impact-weight").val() };
            var impact_unit_price = { "type": $("#impact-unit-price").val() };

            if(impact_price.type != "null")
            {
                impact_price.value = $("#impact-price-value").val();
            }

            if(impact_weight.type != "null")
            {
                impact_weight.value = $("#impact-weight-value").val();
            }

            if(impact_unit_price.type != "null")
            {
                impact_unit_price.value = $("#impact-unit-price-value").val();
            }

            var combination_images = [];
            $("input.combination-image:checked").each(function(){
                combination_images.push($(this).val());
            });

            var warehouse_stock = [];
            $(".warehouse-stock").each(function(){
                warehouse = {};
                warehouse.id = $(this).attr('data-warehouse');
                warehouse.val = $(this).val();
                warehouse_stock.push(warehouse);
            });

            combination.default = default_combination;
            combination.options = selected_options;
            combination.options_text = selected_options_text;
            combination.supplier_sku = combination_supplier_sku;
            combination.ean = combination_ean;
            combination.upc = combination_upc;
            combination.sku = combination_sku;
            combination.minimum_qty = combination_minimum_qty;
            combination.impact_price = impact_price;
            combination.impact_weight = impact_weight;
            combination.impact_unit_price = impact_unit_price;
            combination.warehouse_stock = warehouse_stock;
            combination.images = combination_images;

            impact_price_text = '';
            impact_weight_text = '';

            if(impact_price.type != 'null')
            {
                if(impact_price.type == "1")
                {
                    impact_price_text = '<i class="fa fa-plus" aria-hidden="true"></i>';
                }
                else
                {
                    impact_price_text = '<i class="fa fa-minus" aria-hidden="true"></i>';
                }
                
                impact_price_text += ' € ' + impact_price.value;
            }

            if(impact_weight.type != 'null')
            {
                if(impact_weight.type == "1")
                {
                    impact_weight_text = '<i class="fa fa-plus" aria-hidden="true"></i> ';
                }
                else
                {
                    impact_weight_text = '<i class="fa fa-minus" aria-hidden="true"></i> ';
                }
                
                impact_weight_text += impact_weight.value + ' KG';
            }

            var random_string = randomString(10);

            var table_row = '<tr id="' + random_string + '">';
            table_row += '<td>' + selected_options_text + '</td>';
            table_row += '<td>' + impact_price_text + '</td>';
            table_row += '<td>' + impact_weight_text + '</td>';
            table_row += '<td>' + combination_supplier_sku + '</td>';
            table_row += '<td class="text-center"><div class="btn-group">';
            table_row += '<button type="button" class="btn-white btn btn-sm edit-combination" data-edit-combination="' + random_string + '" data-toggle="modal" data-target="#addCombination"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>';
            table_row += '<button class="btn-white btn btn-sm remove-combination" data-remove-combination="' + random_string + '" type="button"><i class="fa fa-times" aria-hidden="true"></i></button></div></td></tr>';
            table_row += '<input name="combinations[]" data-combination="' + random_string + '" type="hidden" value=\'' + JSON.stringify(combination) + '\' />';

            return table_row;
        }

        function emptyCombinationModalForm()
        {
            $('input[name="default-combination"][value="0"]').iCheck('check');
            $('.combination-attribute').select2('val', null);
            $('.combination-attribute-options').empty().select2('val', null);
            $('.selected-options').html("");
            $('#combination-supplier-sku, #combination-ean, #combination-upc, #combination-sku, #combination-minimum-qty, #impact-price-value, #impact-weight-value, #impact-unit-price-value, .warehouse-stock').val("");
            $('#impact-price').select2('val', null);
            $('#impact-weight').select2('val', null);
            $('#impact-unit-price').select2('val', null);
            $('.combination-image').iCheck('uncheck');
        }
    </script>
@stop

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>{{ trans('pages.inquiry_detail_header') }}</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('home') }}">{{ trans('menu.home') }}</a>
                </li>
                <li>
                    <a href="">{{ trans('menu.inquiries') }}</a>
                </li>
                <li class="active">
                    <strong>{{ trans('page.inquiry') }}</strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">
        @if (count($errors) > 0)
            <div class="alert alert-danger alert-dismissable">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <form id="profileForm" method="post" class="form-horizontal">
            <div class="mb20"></div>

            <div class="row">
                <div class="col-md-8">
                    <div class="tabs-container">
                        <div class="tabs-left">
                            <ul class="nav nav-tabs">
                                <li class="active"><a data-toggle="tab" href="#tab-general"><i class="fa fa-certificate" aria-hidden="true"></i> <span class="hidden-xs">General</span></a></li>
                                <li class=""><a data-toggle="tab" href="#tab-information"><i class="fa fa-info-circle" aria-hidden="true"></i> <span class="hidden-xs">Information</span></a></li>
                                <li class=""><a data-toggle="tab" href="#tab-images"><i class="fa fa-file-image-o" aria-hidden="true"></i> <span class="hidden-xs">Images</span></a></li>
                                <li class=""><a data-toggle="tab" href="#tab-features"><i class="fa fa-check-square-o" aria-hidden="true"></i> <span class="hidden-xs">Features</span></a></li>
                                <li class=""><a data-toggle="tab" href="#tab-prices"><i class="fa fa-usd" aria-hidden="true"></i> <span class="hidden-xs">Prices</span></a></li>
                                <li class=""><a data-toggle="tab" href="#tab-associations"><i class="fa fa-sitemap" aria-hidden="true"></i> <span class="hidden-xs">Associations</span></a></li>
                                <li class=""><a data-toggle="tab" href="#tab-combinations"><i class="fa fa-share-alt" aria-hidden="true"></i> <span class="hidden-xs">Combinations</span></a></li>
                                <li class=""><a data-toggle="tab" href="#tab-quantities"><i class="fa fa-cubes" aria-hidden="true"></i> <span class="hidden-xs">Quantities</span></a></li>
                                <li class=""><a data-toggle="tab" href="#tab-shipping"><i class="fa fa-truck" aria-hidden="true"></i> <span class="hidden-xs">Shipping</span></a></li>
                            </ul>
                            <div class="tab-content ">
                                <div id="tab-general" class="tab-pane active">
                                    <div class="panel-body">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="col-md-2 control-label">Active</label>
                                                <div class="col-md-5">
                                                    <label><input type="radio" class="form-control i-checks" name="active" value="1" checked> Enabled</label>
                                                </div>
                                                <div class="col-md-5">
                                                    <label><input type="radio" class="form-control i-checks" name="active" value="0"> Disabled</label>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-2 control-label">Supplier SKU</label>
                                                <div class="col-md-3"><input type="text" class="form-control" name="supplier_sku" placeholder="Supplier SKU"></div>
                                                <div class="col-md-4"><input type="text" class="form-control" name="ean" placeholder="EAN-13 or JAN barcode"></div>
                                                <div class="col-md-3"><input type="text" class="form-control" name="upc" placeholder="UPC barcode"></div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-2 control-label">Own SKU</label>
                                                <div class="col-md-10"><input type="text" class="form-control" name="sku" placeholder="Own SKU"></div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-2 control-label">Minimum QTY</label>
                                                <div class="col-md-10"><input type="text" class="qty-input" name="minimum_qty" placeholder="Minimum QTY"></div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-2 control-label">Brand</label>
                                                <div class="col-md-10">
                                                    @if($brands->count() > 0)
                                                        <select class="select2 form-control" name="brand">
                                                            <option value="">Select</option>
                                                            @foreach($brands as $brand)
                                                                <option value="{{ $brand->id }}">{{ $brand->name }}</option>
                                                            @endforeach
                                                        </select>
                                                    @else
                                                        <p class="form-control-static">
                                                            <a href="{{ route('new_brand') }}">Create first brand</a>
                                                        </p>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-2 control-label">Tax</label>
                                                @if($tax_groups->count() > 0)
                                                    <div class="col-md-5">
                                                        <select class="select2 tax_groups form-control" name="tax_group">
                                                            <option value="">Select</option>
                                                            @foreach($tax_groups as $tax_group)
                                                                <option value="{{ $tax_group->id }}">{{ $tax_group->name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="col-md-5">
                                                        <select class="select2 tax_rules form-control" name="tax_rule">
                                                            <option value="">Select</option>
                                                        </select>
                                                    </div>
                                                @else
                                                    <div class="col-md-10">
                                                        <p class="form-control-static">
                                                            <a href="{{ route('new_tax_group') }}">Create first tax group</a>
                                                        </p>
                                                    </div>
                                                @endif
                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <div id="tab-information" class="tab-pane">
                                    <div class="panel-body">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="col-md-2 control-label">Language</label>
                                                <div class="col-md-10">
                                                    <select id="multi-language" class="form-control countries-select">
                                                        <option value="all" icon="United-Nations" selected>All</option>

                                                        @foreach($languages as $language)
                                                            <option value="{{ $language->id }}" icon="{{ $language->icon }}">{{ $language->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="multi-language-info-box multi-language-all">
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">Product Name</label>
                                                    <div class="col-md-10"><input type="text" class="form-control all-language-input product-name-input" data-input="product-name-input" placeholder="Product Name" value="{{ old('product_info.all.product_name') }}"></div>
                                                </div>                                 

                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">Meta Description</label>
                                                    <div class="col-md-10"><input type="text" class="form-control all-language-input product-meta-description-input" name="product_info[all][meta_description]" data-input="product-meta-description-input" placeholder="Describe short" value="{{ old('product_info.all.meta_description') }}"></div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">Meta Link</label>
                                                    <div class="col-md-10"><input type="text" class="form-control all-language-input product-meta-link-input" data-input="product-meta-link-input" name="product_info[all][meta_link]" placeholder="Custom Link" value="{{ old('product_info.all.meta_link') }}"></div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">Tags</label>
                                                    <div class="col-md-10"><input class="all-language-tagsinput form-control" type="text" placeholder="Product Tags" value=""/></div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">Description</label>
                                                    <div class="col-md-10">
                                                        <textarea class="all-language-summernote"></textarea>
                                              
                                                    </div>
                                                </div>
                                            </div>

                                            @foreach($languages as $language)
                                                <div class="multi-language-info-box multi-language-{{ $language->id}}">
                                                    <div class="form-group">
                                                        <label class="col-md-2 control-label">Product Name</label>
                                                        <div class="col-md-10"><input type="text" class="form-control product-name-input" name="product_info[{{ $language->id }}][product_name]" placeholder="Product Name" value="{{ old('product_info.' . $language->id . '.product_name') }}"></div>
                                                    </div>                                 

                                                    <div class="form-group">
                                                        <label class="col-md-2 control-label">Meta Description</label>
                                                        <div class="col-md-10"><input type="text" class="form-control product-meta-description-input" name="product_info[{{ $language->id }}][meta_description]" placeholder="Describe short" value="{{ old('product_info.' . $language->id . '.meta_description') }}"></div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-md-2 control-label">Meta Link</label>
                                                        <div class="col-md-10"><input type="text" class="form-control product-meta-link-input" name="product_info[{{ $language->id }}][meta_link]" placeholder="Custom Link" value="{{ old('product_info.' . $language->id . '.meta_link') }}"></div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-md-2 control-label">Tags</label>
                                                        <div class="col-md-10"><input class="tagsinput form-control" type="text" placeholder="Product Tags" name="product_info[{{ $language->id }}][tags]" /></div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-md-2 control-label">Description</label>
                                                        <div class="col-md-10">
                                                            <textarea class="summernote" name="product_info[{{ $language->id }}][description]">
                                                                {{ old('product_info.2.description') }}
                                                            </textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>

                                <div id="tab-images" class="tab-pane">
                                    <div class="panel-body">
                                        <div class="alert alert-warning">You must save this product before adding images.</div>
                                    </div>
                                </div>

                                <div id="tab-features" class="tab-pane">
                                    <div class="panel-body">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="col-md-2 control-label">Feature Types</label>
                                                <div class="col-md-10">
                                                    <select class="form-control select2 feature-type" name="feature_type">
                                                        <option value="null">No Feature</option>
                                                        @foreach($product_feature_types as $feature_type)
                                                            <option value="{{ $feature_type->id }}">{{ $feature_type->info->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <hr>

                                            <div class="features"></div>
                                        </div>
                                    </div>
                                </div>

                                <div id="tab-prices" class="tab-pane">
                                    <div class="panel-body">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="col-md-2 control-label">Purchase Price</label>
                                                <div class="col-md-10">
                                                    <input type="text" class="price-input" name="purchasing_price" placeholder="Purchasing Price" value="{{ old('purchasing_price') }}">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-2 control-label">Price</label>
                                                <div class="col-md-5">
                                                    <input type="text" class="price-input" name="retail_price" placeholder="Retail Price" value="{{ old('retail_price') }}">
                                                </div>
                                                <div class="col-md-5">
                                                    <input type="text" class="price-input" name="list_price" placeholder="List Price" value="{{ old('list_price') }}">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-2 control-label">Unit Price</label>
                                                <div class="col-md-4">
                                                    <input type="text" class="qty-input" name="unit_qty" placeholder="Unit Price" value="{{ old('retail_price') }}">
                                                </div>
                                                <div class="col-md-2 text-center">
                                                    per
                                                </div>
                                                <div class="col-md-4">
                                                    <input type="text" class="price-input" name="unit_price" placeholder="Gram, Bottle, Pound" value="{{ old('list_price') }}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div id="tab-associations" class="tab-pane">
                                    <div class="panel-body">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="col-md-2 control-label">Shops</label>

                                                <div class="col-md-10">
                                                    @foreach($shops as $shop)
                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                <label>
                                                                    <input class="i-checks" type="checkbox" name="shops[]" value="{{ $shop->id }}" /> {{ $shop->name }}
                                                                </label>
                                                            </div>

                                                            <div class="col-md-9">
                                                                @if($shop->categories->count() > 0)
                                                                    <select class="select2 form-control" multiple="multiple" name="category[{{ $shop->id }}][]">
                                                                        @foreach($shop->categories as $category)
                                                                            <option value="{{ $category->id }}">{{ $category->multilanguage->info()->name }}</option>
                                                                        @endforeach
                                                                    </select>

                                                                @else
                                                                    <p class="form-control-static">
                                                                        <a href="{{ route('new_category', ["shop_id" => $shop->id]) }}">Create first category</a>
                                                                    </p>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="mb15"></div>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div id="tab-combinations" class="tab-pane">
                                    <div class="panel-body">
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-striped">
                                                <thead>
                                                <tr>
                                                    <th>
                                                        Attribute - Value Pair 
                                                    </th>
                                                    <th>
                                                        Impact on price
                                                    </th>
                                                    <th>
                                                        Impact on weight 
                                                    </th>
                                                    <th>
                                                        Reference
                                                    </th>
                                                    <th>
                                                        Action
                                                    </th>
                                                </tr>
                                                </thead>
                                                <tbody class="combinations-table">
                                                </tbody>
                                            </table>
                                        </div>

                                        <button type="button" class="btn btn-primary pull-right add-combination-modal" data-toggle="modal" data-target="#addCombination">
                                            <i class="fa fa-plus-circle" aria-hidden="true"></i> Add Combination
                                        </button>

                                        <div class="modal inmodal fade" id="addCombination" role="dialog"  aria-hidden="true">
                                            <div class="modal-dialog modal-lg">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                        <h4 class="modal-title">Add Combination</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="form-horizontal">
                                                            <div class="form-group">
                                                                <label class="col-md-2 control-label">Default</label>
                                                                <div class="col-md-5">
                                                                    <label><input type="radio" name="default-combination" class="form-control i-checks default-combination" value="1"> Yes</label>
                                                                </div>
                                                                <div class="col-md-5">
                                                                    <label><input type="radio" name="default-combination" class="form-control i-checks default-combination" value="0" checked> No</label>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="col-md-2 control-label">Attribute</label>
                                                                <div class="col-md-5">
                                                                    <select class="form-control select2 combination-attribute">
                                                                        <option selected="selected" value="">Choose an Attribute</option>
                                                                        @foreach($attributes as $attribute)
                                                                            <option value="{{ $attribute->id }}">{{ $attribute->multilanguage->info()->name }}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                                <div class="col-md-5">
                                                                    <select class="form-control select2 combination-attribute-options">
                                                                        <option selected="selected" value="">Choose an Option</option>
                                                                    </select>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <div class="col-md-5 col-md-offset-2">
                                                                    <button class="btn btn-block btn-danger remove-combination-attribute" type="button"><i aria-hidden="true" class="fa fa-minus-circle"></i> Remove</button>
                                                                </div>
                                                                <div class="col-md-5">
                                                                    <button class="btn btn-block btn-primary add-combination" type="button"><i aria-hidden="true" class="fa fa-plus-circle"></i> Add</button>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <div class="col-md-10 col-md-offset-2">
                                                                    <select id="selected-options" class="selected-options form-control" multiple=""></select>
                                                                </div>
                                                            </div>

                                                            <hr>

                                                            <div class="form-group">
                                                                <label class="col-md-2 control-label">Supplier SKU</label>
                                                                <div class="col-md-3"><input id="combination-supplier-sku" type="text" class="form-control" placeholder="Supplier SKU"></div>
                                                                <div class="col-md-4"><input id="combination-ean" type="text" class="form-control" placeholder="EAN-13 or JAN barcode"></div>
                                                                <div class="col-md-3"><input id="combination-upc" type="text" class="form-control" placeholder="UPC barcode"></div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="col-md-2 control-label">Own SKU</label>
                                                                <div class="col-md-10"><input id="combination-sku" type="text" class="form-control" placeholder="Own SKU"></div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="col-md-2 control-label">Minimum QTY</label>
                                                                <div class="col-md-10"><input id="combination-minimum-qty" type="text" class="form-control qty-input" placeholder="Minimum QTY"></div>
                                                            </div>

                                                            <hr>

                                                            @if($warehouses->count() > 0)
                                                                @foreach($warehouses as $warehouse)
                                                                    <div class="form-group">
                                                                        <label class="col-md-2 control-label">{{ $warehouse->name }}</label>
                                                                        <div class="col-md-10">
                                                                            <input type="text" class="form-control qty-input warehouse-stock" data-warehouse="{{ $warehouse->id }}" name="stock[{{ $warehouse->id }}]" placeholder="Stock">
                                                                        </div>
                                                                    </div>
                                                                @endforeach
                                                            @else
                                                                <div class="form-group">
                                                                    <label class="col-md-2 control-label">Stock</label>
                                                                    <div class="col-md-10">
                                                                        <p class="form-control-static">
                                                                            <a href="{{ route('new_warehouse') }}">Create first warehouse</a>
                                                                        </p>
                                                                    </div>
                                                                </div>
                                                            @endif

                                                            <hr>

                                                            <div class="form-group">
                                                                <label class="col-md-2 control-label">Impact on price</label>
                                                                <div class="col-md-5">
                                                                    <select class="select2" id="impact-price">
                                                                        <option value="null">None</option>
                                                                        <option value="1">Increase</option>
                                                                        <option value="0">Reduce</option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-md-5">
                                                                    <input id="impact-price-value" class="price-input" type="text" placeholder="Impact on price">
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="col-md-2 control-label">Impact on weight</label>
                                                                <div class="col-md-5">
                                                                    <select class="select2" id="impact-weight">
                                                                        <option value="null">None</option>
                                                                        <option value="1">Increase</option>
                                                                        <option value="0">Reduce</option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-md-5">
                                                                    <input id="impact-weight-value" class="weight-input" type="text" placeholder="Impact on weight">
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="col-md-2 control-label">Impact on unit price</label>
                                                                <div class="col-md-5">
                                                                    <select class="select2" id="impact-unit-price">
                                                                        <option value="null">None</option>
                                                                        <option value="1">Increase</option>
                                                                        <option value="0">Reduce</option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-md-5">
                                                                    <input type="text" id="impact-unit-price-value" placeholder="Impact on unit price">
                                                                </div>
                                                            </div>

                                                            <hr>

                                                            <div class="form-group">
                                                                <label class="col-md-2 control-label">Images</label>
                                                                <div class="col-md-10">
                                                                    <div class="combination_images">
                                                                        <div>
                                                                            <label class="text-center">
                                                                                <img class="img-lg" src="http://www.rd.com/wp-content/uploads/sites/2/2016/04/01-cat-wants-to-tell-you-laptop.jpg"><br />
                                                                                <div class="mb10"></div>
                                                                                <input type="checkbox" class="form-control i-checks combination-image" value="1">
                                                                            </label>
                                                                        </div>
                                                                        <div>
                                                                            <label class="text-center">
                                                                                <img class="img-lg" src="http://www.rd.com/wp-content/uploads/sites/2/2016/04/01-cat-wants-to-tell-you-laptop.jpg"><br />
                                                                                <div class="mb10"></div>
                                                                                <input type="checkbox" class="form-control i-checks combination-image" value="2">
                                                                            </label>
                                                                        </div>
                                                                        <div>
                                                                            <label class="text-center">
                                                                                <img class="img-lg" src="http://www.rd.com/wp-content/uploads/sites/2/2016/04/01-cat-wants-to-tell-you-laptop.jpg"><br />
                                                                                <div class="mb10"></div>
                                                                                <input type="checkbox" class="form-control i-checks combination-image" value="3">
                                                                            </label>
                                                                        </div>
                                                                        <div>
                                                                            <label class="text-center">
                                                                                <img class="img-lg" src="http://www.rd.com/wp-content/uploads/sites/2/2016/04/01-cat-wants-to-tell-you-laptop.jpg"><br />
                                                                                <div class="mb10"></div>
                                                                                <input type="checkbox" class="form-control i-checks combination-image" value="4">
                                                                            </label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>

                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                                                        <button type="button" class="btn btn-primary add" id="save-combination" data-dismiss="modal">Add</button>
                                                        <button type="button" class="btn btn-primary edit" id="save-edit-combination" data-dismiss="modal">Edit</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div id="tab-quantities" class="tab-pane">
                                    <div class="panel-body">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="col-md-2 control-label">Stock management</label>
                                                <div class="col-md-5">
                                                    <label><input type="radio" class="form-control i-checks" name="stock_management" value="1" checked> Yes</label>
                                                </div>
                                                <div class="col-md-5">
                                                    <label><input type="radio" class="form-control i-checks" name="stock_management" value="0"> No</label>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-2 control-label">When out of stock</label>
                                                <div class="col-md-5">
                                                    <label><input type="radio" class="form-control i-checks" name="deny_order" value="1" checked> Deny orders</label>
                                                </div>
                                                <div class="col-md-5">
                                                    <label><input type="radio" class="form-control i-checks" name="deny_order" value="0"> Allow orders</label>
                                                </div>
                                            </div>

                                            @if($warehouses->count() > 0)
                                                @foreach($warehouses as $warehouse)
                                                    <div class="form-group">
                                                        <label class="col-md-2 control-label">{{ $warehouse->name }}</label>
                                                        <div class="col-md-10">
                                                            <input type="text" class="form-control qty-input" name="stock[{{ $warehouse->id }}]" placeholder="Stock">
                                                        </div>
                                                    </div>
                                                @endforeach
                                            @else
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">Warehouse</label>
                                                    <div class="col-md-10">
                                                        <p class="form-control-static">
                                                            <a href="{{ route('new_warehouse') }}">Create first warehouse</a>
                                                        </p>
                                                    </div>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div id="tab-shipping" class="tab-pane">
                                    <div class="panel-body">
                                        <div class="col-md-12">

                                            <div class="form-group">
                                                <label class="col-md-2 control-label">Width</label>
                                                <div class="col-md-10">
                                                    <input type="text" class="size-input" name="package_width" placeholder="Package width" value="{{ old('retail_price') }}">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-2 control-label">Height</label>
                                                <div class="col-md-10">
                                                    <input type="text" class="size-input" name="package_height" placeholder="Package height" value="{{ old('retail_price') }}">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-2 control-label">Depth</label>
                                                <div class="col-md-10">
                                                    <input type="text" class="size-input" name="package_depth" placeholder="Package depth" value="{{ old('retail_price') }}">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-2 control-label">Weight</label>
                                                <div class="col-md-10">
                                                    <input type="text" class="form-control weight-input" name="package_weight" placeholder="Package weight" value="{{ old('retail_price') }}">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-2 control-label">Additional Fee</label>
                                                <div class="col-md-10">
                                                    <input type="text" class="form-control price-input" name="additional_fee_per_item" placeholder="Additional fee per item" value="{{ old('retail_price') }}">
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="ibox">
                        <div class="ibox-title">
                            <h5>Action</h5>
                        </div>

                        <div class="ibox-content">

                            {{ csrf_field() }}
                            <button class="btn btn-block btn-outline btn-success" type="submit"><i class="fa fa-floppy-o"></i> Save</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>

        <div class="mb30"></div>
    </div>
@stop