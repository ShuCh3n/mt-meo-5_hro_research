@extends('dashboard.layout.main')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-6">
            <h2>{{ trans('pages.inquiry_detail_header') }}</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('home') }}">{{ trans('menu.home') }}</a>
                </li>
                <li>
                    <a href="">{{ trans('menu.inquiries') }}</a>
                </li>
                <li class="active">
                    <strong>{{ trans('page.inquiry') }}</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-6">
            <a class="btn btn-outline btn-primary pull-right mt30" href="{{ route('new_reward_program') }}"><i class="fa fa-plus-circle" aria-hidden="true"></i> New program</a>
        </div>
    </div>

	<div class="wrapper wrapper-content animated fadeInRight ecommerce">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-content">

                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>Order</th>
                                <th>Company</th>
                                <th>IBAN</th>
                                <th>Total</th>
                                <th>Payout</th>
                                <th>Date</th>
                                <th class="text-right">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($reward_program->orders as $program_order)
                                    <tr>
                                        <td>
                                            <a href="{{ route('view_order', $program_order->order->id) }}">{{ $program_order->order->id }}</a>
                                        </td>
                                        <td>
                                            {{ $program_order->company }}
                                        </td>
                                        <td>
                                            {{ $program_order->iban }}
                                        </td>
                                        <td>
                                            {{ price($program_order->total_price) }}
                                        </td>
                                        <td>
                                            {!! ($program_order->payout)? '<i class="fa fa-check text-navy" aria-hidden="true"></i>' : '<i class="fa fa-times text-danger" aria-hidden="true"></i>' !!}
                                        </td>
                                        <td>
                                            @if(carbon()->now()->diffInDays($program_order->created_at) > 0)
                                                <b>{{ $program_order->created_at->format('H:i') }}</b>
                                                {{ $program_order->created_at->format('d M Y') }}
                                            @else
                                                {{ str_replace('before', 'ago', $program_order->created_at->diffForHumans(carbon())) }}
                                            @endif
                                        </td>
                                        <td class="text-right">
                                            <a class="btn-white btn btn-xs" href="{{ route('view_program_order', [$reward_program->id, $program_order->id]) }}">View</a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop