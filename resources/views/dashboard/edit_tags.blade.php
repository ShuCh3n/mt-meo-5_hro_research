@extends('dashboard.layout.main')

@section('content')
    <div class="ibox-content m-b-sm border-bottom">
        <div class="row">
            <div class="col-xs-12">
                <a class="btn btn-primary pull-right" href="{{ route('new_blog_tag', $blog->id) }}"><i class="fa fa-plus-circle" aria-hidden="true"></i> New tag</a>
            </div>
        </div>
    </div>

    <div class="ibox">
        <div class="ibox-content">
            <table class="table table-hover">
                <thead>
                <tr>
                    <th>
                        Name
                    </th>
                    <th class="text-right">
                        Action
                    </th>
                </tr>
                </thead>
                <tbody>
                @foreach($tags as $tag)
                    <tr>
                        <td>
                            <a href="{{route('edit_blog_tag', $tag->id)}}" class="btn-white btn">{{$tag->tagContentLang(2)->first() ? $tag->tagContentLang(2)->first()->name: ''}}</a>
                        </td>
                        <td class="text-right">
                            <div class="btn-group">
                                <a href="{{ route('edit_blog_tag', $tag->id)}}" class="btn-white btn btn-xs"><i class="fa fa-2x fa-pencil"></i></a>
                                <a href="{{ route('delete_blog_tag', $tag->id) }}" class="btn-white btn btn-xs"><i class="fa fa-2x fa-trash-o"></i></a>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@stop