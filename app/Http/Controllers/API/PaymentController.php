<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Classes\Orders\OrderFactory;
use App\Order;
use App\OrderStatus;
use App\Currency;
use App\Country;
use App\OrderTransaction;

class PaymentController extends Controller
{
    public function payments()
	{
		$this->request->shop->payments();
		return $this->request->shop->payments;
	}

	public function pay()
	{
		$order = new Order;
		$order->shop_id = $this->request->shop->id;
		$order->payment_id = $this->request->payment_method;
		$order->order_status_id = OrderStatus::status('awaiting_payment')->first()->id;
		$order->customer_id = $this->request->customer_session->customer_id;
		$order->shipping_id = null;
		$order->currency_id = Currency::name('euro')->first()->id;
		$order->reference = strtoupper(str_random(8));
		$order->company = $this->request->company;
		$order->street = $this->request->street;
		$order->housenumber = $this->request->housenumber;
		$order->zipcode = $this->request->zipcode;
		$order->city = $this->request->city;
		$order->country_id = Country::getCountry($this->request->country)->id;
		$order->dropship = $this->request->dropship;
		$order->shipping_price = 0;
		$order->total_price = 0;
		$order->ip = $this->request->ip;
		$order->save();

		foreach($this->request->cart['products'] as $product)
		{
			$order->addLine($product['product']['id'], $product['qty']);
		}

		$order->calculateShopTax($this->request->shop);

		OrderFactory::create($this->request, $order);

		if($this->request->provider == "paynl")
		{
			$transaction = array(
				'amount' => $order->total_price,
				'returnUrl' => $this->request->return_url,
				'paymentMethod' => $this->request->payment_method,
			);

			if($this->request->payment_method == 10)
			{
				$transaction['bank'] = $this->request->bank;
			}

			$paynl_transaction = $this->request->shop->pay($this->request->provider, $transaction);

			$order_transaction = new OrderTransaction;
			$order_transaction->order_id = $order->id;
			$order_transaction->payment = $this->request->provider;
			$order_transaction->transaction_reference = $paynl_transaction->getData()["transaction"]["transactionId"];
			$order_transaction->save();

			return array(
				"order"			=> $order,
				"redirect_url" => $paynl_transaction->getRedirectUrl()
			);
		}
	}

	public function paymentResponse()
	{
		$order = OrderTransaction::where('payment', $this->request->provider)->where('transaction_reference', $this->request->order_id)->firstOrFail()->order->load('lines');

        if($this->request->shop->confirmPayment($this->request))
        {
            $order->updateStatus('payment_accepted');

            return array(
                "success" 		=> $this->request->shop->confirmPayment($this->request),
                "order" 		=> $order
            );
        }

		return array(
            "success" 		=> false,
            "order" 		=> $order
        );
	}


	private function calculateTotalPrice()
	{
		$cart_price = $this->cart()->total_price;
		$additional_price = $this->request->additional_price;

		return $cart_price + $additional_price;
	}
}
