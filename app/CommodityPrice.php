<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CommodityPrice extends Model
{
    protected $fillable = ['commodity_id', 'price', 'created_at'];

    public function scopeRange($query, $from, $until)
    {
        return $query->where('created_at', '>=', $from)->where('created_at', '<=', $until);
    }
}
