@extends('dashboard.layout.main')

@section('stylesheet')
    <link href="/css/plugins/select2/select2.min.css" rel="stylesheet">
@stop

@section('content')
	<div class="wrapper wrapper-content animated fadeInRight ecommerce">
        <div class="ibox-content m-b-sm border-bottom">
            <div class="row">
                <div class="col-xs-12">
                    <a class="btn btn-primary pull-right" href="{{ route('new_shipping') }}"><i class="fa fa-plus-circle" aria-hidden="true"></i> New shipping</a>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-content">

                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>Shipping Methods</th>
                                <th class="text-right">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($shippings as $shipping)
                                    <tr>
                                        <td><img src=""></td>
                                        <td>
                                           {{ $shipping->id }} 
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop