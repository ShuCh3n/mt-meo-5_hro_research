<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Image extends Model
{
	use SoftDeletes;
	use SubFilter;

	protected $fillable = ['filename' , 'imagable_type' , 'imagable_id'];
	protected $hidden = ['created_at', 'updated_at', 'deleted_at'];

	public function imagable()
	{
		return $this->morphTo();
	}

	public function imageType()
	{
		return $this->belongsTo(ImageType::class);
	}
}