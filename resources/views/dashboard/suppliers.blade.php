@extends('dashboard.layout.main')

@section('stylesheet')
    <link href="/css/plugins/select2/select2.min.css" rel="stylesheet">
@stop

@section('javascript')
    <!-- Select2 -->
    <script src="/js/plugins/select2/select2.full.min.js"></script>

    <script>
        $(function(){
            $(".shop_list").select2();
        });
    </script>
@stop

@section('content')
	<div class="wrapper wrapper-content animated fadeInRight ecommerce">
        <div class="ibox-content m-b-sm border-bottom">
            <div class="row">
                <div class="col-xs-12">
                    <a class="btn btn-primary pull-right" href="{{ route('new_supplier') }}"><i class="fa fa-plus-circle" aria-hidden="true"></i> New Supplier</a>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-content">

                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th data-toggle="true">Supplier</th>
                                <th data-hide="phone">Products</th>
                                <th class="text-right" data-sort-ignore="true">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($suppliers as $supplier)
                                    <tr>
                                        <td>
                                           {{ $supplier->name }}
                                        </td>
                                        <td>
                                          
                                        </td>
                                        <td class="text-right">
                                            <a class="btn-white btn btn-xs">Edit</a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop