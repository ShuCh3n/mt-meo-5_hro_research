<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerPasswordReset extends Model
{
	public function scopeActive($query)
	{
		$query->where('active', true);
	}

    public function deactivate()
    {
    	$this->active = false;
    	$this->save();
    }
}
