@extends('dashboard.layout.main')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>{{ trans('pages.inquiry_detail_header') }}</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('home') }}">{{ trans('menu.home') }}</a>
                </li>
                <li>
                    <a href="">{{ trans('menu.inquiries') }}</a>
                </li>
                <li class="active">
                    <strong>{{ trans('page.inquiry') }}</strong>
                </li>
            </ol>
        </div>
    </div>

	<div class="wrapper wrapper-content animated fadeInRight ecommerce">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-content">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th data-toggle="true">Cart</th>
                                    <th>Customer</th>
                                    <th>Shop</th>
                                    <th>Cart Value</th>
                                    <th>Checked Out</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($carts as $cart)
                                    <tr>
                                        <td>
                                           {{ $cart->id }}
                                        </td>
                                        <td>
                                            @if($cart->customer_session->customer)
                                                <a href="{{ route('view_customer', $cart->customer_session->customer->id) }}">{{ $cart->customer_session->customer->firstname . ' ' . $cart->customer_session->customer->lastname }}</a>
                                            @else
                                                -
                                            @endif
                                        </td>
                                        <td>
                                           {{ $cart->shop->name }}
                                        </td>
                                        <td>
                                           {{ price($cart->total_price) }}
                                        </td>
                                        <td>
                                            @if($cart->customer_session->checked_out)
                                                <i class="fa fa-check text-navy" aria-hidden="true"></i>
                                            @else
                                                <i class="fa fa-times text-danger" aria-hidden="true"></i>
                                            @endif
                                        </td>
                                        <td class="text-right">
                                            <div class="btn-group">
                                                <a href="{{ route('view_cart', $cart->id) }}" class="btn-white btn btn-xs">View</a>
                                                <a href="#" class="btn-white btn btn-xs">Remove</a>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>

                        <div class="text-center">
                            {{ $carts->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop