<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\ProductInfo;
use App\ProductGroup;
use App\ProductFilterGroup;
use Illuminate\Support\Collection;
use App\Platform;

class Product extends Model
{
    use SubFilter;

    protected $hidden = ['pivot', 'created_at', 'updated_at', 'deleted_at'];
    protected $fillable = [
            'brand_id',
            'product_group_id',
            'product_feature_type_id',
            'brand_id',
            'active',
            'supplier_sku',
            'ean',
            'upc',
            'sku',
            'minimum_qty',
            'stock_management',
            'deny_order_when_out_of_stock',
            'width',
            'height',
            'depth',
            'weight',
            'additional_fee_per_item'
    ];

    public function product_group()
    {
        return $this->belongsTo(ProductGroup::class);
    }

    public function info()
    {
        if(auth()->user())
        {
            return $this->hasOne(ProductInfo::class)->where('language_id', auth()->user()->preferenceLanguage()->id);
        }

        return $this->hasOne(ProductInfo::class);
    }

    public function infos()
    {
        return $this->hasMany(ProductInfo::class);
    }

    public function shop_infos()
    {
        return $this->hasMany(ProductShopInfo::class);
    }

    public function shop_info()
    {
        return $this->hasOne(ProductShopInfo::class);
    }

    //     public function images()
    //     {
    //     	return $this->hasMany(ProductImage::class)->orderBy('rank', 'asc');
    //     }

    public function images()
    {
        return $this->morphMany(Image::class, 'imagable');
    }

    public function group(){
        return $this->belongsTo(ProductGroup::class, 'product_group_id');
    }

    public function groups(){
     	return $this->belongsToMany(Group::class)
                ->withPivot('product_id', 'group_id');
    }

    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }

    public function product_prices()
    {
        return $this->hasMany(ProductPrice::class);
    }

    public function product_price()
    {
        return $this->hasOne(ProductPrice::class)->latest();
    }
    
    public function platforms()
    {
        return $this->belongsToMany(Platform::class);
    }

    public function platform_product_prices()
    {
        return $this->hasMany(PlatformProductPrice::class);
    }

    public function platform_product_price()
    {
        return $this->hasOne(PlatformProductPrice::class);
    }

    public function motors()
    {
        return $this->belongsToMany(Motor::class, 'category_motor_product_vehicle_year');
    }

    public function motorProduct(){
        return $this->hasMany(CategoryMotorProductVehicleYear::class);
    }

    public function scopeBrand($query, $brand)
    {
        return $query->whereHas('brand', function($query) use ($brand){
            $query->where('name', 'like', '%' . $brand . '%');
        });
    }

    public function product_tags()
    {
        return $this->belongsToMany(ProductTag::class);
    }

    public function shops()
    {
        return $this->hasMany(ShopProduct::class);
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }

    public function supplier()
    {
        return $this->belongsTo(Supplier::class);
    }

    public function tax_rules()
    {
        return $this->belongsToMany(TaxRule::class);
    }

    public function product_feature_options()
    {
        return $this->belongsToMany(ProductFeatureOption::class);
    }

    public function product_stocks()
    {
        return $this->hasMany(ProductStock::class);
    }

    public function product_stock()
    {
        return $this->hasOne(ProductStock::class);
    }

    public function total_stocks()
    {
        return $this->product_stocks->sum('amount');
    }

    public function tti_stocks()
    {
        return $this->hasOne(ProductStock::class)->latest()->where('warehouse_id', 1);
    }

    public function third_party_stocks()
    {
        return $this->product_stock()->whereHas('warehouse', function($q){
            $q->where('third_party', true);
        });
    }

    public function product_combinations()
    {
        return $this->hasMany(ProductCombination::class);
    }

    public function product_feature_type()
    {
        return $this->belongsTo(ProductFeatureType::class);
    }

    public function shop_prices()
    {
        return $this->hasMany(ProductShopPrice::class)->latest();
    }

    public function shop_price()
    {
        return $this->hasOne(ProductShopPrice::class)->latest();
    }

    public function shop_shop_price($shop_id)
    {
        return $this->hasOne(ProductShopPrice::class)->where('shop_id', $shop_id)->latest();
    }

    public function order_lines()
    {
        return $this->hasMany(OrderLine::class);
    }

    public function catalog_price_rules()
    {
        return $this->belongsToMany(CatalogPriceRule::class);
    }

    public function shop_products()
    {
        return $this->hasMany(ShopProduct::class);
    }

    public function shop_product()
    {
        return $this->hasOne(ShopProduct::class);
    }

    public function vehicle_years()
    {
        return $this->belongsToMany(VehicleYear::class, 'category_motor_product_vehicle_year');
    }

    public function av_price()
    {
        return $this->product_price->retail_price > 0 ? $this->product_price->retail_price : $this->third_party_stock_price();
    }

    public function third_party_stock_price()
    {
        if($this->third_party_stocks)
        {
            return $this->third_party_stocks->price;
        }

        return null;
    }

    public function scopeFilterMotorVehicleBrand($query, $brands)
    {
        return $query->whereHas('motors.vehicle_brand', function($query) use ($brands){
            $query->whereIn('name', $brands);
        });
    }

    public function scopeFilterMotorVehicleYear($query, $years)
    {
        return $query->whereHas('motors.vehicle_years', function($query) use ($years){
            $query->whereIn('year', $years);
        });
    }


    public function isFeatureType($name, \App\Language $language)
    {
        return $this->product_feature_type()->whereHas('info', function($query) use ($name, $language){
            $query->where('name', $name)->where('language_id', $language->id);
        })->count();
    }

    public function profiles() {
        return $this->product_feature_options()->where('product_feature_id', 8);
    }

    public function allimages($shop = null){
        $productImages = collect();

        if($shop)
        {
            $productImages = $this->shopImages($shop);
        }
        else
        {
            $productImages = $this->images;
        }

        if($this->groups()) {
            $groupImages = [];
            foreach($this->groups as $group){
                if($shop){
                    $groupImages = $group->images->where('shop_id', $shop);
                }else{
                    $groupImages = $group->images;
                }
            }
            return $productImages->merge($groupImages);
        }

        return $productImages;
    }

    public function shopImages($shop_id)
    {
        if(is_object($shop_id))
        {
            return $this->shopImages($shop_id->id);
        }

        return $this->morphMany(Image::class, 'imagable')->where('shop_id', $shop_id)->get();
    }

    public function files()
    {
        return $this->morphMany(File::class, 'filable');
    }

    public function msds()
    {
        return $this->morphMany(ProductMaterialSafetyDataSheet::class, 'filable');
    }

    public function certificates()
    {
        return $this->morphMany(ProductCertificate::class, 'filable');
    }

    public function scopeActive($query)
    {
        return $query->where('active', 1);
    }

    public function selectShop($shop)
    {
        if(is_numeric($shop))
        {
            $this->selected_shop = $shop;
        }

        return $this;
    }

    public function assignTag($tags)
    {
        if(is_object($tags))
        {
            $ids = array($tags->id);
        }

        if(is_numeric($tags))
        {
            $ids = array(ProductTag::findOrFail($tags)->id);
        }

        if(is_array($tags))
        {
            $ids = $tags;
        }

        $this->product_tags()->sync($ids);
    }

    public function assignCategory($categories)
    {
        if(is_numeric($categories) || is_object($categories))
        {
            $category_id = $categories->id ?? $categories;

            if(!$this->hasCategoryID($category_id))
            {
                $this->categories()->attach($category_id);
            }
        }

        if(is_array($categories))
        {
            $ids = $categories;
            $this->categories()->sync($ids);
        }
    }

    public function assignFeature($feature_option)
    {
        if(is_object($feature_option))
        {
            $ids = array($feature_option->id);
        }

        if(is_numeric($feature_option))
        {
            $ids = array(ProductFeatureOption::findOrFail($feature_option)->id);
        }

        if(is_array($feature_option))
        {
            $ids = $feature_option;
        }

        $this->product_feature_options()->syncWithoutDetaching($ids);
    }

    public function syncTags($tags)
    {
        if(is_object($tags))
        {
            $ids = array($tags->id);
        }

        if(is_numeric($tags))
        {
            $ids = array(ProductTag::findOrFail($tags)->id);
        }

        if(is_array($tags))
        {
            $ids = $tags;
        }

        $this->product_tags()->sync($ids);
    }

    public function hasCategoryID($category_id)
    {
        return $this->categories->contains('id', $category_id);
    }

    public function hasShopID($shop_id)
    {
        return $this->shops()->get()->contains('id', $shop_id);
    }

    //Trait
    public function filterThrough($request)
    {
        $this->builder = $this;

        if($request->has('search'))
        {
            $this->builder = $this->filterSearch($request->search);
        }

        return $this->builder;
    }

    private function filterSearch($search_string)
    {
        return $this->builder->join('product_infos', 'products.id', '=', 'product_infos.product_id')
        ->where('name', 'like', '%' . $search_string . '%')
        ->groupBy('product_infos.product_id')
        ->select('products.id');
    }

    public function insertPrice($purchase_price, $retail_price, $list_price, $wholesale_price)
    {
        ProductPrice::where('product_id', $this->id)->delete();

        $price = new ProductPrice;
        $price->product_id = $this->id;
        $price->purchasing_price = $purchase_price;
        $price->retail_price = $retail_price;
        $price->list_price = $list_price;
        $price->wholesale_price = $wholesale_price;
        $price->save();
    }

    public function insertShopPrice($shop, $retail_price, $list_price = 0, $unit_name = null, $unit_price = 0)
    {
        ProductShopPrice::where('product_id', $this->id)->where('shop_id', $shop->id)->delete();

        $price = new ProductShopPrice;
        $price->shop_id = $shop->id;
        $price->product_id = $this->id;
        $price->retail_price = $retail_price;
        $price->list_price = $list_price;
        $price->unit_name = $unit_name;
        $price->unit_price = $unit_price;
        $price->save();
    }

    public function tags()
    {
        $result = array();
        foreach($this->product_tags as $tag)
        {
            $result[$tag->language_id][] = $tag->name;
        }

        foreach($result as $language_id => $tags)
        {
            $result[$language_id]  = implode(",", $tags);
        }

        $this->tags = $result;
    }

    public function getStocks()
    {
        $stocks = $this->product_stocks()->orderBy('created_at', 'desc')->get()->groupBy('warehouse_id');
        $result = array();
        foreach($stocks as $stock)
        {
            $result[$stock[0]->warehouse_id] = $stock[0]->amount;
        }

        $this->stocks = $result;
    }

    public function getSelectedFeatureOptions()
    {
        $result = array();
        foreach($this->product_feature_options as $option)
        {
            $result[$option->product_feature->id] = $option->id;
        }

        $this->feature_options = json_encode($result);
    }

    public function getImageRank()
    {
        $rank = 10;
        $image = $this->images()->orderBy('rank', 'asc')->first();

        if($image)
        {
            $rank = $image->rank + 10;
        }

        return $rank;
    }

    public function scopeShopInfo($query, $shop_id, $language_id)
    {
        return $query
        ->with(['brand.image' => function($query) use ($shop_id){
            $query->where('shop_id', $shop_id);
        }])
        ->with(['shop_price' => function($query) use ($shop_id){
            $query->where('shop_id', $shop_id);
        }])
        ->with(['shop_product' => function($query) use ($shop_id) {
            $query->shop($shop_id)->with('tax_rule');
        }])
        ->with(['shop_info' => function($query) use ($shop_id, $language_id){
            $query->where('shop_id', $shop_id)->where('language_id', $language_id)->with(['shop' => function($q){
                $q->with(['tax_rule' => function($qq){
                    $qq->active();
                }]);
            }]);
        }])
        ->with(['catalog_price_rules' => function($query) use ($shop_id){
            $query->whereHas('shops', function($q) use ($shop_id){
                $q->where('id', $shop_id);
            })->active();
        }]);
    }

    public function scopeFeatureInfo($query, $language_id)
    {
        if(is_object($language_id))
        {
            $language_id = $language_id->id;
        }

        return $query->with(['product_feature_options' => function($query) use ($language_id){

            $query->with(['info' => function($option_query) use ($language_id){
                $option_query->where('language_id', $language_id);
            }]);

            $query->with(['product_feature.infos' => function($feature_query) use ($language_id){
                $feature_query->where('language_id', $language_id);
            }]);
        }]);
    }

    public function scopeFeatureByName($query, $name, $language_id){
        $query->with(['product_feature_options' => function($q) use($name, $language_id){
            $q->with(['info' => function($q) use($language_id){
                $q->where('language_id', $language_id);
            }]);
            $q->with('product_feature.info');
            $q->whereHas('product_feature.info', function($q) use($name, $language_id){
                $q->where('language_id', $language_id);
                $q->where('name', $name);
            });
        }]);
    }

    public function scopeFilter($query, $filters)
    {
        if(isset($filters["brand"]))
        {
            if(!empty($filters["brand"]))
            {
                if(!is_array($filters["brand"]))
                {
                    $filters["brand"] = explode(",", $filters["brand"]);
                }

                $query->whereIn("brand_id", $filters["brand"]);
            }
        }

        if(isset($filters["options"]))
        {
            foreach($filters["options"] as $option)
            {
                if(!empty($option))
                {
                    foreach (explode(",", $option) as $optionVal) {
                        $query->whereHas('product_feature_options', function ($q) use ($filters, $option, $optionVal) {
                            $q->where(function ($q) use ($filters, $option, $optionVal) {
                                $q->where('id', $optionVal);
                            });
                        });
                    }
                }
            }
        }

        if(isset($filters["price"]))
        {
            if(!empty($filters["price"]["min"]) || !empty($filters["price"]["max"]))
            {
                $query->whereHas('product_price', function($q) use ($filters){
                    $q->where('retail_price', '>=', $filters["price"]["min"] ?? 0);
                    $q->where('retail_price', '<=', $filters["price"]["max"] ?? PHP_INT_MAX);
                });
            }
        }

        return $query;
    }

    public function scopeShopFilter($query, $filters, $shop_id)
    {
        if(isset($filters["brand"]))
        {
            if(!empty($filters["brand"]))
            {
                if(!is_array($filters["brand"]))
                {
                    $filters["brand"] = explode(",", $filters["brand"]);
                }

                $query->whereIn("brand_id", $filters["brand"]);
            }
        }

        if(isset($filters["options"]))
        {
            foreach($filters["options"] as $option)
            {
                if(!empty($option))
                {
                    foreach (explode(",", $option) as $optionVal) {
                        $query->whereHas('product_feature_options', function ($q) use ($filters, $option, $optionVal) {
                            $q->where(function ($q) use ($filters, $option, $optionVal) {
                                $q->where('id', $optionVal);
                            });
                        });
                    }
                }
            }
        }

        if(isset($filters["price"]))
        {
            if(!empty($filters["price"]["min"]) || !empty($filters["price"]["max"]))
            {
                $query->whereHas('shop_price', function($q) use ($filters, $shop_id){
                    $q->where('shop_id', $shop_id);
                    $q->where('retail_price', '>=', $filters["price"]["min"] ?? 0);
                    $q->where('retail_price', '<=', $filters["price"]["max"] ?? PHP_INT_MAX);
                });
            }
        }

        return $query;
    }

    public function scopeShopActive($query, $shop_id)
    {
        $query->whereHas('shops', function($q) use ($shop_id){
            $q->where('id', $shop_id)->active();
        });
    }

    public function scopeProductFilter($q, $shop_id)
    {
        $filterGroups = ProductFilterGroup::All();
        foreach($filterGroups as $filterGroup){
            $filters = $filterGroup->filters->where('shop_id', $shop_id);
            $q->where(function($query) use ($shop_id, $filters){
                foreach($filters as $filter)
                {
                    $value = $filter->value;
                    settype($value, $filter->value_type);

                    if($filter->operator == 'less'){
                        $operator = '<';
                    }elseif($filter->operator == 'more'){
                        $operator = '>';
                    }elseif($filter->operator == 'equal'){
                        $operator = '=';
                    }elseif($filter->operator == 'not'){
                        $operator = '!=';
                    }elseif($filter->operator == 'like'){
                        $value = '%'.$value.'%';
                        $operator = 'LIKE';
                    }else{
                        throw new Exception('Operator not found in filter');
                    }

                    $fields = explode('.', $filter->field);

                    if(count($fields) <= 1){
                        $query->subFilter([], $filter->field, $value, $filter->shop_val, $shop_id, $operator, $filter->clause);
                    }else{
                        $num_fields = count($fields);
                        $column = $fields[$num_fields-1];
                        $relations = $fields;
                        if(!is_array($relations)){
                            $column = $relations;
                            $relations = [];
                        }
                        $query->subFilter($relations, $column, $value, $filter->shop_val, $shop_id, $operator, $filter->clause);
                    }
                }
            });
        }
    }

    public function syncFeatureOption($option)
    {
        if(is_array($option))
        {
            foreach($option as $o)
            {
                $this->syncFeatureOption($o);
            }

            return $this;
        }

        if(is_numeric($option))
        {
            $option = ProductFeatureOption::with('product_feature.product_feature_type')->find($option);
        }

        if($this->product_feature_options->contains('product_feature_id', $option->product_feature_id))
        {
           $this->product_feature_options()->detach($this->product_feature_options->where('product_feature_id', $option->product_feature_id));
        }

        $this->product_feature_options()->attach($option->id);

        $this->product_feature_type_id = $option->product_feature->product_feature_type->id;

        $this->save();
    }

    public function chartData()
    {
        $data = array();

        foreach($this->platforms as $platform)
        {

            $prices = $platform->platform_product_prices()->where('product_id', $this->id)->latest()->limit(15)->get()->sortBy('created_at');

            $price_datas = array();

            foreach($prices as $price)
            {
                $price_datas[] = array(
                    "x" => $price->created_at->format('Y-m-d'),
                    "y" => $price->price
                );
            }

            $data[] = array(
                "meta"  => $platform->name,
                "name"  => $platform->name,
                "data"  => $price_datas
            );
        }

        return collect($data)->toJson();
    }

    public static function reformatFeatures($products)
    {
        foreach($products as $product)
        {
            $product->features = new class{};

            foreach($product->product_feature_options as $option)
            {
                $feature = str_slug($option['product_feature']['info']['name'], '_');

                $product->features->$feature = (object)[
                    'feature_id'    => $option['product_feature']['id'],
                    'feature'    => $option['product_feature']['info']['name'],
                    'option_id'     => $option['id'],
                    'option'        => $option['info']['name']
                ];
            }

            unset($product->product_feature_options);
        }
        
        return $products;
    }
}