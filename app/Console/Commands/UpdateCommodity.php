<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Commodity;
use App\CommodityPrice;

class UpdateCommodity extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'commodity:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update commidity prices';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $call = guzzle()->get('https://www.quandl.com/api/v3/datasets/ODA/PRUBB_USD.json', [
            'query' => [
                'api_key'     => 'RWG8zFvcLjZuN-v7VPR9'
            ]
        ]);

        $rubber = Commodity::where('name', 'Rubber')->with('prices')->first();
        
        foreach(json_decode($call->getBody())->dataset->data as $data)
        {
            $date = carbon()->createFromFormat('Y-m-d', $data[0])->startOfDay();

            if($rubber->prices->where('created_at', $date)->count() == 0)
            {
                CommodityPrice::create([
                    "commodity_id"  => $rubber->id,
                    "price"         => $data[1],
                    "created_at"    => $data[0]
                ]);
            }
        }
    }
}
