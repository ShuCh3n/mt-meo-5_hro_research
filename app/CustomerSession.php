<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerSession extends Model
{
	protected $fillable = ['customer_id', 'session_token', 'ip', 'checked_out'];

    public function cart()
    {
    	return $this->hasOne(Cart::class);
    }

    public function customer()
    {
    	return $this->belongsTo(Customer::class);
    }

    public function scopeToken($query, $token)
    {
    	return $query->where('session_token', $token);
    }
}
