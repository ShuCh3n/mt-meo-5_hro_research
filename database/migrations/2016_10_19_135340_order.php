<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Order extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_statuses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('percentage');
            $table->boolean('can_ship')->default(false);
            $table->string('hex_color');
            $table->timestamps();
        });

        DB::table('order_statuses')->insert([
            [
                "name" => "cash",
                "percentage" => "100",
                "can_ship" => true,
                "hex_color" => "#16a085"
            ],
            [
                "name" => "cash_on_deliver",
                "percentage" => "75",
                "can_ship" => true,
                "hex_color" => "#1abc9c"
            ],
            [
                "name" => "awaiting_payment",
                "percentage" => "10",
                "can_ship" => false,
                "hex_color" => "#2c3e50"
            ],
            [
                "name" => "backorder_not_paid",
                "percentage" => "20",
                "can_ship" => false,
                "hex_color" => "#95a5a6"
            ],
            [
                "name" => "backorder_paid",
                "percentage" => "50",
                "can_ship" => true,
                "hex_color" => "#34495e"
            ],
            [
                "name" => "payment_accepted",
                "percentage" => "50",
                "can_ship" => true,
                "hex_color" => "#3498db"
            ],
            [
                "name" => "payment_error",
                "percentage" => "10",
                "can_ship" => false,
                "hex_color" => "#e74c3c"
            ],
            [
                "name" => "processing",
                "percentage" => "60",
                "can_ship" => true,
                "hex_color" => "#8e44ad"
            ],
            [
                "name" => "refunded",
                "percentage" => "100",
                "can_ship" => false,
                "hex_color" => "#e74c3c"
            ],
            [
                "name" => "shipped",
                "percentage" => "90",
                "can_ship" => true,
                "hex_color" => "#2ecc71"
            ],
            [
                "name" => "received",
                "percentage" => "100",
                "can_ship" => true,
                "hex_color" => "#27ae60"
            ],
            [
                "name" => "cancelled",
                "percentage" => "90",
                "can_ship" => false,
                "hex_color" => "#c0392b"
            ],
        ]);

        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('shop_id')->unsigned();
            $table->integer('payment_id')->unsigned();
            $table->integer('order_status_id')->unsigned();
            $table->integer('customer_id')->unsigned();
            $table->integer('shipping_id')->unsigned();
            $table->integer('currency_id')->unsigned();
            $table->string('reference')->unique();
            $table->string('company')->nullable();
            $table->string('street')->nullable();
            $table->string('housenumber')->nullable();
            $table->string('zipcode')->nullable();
            $table->string('city')->nullable();
            $table->integer('country_id')->unsigned();
            $table->boolean('dropship')->default(false);
            $table->text('internal_comment')->nullable();
            $table->text('customer_comment')->nullable();
            $table->decimal('total_price');
            $table->decimal('tax_price');
            $table->decimal('shipping_price')->nullable();
            $table->string('ip');
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('shop_id')
                ->references('id')
                ->on('shops')
                ->onDelete('cascade');

            $table->foreign('payment_id')
                ->references('id')
                ->on('payments')
                ->onDelete('cascade');

            $table->foreign('customer_id')
                ->references('id')
                ->on('customers')
                ->onDelete('cascade');

            $table->foreign('country_id')
                ->references('id')
                ->on('countries')
                ->onDelete('cascade');

            $table->foreign('order_status_id')
                ->references('id')
                ->on('order_statuses')
                ->onDelete('cascade');

            $table->foreign('shipping_id')
                ->references('id')
                ->on('shippings')
                ->onDelete('cascade');

            $table->foreign('currency_id')
                ->references('id')
                ->on('currencies')
                ->onDelete('cascade');
        });

        Schema::create('order_transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order_id')->unsigned();
            $table->string('payment');
            $table->string('transaction_reference');
            $table->timestamps();

            $table->foreign('order_id')
                ->references('id')
                ->on('orders')
                ->onDelete('cascade');
        });

        Schema::create('order_lines', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order_id')->unsigned();
            $table->integer('product_id')->unsigned();
            $table->integer('warehouse_id')->unsigned();
            $table->string('name');
            $table->integer('qty');
            $table->decimal('price');
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('order_id')
                ->references('id')
                ->on('orders')
                ->onDelete('cascade');

            $table->foreign('product_id')
                ->references('id')
                ->on('products')
                ->onDelete('cascade');

            $table->foreign('warehouse_id')
                ->references('id')
                ->on('warehouses')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_lines');
        Schema::dropIfExists('order_transactions');
        Schema::dropIfExists('orders');
        Schema::dropIfExists('order_statuses');
    }
}
