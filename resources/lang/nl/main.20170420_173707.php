<?php
/*************************************************************************
 Generated via "php artisan localization:missing" at 2017/04/10 17:38:45
*************************************************************************/

return array (
  //==================================== Translations ====================================//
  'CMS' => 'CMS',
  'Settings' => 'Instellingen',
  'customers' => 'Klanten',
  'images' => 'Afbeeldingen',
  'logout' => 'Uitloggen',
  'no' => 'Nee',
  'orders' => 'Orders',
  'products' => 'Producten',
  'search' => 'Zoeken',
  'welcome' => 'Welkom',
  'yes' => 'Ja',
);
