@extends('dashboard.layout.main')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-6">
            <h2>{{ trans('pages.inquiry_detail_header') }}</h2>
            <ol class="breadcrumb">
                <li>
                    {{ trans('menu.home') }}
                </li>
                <li>
                    {{ trans('menu.inquiries') }}
                </li>
                <li class="active">
                    <strong>{{ trans('page.inquiry') }}</strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">
        <form method="post"  class="form-horizontal" enctype="multipart/form-data">
            <div class="row">
                <div class="col-xs-8">
                    <div class="ibox">
                        <div class="ibox-title">
                            <h5>New Blog Category</h5>
                        </div>
                        <div class="ibox-content">

                            <div class="form-group">
                                <label class="col-sm-2 control-label">Cover</label>
                                <div class="col-sm-10">
                                    <div class="image_block text-center">
                                        <label for="cover-file-input">
                                            <div class="preview_image center-block hide">
                                                <img id="preview_image" src="#" />
                                            </div>
                                        
                                            <div class="choose_image_block" id="container">
                                                <i class="fa fa-picture-o"></i> Choose Cover Image
                                            </div>
                                        </label>
                                        <input id="cover-file-input" name="cover_image" type="file" />
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label">@lang('blog.language')</label>

                                <div class="col-md-10">
                                    <language-select class="form-control">
                                        @foreach($languages as $language)
                                            <option value="{{ $language->id }}" icon="{{ $language->icon }}">{{ $language->name }}</option>
                                        @endforeach
                                    </language-select>
                                </div>
                            </div>

                            @foreach($languages as $language)
                                <div class="multi_language {{ $language->id }}">
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Name {!! flag_icon($language->icon) !!}</label>

                                        <div class="col-md-10">
                                            <input type="text" class="form-control" name="language[{{ $language->id }}][name]" value="{{(@$blogposts)? @$blogposts->blog_post_content($language->id)->title : '' }}" />
                                        </div>
                                    </div>
                                </div>
                            @endforeach

                        </div>
                    </div>

                </div>

                <div class="col-xs-4">
                    <div class="ibox">
                        <div class="ibox-title">
                            <h5>@lang('blog.action')</h5>
                        </div>
                        <div class="ibox-content">
                            {{ csrf_field() }}
                            <button class="btn btn-block btn-outline btn-success" type="submit"><i class="fa fa-floppy-o"></i> @lang('blog.save')</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
@stop
