<?php
 namespace App\Classes\WebHooks;


 class WebHookFactory
 {
     public static function create($model, $webhook, $event)
     {
         if($webhook->template ==  'wooCommerce'){
             return new WooCommerce($model, $webhook, $event);
         }
        if($webhook->name ==  'utilize'){
             return new Utilize($model, $webhook, $event);
         }
         return new Generic($model, $webhook, $event);
     }
 }
