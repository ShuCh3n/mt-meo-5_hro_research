<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\TicketDepartment;
use App\Ticket;
use App\TicketMessage;

class MessageController extends Controller
{
    public function messages()
    {
    	$departments = TicketDepartment::get();
    	$tickets = Ticket::with(['customer'])->notClosed()->paginate(100);
        $total_unreaded_messages = TicketMessage::whereHas('ticket', function($query){
            $query->notClosed();
        })->where('customer', true)->where('read', false)->get();

        $tickets = $tickets->sortByDesc(function ($product, $key) {
            return $product->ticket_messages->where('customer', true)->where('read', false)->count();
        });
        
    	return view('dashboard.messages', compact('departments', 'tickets', 'total_unreaded_messages'));
    }

    public function closedMessages()
    {
        $departments = TicketDepartment::get();
        $tickets = Ticket::with(['customer'])->closed()->paginate(100);
        $total_unreaded_messages = TicketMessage::whereHas('ticket', function($query){
            $query->notClosed();
        })->where('customer', true)->where('read', false)->get();

        return view('dashboard.messages', compact('departments', 'tickets', 'total_unreaded_messages'));
    }

    public function viewMessage()
    {
    	$departments = TicketDepartment::get();

    	$ticket = Ticket::with(['ticket_messages' => function($query){
    		$query->orderBy('created_at', 'asc');
    	}])->with('order')->findOrFail($this->request->id);

        $ticket->markedReadMessage(true);

        $total_unreaded_messages = TicketMessage::whereHas('ticket', function($query){
            $query->notClosed();
        })->where('customer', true)->where('read', false)->get();

    	return view('dashboard.view_message', compact('departments', 'ticket', 'total_unreaded_messages'));
    }

    public function actionUserReplyMessage()
    {
    	$ticket = Ticket::findOrFail($this->request->id);
    	$ticket->addMessage($this->request);

    	return redirect()->route('view_messages', $ticket->id);
    }

    public function closeTicket()
    {
        $ticket = Ticket::findOrFail($this->request->id);
        $ticket->close();

        return redirect()->route('messages');
    }
}
