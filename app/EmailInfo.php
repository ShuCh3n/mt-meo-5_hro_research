<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmailInfo extends Model
{
    public function scopeBackOffice($query)
    {
    	$query->where('language_id', auth()->user()->preferenceLanguage()->id);
    }

    public function scopeLanguage($query, $language_id)
    {
    	$query->where('language_id', $language_id);
    }
}
