<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\CategoryInfo;

class Category extends Model
{
    protected $hidden = [
        'created_at', 'deleted_at', 'updated_at'
    ];
    
    public function parent()
    {
    	return $this->belongsTo(Category::class);
    }

    public function subcategories()
    {
    	return $this->hasMany(Category::class, 'parent_id');
    }

    public function infos()
    {
    	return $this->hasMany(CategoryInfo::class);
    }

    public function info()
    {
        return $this->hasOne(CategoryInfo::class);
    }

    public function products()
    {
        return $this->belongsToMany(Product::class);
    }

    public function shop()
    {
        return $this->belongsTo(Shop::class);
    }
}
