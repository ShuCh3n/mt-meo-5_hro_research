@extends('dashboard.layout.main')

@section('vue_templates')
    <script type="text/x-template" id="page-content">
        <div class="wrapper wrapper-content animated fadeInRight">
            {!! session()->has('success')? '<div class="alert alert-success alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>' . session('success') . '</div>' : '' !!}

            <form method="post" class="form-horizontal">
                <div class="row">
                    <div class="col-md-8">
                        <div class="ibox">

                            <div class="ibox-title">
                                <h5>New Motor</h5>
                            </div>

                            <div class="ibox-content">
                                <div class="mb20"></div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">Motor Brand</label>
                                            <div class="col-md-10">
                                                <select2 name="vehicle_brand">
                                                    @foreach($vehicle_brands as $brand)
                                                        <option value="{{ $brand->id }}">{{ $brand->name }}</option>
                                                    @endforeach
                                                </select2>  
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-2 control-label">Years</label>
                                            <div class="col-md-5">
                                                <select2 name="from_year" v-model="first_year">
                                                    @for($i=1970;$i<=date('Y');$i++)
                                                        <option value="{{ $i }}">{{ $i }}</option>
                                                    @endfor
                                                </select>
                                            </div>
                                            <div class="col-md-5">
                                                <select2 name="until_year" v-model="last_year">
                                                    @for($i=1970;$i<=date('Y');$i++)
                                                        <option value="{{ $i }}">{{ $i }}</option>
                                                    @endfor
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-2 control-label">Code</label>
                                            <div class="col-md-10">
                                                <input type="text" class="form-control" name="code">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-2 control-label">Name</label>
                                            <div class="col-md-10">
                                                <input type="text" class="form-control" name="name">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-2 control-label">Model</label>
                                            <div class="col-md-10">
                                                <input type="text" class="form-control" name="model">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-2 control-label">Type</label>
                                            <div class="col-md-10">
                                                <input type="text" class="form-control" name="type">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-2 control-label">CC</label>
                                            <div class="col-md-10">
                                                <integer-input name="cc" max="5000"></integer-input>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-2 control-label">Different Sizes</label>
                                            <div class="col-md-10">
                                                <div class="switch">
                                                    <div class="onoffswitch">
                                                        <input type="checkbox" class="onoffswitch-checkbox" id="different_tyre_sizes" v-model="different_tyre_sizes" name="different_tyre_sizes" value="1">
                                                        <label class="onoffswitch-label" for="different_tyre_sizes">
                                                            <span class="onoffswitch-inner"></span>
                                                            <span class="onoffswitch-switch"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div v-show="!this.different_tyre_sizes">
                                            <div class="form-group">
                                                <label class="col-md-2 control-label">Tyre Size</label>
                                                <div class="col-md-10">
                                                    <div class="row">
                                                        <div class="col-md-6"><tyre-size-input ref="size_front" allsize="front" name="size_front"></tyre-size-input></div>
                                                        <div class="col-md-6"><tyre-size-input ref="size_rear" allsize="rear" name="size_rear"></tyre-size-input></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div v-show="this.different_tyre_sizes">
                                            <div class="form-group" v-for="year in years">
                                                <label class="col-md-2 control-label">Tyre Size @{{ year.year }}</label>
                                                <div class="col-md-10">
                                                    <div class="row">
                                                        <div class="col-md-6"><tyre-size-input :value="$refs['size_front'].size" :name="'custom_size_front[' + year.year + ']'"></tyre-size-input></div>
                                                        <div class="col-md-6"><tyre-size-input :value="$refs['size_rear'].size" :name="'custom_size_rear[' + year.year + ']'"></tyre-size-input></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div> 
                    </div>

                    <div class="col-md-4">
                        <div class="ibox">
                            <div class="ibox-title">
                                <h5>Action</h5>
                            </div>

                            <div class="ibox-content">
                                {{ csrf_field() }}
                                <button class="btn btn-block btn-outline btn-success" type="submit"><i class="fa fa-floppy-o"></i> Save</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>

            <div class="mb30"></div>
        </div>
    </script>
@stop

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>{{ trans('pages.inquiry_detail_header') }}</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('home') }}">{{ trans('menu.home') }}</a>
                </li>
                <li>
                    <a href="">{{ trans('menu.inquiries') }}</a>
                </li>
                <li class="active">
                    <strong>{{ trans('page.inquiry') }}</strong>
                </li>
            </ol>
        </div>
    </div>

    <motor-profile></motor-profile>
@stop