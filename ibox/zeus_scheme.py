# coding: utf-8
from sqlalchemy import BigInteger, Column, Date, DateTime, Float, ForeignKey, Integer, JSON, Numeric, String, Table, text
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base


Base = declarative_base()
metadata = Base.metadata


class AccountViewSetting(Base):
    __tablename__ = 'account_view_settings'

    id = Column(Integer, primary_key=True)
    platform_id = Column(ForeignKey('platforms.id', ondelete='CASCADE'), nullable=False, index=True)
    country_id = Column(ForeignKey('countries.id', ondelete='CASCADE'), nullable=False, index=True)
    settings = Column(String, nullable=False)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)

    country = relationship('Country')
    platform = relationship('Platform')

class AnalistFile(Base):
    __tablename__ = 'analist_files'

    id = Column(Integer, primary_key=True)
    user_id = Column(ForeignKey('users.id'), nullable=False, index=True)
    original_filename = Column(String(255), nullable=False)
    filename = Column(String(255), nullable=False)
    deleted_at = Column(DateTime)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)

    user = relationship('User')


class ArInternalMetadatum(Base):
    __tablename__ = 'ar_internal_metadata'

    key = Column(String(255), primary_key=True)
    value = Column(String(255))
    created_at = Column(DateTime)
    updated_at = Column(DateTime)


class BlogPostBlogTag(Base):
    __tablename__ = 'blog_post_blog_tags'

    id = Column(Integer, primary_key=True)
    blog_tags_id = Column(ForeignKey('blog_tags.id'), nullable=False, index=True)
    blog_post_id = Column(ForeignKey('blog_posts.id'), nullable=False, index=True)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)

    blog_post = relationship('BlogPost')
    blog_tags = relationship('BlogTag')


class BlogPostContent(Base):
    __tablename__ = 'blog_post_contents'

    id = Column(Integer, primary_key=True)
    active = Column(String(5), nullable=False)
    languages_id = Column(ForeignKey('languages.id'), nullable=False, index=True)
    blog_post_id = Column(ForeignKey('blog_posts.id'), nullable=False, index=True)
    title = Column(String(255), nullable=False)
    short = Column(String, nullable=False)
    body = Column(String, nullable=False)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)
    deleted_at = Column(DateTime)

    blog_post = relationship('BlogPost')
    languages = relationship('Language')


class BlogPost(Base):
    __tablename__ = 'blog_posts'

    id = Column(Integer, primary_key=True)
    active = Column(String(5), nullable=False)
    published_at = Column(DateTime)
    category = Column(String(250))
    blog_id = Column(ForeignKey('blogs.id'), index=True)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)
    deleted_at = Column(DateTime)

    blog = relationship('Blog')


class BlogTagLanguage(Base):
    __tablename__ = 'blog_tag_languages'

    id = Column(Integer, primary_key=True)
    blog_tags_id = Column(ForeignKey('blog_tags.id'), nullable=False, index=True)
    languages_id = Column(ForeignKey('languages.id'), nullable=False, index=True)
    name = Column(String(255), nullable=False)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)

    blog_tags = relationship('BlogTag')
    languages = relationship('Language')


class BlogTag(Base):
    __tablename__ = 'blog_tags'

    id = Column(Integer, primary_key=True)
    blogs_id = Column(ForeignKey('blogs.id'), nullable=False, index=True)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)

    blogs = relationship('Blog')


class Blog(Base):
    __tablename__ = 'blogs'

    id = Column(Integer, primary_key=True)
    title = Column(String(255), nullable=False)
    active = Column(String(5), nullable=False)
    shop_id = Column(ForeignKey('shops.id'), nullable=False, index=True)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)
    deleted_at = Column(DateTime)

    shop = relationship('Shop')


class Brand(Base):
    __tablename__ = 'brands'

    id = Column(Integer, primary_key=True)
    logo = Column(String(255))
    name = Column(String(255), nullable=False)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)


class CartProduct(Base):
    __tablename__ = 'cart_products'

    id = Column(Integer, primary_key=True)
    cart_id = Column(ForeignKey('carts.id'), nullable=False, index=True)
    product_id = Column(ForeignKey('products.id'), nullable=False, index=True)
    qty = Column(Integer, nullable=False)
    retail_price = Column(Numeric(10, 2), nullable=False)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)

    cart = relationship('Cart')
    product = relationship('Product')


t_cart_rule_category = Table(
    'cart_rule_category', metadata,
    Column('cart_rule_id', ForeignKey('cart_rules.id'), primary_key=True, nullable=False),
    Column('category_id', ForeignKey('categories.id'), primary_key=True, nullable=False, index=True)
)


t_cart_rule_country = Table(
    'cart_rule_country', metadata,
    Column('cart_rule_id', ForeignKey('cart_rules.id'), primary_key=True, nullable=False),
    Column('country_id', ForeignKey('countries.id'), primary_key=True, nullable=False, index=True)
)


t_cart_rule_customer = Table(
    'cart_rule_customer', metadata,
    Column('cart_rule_id', ForeignKey('cart_rules.id'), primary_key=True, nullable=False),
    Column('customer_id', ForeignKey('customers.id'), primary_key=True, nullable=False, index=True)
)


t_cart_rule_customer_group = Table(
    'cart_rule_customer_group', metadata,
    Column('cart_rule_id', ForeignKey('cart_rules.id'), primary_key=True, nullable=False),
    Column('customer_group_id', ForeignKey('customer_groups.id'), primary_key=True, nullable=False, index=True)
)


t_cart_rule_product = Table(
    'cart_rule_product', metadata,
    Column('cart_rule_id', ForeignKey('cart_rules.id'), primary_key=True, nullable=False),
    Column('product_id', ForeignKey('products.id'), primary_key=True, nullable=False, index=True)
)


t_cart_rule_product_feature_type = Table(
    'cart_rule_product_feature_type', metadata,
    Column('cart_rule_id', ForeignKey('cart_rules.id'), primary_key=True, nullable=False),
    Column('product_feature_type_id', ForeignKey('product_feature_types.id'), primary_key=True, nullable=False, index=True)
)


t_cart_rule_shop = Table(
    'cart_rule_shop', metadata,
    Column('cart_rule_id', ForeignKey('cart_rules.id'), primary_key=True, nullable=False),
    Column('shop_id', ForeignKey('shops.id'), primary_key=True, nullable=False, index=True)
)


class CartRule(Base):
    __tablename__ = 'cart_rules'

    id = Column(Integer, primary_key=True)
    active = Column(String(5), nullable=False)
    giftcard = Column(String(5), nullable=False)
    free_shipping = Column(String(5), nullable=False)
    name = Column(String(255), nullable=False)
    amount_or_percentage = Column(String(5), nullable=False)
    amount = Column(Numeric(10, 2), nullable=False)
    voucher_code = Column(String(255), nullable=False)
    usage_limit = Column(Integer)
    valid_from = Column(Date)
    valid_until = Column(Date)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)

    shops = relationship('Shop', secondary='cart_rule_shop')
    categorys = relationship('Category', secondary='cart_rule_category')
    customers = relationship('Customer', secondary='cart_rule_customer')
    customer_groups = relationship('CustomerGroup', secondary='cart_rule_customer_group')
    product_feature_types = relationship('ProductFeatureType', secondary='cart_rule_product_feature_type')
    countrys = relationship('Country', secondary='cart_rule_country')
    products = relationship('Product', secondary='cart_rule_product')


class Cart(Base):
    __tablename__ = 'carts'

    id = Column(Integer, primary_key=True)
    shop_id = Column(ForeignKey('shops.id'), nullable=False, index=True)
    customer_session_id = Column(ForeignKey('customer_sessions.id'), nullable=False, index=True)
    deleted_at = Column(DateTime)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)
    additional_fee = Column(Numeric(10, 2))
    shipment_fee = Column(Numeric(10, 2))

    customer_session = relationship('CustomerSession')
    shop = relationship('Shop')


t_catalog_price_rule_category = Table(
    'catalog_price_rule_category', metadata,
    Column('catalog_price_rule_id', ForeignKey('catalog_price_rules.id'), primary_key=True, nullable=False),
    Column('category_id', ForeignKey('categories.id'), primary_key=True, nullable=False, index=True)
)


t_catalog_price_rule_country = Table(
    'catalog_price_rule_country', metadata,
    Column('catalog_price_rule_id', ForeignKey('catalog_price_rules.id'), primary_key=True, nullable=False),
    Column('country_id', ForeignKey('countries.id'), primary_key=True, nullable=False, index=True)
)


t_catalog_price_rule_customer_group = Table(
    'catalog_price_rule_customer_group', metadata,
    Column('catalog_price_rule_id', ForeignKey('catalog_price_rules.id'), primary_key=True, nullable=False),
    Column('customer_group_id', ForeignKey('customer_groups.id'), primary_key=True, nullable=False, index=True)
)


t_catalog_price_rule_product = Table(
    'catalog_price_rule_product', metadata,
    Column('catalog_price_rule_id', ForeignKey('catalog_price_rules.id'), primary_key=True, nullable=False),
    Column('product_id', ForeignKey('products.id'), primary_key=True, nullable=False, index=True)
)


t_catalog_price_rule_product_feature_type = Table(
    'catalog_price_rule_product_feature_type', metadata,
    Column('catalog_price_rule_id', ForeignKey('catalog_price_rules.id'), primary_key=True, nullable=False),
    Column('product_feature_type_id', ForeignKey('product_feature_types.id'), primary_key=True, nullable=False, index=True)
)


t_catalog_price_rule_shop = Table(
    'catalog_price_rule_shop', metadata,
    Column('catalog_price_rule_id', ForeignKey('catalog_price_rules.id'), primary_key=True, nullable=False),
    Column('shop_id', ForeignKey('shops.id'), primary_key=True, nullable=False, index=True)
)


class CatalogPriceRule(Base):
    __tablename__ = 'catalog_price_rules'

    id = Column(Integer, primary_key=True)
    active = Column(String(5), nullable=False)
    name = Column(String(255), nullable=False)
    add_or_subtract = Column(String(5), nullable=False)
    amount_or_percentage = Column(String(5), nullable=False)
    amount = Column(Numeric(10, 2), nullable=False)
    deleted_at = Column(DateTime)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)
    priority = Column(Integer)
    valid_from = Column(Date)
    valid_until = Column(Date)

    shops = relationship('Shop', secondary='catalog_price_rule_shop')
    customer_groups = relationship('CustomerGroup', secondary='catalog_price_rule_customer_group')
    product_feature_types = relationship('ProductFeatureType', secondary='catalog_price_rule_product_feature_type')
    products = relationship('Product', secondary='catalog_price_rule_product')
    countrys = relationship('Country', secondary='catalog_price_rule_country')
    categorys = relationship('Category', secondary='catalog_price_rule_category')


class Category(Base):
    __tablename__ = 'categories'

    id = Column(Integer, primary_key=True)
    parent_id = Column(ForeignKey('categories.id'), index=True)
    shop_id = Column(ForeignKey('shops.id'), nullable=False, index=True)
    rank = Column(Integer, nullable=False)
    deleted_at = Column(DateTime)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)

    parent = relationship('Category', remote_side=[id])
    shop = relationship('Shop')
    products = relationship('Product', secondary='category_product')


class CategoryInfo(Base):
    __tablename__ = 'category_infos'

    id = Column(Integer, primary_key=True)
    category_id = Column(ForeignKey('categories.id'), nullable=False, index=True)
    language_id = Column(ForeignKey('languages.id'), nullable=False, index=True)
    name = Column(String(255), nullable=False)
    meta_description = Column(String(255))
    description = Column(String)
    link = Column(String(255))
    created_at = Column(DateTime)
    updated_at = Column(DateTime)

    category = relationship('Category')
    language = relationship('Language')


class CategoryMotorProductVehicleYear(Base):
    __tablename__ = 'category_motor_product_vehicle_year'

    category_id = Column(ForeignKey('categories.id', ondelete='CASCADE'), primary_key=True, nullable=False)
    motor_id = Column(ForeignKey('motors.id', ondelete='CASCADE'), primary_key=True, nullable=False, index=True)
    product_id = Column(ForeignKey('products.id', ondelete='CASCADE'), primary_key=True, nullable=False, index=True)
    vehicle_year_id = Column(ForeignKey('vehicle_years.id', ondelete='CASCADE'), primary_key=True, nullable=False, index=True)
    month = Column(Integer)
    sku = Column(String(255, 'utf8mb4_unicode_ci'), primary_key=True, nullable=False)
    min_qty = Column(Integer)

    category = relationship('Category')
    motor = relationship('Motor')
    product = relationship('Product')
    vehicle_year = relationship('VehicleYear')


t_category_product = Table(
    'category_product', metadata,
    Column('category_id', ForeignKey('categories.id'), primary_key=True, nullable=False),
    Column('product_id', ForeignKey('products.id'), primary_key=True, nullable=False, index=True)
)


class Commodity(Base):
    __tablename__ = 'commodities'

    id = Column(Integer, primary_key=True)
    name = Column(String(255, 'utf8mb4_unicode_ci'), nullable=False)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)


class CommodityPrice(Base):
    __tablename__ = 'commodity_prices'

    id = Column(Integer, primary_key=True)
    commodity_id = Column(ForeignKey('commodities.id', ondelete='CASCADE'), nullable=False, index=True)
    price = Column(Numeric(10, 2), nullable=False)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)

    commodity = relationship('Commodity')


class Country(Base):
    __tablename__ = 'countries'

    id = Column(Integer, primary_key=True)
    name = Column(String(255), nullable=False)
    iso_code_2 = Column(String(2), nullable=False)
    iso_code_3 = Column(String(255), nullable=False)
    zipcode_format = Column(String(255))
    call_prefix = Column(Integer, nullable=False)
    icon = Column(String(255), nullable=False)

    tax_rules = relationship('TaxRule', secondary='country_tax_rule')
    shops = relationship('Shop', secondary='country_shop')


t_country_shop = Table(
    'country_shop', metadata,
    Column('country_id', ForeignKey('countries.id'), primary_key=True, nullable=False),
    Column('shop_id', ForeignKey('shops.id'), primary_key=True, nullable=False, index=True)
)


t_country_tax_rule = Table(
    'country_tax_rule', metadata,
    Column('country_id', ForeignKey('countries.id'), primary_key=True, nullable=False),
    Column('tax_rule_id', ForeignKey('tax_rules.id'), primary_key=True, nullable=False, index=True)
)


class Currency(Base):
    __tablename__ = 'currencies'

    id = Column(Integer, primary_key=True)
    name = Column(String(255), nullable=False)
    decimal_separator = Column(String(1), nullable=False)
    thousand_separator = Column(String(1), nullable=False)
    icon = Column(String(255), nullable=False)
    symbol_icon = Column(String(5), nullable=False)
    symbol = Column(String(5), nullable=False)
    rate = Column(Numeric(10, 5), nullable=False)

    shops = relationship('Shop', secondary='currency_shop')


t_currency_shop = Table(
    'currency_shop', metadata,
    Column('shop_id', ForeignKey('shops.id'), primary_key=True, nullable=False),
    Column('currency_id', ForeignKey('currencies.id'), primary_key=True, nullable=False, index=True)
)


class CustomerAddress(Base):
    __tablename__ = 'customer_addresses'

    id = Column(Integer, primary_key=True)
    customer_id = Column(ForeignKey('customers.id'), nullable=False, index=True)
    alias = Column(String(255), nullable=False)
    company = Column(String(255))
    firstname = Column(String(255), nullable=False)
    lastname = Column(String(255), nullable=False)
    mobile_phone = Column(String(255))
    home_phone = Column(String(255))
    street = Column(String(255), nullable=False)
    housenumber = Column(String(255), nullable=False)
    city = Column(String(255), nullable=False)
    zipcode = Column(String(255), nullable=False)
    state = Column(String(255))
    state_id = Column(ForeignKey('states.id'), index=True)
    country_id = Column(ForeignKey('countries.id'), nullable=False, index=True)
    default = Column(String(5), nullable=False)
    note = Column(String)
    deleted_at = Column(DateTime)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)

    country = relationship('Country')
    customer = relationship('Customer')
    state1 = relationship('State')


class CustomerGroup(Base):
    __tablename__ = 'customer_groups'

    id = Column(Integer, primary_key=True)
    name = Column(String(255), nullable=False)
    discount = Column(Integer, nullable=False)
    tax = Column(String(5), nullable=False)
    show_prices = Column(String(5), nullable=False)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)


class CustomerLog(Base):
    __tablename__ = 'customer_logs'

    id = Column(Integer, primary_key=True)
    customer_id = Column(ForeignKey('customers.id'), nullable=False, index=True)
    message = Column(String(255), nullable=False)
    level = Column(Integer, nullable=False)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)

    customer = relationship('Customer')


class CustomerPasswordReset(Base):
    __tablename__ = 'customer_password_resets'

    id = Column(Integer, primary_key=True)
    shop_id = Column(ForeignKey('shops.id'), nullable=False, index=True)
    customer_id = Column(ForeignKey('customers.id'), nullable=False, index=True)
    token = Column(String(255), nullable=False)
    active = Column(String(5), nullable=False)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)

    customer = relationship('Customer')
    shop = relationship('Shop')


class CustomerSession(Base):
    __tablename__ = 'customer_sessions'

    id = Column(Integer, primary_key=True)
    customer_id = Column(ForeignKey('customers.id'), index=True)
    session_token = Column(String(255), nullable=False)
    ip = Column(String(255), nullable=False)
    checked_out = Column(String(5), nullable=False)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)

    customer = relationship('Customer')


class Customer(Base):
    __tablename__ = 'customers'

    id = Column(Integer, primary_key=True)
    av_deb = Column(String(255))
    mand = Column(String(255), nullable=False)
    company_name = Column(String(255))
    shop_id = Column(ForeignKey('shops.id'), nullable=False, index=True)
    customer_group_id = Column(ForeignKey('customer_groups.id'), index=True)
    gender = Column(String(5))
    firstname = Column(String(255), nullable=False)
    lastname = Column(String(255), nullable=False)
    dob = Column(Date)
    email = Column(String(255), nullable=False)
    password = Column(String(255), nullable=False)
    mobile_phone = Column(String(255))
    home_phone = Column(String(255))
    billing_company = Column(String(255))
    billing_vat_number = Column(String(255))
    billing_firstname = Column(String(255), nullable=False)
    billing_lastname = Column(String(255), nullable=False)
    billing_street = Column(String(255), nullable=False)
    billing_housenumber = Column(String(255), nullable=False)
    billing_city = Column(String(255), nullable=False)
    billing_zipcode = Column(String(255), nullable=False)
    billing_state = Column(String(255))
    billing_state_id = Column(ForeignKey('states.id'), index=True)
    billing_country_id = Column(ForeignKey('countries.id'), nullable=False, index=True)
    tax_rule_id = Column(ForeignKey('tax_rules.id'), index=True)
    optin = Column(String(5), nullable=False)
    note = Column(String)
    active = Column(String(5), nullable=False)
    deleted_at = Column(DateTime)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)

    billing_country = relationship('Country')
    billing_state1 = relationship('State')
    customer_group = relationship('CustomerGroup')
    shop = relationship('Shop')
    tax_rule = relationship('TaxRule')


class Department(Base):
    __tablename__ = 'departments'

    id = Column(Integer, primary_key=True)
    name = Column(String(255), nullable=False)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)


class EmailAvatar(Base):
    __tablename__ = 'email_avatars'

    id = Column(Integer, primary_key=True)
    email = Column(String(255), nullable=False)
    filename = Column(String(255), nullable=False)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)


class EmailInfo(Base):
    __tablename__ = 'email_infos'

    id = Column(Integer, primary_key=True)
    email_id = Column(ForeignKey('emails.id'), nullable=False, index=True)
    language_id = Column(ForeignKey('languages.id'), nullable=False, index=True)
    title = Column(String(255), nullable=False)
    view_file = Column(String(255), nullable=False)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)

    email = relationship('Email')
    language = relationship('Language')


class EmailTypeInfo(Base):
    __tablename__ = 'email_type_infos'

    id = Column(Integer, primary_key=True)
    language_id = Column(ForeignKey('languages.id'), nullable=False, index=True)
    email_type_id = Column(ForeignKey('email_types.id'), nullable=False, index=True)
    name = Column(String(255), nullable=False)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)

    email_type = relationship('EmailType')
    language = relationship('Language')


class EmailType(Base):
    __tablename__ = 'email_types'

    id = Column(Integer, primary_key=True)
    identifier = Column(String(255), nullable=False)
    newsletter = Column(String(5), nullable=False)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)


class Email(Base):
    __tablename__ = 'emails'

    id = Column(Integer, primary_key=True)
    email_type_id = Column(ForeignKey('email_types.id'), nullable=False, index=True)
    shop_id = Column(ForeignKey('shops.id'), nullable=False, index=True)
    active = Column(String(5), nullable=False)
    deleted_at = Column(DateTime)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)

    email_type = relationship('EmailType')
    shop = relationship('Shop')


t_files = Table(
    'files', metadata,
    Column('filename', String(255, 'utf8mb4_unicode_ci'), nullable=False),
    Column('filable_id', Integer, nullable=False, index=True),
    Column('filable_type', String(255, 'utf8mb4_unicode_ci'), nullable=False, index=True),
    Column('extension', String(255, 'utf8mb4_unicode_ci'), nullable=False),
    Column('type', String(255, 'utf8mb4_unicode_ci'), nullable=False),
    Column('shop_id', ForeignKey('shops.id', ondelete='CASCADE'), index=True),
    Column('deleted_at', DateTime),
    Column('created_at', DateTime),
    Column('updated_at', DateTime)
)


t_group_product = Table(
    'group_product', metadata,
    Column('product_id', ForeignKey('products.id', ondelete='CASCADE'), primary_key=True, nullable=False),
    Column('group_id', ForeignKey('groups.id', ondelete='CASCADE'), primary_key=True, nullable=False, index=True)
)


class Group(Base):
    __tablename__ = 'groups'

    id = Column(Integer, primary_key=True)
    name = Column(String(255, 'utf8mb4_unicode_ci'), nullable=False)
    shop_id = Column(ForeignKey('shops.id', ondelete='CASCADE'), index=True)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)

    shop = relationship('Shop')
    products = relationship('Product', secondary='group_product')


class ImageType(Base):
    __tablename__ = 'image_type'

    id = Column(Integer, primary_key=True)
    name = Column(String(255), nullable=False)
    deleted_at = Column(DateTime)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)


class Image(Base):
    __tablename__ = 'images'

    id = Column(Integer, primary_key=True)
    filename = Column(String(255), nullable=False)
    imagable_id = Column(Integer, nullable=False)
    imagable_type = Column(String(255), nullable=False)
    image_type_id = Column(ForeignKey('image_type.id'), index=True)
    shop_id = Column(ForeignKey('shops.id'), index=True)
    deleted_at = Column(DateTime)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)

    image_type = relationship('ImageType')
    shop = relationship('Shop')


t_language_shop = Table(
    'language_shop', metadata,
    Column('shop_id', ForeignKey('shops.id'), primary_key=True, nullable=False),
    Column('language_id', ForeignKey('languages.id'), primary_key=True, nullable=False, index=True)
)


class Language(Base):
    __tablename__ = 'languages'

    id = Column(Integer, primary_key=True)
    name = Column(String(255), nullable=False)
    iso = Column(String(2), nullable=False)
    locale = Column(String(255), nullable=False)
    icon = Column(String(255), nullable=False)

    shops = relationship('Shop', secondary='language_shop')


class Migration(Base):
    __tablename__ = 'migrations'

    id = Column(Integer, primary_key=True)
    migration = Column(String(255), nullable=False)
    batch = Column(Integer, nullable=False)


class MotorVehicleYear(Base):
    __tablename__ = 'motor_vehicle_year'

    motor_id = Column(ForeignKey('motors.id', ondelete='CASCADE'), primary_key=True, nullable=False)
    vehicle_year_id = Column(ForeignKey('vehicle_years.id', ondelete='CASCADE'), primary_key=True, nullable=False, index=True)
    tyre_front_width = Column(Integer, nullable=False)
    tyre_front_height = Column(Integer, nullable=False)
    tyre_front_inch = Column(Integer, nullable=False)
    tyre_rear_width = Column(Integer, nullable=False)
    tyre_rear_height = Column(Integer, nullable=False)
    tyre_rear_inch = Column(Integer, nullable=False)

    motor = relationship('Motor')
    vehicle_year = relationship('VehicleYear')


class Motor(Base):
    __tablename__ = 'motors'

    id = Column(Integer, primary_key=True)
    code = Column(String(255, 'utf8mb4_unicode_ci'), nullable=False, unique=True)
    vehicle_brand_id = Column(ForeignKey('vehicle_brands.id'), index=True)
    image = Column(String(255, 'utf8mb4_unicode_ci'))
    name = Column(String(255, 'utf8mb4_unicode_ci'), nullable=False)
    model = Column(String(255, 'utf8mb4_unicode_ci'), nullable=False)
    type = Column(String(255, 'utf8mb4_unicode_ci'), nullable=False)
    cc = Column(Integer, nullable=False)
    different_size = Column(Integer, nullable=False)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)

    vehicle_brand = relationship('VehicleBrand')


class OrderLine(Base):
    __tablename__ = 'order_lines'

    id = Column(Integer, primary_key=True)
    order_id = Column(ForeignKey('orders.id'), nullable=False, index=True)
    product_id = Column(ForeignKey('products.id'), nullable=False, index=True)
    no_products = Column(Integer, nullable=False)
    price = Column(Float(asdecimal=True), nullable=False)
    customer_comment = Column(String, nullable=False)
    third_party_stock = Column(String(5), nullable=False)
    deleted_at = Column(DateTime)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)

    order = relationship('Order')
    product = relationship('Product')


class OrderStatus(Base):
    __tablename__ = 'order_statuses'

    id = Column(Integer, primary_key=True)
    name = Column(String(255), nullable=False)
    percentage = Column(Integer, nullable=False)
    can_ship = Column(String(5), nullable=False)
    hex_color = Column(String(255), nullable=False)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)


class OrderTransaction(Base):
    __tablename__ = 'order_transactions'

    id = Column(Integer, primary_key=True)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)
    order_id = Column(ForeignKey('orders.id'), nullable=False, index=True)
    payment = Column(String(32), nullable=False)
    transaction_reference = Column(String(50), nullable=False)

    order = relationship('Order')


class Order(Base):
    __tablename__ = 'orders'

    id = Column(Integer, primary_key=True)
    ref = Column(String(255), nullable=False)
    placed_on = Column(DateTime)
    ip = Column(String(255), nullable=False)
    customer_id = Column(ForeignKey('customers.id'), nullable=False, index=True)
    address = Column(String(255), nullable=False)
    housenumber = Column(String(255))
    city = Column(String(255), nullable=False)
    postal_code = Column(String(255), nullable=False)
    mand = Column(String(255), nullable=False)
    customer_code = Column(String(255), nullable=False)
    deliver_company = Column(String(255))
    deliver_street = Column(String(255))
    deliver_housenumber = Column(String(255))
    deliver_zipcode = Column(String(255))
    deliver_city = Column(String(255))
    deliver_country_id = Column(ForeignKey('countries.id'), nullable=False, index=True)
    dropship = Column(String(5), nullable=False)
    internal_comment = Column(String)
    customer_comment = Column(String)
    customer_refference = Column(String(255))
    country_id = Column(ForeignKey('countries.id'), nullable=False, index=True)
    shipment_method = Column(String(255), nullable=False)
    shipment_node = Column(String(255))
    shipment_costs = Column(Numeric(8, 2))
    total_price = Column(Numeric(8, 2), nullable=False)
    bem = Column(Numeric(8, 2), nullable=False)
    profit = Column(Numeric(8, 2))
    profit_payed = Column(String(5))
    order_status_id = Column(ForeignKey('order_statuses.id'), nullable=False, index=True)
    payment_id = Column(ForeignKey('payments.id'), nullable=False, index=True)
    shop_id = Column(ForeignKey('shops.id'), nullable=False, index=True)
    checked = Column(String(5), nullable=False)
    deleted_at = Column(DateTime)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)
    email = Column(String(50))
    phone = Column(String(20))
    additional_costs = Column(Numeric(8, 2))

    country = relationship('Country', primaryjoin='Order.country_id == Country.id')
    customer = relationship('Customer')
    deliver_country = relationship('Country', primaryjoin='Order.deliver_country_id == Country.id')
    order_status = relationship('OrderStatus')
    payment = relationship('Payment')
    shop = relationship('Shop')


class PageInfo(Base):
    __tablename__ = 'page_infos'

    id = Column(Integer, primary_key=True)
    page_id = Column(ForeignKey('pages.id'), nullable=False, index=True)
    language_id = Column(ForeignKey('languages.id'), nullable=False, index=True)
    title = Column(String(255), nullable=False)
    meta_description = Column(String(255), nullable=False)
    link = Column(String(255), nullable=False)
    body = Column(String, nullable=False)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)

    language = relationship('Language')
    page = relationship('Page')


class Page(Base):
    __tablename__ = 'pages'

    id = Column(Integer, primary_key=True)
    active = Column(String(5), nullable=False)
    shop_id = Column(ForeignKey('shops.id'), nullable=False, index=True)
    rank = Column(Integer, nullable=False)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)

    shop = relationship('Shop')


t_password_resets = Table(
    'password_resets', metadata,
    Column('email', String(255), nullable=False),
    Column('token', String(255), nullable=False),
    Column('created_at', DateTime, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))
)


t_payment_shop = Table(
    'payment_shop', metadata,
    Column('payment_id', ForeignKey('payments.id'), primary_key=True, nullable=False),
    Column('shop_id', ForeignKey('shops.id'), primary_key=True, nullable=False, index=True)
)


class Payment(Base):
    __tablename__ = 'payments'

    id = Column(Integer, primary_key=True)
    image = Column(String(255))
    name = Column(String(255), nullable=False)
    description = Column(String)
    fee = Column(Numeric(10, 2))
    percentage_fee = Column(Numeric(10, 2))
    created_at = Column(DateTime)
    updated_at = Column(DateTime)

    shops = relationship('Shop', secondary='payment_shop')


class PaynlTransaction(Base):
    __tablename__ = 'paynl_transactions'

    id = Column(Integer, primary_key=True)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)


class Paynl(Base):
    __tablename__ = 'paynls'

    id = Column(Integer, primary_key=True)
    shop_id = Column(ForeignKey('shops.id'), nullable=False, index=True)
    test_mode = Column(String(5), nullable=False)
    service_id = Column(String(255), nullable=False)
    api_token = Column(String(255), nullable=False)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)

    shop = relationship('Shop')


class PermissionLanguage(Base):
    __tablename__ = 'permission_languages'

    id = Column(Integer, primary_key=True)
    permission_id = Column(ForeignKey('permissions.id'), nullable=False, index=True)
    language_id = Column(ForeignKey('languages.id'), nullable=False, index=True)
    name = Column(String(255), nullable=False)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)

    language = relationship('Language')
    permission = relationship('Permission')


class PermissionUser(Base):
    __tablename__ = 'permission_user'

    permission_id = Column(ForeignKey('permissions.id'), primary_key=True, nullable=False)
    user_id = Column(ForeignKey('users.id'), primary_key=True, nullable=False, index=True)
    options = Column(String(250), nullable=False)

    permission = relationship('Permission')
    user = relationship('User')


class Permission(Base):
    __tablename__ = 'permissions'

    id = Column(Integer, primary_key=True)
    parent_id = Column(ForeignKey('permissions.id'), index=True)
    name = Column(String(255), nullable=False)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)

    parent = relationship('Permission', remote_side=[id])


class PgheroQueryStat(Base):
    __tablename__ = 'pghero_query_stats'

    id = Column(Integer, primary_key=True)
    database = Column(String)
    user = Column(String)
    query = Column(String)
    query_hash = Column(BigInteger)
    total_time = Column(Float(asdecimal=True))
    calls = Column(BigInteger)
    captured_at = Column(DateTime)


class PlatformProduct(Base):
    __tablename__ = 'platform_product'

    platform_id = Column(ForeignKey('platforms.id'), primary_key=True, nullable=False)
    product_id = Column(ForeignKey('products.id'), primary_key=True, nullable=False, index=True)
    article_code = Column(String(255))

    platform = relationship('Platform')
    product = relationship('Product')


class PlatformProductPrice(Base):
    __tablename__ = 'platform_product_prices'

    id = Column(Integer, primary_key=True)
    platform_id = Column(ForeignKey('platforms.id'), nullable=False, index=True)
    product_id = Column(ForeignKey('products.id'), nullable=False, index=True)
    price = Column(Numeric(10, 2), nullable=False)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)

    platform = relationship('Platform')
    product = relationship('Product')


class Platform(Base):
    __tablename__ = 'platforms'

    id = Column(Integer, primary_key=True)
    name = Column(String(255), nullable=False)
    priority = Column(Integer, nullable=False)
    b2b = Column(String(5), nullable=False)
    code = Column(String(255), nullable=False)
    settings = Column(JSON, nullable=False)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)


class ProductAttributeLanguage(Base):
    __tablename__ = 'product_attribute_languages'

    id = Column(Integer, primary_key=True)
    product_attribute_id = Column(ForeignKey('product_attributes.id'), nullable=False, index=True)
    language_id = Column(ForeignKey('languages.id'), nullable=False, index=True)
    name = Column(String(255), nullable=False)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)

    language = relationship('Language')
    product_attribute = relationship('ProductAttribute')


class ProductAttributeOptionLanguage(Base):
    __tablename__ = 'product_attribute_option_languages'

    id = Column(Integer, primary_key=True)
    product_attribute_option_id = Column(ForeignKey('product_attribute_options.id'), nullable=False, index=True)
    language_id = Column(ForeignKey('languages.id'), nullable=False, index=True)
    name = Column(String(255), nullable=False)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)

    language = relationship('Language')
    product_attribute_option = relationship('ProductAttributeOption')


t_product_attribute_option_product_combination = Table(
    'product_attribute_option_product_combination', metadata,
    Column('product_attribute_option_id', ForeignKey('product_attribute_options.id'), primary_key=True, nullable=False),
    Column('product_combination_id', ForeignKey('product_combinations.id'), primary_key=True, nullable=False, index=True)
)


class ProductAttributeOption(Base):
    __tablename__ = 'product_attribute_options'

    id = Column(Integer, primary_key=True)
    product_attribute_id = Column(ForeignKey('product_attributes.id'), nullable=False, index=True)
    color = Column(String(255))
    created_at = Column(DateTime)
    updated_at = Column(DateTime)

    product_attribute = relationship('ProductAttribute')
    product_combinations = relationship('ProductCombination', secondary='product_attribute_option_product_combination')


class ProductAttributeType(Base):
    __tablename__ = 'product_attribute_types'

    id = Column(Integer, primary_key=True)
    name = Column(String(255), nullable=False)


class ProductAttribute(Base):
    __tablename__ = 'product_attributes'

    id = Column(Integer, primary_key=True)
    product_attribute_type_id = Column(ForeignKey('product_attribute_types.id'), nullable=False, index=True)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)

    product_attribute_type = relationship('ProductAttributeType')


class ProductBulkPrice(Base):
    __tablename__ = 'product_bulk_prices'

    id = Column(Integer, primary_key=True)
    shop_id = Column(ForeignKey('products.id'), nullable=False, index=True)
    product_id = Column(ForeignKey('products.id'), nullable=False, index=True)
    active = Column(String(5), nullable=False)
    wholesale_price = Column(String(5), nullable=False)
    qty = Column(Integer, nullable=False)
    retail_price = Column(Numeric(10, 2), nullable=False)
    list_price = Column(Numeric(10, 2), nullable=False)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)

    product = relationship('Product', primaryjoin='ProductBulkPrice.product_id == Product.id')
    shop = relationship('Product', primaryjoin='ProductBulkPrice.shop_id == Product.id')


t_product_combination_product_image = Table(
    'product_combination_product_image', metadata,
    Column('product_combination_id', ForeignKey('product_combinations.id'), primary_key=True, nullable=False),
    Column('product_image_id', ForeignKey('product_images.id'), primary_key=True, nullable=False, index=True)
)


class ProductCombinationStock(Base):
    __tablename__ = 'product_combination_stocks'

    id = Column(Integer, primary_key=True)
    product_combination_id = Column(ForeignKey('product_combinations.id'), nullable=False, index=True)
    warehouse_id = Column(ForeignKey('warehouses.id'), nullable=False, index=True)
    amount = Column(Integer, nullable=False)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)

    product_combination = relationship('ProductCombination')
    warehouse = relationship('Warehouse')


class ProductCombination(Base):
    __tablename__ = 'product_combinations'

    id = Column(Integer, primary_key=True)
    product_id = Column(ForeignKey('products.id'), nullable=False, index=True)
    default = Column(String(5), nullable=False)
    supplier_sku = Column(String(255))
    ean = Column(String(255))
    upc = Column(String(255))
    sku = Column(String(255))
    minimum_qty = Column(Integer, nullable=False)
    impact_price_type = Column(Integer)
    impact_price = Column(Numeric(8, 2))
    impact_weight_type = Column(Integer)
    impact_weight = Column(Numeric(10, 2))
    impact_unit_price_type = Column(Integer)
    impact_unit_price = Column(Numeric(10, 2))
    created_at = Column(DateTime)
    updated_at = Column(DateTime)

    product = relationship('Product')
    product_images = relationship('ProductImage', secondary='product_combination_product_image')


class ProductFeatureLanguage(Base):
    __tablename__ = 'product_feature_languages'

    id = Column(Integer, primary_key=True)
    product_feature_id = Column(ForeignKey('product_features.id'), nullable=False, index=True)
    language_id = Column(ForeignKey('languages.id'), nullable=False, index=True)
    name = Column(String(255), nullable=False)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)

    language = relationship('Language')
    product_feature = relationship('ProductFeature')


class ProductFeatureOptionLanguage(Base):
    __tablename__ = 'product_feature_option_languages'

    id = Column(Integer, primary_key=True)
    product_feature_option_id = Column(ForeignKey('product_feature_options.id'), nullable=False, index=True)
    language_id = Column(ForeignKey('languages.id'), nullable=False, index=True)
    name = Column(String(255), nullable=False)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)

    language = relationship('Language')
    product_feature_option = relationship('ProductFeatureOption')


class ProductFeatureOption(Base):
    __tablename__ = 'product_feature_options'

    id = Column(Integer, primary_key=True)
    product_feature_id = Column(ForeignKey('product_features.id'), nullable=False, index=True)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)

    product_feature = relationship('ProductFeature')
    products = relationship('Product', secondary='product_product_feature_option')


class ProductFeatureTypeLanguage(Base):
    __tablename__ = 'product_feature_type_languages'

    id = Column(Integer, primary_key=True)
    product_feature_type_id = Column(ForeignKey('product_feature_types.id'), nullable=False, index=True)
    language_id = Column(ForeignKey('languages.id'), nullable=False, index=True)
    name = Column(String(255), nullable=False)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)

    language = relationship('Language')
    product_feature_type = relationship('ProductFeatureType')


class ProductFeatureType(Base):
    __tablename__ = 'product_feature_types'

    id = Column(Integer, primary_key=True)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)


class ProductFeature(Base):
    __tablename__ = 'product_features'

    id = Column(Integer, primary_key=True)
    product_feature_type_id = Column(ForeignKey('product_feature_types.id'), nullable=False, index=True)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)

    product_feature_type = relationship('ProductFeatureType')


class ProductFilterGroup(Base):
    __tablename__ = 'product_filter_groups'

    id = Column(Integer, primary_key=True)
    name = Column(String(255), nullable=False)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)


class ProductFilter(Base):
    __tablename__ = 'product_filters'

    id = Column(Integer, primary_key=True)
    shop_id = Column(ForeignKey('shops.id'), nullable=False, index=True)
    product_filter_group_id = Column(ForeignKey('product_filter_groups.id'), nullable=False, index=True)
    operator = Column(String(255), nullable=False)
    clause = Column(String(255), nullable=False)
    field = Column(String(255), nullable=False)
    value = Column(String(255), nullable=False)
    value_type = Column(String(255), nullable=False)
    weight = Column(Integer, nullable=False)
    shop_val = Column(String(5), nullable=False)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)

    product_filter_group = relationship('ProductFilterGroup')
    shop = relationship('Shop')


class ProductGroup(Base):
    __tablename__ = 'product_groups'

    id = Column(Integer, primary_key=True)
    name = Column(String(255), nullable=False)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)


class ProductImage(Base):
    __tablename__ = 'product_images'

    id = Column(Integer, primary_key=True)
    product_id = Column(ForeignKey('products.id'), nullable=False, index=True)
    image_link = Column(String(255), nullable=False)
    rank = Column(Integer, nullable=False)
    deleted_at = Column(DateTime)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)

    product = relationship('Product')


class ProductInfo(Base):
    __tablename__ = 'product_infos'

    id = Column(Integer, primary_key=True)
    product_id = Column(ForeignKey('products.id'), nullable=False, index=True)
    language_id = Column(ForeignKey('languages.id'), nullable=False, index=True)
    name = Column(String(255), nullable=False)
    short_description = Column(String)
    description = Column(String)
    specification = Column(String)
    link = Column(String(255))
    created_at = Column(DateTime)
    updated_at = Column(DateTime)

    language = relationship('Language')
    product = relationship('Product')


class ProductPrice(Base):
    __tablename__ = 'product_prices'

    id = Column(Integer, primary_key=True)
    product_id = Column(ForeignKey('products.id'), nullable=False, index=True)
    purchasing_price = Column(Numeric(10, 2))
    retail_price = Column(Float(asdecimal=True), nullable=False)
    list_price = Column(Numeric(10, 2))
    unit_name = Column(String(255))
    unit_price = Column(Numeric(10, 2))
    deleted_at = Column(DateTime)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)

    product = relationship('Product')


t_product_product_feature_option = Table(
    'product_product_feature_option', metadata,
    Column('product_id', ForeignKey('products.id'), primary_key=True, nullable=False),
    Column('product_feature_option_id', ForeignKey('product_feature_options.id'), primary_key=True, nullable=False, index=True)
)


t_product_product_tag = Table(
    'product_product_tag', metadata,
    Column('product_id', ForeignKey('products.id'), primary_key=True, nullable=False),
    Column('product_tag_id', ForeignKey('product_tags.id'), primary_key=True, nullable=False, index=True)
)


class ProductShopBulkPrice(Base):
    __tablename__ = 'product_shop_bulk_prices'

    id = Column(Integer, primary_key=True)
    shop_id = Column(ForeignKey('shops.id'), nullable=False, index=True)
    product_id = Column(ForeignKey('products.id'), nullable=False, index=True)
    active = Column(String(5), nullable=False)
    wholesale_price = Column(String(5), nullable=False)
    qty = Column(Integer, nullable=False)
    retail_price = Column(Numeric(10, 2), nullable=False)
    list_price = Column(Numeric(10, 2), nullable=False)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)

    product = relationship('Product')
    shop = relationship('Shop')


class ProductShopImage(Base):
    __tablename__ = 'product_shop_images'

    id = Column(Integer, primary_key=True)
    shop_id = Column(ForeignKey('shops.id'), nullable=False, index=True)
    product_id = Column(ForeignKey('products.id'), nullable=False, index=True)
    image_link = Column(String(255), nullable=False)
    rank = Column(Integer, nullable=False)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)

    product = relationship('Product')
    shop = relationship('Shop')


class ProductShopInfo(Base):
    __tablename__ = 'product_shop_infos'

    id = Column(Integer, primary_key=True)
    shop_id = Column(ForeignKey('shops.id'), nullable=False, index=True)
    product_id = Column(ForeignKey('products.id'), nullable=False, index=True)
    language_id = Column(ForeignKey('languages.id'), nullable=False, index=True)
    name = Column(String(255))
    short_description = Column(String)
    description = Column(String)
    specification = Column(String)
    link = Column(String(255))
    created_at = Column(DateTime)
    updated_at = Column(DateTime)

    language = relationship('Language')
    product = relationship('Product')
    shop = relationship('Shop')


class ProductShopPrice(Base):
    __tablename__ = 'product_shop_prices'

    id = Column(Integer, primary_key=True)
    shop_id = Column(ForeignKey('shops.id'), nullable=False, index=True)
    product_id = Column(ForeignKey('products.id'), nullable=False, index=True)
    retail_price = Column(Float(asdecimal=True), nullable=False)
    list_price = Column(Numeric(10, 2))
    unit_name = Column(String(255))
    unit_price = Column(Numeric(10, 2))
    created_at = Column(DateTime)
    updated_at = Column(DateTime)
    deleted_at = Column(DateTime)

    product = relationship('Product')
    shop = relationship('Shop')


class ProductStock(Base):
    __tablename__ = 'product_stocks'

    id = Column(Integer, primary_key=True)
    warehouse_id = Column(ForeignKey('warehouses.id'), nullable=False, index=True)
    product_id = Column(ForeignKey('products.id'), nullable=False, index=True)
    amount = Column(Integer, nullable=False)
    deleted_at = Column(DateTime)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)
    price = Column(Numeric(21, 6))

    product = relationship('Product')
    warehouse = relationship('Warehouse')


class ProductTag(Base):
    __tablename__ = 'product_tags'

    id = Column(Integer, primary_key=True)
    language_id = Column(ForeignKey('languages.id'), nullable=False, index=True)
    name = Column(String(255), nullable=False)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)

    language = relationship('Language')


class Product(Base):
    __tablename__ = 'products'

    id = Column(Integer, primary_key=True)
    brand_id = Column(ForeignKey('brands.id'), index=True)
    product_feature_type_id = Column(ForeignKey('product_feature_types.id'), index=True)
    product_group_id = Column(ForeignKey('product_groups.id'), index=True)
    tax_rule_id = Column(ForeignKey('tax_rules.id'), index=True)
    supplier_sku = Column(String(255))
    ean = Column(String(255))
    upc = Column(String(255))
    sku = Column(String(255), nullable=False)
    minimum_qty = Column(Integer, nullable=False)
    stock_management = Column(String(5), nullable=False)
    active_when_out_of_stock = Column(String(5), nullable=False)
    deny_order_when_out_of_stock = Column(String(5), nullable=False)
    width = Column(Numeric(8, 2))
    height = Column(Numeric(8, 2))
    depth = Column(Numeric(8, 2))
    weight = Column(Numeric(8, 2))
    additional_fee_per_item = Column(Numeric(8, 2))
    active = Column(String(5), nullable=False)
    deleted_at = Column(DateTime)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)
    imported = Column(String(5))

    brand = relationship('Brand')
    product_feature_type = relationship('ProductFeatureType')
    product_group = relationship('ProductGroup')
    tax_rule = relationship('TaxRule')
    product_tags = relationship('ProductTag', secondary='product_product_tag')
    platforms = relationship('Platform', secondary='platform_product')
    infos = relationship('ProductInfo')
    platform_product = relationship('PlatformProduct', lazy='dynamic')
    product_prices = relationship('ProductPrice', order_by='desc(ProductPrice.created_at)', lazy='dynamic')
    

class SchemaMigration(Base):
    __tablename__ = 'schema_migrations'

    version = Column(String(255), primary_key=True)


class ShippingInfo(Base):
    __tablename__ = 'shipping_infos'

    id = Column(Integer, primary_key=True)
    shipping_id = Column(ForeignKey('shippings.id'), nullable=False, index=True)
    language_id = Column(ForeignKey('languages.id'), nullable=False, index=True)
    name = Column(String(255), nullable=False)
    description = Column(String, nullable=False)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)

    language = relationship('Language')
    shipping = relationship('Shipping')


class ShippingRule(Base):
    __tablename__ = 'shipping_rules'

    id = Column(Integer, primary_key=True)
    shipping_id = Column(ForeignKey('shippings.id'), nullable=False, index=True)
    country_id = Column(ForeignKey('countries.id'), nullable=False, index=True)
    from_price = Column(Numeric(10, 2))
    till_price = Column(Numeric(10, 2))
    additional_fee = Column(Numeric(10, 2))
    free_shipping = Column(String(5))

    country = relationship('Country')
    shipping = relationship('Shipping')


t_shipping_shop = Table(
    'shipping_shop', metadata,
    Column('shipping_id', ForeignKey('shippings.id'), primary_key=True, nullable=False),
    Column('shop_id', ForeignKey('shops.id'), primary_key=True, nullable=False, index=True)
)


class Shipping(Base):
    __tablename__ = 'shippings'

    id = Column(Integer, primary_key=True)
    logo = Column(String(255), nullable=False)
    av_code = Column(String(255), nullable=False)
    base_fee = Column(Numeric(10, 2), nullable=False)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)

    shops = relationship('Shop', secondary='shipping_shop')


class ShopMaintenanceExcludedIp(Base):
    __tablename__ = 'shop_maintenance_excluded_ips'

    id = Column(Integer, primary_key=True)
    shop_id = Column(ForeignKey('shops.id'), nullable=False, index=True)
    ip = Column(String(255), nullable=False)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)

    shop = relationship('Shop')


class ShopProduct(Base):
    __tablename__ = 'shop_products'

    id = Column(Integer, primary_key=True)
    shop_id = Column(ForeignKey('shops.id'), nullable=False, index=True)
    product_id = Column(ForeignKey('products.id'), nullable=False, index=True)
    tax_rule_id = Column(ForeignKey('tax_rules.id'), index=True)
    minimum_qty = Column(Integer, nullable=False)
    stock_management = Column(String(5), nullable=False)
    active_when_out_of_stock = Column(String(5), nullable=False)
    deny_order_when_out_of_stock = Column(String(5), nullable=False)
    additional_fee_per_item = Column(Numeric(8, 2))
    active = Column(String(5), nullable=False)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)

    product = relationship('Product')
    shop = relationship('Shop')
    tax_rule = relationship('TaxRule')


class Shop(Base):
    __tablename__ = 'shops'

    id = Column(Integer, primary_key=True)
    tax_rule_id = Column(ForeignKey('tax_rules.id'), index=True)
    logo = Column(String(255))
    name = Column(String(255), nullable=False)
    AV_admin = Column(String(255), nullable=False)
    link = Column(String(255), nullable=False)
    key = Column(String(255), nullable=False)
    maintenance = Column(String(5), nullable=False)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)

    tax_rule = relationship('TaxRule')


t_state_tax_rule = Table(
    'state_tax_rule', metadata,
    Column('state_id', ForeignKey('states.id'), primary_key=True, nullable=False),
    Column('tax_rule_id', ForeignKey('tax_rules.id'), primary_key=True, nullable=False, index=True)
)


class State(Base):
    __tablename__ = 'states'

    id = Column(Integer, primary_key=True)
    country_id = Column(ForeignKey('countries.id'), nullable=False, index=True)
    name = Column(String(255), nullable=False)
    iso = Column(String(255))

    country = relationship('Country')
    tax_rules = relationship('TaxRule', secondary='state_tax_rule')


class TaxGroup(Base):
    __tablename__ = 'tax_groups'

    id = Column(Integer, primary_key=True)
    declaration_section_code = Column(String(255))
    name = Column(String(255), nullable=False)
    default = Column(String(5), nullable=False)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)


class TaxRule(Base):
    __tablename__ = 'tax_rules'

    id = Column(Integer, primary_key=True)
    tax_group_id = Column(ForeignKey('tax_groups.id'), nullable=False, index=True)
    enable = Column(String(5), nullable=False)
    code = Column(String(255))
    name = Column(String(255), nullable=False)
    fixed_price = Column(Float(asdecimal=True))
    percentage = Column(Numeric(10, 2))
    created_at = Column(DateTime)
    updated_at = Column(DateTime)

    tax_group = relationship('TaxGroup')


class TicketDepartment(Base):
    __tablename__ = 'ticket_departments'

    id = Column(Integer, primary_key=True)
    name = Column(String(255), nullable=False)
    color = Column(String(255), nullable=False)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)


class TicketMessageAttachment(Base):
    __tablename__ = 'ticket_message_attachments'

    id = Column(Integer, primary_key=True)
    ticket_message_id = Column(ForeignKey('ticket_messages.id'), index=True)
    file = Column(String(255), nullable=False)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)

    ticket_message = relationship('TicketMessage')


class TicketMessage(Base):
    __tablename__ = 'ticket_messages'

    id = Column(Integer, primary_key=True)
    ticket_id = Column(ForeignKey('tickets.id'), index=True)
    user_id = Column(ForeignKey('users.id'), index=True)
    customer = Column(String(5), nullable=False)
    message = Column(String, nullable=False)
    ip = Column(String(255), nullable=False)
    read = Column(String(5), nullable=False)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)

    ticket = relationship('Ticket')
    user = relationship('User')


class Ticket(Base):
    __tablename__ = 'tickets'

    id = Column(Integer, primary_key=True)
    shop_id = Column(ForeignKey('shops.id'), nullable=False, index=True)
    order_id = Column(ForeignKey('orders.id'), index=True)
    ticket_department_id = Column(ForeignKey('ticket_departments.id'), index=True)
    language_id = Column(ForeignKey('languages.id'), nullable=False, index=True)
    customer_id = Column(ForeignKey('customers.id'), index=True)
    reference = Column(String(255), nullable=False)
    subject = Column(String(255), nullable=False)
    gender = Column(String(5))
    initials = Column(String(255))
    first_name = Column(String(255))
    last_name = Column(String(255))
    email = Column(String(255))
    phonenumber = Column(String(255))
    closed = Column(String(5), nullable=False)
    deleted_at = Column(DateTime)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)

    customer = relationship('Customer')
    language = relationship('Language')
    order = relationship('Order')
    shop = relationship('Shop')
    ticket_department = relationship('TicketDepartment')


class User(Base):
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True)
    firstname = Column(String(255), nullable=False)
    lastname = Column(String(255), nullable=False)
    email = Column(String(255), nullable=False)
    password = Column(String(255), nullable=False)
    dob = Column(Date)
    options = Column(String(250), nullable=False)
    active = Column(String(5), nullable=False)
    remember_token = Column(String(100))
    created_at = Column(DateTime)
    updated_at = Column(DateTime)
    department_id = Column(ForeignKey('departments.id'), index=True)
    super_user = Column(String(5))

    department = relationship('Department')


class VehicleBrand(Base):
    __tablename__ = 'vehicle_brands'

    id = Column(Integer, primary_key=True)
    name = Column(String(255, 'utf8mb4_unicode_ci'), nullable=False)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)


class VehicleYear(Base):
    __tablename__ = 'vehicle_years'

    id = Column(Integer, primary_key=True)
    year = Column(Integer, nullable=False)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)


class Warehouse(Base):
    __tablename__ = 'warehouses'

    id = Column(Integer, primary_key=True)
    name = Column(String(255), nullable=False)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)
    third_party = Column(String(5))
    deltime = Column(Integer)


class Weather(Base):
    __tablename__ = 'weathers'

    id = Column(Integer, primary_key=True)
    country_id = Column(Integer, nullable=False)
    degree = Column(Numeric(3, 2), nullable=False)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)


class WebHook(Base):
    __tablename__ = 'web_hooks'

    id = Column(Integer, primary_key=True)
    name = Column(String(255, 'utf8mb4_unicode_ci'), nullable=False)
    url = Column(String(255, 'utf8mb4_unicode_ci'), nullable=False)
    template = Column(String(255, 'utf8mb4_unicode_ci'))
    shop_id = Column(ForeignKey('shops.id', ondelete='CASCADE'), index=True)
    username = Column(String(255, 'utf8mb4_unicode_ci'))
    password = Column(String(255, 'utf8mb4_unicode_ci'))
    token = Column(String(255, 'utf8mb4_unicode_ci'))
    created_at = Column(DateTime)
    updated_at = Column(DateTime)
    deleted_at = Column(DateTime)

    shop = relationship('Shop')
