@extends('dashboard.layout.main')

@section('content')
	<div class="wrapper wrapper-content animated fadeInRight ecommerce">

        <form id="profileForm" method="post" class="form-horizontal">
            <div class="row">
                <div class="col-lg-8">
                    <div class="ibox">
                        <div class="ibox-title">
                            <h5>{{ $platform->name }} Setting</h5>
                        </div>
                        <div class="ibox-content">
                            <div class="mb20"></div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Code</label>
                                        <div class="col-md-10"><input type="text" class="form-control" name="code" placeholder="Supplier Code" value="{{ $platform->code ?? '' }}"></div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Delivery Time</label>
                                        <div class="col-md-10">
                                            <integer-input max="99" min="0" placeholder="In hours" name="delivery" value="{{ $platform->settings()->delivery ?? '' }}"></integer-input>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Shipping</label>
                                        <div class="col-md-10">
                                            <amount-input max="1000" min="0" placeholder="Per product" name="shipping" value="{{ $platform->settings()->shipping ?? '' }}"></amount-input>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>

                    <div class="ibox">
                        <div class="ibox-title">
                            <h5><i class="fa fa-briefcase" aria-hidden="true"></i> Business to Business</h5>
                        </div>
                        <div class="ibox-content">
                            <div class="mb20"></div>

                            <div class="row">
                                <div class="col-md-12">

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Status</label>
                                        <div class="col-md-5">
                                            <label><input type="radio" class="form-control i-checks" name="b2b_active" value="1" {{ ($platform->settings() && $platform->settings()->b2b->active)? 'checked' : '' }}> Enabled</label>
                                        </div>
                                        <div class="col-md-5">
                                            <label><input type="radio" class="form-control i-checks" name="b2b_active" value="0" {{ ($platform->settings() && !$platform->settings()->b2b->active)? 'checked' : '' }}> Disabled</label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Amount</label>
                                        <div class="col-md-10">
                                            <amount-input max="1000" min="-1000" name="b2b_amount" value="{{ $platform->settings()->b2b->amount ?? '' }}"></amount-input>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Percentage</label>
                                        <div class="col-md-10">
                                            <percentage-input max="100" min="-100" name="b2b_percentage" value="{{ $platform->settings()->b2b->percentage ?? '' }}"></percentage-input>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>

                    <div class="ibox">
                        <div class="ibox-title">
                            <h5><i class="fa fa-users" aria-hidden="true"></i> Business to Consumer</h5>
                        </div>
                        <div class="ibox-content">
                            <div class="mb20"></div>

                            <div class="row">
                                <div class="col-md-12">

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Status</label>
                                        <div class="col-md-5">
                                            <label><input type="radio" class="form-control i-checks" name="b2c_active" value="1" {{ ($platform->settings() && $platform->settings()->b2c->active)? 'checked' : '' }}> Enabled</label>
                                        </div>
                                        <div class="col-md-5">
                                            <label><input type="radio" class="form-control i-checks" name="b2c_active" value="0" {{ ($platform->settings() && !$platform->settings()->b2c->active)? 'checked' : '' }}> Disabled</label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Amount</label>
                                        <div class="col-md-10">
                                            <amount-input max="1000" min="-1000" name="b2c_amount" value="{{ $platform->settings()->b2c->amount ?? '' }}"></amount-input>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Percentage</label>
                                        <div class="col-md-10">
                                            <percentage-input max="100" min="-100" name="b2c_percentage" value="{{ $platform->settings()->b2c->percentage ?? '' }}"></percentage-input>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="ibox">
                        <div class="ibox-title">
                            <h5>Action</h5>
                        </div>
                        <div class="ibox-content">

                            <div class="row">
                                <div class="col-md-12">
                                    {{ csrf_field() }}
                                    <button class="btn btn-block btn-outline btn-success" type="submit"><i class="fa fa-floppy-o"></i> Save</button>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
@stop