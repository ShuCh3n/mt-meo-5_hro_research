<?php

namespace App\Http\Controllers;

use App\Http\Requests;

use App\ProductFeature;
use App\ProductFeatureType;
use App\ProductFeatureOption;
use App\Language;
use App\ProductFeatureTypeLanguage;
use App\ProductFeatureLanguage;
use App\ProductFeatureOptionLanguage;
use App\Product;

class ProductFeatureController extends Controller
{
    protected $fillable = ['product_feature_type_id'];

    public function index()
    {
        $feature_types = ProductFeatureType::with(['info' => function($query){
            $query->backOffice();
        }])->get();

        return view('dashboard.product_feature_types', compact('feature_types'));
    }

    // deprecated
    // public function productFeatureOptionLanguagesWithImages(){
    //     $tyreProfile = 8;//ProductFeatureLanguage::where('name', 'Profile')->firstOrFail();

    //     $profiles = ProductFeatureOption::where('product_feature_id', $tyreProfile)
    //         ->leftJoin('product_feature_option_languages', 'product_feature_options.id', 'product_feature_option_languages.product_feature_option_id')
    //         ->with('images.imagetype');

    //     if($this->request->input('filter')){
    //         $profiles->where('name', 'ILIKE', '%'.strtolower($this->request->input('filter')).'%');
    //     }

    //     return($profiles)->get(['product_feature_option_languages.id', 'product_feature_option_id', 'name']);
    // } 

    public function newProductFeatureType()
    {
        $languages = Language::all();
        return view('dashboard.new_product_feature_type', compact('languages'));
    }

    public function actionNewProductFeatureType()
    {
        $feature_type = new ProductFeatureType;
        $feature_type->save();

        foreach($this->request->name as $language_id => $name)
        {
            $feature_type_language = new ProductFeatureTypeLanguage;
            $feature_type_language->language_id = $language_id;
            $feature_type_language->product_feature_type_id = $feature_type->id;
            $feature_type_language->name = $name;
            $feature_type_language->save();
        }

        return redirect()->route('product_feature_type');
    }

    public function editProductFeatureType()
    {
        $feature_type = ProductFeatureType::findOrFail($this->request->id);
        $languages = Language::all();

        return view('dashboard.edit_product_feature_type', compact('feature_type', 'languages'));
    }

    public function actionEditProductFeatureType()
    {
        $feature_type = ProductFeatureType::findOrFail($this->request->id);

        foreach($this->request->name as $language_id => $name)
        {
            $feature_type_info = $feature_type->infos()->where('language_id', $language_id)->firstOrNew(["product_feature_type_id" => $feature_type->id, "language_id" => $language_id]);
            $feature_type_info->name = $name;
            $feature_type_info->save();
        }

        return redirect()->route('product_feature_type');
    }

    public function removeProductFeatureType()
    {
        $feature_type = ProductFeatureType::findOrFail($this->request->id);
        $feature_type->delete();

        return redirect()->route('product_feature_type');
    }

    public function productFeatures()
    {
        $product_feature_type = ProductFeatureType::with(['product_features' => function($query){
            $query->with(['info' => function($query){
                $query->backOffice();
            }]);
        }])->findOrFail($this->request->id);

        return view('dashboard.product_features', compact('product_feature_type'));
    }

    public function newProductFeature()
    {
        $languages = Language::all();
        return view('dashboard.new_product_feature', compact('languages'));
    }

    public function actionNewProductFeature()
    {
        $feature_type = ProductFeatureType::findOrFail($this->request->id);

    	$feature = new ProductFeature;
    	$feature->product_feature_type_id = $feature_type->id;
    	$feature->save();

        foreach($this->request->feature_name as $language_id => $feature_name)
        {
            if(is_numeric($language_id))
            {
                $feature_language = new ProductFeatureLanguage;
                $feature_language->product_feature_id = $feature->id;
                $feature_language->language_id = $language_id;
                $feature_language->name = $feature_name;
                $feature_language->save();
            }
        }

    	foreach($this->request->option_name as $names)
        {
            $option = new ProductFeatureOption;
            $option->product_feature_id = $feature->id;
            $option->save();

            foreach($names as $language_id => $name)
            {
                if(is_numeric($language_id))
                {
                    $feature_option_language = new ProductFeatureOptionLanguage;
                    $feature_option_language->product_feature_option_id = $option->id;
                    $feature_option_language->language_id = $language_id;
                    $feature_option_language->name = $name;
                    $feature_option_language->save();
                }
            }
        }

    	return redirect()->route('product_features', $feature_type->id);
    }

    public function editProductFeature()
    {
        $product_feature = ProductFeature::with('infos')->with(['info' => function($query){
            $query->backOffice();
        }])->with(['options' => function($query){
            $query->with('infos')->with('products');
        }])->findOrFail($this->request->id);

        $languages = Language::all();

        return view('dashboard.edit_product_feature', compact('product_feature', 'languages'));
    }

    public function actionEditProductFeature()
    {
        $feature = ProductFeature::findOrFail($this->request->id);

        foreach($this->request->feature_name as $language_id => $name)
        {
            ProductFeatureLanguage::updateOrCreate(
                ['product_feature_id' => $feature->id, 'language_id' => $language_id],
                ['name' => $name ?? '']
            );
        }

        // foreach($this->request->option_name as $options)
        // {
        //     foreach($options as $language_id => $feature_options)
        //     {
        //         foreach($feature_options as $option_id => $name)
        //         {
        //             if($name)
        //             {
        //                 $product_feature_option = ProductFeatureOption::firstOrCreate(['product_feature_id' => $option_id]);

        //                 $feature_option_info = ProductFeatureOptionLanguage::updateOrCreate(
        //                     ['product_feature_option_id' => $product_feature_option->id, 'language_id' => $language_id],
        //                     ['name' => $name]
        //                 );
        //             }
        //         }
        //     }
        // }

        return redirect()->route('edit_product_feature', [$feature->product_feature_type->id, $feature->id]);
    }

    public function removeProductFeature()
    {
        $feature = ProductFeature::with(['infos', 'options'])->findOrFail($this->request->id);

        foreach($feature->options as $option)
        {
             foreach($option->infos as $info)
            {
                $info->delete();
            }

            $option->products()->detach();
            $option->delete();
        }

        foreach($feature->infos as $info)
        {
            $info->delete();
        }
        
        $feature->delete();

        $feature_type = $feature->product_feature_type;

        return redirect()->route('product_features', compact('feature_type'));
    }

    public function getFeatures()
    {
        $product_feature_type = ProductFeatureType::with(['product_features' => function($query){
            $query->with(['info' => function($query){
                $query->backOffice();
            }])->with(['options.info' => function($query){
                $query->backOffice();
            }]);
        }])->with(['info' => function($query){
            $query->backOffice();
        }])->findOrFail($this->request->id);

        $product_feature_type->name = $product_feature_type->info->name;

        foreach($product_feature_type->product_features as $feature)
        {
            $feature->name = $feature->info->name;

            foreach($feature->options as $option)
            {
                $option->name = @$option->info->name;
            }
        }

        return $product_feature_type;
    }

    public function transferOption()
    {
        $product_feature_types = ProductFeatureType::all();

        $feature_option = ProductFeatureOption::with(['product_feature' => function($query){
            $query->with(['info' => function($query){
                $query->backOffice();
            }])->with(['product_feature_type.info' => function($query){
                $query->backOffice();
            }]);
        }])->with(['info' => function($query){
            $query->backOffice();
        }])->find($this->request->id);

        return view('dashboard.product_feature_option_transfer', compact('feature_option', 'product_feature_types'));
    }

    public function actionTransferOption()
    {
        $new_feature_option = ProductFeatureOption::findOrFail($this->request->feature_option);

        $feature_option = ProductFeatureOption::with(['product_feature.product_feature_type', 'products'])->findOrFail($this->request->id);

        foreach($feature_option->products as $product)
        {
            $product->product_feature_options()->detach($feature_option);
            $product->syncFeatureOption($new_feature_option);
        }
        
        return redirect()->route('edit_product_feature', [$feature_option->product_feature->product_feature_type->id, $feature_option->product_feature->id]);
    }

    public function removeProductFeatureOption()
    {
        $feature_option = ProductFeatureOption::findOrFail($this->request->id);
        $feature_option->delete();

        return array("status" => true);
    }
}
