<?php

namespace App\Observers;

use App\Jobs\UpdateMotorProduct;
use App\WebHook;


class MotorProductObserver
{
    public function created($model)
    {
        $this->dispatch($model);
    }

    public function updated($model)
    {
        $this->dispatch($model, 'updated');
    }

    public function deleted($model)
    {
        $this->dispatch($model, 'deleted');
    }

    public function dispatch($model, $event = 'created'){
        foreach(WebHook::all() as $webhook){
            dispatch(new UpdateMotorProduct($model, $webhook, $event));
        }
    }
}