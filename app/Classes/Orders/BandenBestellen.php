<?php
namespace App\Classes\Orders;

use App\RewardProgram;
use App\Product;

class BandenBestellen
{
    public $reward_program_order;

    public function __construct($request, $order)
    {
        $this->bem_cost = 1.3;
        $this->marketing_fee = 5;

        $this->request = $request;
        $this->order = $order;

        $this->rewardProgram();

        if($this->reward_program_order)
        {
            $this->rewardProducts();
            $this->addAssemblyCostToOrder();
            $this->addAssemblyCostToReward();
            $this->rewardBEM();
            $this->rewardFee();
        }
    }

    public function rewardProgram()
    {
        $reward_program = RewardProgram::getShop($this->order->shop_id)->active()->first();

        if($reward_program)
        {
            $this->reward_program_order = $reward_program->orders()->create([
                'order_id'      => $this->order->id,
                'company'       => $this->request->company,
                'iban'          => $this->request->iban,
                'total_price'   => 0
            ]);
        }
    }

    public function addAssemblyCostToOrder()
    {
        $services = $this->request->cart['assembly'];

        unset($services['hasRunflat']);
        unset($services['total_assembly']);
        
        foreach($services as $service => $price)
        {
            $this->order->lines()->create([
                "name"          => $service,
                "qty"           => 1,
                "price"         => $price
            ]);
        }

        $this->order->total_price = $this->order->total_price + $this->request->cart['assembly']['total_assembly'];
        $this->order->save();
    }

    public function addAssemblyCostToReward()
    {
        $services = $this->request->cart['assembly'];

        unset($services['hasRunflat']);
        unset($services['total_assembly']);

        foreach($services as $service => $price)
        {
            $this->reward_program_order->lines()->create([
                "name"          => $service,
                "qty"           => 1,
                "price"         => $price,
                "credit"        => false
            ]);
        }

        $this->reward_program_order->total_price += $this->request->cart['assembly']['total_assembly'];
        $this->reward_program_order->save();

        return $this;
    }

    public function rewardBEM()
    {
        foreach($this->order->lines()->where('name', 'BEM (Consumer)')->get() as $bem)
        {
            $reward_line = $this->reward_program_order->lines()->create([
                "name"          => 'BEM',
                "qty"           => $bem->qty,
                "price"         => ($bem->price - $this->bem_cost) * $bem->qty,
                "credit"        => false
            ]);

            $this->reward_program_order->total_price += $reward_line->price;
            $this->reward_program_order->save();
        }
    }

    public function rewardProducts()
    {
        foreach($this->order->lines()->whereNotNull('product_id')->get() as $order_product)
        {
            $product = Product::with(['shop_price' => function($query){
                $query->where('shop_id', $this->request->shop->id);
            }])->with(['shop_info' => function($query){
                $query->where('shop_id', $this->request->shop->id);
            }])->findOrFail($order_product->product_id);

            $reward_line = $this->reward_program_order->lines()->create([
                "product_id"    => $product->id,
                "name"          => $product->shop_info->name,
                "qty"           => $order_product->qty,
                "price"         => $product->shop_price->retail_price - $product->av_price(),
                "credit"        => false
            ]);

            $this->reward_program_order->total_price += $reward_line->price;
            $this->reward_program_order->save();
        }

        return $this;
    }

    public function rewardFee()
    {
        $reward_line = $this->reward_program_order->lines()->create([
            "name"          => 'Marketing',
            "qty"           => 1,
            "price"         => $this->marketing_fee,
            "credit"        => true
        ]);

        $this->reward_program_order->total_price -= $reward_line->price;
        $this->reward_program_order->save();
    }
}
