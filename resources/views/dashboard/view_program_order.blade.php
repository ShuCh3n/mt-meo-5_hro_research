@extends('dashboard.layout.main')

@section('content')
	<div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>{{ trans('pages.inquiry_detail_header') }}</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('home') }}">{{ trans('menu.home') }}</a>
                </li>
                <li>
                    <a href="">{{ trans('menu.inquiries') }}</a>
                </li>
                <li class="active">
                    <strong>{{ trans('page.inquiry') }}</strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="ibox-content p-xl">
            <div class="row">
                <div class="col-sm-6 ">
                    <h4 class="text-navy">{{ $program_order->order->reference }}</h4>

                    <address>
                        <strong>{{ $program_order->company }}</strong><br>
                        {{ $program_order->order->street }} {{ $program_order->order->housenumber }}<br>
                        {{ $program_order->order->city }} {{ $program_order->order->zipcode }}<br>
                        {!! flag_icon($program_order->order->country->icon) !!} {{ $program_order->order->country->name }}<br>
                        <br>
                    </address>

                    <p>
                        <span><strong>IBAN:</strong> {{ $program_order->iban }}</span>
                    </p>
                    <p>
                        <span><strong>Date:</strong> {{ $program_order->created_at->format('d M Y') }}</span>
                    </p>
                </div>

                <div class="col-sm-6 text-right">
                    <h5>From:</h5>
                    <address>
                        <strong>{{ $program_order->reward_program->shop->name }}</strong>
                    </address>
                </div>

            </div>

            <div class="table-responsive m-t">
                <table class="table invoice-table">
                    <thead>
                        <tr>
                            <th>Item List</th>
                            <th>Quantity</th>
                            <th>Credit</th>
                            <th>Debit</th>
                        </tr>
                    </thead>
                    <tbody>
                         @foreach($program_order->lines as $line)
                            <tr>
                                <td>
                                    <div><strong>
                                        @if($line->product_id)
                                            <a href="{{ route('view_product', $line->product_id) }}">{{ $line->name }}</a>
                                        @else
                                            {{ $line->name }}
                                        @endif
                                    </strong></div>
                                </td>
                                <td>{{ $line->qty }}</td>
                                <td class="text-danger">{{ ($line->credit)? '- ' .price($line->price) : '' }}</td>
                                <td>{{ (!$line->credit)? price($line->price) : '' }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div><!-- /table-responsive -->

            <table class="table invoice-total">
                <tbody>
                    <tr>
                        <td><strong>TOTAL :</strong></td>
                        <td>{{ price($program_order->total_price) }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
@stop