<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Product;
use App\ShopProduct;
use App\Classes\ShopProductPrice;
use App\Classes\ProductFilters;
use App\CatalogPriceRule;
use App\Brand;
use Illuminate\Database\Eloquent\Builder;

class ProductController extends Controller
{
	public function grabProductFilters()
    {
        $this->filters = null;
        $this->paginate = 30;
        $this->sort = null;
        $this->sort_type = null;
        $this->current_page = $this->request->page ?? 1;

        if($this->request->filters)
        {
            $this->filters = $this->request->filters;
        }

        if($this->request->paginate)
        {
            $this->paginate = $this->request->paginate;
        }

        if($this->request->sort)
        {
            $this->sort = $this->request->sort;
            $this->sort_type = $this->request->sort_type;
        }
    }

    public function productsProfiles(){
        $products = Product::featureByName('Profile',2)->with('shops')->whereHas('product_feature_options')->paginate($this->request->paginate);
        foreach ($products as $product){
            $product->images = $product->allimages($this->request->shop->id);
        }

        return $products;

    }

    public function products($displayAll = false)
    {
        $this->grabProductFilters();
        $products = $this->request->shop->shop_products()
            ->join('product_shop_prices as price', function ($join) {
                $join->on('price.product_id', 'shop_products.product_id');
                $join->where('deleted_at', '=', null);
                $join->take(1);
            })
            ->leftJoin('products as prod', 'prod.id', 'shop_products.product_id')
            ->leftJoin('brands as brand', 'brand.id', 'prod.brand_id')
            ->select(['shop_products.*', 'price.retail_price as price', 'brand.name as brand'])
            ->whereHas('product', function($query){
            $query->featureInfo($this->request->language->id)
                ->filter($this->filters)
                ->shopActive($this->request->shop->id)
                ->productFilter($this->request->shop->id);
        })->with(['product' => function($query){
            $query->productFilter($this->request->shop->id)->with('product_price')->with('brand')->with(['product_feature_options' => function($q){
                $q->with(['product_feature' => function($qq){
                    $qq->with('infos');
                    $qq->with('info');
                }])->with(['infos', 'info']);
            }])->with('shop_infos');
        }]);

        if($this->sort){
            $products->orderBy($this->sort, $this->sort_type??'desc');
        }

        if($displayAll)
        {
            return $products->get();
        }

        $products = $products->paginate($this->paginate);

        foreach($products as $shop_product)
        {
            $product = $shop_product->product;

            $product->prices = new ShopProductPrice($product, $this->request->shop);

            if($product->brand)
            {
                if($product->brand->image->first())
                {
                    $product->logo = [
                        'small' => env('CDN_URI') . '/images/brands/' . $product->brand->image->first()->filename . '_small.png',
                        'medium' => env('CDN_URI') . '/images/brands/' . $product->brand->image->first()->filename . '_medium.png',
                        'large' => env('CDN_URI') . '/images/brands/' . $product->brand->image->first()->filename . '_large.png'
                    ];
                }
                else
                {
                    $product->logo = [];
                }
            }
            else
            {
                $product->logo = [];
            }

            $imagesSource = $product->allimages($this->request->shop->id);
            $images = [];

            foreach($imagesSource as $image)
            {
                $images[] = array(
                    'img' => env('CDN_URI') . '/images/' . $image->filename . '.png',
                    'thumb' => env('CDN_URI') . '/images/' . $image->filename . '_thumb.png'
                );
            }

            $product->images = $images;
        }

        return $products;
    }

    public function productsFilters()
    {
        $products = array();

        $this->grabProductFilters();
        $prod = $this->request->shop->shop_products()->getRelated()->join('products', 'product_id', '=', 'products.id')->select('products.*');
        $products = $prod->getRelation('product');
        $filters = new ProductFilters($products->active()->ProductFilter($this->request->shop->id)->Filter($this->filters)->shopInfo($this->request->shop->id, $this->request->language->id)->shopActive($this->request->shop->id), $this->request->language->id, $this->request->shop->id);

        return array("filters" => $filters);
    }

    public function getProduct()
    {
        $shop_product = ShopProduct::where('shop_id', $this->request->shop->id)
                        ->where('product_id', $this->request->id)
                        ->with(['product' => function($query){
                            $query->shopInfo($this->request->shop->id, $this->request->language->id)->featureInfo($this->request->language->id);
                            $query->with('images');
                        }])
                        ->firstOrFail();

        $shop_product->product->prices = new ShopProductPrice($shop_product->product, $this->request->shop);

        $imagesSource = $shop_product->product->allimages($this->request->shop->id);

        if($shop_product->product->brand && $shop_product->product->brand->image->count() > 0)
        {
			$shop_product->product->logo = [
				'small' => env('CDN_URI') . '/images/brands/' . $shop_product->product->brand->image->first()->filename . '_small.png',
				'medium' => env('CDN_URI') . '/images/brands/' . $shop_product->product->brand->image->first()->filename . '_medium.png',
				'large' => env('CDN_URI') . '/images/brands/' . $shop_product->product->brand->image->first()->filename . '_large.png'
			];
		}
        else
        {
			$shop_product->product->logo = [];
		}

        $images = [];

        foreach($imagesSource as $image){
            $images[] = array(
                'img' => env('CDN_URI') . '/images/' . $image->filename . '.png',
                'thumb' => env('CDN_URI') . '/images/' . $image->filename . '_thumb.png'
            );
        }
        $shop_product->images = $images;

        $shop_product->product->category_id = $shop_product->product->categories->where('shop_id', $this->request->shop->id)->first()?$shop_product->product->categories->where('shop_id', $this->request->shop->id)->first()->id:0;

        return $shop_product;
    }


    public function getFeaturedProduct()
    {

    }

    public function getProductOffers()
    {
        $products = array();

        $rules = CatalogPriceRule::whereHas('shops', function($query){
            $query->where('id', $this->request->shop->id);
        })->with(['products' => function($query){
            $query->with('images')->ProductFilter($this->request->shop->id)->shopInfo($this->request->shop->id, $this->request->language->id)->featureInfo($this->request->language->id)->shopActive($this->request->shop->id);
        }])->get();

        foreach($rules as $rule)
        {
            foreach($rule->products as $product)
            {
                $imagesSource = $product->allimages($this->request->shop->id);
                if($product->brand && $product->brand->image->count() > 0)
                {
                    $product->logo = [
                        'small' => env('CDN_URI') . '/images/brands/' . $product->brand->image->first()->filename . '_small.png',
                        'medium' => env('CDN_URI') . '/images/brands/' . $product->brand->image->first()->filename . '_medium.png',
                        'large' => env('CDN_URI') . '/images/brands/' . $product->brand->image->first()->filename . '_large.png'
                    ];
                }
                else
                {
                    $product->logo = [];
                }

                $images = [];

                foreach($imagesSource as $image){
                    $images[] = array(
                        'img' => env('CDN_URI') . '/images/' . $image->filename . '.png',
                        'thumb' => env('CDN_URI') . '/images/' . $image->filename . '_thumb.png'
                    );
                }
                unset($product->images);
                $product->images = $images;

                $product->prices = new ShopProductPrice($product, $this->request->shop);

                $products[] = $product;

            }
        }
        return $products;
    }

    public function searchProduct()
    {
        $paginate = 32;

        $filter = array();
        $brand_array = array();
        $skus = array();

        $search_query = $this->request["query"];
        $brands = $this->request["brands"];
        $categories_ids = $this->request["category_ids"];

        if($this->request["features"])
        {
           $filter["options"] = $this->request["features"];
           if(!is_array($filter["options"])){
           	$filter["options"] = json_decode($filter["options"]);
           }
        }

        if($this->request["prices"])
        {
           $filter["price"] = $this->request["prices"];
        }

        $sort = false;

        if($this->request->sort)
        {
            $sort = $this->request->sort;
            $sort_type = $this->request->sort_type;
        }

        if($this->request["skus"])
        {
            $skus = $this->request["skus"];
        }

        if($this->request["paginate"] && $this->request["paginate"] <= 50)
        {
            $paginate = $this->request->paginate;
        }

        if($brands)
        {
            if(!is_array($brands)){
                $brands = json_decode($brands);
            }

            foreach($brands as $brand)
            {
                $get_brand = Brand::getBrand($brand);

                if($get_brand)
                {
                    $brand_array[] = $get_brand->id;
                }
            }
        }

        $result = $this->request->shop->shop_products()->whereHas('product', function($query) use ($brand_array, $search_query, $filter, $categories_ids, $skus){
            $query->shopActive($this->request->shop->id)->ProductFilter($this->request->shop->id);

            if(!empty($brand_array))
            {
                $query->whereIn('brand_id', $brand_array);
            }

            if($search_query)
            {
                $query->whereHas('shop_infos', function($q) use ($search_query){
                    $q->where('shop_id', $this->request->shop->id)->where('language_id', $this->request->language->id)->where('name', 'like', '%' . $search_query . '%');
                });
            }

            if(!empty($filter))
            {
                $query->filter($filter);
            }

            if(!empty($categories_ids))
            {
                if(!is_array($categories_ids)){
                    $categories_ids = json_decode($categories_ids);
                }
                foreach($categories_ids as $category_id)
                {
                    $query->whereHas('categories', function($q) use ($category_id){
                        $q->where('id', $category_id);
                    });
                }
            }

            if(!empty($skus))
            {
                $query->whereIn('sku', $skus);
            }

        })->with(['product' => function($query){
            $query->with(['product_price', 'product_stocks', 'categories'])->shopInfo($this->request->shop->id, $this->request->language->id)->featureInfo($this->request->language->id);
         }])->leftJoin('product_shop_prices', function ($join){
             $join->on('shop_products.product_id', 'product_shop_prices.product_id')->where('product_shop_prices.shop_id', $this->request->shop->id)->where('product_shop_prices.deleted_at','=', null);
         })->select('shop_products.*');

        if($sort)
        {
            if($sort_type == "asc")
            {
                $result = $result->orderBy($sort, $sort_type);
            }

            if($sort_type == "desc")
            {
                $result = $result->orderBy($sort, $sort_type);
            }
        }
        //dd($result->toSql());
        $result = $result->paginate($paginate);

        foreach($result as $r)
        {
            $r->product->prices = new ShopProductPrice($r->product, $this->request->shop);
            $r->product->logo = [];
            $r->product->total_stocks = $r->product->total_stocks();

            if(isset($r->product->brand) && $r->product->brand->image->first())
            {
                $r->product->logo = [
                    'small' => env('CDN_URI') . '/images/brands/' . $r->product->brand->image->first()->filename . '_small.png',
                    'medium' => env('CDN_URI') . '/images/brands/' . $r->product->brand->image->first()->filename . '_medium.png',
                    'large' => env('CDN_URI') . '/images/brands/' . $r->product->brand->image->first()->filename . '_large.png'
                ];
            }

            $imagesSource = $r->product->allimages($this->request->shop->id);

            $images = [];

            foreach($imagesSource as $image){
                $images[] = array(
                    'img' => env('CDN_URI') . '/images/' . $image->filename . '.png',
                    'thumb' => env('CDN_URI') . '/images/' . $image->filename . '_thumb.png'
                );
            }

            $r->product->images = $images;
        }

        return $result;
    }

    public function getMostSoldProducts()
    {
        $products = ShopProduct::whereHas('product', function($query){
            $query->active()->shopActive($this->request->shop->id)->ProductFilter($this->request->shop->id)->whereHas('order_lines.order.shop', function($query){
                $query->where('id', $this->request->shop->id);
            });
        })->with(['product' => function($query){
            $query->shopInfo($this->request->shop->id, $this->request->language->id)->with('images')->with('product_price')->with('brand')->with(['product_feature_options' => function($query){
                $query->with(['product_feature' => function($query){
                    $query->with('infos');
                }])->with('infos');
            }]);
        }])->get();


        foreach($products as $shop_product)
        {
            $imagesSource = $shop_product->product->allimages($this->request->shop->id);
            if($shop_product->product->brand && $shop_product->product->brand->image->count() > 0)
            {
                $shop_product->product->logo = [
                    'small' => env('CDN_URI') . '/images/brands/' . $shop_product->product->brand->image->first()->filename . '_small.png',
                    'medium' => env('CDN_URI') . '/images/brands/' . $shop_product->product->brand->image->first()->filename . '_medium.png',
                    'large' => env('CDN_URI') . '/images/brands/' . $shop_product->product->brand->image->first()->filename . '_large.png'
                ];
            }
            else
            {
                $shop_product->product->logo = [];
            }

            $images = [];

            foreach($imagesSource as $image){
                $images[] = array(
                    'img' => env('CDN_URI') . '/images/' . $image->filename . '.png',
                    'thumb' => env('CDN_URI') . '/images/' . $image->filename . '_thumb.png'
                );
            }
            unset($shop_product->product->images);
            $shop_product->product->images = $images;

            $shop_product->product->prices = new ShopProductPrice($shop_product->product, $this->request->shop);
        }

        return $products;
    }
}