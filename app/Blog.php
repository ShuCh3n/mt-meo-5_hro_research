<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Blog extends Model
{
    use SoftDeletes;
    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];
    protected $dates = ['published_at'];
    
    public function shop()
    {
        return $this->belongsTo(Shop::class);
    }

    public function blog_category()
    {
        return $this->belongsTo(BlogCategory::class);
    }

    public function infos()
    {
        return $this->hasMany(BlogInfo::class);
    }

    public function info()
    {
        return $this->hasOne(BlogInfo::class);
    }

    public function scopePost($query, $id)
    {
        return $query->where('id', $id);
    }

    public function scopePublished($query)
    {
        return $query->where('published_at', '<', carbon()->now());
    }

    public function scopeActive($query)
    {
        return $query->where('active', true);
    }
}
