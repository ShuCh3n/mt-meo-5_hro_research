import databaseMinion
import CSVMinion

from threading import Thread
from sqlalchemy.orm import sessionmaker
from sqlalchemy import *
import pandas as pd

class Tyre24(Thread):
	def __init__(self):
		Thread.__init__(self)

		self.database_minion = databaseMinion.DatabaseMinion()
		self.csv_minion = CSVMinion.CSVMinion('files')

		self.seperator = "|"
		self.filename = "T24_export.csv"
		self.platform = self.database_minion.getPlatform('Tyre24')

		self.start()

	def run(self):
		self.data = self.csv_minion.getData(self.seperator, self.filename)

		for index, row in self.data.iterrows():
			product = self.database_minion.findProduct(self.platform, [row['manufacturer_number']], row['id'])

			if product:
				self.database_minion.insertPrice(self.platform, product, row['price'], row['id'])
				self.database_minion.insertStock(self.platform.name, product, row['availability'])
				print(product.id)

class O7zr(Thread):
	def __init__(self):
		Thread.__init__(self)

		self.database_minion = databaseMinion.DatabaseMinion()
		self.csv_minion = CSVMinion.CSVMinion('files')

		self.seperator = "="
		self.filename = "07ZR_export.csv"
		self.platform = self.database_minion.getPlatform('07zr')

		self.start()

	def run(self):
		self.data = self.csv_minion.getData(self.seperator, self.filename)

		for index, row in self.data.iterrows():
			product = self.database_minion.findProduct(self.platform, [row['ARTICLE CODE;'].replace(";", ""), row['EAN;'].replace(";", "")])

			if product:
				self.database_minion.insertPrice(self.platform, product, row['BEST PRICE;'].replace(";", ""), row['ARTICLE CODE;'].replace(";", ""))
				self.database_minion.insertStock(self.platform.name, product, row['TIRES QTY BEST PRICE;'].replace(";", "").replace("> ", ""))
				print(product.id)
			
class Allopneus(Thread):
	def __init__(self):
		Thread.__init__(self)

		self.database_minion = databaseMinion.DatabaseMinion()
		self.csv_minion = CSVMinion.CSVMinion('files')

		self.seperator = ";"
		self.filename = "ALLOPNEUS_export.csv"
		self.platform = self.database_minion.getPlatform('Allopneus')

		self.start()

	def run(self):
		self.data = self.csv_minion.getData(self.seperator, self.filename)

		for index, row in self.data.iterrows():
			product = self.database_minion.findProduct(self.platform, [row['CLE_UNIQUE'], row['EAN_CODE']])

			if product:
				self.database_minion.insertPrice(self.platform, product, row['PRIX_ACHAT'], row['CLE_UNIQUE'])
				self.database_minion.insertStock(self.platform.name, product, row['STOCK'])
				print(product.id)

class Boxenstopp(Thread):
	def __init__(self):
		Thread.__init__(self)

		self.database_minion = databaseMinion.DatabaseMinion()
		self.csv_minion = CSVMinion.CSVMinion('files')

		self.seperator = ";"
		self.filename = "BOXENSTOP_export.csv"
		self.platform = self.database_minion.getPlatform('Boxstop')

		self.start()

	def run(self):
		self.data = self.csv_minion.getData(self.seperator, self.filename)

		for index, row in self.data.iterrows():
			product = self.database_minion.findProduct(self.platform, [row['Item_No'], row['UPC']])

			if product:
				self.database_minion.insertPrice(self.platform, product, row['Price1'].replace(',', '.'), row['Item_No'])
				self.database_minion.insertStock(self.platform.name, product, row['Stock'])
				print(product.id)
			
class Deldo(Thread):
	def __init__(self):
		Thread.__init__(self)

		self.database_minion = databaseMinion.DatabaseMinion()
		self.csv_minion = CSVMinion.CSVMinion('files')

		self.seperator = ";"
		self.filename = "DELDO_export.csv"
		self.platform = self.database_minion.getPlatform('Deldo')

		self.start()

	def run(self):
		self.data = self.csv_minion.getData(self.seperator, self.filename)

		for index, row in self.data.iterrows():
			product = self.database_minion.findProduct(self.platform, [row['EAN'], row['Article']])
			
			if product:
				self.database_minion.insertPrice(self.platform, product, row['Price'], row['Article'])
				self.database_minion.insertStock(self.platform.name, product, row['Stock'])
				print(product.id)

class Hanse(Thread):
	def __init__(self):
		Thread.__init__(self)

		self.database_minion = databaseMinion.DatabaseMinion()
		self.csv_minion = CSVMinion.CSVMinion('files')

		self.seperator = ";"
		self.filename = "HANSE_export.csv"
		self.platform = self.database_minion.getPlatform('Hanse')

		self.start()

	def run(self):
		self.data = self.csv_minion.getData(self.seperator, self.filename)

		for index, row in self.data.iterrows():
			product = self.database_minion.findProduct(self.platform, [row['EAN'], row['Hersteller_ArtNr']])

			if product:
				self.database_minion.insertPrice(self.platform, product, row['Netto'].replace(',', '.'), row['Hersteller_ArtNr'])
				self.database_minion.insertStock(self.platform.name, product, int(row['Bestand'].replace(',', '')) / 1000)
				print(product.id)

class Hohl(Thread):
	def __init__(self):
		Thread.__init__(self)

		self.database_minion = databaseMinion.DatabaseMinion()
		self.csv_minion = CSVMinion.CSVMinion('files')

		self.seperator = ";"
		self.filename = "HOHL_export.csv"
		self.platform = self.database_minion.getPlatform('Hohl')

		self.start()

	def run(self):
		self.data = self.csv_minion.getData(self.seperator, self.filename)

		for index, row in self.data.iterrows():
			if not pd.isnull(row['EAN']):
				product = self.database_minion.findProduct(self.platform, [int(row['EAN']), row['ARTNR']])

				if product:
					self.database_minion.insertPrice(self.platform, product, row['PREIS'], row['ARTNR'])
					self.database_minion.insertStock(self.platform.name, product, row['BESTAND'])
					print(product.id)

class Interpneus(Thread):
	def __init__(self):
		Thread.__init__(self)

		self.database_minion = databaseMinion.DatabaseMinion()
		self.csv_minion = CSVMinion.CSVMinion('files')

		self.seperator = "|"
		self.filename = "INTERPNEUS_export.csv"
		self.platform = self.database_minion.getPlatform('Interpneus')

		self.start()

	def run(self):
		self.data = self.csv_minion.getData(self.seperator, self.filename)

		for index, row in self.data.iterrows():
			if not pd.isnull(row[1]):
				product = self.database_minion.findProduct(self.platform, [int(row[1]), row[2]])

				if product:
					self.database_minion.insertPrice(self.platform, product, row[4].replace(',', '.'), row[2])
					self.database_minion.insertStock(self.platform.name, product, int(row[3].replace(',00', '')))
					print(product.id)

class Krieg(Thread):
	def __init__(self):
		Thread.__init__(self)

		self.database_minion = databaseMinion.DatabaseMinion()
		self.csv_minion = CSVMinion.CSVMinion('files')

		self.seperator = ";"
		self.filename = "KRIEG_export.csv"
		self.platform = self.database_minion.getPlatform('Krieg')

		self.start()

	def run(self):
		self.data = self.csv_minion.getData(self.seperator, self.filename)

		for index, row in self.data.iterrows():
			product = self.database_minion.findProduct(self.platform, [row[4]])

			if product:
				self.database_minion.insertPrice(self.platform, product, row[9].replace(',', '.'), row[5])
				self.database_minion.insertStock(self.platform.name, product, row[7].replace(',000', ''))
				print(product.id)

class Krupp(Thread):
	def __init__(self):
		Thread.__init__(self)

		self.database_minion = databaseMinion.DatabaseMinion()
		self.csv_minion = CSVMinion.CSVMinion('files')

		self.seperator = ";"
		self.filename = "KRUPP_export.csv"
		self.platform = self.database_minion.getPlatform('Krupp')

		self.start()

	def run(self):

		self.data = self.csv_minion.getData(self.seperator, self.filename)

		for index, row in self.data.iterrows():
			product = self.database_minion.findProduct(self.platform, [row[13].strip(), row[14]])

			if product:
				self.database_minion.insertPrice(self.platform, product, row[4], row[14])
				self.database_minion.insertStock(self.platform.name, product, row[27])
				print(product.id)

class Wolf(Thread):
	def __init__(self):
		Thread.__init__(self)

		self.database_minion = databaseMinion.DatabaseMinion()
		self.csv_minion = CSVMinion.CSVMinion('files')

		self.seperator = ";"
		self.filename = "WOLF_export.csv"
		self.platform = self.database_minion.getPlatform('Wolf')

		self.start()

	def run(self):
		self.data = self.csv_minion.getData(self.seperator, self.filename, 1)

		for index, row in self.data.iterrows():
			product = self.database_minion.findProduct(self.platform, [row[1], row[2]])

			if product:
				self.database_minion.insertPrice(self.platform, product, row[13], row[1])
				self.database_minion.insertStock(self.platform.name, product, row[14])
				print(product.id)

class Seng(Thread):
	def __init__(self):
		Thread.__init__(self)

		self.database_minion = databaseMinion.DatabaseMinion()
		self.csv_minion = CSVMinion.CSVMinion('files')

		self.seperator = ";"
		self.filename = "SENG_export.csv"
		self.platform = self.database_minion.getPlatform('Seng')

		self.start()

	def run(self):
		self.data = self.csv_minion.getData(self.seperator, self.filename)

		for index, row in self.data.iterrows():
			product = self.database_minion.findProduct(self.platform, [row['ArtikelNr. (eindeutig)'], row['EAN']])

			if product:
				self.database_minion.insertPrice(self.platform, product, row['Preis'].replace(',', '.'), row['ArtikelNr. (eindeutig)'])
				self.database_minion.insertStock(self.platform.name, product, row['Bestand'])
				print(product.id)

class ShTyre(Thread):
	def __init__(self):
		Thread.__init__(self)

		self.database_minion = databaseMinion.DatabaseMinion()
		self.csv_minion = CSVMinion.CSVMinion('files')

		self.seperator = ";"
		self.filename = "S_H_export.csv"
		self.platform = self.database_minion.getPlatform('Shtyre')

		self.start()

	def run(self):
		self.data = self.csv_minion.getData(self.seperator, self.filename)

		for index, row in self.data.iterrows():
			product = self.database_minion.findProduct(self.platform, [row['ProductEANCode'], row['ProductCode']])

			if product:
				self.database_minion.insertPrice(self.platform, product, row['PricePP'].replace(',', '.'), row['ProductCode'])
				self.database_minion.insertStock(self.platform.name, product, row['Stock'])
				print(product.id)

class Straub(Thread):
	def __init__(self):
		Thread.__init__(self)

		self.database_minion = databaseMinion.DatabaseMinion()
		self.csv_minion = CSVMinion.CSVMinion('files')

		self.seperator = ";"
		self.filename = "STRAUB_export.csv"
		self.platform = self.database_minion.getPlatform('Straub')

		self.start()

	def run(self):
		self.data = self.csv_minion.getData(self.seperator, self.filename)

		for index, row in self.data.iterrows():
			product = self.database_minion.findProduct(self.platform, [row[11], row[12]])

			if product:
				self.database_minion.insertPrice(self.platform, product, row[25], row[13])
				self.database_minion.insertStock(self.platform.name, product, row[4])
				print(product.id)

class TyreDistribution(Thread):
	def __init__(self):
		Thread.__init__(self)

		self.database_minion = databaseMinion.DatabaseMinion()
		self.csv_minion = CSVMinion.CSVMinion('files')

		self.seperator = ";"
		self.filename = "TYREDIS_export.csv"
		self.platform = self.database_minion.getPlatform('TyreDistribution')

		self.start()

	def run(self):
		self.data = self.csv_minion.getData(self.seperator, self.filename)

		for index, row in self.data.iterrows():
			product = self.database_minion.findProduct(self.platform, [row['Artikelnr. '], row['Barcode ']])

			if product:
				self.database_minion.insertPrice(self.platform, product, row['Bruto Prijs EUR'], row['Artikelnr. '])
				self.database_minion.insertStock(self.platform.name, product, row['Stock '])
				print(product.id)

class MuellerHammelburg(Thread):
	def __init__(self):
		Thread.__init__(self)

		self.database_minion = databaseMinion.DatabaseMinion()
		self.csv_minion = CSVMinion.CSVMinion('files')

		self.seperator = ";"
		self.filename = "MUELLERH_export.csv"
		self.platform = self.database_minion.getPlatform('MuellerHammelburg')

		self.start()

	def run(self):
		self.data = self.csv_minion.getData(self.seperator, self.filename)

		for index, row in self.data.iterrows():
			product = self.database_minion.findProduct(self.platform, [row[0], row[1], row[2]])

			if product:
				self.database_minion.insertPrice(self.platform, product, row[29], row[0])
				self.database_minion.insertStock(self.platform.name, product, row[29])
				print(product.id)