<?php

namespace App\Http\Controllers\Classes;
use App\Product;
use App\Platform;
use App\PlatformProductPrice;

class iBox
{
    public function __construct($request)
    {
        $this->request = $request;
    }

    public function product()
    {
        $product = new Product;
        $platform = Platform::firstOrCreate([
            'name'  => title_case($this->request->platform),
            'b2b'   => false
        ]);

        if($this->request->id)
        {
            $product = $product->orWhere('id', $this->request->id);
        }

        if($this->request->sku)
        {
            $product = $product->orWhere('sku', $this->request->sku);
        }

        if($this->request->ean)
        {
            $product = $product->orWhere('ean', $this->request->ean);
        }

        if($this->request->upc)
        {
            $product = $product->orWhere('upc', $this->request->upc);
        }

        $product = $product->first();

        if($product)
        {
            $product->platforms()->syncWithoutDetaching([$platform->id => ['article_code' => $this->request->article_code]]);

            PlatformProductPrice::create([
                "platform_id"   => $platform->id,
                "product_id"    => $product->id,
                "price"         => $this->request->price
            ]);

            return array("status" => true);
        }

        return array("status" => false, "message" => "Product not found");
    }
}
