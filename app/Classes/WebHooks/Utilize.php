<?php
namespace App\Classes\WebHooks;

class Utilize extends WebHook
{
    public function doRequest()
    {
        switch ($this->event) {
            case 'created':
                $method = 'POST';
                break;
            case 'updated':
                $method = 'PUT';
                break;
            case 'deleted':
                $method = 'DELETE';
                break;
        }
        $body =  ['body' => $this->template];
        switch (class_basename($this->model)) {
            case 'CategoryMotorProductVehicleYear':
                $suffix = 'categoryMotorProductVehicleYear';
                break;
             
            default:
                # code...
                break;
        }
        $this->setUrl($this->webhook->url .$suffix);
        if ($method != 'POST') {
            $this->setUrl($this->webhook->url . $suffix . '/' . $this->model->id);
        }
         
        return $this->request->request($method, $this->url, $body);
    }

    public function format($template = null)
    {
        switch ($template) {
            case 'product':
                return collect([
                    "sku" => (string) $this->model->sku,
                    "name" => (string) $this->model->info->name,
                    "reviews_allowed"=> "false",
                    "price" => (string) $this->model->prices->retail_price,
                    "regular_price" => (string) $this->model->prices->retail_price,
                    "manage_stock" => (string) true,
                    "short_description" => (string) $this->model->info->short_description,
                    "description" => (string) $this->model->info->description,
                    "stock_quantity" => (string) $this->model->total_stocks(),
                ]);
                break;
            case 'motor':
                return collect([
                    "sku" => (string) $this->model->code,
                    "brand" => (string) $this->model->vehicle_brand->name,
                    "vehicle_name" => (string) $this->model->name,
                    "vehicle_type" => (string) $this->model->type,
                    "build_year_from" => (string) $this->model->vehicle_years->min('year'),
                    "build_year_to" => (string) $this->model->vehicle_years->max('year'),
                    "cc" => (string) $this->model->cc
                ]);
                break;
            case 'motor_product':
                return collect([
                    "avSku" => $this->model->product->sku,
                    "categoryId" => $this->model->category_id,
                    "id" => $this->model->id,
                    "minQty" => $this->model->min_qty,
                    "month" => $this->model->month,
                    "productId" => $this->model->product_id,
                    "sku" => $this->model->sku,
                    "vehicleYearId" => $this->model->vehicle_year_id
                ]);
                break;
        }
        return $this->model;
    }
}