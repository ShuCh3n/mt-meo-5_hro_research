@extends('dashboard.layout.main')

@section('content')
	<div class="wrapper wrapper-content animated fadeInRight ecommerce">
            <div class="ibox-content m-b-sm border-bottom">
                <div class="row">
                    <div class="col-xs-3 col-xs-push-9">
                        <a class="btn btn-primary pull-right" href="{{ route('new_category') }}"><i class="fa fa-plus-circle" aria-hidden="true"></i> New Category</a>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox">
                        <div class="ibox-content">

                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th>Category</th>
                                    <th>Products</th>
                                    <th class="text-right">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                	@foreach($categories as $category)
										<tr>
                                            <td>
                                               <a href="{{ route('shop_category', [$category->shop->id, $category->id]) }}">{{ @$category->info->name }}</a>
                                            </td>
                                            <td>
                                              {{ $category->products->count() }}
                                            </td>
                                            <td class="text-right">
                                                <div class="btn-group">
                                                    <a href="{{ route('edit_category', $category->id) }}" class="btn-white btn btn-xs">Edit</a>
                                                    <button class="btn-white btn btn-xs remove" data-id="{{ $category->id }}">Delete</button>
                                                </div>
                                            </td>
                                        </tr>
									@endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@stop