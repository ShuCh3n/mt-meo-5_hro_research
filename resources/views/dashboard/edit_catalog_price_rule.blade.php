@extends('dashboard.layout.main')

@section('stylesheet')
    <link href="/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="/css/plugins/touchspin/jquery.bootstrap-touchspin.min.css" rel="stylesheet">
    <style>
        .select2-result-repository__avatar {
            float: left;
            margin-right: 10px;
            width: 100px;
        }
        .select2-result-repository__title {
            color: black;
            font-weight: bold;
            line-height: 1.1;
            margin-bottom: 4px;
            word-wrap: break-word;
        }
        .select2-result-repository__forks, .select2-result-repository__stargazers, .select2-result-repository__watchers {
            color: #ddd;
            display: inline-block;
            font-size: 11px;
            margin-right: 10px;
        }
    </style>
@stop

@section('javascript')
    <!-- iCheck -->
    <script src="/js/plugins/iCheck/icheck.min.js"></script>

    <!-- TouchSpin -->
    <script src="/js/plugins/touchspin/jquery.bootstrap-touchspin.min.js"></script>

    <script>
        $(function(){
            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });

            $(".input-price").TouchSpin({
                max: 1000000000,
                min: 0,
                step: 0.01,
                decimals: 2,
                boostat: 5,
                maxboostedstep: 10,
                prefix: '€',
                buttondown_class: 'btn btn-white',
                buttonup_class: 'btn btn-white'
            });

            $(".input-percentage").TouchSpin({
                max: 1000000000,
                min: 0,
                step: 0.01,
                decimals: 2,
                boostat: 5,
                maxboostedstep: 10,
                postfix: '%',
                buttondown_class: 'btn btn-white',
                buttonup_class: 'btn btn-white'
            });

            $(".find-product").select2({
                ajax: {
                    url: "/ajax/products/search",
                    headers: { 'X-CSRF-Token': csrf_token },
                    dataType: 'json',
                    type: "post",
                    delay: 250,
                    data: function (params) {
                        return {
                            q: params.term, // search term
                            shop: $("#pick-shop").val(),
                            page: params.page
                        };
                    },
                    processResults: function (data, params) {
                        params.page = params.page || 1;
                        return {
                            results: data.items,
                            pagination: {
                                more: (params.page * 30) < data.total_count
                            }
                        };
                    },
                        cache: true
                    },
                    escapeMarkup: function (markup) { return markup; },
                    minimumInputLength: 1,
                    templateResult: formatProduct,
                    templateSelection: formatProductSelection
            });

            if($('.check-amount').attr('checked')) {
                $('.input-price-amount').show();
                $('.input-percentage-amount').hide();
            }

            if($('.check-percentage').attr('checked')) {
                $('.input-percentage-amount').show();
                $('.input-price-amount').hide();
            }

            $('.check-amount').on('ifChecked', function(){
                $('.input-price-amount').show();
                $('.input-percentage-amount').hide();
            });

            $('.check-percentage').on('ifChecked', function(){
                $('.input-percentage-amount').show();
                $('.input-price-amount').hide();
            });
        });


        function formatProduct (product) {
            if (product.loading) return product.text;

            var markup = "<div class='select2-result-repository clearfix'>";

            if(product.images.length > 0)
            {
                markup += "<div class='select2-result-repository__avatar'><img src='/product/images/" + product.images[0].id + "/small/filename' width='100' /></div>";
            }
            else
            {
                markup += "<div class='select2-result-repository__avatar'><img src='/product/images/no_image/small/filename' width='100' /></div>";
            }

            markup += "<div class='select2-result-repository__meta'><div class='select2-result-repository__title'>" + product.infos[0].name + "</div>";

            if (product.infos[0].short_description) {
                markup += "<div class='select2-result-repository__description'>" + product.infos[0].short_description + "</div>";
            }

            markup += "<div class='select2-result-repository__statistics'>" +
            "<div class='select2-result-repository__forks'><i class='fa fa-hashtag'></i> " + product.id + "</div>" +
            "<div class='select2-result-repository__stargazers'><i class='fa fa-qrcode'></i> " + product.ean + "</div>" +
            "<div class='select2-result-repository__watchers'><i class='fa fa-shopping-bag'></i> " + product.sku + "</div>" +
            "</div>" +
            "</div></div>";

            return markup;
        }

        function formatProductSelection (product) {

            if(product.infos)
            {
                return product.infos[0].name;
            }

            if(product.text)
            {
                return product.text;
            }

            return product;         
        }
    </script>
@stop

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>{{ trans('pages.inquiry_detail_header') }}</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('home') }}">{{ trans('menu.home') }}</a>
                </li>
                <li>
                    <a href="">{{ trans('menu.inquiries') }}</a>
                </li>
                <li class="active">
                    <strong>{{ trans('page.inquiry') }}</strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">
        {!! session()->has('success')? '<div class="alert alert-success alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>' . session('success') . '</div>' : '' !!}

        <form id="profileForm" method="post" class="form-horizontal">
            <div class="row">
                <div class="col-md-8">
                    <div class="ibox">

                        <div class="ibox-title">
                            <h5>New Catalog Price Rule</h5>
                        </div>

                        <div class="ibox-content">
                            <div class="mb20"></div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Active</label>
                                        <div class="col-md-5">
                                            <label><input type="radio" class="form-control i-checks" name="active" value="1" {{ ($catalog_price_rule->active)? 'checked' : '' }}> Yes</label>
                                        </div>
                                        <div class="col-md-5">
                                            <label><input type="radio" class="form-control i-checks" name="active" value="0" {{ (!$catalog_price_rule->active)? 'checked' : '' }}> No</label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Name</label>
                                        <div class="col-md-10"><input type="text" class="form-control" name="name" placeholder="Cataglog Price Rule Name" value="{{ $catalog_price_rule->name }}"></div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Add / Subtract</label>
                                        <div class="col-md-5">
                                            <label><input type="radio" class="form-control i-checks" name="add_or_subtract" value="1" {{ ($catalog_price_rule->add_or_subtract)? 'checked' : '' }}> Add</label>
                                        </div>
                                        <div class="col-md-5">
                                            <label><input type="radio" class="form-control i-checks" name="add_or_subtract" value="0" {{ (!$catalog_price_rule->add_or_subtract)? 'checked' : '' }}> Subtract</label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Amount / Percentage</label>
                                        <div class="col-md-5">
                                            <label><input type="radio" class="form-control i-checks check-amount" name="amount_or_percentage" value="1" {{ ($catalog_price_rule->amount_or_percentage)? 'checked' : '' }}> Amount</label>
                                        </div>
                                        <div class="col-md-5">
                                            <label><input type="radio" class="form-control i-checks check-percentage" name="amount_or_percentage" value="0" {{ (!$catalog_price_rule->amount_or_percentage)? 'checked' : '' }}> Percentage</label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Amount</label>
                                        <div class="col-md-10 input-price-amount">
                                            <input type="text" class="input-price" name="price_amount" placeholder="Price Amount" value="{{ ($catalog_price_rule->amount_or_percentage)? $catalog_price_rule->amount : '' }}">
                                        </div>
                                        <div class="col-md-10 input-percentage-amount">
                                            <input type="text" class="input-percentage" name="percentage_amount" placeholder="Percentage Amount" value="{{ (!$catalog_price_rule->amount_or_percentage)? $catalog_price_rule->amount : '' }}">
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="ibox">

                        <div class="ibox-title">
                            <h5>Catalog Rule</h5>
                        </div>

                        <div class="ibox-content">
                            <div class="mb20"></div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Shops</label>
                                        <div class="col-md-10">
                                            <select2 multiple="" name="shops[]">
                                                @foreach($shops as $shop)
                                                    <option value="{{ $shop->id }}" {{ ($catalog_price_rule->shops->contains('id', $shop->id))? 'selected' : '' }}>{{ $shop->name }}</option>
                                                @endforeach
                                            </select2>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Country</label>
                                        <div class="col-md-10">
                                            <select2 multiple="" name="countries[]">
                                                @foreach($countries as $country)
                                                    <option value="{{ $country->id }}" icon="{{ $country->icon }}" {{ ($catalog_price_rule->countries->contains('id', $country->id))? 'selected' : '' }}>{{ $country->name }}</option>
                                                @endforeach
                                            </select2>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Customer Group</label>
                                        <div class="col-md-10">
                                            <select2 name="customer_groups[]">
                                                @foreach($customer_groups as $group)
                                                    <option value="{{ $group->id }}" {{ ($catalog_price_rule->customer_groups->contains('id', $group->id))? 'selected' : '' }}>{{ $group->name }}</option>
                                                @endforeach
                                            </select2>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Products</label>
                                        <div class="col-md-10">
                                            <select2 multiple="" name="products[]">
                                                @foreach($catalog_price_rule->products as $product)
                                                    <option value="{{ $product->id }}" selected>{{ $product->info->name }}</option>
                                                @endforeach
                                            </select2>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Feature Type</label>
                                        <div class="col-md-10">
                                            <select2 multiple="" name="feature_types[]">
                                                @foreach($product_feature_types as $type)
                                                    <option value="{{ $type->id }}" {{ ($catalog_price_rule->feature_types->contains('id', $type->id))? 'selected' : '' }}>{{ $type->info->name }}</option>
                                                @endforeach
                                            </select2>
                                        </div>
                                    </div>

                                    <hr />

                                    @foreach($shops as $shop)
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">{{ $shop->name }}</label>
                                            <div class="col-md-10">
                                                @if($shop->categories->count() > 0)
                                                    <select2 multiple="multiple" name="categories[]">
                                                        @foreach($shop->categories as $category)
                                                            <option value="{{ $category->id }}" {{ ($catalog_price_rule->categories->contains('id', $category->id))? 'selected' : '' }}>{{ $category->multilanguage->info()->name }}</option>
                                                        @endforeach
                                                    </select2>

                                                @else
                                                    <p class="form-control-static">
                                                        <a href="{{ route('new_category', ["shop_id" => $shop->id]) }}">Create first category</a>
                                                    </p>
                                                @endif
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>

                <div class="col-md-4">
                    <div class="ibox">
                        <div class="ibox-title">
                            <h5>Action</h5>
                        </div>

                        <div class="ibox-content">
                            {{ csrf_field() }}
                            <button class="btn btn-block btn-outline btn-success" type="submit"><i class="fa fa-floppy-o"></i> Save</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>

        <div class="mb30"></div>
    </div>
@stop