<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ticket_departments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('color');
            $table->timestamps();
        });

        DB::table('ticket_departments')->insert([
            [
                "name" => "IT",
                "color" => "1abc9c"
            ],
            [
                "name" => "Sales",
                "color" => "3498db"
            ],
            [
                "name" => "Customer Support",
                "color" => "9b59b6"
            ],
            [
                "name" => "Purchase",
                "color" => "34495e"
            ],
            [
                "name" => "Marketing",
                "color" => "e67e22"
            ]
        ]);

        Schema::create('tickets', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('shop_id')->unsigned();
            $table->integer('order_id')->nullable()->unsigned();
            $table->integer('ticket_department_id')->nullable()->unsigned();
            $table->integer('language_id')->unsigned();
            $table->integer('customer_id')->nullable()->unsigned();
            $table->string('reference')->unique();
            $table->string('subject');
            $table->boolean('gender')->nullable();
            $table->string('initials')->nullable();
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('email')->nullable();
            $table->string('phonenumber')->nullable();
            $table->boolean('closed')->default(0);
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('shop_id')
                ->references('id')
                ->on('shops')
                ->onDelete('cascade');

            $table->foreign('order_id')
                ->references('id')
                ->on('orders')
                ->onDelete('cascade');

            $table->foreign('ticket_department_id')
                ->references('id')
                ->on('ticket_departments')
                ->onDelete('cascade');

            $table->foreign('language_id')
                ->references('id')
                ->on('languages')
                ->onDelete('cascade');

            $table->foreign('customer_id')
                ->references('id')
                ->on('customers')
                ->onDelete('cascade');
        });

        Schema::create('ticket_messages', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ticket_id')->nullable()->unsigned();
            $table->integer('user_id')->nullable()->unsigned();
            $table->boolean('customer');
            $table->text('message');
            $table->string('ip');
            $table->boolean('read')->default(0);
            $table->timestamps();

            $table->foreign('ticket_id')
                ->references('id')
                ->on('tickets')
                ->onDelete('cascade');

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });

        Schema::create('ticket_message_attachments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ticket_message_id')->nullable()->unsigned();
            $table->string('file');
            $table->timestamps();

            $table->foreign('ticket_message_id')
                ->references('id')
                ->on('ticket_messages')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ticket_message_attachments');
        Schema::dropIfExists('ticket_messages');
        Schema::dropIfExists('tickets');
        Schema::dropIfExists('ticket_departments');
    }
}
