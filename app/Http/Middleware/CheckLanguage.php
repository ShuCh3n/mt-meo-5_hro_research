<?php

namespace App\Http\Middleware;

use Closure;
use App\Language;

class CheckLanguage
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(env('API_DEBUG'))
        {
            $request->language = Language::getLanguage(env('DEBUG_LANGUAGE'));
        }

        if($request->header('Language'))
        {
            $request->language = Language::getLanguage($request->header('Language'));
        }

        if(!$request->language)
        {
            $request->language = Language::getLanguage("en");
        }

        return $next($request);
    }
}
