<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Shop;

class CustomerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $shop = Shop::findOrFail($this->shop);

        if($shop){
            $this->shop_object = $shop;
            return true;
        }

        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = array(
            "shop"          => "required",
            "active"        => "required",
            "gender"        => "required",
            "email"         => "required",
            "newsletter"    => "required",
            "optin"         => "required"
        );

        if($this->route()->getName() != "action_edit_customer")
        {
            $rules["password"] = "required";
        }

        return $rules;
    }
}
