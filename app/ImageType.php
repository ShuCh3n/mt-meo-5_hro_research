<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ImageType extends Model
{
	use SoftDeletes;
	protected $table = 'image_type';
	protected $hidden = ['created_at', 'updated_at', 'deleted_at'];
}
