<?php

namespace App\Http\Middleware;

use App\Shop;
use Closure;

class ShopAPI
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->header('ShopToken') || env('API_DEBUG'))
        {
            if($request->header('ShopToken'))
            {
                $request->shop = Shop::with('tax_rule')->where("key", $request->header('ShopToken'))->firstOrFail();
            }
            
            if(env('API_DEBUG'))
            {
                $request->shop = Shop::findOrFail(env('DEBUG_SHOP'));
            }

            return $next($request);
        }

        return response(array('status' => false));
    }
}
