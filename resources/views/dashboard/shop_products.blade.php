@extends('dashboard.layout.main')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-6">
            <h2>{{ trans('pages.inquiry_detail_header') }}</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('home') }}">{{ trans('menu.home') }}</a>
                </li>
                <li>
                    <a href="">{{ trans('menu.inquiries') }}</a>
                </li>
                <li class="active">
                    <strong>{{ trans('page.inquiry') }}</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-6">
            @if($shop)
                <a class="btn btn-outline btn-primary pull-right  mt30" href="{{ route('new_product', ["shop_id" => $shop->id]) }}"><i class="fa fa-plus-circle" aria-hidden="true"></i> @lang('product.new_product')</a>
            @else
                <a class="btn btn-outline btn-primary pull-right  mt30" href="{{ route('new_product') }}"><i class="fa fa-plus-circle" aria-hidden="true"></i> @lang('product.new_product')</a>
            @endif
        </div>
    </div>

	<div class="wrapper wrapper-content animated fadeInRight ecommerce">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-content">

                        <form action="{{ route('product_bulk_edit') }}" method="post">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th>
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-white btn-sm check-all"><i class="fa fa-check-square-o" aria-hidden="true"></i></button>
                                            <button type="submit" class="btn btn-primary btn-sm" type="button">Bulk edit</button>
                                        </div>
                                    </th>
                                    <th></th>
                                    <th>@lang('product.id')</th>
                                    <th>@lang('product.productname')</th>
                                    <th>@lang('product.sku')</th>
                                    <th>@lang('product.ean')</th>
                                    <th>@lang('product.retailprice')</th>
                                    <th>@lang('product.active')</th>
                                    <th class="text-right">@lang('product.action')</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach($products as $product)
                                        <tr>
                                            <td>
                                                <input type="checkbox" name="product[]" class="i-checks" value="{{ $product->id }}">
                                            </td>
                                            <td>
                                                @if($product->allimages()->count() > 0)
                                                    <img class="img-responsive img-thumbnail" width="64" src="{{ route('product_image', [$product->allimages()->first()->id, "thumb", "filename"]) }}" />
                                                @else
                                                    <img class="img-responsive img-thumbnail" width="64" src="{{ route('product_image', ["no_image", "small", "filename"]) }}" />
                                                @endif
                                            </td>
                                            <td>
                                                {{ $product->id }}
                                            </td>
                                            <td>
                                                {{@ $product->info->name }}
                                            </td>
                                            <td>
                                                {{ $product->sku }}
                                            </td>
                                            <td>
                                                {{ $product->ean }}
                                            </td>
                                            <td>
                                                {{ price($product->product_price->retail_price ?? 0) }}
                                            </td>
                                            <td>
                                                {!! ($product->active)? '<i class="fa fa-check text-navy" aria-hidden="true"></i>' : '<i class="fa fa-times text-danger" aria-hidden="true"></i>' !!}
                                            </td>
                                            <td class="text-right">
                                                <div class="btn-group">
                                                    <a class="btn-white btn btn-xs" href="{{ route('view_product', $product->id) }}">@lang('product.view')</a>
                                                    @if($shop)
                                                        <a class="btn-white btn btn-xs" href="{{ route('edit_product', [$product->id, "shop" => $shop->id]) }}">@lang('product.edit')</a>
                                                    @else
                                                        <a class="btn-white btn btn-xs" href="{{ route('edit_product', $product->id) }}">@lang('product.edit')</a>
                                                    @endif
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                    <tr>
                                        <td>
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-white btn-sm check-all"><i class="fa fa-check-square-o" aria-hidden="true"></i></button>
                                                {{ csrf_field() }}
                                                <button type="submit" class="btn btn-primary btn-sm" type="button">@lang('product.bulk_edit')</button>
                                            </div>
                                        </td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                </tbody>
                            </table>
                        </form>

                        <div class="text-center">
                            {{ $products->links() }}
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
