from datetime import *
from decimal import *

import databaseMinion
import pandas as pd
import sys
import datetime
import Platform as pf
import ftpMinion as ftp_minion

sys.path.append('..')
import zeus_scheme
sys.path.remove('..')

class ThirdPartyMinion():
	def __init__(self):
		self.results = []
		self.platforms = []
		self.database_minion = databaseMinion.DatabaseMinion()
		self.products = self.database_minion.session.query(zeus_scheme.Product).all()
		self._min_stock = 4

		self.run()

	def run(self):

		for product in self.products:
			collection = []

			for platform in product.platforms:
				warehouse = self.database_minion.session.query(zeus_scheme.Warehouse).filter_by(name = platform.name).first()

				if warehouse:
					price = self.database_minion.session.query(zeus_scheme.PlatformProductPrice).filter_by(platform_id = platform.id, product_id = product.id).order_by(zeus_scheme.PlatformProductPrice.created_at).first()
					stock = self.database_minion.session.query(zeus_scheme.ProductStock).filter_by(warehouse_id = warehouse.id, product_id = product.id).order_by(zeus_scheme.ProductStock.created_at).first()

					if stock:
						collection.append({
							'platform': platform,
							'price':price.price,
							'stock':stock.amount,
							'supplier_sku':product.platform_product.filter_by(platform_id = platform.id).first().article_code
						})

			self.caculate(product, collection)
			print(product)

		self.platformExport()	

	def caculate(self, product, collection):
		caculated = []
		product_name = "No Name"

		for c in collection:
			if c['stock'] >= self._min_stock and c['platform'].settings['b2b']['active'] == "1":
				price = c['price']
				percentage = 100
				amount = 0
				shipping = 0

				if c['platform'].settings['b2b']['percentage']:
					percentage += Decimal(c['platform'].settings['b2b']['percentage'])

				if c['platform'].settings['b2b']['amount']:
					amount += Decimal(c['platform'].settings['b2b']['amount'])

				if c['platform'].settings['shipping']:
					shipping += Decimal(c['platform'].settings['shipping'])

				price = (((price + amount) / 100) * percentage)  + shipping
				price = round(price, 2)
				
				caculated.append({
					"platform": c['platform'],
					"original_price": c['price'],
					"price": price,
					"stock": c['stock'],
					"supplier_sku": c['supplier_sku']
				})

		if len(product.infos) > 0:
			product_name = product.infos[0].name

		sorted_prices = sorted(caculated, key=lambda k: k['price'])
		
		if sorted_prices:
			platform_obj = self.getPlatform(sorted_prices[0]['platform'])
			platform_full = self.getPlatform('full')

			platform_obj.results.append([
				product.supplier_sku,
				product.sku,
				product_name,
				product.ean,
				sorted_prices[0]['stock'],
				sorted_prices[0]['original_price'],
				sorted_prices[0]['price'],
				sorted_prices[0]['supplier_sku'],
				platform_obj.code,
				platform_obj.name
			])

			platform_full.results.append([
				product.supplier_sku,
				product.sku,
				product_name,
				product.ean,
				sorted_prices[0]['stock'],
				sorted_prices[0]['original_price'],
				sorted_prices[0]['price'],
				sorted_prices[0]['supplier_sku'],
				platform_obj.code,
				platform_obj.name
			])

	def getPlatform(self, platform):
		if isinstance(platform, str):
			name = 'full'
		else:
			name = platform.name

		for known_platform in self.platforms:
			if name == known_platform.name:
				return known_platform

		platform_obj = pf.Platform(platform)
		self.platforms.append(platform_obj)

		return platform_obj

	def platformExport(self):
		ftp = ftp_minion.FTPMinion()
		ftp.tti_server()
		
		for platform in self.platforms:
			platform.export().setFTP(ftp).store()

		ftp.close()


