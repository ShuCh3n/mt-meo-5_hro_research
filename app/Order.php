<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    use SoftDeletes;
    protected $hidden = ['updated_at', 'deleted_at'];

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function payment()
    {
        return $this->belongsTo(Payment::class);
    }

    public function shop()
    {
        return $this->belongsTo(Shop::class);
    }

    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    public function order_status()
    {
        return $this->belongsTo(OrderStatus::class);
    }

    public function order_address()
    {
        return $this->hasOne(OrderAddress::class);
    }

    public function lines()
    {
        return $this->hasMany(OrderLine::class);
    }

    public function scopeReference($query, $reference)
    {
        return $query->where('reference', $reference);
    }

    public function products(){
        return Product::whereHas('order_lines', function ($q){
            $q->where('order_id', $this->id);
        })->get();
    }

    public function line_products(){
        return $this->hasMany(OrderLine::class)->with('product')->get();
    }

    public function service_products()
    {
        return $this->hasMany(OrderLine::class)
            ->whereHas('product', function ($query){
                $query->where('product_feature_type_id', 2);
            })->with('product');
    }

    public function filter($istyre, $third_party)
    {
        return $this->hasMany(OrderLine::class)
            ->where('third_party_stock', $third_party)
            ->whereHas('product', function ($query) use($istyre) {
                $query->where('product_feature_type_id', '!=' , 2);
                if($istyre){
                    $query->where('product_feature_type_id', 1);
                }else{
                    $query->where('product_feature_type_id', '!=', 1);
                }
            })
            ->with(['product' => function ($query) use($istyre) {
                if($istyre){
                    $query->where('product_feature_type_id', 1);
                }else{
                    $query->where('product_feature_type_id', '!=', 1);
                }
            }
            ])
            ->get();
    }

    public function addLine($product_id, $qty)
    {
        $product = Product::with(['shop_price' => function($query){
            $query->where('shop_id', $this->shop_id);
        }])->with(['shop_info' => function($query){
            $query->where('shop_id', $this->shop_id);
        }])->with(['shop_product' => function($query){
            $query->where('shop_id', $this->shop_id)->with('tax_rule');
        }])->findOrFail($product_id);

        $line = $this->lines()->create([
            "product_id"    => $product->id,
            "warehouse_id"  => null,
            "name"          => $product->shop_info->name,
            "qty"           => $qty,
            "price"         => $product->shop_price->retail_price
        ]);

        $this->total_price = $this->total_price + ($line->price * $line->qty);

        if($product->shop_product->tax_rule)
        {
            $tax_line = $this->lines()->create([
                "name"          => $product->shop_product->tax_rule->name,
                "qty"           => $qty,
                "price"         => (($product->shop_product->tax_rule->percentage / 100) * $line->price) + $product->shop_product->tax_rule->fixed_price
            ]);

            $this->total_price = $this->total_price + ($tax_line->price * $tax_line->qty);
        }

        $this->save();
    }

    public function calculateShopTax($shop)
    {
        $this->tax_price = (($shop->tax_rule->percentage / 100) * $this->total_price) + $shop->tax_rule->fixed_price;
        $this->save();
    }

    public function updateStatus($status)
    {
        $this->order_status_id = OrderStatus::status($status)->first()->id;
        $this->save();
    }
}
