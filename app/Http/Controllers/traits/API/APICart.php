<?php

namespace App\Http\Controllers\traits\API;

use App\Cart;
use App\ShopProduct;
use App\Classes\ShopProductPrice;

trait APICart {

	public function cart()
	{
		$cart = $this->request->customer_session->fresh()->cart;

		if(!$cart)
		{
			$cart = $this->initCart();
		}

		$cart->cart_products = $cart->cart_products()->with(['product' => function($query){
				                            $query->shopInfo($this->request->shop->id, $this->request->language->id)->featureInfo($this->request->language->id)->with('product_price');
				                }])->get();
		$cart->total_tax_price = 0;

		foreach($cart->cart_products as $cart_product)
		{
			$cart_product->product->prices = new ShopProductPrice($cart_product->product, $this->request->shop);
			$cart->total_tax_price += $cart_product->product->prices->tax_price;
		}

		$cart->total_price = $cart->totalprice($this->request);
		return $cart;
	}

	public function addToCart()
	{
        $cart = $this->cart();
		foreach($this->request->products as $product)
        {
			$shop_product = ShopProduct::where('shop_id', $this->request->shop->id)->where('product_id', $product["product_id"])->with(['product' => function($query){
                            $query->shopInfo($this->request->shop->id, $this->request->language->id)->featureInfo($this->request->language->id)->with('product_price')->featureInfo($this->request->language->id);
                        }])->firstOrFail();
			if($shop_product->product)
            {
                if(isset($product['product_price'])){
                    $cart->addProduct($product, $shop_product->product, $this->request, $product['product_price']);
                }else{
                    $cart->addProduct($product, $shop_product->product, $this->request);
                }
			}
		}

		$cart->total_price = $cart->totalprice($this->request);
		return $this->cart();
	}

	public function clearCart(){
		$this->cart()->delete();
	    return $this->initCart();
    }

	protected function initCart()
	{
		$cart = new Cart;
		$cart->shop_id = $this->request->shop->id;
		$cart->customer_session_id = $this->request->customer_session->id;
		$cart->save();

		return $cart;
	}
}
