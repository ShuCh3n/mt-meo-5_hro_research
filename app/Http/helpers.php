<?php
	use App\Country;
	use App\CustomerSession;
	use Carbon\Carbon;
	use GuzzleHttp\Client;
	use App\VehicleYear;

	function price($price)
	{
		return "€ " . number_format($price, 2, ',', '.');
	}

	function raw_price($price)
	{
		return floatval(str_replace(',', '.', trim(str_replace('.', '', $price), '€ ')));
	}

	function get_right_price($price, $incl_vat)
	{
		if($incl_vat == "true")
		{
			$rule = Country::where('name', 'netherlands')->first()->getDefaultTaxRule();
			$price = ($price / intval("1" . $rule->percentage)) * 100;
		}

		return $price;
	}

	function safeFill($model, $array){
        foreach ($array as $column => $value){
            if($model->__isset($column)){
                $model->$column = $value;
            }
        }
        $model->save();
	}

	function customer_session($session_token)
	{
		return CustomerSession::where('session_token', $session_token)->first();
	}

	function get_feature_option_value($options, $search)
	{
		foreach($options as $option)
		{
			if(collect($option->product_feature->infos)->where('name', $search)->first())
			{
				return $option;
			}
		}

		return null;
	}

	function carbon()
	{
		return new Carbon;
	}

	function guzzle()
	{
		return new Client;
	}

	function tyre_sizes($tyre_string)
	{
		$collect = collect();
		$explode = explode('/', $tyre_string);
		$collect->height = $explode[0];
		$collect->width = $explode[1];
		$collect->inch = $explode[2];

		return $collect;
	}

	function vehicle_year($year)
	{
		$cache_name = env('CACHE_PREFIX') . 'vehicle_year_' . $year;

		if(!cache()->has($cache_name))
		{
			cache()->forever($cache_name, VehicleYear::where('year', $year)->first());
		}

		return cache()->get($cache_name);
	}

	function select2_multiple($array)
	{
		return implode(',', $array);
	}

	function flag_icon($icon)
	{
		return '<img src="/images/flags/16/' . $icon. '.png" />';
	}

	function country($iso)
	{
		return Country::where('iso_code_2', $iso)->first();
	}