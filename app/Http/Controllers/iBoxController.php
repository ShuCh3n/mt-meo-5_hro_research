<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\AnalistFile;
use App\Platform;
use App\Product;
use App\PlatformProductPrice;
use App\Brand;
use App\Commodity;
use App\Classes\iBox\Analyzer;
use App\Weather;
use App\Country;
use App\AccountViewSetting;

use DB;
use File;
use Storage;


class iBoxController extends Controller
{

    public function analist()
    {
    	$histories = AnalistFile::with('user')->orderBy('created_at', 'desc')->paginate(20);

    	return view('dashboard.analist', compact('histories'));
    }

    public function platforms()
    {
    	$platforms = Platform::orderBy('name', 'asc')->paginate(100);
    	return view('dashboard.platform', compact('platforms'));
    }

    public function platformProducts()
    {
        $platform = Platform::findOrFail($this->request->id);
        $products = $platform->products()->with(['images', 'info', 'product_price'])->paginate(100);

        return view('dashboard.platform_products', compact('platform', 'products'));
    }

    public function removeFile()
    {
        $file = AnalistFile::findOrFail($this->request->id);
        $file->delete();
    }

    public function platformSettings()
    {
        $platform = Platform::findOrFail($this->request->id);
        return view('dashboard.platform_settings', compact('platform'));
    }
    
    public function uploadAnalistFile()
    {
        $filename = date('YmdHis') . '_' . str_random(10) . "." . $this->request->file->getClientOriginalExtension();
        $file = File::get($this->request->file);

        Storage::disk('local')->put('ibox/analist/excel/' . $filename, $file);

        $analistFile = new AnalistFile;
        $analistFile->user_id = auth()->user()->id;
        $analistFile->original_filename = $this->request->file->getClientOriginalName();
        $analistFile->filename = $filename;

        $analistFile->save();

        return array(
            "status"        => true,
            "id"            => $analistFile->id,
            "filename"      => $filename,
            "user"          => auth()->user()->firstname,
            "date"          => $analistFile->created_at->format('d M Y')
        );
    }

    public function analyze()
    {
        $analist_file = AnalistFile::findOrFail($this->request->file_id);
        $analyzer = new Analyzer($analist_file, $this->request->formula, $this->request->equivalent, $this->request->segments);
        
        if($this->request->export_as == 'excel')
        {
            return $analyzer->resultInExcel();
        }
        
        $this->request->merge(['export_as' => 'excel']);

        return view('dashboard.analyse_result', compact('analyzer', 'file'));
    }

    public function getPriceStatusInString($status)
    {
        switch ($status) {
            case 1:
                $price_status = "Deal";
                break;
            case 2:
                $price_status = "No Deal";
                break;
            case 3:
                $price_status = "No Info";
                break;
            default:
                $price_status = "No Info";
                break;
        }

        return $price_status;
    }

    public function analyseInExcel($results, $filename)
    {
        $filename = pathinfo($filename, PATHINFO_FILENAME);

        return Excel::create($filename, function($excel) use ($results){
            $excel->setCreator('Internet Box')->setCompany('TTI B.V.');

            $header = array('EAN', 'Product Code', 'Article Code', 'Name', 'Supplier Price', 'Difference', 'Target', 'Cheapest');

            foreach(collect($results->platforms)->sortBy('priority') as $platform)
            {
                $header[] = $platform->name;
            }

            $header[] = 'TTI';
            $header[] = 'Price Status';

            $excel->sheet('Analyzer', function($sheet) use($results, $header) {
                $sheet->row(1, $header);

                foreach($results->items as $result)
                {
                    if($result->product)
                    {
                        $row['ean'] = $result->product->ean;
                        $row['product_code'] = $result->product->supplier_sku;
                        $row['article_code'] = $result->product->sku;
                        $row['name'] = $result->product->info->name ?? '-';
                        $row['supplier_price'] = price($result->row->price ?? 0);
                        $row['difference'] = ($result->percentage_difference ?? '-') . '%';
                        $row['target'] =  price($result->caculated_price ?? 0);
                        $row['cheapest'] = price($result->cheapest_platform['price']->price ?? 0);

                        foreach(collect($results->platforms)->sortBy('priority') as $platform)
                        {
                            $price = 0;

                            if($result->platform_prices)
                            {
                                $price = collect($result->platform_prices)->where('platform.name', $platform->name)->first()->price ?? 0;
                            }

                            $row[$platform->name] = price($price);
                        }

                        $row["tti_price"] = price($result->product_price->retail_price ?? 0);
                        $row["dealornodeal"] = $this->getPriceStatusInString($result->status);

                        $sheet->appendRow($row);
                    }

                    if(!$result->product)
                    {
                        $row = array();

                        $row['ean'] = $result->row->ean_code ?? '';
                        $row['product_code'] = $result->row->prod_code ?? '';
                        $row['article_code'] = $result->row->article_code ?? '';
                        $row['name'] = $result->product->row->name ?? '';
                        $row['supplier_price'] = price($result->row->price ?? 0);

                        $sheet->appendRow($row);

                        $sheet->row($sheet->getHighestRow(), function($row) {
                            $row->setBackground('#E82C0C')->setFontColor('#ffffff');
                        });
                    }
                }
            });

        })->download('xlsx');
    }

    public function statistics()
    {
        $rubber = Commodity::getStats('Rubber', request()->from ?? carbon()->startOfMonth(), request()->until ?? carbon()->endOfMonth());
        $weather = Weather::getStats((request()->from)? carbon()->createFromFormat('d M Y', request()->from)->startOfDay() : carbon()->startOfMonth(), (request()->until)? carbon()->createFromFormat('d M Y', request()->until)->endOfDay() : carbon()->endOfMonth());

        return view('dashboard.ibox_statistics', compact('rubber', 'weather'));
    }

    public function actionPlatformSettings()
    {
        $settings = collect([
            "delivery"          => $this->request->delivery,
            "shipping"          => $this->request->shipping,
            "b2b"               => [
                "active"            => $this->request->b2b_active,
                "amount"            => $this->request->b2b_amount,
                "percentage"        => $this->request->b2b_percentage
            ],
            "b2c"               => [
                "active"            => $this->request->b2c_active,
                "amount"            => $this->request->b2c_amount,
                "percentage"        => $this->request->b2c_percentage
            ]
        ])->toJson();

        $platform = Platform::findOrFail($this->request->id)->update([
            'code'      => $this->request->code,
            'settings'  => $settings
        ]);

        return redirect()->route('platform_setting', $this->request->id);
    }

    public function accountView()
    {
        $countries = Country::oldest('name')->get();
        $platforms = Platform::orderBy('name', 'asc')->get();
        $settings = AccountViewSetting::oldest()->get();

        return view('dashboard.ibox_account_view', compact('countries', 'platforms', 'settings'));
    }

    public function actionAccountView()
    {
        if(isset($this->request->country['exist']))
        {
            foreach($this->request->country['exist'] as $key => $country)
            {
                $this->insertUpdateAccountViewSettings([
                    "id"            => $key,
                    "country"       => $country,
                    "platform"      => $this->request->platform['exist'][$key],
                    "threshold"     => $this->request->threshold['exist'][$key],
                    "amount"        => $this->request->amount['exist'][$key],
                    "percentage"    => $this->request->percentage['exist'][$key]
                ]);
            }
        }

        if(isset($this->request->country['new']))
        {
            foreach($this->request->country['new'] as $key => $country)
            {
                $this->insertUpdateAccountViewSettings([
                    "country"       => $country,
                    "platform"      => $this->request->platform['new'][$key],
                    "threshold"     => $this->request->threshold['new'][$key],
                    "amount"        => $this->request->amount['new'][$key],
                    "percentage"    => $this->request->percentage['new'][$key]
                ]);
            }
        }
        

        return redirect()->route('account_view');
    }

    private function insertUpdateAccountViewSettings($infos)
    {
        if($infos['platform'] && $infos['threshold'] && $infos['amount'] && $infos['percentage'])
        {
            $country = Country::findOrFail($infos['country']);
            $platform = Platform::findOrFail($infos['platform']);

            $settings = collect([
                "threshold"     => $infos['threshold'],
                "amount"        => $infos['amount'],
                "percentage"    => $infos['percentage']
            ])->toJson();

            if(isset($infos['id']))
            {
                AccountViewSetting::find($infos['id'])->update([
                    'platform_id' => $platform->id, 
                    'country_id' => $country->id, 
                    'settings' => $settings
                ]);

                return $this;
            }

            AccountViewSetting::create([
                'platform_id' => $platform->id, 
                'country_id' => $country->id, 
                'settings' => $settings
            ]);
        }
    }

    public function removeAccountViewFormule()
    {
        $settings = AccountViewSetting::findOrFail($this->request->id)->delete();

        return array('status' => true);
    }
}