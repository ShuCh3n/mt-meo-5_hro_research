<?php

use Illuminate\Http\Request;

Route::group(['middleware' => 'administration', 'as' => 'administration.'], function () {
	Route::get('product/{sku}', ["uses" => "AdministrationController@searchProduct"]);
	Route::get('products', ["uses" => "AdministrationController@products"]);
});
