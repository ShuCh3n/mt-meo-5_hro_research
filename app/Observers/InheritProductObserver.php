<?php

namespace App\Observers;

use App\Jobs\UpdateProduct;

/**
 * Observes the Model
 */
class InheritProductObserver 
{
    /**
     * Function will be triggerd when a Model is updated
     * 
     * @param Model $model
     */
    public function saved($model)
    {
        foreach($model->products as $product){
            $product->touch();
        }
    }
}