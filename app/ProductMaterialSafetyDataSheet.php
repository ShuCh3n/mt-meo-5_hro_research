<?php
namespace App;

use Nanigans\SingleTableInheritance\SingleTableInheritanceTrait;

class ProductMaterialSafetyDataSheet extends File
{
    use SingleTableInheritanceTrait;

    protected static $singleTableType = 'ProductMaterialSafetyDataSheet';
}
