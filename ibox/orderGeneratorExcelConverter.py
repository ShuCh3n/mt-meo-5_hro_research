import pandas
import numpy
import sys
import os


if not os.path.isdir('../storage/app/generated-orders/json'):
    os.makedirs('../storage/app/generated-orders/json')


file = pandas.read_excel('../storage/app/generated-orders/excel/' + sys.argv[1], encoding='utf8')
f = open('../storage/app/generated-orders/json/' + sys.argv[1] + '.json', 'w')
f.write(file.to_json(orient='records'))
f.close()