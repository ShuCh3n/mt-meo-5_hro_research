@extends('dashboard.layout.main')

@section('javascript')
    <script>
        $(function(){
            $(".shop_list").select2();
        });
    </script>
@stop

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-6">
            <h2>{{ trans('pages.inquiry_detail_header') }}</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('home') }}">{{ trans('menu.home') }}</a>
                </li>
                <li>
                    <a href="">{{ trans('menu.inquiries') }}</a>
                </li>
                <li class="active">
                    <strong>{{ trans('page.inquiry') }}</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-6">
            <a class="btn btn-outline btn-primary pull-right mt30" href="{{ route('new_customer') }}"><i class="fa fa-plus-circle" aria-hidden="true"></i> New customer</a>
        </div>
    </div>

	<div class="wrapper wrapper-content animated fadeInRight ecommerce">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-content">

                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Firstname</th>
                                <th>Lastname</th>
                                <th>Email address</th>
                                <th>Newsletter</th>
                                <th>Shop</th>
                                <th>Last visit</th>
                                <th>Registration</th>
                                <th>Active</th>
                                <th class="text-right">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($customers as $customer)
                                    <tr>
                                        <td>
                                          {{ $customer->id }}
                                        </td>
                                        <td>
                                           {{ $customer->firstname }}
                                        </td>
                                        <td>
                                          {{ $customer->lastname }}
                                        </td>
                                        <td>
                                          {{ $customer->email }}
                                        </td>
                                        <td>
                                          {!! ($customer->newsletter)? '<i class="fa fa-check text-navy" aria-hidden="true"></i>' : '<i class="fa fa-times text-danger" aria-hidden="true"></i>' !!}
                                        </td>
                                        <td>
                                          {{ $customer->shop->name }}
                                        </td>
                                        <td>
                                          {{ ($customer->lastVisit())? date("d M Y", strtotime($customer->lastVisit())) : "Never" }}
                                        </td>
                                        <td>
                                          {{ date("d M Y", strtotime($customer->created_at)) }}
                                        </td>
                                        <td>
                                          {!! ($customer->active)? '<i class="fa fa-check text-navy" aria-hidden="true"></i>' : '<i class="fa fa-times text-danger" aria-hidden="true"></i>' !!}
                                        </td>
                                        <td class="text-right">
                                            <div class="btn-group">
                                                <a class="btn-white btn btn-xs" href="{{ route('view_customer', $customer->id) }}">View</a>
                                                <a class="btn-white btn btn-xs" href="{{ route('edit_customer', $customer->id) }}">Edit</a>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <td colspan="6">
                                    <ul class="pagination pull-right"></ul>
                                </td>
                            </tr>
                            </tfoot>
                        </table>

                        {{ $customers->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop