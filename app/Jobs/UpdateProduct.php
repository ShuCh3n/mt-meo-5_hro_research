<?php

namespace App\Jobs;

use App\Product;
use App\Classes\WebHooks\WebHookFactory;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class UpdateProduct implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $product;
    protected $webhook;
    protected $event;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Product $product, $webhook, $event = 'create')
    {
        $this->product = $product;
        $this->webhook = $webhook;
        $this->event = $event;
    }
    /**

     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
    	$webhook = WebHookFactory::create($this->product, $this->webhook, $this->event);
    	$webhook->execute();
    }
}
