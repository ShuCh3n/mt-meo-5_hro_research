@extends('dashboard.layout.main')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-6">
            <h2>{{ trans('pages.inquiry_detail_header') }}</h2>
            <ol class="breadcrumb">
                <li>
                    {{ trans('menu.home') }}
                </li>
                <li>
                    {{ trans('menu.inquiries') }}
                </li>
                <li class="active">
                    <strong>{{ trans('page.inquiry') }}</strong>
                </li>
            </ol>
        </div>

        <div class="col-lg-6">
            <a class="btn btn-outline btn-primary pull-right mt30" href="{{ route('new_catalog_price_rule') }}"><i class="fa fa-plus-circle" aria-hidden="true"></i> New Catalog Price Rule</a>
        </div>

    </div>

	<div class="wrapper wrapper-content animated fadeInRight ecommerce">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-content">

                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>Catalog Price Rule</th>
                                    <th>Amount</th>
                                    <th>Country</th>
                                    <th>Customer Group</th>
                                    <th>Product</th>
                                    <th>Feature Type</th>
                                    <th>Category</th>
                                    <th>Valid</th>
                                    <th class="text-right">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($price_rules as $rule)
                                    <tr>
                                        <td>
                                           {{ $rule->name }}
                                        </td>
                                        <td>
                                            @if($rule->add_or_subtract)
                                                <i class="fa fa-plus" aria-hidden="true"></i>
                                            @else
                                                <i class="fa fa-minus" aria-hidden="true"></i>
                                            @endif

                                            @if($rule->amount_or_percentage)
                                             €
                                           @endif

                                           {{ $rule->amount }}

                                           @if(!$rule->amount_or_percentage)
                                            %
                                           @endif
                                        </td>
                                        <td>
                                          {!! ($rule->countries->count() > 0)? '<i class="fa fa-check text-navy" aria-hidden="true"></i>' : '<i class="fa fa-times text-danger" aria-hidden="true"></i>' !!}
                                        </td>
                                        <td>
                                          {!! ($rule->customer_groups->count() > 0)? '<i class="fa fa-check text-navy" aria-hidden="true"></i>' : '<i class="fa fa-times text-danger" aria-hidden="true"></i>' !!}
                                        </td>
                                        <td>
                                          {!! ($rule->products->count() > 0)? '<i class="fa fa-check text-navy" aria-hidden="true"></i>' : '<i class="fa fa-times text-danger" aria-hidden="true"></i>' !!}
                                        </td>
                                        <td>
                                          {!! ($rule->feature_types->count() > 0)? '<i class="fa fa-check text-navy" aria-hidden="true"></i>' : '<i class="fa fa-times text-danger" aria-hidden="true"></i>' !!}
                                        </td>
                                        <td>
                                          {!! ($rule->categories->count() > 0)? '<i class="fa fa-check text-navy" aria-hidden="true"></i>' : '<i class="fa fa-times text-danger" aria-hidden="true"></i>' !!}
                                        </td>
                                        <td>
                                            {!! (carbon()->now()->diffInDays($rule->valid_until, false) > 0)? '<i class="fa fa-check text-navy" aria-hidden="true"></i> ' . carbon()->now()->diffInDays($rule->valid_until) : '<i class="fa fa-times text-danger" aria-hidden="true"></i> ' . 0 !!} days left
                                        </td>
                                        <td class="text-right">
                                            <div class="btn-group">
                                                <a href="{{ route('edit_catalog_price_rule', $rule->id) }}" class="btn-white btn btn-xs">Edit</a>
                                                <button data-rule-id="{{ $rule->id }}" class="btn-white btn btn-xs demo4">Delete</button>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop