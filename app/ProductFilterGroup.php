<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\ProductFilter; 

class ProductFilterGroup extends Model
{
    public function filters() {
        return $this->hasMany(ProductFilter::class);
    }
}
