<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Weather extends Model
{
    protected $fillable = ['country_id', 'degree'];

    public function scopeRange($query, $from, $until)
    {
        return $query->where('created_at', '>=', $from)->where('created_at', '<=', $until);
    }

    public static function getStats($from, $until)
    {
    	$stats = [];
    	$weathers = self::range($from, $until)->get();

    	foreach($weathers as $weather)
    	{
    		$stats[] = array(
				(carbon()->createFromFormat('Y-m-d H:i:s', $weather->created_at)->timestamp) * 1000,
				$weather->degree
			);
    	}

    	return collect($stats)->toJson();
    }
}
