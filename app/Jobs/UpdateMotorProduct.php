<?php

namespace App\Jobs;


use App\CategoryMotorProductVehicleYear;
use App\Motor;
use App\Classes\WebHooks\WebHookFactory;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class UpdateMotorProduct implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $motor_product;
    protected $webhook;
    protected $event;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(CategoryMotorProductVehicleYear $motor_product, $webhook, $event = 'create')
    {
        $this->motor_product = $motor_product;
        $this->webhook = $webhook;
        $this->event = $event;
    }
    /**

     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $webhook = WebHookFactory::create($this->motor_product, $this->webhook, $this->event);
        $webhook->execute();
    }
}