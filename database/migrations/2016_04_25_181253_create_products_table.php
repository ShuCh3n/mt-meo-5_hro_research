<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_feature_types', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
        });

        DB::table('product_feature_types')->insert([
            [
                "updated_at" => "2016-09-22 13:56:08",
                "created_at" => "2016-09-22 13:56:08"
            ]
        ]);

        Schema::create('product_feature_type_languages', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_feature_type_id')->unsigned();
            $table->integer('language_id')->unsigned();
            $table->string('name');
            $table->timestamps();

            $table->foreign('product_feature_type_id')
                ->references('id')
                ->on('product_feature_types')
                ->onDelete('cascade');

            $table->foreign('language_id')
                ->references('id')
                ->on('languages')
                ->onDelete('cascade');
        });

        DB::table('product_feature_type_languages')->insert([
            [
                "product_feature_type_id" => 1,
                "language_id" => 2,
                "name" => "Banden"
            ]
        ]);

        Schema::create('product_features', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_feature_type_id')->unsigned();
            $table->timestamps();

            $table->foreign('product_feature_type_id')
                ->references('id')
                ->on('product_feature_types')
                ->onDelete('cascade');
        });

        DB::table('product_features')->insert([
            [
                "product_feature_type_id" => 1
            ],
            [
                "product_feature_type_id" => 1
            ],
            [
                "product_feature_type_id" => 1
            ],
            [
                "product_feature_type_id" => 1
            ],
            [
                "product_feature_type_id" => 1
            ],
            [
                "product_feature_type_id" => 1
            ],
            [
                "product_feature_type_id" => 1
            ],
            [
                "product_feature_type_id" => 1
            ],
            [
                "product_feature_type_id" => 1
            ],
            [
                "product_feature_type_id" => 1
            ],
            [
                "product_feature_type_id" => 1
            ],
            [
                "product_feature_type_id" => 1
            ]
        ]);

        Schema::create('product_feature_languages', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_feature_id')->unsigned();
            $table->integer('language_id')->unsigned();
            $table->string('name');
            $table->timestamps();

            $table->foreign('product_feature_id')
                ->references('id')
                ->on('product_features')
                ->onDelete('cascade');

            $table->foreign('language_id')
                ->references('id')
                ->on('languages')
                ->onDelete('cascade');
        });

        DB::table('product_feature_languages')->insert([
            [
                "product_feature_id" => 1,
                "language_id" => 2,
                "name" => "Hoogte"
            ],
            [
                "product_feature_id" => 2,
                "language_id" => 2,
                "name" => "Breedte"
            ],
            [
                "product_feature_id" => 3,
                "language_id" => 2,
                "name" => "Inch"
            ],
            [
                "product_feature_id" => 4,
                "language_id" => 2,
                "name" => "Decibel"
            ],
            [
                "product_feature_id" => 5,
                "language_id" => 2,
                "name" => "Seizoen"
            ],
            [
                "product_feature_id" => 6,
                "language_id" => 2,
                "name" => "Runflat"
            ],
            [
                "product_feature_id" => 7,
                "language_id" => 2,
                "name" => "DOT"
            ],
            [
                "product_feature_id" => 8,
                "language_id" => 2,
                "name" => "Profile"
            ],
            [
                "product_feature_id" => 9,
                "language_id" => 2,
                "name" => "Speedindex"
            ],
            [
                "product_feature_id" => 10,
                "language_id" => 2,
                "name" => "Loadindex"
            ],
            [
                "product_feature_id" => 11,
                "language_id" => 2,
                "name" => "Tubeless"
            ],
            [
                "product_feature_id" => 12,
                "language_id" => 2,
                "name" => "Energie Label"
            ]
        ]);

        Schema::create('product_groups', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->timestamps();
        });

        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('brand_id')->nullable()->unsigned();
            $table->integer('product_feature_type_id')->nullable()->unsigned();
            $table->integer('product_group_id')->nullable()->unsigned();
            $table->integer('tax_rule_id')->nullable()->unsigned();
            $table->string('supplier_sku')->nullable();
            $table->string('ean')->nullable();
            $table->string('upc')->nullable();
            $table->string('sku')->unique();
            $table->integer('minimum_qty')->default(1);
            $table->boolean('stock_management')->default(0);
            $table->boolean('active_when_out_of_stock')->default(1);
            $table->boolean('deny_order_when_out_of_stock')->default(1);
            $table->decimal('width')->nullable();
            $table->decimal('height')->nullable();
            $table->decimal('depth')->nullable();
            $table->decimal('weight')->nullable();
            $table->decimal('additional_fee_per_item')->nullable();
            $table->boolean('active')->default(1);
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('brand_id')
                ->references('id')
                ->on('brands')
                ->onDelete('cascade');

            $table->foreign('product_feature_type_id')
                ->references('id')
                ->on('product_feature_types')
                ->onDelete('cascade');

            $table->foreign('product_group_id')
                ->references('id')
                ->on('product_groups')
                ->onDelete('cascade');

            $table->foreign('tax_rule_id')
                ->references('id')
                ->on('tax_rules')
                ->onDelete('cascade');
        });

        DB::table('products')->insert([
            [
                'ean' => 'lichtmetalen_velg_montage',
                'sku' => 'BSMONTLM',
                'brand_id' => null,
                'minimum_qty' => 1,
                'product_feature_type_id' => null,
                'stock_management' => 0,
                'active_when_out_of_stock' => 1,
                'deny_order_when_out_of_stock' => 0,
                'active' => true
            ],
            [
                'ean' => 'stalen_velg_montage',
                'sku' => 'BSMONTST',
                'brand_id' => null,
                'minimum_qty' => 1,
                'product_feature_type_id' => null,
                'stock_management' => 0,
                'active_when_out_of_stock' => 1,
                'deny_order_when_out_of_stock' => 0,
                'active' => true
            ],
            [
                'ean' => 'stikstof_vulling',
                'sku' => 'BSSTIK',
                'brand_id' => null,
                'minimum_qty' => 1,
                'product_feature_type_id' => null,
                'stock_management' => 0,
                'active_when_out_of_stock' => 1,
                'deny_order_when_out_of_stock' => 0,
                'active' => true
            ],
            [
                'ean' => 'banden_uitlijnen',
                'sku' => 'BSUITL',
                'brand_id' => null,
                'minimum_qty' => 1,
                'product_feature_type_id' => null,
                'stock_management' => 0,
                'active_when_out_of_stock' => 1,
                'deny_order_when_out_of_stock' => 0,
                'active' => true
            ],
            [
                'ean' => 'banden_opslagkosten',
                'sku' => 'BSOPSL',
                'brand_id' => null,
                'minimum_qty' => 1,
                'product_feature_type_id' => null,
                'stock_management' => 0,
                'active_when_out_of_stock' => 1,
                'deny_order_when_out_of_stock' => 0,
                'active' => true
            ],
            [
                'ean' => '12345',
                'sku' => 'test_product',
                'brand_id' => null,
                'minimum_qty' => 1,
                'product_feature_type_id' => null,
                'stock_management' => 0,
                'active_when_out_of_stock' => 0,
                'deny_order_when_out_of_stock' => 1,
                'active' => true
            ],
            [
                'ean' => '54321',
                'sku' => 'test_tyre',
                'brand_id' => 1,
                'minimum_qty' => 1,
                'product_feature_type_id' => 1,
                'stock_management' => 0,
                'active_when_out_of_stock' => 0,
                'deny_order_when_out_of_stock' => 1,
                'active' => true
            ]
        ]);

        Schema::create('product_bulk_prices', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('shop_id')->unsigned();
            $table->integer('product_id')->unsigned();
            $table->boolean('active');
            $table->boolean('wholesale_price');
            $table->integer('qty');
            $table->decimal('retail_price', 10, 2);
            $table->decimal('list_price', 10, 2);
            $table->timestamps();

            $table->foreign('shop_id')
                ->references('id')
                ->on('products')
                ->onDelete('cascade');

            $table->foreign('product_id')
                ->references('id')
                ->on('products')
                ->onDelete('cascade');
        });

        Schema::create('product_prices', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_id')->unsigned();
            $table->decimal('purchasing_price', 10, 2)->nullable();
            $table->decimal('retail_price', 10, 2);
            $table->decimal('list_price', 10, 2)->nullable();
            $table->string('unit_name')->nullable();
            $table->decimal('unit_price', 10, 2)->nullable();
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('product_id')
                ->references('id')
                ->on('products')
                ->onDelete('cascade');
        });

        DB::table('product_prices')->insert([
            [
                "product_id" => 1,
                "purchasing_price" => 1.00,
                "retail_price" => 1.00,
                "list_price" => 1.00,
                "unit_price" => 1.00,
            ],
            [
                "product_id" => 2,
                "purchasing_price" => 1.00,
                "retail_price" => 1.00,
                "list_price" => 1.00,
                "unit_price" => 1.00,
            ],[
                "product_id" => 3,
                "purchasing_price" => 1.00,
                "retail_price" => 1.00,
                "list_price" => 1.00,
                "unit_price" => 1.00,
            ],
            [
                "product_id" => 4,
                "purchasing_price" => 1.00,
                "retail_price" => 1.00,
                "list_price" => 1.00,
                "unit_price" => 1.00,
            ],[
                "product_id" => 5,
                "purchasing_price" => 1.00,
                "retail_price" => 1.00,
                "list_price" => 1.00,
                "unit_price" => 1.00,
            ],
            [
                "product_id" => 6,
                "purchasing_price" => 2.00,
                "retail_price" => 3.00,
                "list_price" => 4.00,
                "unit_price" => 5.00,
            ],
            [
                "product_id" => 7,
                "purchasing_price" => 2.00,
                "retail_price" => 3.00,
                "list_price" => 4.00,
                "unit_price" => 5.00,
            ]
        ]);

        Schema::create('product_shop_prices', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('shop_id')->unsigned();
            $table->integer('product_id')->unsigned();
            $table->float('retail_price');
            $table->decimal('list_price', 10, 2)->nullable();
            $table->string('unit_name')->nullable();
            $table->decimal('unit_price', 10, 2)->nullable();
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('product_id')
                ->references('id')
                ->on('products')
                ->onDelete('cascade');

            $table->foreign('shop_id')
                ->references('id')
                ->on('shops')
                ->onDelete('cascade');
        });

        Schema::create('product_shop_bulk_prices', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('shop_id')->unsigned();
            $table->integer('product_id')->unsigned();
            $table->boolean('active');
            $table->boolean('wholesale_price');
            $table->integer('qty');
            $table->decimal('retail_price', 10, 2);
            $table->decimal('list_price', 10, 2);
            $table->timestamps();

            $table->foreign('product_id')
                ->references('id')
                ->on('products')
                ->onDelete('cascade');

            $table->foreign('shop_id')
                ->references('id')
                ->on('shops')
                ->onDelete('cascade');
        });

        Schema::create('product_images', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_id')->unsigned();
            $table->string('image_link');
            $table->integer('rank');
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('product_id')
                ->references('id')
                ->on('products')
                ->onDelete('cascade');
        });

        Schema::create('product_shop_images', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('shop_id')->unsigned();
            $table->integer('product_id')->unsigned();
            $table->string('image_link');
            $table->integer('rank');
            $table->timestamps();

            $table->foreign('shop_id')
                ->references('id')
                ->on('shops')
                ->onDelete('cascade');

            $table->foreign('product_id')
                ->references('id')
                ->on('products')
                ->onDelete('cascade');
        });

        Schema::create('product_infos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_id')->unsigned();
            $table->integer('language_id')->unsigned();
            $table->string('name');
            $table->text('short_description')->nullable();
            $table->text('description')->nullable();
            $table->text('specification')->nullable();
            $table->string('link')->nullable();
            $table->timestamps();

            $table->foreign('product_id')
                ->references('id')
                ->on('products')
                ->onDelete('cascade');

            $table->foreign('language_id')
                ->references('id')
                ->on('languages')
                ->onDelete('cascade');
        });

        DB::table('product_infos')->insert([
            [
                "product_id" => 1,
                "language_id" => 2,
                "name" => "lichtmetalen_velg_montage",
                "short_description" => "lichtmetalen_velg_montage",
                "description" => "lichtmetalen_velg_montage",
                "specification" => "lichtmetalen_velg_montage",
                "link" => ""
            ],
            [
                "product_id" => 2,
                "language_id" => 2,
                "name" => "stalen_velg_montage",
                "short_description" => "stalen_velg_montage",
                "description" => "stalen_velg_montage",
                "specification" => "stalen_velg_montage",
                "link" => ""
            ],
            [
                "product_id" => 3,
                "language_id" => 2,
                "name" => "stikstof_vulling",
                "short_description" => "stikstof_vulling",
                "description" => "stikstof_vulling",
                "specification" => "stikstof_vulling",
                "link" => ""
            ],
            [
                "product_id" => 4,
                "language_id" => 2,
                "name" => "banden_uitlijnen",
                "short_description" => "banden_uitlijnen",
                "description" => "banden_uitlijnen",
                "specification" => "banden_uitlijnen",
                "link" => ""
            ],
            [
                "product_id" => 5,
                "language_id" => 2,
                "name" => "banden_opslagkosten",
                "short_description" => "banden_opslagkosten",
                "description" => "banden_opslagkosten",
                "specification" => "banden_opslagkosten",
                "link" => ""
            ],
            [
                "product_id" => 6,
                "language_id" => 2,
                "name" => "test product",
                "short_description" => "test",
                "description" => "test",
                "specification" => "test",
                "link" => "test"
            ],
            [
                "product_id" => 7,
                "language_id" => 2,
                "name" => "test band",
                "short_description" => "test",
                "description" => "test",
                "specification" => "test",
                "link" => "test"
            ]
        ]);

        Schema::create('product_shop_infos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('shop_id')->unsigned();
            $table->integer('product_id')->unsigned();
            $table->integer('language_id')->unsigned();
            $table->string('name')->nullable();
            $table->text('short_description')->nullable();
            $table->text('description')->nullable();
            $table->text('specification')->nullable();
            $table->string('link')->nullable();
            $table->timestamps();

            $table->foreign('shop_id')
                ->references('id')
                ->on('shops')
                ->onDelete('cascade');

            $table->foreign('product_id')
                ->references('id')
                ->on('products')
                ->onDelete('cascade');

            $table->foreign('language_id')
                ->references('id')
                ->on('languages')
                ->onDelete('cascade');
        });

        DB::table('product_shop_infos')->insert([

            [
                "shop_id" => 3,
                "product_id" => 1,
                "language_id" => 2,
                "name" => "lichtmetalen_velg_montage",
                "short_description" => "lichtmetalen_velg_montage"
            ],
            [
                "shop_id" => 3,
                "product_id" => 2,
                "language_id" => 2,
                "name" => "stalen_velg_montage",
                "short_description" => "stalen_velg_montage"
            ],
            [
                "shop_id" => 3,
                "product_id" => 3,
                "language_id" => 2,
                "name" => "stikstof_vulling",
                "short_description" => "stikstof_vulling"
            ],
            [
                "shop_id" => 3,
                "product_id" => 4,
                "language_id" => 2,
                "name" => "banden_uitlijnen",
                "short_description" => "banden_uitlijnen"
            ],
            [
                "shop_id" => 3,
                "product_id" => 5,
                "language_id" => 2,
                "name" => "banden_opslagkosten",
                "short_description" => "banden_opslagkosten"
            ],
            [
                "shop_id" => 3,
                "product_id" => 6,
                "language_id" => 2,
                "name" => "test product",
                "short_description" => "test"
            ],
            [
                "shop_id" => 3,
                "product_id" => 7,
                "language_id" => 2,
                "name" => "test band",
                "short_description" => "test"
            ]
        ]);

        Schema::create('product_feature_options', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_feature_id')->unsigned();
            $table->timestamps();

            $table->foreign('product_feature_id')
                ->references('id')
                ->on('product_features')
                ->onDelete('cascade');
        });

        Schema::create('product_feature_option_languages', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_feature_option_id')->unsigned();
            $table->integer('language_id')->unsigned();
            $table->string('name');
            $table->timestamps();

            $table->foreign('product_feature_option_id', 'pfo_id_foreign')
                ->references('id')
                ->on('product_feature_options')
                ->onDelete('cascade');

            $table->foreign('language_id')
                ->references('id')
                ->on('languages')
                ->onDelete('cascade');
        });

        Schema::create('product_attribute_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
        });

        DB::table('product_attribute_types')->insert([
            [
                'name' => 'dropdown-list',
            ],
            [
                'name' => 'radio-buttons',
            ],
            [
                'name' => 'color',
            ]
        ]);

        Schema::create('product_attributes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_attribute_type_id')->unsigned();
            $table->timestamps();

            $table->foreign('product_attribute_type_id')
                ->references('id')
                ->on('product_attribute_types')
                ->onDelete('cascade');
        });

        Schema::create('product_attribute_options', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_attribute_id')->unsigned();
            $table->string('color')->nullable();
            $table->timestamps();

            $table->foreign('product_attribute_id')
                ->references('id')
                ->on('product_attributes')
                ->onDelete('cascade');
        });

        Schema::create('product_attribute_languages', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_attribute_id')->unsigned();
            $table->integer('language_id')->unsigned();
            $table->string('name');
            $table->timestamps();

            $table->foreign('product_attribute_id')
                ->references('id')
                ->on('product_attributes')
                ->onDelete('cascade');

            $table->foreign('language_id')
                ->references('id')
                ->on('languages')
                ->onDelete('cascade');
        });

        Schema::create('product_attribute_option_languages', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_attribute_option_id')->unsigned();
            $table->integer('language_id')->unsigned();
            $table->string('name');
            $table->timestamps();

            $table->foreign('product_attribute_option_id', 'pao_id_foreign')
                ->references('id')
                ->on('product_attribute_options')
                ->onDelete('cascade');

            $table->foreign('language_id')
                ->references('id')
                ->on('languages')
                ->onDelete('cascade');
        });

        Schema::create('product_stocks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('warehouse_id')->unsigned();
            $table->integer('product_id')->unsigned();
            $table->integer('amount');
            $table->decimal('price')->nullable()->default(0.0);
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('warehouse_id')
                ->references('id')
                ->on('warehouses')
                ->onDelete('cascade');

            $table->foreign('product_id')
                ->references('id')
                ->on('products')
                ->onDelete('cascade');
        });

        DB::table('product_stocks')->insert([
            [
                'warehouse_id' => 1,
                'product_id' => 1,
                'amount' => 5
            ],
            [
                'warehouse_id' => 1,
                'product_id' => 2,
                'amount' => 2
            ],            [
                'warehouse_id' => 1,
                'product_id' => 3,
                'amount' => 5
            ],
            [
                'warehouse_id' => 1,
                'product_id' => 4,
                'amount' => 2
            ],            [
                'warehouse_id' => 1,
                'product_id' => 5,
                'amount' => 5
            ],
            [
                'warehouse_id' => 1,
                'product_id' => 6,
                'amount' => 2
            ],            [
                'warehouse_id' => 1,
                'product_id' => 7,
                'amount' => 5
            ]
        ]);

        Schema::create('product_tags', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('language_id')->unsigned();
            $table->string('name');
            $table->timestamps();

            $table->foreign('language_id')
                ->references('id')
                ->on('languages')
                ->onDelete('cascade');
        });

        Schema::create('product_product_tag', function (Blueprint $table) {
            $table->integer('product_id')->unsigned();
            $table->integer('product_tag_id')->unsigned();

            $table->foreign('product_id')
                ->references('id')
                ->on('products')
                ->onDelete('cascade');

            $table->foreign('product_tag_id')
                ->references('id')
                ->on('product_tags')
                ->onDelete('cascade');

            $table->primary(['product_id', 'product_tag_id']);
        });

        Schema::create('product_product_feature_option', function (Blueprint $table) {
            $table->integer('product_id')->unsigned();
            $table->integer('product_feature_option_id')->unsigned();

            $table->foreign('product_id')
                ->references('id')
                ->on('products')
                ->onDelete('cascade');

            $table->foreign('product_feature_option_id')
                ->references('id')
                ->on('product_feature_options')
                ->onDelete('cascade');

            $table->primary(['product_id' , 'product_feature_option_id'], 'pfoi');
        });

        Schema::create('product_combinations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_id')->unsigned();
            $table->boolean('default');
            $table->string('supplier_sku')->nullable();
            $table->string('ean')->nullable();
            $table->string('upc')->nullable();
            $table->string('sku')->nullable();
            $table->integer('minimum_qty')->default(1);
            $table->integer('impact_price_type')->nullable();
            $table->decimal('impact_price')->nullable();
            $table->integer('impact_weight_type')->nullable();
            $table->decimal('impact_weight', 10, 2)->nullable();
            $table->integer('impact_unit_price_type')->nullable();
            $table->decimal('impact_unit_price', 10, 2)->nullable();
            $table->timestamps();

            $table->foreign('product_id')
                ->references('id')
                ->on('products')
                ->onDelete('cascade');
        });

        Schema::create('product_combination_stocks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_combination_id')->unsigned();
            $table->integer('warehouse_id')->unsigned();
            $table->integer('amount');
            $table->timestamps();

            $table->foreign('product_combination_id')
                ->references('id')
                ->on('product_combinations')
                ->onDelete('cascade');

            $table->foreign('warehouse_id')
                ->references('id')
                ->on('warehouses')
                ->onDelete('cascade');
        });

        Schema::create('product_attribute_option_product_combination', function (Blueprint $table) {
            $table->integer('product_attribute_option_id')->unsigned();
            $table->integer('product_combination_id')->unsigned();

            $table->foreign('product_attribute_option_id', 'fk_paoid')
                ->references('id')
                ->on('product_attribute_options')
                ->onDelete('cascade');

            $table->foreign('product_combination_id', 'fk_pcid')
                ->references('id')
                ->on('product_combinations')
                ->onDelete('cascade');

            $table->primary(['product_attribute_option_id', 'product_combination_id'], 'paoid');
        });

        Schema::create('product_combination_product_image', function (Blueprint $table) {
            $table->integer('product_combination_id')->unsigned();
            $table->integer('product_image_id')->unsigned();

            $table->foreign('product_combination_id', 'fk_pcid2')
                ->references('id')
                ->on('product_combinations')
                ->onDelete('cascade');

            $table->foreign('product_image_id', 'fk_piid')
                ->references('id')
                ->on('product_images')
                ->onDelete('cascade');

            $table->primary(['product_combination_id', 'product_image_id'], 'pcid');
        });

        Schema::create('shop_products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('shop_id')->unsigned();
            $table->integer('product_id')->unsigned();
            $table->integer('tax_rule_id')->nullable()->unsigned();
            $table->boolean('featured')->default(0);
            $table->integer('minimum_qty')->default(1);
            $table->boolean('stock_management')->default(0);
            $table->boolean('active_when_out_of_stock')->default(0);
            $table->boolean('deny_order_when_out_of_stock')->default(1);
            $table->decimal('additional_fee_per_item')->nullable()->default(0.0);
            $table->boolean('active')->default(1);
            $table->timestamps();

            $table->foreign('shop_id')
                ->references('id')
                ->on('shops')
                ->onDelete('cascade');

            $table->foreign('product_id')
                ->references('id')
                ->on('products')
                ->onDelete('cascade');

            $table->foreign('tax_rule_id')
                ->references('id')
                ->on('tax_rules')
                ->onDelete('cascade');
        });

        DB::table('shop_products')->insert([
            [
                'shop_id' => 3,
                'product_id' => 1,
                'tax_rule_id' => 14
            ],
            [
                'shop_id' => 3,
                'product_id' => 2,
                'tax_rule_id' => 14
            ],
            [
                'shop_id' => 3,
                'product_id' => 3,
                'tax_rule_id' => 14
            ],
            [
                'shop_id' => 3,
                'product_id' => 4,
                'tax_rule_id' => 14
            ],
            [
                'shop_id' => 3,
                'product_id' => 5,
                'tax_rule_id' => 14
            ],
            [
                'shop_id' => 3,
                'product_id' => 6,
                'tax_rule_id' => 14
            ],
            [
                'shop_id' => 3,
                'product_id' => 7,
                'tax_rule_id' => 14
            ]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shop_products');
        Schema::dropIfExists('product_combination_product_image');
        Schema::dropIfExists('product_attribute_option_product_combination');
        Schema::dropIfExists('product_combination_stocks');
        Schema::dropIfExists('product_combinations');
        Schema::dropIfExists('product_product_feature_option');
        Schema::dropIfExists('product_product_tag');
        Schema::dropIfExists('product_tags');
        Schema::dropIfExists('product_stocks');
        Schema::dropIfExists('product_attribute_option_languages');
        Schema::dropIfExists('product_attribute_languages');
        Schema::dropIfExists('product_attribute_options');
        Schema::dropIfExists('product_attributes');
        Schema::dropIfExists('product_attribute_types');
        Schema::dropIfExists('product_feature_option_languages');
        Schema::dropIfExists('product_feature_options');
        Schema::dropIfExists('product_shop_infos');
        Schema::dropIfExists('product_infos');
        Schema::dropIfExists('product_shop_images');
        Schema::dropIfExists('product_images');
        Schema::dropIfExists('product_shop_bulk_prices');
        Schema::dropIfExists('product_shop_prices');
        Schema::dropIfExists('product_prices');
        Schema::dropIfExists('product_bulk_prices');
        Schema::dropIfExists('products');
        Schema::dropIfExists('product_groups');
        Schema::dropIfExists('product_feature_languages');
        Schema::dropIfExists('product_features');
        Schema::dropIfExists('product_feature_type_languages');
        Schema::dropIfExists('product_feature_types');
    }
}
