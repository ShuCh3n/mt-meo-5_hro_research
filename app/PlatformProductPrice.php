<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlatformProductPrice extends Model
{
	protected $fillable = ['platform_id', 'product_id', 'price'];

    public function platform()
    {
    	return $this->belongsTo(Platform::class);
    }

    public function product()
    {
    	return $this->belongsTo(Product::class);
    }

    public function scopePlatform($query, $id)
    {
    	return $query->where('platform_id', $id);
    }
}
