@extends('dashboard.layout.main')

@section('content')
	<div class="wrapper wrapper-content animated fadeInRight ecommerce">
            <div class="ibox-content m-b-sm border-bottom">
                <div class="row">
                    <div class="col-xs-12">
                        <a class="btn btn-primary pull-right" href="{{ route('new_page', ['shop_id' => $shop->id]) }}"><i class="fa fa-plus-circle" aria-hidden="true"></i> New page</a>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox">
                        <div class="ibox-content">

                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th data-toggle="true">Title</th>
                                        <th class="text-right" data-sort-ignore="true">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($shop->pages as $page)
                                        <tr>
                                            <td>
                                               {{ $page->info()->title }}
                                            </td>
                                            <td class="text-right">
                                                <div class="btn-group">
                                                    <a href="{{ route('shop_pages', $page->id) }}" class="btn-white btn btn-xs">View</a>
                                                    <a href="{{ route('edit_page', $page->id) }}" class="btn-white btn btn-xs">Edit</a>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@stop