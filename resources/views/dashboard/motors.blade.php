@extends('dashboard.layout.main')

@section('content')
	    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-6">
            <h2>{{ trans('pages.inquiry_detail_header') }}</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('home') }}">{{ trans('menu.home') }}</a>
                </li>
                <li>
                    <a href="">{{ trans('menu.inquiries') }}</a>
                </li>
                <li class="active">
                    <strong>{{ trans('page.inquiry') }}</strong>
                </li>
            </ol>
        </div>
    </div>

	<div class="wrapper wrapper-content animated fadeInRight ecommerce">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-title">
                        <h5>@lang('motor.filters')</h5>
                    </div>

                    <div class="ibox-content">
                        <div class="mb20"></div>

                        <div class="row m-b-lg">
                            <div class="col-md-12">

                                <form class="form-horizontal" method="get">
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">@lang('motor.search')</label>
                                        <div class="col-md-10">
                                            <input type="text" class="form-control" name="q" placeholder="Code / Name / Model" value="{{ request()->q }}">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">@lang('motor.brand')</label>
                                        <div class="col-md-10">
                                            <select2 multiple name="filter[brand][]" select="{{ (isset(request()->filter['brand']))? select2_multiple(request()->filter['brand']) : '' }}">
                                                @foreach($brands as $brand)
                                                    <option value="{{ $brand->id }}">{{ $brand->name }}</option>
                                                @endforeach
                                            </select2>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">@lang('motor.years')</label>
                                        <div class="col-md-10">
                                            <select2 multiple name="filter[year][]" select="{{ (isset(request()->filter['year']))? select2_multiple(request()->filter['year']) : '' }}">
                                                @foreach($vehicle_years as $year)
                                                    <option value="{{ $year->year }}">{{ $year->year }}</option>
                                                @endforeach
                                            </select2>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">@lang('motor.cc')</label>
                                        <div class="col-md-10">
                                            <select2 multiple name="filter[cc][]" select="{{ (isset(request()->filter['cc']))? select2_multiple(request()->filter['cc']) : '' }}">
                                                @foreach($ccs as $cc)
                                                    <option value="{{ $cc->cc }}">{{ $cc->cc }}</option>
                                                @endforeach
                                            </select2>
                                        </div>
                                    </div>

                                    <button class="btn btn-primary pull-right"><i class="fa fa-filter" aria-hidden="true"></i> @lang('motor.filter')</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="ibox">
                    <div class="ibox-title">
                        <h5>@lang('motor.motors')</h5>
                        <div class="ibox-tools">
                            <a href="{{ route('new_motor') }}" class="btn btn-primary btn-xs">@lang('motor.new_motor')</a>
                        </div>
                    </div>

                    <div class="ibox-content">

                        <form action="{{ route('product_bulk_edit') }}" method="post">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th>@lang('motor.id')</th>
                                    <th>@lang('motor.code')</th>
                                    <th>Brand</th>
                                    <th>@lang('motor.name')</th>
                                    <th>@lang('motor.model')</th>
                                    <th>@lang('motor.cc')</th>
                                    <th class="text-right">@lang('motor.action')</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach($motors as $motor)
                                        <tr>
                                            <td>
                                                {{ $motor->id }}
                                            </td>
                                            <td>
                                                {{ $motor->code }}
                                            </td>
                                            <td>
                                                {{ $motor->vehicle_brand->name ?? '' }}
                                            </td>
                                            <td>
                                                {{ $motor->name ?? '' }}
                                            </td>
                                            <td>
                                                {{ $motor->model ?? '' }}
                                            </td>
                                            <td>
                                                {{ $motor->cc ?? '' }}
                                            </td>
                                            <td class="text-right">
                                                <div class="btn-group">
                                                    <a class="btn-white btn btn-xs" href="{{ route('view_motor', $motor->id) }}">@lang('motor.view')</a>
                                                    <a class="btn-white btn btn-xs" href="{{ route('edit_motor', $motor->id) }}">@lang('motor.edit')</a>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </form>

                        <div class="text-center">
                            {{ $motors->links() }}
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
