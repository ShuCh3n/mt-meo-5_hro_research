<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLanguagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('languages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('iso', 2);
            $table->string('locale')->unique();
            $table->string('icon');
        });

        Schema::create('currencies', function(Blueprint $table){
            $table->increments('id');
            $table->string('name');
            $table->string('decimal_separator', 1);
            $table->string('thousand_separator', 1);
            $table->string('icon');
            $table->string('symbol_icon', 5);
            $table->string('symbol', 5);
            $table->decimal('rate', 10, 5);
        });

        Schema::create('countries', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('iso_code_2', 2);
            $table->string('iso_code_3');
            $table->string('zipcode_format')->nullable();
            $table->integer('call_prefix');
            $table->string('icon');
        });

        Schema::create('states', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('country_id')->unsigned();
            $table->string('name');
            $table->string('iso')->nullable();

            $table->foreign('country_id')
                ->references('id')
                ->on('countries')
                ->onDelete('cascade');
        });

        DB::table('currencies')->insert([
            [
                "name"                  => "us_dollar",
                "decimal_separator"     => ".",
                "thousand_separator"    => ",",
                "icon"                  => "United-States",
                "symbol_icon"           => "$",
                "symbol"                => "USD",
                "rate"                  => 1.00000
            ],
            [
                "name"                  => "euro",
                "decimal_separator"     => ",",
                "thousand_separator"    => ".",
                "icon"                  => "European-Union",
                "symbol_icon"           => "€",
                "symbol"                => "EUR",
                "rate"                  => 1.00000
            ],
            [
                "name"                  => "japanese_yen",
                "decimal_separator"     => ".",
                "thousand_separator"    => ",",
                "icon"                  => "Japan",
                "symbol_icon"           => "¥",
                "symbol"                => "JPY",
                "rate"                  => 1.00000
            ],
            [
                "name"                  => "pound",
                "decimal_separator"     => ".",
                "thousand_separator"    => ",",
                "icon"                  => "United-Kingdom",
                "symbol_icon"           => "£",
                "symbol"                => "GBP",
                "rate"                  => 1.00000
            ],
            [
                "name"                  => "taiwanese_dollar",
                "decimal_separator"     => ".",
                "thousand_separator"    => ",",
                "icon"                  => "Taiwan",
                "symbol_icon"           => "NT$",
                "symbol"                => "NTD",
                "rate"                  => 1.00000
            ],
            [
                "name"                  => "australian_dollar",
                "decimal_separator"     => ".",
                "thousand_separator"    => ",",
                "icon"                  => "Australia",
                "symbol_icon"           => "$",
                "symbol"                => "AUD",
                "rate"                  => 1.00000
            ],
            [
                "name"                  => "swiss_franc",
                "decimal_separator"     => ".",
                "thousand_separator"    => ",",
                "icon"                  => "Switzerland",
                "symbol_icon"           => "Fr",
                "symbol"                => "CHF",
                "rate"                  => 1.00000
            ],
            [
                "name"                  => "mexican_peso",
                "decimal_separator"     => ".",
                "thousand_separator"    => ",",
                "icon"                  => "Mexico",
                "symbol_icon"           => "$",
                "symbol"                => "MXN",
                "rate"                  => 1.00000
            ],
            [
                "name"                  => "chinese_yuan",
                "decimal_separator"     => ".",
                "thousand_separator"    => ",",
                "icon"                  => "China",
                "symbol_icon"           => "¥",
                "symbol"                => "CNY",
                "rate"                  => 1.00000
            ],
            [
                "name"                  => "new_zealand_dollar",
                "decimal_separator"     => ".",
                "thousand_separator"    => ",",
                "icon"                  => "New-Zealand",
                "symbol_icon"           => "$",
                "symbol"                => "NZD",
                "rate"                  => 1.00000
            ],
            [
                "name"                  => "swedish_krona",
                "decimal_separator"     => ".",
                "thousand_separator"    => ",",
                "icon"                  => "Sweden",
                "symbol_icon"           => "kr",
                "symbol"                => "SEK",
                "rate"                  => 1.00000
            ],
            [
                "name"                  => "russian_ruble",
                "decimal_separator"     => ".",
                "thousand_separator"    => ",",
                "icon"                  => "Russia",
                "symbol_icon"           => "₽",
                "symbol"                => "RUB",
                "rate"                  => 1.00000
            ],
            [
                "name"                  => "hong_kong_dollar",
                "decimal_separator"     => ".",
                "thousand_separator"    => ",",
                "icon"                  => "Hong-Kong",
                "symbol_icon"           => "$",
                "symbol"                => "HKD",
                "rate"                  => 1.00000
            ],
            [
                "name"                  => "norwegian_krone",
                "decimal_separator"     => ".",
                "thousand_separator"    => ",",
                "icon"                  => "Norway",
                "symbol_icon"           => "kr",
                "symbol"                => "NOK",
                "rate"                  => 1.00000
            ],
            [
                "name"                  => "singapore_dollar",
                "decimal_separator"     => ".",
                "thousand_separator"    => ",",
                "icon"                  => "Singapore",
                "symbol_icon"           => "$",
                "symbol"                => "SGD",
                "rate"                  => 1.00000
            ],
            [
                "name"                  => "turkish_lira",
                "decimal_separator"     => ".",
                "thousand_separator"    => ",",
                "icon"                  => "Turkey",
                "symbol_icon"           => "₺",
                "symbol"                => "TRY",
                "rate"                  => 1.00000
            ],
            [
                "name"                  => "south_korean_won",
                "decimal_separator"     => ".",
                "thousand_separator"    => ",",
                "icon"                  => "South-Korea",
                "symbol_icon"           => "₩",
                "symbol"                => "KRW",
                "rate"                  => 1.00000
            ],
            [
                "name"                  => "south_african_rand",
                "decimal_separator"     => ".",
                "thousand_separator"    => ",",
                "icon"                  => "South-Africa",
                "symbol_icon"           => "R",
                "symbol"                => "ZAR",
                "rate"                  => 1.00000
            ],
            [
                "name"                  => "brazillian_real",
                "decimal_separator"     => ".",
                "thousand_separator"    => ",",
                "icon"                  => "Brazil",
                "symbol_icon"           => "R$",
                "symbol"                => "BRL",
                "rate"                  => 1.00000
            ],
            [
                "name"                  => "indian_rupee",
                "decimal_separator"     => ".",
                "thousand_separator"    => ",",
                "icon"                  => "India",
                "symbol_icon"           => "₹",
                "symbol"                => "INR",
                "rate"                  => 1.00000
            ],
        ]);

        DB::table('countries')->insert([
            [
                "name"              => "afghanistan",
                "iso_code_2"        => "AF",
                "iso_code_3"        => "AFG",
                "call_prefix"       => 376,
                "icon"              => "Afghanistan"
            ],
            [
                "name"              => "albania",
                "iso_code_2"        => "AL",
                "iso_code_3"        => "ALB",
                "call_prefix"       => 355,
                "icon"              => "Albania"
            ],
            [
                "name"              => "algeria",
                "iso_code_2"        => "DZ",
                "iso_code_3"        => "DZA",
                "call_prefix"       => 213,
                "icon"              => "Algeria"
            ],
            [
                "name"              => "american_samoa",
                "iso_code_2"        => "AS",
                "iso_code_3"        => "ASM",
                "call_prefix"       =>  1684,
                "icon"              => "American-Samoa"
            ],
            [
                "name"              => "andorra",
                "iso_code_2"        => "AD",
                "iso_code_3"        => "AND",
                "call_prefix"       =>  376,
                "icon"              => "Andorra"
            ],
            [
                "name"              => "angola",
                "iso_code_2"        => "AO",
                "iso_code_3"        => "AGO",
                "call_prefix"       =>  244,
                "icon"              => "Angola"
            ],
            [
                "name"              => "anguilla",
                "iso_code_2"        => "AI",
                "iso_code_3"        => "AIA",
                "call_prefix"       =>  1264,
                "icon"              => "Anguilla"
            ],
            [
                "name"              => "antigua_and_barbuda",
                "iso_code_2"        => "AG",
                "iso_code_3"        => "ATG",
                "call_prefix"       =>  1268,
                "icon"              => "Anguilla"
            ],
            [
                "name"              => "argentina",
                "iso_code_2"        => "AR",
                "iso_code_3"        => "ARG",
                "call_prefix"       =>  54,
                "icon"              => "Argentina"
            ],
            [
                "name"              => "armenia",
                "iso_code_2"        => "AM",
                "iso_code_3"        => "ARM",
                "call_prefix"       =>  374,
                "icon"              => "Armenia"
            ],
            [
                "name"              => "aruba",
                "iso_code_2"        => "AW",
                "iso_code_3"        => "ABW",
                "call_prefix"       =>  297,
                "icon"              => "Aruba"
            ],
            [
                "name"              => "australia",
                "iso_code_2"        => "AU",
                "iso_code_3"        => "AUS",
                "call_prefix"       =>  61,
                "icon"              => "Australia"
            ],
            [
                "name"              => "austria",
                "iso_code_2"        => "AT",
                "iso_code_3"        => "AUT",
                "call_prefix"       =>  43,
                "icon"              => "Austria"
            ],
            [
                "name"              => "azerbaijan",
                "iso_code_2"        => "AZ",
                "iso_code_3"        => "AZE",
                "call_prefix"       =>  994,
                "icon"              => "Azerbaijan"
            ],
            [
                "name"              => "bahamas",
                "iso_code_2"        => "BS",
                "iso_code_3"        => "BHS",
                "call_prefix"       =>  1242,
                "icon"              => "Bahamas"
            ],
            [
                "name"              => "bahrain",
                "iso_code_2"        => "BH",
                "iso_code_3"        => "BHR",
                "call_prefix"       =>  973,
                "icon"              => "Bahrain"
            ],
            [
                "name"              => "bangladesh",
                "iso_code_2"        => "BD",
                "iso_code_3"        => "BGD",
                "call_prefix"       =>  880,
                "icon"              => "Bangladesh"
            ],
            [
                "name"              => "barbados",
                "iso_code_2"        => "BB",
                "iso_code_3"        => "BRB",
                "call_prefix"       =>  1246,
                "icon"              => "Barbados"
            ],
            [
                "name"              => "belarus",
                "iso_code_2"        => "BY",
                "iso_code_3"        => "BLR",
                "call_prefix"       =>  375,
                "icon"              => "Belarus"
            ],
            [
                "name"              => "belgium",
                "iso_code_2"        => "BE",
                "iso_code_3"        => "BEL",
                "call_prefix"       =>  32,
                "icon"              => "Belgium"
            ],
            [
                "name"              => "belize",
                "iso_code_2"        => "BZ",
                "iso_code_3"        => "BLZ",
                "call_prefix"       =>  501,
                "icon"              => "Belize"
            ],
            [
                "name"              => "benin",
                "iso_code_2"        => "BJ",
                "iso_code_3"        => "BEN",
                "call_prefix"       =>  229,
                "icon"              => "Benin"
            ],
            [
                "name"              => "bermuda",
                "iso_code_2"        => "BM",
                "iso_code_3"        => "BMU",
                "call_prefix"       =>  1441,
                "icon"              => "Bermuda"
            ],
            [
                "name"              => "bhutan",
                "iso_code_2"        => "BT",
                "iso_code_3"        => "BTN",
                "call_prefix"       =>  975,
                "icon"              => "Bhutan"
            ],
            [
                "name"              => "bolivia",
                "iso_code_2"        => "BO",
                "iso_code_3"        => "BOL",
                "call_prefix"       =>  591,
                "icon"              => "Bolivia"
            ],
            [
                "name"              => "bosnia_and_herzegovina",
                "iso_code_2"        => "BA",
                "iso_code_3"        => "BIH",
                "call_prefix"       =>  387,
                "icon"              => "Bosnia-and-Herzegovina"
            ],
            [
                "name"              => "botswana",
                "iso_code_2"        => "BW",
                "iso_code_3"        => "BWA",
                "call_prefix"       =>  267,
                "icon"              => "Botswana"
            ],
            [
                "name"              => "brazil",
                "iso_code_2"        => "BR",
                "iso_code_3"        => "BRA",
                "call_prefix"       =>  55,
                "icon"              => "Brazil"
            ],
            [
                "name"              => "brunei_darussalam",
                "iso_code_2"        => "BN",
                "iso_code_3"        => "BRN",
                "call_prefix"       =>  673,
                "icon"              => "Brunei"
            ],
            [
                "name"              => "bulgaria",
                "iso_code_2"        => "BG",
                "iso_code_3"        => "BGR",
                "call_prefix"       =>  359,
                "icon"              => "Bulgaria"
            ],
            [
                "name"              => "burkina_faso",
                "iso_code_2"        => "BF",
                "iso_code_3"        => "BFA",
                "call_prefix"       =>  226,
                "icon"              => "Burkina-Faso"
            ],
            [
                "name"              => "burundi",
                "iso_code_2"        => "BI",
                "iso_code_3"        => "BDI",
                "call_prefix"       =>  257,
                "icon"              => "Burundi"
            ],
            [
                "name"              => "cambodia",
                "iso_code_2"        => "KH",
                "iso_code_3"        => "KHM",
                "call_prefix"       =>  855,
                "icon"              => "Cambodia"
            ],
            [
                "name"              => "cameroon",
                "iso_code_2"        => "CM",
                "iso_code_3"        => "CMR",
                "call_prefix"       =>  237,
                "icon"              => "Cameroon"
            ],
            [
                "name"              => "canada",
                "iso_code_2"        => "CA",
                "iso_code_3"        => "CAN",
                "call_prefix"       =>  1,
                "icon"              => "Canada"
            ],
            [
                "name"              => "cape_verde",
                "iso_code_2"        => "CV",
                "iso_code_3"        => "CPV",
                "call_prefix"       =>  238,
                "icon"              => "Cape-Verde"
            ],
            [
                "name"              => "cayman_islands",
                "iso_code_2"        => "KY",
                "iso_code_3"        => "CYM",
                "call_prefix"       =>  1345,
                "icon"              => "Cayman-Islands"
            ],
            [
                "name"              => "central_african_republic",
                "iso_code_2"        => "CF",
                "iso_code_3"        => "CAF",
                "call_prefix"       =>  236,
                "icon"              => "Central-African-Republic"
            ],
            [
                "name"              => "chad",
                "iso_code_2"        => "TD",
                "iso_code_3"        => "TCD",
                "call_prefix"       =>  235,
                "icon"              => "Chad"
            ],
            [
                "name"              => "chile",
                "iso_code_2"        => "CL",
                "iso_code_3"        => "CHL",
                "call_prefix"       =>  56,
                "icon"              => "Chile"
            ],
            [
                "name"              => "china",
                "iso_code_2"        => "CN",
                "iso_code_3"        => "CHN",
                "call_prefix"       =>  86,
                "icon"              => "China"
            ],
            [
                "name"              => "colombia",
                "iso_code_2"        => "CO",
                "iso_code_3"        => "COL",
                "call_prefix"       =>  57,
                "icon"              => "Colombia"
            ],
            [
                "name"              => "comoros",
                "iso_code_2"        => "KM",
                "iso_code_3"        => "COM",
                "call_prefix"       =>  269,
                "icon"              => "Comoros"
            ],
            [
                "name"              => "republic_of_the_congo",
                "iso_code_2"        => "CG",
                "iso_code_3"        => "COG",
                "call_prefix"       =>  242,
                "icon"              => "Republic-of-the-Congo"
            ],
            [
                "name"              => "democratic_republic_of_the_congo",
                "iso_code_2"        => "CD",
                "iso_code_3"        => "COD",
                "call_prefix"       =>  243,
                "icon"              => "Democratic-Republic-of-the-Congo"
            ],
            [
                "name"              => "cook_islands",
                "iso_code_2"        => "CK",
                "iso_code_3"        => "COK",
                "call_prefix"       =>  682,
                "icon"              => "Cook-Islands"
            ],
            [
                "name"              => "costa_rica",
                "iso_code_2"        => "CR",
                "iso_code_3"        => "CRI",
                "call_prefix"       =>  506,
                "icon"              => "Costa-Rica"
            ],
            [
                "name"              => "côte_d'ivoire",
                "iso_code_2"        => "CI",
                "iso_code_3"        => "CIV",
                "call_prefix"       =>  225,
                "icon"              => "Cote-dIvoire"
            ],
            [
                "name"              => "croatia",
                "iso_code_2"        => "HR",
                "iso_code_3"        => "HRV",
                "call_prefix"       =>  385,
                "icon"              => "Croatia"
            ],
            [
                "name"              => "cuba",
                "iso_code_2"        => "CU",
                "iso_code_3"        => "CUB",
                "call_prefix"       =>  53,
                "icon"              => "Cuba"
            ],
            [
                "name"              => "curaçao",
                "iso_code_2"        => "CW",
                "iso_code_3"        => "CUW",
                "call_prefix"       =>  599,
                "icon"              => "Curacao"
            ],
            [
                "name"              => "cyprus",
                "iso_code_2"        => "CY",
                "iso_code_3"        => "CYP",
                "call_prefix"       =>  357,
                "icon"              => "Cyprus"
            ],
            [
                "name"              => "czech_republic",
                "iso_code_2"        => "CZ",
                "iso_code_3"        => "CZE",
                "call_prefix"       =>  420,
                "icon"              => "Czech-Republic"
            ],
            [
                "name"              => "denmark",
                "iso_code_2"        => "DK",
                "iso_code_3"        => "DNK",
                "call_prefix"       =>  45,
                "icon"              => "Denmark"
            ],
            [
                "name"              => "djibouti",
                "iso_code_2"        => "DJ",
                "iso_code_3"        => "DJI",
                "call_prefix"       =>  253,
                "icon"              => "Djibouti"
            ],
            [
                "name"              => "dominica",
                "iso_code_2"        => "DM",
                "iso_code_3"        => "DMA",
                "call_prefix"       =>  1767,
                "icon"              => "Dominica"
            ],
            [
                "name"              => "dominican_republic",
                "iso_code_2"        => "DO",
                "iso_code_3"        => "DOM",
                "call_prefix"       =>  1,
                "icon"              => "Dominican-Republic"
            ],
            [
                "name"              => "ecuador",
                "iso_code_2"        => "EC",
                "iso_code_3"        => "ECU",
                "call_prefix"       =>  593,
                "icon"              => "Ecuador"
            ],
            [
                "name"              => "egypt",
                "iso_code_2"        => "EG",
                "iso_code_3"        => "EGY",
                "call_prefix"       =>  20,
                "icon"              => "Egypt"
            ],
            [
                "name"              => "el_salvador",
                "iso_code_2"        => "SV",
                "iso_code_3"        => "SLV",
                "call_prefix"       =>  503,
                "icon"              => "El-Salvador"
            ],
            [
                "name"              => "equatorial_guinea",
                "iso_code_2"        => "GQ",
                "iso_code_3"        => "GNQ",
                "call_prefix"       =>  240,
                "icon"              => "Equatorial-Guinea"
            ],
            [
                "name"              => "eritrea",
                "iso_code_2"        => "ER",
                "iso_code_3"        => "ERI",
                "call_prefix"       =>  291,
                "icon"              => "Eritrea"
            ],
            [
                "name"              => "estonia",
                "iso_code_2"        => "EE",
                "iso_code_3"        => "EST",
                "call_prefix"       =>  372,
                "icon"              => "Estonia"
            ],
            [
                "name"              => "ethiopia",
                "iso_code_2"        => "ET",
                "iso_code_3"        => "ETH",
                "call_prefix"       =>  251,
                "icon"              => "Ethiopia"
            ],
            [
                "name"              => "falkland_island",
                "iso_code_2"        => "FK",
                "iso_code_3"        => "FLK",
                "call_prefix"       =>  500,
                "icon"              => "Falkland-Islands"
            ],
            [
                "name"              => "faroe_islands",
                "iso_code_2"        => "FO",
                "iso_code_3"        => "FRO",
                "call_prefix"       =>  298,
                "icon"              => "Faroes"
            ],
            [
                "name"              => "fiji",
                "iso_code_2"        => "FJ",
                "iso_code_3"        => "FJI",
                "call_prefix"       =>  679,
                "icon"              => "Fiji"
            ],
            [
                "name"              => "finland",
                "iso_code_2"        => "FI",
                "iso_code_3"        => "FIN",
                "call_prefix"       =>  358,
                "icon"              => "Finland"
            ],
            [
                "name"              => "france",
                "iso_code_2"        => "FR",
                "iso_code_3"        => "FRA",
                "call_prefix"       =>  33,
                "icon"              => "France"
            ],
            [
                "name"              => "french_polynesia",
                "iso_code_2"        => "PF",
                "iso_code_3"        => "PYF",
                "call_prefix"       =>  689,
                "icon"              => "French-Polynesia"
            ],
            [
                "name"              => "gabon",
                "iso_code_2"        => "GA",
                "iso_code_3"        => "GAB",
                "call_prefix"       =>  241,
                "icon"              => "Gabon"
            ],
            [
                "name"              => "gambia",
                "iso_code_2"        => "GM",
                "iso_code_3"        => "GMB",
                "call_prefix"       =>  220,
                "icon"              => "Gambia"
            ],
            [
                "name"              => "georgia",
                "iso_code_2"        => "GE",
                "iso_code_3"        => "GEO",
                "call_prefix"       =>  995,
                "icon"              => "Georgia"
            ],
            [
                "name"              => "germany",
                "iso_code_2"        => "DE",
                "iso_code_3"        => "DEU",
                "call_prefix"       =>  49,
                "icon"              => "Germany"
            ],
            [
                "name"              => "ghana",
                "iso_code_2"        => "GH",
                "iso_code_3"        => "GHA",
                "call_prefix"       =>  233,
                "icon"              => "Ghana"
            ],
            [
                "name"              => "gibraltar",
                "iso_code_2"        => "GI",
                "iso_code_3"        => "GIB",
                "call_prefix"       =>  350,
                "icon"              => "Gibraltar"
            ],
            [
                "name"              => "greece",
                "iso_code_2"        => "GR",
                "iso_code_3"        => "GRC",
                "call_prefix"       =>  30,
                "icon"              => "Greece"
            ],
            [
                "name"              => "greenland",
                "iso_code_2"        => "GL",
                "iso_code_3"        => "GRL",
                "call_prefix"       =>  299,
                "icon"              => "Greenland"
            ],
            [
                "name"              => "grenada",
                "iso_code_2"        => "GD",
                "iso_code_3"        => "GRD",
                "call_prefix"       =>  1473,
                "icon"              => "Grenada"
            ],
            [
                "name"              => "guam",
                "iso_code_2"        => "GU",
                "iso_code_3"        => "GUM",
                "call_prefix"       =>  1671,
                "icon"              => "Guam"
            ],
            [
                "name"              => "guatemala",
                "iso_code_2"        => "GT",
                "iso_code_3"        => "GTM",
                "call_prefix"       =>  502,
                "icon"              => "Guatemala"
            ],
            [
                "name"              => "guernsey",
                "iso_code_2"        => "GG",
                "iso_code_3"        => "GGY",
                "call_prefix"       =>  44,
                "icon"              => "Guernsey"
            ],
            [
                "name"              => "guinea",
                "iso_code_2"        => "GN",
                "iso_code_3"        => "GIN",
                "call_prefix"       =>  224,
                "icon"              => "Guinea"
            ],
            [
                "name"              => "guinea-bissau",
                "iso_code_2"        => "GW",
                "iso_code_3"        => "GNB",
                "call_prefix"       =>  245,
                "icon"              => "Guinea-Bissau"
            ],
            [
                "name"              => "guyana",
                "iso_code_2"        => "GY",
                "iso_code_3"        => "GUY",
                "call_prefix"       =>  592,
                "icon"              => "Guyana"
            ],
            [
                "name"              => "haiti",
                "iso_code_2"        => "HT",
                "iso_code_3"        => "HTI",
                "call_prefix"       =>  509,
                "icon"              => "Haiti"
            ],
            [
                "name"              => "honduras",
                "iso_code_2"        => "HN",
                "iso_code_3"        => "HND",
                "call_prefix"       =>  504,
                "icon"              => "Honduras"
            ],
            [
                "name"              => "hong_kong",
                "iso_code_2"        => "HK",
                "iso_code_3"        => "HKG",
                "call_prefix"       =>  852,
                "icon"              => "Hong-Kong"
            ],
            [
                "name"              => "hungary",
                "iso_code_2"        => "Hu",
                "iso_code_3"        => "HUN",
                "call_prefix"       =>  36,
                "icon"              => "Hungary"
            ],
            [
                "name"              => "iceland",
                "iso_code_2"        => "IS",
                "iso_code_3"        => "ISL",
                "call_prefix"       =>  354,
                "icon"              => "Iceland"
            ],
            [
                "name"              => "india",
                "iso_code_2"        => "IN",
                "iso_code_3"        => "IND",
                "call_prefix"       =>  91,
                "icon"              => "India"
            ],
            [
                "name"              => "indonesia",
                "iso_code_2"        => "ID",
                "iso_code_3"        => "IDN",
                "call_prefix"       =>  62,
                "icon"              => "Indonesia"
            ],
            [
                "name"              => "iran",
                "iso_code_2"        => "IR",
                "iso_code_3"        => "IRNIR",
                "call_prefix"       =>  98,
                "icon"              => "Iran"
            ],
            [
                "name"              => "iraq",
                "iso_code_2"        => "IQ",
                "iso_code_3"        => "IRQ",
                "call_prefix"       =>  964,
                "icon"              => "Iraq"
            ],
            [
                "name"              => "ireland",
                "iso_code_2"        => "IE",
                "iso_code_3"        => "IRL",
                "call_prefix"       =>  353,
                "icon"              => "Ireland"
            ],
            [
                "name"              => "isle_of_man",
                "iso_code_2"        => "IM",
                "iso_code_3"        => "IMN",
                "call_prefix"       =>  44,
                "icon"              => "Isle-of-Man"
            ],
            [
                "name"              => "israel",
                "iso_code_2"        => "IL",
                "iso_code_3"        => "ISR",
                "call_prefix"       =>  972,
                "icon"              => "Israel"
            ],
            [
                "name"              => "italy",
                "iso_code_2"        => "IT",
                "iso_code_3"        => "ITA",
                "call_prefix"       =>  39,
                "icon"              => "Italy"
            ],
            [
                "name"              => "jamaica",
                "iso_code_2"        => "JM",
                "iso_code_3"        => "JAM",
                "call_prefix"       =>  1876,
                "icon"              => "Jamaica"
            ],
            [
                "name"              => "japan",
                "iso_code_2"        => "JP",
                "iso_code_3"        => "JPN",
                "call_prefix"       =>  81,
                "icon"              => "Japan"
            ],
            [
                "name"              => "jersey",
                "iso_code_2"        => "JE",
                "iso_code_3"        => "JEY",
                "call_prefix"       =>  44,
                "icon"              => "Jersey"
            ],
            [
                "name"              => "jordan",
                "iso_code_2"        => "JO",
                "iso_code_3"        => "JOR",
                "call_prefix"       =>  962,
                "icon"              => "Jordan"
            ],
            [
                "name"              => "kazakhstan",
                "iso_code_2"        => "KZ",
                "iso_code_3"        => "KAZ",
                "call_prefix"       =>  7,
                "icon"              => "Kazakhstan"
            ],
            [
                "name"              => "kenya",
                "iso_code_2"        => "KE",
                "iso_code_3"        => "KEN",
                "call_prefix"       =>  254,
                "icon"              => "Kenya"
            ],
            [
                "name"              => "kiribati",
                "iso_code_2"        => "KI",
                "iso_code_3"        => "KIR",
                "call_prefix"       =>  686,
                "icon"              => "Kiribati"
            ],
            [
                "name"              => "north_korea",
                "iso_code_2"        => "KP",
                "iso_code_3"        => "PRK",
                "call_prefix"       =>  850,
                "icon"              => "North-Korea"
            ],
            [
                "name"              => "south_korea",
                "iso_code_2"        => "KR",
                "iso_code_3"        => "KOR",
                "call_prefix"       =>  82,
                "icon"              => "South-Korea"
            ],
            [
                "name"              => "kuwait",
                "iso_code_2"        => "KW",
                "iso_code_3"        => "KWT",
                "call_prefix"       =>  965,
                "icon"              => "Kuwait"
            ],
            [
                "name"              => "kyrgyzstan",
                "iso_code_2"        => "KG",
                "iso_code_3"        => "KGZ",
                "call_prefix"       =>  996,
                "icon"              => "Kyrgyzstan"
            ],
            [
                "name"              => "laos",
                "iso_code_2"        => "LA",
                "iso_code_3"        => "LAO",
                "call_prefix"       =>  856,
                "icon"              => "Laos"
            ],
            [
                "name"              => "latvia",
                "iso_code_2"        => "LV",
                "iso_code_3"        => "LVA",
                "call_prefix"       =>  371,
                "icon"              => "Latvia"
            ],
            [
                "name"              => "lebanon",
                "iso_code_2"        => "LB",
                "iso_code_3"        => "LBN",
                "call_prefix"       =>  961,
                "icon"              => "Lebanon"
            ],
            [
                "name"              => "lesotho",
                "iso_code_2"        => "LS",
                "iso_code_3"        => "LSO",
                "call_prefix"       =>  266,
                "icon"              => "Lesotho"
            ],
            [
                "name"              => "liberia",
                "iso_code_2"        => "LR",
                "iso_code_3"        => "LBR",
                "call_prefix"       =>  231,
                "icon"              => "Liberia"
            ],
            [
                "name"              => "libya",
                "iso_code_2"        => "LY",
                "iso_code_3"        => "LBY",
                "call_prefix"       =>  218,
                "icon"              => "Libya"
            ],
            [
                "name"              => "liechtenstein",
                "iso_code_2"        => "LI",
                "iso_code_3"        => "LIE",
                "call_prefix"       =>  423,
                "icon"              => "Liechtenstein"
            ],
            [
                "name"              => "lithuania",
                "iso_code_2"        => "LT",
                "iso_code_3"        => "LTU",
                "call_prefix"       =>  370,
                "icon"              => "Lithuania"
            ],
            [
                "name"              => "luxembourg",
                "iso_code_2"        => "LU",
                "iso_code_3"        => "LUX",
                "call_prefix"       =>  352,
                "icon"              => "Luxembourg"
            ],
            [
                "name"              => "macau",
                "iso_code_2"        => "MO",
                "iso_code_3"        => "MAC",
                "call_prefix"       =>  853,
                "icon"              => "Macau"
            ],
            [
                "name"              => "macedonia",
                "iso_code_2"        => "MK",
                "iso_code_3"        => "MKD",
                "call_prefix"       =>  389,
                "icon"              => "Macedonia"
            ],
            [
                "name"              => "madagascar",
                "iso_code_2"        => "MG",
                "iso_code_3"        => "MDG",
                "call_prefix"       =>  261,
                "icon"              => "Madagascar"
            ],
            [
                "name"              => "malawi",
                "iso_code_2"        => "MW",
                "iso_code_3"        => "MWI",
                "call_prefix"       =>  265,
                "icon"              => "Malawi"
            ],
            [
                "name"              => "malaysia",
                "iso_code_2"        => "MY",
                "iso_code_3"        => "MYS",
                "call_prefix"       =>  60,
                "icon"              => "Malaysia"
            ],
            [
                "name"              => "maldives",
                "iso_code_2"        => "MV",
                "iso_code_3"        => "MDV",
                "call_prefix"       =>  960,
                "icon"              => "Maldives"
            ],
            [
                "name"              => "mali",
                "iso_code_2"        => "ML",
                "iso_code_3"        => "MLI",
                "call_prefix"       =>  223,
                "icon"              => "Mali"
            ],
            [
                "name"              => "malta",
                "iso_code_2"        => "MT",
                "iso_code_3"        => "MLT",
                "call_prefix"       =>  356,
                "icon"              => "Malta"
            ],
            [
                "name"              => "marshall_islands",
                "iso_code_2"        => "MH",
                "iso_code_3"        => "MHL",
                "call_prefix"       =>  692,
                "icon"              => "Marshall-Islands"
            ],
            [
                "name"              => "mauritania",
                "iso_code_2"        => "MR",
                "iso_code_3"        => "MRT",
                "call_prefix"       =>  222,
                "icon"              => "Mauritania"
            ],
            [
                "name"              => "mauritius",
                "iso_code_2"        => "MU",
                "iso_code_3"        => "MUS",
                "call_prefix"       =>  230,
                "icon"              => "Mauritius"
            ],
            [
                "name"              => "mayotte",
                "iso_code_2"        => "YT",
                "iso_code_3"        => "MYT",
                "call_prefix"       =>  262,
                "icon"              => "Mayotte"
            ],
            [
                "name"              => "mexico",
                "iso_code_2"        => "MX",
                "iso_code_3"        => "MEX",
                "call_prefix"       =>  52,
                "icon"              => "Mexico"
            ],
            [
                "name"              => "micronesia",
                "iso_code_2"        =>  "FM",
                "iso_code_3"        =>  "FSM",
                "call_prefix"       =>  691,
                "icon"              => "Micronesia"
            ],
            [
                "name"              => "moldova",
                "iso_code_2"        => "MD",
                "iso_code_3"        => "MDA",
                "call_prefix"       =>  691,
                "icon"              => "Moldova"
            ],
            [
                "name"              => "monaco",
                "iso_code_2"        => "MC",
                "iso_code_3"        => "MCO",
                "call_prefix"       =>  377,
                "icon"              => "Monaco"
            ],
            [
                "name"              => "mongolia",
                "iso_code_2"        => "MN",
                "iso_code_3"        => "MNG",
                "call_prefix"       =>  976,
                "icon"              => "Mongolia"
            ],
            [
                "name"              => "montenegro",
                "iso_code_2"        => "ME",
                "iso_code_3"        => "MNE",
                "call_prefix"       =>  382,
                "icon"              => "Montenegro"
            ],
            [
                "name"              => "montserrat",
                "iso_code_2"        => "MS",
                "iso_code_3"        => "MSR",
                "call_prefix"       =>  382,
                "icon"              => "Montserrat"
            ],
            [
                "name"              => "morocco",
                "iso_code_2"        => "MA",
                "iso_code_3"        => "MAR",
                "call_prefix"       =>  212,
                "icon"              => "Morocco"
            ],
            [
                "name"              => "mozambique",
                "iso_code_2"        => "MZ",
                "iso_code_3"        => "MOZ",
                "call_prefix"       =>  258,
                "icon"              => "Mozambique"
            ],
            [
                "name"              => "myanmar",
                "iso_code_2"        => "MM",
                "iso_code_3"        => "MMR",
                "call_prefix"       =>  95,
                "icon"              => "Myanmar"
            ],
            [
                "name"              => "namibia",
                "iso_code_2"        => "NA",
                "iso_code_3"        => "NAM",
                "call_prefix"       =>  264,
                "icon"              => "Namibia"
            ],
            [
                "name"              => "nauru",
                "iso_code_2"        => "NR",
                "iso_code_3"        => "NRU",
                "call_prefix"       =>  674,
                "icon"              => "Nauru"
            ],
            [
                "name"              => "nepal",
                "iso_code_2"        => "NP",
                "iso_code_3"        => "NPL",
                "call_prefix"       =>  977,
                "icon"              => "Nepal"
            ],
            [
                "name"              => "netherlands",
                "iso_code_2"        => "NL",
                "iso_code_3"        => "NLD",
                "call_prefix"       =>  31,
                "icon"              => "Netherlands"
            ],
            [
                "name"              => "new_caledonia",
                "iso_code_2"        => "NC",
                "iso_code_3"        => "NCL",
                "call_prefix"       =>  687,
                "icon"              => "New-Caledonia"
            ],
            [
                "name"              => "new_zealand",
                "iso_code_2"        => "NZ",
                "iso_code_3"        => "NZL",
                "call_prefix"       =>  64,
                "icon"              => "New-Zealand"
            ],
            [
                "name"              => "nicaragua",
                "iso_code_2"        => "NI",
                "iso_code_3"        => "NIC",
                "call_prefix"       =>  505,
                "icon"              => "Nicaragua"
            ],
            [
                "name"              => "niger",
                "iso_code_2"        => "NE",
                "iso_code_3"        => "NER",
                "call_prefix"       =>  227,
                "icon"              => "Niger"
            ],
            [
                "name"              => "nigeria",
                "iso_code_2"        => "NG",
                "iso_code_3"        => "NGA",
                "call_prefix"       =>  234,
                "icon"              => "Nigeria"
            ],
            [
                "name"              => "niue",
                "iso_code_2"        => "NU",
                "iso_code_3"        => "NIU",
                "call_prefix"       =>  683,
                "icon"              => "Niue"
            ],
            [
                "name"              => "norfolk_island",
                "iso_code_2"        => "NF",
                "iso_code_3"        => "NFK",
                "call_prefix"       =>  672,
                "icon"              => "Norfolk-Island"
            ],
            [
                "name"              => "northern_mariana_islands",
                "iso_code_2"        => "MP",
                "iso_code_3"        => "MNP",
                "call_prefix"       =>  1670,
                "icon"              => "Northern-Mariana-Islands"
            ],
            [
                "name"              => "norway",
                "iso_code_2"        => "NO",
                "iso_code_3"        => "NOR",
                "call_prefix"       =>  47,
                "icon"              => "Norway"
            ],
            [
                "name"              => "oman",
                "iso_code_2"        => "OM",
                "iso_code_3"        => "OMN",
                "call_prefix"       =>  968,
                "icon"              => "Oman"
            ],
            [
                "name"              => "pakistan",
                "iso_code_2"        => "PK",
                "iso_code_3"        => "PAK",
                "call_prefix"       =>  92,
                "icon"              => "Pakistan"
            ],
            [
                "name"              => "palau",
                "iso_code_2"        => "PW",
                "iso_code_3"        => "PLW",
                "call_prefix"       =>  680,
                "icon"              => "Palau"
            ],
            [
                "name"              => "palestine",
                "iso_code_2"        => "PS",
                "iso_code_3"        => "PSE",
                "call_prefix"       =>  970,
                "icon"              => "Palestine"
            ],
            [
                "name"              => "panama",
                "iso_code_2"        => "PA",
                "iso_code_3"        => "PAN",
                "call_prefix"       =>  507,
                "icon"              => "Panama"
            ],
            [
                "name"              => "papua_new_guinea",
                "iso_code_2"        => "PG",
                "iso_code_3"        => "PNG",
                "call_prefix"       =>  675,
                "icon"              => "Papua-New-Guinea"
            ],
            [
                "name"              => "paraguay",
                "iso_code_2"        => "PY",
                "iso_code_3"        => "PRY",
                "call_prefix"       =>  595,
                "icon"              => "Paraguay"
            ],
            [
                "name"              => "peru",
                "iso_code_2"        => "PE",
                "iso_code_3"        => "PER",
                "call_prefix"       =>  51,
                "icon"              => "Peru"
            ],
            [
                "name"              => "philippines",
                "iso_code_2"        => "PH",
                "iso_code_3"        => "PHL",
                "call_prefix"       =>  63,
                "icon"              => "Philippines"
            ],
            [
                "name"              => "pitcairn",
                "iso_code_2"        => "PN",
                "iso_code_3"        => "PCN",
                "call_prefix"       =>  64,
                "icon"              => "Pitcairn-Islands"
            ],
            [
                "name"              => "poland",
                "iso_code_2"        => "PL",
                "iso_code_3"        => "POL",
                "call_prefix"       =>  48,
                "icon"              => "Poland"
            ],
            [
                "name"              => "portugal",
                "iso_code_2"        => "PT",
                "iso_code_3"        => "PRT",
                "call_prefix"       =>  351,
                "icon"              => "Portugal"
            ],
            [
                "name"              => "puerto_rico",
                "iso_code_2"        => "PR",
                "iso_code_3"        => "PRI",
                "call_prefix"       =>  1,
                "icon"              => "Puerto-Rico"
            ],
            [
                "name"              => "qatar",
                "iso_code_2"        => "QA",
                "iso_code_3"        => "QAT",
                "call_prefix"       =>  974,
                "icon"              => "Qatar"
            ],
            [
                "name"              => "romania",
                "iso_code_2"        => "RO",
                "iso_code_3"        => "ROU",
                "call_prefix"       =>  40,
                "icon"              => "Romania"
            ],
            [
                "name"              => "russia",
                "iso_code_2"        => "RU",
                "iso_code_3"        => "RUS",
                "call_prefix"       =>  7,
                "icon"              => "Russia"
            ],
            [
                "name"              => "rwanda",
                "iso_code_2"        => "RW",
                "iso_code_3"        => "RWA",
                "call_prefix"       =>  250,
                "icon"              => "Rwanda"
            ],
            [
                "name"              => "saint_helena",
                "iso_code_2"        => "SH",
                "iso_code_3"        => "SHN",
                "call_prefix"       =>  290,
                "icon"              => "Saint-Helena"
            ],
            [
                "name"              => "saint_kitts_and_nevis",
                "iso_code_2"        => "KN",
                "iso_code_3"        => "KNA",
                "call_prefix"       =>  1869,
                "icon"              => "Saint-Kitts-and-Nevis"
            ],
            [
                "name"              => "saint_lucia",
                "iso_code_2"        => "LC",
                "iso_code_3"        => "LCA",
                "call_prefix"       =>  1758,
                "icon"              => "Saint-Lucia"
            ],
            [
                "name"              => "saint_vincent_and_the_grenadines",
                "iso_code_2"        => "VC",
                "iso_code_3"        => "VCT",
                "call_prefix"       =>  1784,
                "icon"              => "Saint-Vincent-and-the-Grenadines"
            ],
            [
                "name"              => "samoa",
                "iso_code_2"        => "WS",
                "iso_code_3"        => "WSM",
                "call_prefix"       =>  685,
                "icon"              => "Samoa"
            ],
            [
                "name"              => "san_marino",
                "iso_code_2"        => "SM",
                "iso_code_3"        => "SMR",
                "call_prefix"       =>  378,
                "icon"              => "San-Marino"
            ],
            [
                "name"              => "sao_tome_and_principe",
                "iso_code_2"        => "ST",
                "iso_code_3"        => "STP",
                "call_prefix"       =>  239,
                "icon"              => "Sao-Tome-and-Principe"
            ],
            [
                "name"              => "saudi_arabia",
                "iso_code_2"        => "SA",
                "iso_code_3"        => "SAU",
                "call_prefix"       =>  966,
                "icon"              => "Saudi-Arabia"
            ],
            [
                "name"              => "senegal",
                "iso_code_2"        => "SN",
                "iso_code_3"        => "SEN",
                "call_prefix"       =>  221,
                "icon"              => "Senegal"
            ],
            [
                "name"              => "serbia",
                "iso_code_2"        => "RS",
                "iso_code_3"        => "SRB",
                "call_prefix"       =>  381,
                "icon"              => "Serbia"
            ],
            [
                "name"              => "seychelles",
                "iso_code_2"        => "SC",
                "iso_code_3"        => "SYC",
                "call_prefix"       =>  248,
                "icon"              => "Seychelles"
            ],
            [
                "name"              => "sierra_leone",
                "iso_code_2"        => "SL",
                "iso_code_3"        => "SLE",
                "call_prefix"       =>  232,
                "icon"              => "Sierra-Leone"
            ],
            [
                "name"              => "singapore",
                "iso_code_2"        => "SG",
                "iso_code_3"        => "SGP",
                "call_prefix"       =>  65,
                "icon"              => "Singapore"
            ],
            [
                "name"              => "slovakia",
                "iso_code_2"        => "SK",
                "iso_code_3"        => "SVK",
                "call_prefix"       =>  421,
                "icon"              => "Slovakia"
            ],
            [
                "name"              => "slovenia",
                "iso_code_2"        => "SI",
                "iso_code_3"        => "SVN",
                "call_prefix"       =>  386,
                "icon"              => "Slovenia"
            ],
            [
                "name"              => "solomon_islands",
                "iso_code_2"        => "SB",
                "iso_code_3"        => "SLB",
                "call_prefix"       =>  677,
                "icon"              => "Solomon-Islands"
            ],
            [
                "name"              => "somalia",
                "iso_code_2"        => "SO",
                "iso_code_3"        => "SOM",
                "call_prefix"       =>  252,
                "icon"              => "Somalia"
            ],
            [
                "name"              => "south_africa",
                "iso_code_2"        => "ZA",
                "iso_code_3"        => "ZAF",
                "call_prefix"       =>  27,
                "icon"              => "South-Africa"
            ],
            [
                "name"              => "south_sudan",
                "iso_code_2"        => "SS",
                "iso_code_3"        => "SSD",
                "call_prefix"       =>  27,
                "icon"              => "South-Sudan"
            ],
            [
                "name"              => "spain",
                "iso_code_2"        => "ES",
                "iso_code_3"        => "ESP",
                "call_prefix"       =>  34,
                "icon"              => "Spain"
            ],
            [
                "name"              => "sri_lanka",
                "iso_code_2"        => "LK",
                "iso_code_3"        => "LKA",
                "call_prefix"       =>  94,
                "icon"              => "Sri-Lanka"
            ],
            [
                "name"              => "sudan",
                "iso_code_2"        => "SD",
                "iso_code_3"        => "SDN",
                "call_prefix"       =>  249,
                "icon"              => "Sudan"
            ],
            [
                "name"              => "suriname",
                "iso_code_2"        => "SR",
                "iso_code_3"        => "SUR",
                "call_prefix"       =>  597,
                "icon"              => "Suriname"
            ],
            [
                "name"              => "swaziland",
                "iso_code_2"        => "SZ",
                "iso_code_3"        => "SWZ",
                "call_prefix"       =>  268,
                "icon"              => "Swaziland"
            ],
            [
                "name"              => "sweden",
                "iso_code_2"        => "SE",
                "iso_code_3"        => "SWE",
                "call_prefix"       =>  46,
                "icon"              => "Sweden"
            ],
            [
                "name"              => "switzerland",
                "iso_code_2"        => "CH",
                "iso_code_3"        => "CHE",
                "call_prefix"       =>  41,
                "icon"              => "Switzerland"
            ],
            [
                "name"              => "syrian_arab_republic",
                "iso_code_2"        => "SY",
                "iso_code_3"        => "SYR",
                "call_prefix"       =>  963,
                "icon"              => "Syria"
            ],
            [
                "name"              => "taiwan",
                "iso_code_2"        => "TW",
                "iso_code_3"        => "TWN",
                "call_prefix"       =>  886,
                "icon"              => "Taiwan"
            ],
            [
                "name"              => "tajikistan",
                "iso_code_2"        => "TJ",
                "iso_code_3"        => "TJK",
                "call_prefix"       =>  992,
                "icon"              => "Tajikistan"
            ],
            [
                "name"              => "tanzania",
                "iso_code_2"        => "TZ",
                "iso_code_3"        => "TZA",
                "call_prefix"       =>  255,
                "icon"              => "Tanzania"
            ],
            [
                "name"              => "thailand",
                "iso_code_2"        => "TH",
                "iso_code_3"        => "THA",
                "call_prefix"       =>  66,
                "icon"              => "Thailand"
            ],
            [
                "name"              => "east_timor",
                "iso_code_2"        => "TL",
                "iso_code_3"        => "TLS",
                "call_prefix"       =>  670,
                "icon"              => "East-Timor"
            ],
            [
                "name"              => "togo",
                "iso_code_2"        => "TG",
                "iso_code_3"        => "TGO",
                "call_prefix"       =>  228,
                "icon"              => "Togo"
            ],
            [
                "name"              => "tokelau",
                "iso_code_2"        => "TK",
                "iso_code_3"        => "TKL",
                "call_prefix"       =>  690,
                "icon"              => "Tokelau"
            ],
            [
                "name"              => "tonga",
                "iso_code_2"        => "TO",
                "iso_code_3"        => "TON",
                "call_prefix"       =>  676,
                "icon"              => "Tonga"
            ],
            [
                "name"              => "trinidad_and_tobago",
                "iso_code_2"        => "TT",
                "iso_code_3"        => "TTO",
                "call_prefix"       =>  1868,
                "icon"              => "Trinidad-and-Tobago"
            ],
            [
                "name"              => "tunisia",
                "iso_code_2"        => "TN",
                "iso_code_3"        => "TUN",
                "call_prefix"       =>  216,
                "icon"              => "Tunisia"
            ],
            [
                "name"              => "turkey",
                "iso_code_2"        => "TR",
                "iso_code_3"        => "TUR",
                "call_prefix"       =>  90,
                "icon"              => "Turkey"
            ],
            [
                "name"              => "turkmenistan",
                "iso_code_2"        => "TM",
                "iso_code_3"        => "TKM",
                "call_prefix"       =>  993,
                "icon"              => "Turkmenistan"
            ],
            [
                "name"              => "turks_and_caicos_islands",
                "iso_code_2"        => "TC",
                "iso_code_3"        => "TCA",
                "call_prefix"       =>  1649,
                "icon"              => "Turks-and-Caicos-Islands"
            ],
            [
                "name"              => "tuvalu",
                "iso_code_2"        => "TV",
                "iso_code_3"        => "TUV",
                "call_prefix"       =>  688,
                "icon"              => "Tuvalu"
            ],
            [
                "name"              => "uganda",
                "iso_code_2"        => "UG",
                "iso_code_3"        => "UGA",
                "call_prefix"       =>  256,
                "icon"              => "Uganda"
            ],
            [
                "name"              => "ukraine",
                "iso_code_2"        => "UA",
                "iso_code_3"        => "UKR",
                "call_prefix"       =>  380,
                "icon"              => "Ukraine"
            ],
            [
                "name"              => "united_arab_emirates",
                "iso_code_2"        => "AE",
                "iso_code_3"        => "ARE",
                "call_prefix"       =>  971,
                "icon"              => "United-Arab-Emirates"
            ],
            [
                "name"              => "united_kingdom",
                "iso_code_2"        => "GB",
                "iso_code_3"        => "GBR",
                "call_prefix"       =>  44,
                "icon"              => "United-Kingdom"
            ],
            [
                "name"              => "united_states",
                "iso_code_2"        => "US",
                "iso_code_3"        => "USA",
                "call_prefix"       =>  1,
                "icon"              => "United-States"
            ],
            [
                "name"              => "uruguay",
                "iso_code_2"        => "UY",
                "iso_code_3"        => "URY",
                "call_prefix"       =>  598,
                "icon"              => "Uruguay"
            ],
            [
                "name"              => "uzbekistan",
                "iso_code_2"        => "UZ",
                "iso_code_3"        => "UZB",
                "call_prefix"       =>  998,
                "icon"              => "Uzbekistan"
            ],
            [
                "name"              => "vanuatu",
                "iso_code_2"        => "VU",
                "iso_code_3"        => "VUT",
                "call_prefix"       =>  678,
                "icon"              => "Vanuatu"
            ],
            [
                "name"              => "venezuela",
                "iso_code_2"        => "VE",
                "iso_code_3"        => "VEN",
                "call_prefix"       =>  58,
                "icon"              => "Venezuela"
            ],
            [
                "name"              => "vietnam",
                "iso_code_2"        => "VN",
                "iso_code_3"        => "VNM",
                "call_prefix"       =>  84,
                "icon"              => "Vietnam"
            ],
            [
                "name"              => "british_virgin_islands",
                "iso_code_2"        => "VG",
                "iso_code_3"        => "VGB",
                "call_prefix"       =>  1284,
                "icon"              => "British-Virgin-Islands"
            ],
            [
                "name"              => "us_virgin_islands",
                "iso_code_2"        => "VI",
                "iso_code_3"        => "VIR",
                "call_prefix"       =>  1340,
                "icon"              => "US-Virgin-Islands"
            ],
            [
                "name"              => "wallis_and_futuna",
                "iso_code_2"        => "WF",
                "iso_code_3"        => "WLF",
                "call_prefix"       =>  681,
                "icon"              => "Wallis-And-Futuna"
            ],
            [
                "name"              => "western_sahara",
                "iso_code_2"        => "EH",
                "iso_code_3"        => "ESH",
                "call_prefix"       =>  212,
                "icon"              => "Western-Sahara"
            ],
            [
                "name"              => "yemen",
                "iso_code_2"        => "YE",
                "iso_code_3"        => "YEM",
                "call_prefix"       =>  967,
                "icon"              => "Yemen"
            ],
            [
                "name"              => "zambia",
                "iso_code_2"        => "ZM",
                "iso_code_3"        => "ZMB",
                "call_prefix"       =>  260,
                "icon"              => "Zambia"
            ],
            [
                "name"              => "zimbabwe",
                "iso_code_2"        => "ZW",
                "iso_code_3"        => "ZWE",
                "call_prefix"       =>  263,
                "icon"              => "Zimbabwe"
            ]
        ]);
        
        DB::table('states')->insert([
            [
                "country_id"    => 144,
                "name"          => "Drenthe",
                "iso"           => "DR"
            ],
            [
                "country_id"    => 144,
                "name"          => "Flevoland",
                "iso"           => "FL"
            ],
            [
                "country_id"    => 144,
                "name"          => "Friesland",
                "iso"           => "FR"
            ],
            [
                "country_id"    => 144,
                "name"          => "Gelderland",
                "iso"           => "GE"
            ],
            [
                "country_id"    => 144,
                "name"          => "Groningen",
                "iso"           => "GR"
            ],
            [
                "country_id"    => 144,
                "name"          => "Limburg",
                "iso"           => "LI"
            ],
            [
                "country_id"    => 144,
                "name"          => "Noord-Brabant",
                "iso"           => "NB"
            ],
            [
                "country_id"    => 144,
                "name"          => "Noord-Holland",
                "iso"           => "NH"
            ],
            [
                "country_id"    => 144,
                "name"          => "Overijssel",
                "iso"           => "OV"
            ],
            [
                "country_id"    => 144,
                "name"          => "Utrecht",
                "iso"           => "UT"
            ],
            [
                "country_id"    => 144,
                "name"          => "Zeeland",
                "iso"           => "ZE"
            ],
            [
                "country_id"    => 144,
                "name"          => "Zuid-Holland",
                "iso"           => "ZH"
            ],
            [
                "country_id"    => 20,
                "name"          => "Antwerpen",
                "iso"           => "VAN"
            ],
            [
                "country_id"    => 20,
                "name"          => "Brabant Wallon",
                "iso"           => "WBR"
            ],
            [
                "country_id"    => 20,
                "name"          => "Brussels Hoofdstedelijk Gewest",
                "iso"           => "BRU"
            ],
            [
                "country_id"    => 20,
                "name"          => "Hainaut",
                "iso"           => "WHT"
            ],
            [
                "country_id"    => 20,
                "name"          => "Limburg",
                "iso"           => "VLI"
            ],
            [
                "country_id"    => 20,
                "name"          => "Liège",
                "iso"           => "WLG"
            ],
            [
                "country_id"    => 20,
                "name"          => "Luxembourg",
                "iso"           => "WLX"
            ],
            [
                "country_id"    => 20,
                "name"          => "Namur",
                "iso"           => "WNA"
            ],
            [
                "country_id"    => 20,
                "name"          => "Oost-Vlaanderen",
                "iso"           => "VOW"
            ],
            [
                "country_id"    => 20,
                "name"          => "Vlaams Gewest",
                "iso"           => "VLG"
            ],
            [
                "country_id"    => 20,
                "name"          => "Vlaams-Brabant",
                "iso"           => "VBR"
            ],
            [
                "country_id"    => 20,
                "name"          => "Wallonne",
                "iso"           => "WAL"
            ],
            [
                "country_id"    => 20,
                "name"          => "West-Vlaanderen",
                "iso"           => "VWV"
            ],
        ]);
        
        DB::table('languages')->insert([
            [
                "name"      => "English",
                "iso"       => "en",
                "locale"    => "en-gb",
                "icon"      => "United-Kingdom"
            ],
            [
                "name"      => "Nederlands",
                "iso"       => "nl",
                "locale"    => "nl-nl",
                "icon"      => "Netherlands"
            ],
            [
                "name"      => "Deutsch",
                "iso"       => "de",
                "locale"    => "de-de",
                "icon"      => "Germany"
            ],
            [
                "name"      => "Français",
                "iso"       => "fr",
                "locale"    => "fr-fr",
                "icon"      => "France"
            ],
            [
                "name"      => "Español",
                "iso"       => "es",
                "locale"    => "es-es",
                "icon"      => "Spain"
            ],
            [
                "name"      => "Türkçe",
                "iso"       => "tr",
                "locale"    => "tr",
                "icon"      => "Turkey"
            ],
            [
                "name"      => "Português",
                "iso"       => "pt",
                "locale"    => "pt-pt",
                "icon"      => "Portugal"
            ],
            [
                "name"  => "Italiano",
                "iso"   => "it",
                "locale"    => "it-it",
                "icon"  => "Italy"
            ],
            [
                "name"      => "Svensk",
                "iso"       => "se",
                "locale"    => "sv-se",
                "icon"      => "Sweden"
            ],
            [
                "name"      => "Pусский",
                "iso"       => "ru",
                "locale"    => "ru",
                "icon"      => "Russia"
            ],
            [
                "name"      => "中文 (繁體)",
                "iso"       => "zh",
                "locale"    => "zh-tw",
                "icon"      => "Taiwan"
            ],
            [
                "name"      => "中文 (简体)",
                "iso"       => "zh",
                "locale"    => "zh-cn",
                "icon"      => "China"
            ],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('states');
        Schema::drop('countries');
        Schema::drop('currencies');
        Schema::drop('languages');
    }
}
