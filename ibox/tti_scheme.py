# coding: utf-8
from sqlalchemy import Column, Date, DateTime, Enum, Float, Index, Integer, Numeric, SmallInteger, String, Table, Text, text
from sqlalchemy.dialects.mysql.types import BIT
from sqlalchemy.ext.declarative import declarative_base


Base = declarative_base()
metadata = Base.metadata


class ArtHdr(Base):
    __tablename__ = 'art_hdr'

    rec_id = Column(String(10), primary_key=True, server_default=text("''"))
    hdr_type = Column(Float(asdecimal=True))
    inv_nr = Column(String(10))
    ord_nr = Column(String(10))
    ord_date = Column(Date)
    del_date = Column(Date)
    inv_date = Column(Date)
    ext_desc = Column(String(40))
    rpl_del = Column(String(10))
    rpl_inv = Column(String(10))
    cur_code = Column(String(3))
    sel_code = Column(String(5))
    cost_code = Column(String(10))
    proj_code = Column(String(10))
    emp_nr = Column(String(10))
    comment1 = Column(String(40))
    comment2 = Column(String)
    comment3 = Column(String)
    sub_tot = Column(Float(asdecimal=True))
    ord_tot = Column(Float(asdecimal=True))
    pd_pct = Column(Float(asdecimal=True))
    pd_vat = Column(Float(asdecimal=True))
    pd_amt = Column(Float(asdecimal=True))
    vc_ord = Column(String(2))
    ord_cost = Column(Float(asdecimal=True))
    disc_days = Column(Float(asdecimal=True))
    vc_ship = Column(String(2))
    ship_cost = Column(Float(asdecimal=True))
    vat_ord = Column(Float(asdecimal=True))
    vat_ship = Column(Float(asdecimal=True))
    disc_code = Column(String(2))
    pac_nr = Column(String(10))
    del_addr = Column(String)
    inv_cp = Column(String(40))
    del_cp = Column(String(40))
    ship_mth = Column(String(10))
    cred_days = Column(Float(asdecimal=True))
    vat_tot = Column(Float(asdecimal=True))
    pay_amt = Column(Float(asdecimal=True))
    period = Column(Float(asdecimal=True))
    dj_code = Column(String(3))
    page_nr = Column(String(10))
    acct_nr = Column(String(10))
    dj_post = Column(BIT(1))
    exch_rate = Column(Float(asdecimal=True))
    cpy_prn = Column(String(100))
    cpy_prtype = Column(String(40))
    blk_amt = Column(Float(asdecimal=True))
    bv_amt = Column(Float(asdecimal=True))
    lb_amt = Column(Float(asdecimal=True))
    dd_act = Column(BIT(1))
    sub_cur = Column(Float(asdecimal=True))
    ord_cur = Column(Float(asdecimal=True))
    pdv_cur = Column(Float(asdecimal=True))
    pda_cur = Column(Float(asdecimal=True))
    oc_cur = Column(Float(asdecimal=True))
    sc_cur = Column(Float(asdecimal=True))
    vo_cur = Column(Float(asdecimal=True))
    vs_cur = Column(Float(asdecimal=True))
    vat_cur = Column(Float(asdecimal=True))
    pay_cur = Column(Float(asdecimal=True))
    blk_cur = Column(Float(asdecimal=True))
    bv_cur = Column(Float(asdecimal=True))
    lb_cur = Column(Float(asdecimal=True))
    ass_code = Column(String(10))
    sg_code = Column(String(10))
    sbs_nr = Column(String(10))
    apx_list = Column(String(5))
    pct_list = Column(String(5))
    auth_hist = Column(String)
    qt_ord_nr = Column(String(10))
    doc_nr = Column(String(10))
    dpd_nrs = Column(String)
    l5_trncode = Column(String(10))
    trn_book = Column(BIT(1))
    trn_start = Column(Integer)
    trn_cnt = Column(Integer)
    dpd_pacnr = Column(String)
    excl_mail = Column(BIT(1))
    lng_code = Column(String(5))
    pref_date = Column(DateTime)
    ic_adm = Column(String(10))
    ic_order = Column(BIT(1))
    mand_b2b = Column(BIT(1))
    mand_date = Column(DateTime)
    mand_id = Column(String(35))
    tr_code = Column(String(10))
    mand_seq = Column(Integer)
    mail_eml1 = Column(String(100))
    web_nr = Column(String(20))
    web_user = Column(String(10))
    web_email = Column(String(50))
    pers_name = Column(String(40))
    web_phone = Column(String(20))
    web_fax = Column(String(20))
    web_comm = Column(String)
    web_note = Column(String)
    acct_name = Column(String(40))
    address1 = Column(String(80))
    post_code = Column(String(10))
    city = Column(String(25))
    country = Column(String(30))
    rec_prn = Column(String(100))
    ic_ref = Column(String(10))
    wm_code = Column(String(10))
    gip2_calc = Column(Float(asdecimal=True))


class ArtLine(Base):
    __tablename__ = 'art_line'

    rec_id = Column(String(10), primary_key=True, server_default=text("''"))
    hdr_type = Column(Float(asdecimal=True))
    inv_nr = Column(String(10))
    art_code = Column(String(15))
    art_desc1 = Column(String(40))
    art_date = Column(DateTime)
    art_qty = Column(Float(asdecimal=True))
    rem_qty = Column(Float(asdecimal=True))
    art_px = Column(Float(asdecimal=True))
    px_calc = Column(Float(asdecimal=True))
    vat_code = Column(String(2))
    vat_amt = Column(Float(asdecimal=True))
    art_amt = Column(Float(asdecimal=True))
    disc_pct = Column(Float(asdecimal=True))
    disc_amt = Column(Float(asdecimal=True))
    reva_amt = Column(Float(asdecimal=True))
    pip_amt = Column(Float(asdecimal=True))
    cogs_amt = Column(Float(asdecimal=True))
    pxdif_amt = Column(Float(asdecimal=True))
    whs_code = Column(String(5))
    loc_code = Column(String(5))
    cost_code = Column(String(10))
    pc_code = Column(String(5))
    emp_nr = Column(String(10))
    ag_code = Column(String(5))
    art_bm = Column(BIT(1))
    art_ca = Column(BIT(1))
    bm_tag = Column(String(10))
    cogs_susp = Column(BIT(1))
    al_proc = Column(BIT(1))
    art_level = Column(Integer)
    is_month = Column(Float(asdecimal=True))
    icat_code = Column(String(1))
    icnt_src = Column(String(2))
    icnt_tgt = Column(String(2))
    ishp_code = Column(String(1))
    is_cntnr = Column(Float(asdecimal=True))
    isys_code = Column(String(2))
    ityp_code = Column(String(1))
    isxf_code = Column(String(3))
    is_weight = Column(Float(asdecimal=True))
    is_custcd = Column(String(2))
    rec_blk = Column(BIT(1))
    spl_id = Column(String(10))
    hdr_id = Column(String(10))
    pol_id = Column(String(10))
    vat_cur = Column(Float(asdecimal=True))
    cur_amt = Column(Float(asdecimal=True))
    disc_cur = Column(Float(asdecimal=True))
    pxdif_cur = Column(Float(asdecimal=True))
    art_cur = Column(Float(asdecimal=True))
    pip_cur = Column(Float(asdecimal=True))
    org_ordnr = Column(String(10))
    l5_bonnr = Column(String(15))
    l5_spec = Column(String(40))
    proj_code = Column(String(10))
    proj_id = Column(String(10))
    proj_type = Column(Integer)
    xml_data = Column(Text)
    unt_code = Column(String(10))
    art_qtyu = Column(Float(asdecimal=True))
    link_id = Column(String(10))
    unt_codep = Column(String(10))
    unt_px = Column(Float(asdecimal=True))
    unt_cur = Column(Float(asdecimal=True))
    ord_qty = Column(Float(asdecimal=True))
    ord_qtyu = Column(Float(asdecimal=True))
    hdr_desc = Column(String(40))
    pop_amt = Column(Float(asdecimal=True))
    px_calcmfg = Column(Float(asdecimal=True))
    pop_cur = Column(Float(asdecimal=True))
    pref_date = Column(DateTime)
    dot_code = Column(String(4))
    gip_entry = Column(Float(asdecimal=True))
    gip_costs = Column(Float(asdecimal=True))
    gip_calc = Column(Float(asdecimal=True))
    gip_calceu = Column(Float(asdecimal=True))
    diff_qty = Column(Float(asdecimal=True))
    px_diffs = Column(Float(asdecimal=True))
    cos_amt = Column(Float(asdecimal=True))
    bns_disc = Column(Float(asdecimal=True))
    pmt_disc = Column(Float(asdecimal=True))
    post_pd = Column(BIT(1))
    sol_id = Column(String(10))
    amt_post = Column(Float(asdecimal=True))
    bem_cat = Column(String(10))
    bem_code = Column(String(10))
    bem_px = Column(Float(asdecimal=True))
    bem_amt = Column(Float(asdecimal=True))
    bem_vat = Column(Float(asdecimal=True))
    gip_ts_old = Column(Float(asdecimal=True))
    gip_px_old = Column(Float(asdecimal=True))
    gip_ts_new = Column(Float(asdecimal=True))
    gip_px_new = Column(Float(asdecimal=True))
    web_linenr = Column(String(20))
    rec_qty = Column(Float(asdecimal=True))
    ordnr_rec = Column(String(10))
    timestamp = Column(DateTime, nullable=False, index=True, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))


class ArticleCategory(Base):
    __tablename__ = 'articleCategories'

    id = Column(SmallInteger, primary_key=True)
    ag_code = Column(String(10), nullable=False, unique=True)
    articleCategory = Column(Enum('banden', 'sneeuwkettingen', 'velgen', 'olie', 'montage-artikelen', 'ruitenwissers', 'truckbanden', 'unknown'), nullable=False, server_default=text("'unknown'"))


class ArticleGroup(Base):
    __tablename__ = 'article_group'

    art_grp = Column(String(5), primary_key=True, unique=True, server_default=text("''"))
    grp_name = Column(String(32))
    grp_name_int = Column(String(32), nullable=False)


class Artikelen(Base):
    __tablename__ = 'artikelen'
    __table_args__ = (
        Index('gotPicture_2', 'gotPicture', 'watermerk', 'active'),
        Index('banden_breedte_2', 'banden_breedte', 'banden_hoogte', 'banden_inch')
    )

    artikel = Column(String(24), primary_key=True, server_default=text("''"))
    omschrijving_nl = Column(String(100), nullable=False)
    omschrijving_en = Column(String(100), nullable=False)
    omschrijving_fr = Column(String(100), nullable=False)
    omschrijving_de = Column(String(100), nullable=False)
    notitie = Column(String, nullable=False)
    infoOmschrijving = Column(String, nullable=False)
    artikelgroep = Column(String(2), nullable=False)
    subgroep = Column(String(3), nullable=False)
    subgroep_naam = Column(String(60), nullable=False)
    merk = Column(String(32), nullable=False)
    merk_sort = Column(Integer, nullable=False)
    verkoopprijs = Column(Numeric(9, 4), nullable=False, server_default=text("'0.0000'"))
    standaardkorting = Column(Numeric(7, 2), nullable=False, server_default=text("'0.00'"))
    prijs_incl_btw = Column(Numeric(9, 4), nullable=False, server_default=text("'0.0000'"))
    bruto_ex_prijs = Column(Numeric(9, 4), nullable=False)
    bruto_in_prijs = Column(Numeric(9, 4), nullable=False)
    verkoopeenheid = Column(String(2), nullable=False, server_default=text("''"))
    voorraad = Column(SmallInteger, nullable=False, server_default=text("'0'"))
    av_voorraad = Column(SmallInteger, nullable=False, server_default=text("'0'"))
    vrd3 = Column(Integer, nullable=False)
    av_vrd3 = Column(SmallInteger, nullable=False, server_default=text("'0'"))
    av_vrd_prijs = Column(Numeric(9, 4), nullable=False, server_default=text("'0.0000'"))
    vrd3_deltime = Column(Integer, nullable=False)
    vrd3_lev = Column(String(32), nullable=False)
    opmerking = Column(String(32), nullable=False, server_default=text("''"))
    afmeting = Column(String(24), nullable=False)
    intern_artikelnummer = Column(String(7), nullable=False, index=True, server_default=text("''"))
    barcode = Column(String(13), nullable=False, server_default=text("''"))
    leverweek = Column(Integer, nullable=False, server_default=text("'0'"))
    seizoen = Column(String(32), nullable=False)
    maat = Column(String(32), nullable=False)
    zoekmaat = Column(String(15), nullable=False, index=True)
    label = Column(String(32), nullable=False)
    li = Column(String(32), nullable=False)
    si = Column(String(32), nullable=False)
    profiel = Column(String(32), nullable=False)
    dot = Column(String(32), nullable=False)
    extra = Column(String(32), nullable=False)
    weight = Column(Numeric(10, 0), nullable=False)
    runflat = Column(Integer, nullable=False, server_default=text("'0'"))
    banden_breedte = Column(String(4), nullable=False, index=True)
    banden_hoogte = Column(String(4), nullable=False, index=True)
    banden_inch = Column(String(4), nullable=False, index=True)
    tyretype = Column(Integer, nullable=False)
    articleType = Column(String(255), nullable=False)
    articleCategory = Column(Enum('banden', 'sneeuwkettingen', 'velgen', 'olie', 'montage-artikelen', 'ruitenwissers', 'truckbanden', 'unknown'), nullable=False, index=True, server_default=text("'unknown'"))
    bijbehorend_aantal = Column(Integer)
    bijbehorend_artikel = Column(String(12))
    m_equi = Column(String(200), nullable=False)
    ag_code = Column(String(10), nullable=False, server_default=text("'0000'"))
    loc_code = Column(String(6), nullable=False)
    gotPicture = Column(Integer, nullable=False, index=True)
    watermerk = Column(Integer, nullable=False, server_default=text("'0'"))
    totalImages = Column(Integer, nullable=False)
    imageFileName_nl = Column(String(32), nullable=False)
    imageFileName_en = Column(String(32), nullable=False)
    imageFileName_fr = Column(String(32), nullable=False)
    imageFileName_de = Column(String(32), nullable=False)
    highlight = Column(Integer, nullable=False, server_default=text("'0'"))
    image = Column(String(120), nullable=False)
    t_bem = Column(Integer, nullable=False, server_default=text("'0'"))
    bem_cat = Column(String(12), nullable=False)
    active = Column(Integer, nullable=False, index=True, server_default=text("'0'"))
    timestamp = Column(DateTime, nullable=False, index=True, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))


class ArtikelenEquivalenten(Base):
    __tablename__ = 'artikelen_equivalenten'

    artikelnummer = Column(String(12), primary_key=True, nullable=False, index=True, server_default=text("''"))
    artikelnummer_equivalent = Column(String(12), primary_key=True, nullable=False, index=True, server_default=text("''"))


t_artikelen_samenstellingen = Table(
    'artikelen_samenstellingen', metadata,
    Column('artikel', String(12), nullable=False),
    Column('artikel_samenstelling', String(12), nullable=False),
    Column('aantal', Integer, nullable=False),
    Index('artikel', 'artikel', 'artikel_samenstelling', unique=True)
)


class Bem(Base):
    __tablename__ = 'bem'

    id = Column(Integer, primary_key=True)
    rec_id = Column(String(10))
    tbl_code = Column(String(10))
    cnt_code = Column(String(2))
    bem_cat = Column(String(10))
    bem_pchs = Column(String(10))
    bem_sale = Column(String(10))


class BemLine(Base):
    __tablename__ = 'bem_line'

    id = Column(Integer, primary_key=True)
    acct_bem = Column(String(10))
    bem_code = Column(String(10))
    bem_desc = Column(String(40))
    bem_type = Column(Integer)
    px_bem = Column(Numeric(4, 2))


class BestellingKop(Base):
    __tablename__ = 'bestelling_kop'
    __table_args__ = (
        Index('datum_tijd', 'datum_tijd', 'besteller_id'),
    )

    bestelling_id = Column(Integer, primary_key=True)
    datum_tijd = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    ip = Column(String(46, 'ascii_bin'), nullable=False, index=True, server_default=text("''"))
    besteller_id = Column(String(16), nullable=False, server_default=text("'0'"))
    email = Column(String(50), nullable=False)
    opmerking = Column(Text)
    acct_name = Column(String(64), nullable=False)
    address1 = Column(String(32), nullable=False)
    city = Column(String(32), nullable=False)
    post_code = Column(String(10), nullable=False)
    dpd_sender = Column(String(3), nullable=False, server_default=text("'mot'"))
    cnt_code = Column(String(32), nullable=False)
    ship_mth = Column(String(32), nullable=False)
    order_referentie = Column(String(32), nullable=False)
    betaal_methode = Column(String(2), nullable=False)
    bank_methode = Column(Integer, nullable=False)
    tti_comment1 = Column(String(100), nullable=False)
    tti_comment2 = Column(String(100), nullable=False)
    tti_comment3 = Column(Text, nullable=False)
    motoria_comment1 = Column(String(32), nullable=False)
    motoria_comment2 = Column(String(32), nullable=False)
    motoria_comment3 = Column(Text, nullable=False)
    totalProducts = Column(SmallInteger, nullable=False)
    subTotal = Column(Numeric(9, 4), nullable=False)
    sendCosts = Column(Numeric(9, 4), nullable=False)
    bemPrice = Column(Numeric(9, 4), nullable=False)
    taxPrice = Column(Numeric(9, 4), nullable=False)
    totalPrice = Column(Numeric(9, 4), nullable=False)
    profit = Column(Numeric(9, 4), nullable=False)
    profitPayed = Column(Integer, nullable=False, index=True, server_default=text("'0'"))
    akkoord_voorwaarden = Column(Integer, nullable=False, server_default=text("'0'"))
    isSpiegelShop = Column(Integer, nullable=False, server_default=text("'0'"))
    mailed = Column(Integer, nullable=False, server_default=text("'0'"))
    orderId = Column(String(32), nullable=False)
    entranceCode = Column(String(40), nullable=False)
    status = Column(Integer, nullable=False, server_default=text("'0'"))
    payNLPayment = Column(Integer, nullable=False, server_default=text("'0'"))
    payNLStatus = Column(String(8), nullable=False)
    xml = Column(Integer, nullable=False, index=True)


class BestellingRegel(Base):
    __tablename__ = 'bestelling_regels'
    __table_args__ = (
        Index('bestelling_id_3', 'bestelling_id', 'artikel', 'productType', unique=True),
    )

    id = Column(Integer, primary_key=True)
    bestelling_id = Column(Integer, nullable=False, index=True, server_default=text("'0'"))
    artikel = Column(String(24, 'ascii_bin'), nullable=False, server_default=text("''"))
    aantal = Column(Integer, nullable=False, server_default=text("'0'"))
    prijs = Column(Float, nullable=False, server_default=text("'0'"))
    status = Column(Integer, nullable=False, server_default=text("'0'"))
    opmerking = Column(Text(collation='ascii_bin'))
    productType = Column(Enum('part', 'tyre', 'krasactie', 'verzendCode', 'profit'), nullable=False, index=True)
    voorraadType = Column(Integer, nullable=False, server_default=text("'0'"))
    vrd3_lev = Column(String(32, 'ascii_bin'), nullable=False)
    klant_prijs = Column(Float)


class ChainsAndSock(Base):
    __tablename__ = 'chainsAndSocks'

    id = Column(SmallInteger, primary_key=True)
    chain = Column(Integer, nullable=False)
    width = Column(Integer, nullable=False)
    height = Column(Integer, nullable=False)
    inch = Column(Integer, nullable=False)
    size = Column(Integer, nullable=False)
    type = Column(Enum('matic', 'socks', '', ''), nullable=False)
    artikel = Column(String(24), nullable=False, index=True)


class DebiteurActiviteiten(Base):
    __tablename__ = 'debiteur_activiteiten'

    debiteur = Column(String(7), primary_key=True)
    laatst_actief = Column(DateTime, nullable=False)


class Debiteuren(Base):
    __tablename__ = 'debiteuren'
    __table_args__ = (
        Index('debiteurennaam', 'debiteurennaam', 'wachtwoord'),
    )

    debiteurennummer = Column(String(16), primary_key=True, server_default=text("''"))
    uniqueID = Column(String(40), nullable=False, index=True)
    debiteurennaam = Column(String(64))
    debiteurenemail = Column(String(50), nullable=False)
    adres = Column(String(64))
    postcode = Column(String(7), nullable=False, server_default=text("''"))
    plaats = Column(String(18), nullable=False, server_default=text("''"))
    country = Column(String(32), nullable=False)
    landcode = Column(String(2), nullable=False, server_default=text("'00'"))
    cnt_code = Column(String(2), nullable=False)
    rayonNr = Column(Integer, nullable=False)
    telefoonnummer = Column(String(24), nullable=False, server_default=text("''"))
    alfacode = Column(String(7), nullable=False, server_default=text("''"))
    bankrekeningnummer = Column(String(64), nullable=False, server_default=text("''"))
    girorekeningnummer = Column(String(7), nullable=False, server_default=text("''"))
    bankSwift = Column(String(32), nullable=False)
    automatischeIncasso = Column(Integer, nullable=False, server_default=text("'0'"))
    verwijsnummer = Column(String(7), nullable=False, server_default=text("''"))
    aantal_dagen_crediet = Column(String(3), nullable=False, server_default=text("''"))
    factuurkorting = Column(Float)
    prijskolom = Column(String(12), nullable=False, server_default=text("''"))
    verzendwijze = Column(String(10), nullable=False, server_default=text("''"))
    contactpersoon = Column(String(25), nullable=False, server_default=text("''"))
    statuscode = Column(String(1), nullable=False, server_default=text("''"))
    wachtwoord = Column(String(32), nullable=False, server_default=text("''"))
    artikelgroep = Column(String(99), nullable=False, server_default=text("''"))
    max_korting = Column(Float)
    taal = Column(String(1), nullable=False)
    timestamp = Column(DateTime, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))
    omzetStaffel = Column(String(8), nullable=False)
    disc_code = Column(String(16), nullable=False)
    customerPriceActive = Column(Integer, nullable=False, server_default=text("'0'"))
    customerPricePercentage = Column(Integer, nullable=False, server_default=text("'0'"))
    settings = Column(String, nullable=False)
    changed = Column(Integer, nullable=False, index=True, server_default=text("'0'"))
    changedDetails = Column(String, nullable=False)
    active = Column(Integer, nullable=False)
    cng_date = Column(DateTime, nullable=False, index=True)


t_debiteuren_email = Table(
    'debiteuren_email', metadata,
    Column('debiteur', String(16), nullable=False),
    Column('ip', String(15), nullable=False),
    Column('datum', DateTime, nullable=False),
    Column('email', String(50), nullable=False),
    Index('debiteur', 'debiteur', 'email', unique=True)
)


class DebiteurenWebshop(Base):
    __tablename__ = 'debiteuren_webshop'

    debiteurnummer = Column(String(7), primary_key=True)
    debiteurnaam = Column(String(50), nullable=False)
    emailadres = Column(String(50), nullable=False)
    banner = Column(String(150), nullable=False)
    letterkleur = Column(String(7), nullable=False)
    ideal = Column(Integer, nullable=False)
    logo = Column(String(32), nullable=False)
    cssFile = Column(String(255), nullable=False)
    settings = Column(Text, nullable=False)
    active = Column(Integer, nullable=False, server_default=text("'0'"))


class KrasActie(Base):
    __tablename__ = 'krasActie'

    id = Column(SmallInteger, primary_key=True)
    krasCode = Column(String(16), nullable=False, unique=True)
    article = Column(String(24), nullable=False)
    brand = Column(String(32), nullable=False)
    description = Column(Text, nullable=False)
    size = Column(String(16), nullable=False)
    amount = Column(SmallInteger, nullable=False)


t_krasActieFoutieveCodes = Table(
    'krasActieFoutieveCodes', metadata,
    Column('debiteurnummer', String(32), nullable=False),
    Column('krascode', String(16), nullable=False),
    Column('date', DateTime, nullable=False)
)


class KraslotDebiteur(Base):
    __tablename__ = 'kraslotDebiteur'

    id = Column(Integer, primary_key=True)
    debiteurnummer = Column(String(16), nullable=False)
    krascode = Column(String(16), nullable=False, unique=True)
    addedToOrder = Column(Integer, nullable=False, index=True, server_default=text("'0'"))
    redeemedDate = Column(DateTime, nullable=False)
    orderDate = Column(DateTime, nullable=False)


t_openitem = Table(
    'openitem', metadata,
    Column('sub_nr', String(10, 'utf8_bin'), nullable=False, index=True),
    Column('deb_show', Integer, nullable=False),
    Column('cred_show', Integer, nullable=False),
    Column('inv_nr', String(12, 'utf8_bin'), nullable=False, unique=True),
    Column('trn_date', Date, nullable=False),
    Column('date_paid', Date, nullable=False),
    Column('trn_desc', String(40, 'utf8_bin'), nullable=False),
    Column('cost_code', String(10, 'utf8_bin'), nullable=False),
    Column('cur_code', String(3, 'utf8_bin'), nullable=False),
    Column('dr_type', Integer, nullable=False),
    Column('dr_amt', Float(asdecimal=True), nullable=False),
    Column('cr_amt', Float(asdecimal=True), nullable=False),
    Column('cur_damt', Float(asdecimal=True), nullable=False),
    Column('cur_camt', Float(asdecimal=True), nullable=False),
    Column('pay_desc', String(130, 'utf8_bin'), nullable=False),
    Column('amt_pay', Float(asdecimal=True), nullable=False),
    Column('amt_mark', Float(asdecimal=True), nullable=False),
    Column('pd_amt', Float(asdecimal=True), nullable=False),
    Column('pd_pct', Float, nullable=False),
    Column('disc_days', Float, nullable=False),
    Column('cred_days', Float, nullable=False),
    Column('post_type', Float, nullable=False),
    Column('ag_date', Date, nullable=False),
    Column('rem_date', Date, nullable=False),
    Column('fiat_prio', Float, nullable=False),
    Column('rem_cnt', Float, nullable=False),
    Column('pmt_chq', Integer, nullable=False),
    Column('rem_blk', Integer, nullable=False),
    Column('pmt_ord1', String(2, 'utf8_bin'), nullable=False),
    Column('pmt_ord2', String(2, 'utf8_bin'), nullable=False),
    Column('pay_prom', Date, nullable=False),
    Column('pmt_ord3', String(2, 'utf8_bin'), nullable=False),
    Column('pay_coll', Integer, nullable=False),
    Column('pmt_ord4', String(2, 'utf8_bin'), nullable=False),
    Column('mark_pg', Integer, nullable=False),
    Column('mark_dd', Integer, nullable=False),
    Column('pmt_type', String(2, 'utf8_bin'), nullable=False),
    Column('pmt_goods', String(2, 'utf8_bin'), nullable=False),
    Column('pmt_arts', String(2, 'utf8_bin'), nullable=False),
    Column('pmt_desc', String(80, 'utf8_bin'), nullable=False),
    Column('pmt_crecv', Integer, nullable=False),
    Column('pmt_csend', Integer, nullable=False),
    Column('pmt_curr', String(3, 'utf8_bin'), nullable=False),
    Column('pmt_amt', Float, nullable=False),
    Column('pmt_fast', Integer, nullable=False),
    Column('dd_act', Integer, nullable=False),
    Column('pg_blk', Integer, nullable=False),
    Column('inp_date', DateTime, nullable=False),
    Column('cng_nr', Integer, nullable=False),
    Column('cng_date', DateTime),
    Column('grek_type', String(1, 'utf8_bin'), nullable=False),
    Column('bank_acct', String(10, 'utf8_bin'), nullable=False),
    Column('disp_inv', Integer, nullable=False),
    Column('item_proc', Integer, nullable=False),
    Column('comment1', String(collation='utf8_bin'), nullable=False),
    Column('coll_date', Date, nullable=False),
    Column('dj_code', String(3, 'utf8_bin'), nullable=False),
    Column('page_nr', String(10, 'utf8_bin'), nullable=False),
    Column('oi_stat', Float, nullable=False),
    Column('auth_emp', String(10, 'utf8_bin'), nullable=False),
    Column('auth_date', Date, nullable=False),
    Column('proj_code', String(10, 'utf8_bin'), nullable=False),
    Column('pay_id', String(10, 'utf8_bin'), nullable=False),
    Column('amt_int', Float(asdecimal=True), nullable=False),
    Column('cur_int', Float(asdecimal=True), nullable=False),
    Column('doc_id', String(32, 'utf8_bin'), nullable=False),
    Column('dpd_nrs', String(collation='utf8_bin'), nullable=False),
    Column('dpd_pacnr', String(collation='utf8_bin'), nullable=False),
    Column('c4_discc', String(2, 'utf8_bin'), nullable=False),
    Column('mand_id', String(35, 'utf8_bin'), nullable=False),
    Column('rec_mark', Integer, nullable=False),
    Column('mand_date', Date, nullable=False),
    Column('mand_b2b', Integer, nullable=False),
    Column('mand_seq', Integer, nullable=False),
    Column('sav_cdays', Float, nullable=False),
    Column('sav_ddays', Float, nullable=False),
    Column('sav_pdpct', Float, nullable=False),
    Column('timestamp', DateTime, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP")),
    Index('sub_nr_2', 'sub_nr', 'dr_amt', 'cr_amt')
)


class PayNlBank(Base):
    __tablename__ = 'payNlBanks'

    id = Column(SmallInteger, primary_key=True)
    name = Column(String(32), nullable=False)
    issuerId = Column(String(8), nullable=False)
    swift = Column(String(32), nullable=False)
    icon = Column(String(64), nullable=False)
    available = Column(Integer, nullable=False, index=True)


class PayNlIp(Base):
    __tablename__ = 'payNlIps'

    ip = Column(String(20), primary_key=True)


class PayNlPaymentMethod(Base):
    __tablename__ = 'payNlPaymentMethods'

    id = Column(Integer, primary_key=True)
    paymentProfileId = Column(SmallInteger, nullable=False, unique=True)
    countryCode = Column(String(3), nullable=False)
    number = Column(String(32), nullable=False)
    image = Column(String(64), nullable=False)


t_rma_kop = Table(
    'rma_kop', metadata,
    Column('id', Integer, nullable=False),
    Column('debiteurnummer', String(16), nullable=False),
    Column('pdf', Integer, nullable=False, index=True, server_default=text("'0'")),
    Column('mailed', Integer, nullable=False, index=True, server_default=text("'0'"))
)


class RmaRegel(Base):
    __tablename__ = 'rma_regels'

    id = Column(Integer, primary_key=True)
    rma_kop_id = Column(Integer, nullable=False)
    rec_id = Column(String(10), nullable=False)
    artikelnummer = Column(String(24), nullable=False)
    aantal = Column(SmallInteger, nullable=False)
    reden = Column(String(32), nullable=False)


class RoadPromotion(Base):
    __tablename__ = 'roadPromotion'

    debiteurennummer = Column(String(16), primary_key=True)
    verkoper = Column(Enum('Filiz Agca', 'Lesly Bastemeijer', 'Merel Braber', 'Nienke Schuijt', 'Nynke de Vries', 'Roos Ketting', 'Veronique van Esch'), nullable=False)
    firstLogin = Column(Integer, nullable=False, index=True, server_default=text("'0'"))


class SearchResult(Base):
    __tablename__ = 'searchResults'
    __table_args__ = (
        Index('debiteurnummer', 'debiteurnummer', 'webshopType', 'customerPriceType', 'keyword', 'pagename', 'mode', unique=True),
    )

    id = Column(Integer, primary_key=True)
    debiteurnummer = Column(String(20), nullable=False)
    webshopType = Column(Integer, nullable=False, index=True)
    customerPriceType = Column(Integer, nullable=False)
    keyword = Column(String(32), nullable=False)
    startSearchDate = Column(Date, nullable=False)
    searchDate = Column(Date, nullable=False)
    searchTime = Column(DateTime, nullable=False)
    totalRows = Column(Integer, nullable=False)
    page = Column(SmallInteger, nullable=False)
    pagename = Column(Enum('sneeuwkettingen', 'velgen', 'olie', 'ruitenwissers', 'banden', 'product_zoeken', 'montage-artikelen', 'truckbanden'), nullable=False)
    mode = Column(Enum('lijst', 'gallerij', ''), nullable=False)
    query = Column(Text, nullable=False)
    results = Column(String, nullable=False)
    startResults = Column(String, nullable=False)
    uniqueId = Column(String(128), nullable=False, index=True)


class SoHdr(Base):
    __tablename__ = 'so_hdr'

    ord_nr = Column(String(10, 'utf8_bin'), primary_key=True)
    pac_nr = Column(String(10, 'utf8_bin'), nullable=False)
    rpl_del = Column(String(10, 'utf8_bin'), nullable=False)
    ord_date = Column(Date, nullable=False)
    del_date = Column(Date, nullable=False)
    inv_date = Column(Date, nullable=False)
    rpl_inv = Column(String(10, 'utf8_bin'), nullable=False)
    cur_code = Column(String(3, 'utf8_bin'), nullable=False)
    exch_rate = Column(Float(asdecimal=True), nullable=False)
    sub_tot = Column(Float(asdecimal=True), nullable=False)
    vat_tot = Column(Float(asdecimal=True), nullable=False)
    del_addr = Column(Text)
    ord_tot = Column(Float(asdecimal=True), nullable=False)
    disc_code = Column(String(2, 'utf8_bin'), nullable=False)
    ship_mth = Column(String(10, 'utf8_bin'), nullable=False)
    ord_cost = Column(Float(asdecimal=True), nullable=False)
    ship_cost = Column(Float(asdecimal=True), nullable=False)
    vc_ord = Column(String(2, 'utf8_bin'), nullable=False)
    vc_ship = Column(String(2, 'utf8_bin'), nullable=False)
    vat_ord = Column(Float(asdecimal=True), nullable=False)
    vat_ship = Column(Float(asdecimal=True), nullable=False)
    pd_pct = Column(Float, nullable=False)
    pd_amt = Column(Float(asdecimal=True), nullable=False)
    pd_vat = Column(Float(asdecimal=True), nullable=False)
    inv_cp = Column(String(40, 'utf8_bin'), nullable=False)
    inv_pp = Column(String(10, 'utf8_bin'), nullable=False)
    del_cp = Column(String(40, 'utf8_bin'), nullable=False)
    del_pp = Column(String(10, 'utf8_bin'), nullable=False)
    disc_days = Column(Float, nullable=False)
    cred_days = Column(Float, nullable=False)
    pay_amt = Column(Float(asdecimal=True), nullable=False)
    cost_code = Column(String(10, 'utf8_bin'), nullable=False)
    proj_code = Column(String(10, 'utf8_bin'), nullable=False)
    sel_code = Column(String(5, 'utf8_bin'), nullable=False)
    emp_nr = Column(String(10, 'utf8_bin'), nullable=False)
    comment1 = Column(String(40, 'utf8_bin'), nullable=False)
    comment3 = Column(String(collation='utf8_bin'), nullable=False)
    comment2 = Column(String(collation='utf8_bin'), nullable=False)
    ord_ship = Column(Integer, nullable=False)
    ord_bo = Column(Integer, nullable=False)
    ord_proc = Column(Integer, nullable=False)
    ord_compl = Column(Integer, nullable=False)
    ord_invd = Column(Integer, nullable=False)
    ord_stat = Column(Float, nullable=False)
    ord_coll = Column(Integer, nullable=False)
    ord_keep = Column(Integer, nullable=False)
    gross_flg = Column(Integer, nullable=False)
    cng_nr = Column(Integer, nullable=False)
    del_not = Column(Integer, nullable=False)
    date_not = Column(Integer, nullable=False)
    address1 = Column(String(80, 'utf8_bin'), nullable=False)
    city = Column(String(25, 'utf8_bin'), nullable=False)
    post_code = Column(String(10, 'utf8_bin'), nullable=False)
    country = Column(String(30, 'utf8_bin'), nullable=False)
    po_box = Column(String(80, 'utf8_bin'), nullable=False)
    po_city = Column(String(25, 'utf8_bin'), nullable=False)
    po_code = Column(String(10, 'utf8_bin'), nullable=False)
    st_street = Column(String(60, 'utf8_bin'), nullable=False)
    st_nr = Column(String(10, 'utf8_bin'), nullable=False)
    st_extra = Column(String(10, 'utf8_bin'), nullable=False)
    st_prop = Column(String(40, 'utf8_bin'), nullable=False)
    acct_name = Column(String(40, 'utf8_bin'), nullable=False)
    st_region = Column(String(40, 'utf8_bin'), nullable=False)
    soh_type = Column(Integer, nullable=False)
    wage_sh = Column(Float(asdecimal=True), nullable=False)
    blk_amt = Column(Float(asdecimal=True), nullable=False)
    bv_amt = Column(Float(asdecimal=True), nullable=False)
    lb_amt = Column(Float(asdecimal=True), nullable=False)
    dd_act = Column(Integer, nullable=False)
    ass_code = Column(String(10, 'utf8_bin'), nullable=False)
    apx_list = Column(String(5, 'utf8_bin'), nullable=False)
    pct_list = Column(String(5, 'utf8_bin'), nullable=False)
    lng_code = Column(String(5, 'utf8_bin'), nullable=False)
    sg_code = Column(String(10, 'utf8_bin'), nullable=False)
    cnt_code = Column(String(2, 'utf8_bin'), nullable=False)
    sbs_nr = Column(String(10, 'utf8_bin'), nullable=False)
    del_code = Column(String(10, 'utf8_bin'), nullable=False)
    auth_hist = Column(String(collation='utf8_bin'), nullable=False)
    qt_ord_nr = Column(String(10, 'utf8_bin'), nullable=False)
    whs_code = Column(String(5, 'utf8_bin'), nullable=False)
    pol_prio = Column(Float, nullable=False)
    ship_comp = Column(Integer, nullable=False)
    page_blk = Column(Integer, nullable=False)
    inp_usr = Column(String(20, 'utf8_bin'), nullable=False)
    inp_date = Column(DateTime, nullable=False)
    trn_book = Column(Integer, nullable=False)
    trn_start = Column(Integer, nullable=False)
    trn_cnt = Column(Integer, nullable=False)
    cng_usr = Column(String(20, 'utf8_bin'), nullable=False)
    l5_address = Column(String(10, 'utf8_bin'), nullable=False)
    cng_date = Column(DateTime, nullable=False)
    dpd_nrs = Column(String(collation='utf8_bin'), nullable=False)
    dpd_pacnr = Column(String(collation='utf8_bin'), nullable=False)
    dt_exp = Column(DateTime, nullable=False)
    edi_3rd = Column(Integer, nullable=False)
    edi_exp = Column(Integer, nullable=False)
    excl_mail = Column(Integer, nullable=False)
    inv_per = Column(String(10, 'utf8_bin'), nullable=False)
    pref_date = Column(Date, nullable=False)
    tot_tinv = Column(Float, nullable=False)
    user_exp = Column(String(20, 'utf8_bin'), nullable=False)
    ic_adm = Column(String(10, 'utf8_bin'), nullable=False)
    ic_order = Column(Integer, nullable=False)
    to_email = Column(String(collation='utf8_bin'), nullable=False)
    cc_email = Column(String(collation='utf8_bin'), nullable=False)
    bcc_email = Column(String(collation='utf8_bin'), nullable=False)
    mand_id = Column(String(35, 'utf8_bin'), nullable=False)
    mand_date = Column(Date, nullable=False)
    mand_b2b = Column(Integer, nullable=False)
    mand_seq = Column(Integer, nullable=False)
    c1_recblk = Column(Integer, nullable=False)
    rec_blk = Column(Integer, nullable=False)
    smail_bus = Column(String(50, 'utf8_bin'), nullable=False)
    spc_blk = Column(Integer, nullable=False)
    stel_bus = Column(String(20, 'utf8_bin'), nullable=False)
    dpd_sender = Column(String(3, 'utf8_bin'), nullable=False)
    gss_ord_st = Column(Float, nullable=False)
    ic_ref = Column(String(10, 'utf8_bin'), nullable=False)
    nr_parcels = Column(Float, nullable=False)
    rembours = Column(Integer, nullable=False)
    imp_file = Column(String(100, 'utf8_bin'), nullable=False)
    web_nr = Column(String(20, 'utf8_bin'), nullable=False)
    web_user = Column(String(10, 'utf8_bin'), nullable=False)
    usr_blk = Column(String(20, 'utf8_bin'), nullable=False)
    web_email = Column(String(50, 'utf8_bin'), nullable=False)
    pers_name = Column(String(40, 'utf8_bin'), nullable=False)
    usr_unblk = Column(String(20, 'utf8_bin'), nullable=False)
    blk_status = Column(Float, nullable=False)
    web_phone = Column(String(20, 'utf8_bin'), nullable=False)
    web_fax = Column(String(20, 'utf8_bin'), nullable=False)
    web_comm = Column(String(collation='utf8_bin'), nullable=False)
    web_note = Column(String(collation='utf8_bin'), nullable=False)
    web_ack = Column(Integer, nullable=False)
    web_stat = Column(String(20, 'utf8_bin'), nullable=False)
    tot_bem = Column(Float(asdecimal=True), nullable=False)
    tot_bvat = Column(Float(asdecimal=True), nullable=False)
    wm_code = Column(String(10, 'utf8_bin'), nullable=False)
    timestamp = Column(DateTime, nullable=False, index=True, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))


t_so_line = Table(
    'so_line', metadata,
    Column('ord_nr', String(10, 'utf8_bin'), nullable=False, index=True),
    Column('art_code', String(15, 'utf8_bin'), nullable=False, index=True),
    Column('whs_code', String(5, 'utf8_bin'), nullable=False),
    Column('loc_code', String(5, 'utf8_bin'), nullable=False),
    Column('art_desc1', Text, nullable=False),
    Column('unt_code', String(10, 'utf8_bin'), nullable=False),
    Column('unt_codep', String(10, 'utf8_bin'), nullable=False),
    Column('ord_qty', Float(asdecimal=True), nullable=False),
    Column('ord_qtyu', Float(asdecimal=True), nullable=False),
    Column('art_qty', Float(asdecimal=True), nullable=False),
    Column('art_qtyu', Float(asdecimal=True), nullable=False),
    Column('ship_qty', Float(asdecimal=True), nullable=False),
    Column('inv_qty', Float(asdecimal=True), nullable=False),
    Column('tinv_qty', Float(asdecimal=True), nullable=False),
    Column('tinv_qtyu', Float(asdecimal=True), nullable=False),
    Column('art_px', Float, nullable=False),
    Column('px_calc', Float(asdecimal=True), nullable=False),
    Column('disc_pct', Float, nullable=False),
    Column('del_date', Date, nullable=False),
    Column('pc_code', String(5, 'utf8_bin'), nullable=False),
    Column('cost_code', String(10, 'utf8_bin'), nullable=False),
    Column('emp_nr', String(10, 'utf8_bin'), nullable=False),
    Column('art_ca', Integer, nullable=False),
    Column('bm_tag', String(10, 'utf8_bin'), nullable=False),
    Column('art_level', Integer, nullable=False),
    Column('no_res', Integer, nullable=False),
    Column('sol_nocal', Integer, nullable=False),
    Column('vat_code', String(2, 'utf8_bin'), nullable=False),
    Column('vat_amt', Float(asdecimal=True), nullable=False),
    Column('sol_hide', Integer, nullable=False),
    Column('art_amt', Float(asdecimal=True), nullable=False),
    Column('rec_id', String(10, 'utf8_bin'), nullable=False, unique=True),
    Column('pol_id', String(10, 'utf8_bin'), nullable=False),
    Column('is_month', Integer, nullable=False),
    Column('pol_prio', Float, nullable=False),
    Column('icat_code', String(1, 'utf8_bin'), nullable=False),
    Column('pol_pb', Integer, nullable=False),
    Column('icnt_tgt', String(2, 'utf8_bin'), nullable=False),
    Column('icnt_src', String(2, 'utf8_bin'), nullable=False),
    Column('ishp_code', String(1, 'utf8_bin'), nullable=False),
    Column('is_cntnr', Float, nullable=False),
    Column('isys_code', String(2, 'utf8_bin'), nullable=False),
    Column('ityp_code', String(1, 'utf8_bin'), nullable=False),
    Column('isxf_code', String(3, 'utf8_bin'), nullable=False),
    Column('is_weight', Float, nullable=False),
    Column('is_custcd', Float, nullable=False),
    Column('rec_ord', Integer, nullable=False),
    Column('parent_id', String(10, 'utf8_bin'), nullable=False),
    Column('std_qty', Float(asdecimal=True), nullable=False),
    Column('org_ordnr', String(10, 'utf8_bin'), nullable=False),
    Column('sys_gen', String(10, 'utf8_bin'), nullable=False),
    Column('sell_info', String(collation='utf8_bin'), nullable=False),
    Column('nr_packs', Float, nullable=False),
    Column('stat_info', Integer, nullable=False),
    Column('art_gen', String(15, 'utf8_bin'), nullable=False),
    Column('rel_gen', Float, nullable=False),
    Column('dsp_level', Integer, nullable=False),
    Column('sbsl_id', String(10, 'utf8_bin'), nullable=False),
    Column('ship_comp', Integer, nullable=False),
    Column('proj_code', String(10, 'utf8_bin'), nullable=False),
    Column('l5_bonnr', String(15, 'utf8_bin'), nullable=False),
    Column('l5_spec', String(40, 'utf8_bin'), nullable=False),
    Column('unt_px', Float, nullable=False),
    Column('canc_qty', Float(asdecimal=True), nullable=False),
    Column('canc_qtyu', Float(asdecimal=True), nullable=False),
    Column('c1_art_qty', Float(asdecimal=True), nullable=False),
    Column('hdr_desc', String(40, 'utf8_bin'), nullable=False),
    Column('lineyesno', Integer, nullable=False),
    Column('seq_nr', Integer, nullable=False),
    Column('pref_date', Date, nullable=False),
    Column('dot_code', String(4, 'utf8_bin'), nullable=False),
    Column('dot_max', String(4, 'utf8_bin'), nullable=False),
    Column('gip_entry', Float, nullable=False),
    Column('cos_amt', Float(asdecimal=True), nullable=False),
    Column('gip_ts_old', Float, nullable=False),
    Column('gip_ts_new', Float, nullable=False),
    Column('sol_id', String(10, 'utf8_bin'), nullable=False),
    Column('web_linenr', String(20, 'utf8_bin'), nullable=False),
    Column('bem_cat', String(10, 'utf8_bin'), nullable=False),
    Column('bem_code', String(10, 'utf8_bin'), nullable=False),
    Column('bem_px', Float, nullable=False),
    Column('bem_amt', Float(asdecimal=True), nullable=False),
    Column('bem_vat', Float(asdecimal=True), nullable=False),
    Column('timestamp', DateTime, nullable=False, index=True, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))
)


class SpiegelshopToegang(Base):
    __tablename__ = 'spiegelshop_toegang'

    debiteurnummer = Column(String(16), primary_key=True)
    timestamp = Column(DateTime, nullable=False, server_default=text("CURRENT_TIMESTAMP"))
    toegang = Column(Integer, nullable=False, server_default=text("'1'"))


class Staffel1(Base):
    __tablename__ = 'staffel1'
    __table_args__ = (
        Index('qd_id', 'qd_id', 'qd_qty'),
    )

    rec_id = Column(String(24), primary_key=True)
    qd_id = Column(String(24), nullable=False)
    qd_qty = Column(Integer, nullable=False, index=True, server_default=text("'0'"))
    qd_qtyu = Column(Integer, nullable=False, server_default=text("'0'"))
    qd_val = Column(Numeric(9, 3), nullable=False, server_default=text("'0.000'"))
    qd_valu = Column(Numeric(19, 3), nullable=False)
    px_netto = Column(Numeric(9, 3), nullable=False, server_default=text("'0.000'"))
    timestamp = Column(DateTime, nullable=False, index=True, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))


class Staffel2(Base):
    __tablename__ = 'staffel2'
    __table_args__ = (
        Index('sub_nr', 'sub_nr', 'pl_code', 'art_code'),
        Index('pl_code', 'pl_code', 'art_code')
    )

    rec_id = Column(String(10), primary_key=True, server_default=text("''"))
    sub_nr = Column(String(10))
    pl_code = Column(String(5))
    art_code = Column(String(15))
    ag_code = Column(String(5))
    qd_sp = Column(Float(asdecimal=True))
    qd_type = Column(Float(asdecimal=True))
    qd_desc = Column(String(40))
    unt_code = Column(String(10))
    cur_code = Column(String(3))
    intr_date = Column(DateTime)
    beg_date = Column(Date)
    end_date = Column(Date)
    rec_blk = Column(BIT(1))
    cng_nr = Column(Integer)
    inp_date = Column(DateTime)
    cng_date = Column(DateTime)
    cng_usr = Column(String(20))
    inp_usr = Column(String(20))
    qs_base = Column(Float(asdecimal=True))
    qs_perc = Column(Float(asdecimal=True))
    qs_amount = Column(Float(asdecimal=True))
    qs_high = Column(Integer, index=True, server_default=text("'0'"))
    comment1 = Column(Text)
    qd_qty = Column(Integer, nullable=False, server_default=text("'0'"))
    qd_val = Column(Numeric(9, 3), nullable=False, server_default=text("'0.000'"))
    marge = Column(Numeric(6, 2), nullable=False)
    active = Column(Integer, nullable=False, index=True, server_default=text("'0'"))
    timestamp = Column(DateTime, nullable=False, index=True, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))


t_tyre_sel = Table(
    'tyre_sel', metadata,
    Column('rec_id', String(10, 'utf8_bin'), nullable=False, unique=True),
    Column('type', String(40, 'utf8_bin'), nullable=False),
    Column('des_car', String(40, 'utf8_bin'), nullable=False),
    Column('follow_up', String(40, 'utf8_bin'), nullable=False),
    Column('rim_front', String(15, 'utf8_bin'), nullable=False, index=True),
    Column('rim_back', String(15, 'utf8_bin'), nullable=False, index=True),
    Column('air_front', String(3, 'utf8_bin'), nullable=False),
    Column('air_back', String(3, 'utf8_bin'), nullable=False),
    Column('front_tire', String(15, 'utf8_bin'), nullable=False, index=True),
    Column('back_tire', String(15, 'utf8_bin'), nullable=False, index=True),
    Column('yr_manuf', Date, nullable=False)
)


t_vat = Table(
    'vat', metadata,
    Column('vat_code', String(2, 'utf8_bin'), nullable=False, unique=True),
    Column('vat_name', String(40, 'utf8_bin'), nullable=False),
    Column('vat_acct', String(10, 'utf8_bin'), nullable=False),
    Column('vat_pct', Float, nullable=False),
    Column('vat_type', Float, nullable=False, index=True),
    Column('calc_type', Float, nullable=False),
    Column('frm_code', String(10, 'utf8_bin'), nullable=False),
    Column('acct_memo', String(10, 'utf8_bin'), nullable=False),
    Column('frm_memo', String(10, 'utf8_bin'), nullable=False),
    Column('vat_link', String(2, 'utf8_bin'), nullable=False),
    Column('cogs_type', Integer, nullable=False),
    Column('calc_cum', Integer, nullable=False),
    Column('cng_nr', Integer, nullable=False),
    Column('inp_date', DateTime, nullable=False),
    Column('cng_date', DateTime, nullable=False),
    Column('cng_usr', String(20, 'utf8_bin'), nullable=False),
    Column('inp_usr', String(20, 'utf8_bin'), nullable=False),
    Column('is_abc', Integer, nullable=False),
    Column('l2_invtype', String(1, 'utf8_bin'), nullable=False),
    Column('l2_vattp2', String(1, 'utf8_bin'), nullable=False, index=True),
    Column('l2_djtype', Float, nullable=False),
    Column('l2_catvtcr', String(10, 'utf8_bin'), nullable=False, index=True),
    Column('l2_catvtdr', String(10, 'utf8_bin'), nullable=False, index=True),
    Column('l2_catctdr', String(10, 'utf8_bin'), nullable=False, index=True),
    Column('l2_catccr1', String(10, 'utf8_bin'), nullable=False, index=True),
    Column('l2_catccr2', String(10, 'utf8_bin'), nullable=False, index=True),
    Column('l2_cattrd', String(10, 'utf8_bin'), nullable=False, index=True),
    Column('l2_vatpart', Integer, nullable=False),
    Column('l2_vatperc', Float, nullable=False),
    Column('l2_ictype', Float, nullable=False),
    Column('timestamp', DateTime, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP")),
    Index('l2_codetyp', 'l2_vattp2', 'vat_code')
)


t_winkelwagen_sessies = Table(
    'winkelwagen_sessies', metadata,
    Column('klantid', String(50), nullable=False),
    Column('artikelnummer', String(20), nullable=False),
    Column('aantal', Integer, nullable=False),
    Column('omschrijving', String(255)),
    Column('motor', String(8)),
    Column('bouwjaar', String(4)),
    Column('opmerking', Text),
    Column('added', DateTime, nullable=False),
    Index('klantid', 'klantid', 'artikelnummer', unique=True)
)
