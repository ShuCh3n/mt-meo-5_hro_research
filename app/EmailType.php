<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmailType extends Model
{
    public function infos()
    {
    	return $this->hasMany(EmailTypeInfo::class);
    }

    public function info()
    {
    	return $this->hasOne(EmailTypeInfo::class);
    }
}
