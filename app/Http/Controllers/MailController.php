<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Shop;
use App\Language;
use Mail;

class MailController extends Controller
{
    public function sendWelcomeMail($user)
    {
    	$email = new NewCustomer($this->request);
    	$email->name = $user->firstname . ' ' . $user->lastname;

    	Mail::to($user->email)->send($email);
    }

    public function sendNewPassword($user, $password_reset)
    {
    	$email = new NewPassword($this->request);
    	$email->gender = ($user->gender)? "Dhr." : "Mevr.";
    	$email->lastname = $user->lastname;
    	$email->email = $user->email;
    	$email->token = $password_reset->token;

    	Mail::to($user->email)->send($email);
    }
}
