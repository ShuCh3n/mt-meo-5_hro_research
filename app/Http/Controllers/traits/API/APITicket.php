<?php

namespace App\Http\Controllers\traits\API;

use App\Ticket;
use App\Order;
use App\TicketDepartment;

trait APITicket {
	public function newMessage()
	{
		$ticket = $this->findExistTicket();

		if(!$ticket)
		{
			$ticket = $this->newTicket();
		}

		$ticket->addMessage($this->request, true);
	}

	private function findExistTicket()
	{
		$order = Order::where('ref', $this->request->order_reference)->first();
		$customer = $this->request->customer_session->customer ?? null;

		if(!$customer)
		{
			$customer = $this->request->shop->customers()->where('email', $this->request->email)->first() ?? null;
		}

		$ticket = $this->request->shop->tickets()->notClosed();

		if($order)
		{
			$ticket->where('order_id', $order->id);
		}

		if($customer)
		{
			$ticket->where('customer_id', $customer->id);
		}

		if(!$order && !$customer)
		{
			$ticket->where('email', $this->request->email)->where('order_id', null)->where('customer_id', null);
		}

		$ticket = $ticket->first();

		return $ticket;
	}

	private function newTicket()
	{
		$order = Order::where('ref', $this->request->order_reference)->first();
		$ticket_department = TicketDepartment::findOrFail($this->request->department);
		$customer = $this->request->customer_session->customer ?? null;

		if(!$customer)
		{
			$customer = $this->request->shop->customers()->where('email', $this->request->email)->first() ?? null;
		}

		$ticket = new Ticket;
		$ticket->shop_id = $this->request->shop->id;
		$ticket->order_id = $order->id ?? null;
		$ticket->ticket_department_id = $ticket_department->id;
		$ticket->language_id = $this->request->language->id;
		$ticket->customer_id = $customer->id ?? null;
		$ticket->reference = strtoupper(str_random(8));
		$ticket->subject = $this->request->subject;

		if(!$customer)
		{
			$ticket->gender = $this->request->gender;
			$ticket->first_name = $this->request->first_name;
			$ticket->last_name = $this->request->last_name;
			$ticket->email = $this->request->email;
			$ticket->phonenumber = $this->request->phone;
		}

		$ticket->save();

		return $ticket;
	}

	public function findTicket()
	{
		$ticket = $this->request->shop->tickets()->with(['ticket_messages' => function($query){
			$query->with('user')->orderBy('created_at', 'asc');
		}])->where("reference", $this->request->ticket_reference)->first();

		if($ticket && $ticket->customer)
		{
			if($ticket->customer->email == $this->request->email)
			{
				return array("status" => true, "ticket" => $ticket);
			}
		}

		if($ticket && $ticket->email)
		{
			if($ticket->email == $this->request->email)
			{
				return array("status" => true, $ticket);
			}
		}

		return array("status" => false);
	}

	public function newTicketMessage()
	{
		$ticket = $this->findTicket();

		if($ticket["status"])
		{
			$ticket["ticket"]->addMessage($this->request, true);
			return array("status" => true);
		}
		
		return array("status" => false);
	}
}