<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TaxGroup extends Model
{
    public function tax_rules()
    {
    	return $this->hasMany(TaxRule::class);
    }

    public function setTaxGroup($request)
    {
    	if($request->default)
		{
			$default_tax_groups = TaxGroup::where('default', 1)->first();

            if($default_tax_groups)
            {
                $default_tax_groups->default = 0;
                $default_tax_groups->save();
            }			
		}		

		$this->declaration_section_code = $request->section_code;
    	$this->name = $request->name;
		$this->default = $request->default;

    	$this->save();
    }
}
