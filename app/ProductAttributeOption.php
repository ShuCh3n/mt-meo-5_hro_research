<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductAttributeOption extends Model
{
    public $multilanguage;

    public function __construct()
    {
        $this->multilanguage = new Multilanguage($this);
    }
    
    public function product_attribute()
    {
    	return $this->belongsTo(ProductAttribute::class);
    }

	public function infos()
    {
        return $this->hasMany(ProductAttributeOptionLanguage::class);
    }
}
