<?php

namespace App\Http\Controllers\Sync;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use  App\Http\Controllers\Classes\iBox;

class iBoxController extends Controller
{
	public function ibox()
    {
        $ibox = new iBox($this->request);

        if($this->request->type == 'product')
        {
            return $ibox->product();
        }
    }

}