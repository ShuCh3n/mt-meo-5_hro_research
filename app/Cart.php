<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Classes\ShopProductPrice;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cart extends Model
{
    use SoftDeletes;

    protected $hidden = ["deleted_at", "created_at", "updated_at"];

    protected $fillable = ['shipment_fee', 'additional_fee'];

    public function customer_session()
    {
        return $this->belongsTo(CustomerSession::class);
    }

    public function shop()
    {
        return $this->belongsTo(Shop::class);
    }

    public function cart_products()
    {
    	return $this->hasMany(CartProduct::class);
    }

    public function addProduct($cart, Product $product, $request, $price=0.00)
    {
        if($request['shipment_fee']){
            Cart::find($this->id)->update(['shipment_fee' => $request['shipment_fee']]);
        }

        if($request['additional_fee']){
            Cart::find($this->id)->update(['additional_fee' => $request['additional_fee']]);
        }
    	if($this->cart_products->contains('product_id', $product->id))
    	{
            $cart_product = $this->cart_products()->where('product_id', $product->id)->first();
            if($cart["qty"] <= 0)
            {
                $cart_product->delete();

                $this->load('cart_products');
                return $this;
            }

            if(isset($product->product_feature_type) && $product->product_feature_type->infos->first()->name != 'Service'){
                $cart_product->retail_price = (new ShopProductPrice($product, $request))->retail_price;
            }else{
                $cart_product->retail_price = $price;
            }

            if(!$cart["static"])
            {
                $cart["qty"] = $cart_product->qty + $cart["qty"];
            }

            $cart_product->qty = $cart["qty"];
            $cart_product->save();

            $this->load('cart_products');
            return $this;
    	}

        if($cart["qty"] > 0)
        {
            $cart_product = new CartProduct;
            $cart_product->cart_id = $this->id;
            $cart_product->product_id = $product->id;
            $cart_product->qty = $cart["qty"];
            if(isset($product->product_feature_type) && $product->product_feature_type->infos->first()->name != 'Service'){
                $cart_product->retail_price = (new ShopProductPrice($product, $request))->retail_price;
            }else{
                $cart_product->retail_price = $price;
            }
            $cart_product->save();
        }
        
        $this->load('cart_products');
        return $this;
    }

    public function totalprice($request)
    {
        $totalprice = 0;

        foreach($this->fresh()->cart_products as $cart_product)
        {
            $product = Product::find($cart_product->product_id);
            if($product->product_feature_type_id == 2){
                $totalprice += $cart_product->retail_price * $cart_product->qty;
            }else{
                $totalprice += (new ShopProductPrice($cart_product->product, $request->shop))->retail_price * $cart_product->qty;
            }
        }
        return $totalprice+$this->additional_fee+$this->shipment_fee;
    }

    public function countProducts()
    {
        return 1;
    }

    public function getCartTotalPrice()
    {
        $this->total_price = 0;

        foreach($this->cart_products as $cart_product)
        {
            $cart_product->product->prices = new ShopProductPrice($cart_product->product, $this->shop);
            $this->total_price += $cart_product->product->prices->retail_price * $cart_product->qty;
        }

        return $this;
    }

    public function hasCustomer()
    {
        if($this->customer_session->customer)
        {
            return true;
        }

        return false;
    }
}
