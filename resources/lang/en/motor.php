<?php
/*************************************************************************
 Generated via "php artisan localization:missing" at 2017/04/10 17:31:53 
*************************************************************************/

return array (
  //============================== New strings to translate ==============================//
  // Defined in file /var/www/cms/resources/views/dashboard/view_motor.blade.php
  'auto_fill' => 'auto_fill',
  // Defined in file /var/www/cms/resources/views/dashboard/view_motor.blade.php
  'product' => 'product',
  //==================================== Translations ====================================//
  'action' => 'action',
  'brand' => 'brand',
  'category' => 'category',
  'cc' => 'cc',
  'code' => 'code',
  'different_size' => 'different_size',
  'edit' => 'edit',
  'export_excel' => 'export_excel',
  'filter' => 'filter',
  'filters' => 'filters',
  'id' => 'id',
  'model' => 'model',
  'motor' => 'motor',
  'motors' => 'motors',
  'name' => 'name',
  'new_motor' => 'new_motor',
  'save' => 'save',
  'search' => 'search',
  'type' => 'type',
  'tyre_size' => 'tyre_size',
  'view' => 'view',
  'years' => 'years',
);