<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests;

class ShopController extends Controller
{
	public function getShop()
	{
		return array(
			"status" => true,
			"shop" => $this->request->shop->load(['tax_rule' => function($query){
				$query->active();
			}])
		);
	}
}