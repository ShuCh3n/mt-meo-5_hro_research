@extends('dashboard.layout.main')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>{{ trans('pages.inquiry_detail_header') }}</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('home') }}">{{ trans('menu.home') }}</a>
                </li>
                <li>
                    <a href="">{{ trans('menu.inquiries') }}</a>
                </li>
                <li class="active">
                    <strong>{{ trans('page.inquiry') }}</strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">
        <form method="post" class="form-horizontal">
            <div class="row">
                <div class="col-md-8">
                    <div class="ibox">

                        <div class="ibox-title">
                            <h5>New Tax</h5>
                        </div>

                        <div class="ibox-content">
                            <div class="mb20"></div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Status</label>
                                        <div class="col-md-5">
                                            <label><input type="radio" class="form-control i-checks" name="enable" value="1" checked> Enabled</label>
                                        </div>
                                        <div class="col-md-5">
                                            <label><input type="radio" class="form-control i-checks" name="enable" value="0"> Disabled</label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Name</label>
                                        <div class="col-md-10"><input type="text" class="form-control" name="name" placeholder="Name" value="{{ $tax_rule->name }}"></div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Fixed Price</label>
                                        <div class="col-md-10">
                                            <integer-input name="fixed_price" placeholder="Fixed Price" value="{{ $tax_rule->fixed_price }}"></integer-input>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Percentage</label>
                                        <div class="col-md-10">
                                            <percentage-input name="percentage" placeholder="Rate in percentage" value="{{ $tax_rule->percentage }}"></percentage-input>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="ibox">
                        <div class="ibox-title">
                            <h5>Location</h5>
                        </div>

                        <div class="ibox-content">
                            <div class="mb20"></div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Country</label>
                                        <div class="col-md-10">
                                            <country-select class="form-control" multiple="multiple" name="countries[]">
                                                @foreach($countries as $country)
                                                    <option value="{{ $country->id }}" icon="{{ $country->icon }}" {{ ($tax_rule->countries->contains('id', $country->id))? "selected" : "" }}>{{ $country->name }}</option>
                                                @endforeach
                                            </country-select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">State</label>
                                        <div class="col-md-10">
                                            <select2 class="form-control" multiple="multiple" name="states[]">
                                                @foreach($tax_rule->countries as $country)
                                                    @foreach($country->states as $state)
                                                        <option value="{{ $state->id }}" {{ ($tax_rule->states->contains('id', $state->id))? "selected" : "" }}>{{ $state->name }}</option>
                                                    @endforeach
                                                @endforeach
                                            </select2>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="ibox">
                        <div class="ibox-title">
                            <h5>Action</h5>
                        </div>

                        <div class="ibox-content">
                            {{ csrf_field() }}
                            <button class="btn btn-block btn-outline btn-success" type="submit"><i class="fa fa-floppy-o"></i> Save</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>

        <div class="mb30"></div>
    </div>
@stop