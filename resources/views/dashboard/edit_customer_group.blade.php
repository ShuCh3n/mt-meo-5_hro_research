@extends('dashboard.layout.main')

@section('content')
	<div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>{{ trans('pages.inquiry_detail_header') }}</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('home') }}">{{ trans('menu.home') }}</a>
                </li>
                <li>
                    <a href="">{{ trans('menu.inquiries') }}</a>
                </li>
                <li class="active">
                    <strong>{{ trans('page.inquiry') }}</strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">

        <form id="profileForm" method="post" class="form-horizontal">
            <div class="row">
                <div class="col-md-8">
                    <div class="ibox">

                        <div class="ibox-title">
                            <h5>New Group</h5>
                        </div>

                        <div class="ibox-content">
                            <div class="mb20"></div>

                            <div class="row">
                                <div class="col-md-12">
                                	<div class="form-group">
                                        <label class="col-md-2 control-label">Name</label>
                                        <div class="col-md-10"><input type="text" class="form-control" name="name" value="{{ $customer_group->name }}" placeholder="Group Name"></div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Mandant</label>
                                        <div class="col-md-10"><input type="text" class="form-control" name="mandant" value="{{ $customer_group->mandant }}" placeholder="Mandant"></div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Discount</label>
                                        <div class="col-md-10">
                                            <percentage-input name="discount" value="{{ $customer_group->discount }}"></percentage-input>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Tax</label>
                                        <div class="col-md-5">
                                            <label><input type="radio" class="form-control i-checks" name="tax" value="1" {{ ($customer_group->tax == 1)? "checked" : "" }}> Included</label>
                                        </div>
                                        <div class="col-md-5">
                                            <label><input type="radio" class="form-control i-checks" name="tax" value="0" {{ ($customer_group->tax == 0)? "checked" : "" }}> Excluded</label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Show prices</label>
                                        <div class="col-md-5">
                                            <label><input type="radio" class="form-control i-checks" name="show_prices" value="1" {{ ($customer_group->show_prices == 1)? "checked" : "" }}> Yes</label>
                                        </div>
                                        <div class="col-md-5">
                                            <label><input type="radio" class="form-control i-checks" name="show_prices" value="0" {{ ($customer_group->show_prices == 0)? "checked" : "" }}> No</label>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="ibox">
                        <div class="ibox-title">
                            <h5>Action</h5>
                        </div>

                        <div class="ibox-content">
                            {{ csrf_field() }}
                            <button class="btn btn-block btn-outline btn-success" type="submit"><i class="fa fa-floppy-o"></i> Save</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>

        <div class="mb30"></div>
    </div>
@stop