<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Paynl\Config;
use Paynl\Paymentmethods;
use Paynl\Transaction;

class Paynl extends Model
{

	public function config()
	{
		Config::setApiToken($this->api_token);
		Config::setServiceId($this->service_id);

		return $this;
	}

	public function payments()
	{
		return Paymentmethods::getList();
	}

    public function pay($transaction)
    {
    	$transaction['testmode'] = $this->test_mode;
    	
    	$result = Transaction::start($transaction);
    	return $result;
    }

    public function confirm($transaction_id)
    {
    	$result = Transaction::get($transaction_id);
    	return $result->isPaid();
    }
}