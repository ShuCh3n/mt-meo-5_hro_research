@extends('dashboard.layout.main')

@section('content')
	<div class="wrapper wrapper-content animated fadeInRight ecommerce">
            <div class="ibox-content m-b-sm border-bottom">
                <div class="row">
                    <div class="col-xs-3 col-xs-push-9">
                        <a class="btn btn-primary pull-right" href="{{ route('new_category') }}"><i class="fa fa-plus-circle" aria-hidden="true"></i> New Category</a>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox">
                        <div class="ibox-content">

                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>Category</th>
                                        <th>Products</th>
                                        <th class="text-right" >Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                	@foreach($category->subcategories as $subcategory)
										<tr>
                                            <td>
                                               <a href="{{ route('shop_category', [$subcategory->shop->id, $subcategory->id]) }}">{{ $subcategory->info->name }}</a>
                                            </td>
                                            <td>
                                              {{ $subcategory->products->count() }}
                                            </td>
                                            <td class="text-right">
                                                <a href="{{ route('edit_category', $subcategory->id) }}" class="btn-white btn btn-xs">Edit</a>
                                            </td>
                                        </tr>
									@endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@stop