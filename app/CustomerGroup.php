<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerGroup extends Model
{
	protected $fillable = ['name', 'mandant', 'discount', 'tax', 'show_prices'];

	public function customers()
	{
		return $this->hasMany(Customer::class);
	}
}
