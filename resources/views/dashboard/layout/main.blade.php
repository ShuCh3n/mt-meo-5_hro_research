<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta id="_token" value="{{ csrf_token() }}" />

    <title>@lang('main.CMS')</title>

    <link href="/css/zeus.css" rel="stylesheet">

	<link href="/bower_components/bootstrap-select/dist/css/bootstrap-select.min.css" rel="stylesheet">
    <link href="/bower_components/angular-upload/src/directives/btnUpload.min.css" rel="stylesheet">

    @yield('stylesheet')
</head>

<body>
    <div id="wrapper">
        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav metismenu" id="side-menu">
                    <li class="nav-header">
                        <div class="dropdown profile-element"> <span>
                            <img alt="image" class="img-circle" src="/img/profile_small.jpg" />
                             </span>
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold">{{ auth()->user()->firstname }} {{ auth()->user()->lastname }}</strong>
                            </span> <span class="text-muted text-xs block">@lang('main.Settings') <b class="caret"></b></span> </span> </a>
                            <ul class="dropdown-menu animated fadeInRight m-t-xs">
                                <li><a href="profile.html">@lang('main.Settings')</a></li>
                                <li class="divider"></li>
                                <li><a href="{{ route('logout') }}">Logout</a></li>
                            </ul>
                        </div>
                        <div class="logo-element">
                            ZEUS
                        </div>
                    </li>
                    <li {{ (Route::currentRouteName() == 'home')? 'class=active' : '' }}>
                        <a href="{{ route('home') }}"><i class="fa fa-home"></i> <span class="nav-label">Dashboard</span>  </a>
                    </li>
					<li {{ (in_array(Route::currentRouteName(), ['customers', 'edit_customer', 'customer_addresses', 'new_customer_address', 'edit_customer_address', 'new_customer', 'customer_groups', 'view_customer', 'new_customer_group', 'carts', 'view_cart']))? 'class=active' : '' }}>
						<a href="#"><i class="fa fa-users"></i> <span class="nav-label">@lang('main.customers')</span><span class="fa arrow"></span></a>
						<ul class="nav nav-second-level collapse">
							<li {{ (in_array(Route::currentRouteName(), ['customers', 'edit_customer', 'new_customer', 'view_customer']))? 'class=active' : '' }}><a href="{{ route('customers') }}">@lang('main.customers')</a></li>
							<li {{ (in_array(Route::currentRouteName(), ['customer_addresses', 'edit_customer_address', 'new_customer_address',]))? 'class=active' : '' }}><a href="{{ route('customer_addresses') }}">Addresses</a></li>
                            <li {{ (in_array(Route::currentRouteName(), ['carts', 'view_cart']))? 'class=active' : '' }}><a href="{{ route('carts') }}">@lang('main.carts')</a></li>
							<li {{ (in_array(Route::currentRouteName(), ['customer_groups', 'new_customer_group', 'edit_customer_group']))? 'class=active' : '' }}><a href="{{ route('customer_groups') }}">@lang('main.groups')</a></li>
                        </ul>
					</li>

                    <li {{ (in_array(Route::currentRouteName(), ['orders', 'view_order', 'new_order', 'reward_programs', 'new_reward_program', 'edit_reward_program', 'reward_program_orders', 'generated_orders', 'view_program_order']))? 'class=active' : '' }}>
                        <a href="#"><i class="fa fa-credit-card-alt"></i> <span class="nav-label">Orders</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level collapse">
                            <li {{ (in_array(Route::currentRouteName(), ['orders', 'view_order', 'new_order']))? 'class=active' : '' }}><a href="{{ route('orders') }}">@lang('main.orders')</a></li>
                            <li {{ (in_array(Route::currentRouteName(), ['reward_programs', 'new_reward_program', 'edit_reward_program', 'reward_program_orders', 'view_program_order']))? 'class=active' : '' }}><a href="{{ route('reward_programs') }}">Reward Program</a></li>
                            <li {{ (in_array(Route::currentRouteName(), ['merchandise_returns', 'view_merchandise_return']))? 'class=active' : '' }}><a href="{{ route('customer_addresses') }}">Merchandise Returns</a></li>
                            <li {{ (in_array(Route::currentRouteName(), ['generated_orders']))? 'class=active' : '' }}><a href="{{ route('generated_orders') }}">@lang('main.generated_orders')</a></li>
                        </ul>
                    </li>
                    <li {{ (in_array(Route::currentRouteName(), ['messages', 'view_messages']))? 'class=active' : '' }}>
                        <a href="{{ route('messages') }}"><i class="fa fa-comments-o" aria-hidden="true"></i> <span class="nav-label">@lang('main.messages')</span>  </a>
                    </li>
                    <li {{ (in_array(Route::currentRouteName(), ['products', 'shop_products', 'new_product', 'edit_product', 'brands', 'shop_brands', 'new_brand', 'product_feature_type', 'new_product_feature_type', 'product_features', 'edit_product_feature_type', 'new_product_feature', 'edit_product_feature', 'product_attributes', 'new_product_attribute', 'product_attribute_options', 'new_product_attribute_option', 'edit_product_attribute', 'edit_product_attribute_option', 'product_groups', 'new_product_group', 'edit_product_group', 'product_group', 'product_bulk_edit', 'view_product', 'transfer_feature_option', 'product_sheet_upload']))? 'class=active' : '' }}>
                        <a href="#"><i class="fa fa-cubes" aria-hidden="true"></i> <span class="nav-label">@lang('main.products')</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level collapse">
                            <li {{ (in_array(Route::currentRouteName(), ['products', 'shop_products', 'new_product', 'edit_product', 'product_bulk_edit', 'view_product', 'product_sheet_upload']))? 'class=active' : '' }}><a href="{{ route('products') }}">@lang('main.products')</a></li>
                            <li {{ (in_array(Route::currentRouteName(), ['product_groups', 'new_product_group', 'edit_product_group', 'product_group']))? 'class=active' : '' }}><a href="{{ route('product_groups') }}">@lang('main.groups')</a></li>
                            <li {{ (in_array(Route::currentRouteName(), ['product_attributes', 'new_product_attribute', 'product_attribute_options', 'new_product_attribute_option', 'edit_product_attribute', 'edit_product_attribute_option']))? 'class=active' : '' }}><a href="{{ route('product_attributes') }}">@lang('main.attributes')</a></li>
                            <li {{ (in_array(Route::currentRouteName(), ['product_feature_type', 'new_product_feature_type', 'product_features', 'edit_product_feature_type', 'new_product_feature', 'edit_product_feature', 'transfer_feature_option']))? 'class=active' : '' }}><a href="{{ route('product_feature_type') }}">@lang('main.features')</a></li>
                            <li {{ (in_array(Route::currentRouteName(), ['brands', 'shop_brands', 'new_brand']))? 'class=active' : ''? 'class=active' : '' }}><a href="{{ route('brands') }}">@lang('main.brands')</a></li>
                        </ul>
                    </li>
                    <li {{ (in_array(Route::currentRouteName(), ['cart_rules', 'new_cart_rule', 'catalog_price_rules', 'new_catalog_price_rule', 'bulk_price_rules', 'wholesale_price_rules', 'new_catalog_price_rule', 'edit_catalog_price_rule']))? 'class=active' : '' }}>
                        <a href="#"><i class="fa fa-usd" aria-hidden="true"></i> <span class="nav-label">@lang('main.price_rules')</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level collapse">
                            <li {{ (in_array(Route::currentRouteName(), ['cart_rules', 'new_cart_rule']))? 'class=active' : ''? 'class=active' : '' }}><a href="{{ route('cart_rules') }}">@lang('main.cartrules')</a></li>
                            <li {{ (in_array(Route::currentRouteName(), ['catalog_price_rules', 'new_catalog_price_rule', 'edit_catalog_price_rule']))? 'class=active' : ''? 'class=active' : '' }}><a href="{{ route('catalog_price_rules') }}">@lang('main.catalog_price_rules')</a></li>
                            <li {{ (in_array(Route::currentRouteName(), ['bulk_price_rules']))? 'class=active' : ''? 'class=active' : '' }}><a href="{{ route('bulk_price_rules') }}">Bulk Price Rules</a></li>
                            <li {{ (in_array(Route::currentRouteName(), ['wholesale_price_rules']))? 'class=active' : ''? 'class=active' : '' }}><a href="{{ route('wholesale_price_rules') }}">Wholsale Price Rules</a></li>
                        </ul>
                    </li>

                    <li {{ (in_array(Route::currentRouteName(), ['categories', 'shop_categories', 'shop_category', 'new_category']))? 'class=active' : ''? 'class=active' : '' }}>
                        <a href="{{ route('categories') }}"><i class="fa fa-sitemap" aria-hidden="true"></i> <span class="nav-label">@lang('main.categories')</span>  </a>
                    </li>

                    <li {{ (in_array(Route::currentRouteName(), ['pages', 'blogs', 'new_page', 'edit_blog', 'new_blog_article', 'edit_blog_article', 'emails', 'shop_emails', 'new_email', 'edit_email']))? 'class=active' : '' }}>
                        <a href="#"><i class="fa fa-file-text-o" aria-hidden="true"></i> <span class="nav-label">CMS</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level collapse">
                            <li {{ (in_array(Route::currentRouteName(), ['pages', 'new_page', 'edit_blog']))? 'class=active' : '' }}><a href="{{ route('pages') }}">Pages</a></li>
                        </ul>
                        <ul class="nav nav-second-level collapse">
                            <li {{ (in_array(Route::currentRouteName(), ['emails', 'shop_emails', 'new_email', 'edit_email']))? 'class=active' : '' }}><a href="{{ route('emails') }}">Emails</a></li>
                        </ul>
						<ul class="nav nav-second-level collapse">
                            <li {{ (in_array(Route::currentRouteName(), ['blogs', 'new_blog_article', 'edit_blog_article']))? 'class=active' : '' }}><a href="{{ route('blogs') }}">Blogs</a></li>
                        </ul>
                    </li>
                    <li {{ (in_array(Route::currentRouteName(), ['taxes', 'new_tax_group', 'tax_groups', 'new_tax', 'view_tax_group', 'edit_tax', 'shippings', 'edit_tax_group']))? 'class=active' : '' }}>
                        <a href="#"><i class="fa fa-globe"></i> <span class="nav-label">@lang('main.locale')</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level collapse">
                            <li {{ (in_array(Route::currentRouteName(), ['taxes', 'view_merchandise_return', 'new_tax_group', 'tax_groups', 'new_tax', 'view_tax_group', 'edit_tax', 'edit_tax_group']))? 'class=active' : '' }}><a href="{{ route('tax_groups') }}">Tax Groups</a></li>
                            <li {{ (Route::currentRouteName() == 'shippings')? 'class=active' : '' }}><a href="{{ route('shippings') }}">Shipping</a></li>
                        </ul>
                    </li>
                    <li {{ (in_array(Route::currentRouteName(), ['shops', 'edit_shop', 'new_shop', 'warehouses', 'edit_warehouse', 'new_warehouse']))? 'class=active' : '' }}>
                        <a href="#"><i class="fa fa-shopping-bag" aria-hidden="true"></i> <span class="nav-label">Shops</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level collapse">
                            <li {{ (in_array(Route::currentRouteName(), ['shops', 'edit_shop', 'new_shop']))? 'class=active' : '' }}><a href="{{ route('shops') }}">Shops</a></li>
                            <li {{ (in_array(Route::currentRouteName(), ['warehouses', 'edit_warehouse', 'new_warehouse']))? 'class=active' : '' }}><a href="{{ route('warehouses') }}">Warehouses</a></li>
                        </ul>
                    </li>

                    <li {{ (in_array(Route::currentRouteName(), ['motors', 'view_motor', 'new_motor', 'edit_motor']))? 'class=active' : '' }}>
                        <a href="#"><i class="fa fa-car" aria-hidden="true"></i> <span class="nav-label">@lang('main.vehicles')</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level collapse">
                            <li {{ (in_array(Route::currentRouteName(), ['motors', 'view_motor', 'new_motor', 'edit_motor']))? 'class=active' : '' }}><a href="{{ route('motors') }}">Motors</a></li>
                            <li {{ (in_array(Route::currentRouteName(), ['departments']))? 'class=active' : '' }}><a href="{{ route('departments') }}">Cars</a></li>
                        </ul>
                    </li>

					<li {{ (in_array(Route::currentRouteName(), ['images']))? 'class=active' : ''? 'class=active' : '' }}>
                        <a href="{{ route('images') }}"><i class="fa fa-file-image-o" aria-hidden="true"></i> <span class="nav-label">@lang('main.images')</span>  </a>
                    </li>

                    <li {{ (in_array(Route::currentRouteName(), ['analist', 'platforms', 'platform_products', 'analyse_file', 'ibox_statistics', 'platform_setting', 'account_view', 'analyze']))? 'class=active' : '' }}>
                        <a href="#"><i class="fa fa-lightbulb-o" aria-hidden="true"></i> <span class="nav-label">iBox</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level collapse">
                            <li {{ (in_array(Route::currentRouteName(), ['analist', 'analyse_file', 'analyze']))? 'class=active' : '' }}><a href="{{ route('analist') }}">Analist</a></li>
                            <li {{ (in_array(Route::currentRouteName(), ['account_view']))? 'class=active' : '' }}><a href="{{ route('account_view') }}">Account View</a></li>
                            <li {{ (in_array(Route::currentRouteName(), ['platforms', 'platform_products', 'platform_setting']))? 'class=active' : '' }}><a href="{{ route('platforms') }}">Platforms</a></li>
                            <li {{ (in_array(Route::currentRouteName(), ['ibox_statistics']))? 'class=active' : '' }}><a href="{{ route('ibox_statistics') }}">@lang('main.statistics')</a></li>
                        </ul>
                    </li>

                    <li {{ (in_array(Route::currentRouteName(), ['employees', 'edit_employee', 'departments']))? 'class=active' : '' }}>
                        <a href="#"><i class="fa fa-black-tie" aria-hidden="true"></i> <span class="nav-label">Management</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level collapse">
                            <li {{ (in_array(Route::currentRouteName(), ['employees', 'edit_employee']))? 'class=active' : '' }}><a href="{{ route('employees') }}">Employees</a></li>
                            <li {{ (in_array(Route::currentRouteName(), ['departments']))? 'class=active' : '' }}><a href="{{ route('departments') }}">Departments</a></li>
                        </ul>
                    </li>

                    <li {{ (in_array(Route::currentRouteName(), ['statistics']))? 'class=active' : '' }}>
                        <a href="#"><i class="fa fa-area-chart" aria-hidden="true"></i> <span class="nav-label">Statistics</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level collapse">
                            <li {{ (in_array(Route::currentRouteName(), ['customers', 'customers']))? 'class=active' : '' }}><a href="{{ route('customers') }}">@lang('main.customers')</a></li>
                        </ul>
                    </li>
                </ul>

            </div>
        </nav>

        <div id="page-wrapper" class="gray-bg dashbard-1">
            <div class="row border-bottom">
                <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                    <div class="navbar-header">
                        <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
                        <form role="search" class="navbar-form-custom" action="{{ route('search') }}">
                            <div class="form-group">
                                <input type="text" placeholder="@lang('main.search')" class="form-control" name="q" id="top-search">
                            </div>
                        </form>
                    </div>
                    <ul class="nav navbar-top-links navbar-right">
                        <li>
                            <a href="{{route('lang', 'nl')}}"><img src="/images/flags/16/Netherlands.png" alt="NL"></a>
                        </li>
                        <li>
                            <a href="{{route('lang', 'en')}}"><img src="/images/flags/16/United-Kingdom.png" alt="UK"></a>
                        </li>
                        <li>
                            <span class="m-r-sm text-muted welcome-message">@lang('main.welcome')</span>
                        </li>
                        <li class="dropdown">
                            <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                                <i class="fa fa-envelope"></i>  <span class="label label-warning">16</span>
                            </a>
                            <ul class="dropdown-menu dropdown-messages">
                                <li>
                                    <div class="dropdown-messages-box">
                                        <a href="profile.html" class="pull-left">
                                            <img alt="image" class="img-circle" src="/img/profile_small.jpg">
                                        </a>
                                        <div class="media-body">
                                            <small class="pull-right">46h ago</small>
                                            <strong>Mike Loreipsum</strong> started following <strong>Monica Smith</strong>. <br>
                                            <small class="text-muted">3 days ago at 7:58 pm - 10.06.2014</small>
                                        </div>
                                    </div>
                                </li>
                                <li class="divider"></li>
                                <li>
                                    <div class="dropdown-messages-box">
                                        <a href="profile.html" class="pull-left">
                                            <img alt="image" class="img-circle" src="/img/profile_small.jpg">
                                        </a>
                                        <div class="media-body ">
                                            <small class="pull-right text-navy">5h ago</small>
                                            <strong>Chris Johnatan Overtunk</strong> started following <strong>Monica Smith</strong>. <br>
                                            <small class="text-muted">Yesterday 1:21 pm - 11.06.2014</small>
                                        </div>
                                    </div>
                                </li>
                                <li class="divider"></li>
                                <li>
                                    <div class="dropdown-messages-box">
                                        <a href="profile.html" class="pull-left">
                                            <img alt="image" class="img-circle" src="/img/profile_small.jpg">
                                        </a>
                                        <div class="media-body ">
                                            <small class="pull-right">23h ago</small>
                                            <strong>Monica Smith</strong> love <strong>Kim Smith</strong>. <br>
                                            <small class="text-muted">2 days ago at 2:30 am - 11.06.2014</small>
                                        </div>
                                    </div>
                                </li>
                                <li class="divider"></li>
                                <li>
                                    <div class="text-center link-block">
                                        <a href="mailbox.html">
                                            <i class="fa fa-envelope"></i> <strong>Read All Messages</strong>
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                                <i class="fa fa-bell"></i>  <span class="label label-primary">8</span>
                            </a>
                            <ul class="dropdown-menu dropdown-alerts">
                                <li>
                                    <a href="mailbox.html">
                                        <div>
                                            <i class="fa fa-envelope fa-fw"></i> You have 16 messages
                                            <span class="pull-right text-muted small">4 minutes ago</span>
                                        </div>
                                    </a>
                                </li>
                                <li class="divider"></li>
                                <li>
                                    <a href="profile.html">
                                        <div>
                                            <i class="fa fa-twitter fa-fw"></i> 3 New Followers
                                            <span class="pull-right text-muted small">12 minutes ago</span>
                                        </div>
                                    </a>
                                </li>
                                <li class="divider"></li>
                                <li>
                                    <a href="grid_options.html">
                                        <div>
                                            <i class="fa fa-upload fa-fw"></i> Server Rebooted
                                            <span class="pull-right text-muted small">4 minutes ago</span>
                                        </div>
                                    </a>
                                </li>
                                <li class="divider"></li>
                                <li>
                                    <div class="text-center link-block">
                                        <a href="notifications.html">
                                            <strong>See All Alerts</strong>
                                            <i class="fa fa-angle-right"></i>
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="{{ route('logout') }}">
                                <i class="fa fa-sign-out"></i> @lang('main.logout')
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>

            @yield('vue_templates')

            @if(isset($noVue))
                <div id="app"></div>
                
                @yield('content')
            @else
                <div id="app">
                    @yield('content')
                </div>
            @endif

            <div class="footer">
                <div>
                    <strong>Copyright</strong> Zeus &copy; {{ date('Y') }}
                </div>
            </div>
        </div>
    </div>

    @yield('predefined-javascript')

    <script src="/js/zeus.js"></script>

    @yield('javascript')

</body>
</html>
