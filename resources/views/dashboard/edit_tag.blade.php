@extends('dashboard.layout.main')

@section('content')
    <form method="post" action="{{route('update_blog_tag', $tag->id)}}">
        <div class="ibox">
            <div class="ibox-content">
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th>
                            Name
                        </th>
                        <th class="text-right">
                            Input
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($languages as $language)
                        <tr>
                            <td>
                                {{$language->name}}
                            </td>
                            <td>
                                <input type="text" class="form-control" name="tag[{{$language->id}}]" value="{{$tagnames->where('languages_id', $language->id)->first()->name?? ''}}"></input>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            {{ csrf_field() }}
            <button class="btn btn-primary" type="submit"><i class="fa fa-floppy-o"></i> Save</button>
        </div>
    </form>
@stop