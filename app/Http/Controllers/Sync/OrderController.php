<?php

namespace App\Http\Controllers\Sync;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Classes\Orders\Order as OrderClass;
use App\Order;

class OrderController extends Controller
{
	// public function newOrders()
 //    {
 //        $orderClass = new OrderClass();
        
 //        if($orderClass->createLast()){
 //            if (!($this->request->input('nocheck') !== null)) {
 //                $orderClass->checkOrder();
 //            }
 //            return $orderClass->getOrderFormatted();
 //        }else{
 //            return ["error" => 'no orders'];
 //        }
 //    }

    // public function getOrder()
    // {
    //     // $order = Order::with(['lines.product', 'shop'])->findOrFail($this->request->orderid);
    //     // $order = SyncFactory::create('order')->setOrder($order)->setShopRule($order->shop)->getFormatedOrder();
    //     // return $order->order;
    //     $orderClass = new OrderClass();
    //     $orderClass->createFromId($this->request->orderid);
    //     if (!($this->request->input('nocheck') !== null)) {
    //         $orderClass->checkOrder();
    //     }

    //     return $orderClass->getOrderFormatted();
    // }

	public function newestOrder()
	{
        $order = Order::with(['shop', 'country', 'customer'])->first();

		return array(
            "id"                    => $order->id,
            "ref"                   => $order->reference,
            "placed_on"             => $order->created_at->format('Y-m-d H:i:s'),
            "ip"                    => $order->ip,
            "customer_id"           => $order->customer_id,
            "address"               => $order->customer->billing_street,
            "housenumber"           => $order->customer->billing_housenumber,
            "city"                  => $order->customer->billing_city,
            "postal_code"           => $order->customer->billing_zipcode,
            "mand"                  => "",
            "customer_code"         => "",
            "deliver_company"       => $order->company,
            "deliver_street"        => $order->street,
            "deliver_housenumber"   => $order->housenumber,
            "deliver_zipcode"       => $order->zipcode,
            "deliver_city"          => $order->city,
            "deliver_country_id"    => $order->country_id,
            "dropship"              => $order->dropship,
            "internal_comment"      => $order->internal_comment,
            "customer_comment"      => $order->customer_comment,
            "customer_reference"    => "",
            "country_id"            => $order->country_id,
            "shipment_method"       => "",
            "shipment_node"         => "",
            "shipment_costs"        => $order->shipping_price,
            "total_price"           => $order->total_price,
            "bem"                   => "",
            "profit"                => "",
            "profit_payed"          => "",
            "order_status_id"       => "",
            "payment_id"            => $order->payment_id,
            "shop_id"               => $order->shop_id,
            "checked"               => "",
            "email"                 => $order->customer->email,
            "phone"                 => $order->customer->mobile_phone ?? $order->customer->home_phone,
            "additional_costs"      => "",
            "country_code"          => $order->country->iso_code_2,
            "country_name"          => $order->country->name,
            "company_name"          => $order->company,
            "shop_name"             => $order->shop->name,
            "AV_admin"              => "",
            "productCount"          => "",
            "service_products"      => ""
        );
	}
}