<?php

namespace App\Classes;

use App\Brand;
use App\Product;
use App\Shop;
use DB;
use App\Classes\ShopProductPrice;

class ProductFilters {
	private $products;
	private $language_id;

	public function __construct($products, $language_id, $shop_id = null)
	{
		$this->language_id = $language_id;
		$this->products = $products;
		if(get_class($products->getModel()) == 'App\ShopProduct') {
            $this->products = $products->getRelation('product');
        }
        if($shop_id){
            $this->products->shopActive($shop_id);
        }
        $this->products->ProductFilter($shop_id);
		$this->shop_id = $shop_id;
		$this->getFilters();
	}

	public function getFilters()
	{
        $this->brands = $this->getBrand();
		$this->getFeatures();
		$this->getMinMaxPrice();
	}

    public function getBrand()
    {
        $products = clone $this->products;
        $products = Shop::findOrFail($this->shop_id)->products()->ProductFilter($this->shop_id);
        $products->distinct();
        $products->join('brands', 'brands.id', '=', 'brand_id');
        return $products->get(['brands.id', 'brands.name', 'brands.logo'])->toArray();

    }

	public function getFeatures()
	{
        $products = clone $this->products;

        $products->groupBy(['product_feature_languages.id', 'product_feature_option_languages.id']);
        $products->join('product_product_feature_option', 'products.id', '=', 'product_product_feature_option.product_id');

        $products->join('product_feature_options', 'product_feature_options.id', '=', 'product_product_feature_option.product_feature_option_id');

        $products->join('product_feature_option_languages', 'product_feature_options.id', '=', 'product_feature_option_languages.product_feature_option_id');

        $products->join('product_feature_languages', 'product_feature_options.product_feature_id', '=', 'product_feature_languages.product_feature_id');
       	$products->select(['product_feature_languages.id', 'product_feature_option_languages.id', 'product_feature_languages.name as featurename', 'product_feature_option_languages.id', 'product_feature_option_languages.product_feature_option_id', 'product_feature_option_languages.language_id', 'product_feature_option_languages.name']); 
        $features = $products->get()->groupBy('featurename');
        foreach ($features as $key=>$feature){
            $featurenode = str_slug($key, '_');
            $this->$featurenode = $feature;
        }
	}

    public function getMinMaxPrice()
    {
        $this->price_range = array("min" => null, "max" => null);

        $productsMin = clone $this->products;
        if($this->shop_id){

            $productsMin->selectRaw('products.id,product_shop_prices.retail_price as price');

            $productsMin->leftJoin('product_shop_prices', 'products.id', '=', 'product_shop_prices.product_id');
            $productsMin->where('product_shop_prices.shop_id', '=', $this->shop_id);
            $productsMin->where('product_shop_prices.deleted_at', '=', null);
            $productsMin->where('product_shop_prices.retail_price', '>', 0);
            $productsMax = clone $productsMin;
            $productsMin->orderBy('product_shop_prices.retail_price', 'asc');
            $productsMax->orderBy('product_shop_prices.retail_price', 'desc');
            if($productsMin->first()) {
                $shop = Shop::findOrFail($this->shop_id);
                $this->price_range["min"] = (new ShopProductPrice($productsMin->first(), $shop))->retail_price;
                $this->price_range["max"] = (new ShopProductPrice($productsMax->first(), $shop))->retail_price;
            }
        }else{
            $productsMin->selectRaw('GREATEST(product_prices.retail_price, product_stocks.price) as price');
            $productsMin->leftJoin('product_prices', 'products.id', '=', 'product_prices.product_id');
            $productsMin->leftJoin('product_stocks', 'product_stocks.product_id', '=', 'products.id');
            $productsMin->where('product_stocks.deleted_at', '=', null);
            $productsMin->where('product_prices.deleted_at', '=', null);
            $productsMin->where('product_prices.retail_price', '>', 0);
            $productsMin->where('product_stocks.price', '>', 0);
            $productsMax = clone $productsMin;
            $productsMin->orderBy('price', 'asc');
            $productsMax->orderBy('price', 'desc');

            if($productsMin->first()) {
                $this->price_range["min"] = $productsMin->first()->price;
                $this->price_range["max"] = $productsMax->first()->price;
            }
        }


    }
}
