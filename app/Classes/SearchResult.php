<?php
namespace App\Classes;

use Oefenweb\DamerauLevenshtein\DamerauLevenshtein;

class SearchResult {
	public $category;
	public $title;
	public $link;
	public $points;
	public $description;
	
	public function __construct($search_query)
	{
		$this->search_query = $search_query;
		$this->points = 0;
	}

	public function getSimilarity($texts)
    {
    	if(is_array($texts))
    	{
    		foreach($texts as $text)
        	{
        		$this->getSimilarity($text);
        	}

        	return $this;
    	}

		$this->points += (new DamerauLevenshtein($this->search_query, $texts))->getSimilarity();

        return $this;
    }
}