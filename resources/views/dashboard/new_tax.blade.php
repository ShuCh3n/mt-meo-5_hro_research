@extends('dashboard.layout.main')

@section('stylesheet')
    <link href="/css/plugins/iCheck/custom.css" rel="stylesheet">

    <link href="/css/plugins/summernote/summernote.css" rel="stylesheet">
    <link href="/css/plugins/summernote/summernote-bs3.css" rel="stylesheet">
    <link href="/css/plugins/touchspin/jquery.bootstrap-touchspin.min.css" rel="stylesheet">
@stop

@section('javascript')
    <!-- iCheck -->
    <script src="/js/plugins/iCheck/icheck.min.js"></script>

    <!-- SUMMERNOTE -->
    <script src="/js/plugins/summernote/summernote.min.js"></script>

    <!-- TouchSpin -->
    <script src="/js/plugins/touchspin/jquery.bootstrap-touchspin.min.js"></script>

    <script>
        $(function(){
            $('.summernote').summernote();

            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });

            $(".price-input").TouchSpin({
                max: 1000000000,
                min: 0,
                step: 1,
                decimals: 2,
                boostat: 5,
                maxboostedstep: 10,
                prefix: '€',
                buttondown_class: 'btn btn-white',
                buttonup_class: 'btn btn-white'
            });

            $(".percentage-input").TouchSpin({
                max: 100,
                min: 0,
                step: 1,
                decimals: 2,
                boostat: 5,
                maxboostedstep: 10,
                postfix: '%',
                buttondown_class: 'btn btn-white',
                buttonup_class: 'btn btn-white'
            });

        });
    </script>
@stop

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>{{ trans('pages.inquiry_detail_header') }}</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('home') }}">{{ trans('menu.home') }}</a>
                </li>
                <li>
                    <a href="">{{ trans('menu.inquiries') }}</a>
                </li>
                <li class="active">
                    <strong>{{ trans('page.inquiry') }}</strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">
        {!! session()->has('success')? '<div class="alert alert-success alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>' . session('success') . '</div>' : '' !!}

        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <form id="profileForm" method="post" class="form-horizontal">
            <div class="row">
                <div class="col-md-8">
                    <div class="ibox">

                        <div class="ibox-title">
                            <h5>New Tax</h5>
                        </div>

                        <div class="ibox-content">
                            <div class="mb20"></div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Status</label>
                                        <div class="col-md-5">
                                            <label><input type="radio" class="form-control i-checks" name="enable" value="1" checked> Enabled</label>
                                        </div>
                                        <div class="col-md-5">
                                            <label><input type="radio" class="form-control i-checks" name="enable" value="0"> Disabled</label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Name</label>
                                        <div class="col-md-10"><input type="text" class="form-control" name="name" placeholder="Name"></div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Fixed Price</label>
                                        <div class="col-md-10">
                                            <input type="text" class="price-input" name="fixed_price" placeholder="Fixed Price">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Rate</label>
                                        <div class="col-md-10">
                                            <input type="text" class="percentage-input" name="percentage" placeholder="Rate in percentage">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="ibox">
                        <div class="ibox-title">
                            <h5>Location</h5>
                        </div>

                        <div class="ibox-content">
                            <div class="mb20"></div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Country</label>
                                        <div class="col-md-10">
                                            <select class="countries-select form-control" multiple="multiple" name="countries[]">
                                                @foreach($countries as $country)
                                                    <option value="{{ $country->id }}" icon="{{ $country->icon }}">{{ $country->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">State</label>
                                        <div class="col-md-10">
                                            <select class="select2 form-control" multiple="multiple" name="states[]">
                                                <option value="1">Zuid-Holland</option>
                                                <option value="2">Noord-Holland</option>
                                                <option value="3">Overijssel</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="ibox">
                        <div class="ibox-title">
                            <h5>Action</h5>
                        </div>

                        <div class="ibox-content">
                            {{ csrf_field() }}
                            <button class="btn btn-block btn-outline btn-success" type="submit"><i class="fa fa-floppy-o"></i> Save</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>

        <div class="mb30"></div>
    </div>
@stop