<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Facades\Auth;
use SoapBox\Formatter\Formatter;
use App\Language;

class customerAPI
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $auth = Auth::guard('customers')->basic();
        $langHeader = $request->header('Accept-Language');
        $language = Language::first();
        if($langHeader){
            $language = Language::where('locale', $langHeader)->first() ?? $language;
        }
        $request->language = $language;
        if(!Auth::guard('customers')->user()){
            return $auth;
        }
        $request->shop = Auth::guard('customers')->user()->shop;

        $response = $next($request);
        if($request->header('Accept') == 'application/xml'){
            $response->header('Content-Type', 'application/xml');
            $response->setContent(Formatter::make($response->getContent(), Formatter::JSON)->toXml());
            //dd($response);
        }
        return $auth ?: $response;
    }
}