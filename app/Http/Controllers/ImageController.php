<?php

namespace App\Http\Controllers;

use Storage;
use App\Product;
use App\ProductGroup;
use App\ProductImage;
use App\ImageProductGroup;
use App\Shop;
use App\Group;
use App\Brand;
use App\Image;
use App\ImageType;
use App\Http\Requests;
use Intervention\Image\Facades\Image as FacadesImage;
use Uuid;


class ImageController extends Controller
{
    public function imageTypes()
    {
        return ImageType::All();
    }

    public function UpdateImageType()
    {
        $imgType = $this->request->input('imgType');
        if($imgType){
            $image = Image::findOrFail($this->request->id);
            $image->image_type_id = $imgType;
            $image->save();
        }
        return ['response' => true];
    }

    public function UploadProductPic()
    {
        $shops = json_decode($this->request->shops);
        $uuid = Uuid::generate(4);
        $uploadedfile = $this->request->file('image')->getRealPath();
        $img = FacadesImage::make($uploadedfile)
                ->resize(500, 500, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                })
                ->resizeCanvas(500, 500, 'center', false, '#fff');


        $thumb = FacadesImage::make($uploadedfile)->fit(150, 150)
                ->resize(150, 150, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                })
                ->resizeCanvas(150, 150, 'center', false, '#fff');
        if(count($shops) > 0 && $shops[0] != 0){
            foreach ($shops as $shop) {
                $Image = new Image;
                $Image->filename = $uuid;
                $Image->shop_id = $shop;
                $Image->imagable_type = 'App\Product';
                $Image->imagable_id = $this->request->id;
                $Image->save();
            }

            if (isset($Image)) {
                Storage::put('cdn/images/'.$uuid.'.png', (string) $img->encode('png', 75), 'public');
                Storage::put('cdn/images/'.$uuid.'_thumb.png', (string) $thumb->encode('png', 75), 'public');
            }
        }
        return ['response' => true];
    }

        public function UploadBrandPic()
    {
        $shops = json_decode($this->request->shops);
        $uuid = Uuid::generate(4);
        $uploadedfile = $this->request->file('image')->getRealPath();
        $small = FacadesImage::make($uploadedfile)->fit(120, 25);
        $medium = FacadesImage::make($uploadedfile)->fit(130, 25);
        $large = FacadesImage::make($uploadedfile)->fit(165, 35);

        $brand = Brand::findOrFail($this->request->brand);

        if(count($shops) > 0 && $shops[0] != 0){
            foreach ($shops as $shop) {
                $Image = new Image;
                $Image->filename = $uuid;
                $Image->shop_id = $shop;
                $Image->imagable_type = 'App\Brand';
                $Image->imagable_id = $this->request->brand;
                $Image->save();
            }

            if (isset($Image)) {
                Storage::put('cdn/images/brands/'.$uuid.'_small.png', (string) $small->encode('png', 75), 'public');
                Storage::put('cdn/images/brands/'.$uuid.'_medium.png', (string) $medium->encode('png', 75), 'public');
                Storage::put('cdn/images/brands/'.$uuid.'_large.png', (string) $large->encode('png', 75), 'public');
            }
        }
        return ['response' => true];
    }

    public function DeleteProductPic()
    {
        $currentImage = Image::findOrFail($this->request->id);
        Storage::delete(['cdn/images/'.$currentImage->filename.'.png', 'cdn/images/'.$currentImage->filename.'_thumb.png']);
        $currentImage->delete();
        return ['response' => true];
    }

    public function DeleteBrandPic()
    {
        $currentImage = Image::findOrFail($this->request->id);
        Storage::delete(['cdn/images/'.$currentImage->filename.'_small.png', 'cdn/images/'.$currentImage->filename.'_medium.png']);
        $currentImage->delete();
        return ['response' => true];
    }

    public function UploadTyreTypePic(){
        $shops = json_decode($this->request->shops);
        $uuid = Uuid::generate(4);
        $uploadedfile = $this->request->file('image')->getRealPath();
        $img = FacadesImage::make($uploadedfile)
                ->resize(500, 500, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                })
                ->resizeCanvas(500, 500, 'center', false, '#fff');


        $thumb = FacadesImage::make($uploadedfile)->fit(150, 150)
                ->resize(150, 150, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                })
                ->resizeCanvas(150, 150, 'center', false, '#fff');
        if(count($shops) > 0 && $shops[0] != 0){

            foreach ($shops as $shop) {
                $Image = new Image;
                $Image->filename = $uuid;
                $Image->image_type_id = ImageType::findOrFail(1)->id;
                $Image->imagable_type = 'App\Group';
                $Image->imagable_id = $this->request->id;
                $Image->shop_id = $shop;
                $Image->save();
            }
            if (isset($Image)) {
                Storage::put('cdn/images/'.$uuid.'.png', (string) $img->encode('png', 75), 'public');
                Storage::put('cdn/images/'.$uuid.'_thumb.png', (string) $thumb->encode('png', 75), 'public');
            }
        }
        return ['response' => true];
    }

    public function UploadGroupPic()
    {
        $shops = json_decode($this->request->shops);
        $uuid = Uuid::generate(4);
        $uploadedfile = $this->request->file('image')->getRealPath();
        $img = FacadesImage::make($uploadedfile)
                ->resize(500, 500, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                })
                ->resizeCanvas(500, 500, 'center', false, '#fff');


        $thumb = FacadesImage::make($uploadedfile)->fit(150, 150)
                ->resize(150, 150, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                })
                ->resizeCanvas(150, 150, 'center', false, '#fff');
        if(count($shops) > 0 && $shops[0] != 0){

            foreach ($shops as $shop) {
                $Image = new Image;
                $Image->filename = $uuid;
                $Image->image_type_id = ImageType::findOrFail(1)->id;
                $Image->imagable_type = 'App\ProductGroup';
                $Image->imagable_id = $this->request->id;
                $Image->shop_id = $shop;
                $Image->save();
            }
            if (isset($Image)) {
                Storage::put('cdn/images/'.$uuid.'.png', (string) $img->encode('png', 75), 'public');
                Storage::put('cdn/images/'.$uuid.'_thumb.png', (string) $thumb->encode('png', 75), 'public');
            }
        }
        return ['response' => true];
    }

    public function DeleteGroupPic()
    {
        $currentImage = Image::findOrFail($this->request->id);

        $numberLeft = Image::where('filename', $currentImage->filename)->count();

        if($numberLeft < 1) {
            Storage::delete(['cdn/images/'.$currentImage->filename.'.png', 'cdn/images/'.$currentImage->filename.'_thumb.png']);
        }
        $currentImage->delete();
        return ['response' => true];
    }

    public function getGroupImages(){
        $groups = Group::with('images.imagetype');
        if($this->request->input('filter')){
            $groups->where('name', 'LIKE', '%'.strtolower($this->request->input('filter')).'%');
        }
        return $groups->get();
    }


    public function Index()
    {
        $shops = Shop::All();
        return view('dashboard.images', compact('shops'));
    }

    public function bulkImageUpload()
    {
        //dd($this->request->file('file'));
//        dd($this->request->input());
        $Image = null;
        if ($this->request->hasFile('file')) {
            $file = $this->request->file('file');
            $sku = explode('_', $file->getClientOriginalName())[0];
            $product = Product::where('sku', $sku)->first();
            $shops = $this->request->selectedshops;
            if($product){
                if(count($shops) > 0 && $shops[0] != 0){
                    foreach ($shops as $shop) {
                        $shopModel = Shop::findOrFail($shop);
                        $uuid = Uuid::generate(4);
                        $img = FacadesImage::make($file)
                            ->resize($shopModel->default_image_width, $shopModel->default_image_height, function ($constraint) {
                                $constraint->aspectRatio();
                                $constraint->upsize();
                            })
                            ->resizeCanvas($shopModel->default_image_width, $shopModel->default_image_height, 'center', false, '#fff');


                        $thumb = FacadesImage::make($file)->fit(150, 150)
                            ->resize(150, 150, function ($constraint) {
                                $constraint->aspectRatio();
                                $constraint->upsize();
                            })
                            ->resizeCanvas(150, 150, 'center', false, '#fff');
                        $Image = new Image;
                        $Image->filename = $uuid;
                        $Image->shop_id = $shop;
                        $Image->imagable_type = 'App\Product';
                        $Image->imagable_id = $product->id;
                        $Image->save();
                        if (isset($Image)) {
                            Storage::put('cdn/images/'.$uuid.'.png', (string) $img->encode('png', 75), 'public');
                            Storage::put('cdn/images/'.$uuid.'_thumb.png', (string) $thumb->encode('png', 75), 'public');
                        }
                    }
                }
            }
        }
        if($Image){
            return ['status' => 'true'];
        }
        return ['status' => 'false'];
    }

    public function bulkImage()
    {
        $shops = Shop::All();
        return view('dashboard.image_bulk', compact('shops'));
    }
}
