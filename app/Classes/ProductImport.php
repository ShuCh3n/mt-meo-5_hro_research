<?php

namespace App\Classes;
use App\ProductFeatureType;
use App\ProductFeature;
use App\ProductFeatureOption;
use App\ShopProduct;
use App\Category;
use App\CategoryInfo;

class ProductImport {
	public function __construct($request)
    {
    	$this->request = $request;
    }

    public function setShop($shop)
    {
    	$this->shop = $shop;
    }

    public function setLanguage($language)
    {
    	$this->language = $language;
    }

    public function setProduct($product)
    {
    	$this->product = $product;
    }

    public function isImportable()
    {
    	if($this->shop && $this->language && $this->product)
    	{
    		return true;
    	}

    	return false;
    }

    public function attachCategory()
    {
        $category = $this->shop->categories()->whereHas('info', function($query){
            $query->language(2)->name($this->request->category);
        })->first();

        if(!$category)
        {
            $category = new Category;
            $category->shop_id = $this->shop->id;
            $category->rank = 10;
            $category->save();

            $category_info = new CategoryInfo;
            $category_info->category_id = $category->id;
            $category_info->language_id = 2;
            $category_info->name = $this->request->category;
            $category_info->save();
        }

        $this->product->categories()->attach($category);
    }

    public function importFeatures()
    {
        $product_feature_options = array();

    	foreach($this->request as $column => $value)
        {
            if(strpos($column, 'feature_') !== false) {
                $feature_type_name = $this->request->featuretype;
                $feature_name = str_replace("feature_", "", $column);

                $feature_type = ProductFeatureType::firstOrCreateFeatureType($feature_type_name, $this->language->id);
                $product_feature_option = get_feature_option_value($this->product->product_feature_options, title_case($feature_name));
                
                if(!$product_feature_option || title_case(collect($product_feature_option->infos)->first()->name) != title_case($value))
                {
                    $product_feature = ProductFeature::firstOrCreateFeature($feature_type, $feature_name, $this->language->id);
                    $product_feature_option = ProductFeatureOption::firstOrCreateFeatureOption($product_feature, $value, $this->language->id);
                }

                $product_feature_options[] = $product_feature_option;
            }
        }

        $this->product->syncFeatureOption($product_feature_options);

        return $this;
    }

    public function importBrand()
    {
    	if(!$this->product->brand || $this->product->brand && strtolower($this->product->brand->name) != strtolower($this->request->product_brand))
        {
            $brand = Brand::getBrandOrCreate($this->request->product_brand);
            $this->product->brand_id = $brand->id;
            $this->product->save();
        }

        return $this;
    }

    public function addShopProduct()
    {
        ShopProduct::updateOrCreate(
            ['shop_id' => $this->shop->id, 'product_id' => $this->product->id],
            [
                'tax_rule_id' => $this->product->tax_rule_id,
                'minimum_qty' => $this->product->minimum_qty,
                'stock_management' => $this->product->stock_management,
                'active_when_out_of_stock' => $this->product->active_when_out_of_stock,
                'deny_order_when_out_of_stock' => $this->product->deny_order_when_out_of_stock,
                'additional_fee_per_item' => 0,
                'active' => 0
            ]
        );

        return $this;
    }

    public function importProductShopInfo()
    {
    	if($this->product->shop_info)
        {
            $shop_product = $this->product->shop_info;
        }
        else
        {
            $shop_product = new ProductShopInfo;
            $shop_product->shop_id = $shop->id;
            $shop_product->language_id = $this->language->id;
        }

        $shop_product->name = $this->request->product_name;
        $shop_product->short_description = $this->request->product_short_description;
        $shop_product->description = $this->request->product_description;
        $shop_product->specification = @$this->request->product_specification;
        $shop_product->link = @$this->request->product_link;
        $shop_product->save();

        return false;
    }

    public function updateShopPrice()
    {
		if(!empty($this->request->retail_price))
		{
			$this->product->insertShopPrice($this->shop, $this->request->retail_price);
		}

		return false;
    }
}