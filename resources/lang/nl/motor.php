<?php
/*************************************************************************
 Generated via "php artisan localization:missing" at 2017/04/10 17:31:53
*************************************************************************/

return array (
  'auto_fill' => 'Automatisch invullen',
  'product' => 'Product',
  'action' => 'Actie',
  'brand' => 'Merk',
  'category' => 'Categorie',
  'cc' => 'CC',
  'code' => 'Code',
  'different_size' => 'Afwijkende bandenmaat',
  'edit' => 'Bewerken',
  'export_excel' => 'Exporteer naar Excel',
  'filter' => 'Filter',
  'filters' => 'Filters',
  'id' => 'ID',
  'model' => 'Model',
  'motor' => 'Motor',
  'motors' => 'Motors',
  'name' => 'Naam',
  'new_motor' => 'Nieuwe motor',
  'save' => 'Opslaan',
  'search' => 'Zoeken',
  'type' => 'Type',
  'tyre_size' => 'Bandenmaat',
  'view' => 'Bekijken',
  'years' => 'Jaren',
);
