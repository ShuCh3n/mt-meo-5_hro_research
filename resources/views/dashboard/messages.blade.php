@extends('dashboard.layout.main')

@section('content')
        <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-lg-3">
                <div class="ibox float-e-margins">
                    <div class="ibox-content mailbox-content">
                        <div class="file-manager">
                            <a class="btn btn-block btn-primary compose-mail" href="mail_compose.html"><i class="fa fa-ticket" aria-hidden="true"></i> Compose Ticket</a>
                            <div class="space-25"></div>
                            <h5>Tickets</h5>
                            <ul class="folder-list m-b-md" style="padding: 0">
                                <li><a href="{{ route('messages') }}"> <i class="fa fa-inbox "></i> Open <span class="label label-warning pull-right">{{ $total_unreaded_messages->count() }}</span> </a></li>
                                <li><a href="{{ route('closed_messages') }}"> <i class="fa fa-envelope-o"></i> Closed</a></li>
                            </ul>
                            <h5>Department</h5>
                            <ul class="category-list" style="padding: 0">
                                @foreach($departments as $department)
                                    <li><a href="#"> <i class="fa fa-circle" style="color:#{{ $department->color }}"></i> {{ $department->name }}</a></li>
                                @endforeach
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-9 animated fadeInRight">
            <div class="mail-box-header">

                <form method="get" action="index.html" class="pull-right mail-search">
                    <div class="input-group">
                        <input type="text" class="form-control input-sm" name="search" placeholder="Search Tickets">
                        <div class="input-group-btn">
                            <button type="submit" class="btn btn-sm btn-primary">
                                Search
                            </button>
                        </div>
                    </div>
                </form>
                <h2>
                    Inbox ({{ $tickets->count() }})
                </h2>
                <div class="mail-tools tooltip-demo m-t-md">
                    <div class="btn-group pull-right">
                        <button class="btn btn-white btn-sm"><i class="fa fa-arrow-left"></i></button>
                        <button class="btn btn-white btn-sm"><i class="fa fa-arrow-right"></i></button>

                    </div>
                    <button class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="left" title="Refresh inbox"><i class="fa fa-refresh"></i> Refresh</button>
                    <button class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="Mark as unread"><i class="fa fa-eye-slash" aria-hidden="true"></i> </button>
                    <button class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="Mark as closed"><i class="fa fa-times" aria-hidden="true"></i> </button>
                    <button class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="Move to trash"><i class="fa fa-trash-o"></i> </button>
                </div>
            </div>
                <div class="mail-box">
                    <table class="table table-hover table-mail">
                        <tbody>
                            @foreach($tickets as $ticket)
                                <tr class="{{ ($ticket->isUnread())? 'unread' : 'read' }}">
                                    <td class="check-mail">
                                        <input type="checkbox" class="i-checks">
                                    </td>
                                    <td class="mail-ontact">
                                        @if($ticket->customer)
                                            <a href="{{ route('view_messages', [$ticket->id]) }}">{{ $ticket->customer->firstname . ' ' . $ticket->customer->lastname }}</a>
                                        @else
                                            <a href="{{ route('view_messages', [$ticket->id]) }}">{{ $ticket->first_name . ' ' . $ticket->last_name }}</a>
                                        @endif

                                        <span class="label pull-right" style="background-color:#{{ $ticket->ticket_department->color  }}; color:#fff;">{{ $ticket->ticket_department->name }}</span>
                                    </td>
                                    <td class="mail-subject"><a href="{{ route('view_messages', [$ticket->id]) }}">{{ $ticket->subject }}</a></td>
                                    <td class=""></td>
                                    <td class="text-right mail-date">{{ date("H:i d M Y", strtotime($ticket->ticket_messages()->latest()->first()->created_at)) }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        </div>
@stop