<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
    public function scopeName($query, $name)
    {
    	return $query->where('name', $name);
    }
}
