<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Storage;
use File;

class Shop extends Model
{
    protected $fillable = ['name'];
    protected $hidden = ["AV_admin", "key", "updated_at", "created_at", "maintenance"];

    public function categories()
    {
        return $this->hasMany(Category::class);
    }

    public function shippings(){
        return $this->belongsToMany(Shipping::class);
    }

    public function theme()
    {
        return $this->belongsTo(Theme::class);
    }

    public function languages()
    {
        return $this->belongsToMany(Language::class);
    }

    public function currencies()
    {
        return $this->belongsToMany(Currency::class);
    }

    public function countries()
    {
        return $this->belongsToMany(Country::class);
    }

    public function pages()
    {
        return $this->hasMany(Page::class);
    }

    public function emails()
    {
        return $this->hasMany(Email::class);
    }

    public function tickets()
    {
        return $this->hasMany(Ticket::class);
    }

    public function shop_products()
    {
        return $this->hasMany(ShopProduct::class);
    }

    public function products()
    {
        return $this->hasMany(ShopProduct::class)->getRelation('product')->productFilter($this->id)->shopActive($this->id);
    }

    public function paynl()
    {
        return $this->hasOne(Paynl::class);
    }

    public function tax_rule()
    {
        return $this->belongsTo(TaxRule::class);
    }

    public function blogs()
    {
        return $this->hasMany(Blog::class);
    }

    public function customers()
    {
        return $this->hasMany(Customer::class);
    }

    public function customer()
    {
        return $this->hasOne(Customer::class);
    }

    public function scopeName($query, $name)
    {
        return $query->where('name', $name);
    }

    public function blogposts($blog, $lang){
        
        return $this->hasMany(Blog::class)
            ->where('active', true)
            ->findOrFail($blog)->firstOrFail()
            ->blogposts()
            ->where('active', true)
            ->where('published_at' ,'<', date('Y-m-d H:i:s'))
            ->whereHas('content', function($query) use($lang){
                return $query->where('languages_id', $lang);
            })
            ->with(['content'=> function($query) use($lang){
                return $query->where('languages_id', $lang);
            }])
            ->orderBy('published_at');
    }
    
    public function brands()
    {
        $brands = [];

        $products = $this->products()->whereHas('brand')->distinct('brand_id')->get();

        foreach($products as $product)
        {
            $brands[] = $product->brand; 
        }

        return collect($brands);
    }

    public function ShopCategories($language)
    {
        $categories = array();

        foreach($this->categories()->whereNull('parent_id')->orderBy('rank', 'asc')->get() as $category)
        {
            $category_info = $category->infos()->where('language_id', $language->id)->first();

            $categories[] = array(
                "id"			=> $category->id,
                "name"			=> $category_info->name,
                "link"			=> $category_info->link . "-" . $category->id,
                "description"	=> $category_info->description,
                "subcategories"	=> $category->getSubCategories()
            );
        }

        return $categories;
    }

    public function hasLanguage($language)
    {
        if(is_string($language) || is_int($language))
        {
            return $this->languages->contains('id', $language);
        }

        return !! $language->intersect($this->languages)->count();
    }

    public function hasCurrency($currency)
    {
        return $this->currencies->contains('id', $currency);
    }

    public function hasCountry($country)
    {
        return $this->countries->contains('id', $country);
    }

    public function unsignLanguage($language_id = false)
    {
        if($language_id)
        {
            $this->languages()->detach($language_id); 
        }
        else
        {
            $this->languages()->detach();
        }
    }

    public function assignLanguage($language_id)
    {
        return $this->languages()->save(
            Language::whereId($language_id)->firstOrFail()
        );
    }

    public function assignCurrency($currency_id)
    {
        return $this->currencies()->save(
            Currency::whereId($currency_id)->firstOrFail()
        );
    }

    public function assignCountry($country_id)
    {
        return $this->countries()->save(
            Country::whereId($country_id)->firstOrFail()
        );
    }

    public function unsignCurrency($currency_id = false)
    {
        if($currency_id)
        {
            $this->currencies()->detach($currency_id); 
        }
        else
        {
            $this->currencies()->detach();
        }
    }

    public function unsignCountry($country_id = false)
    {
        if($country_id)
        {
            $this->countries()->detach($country_id); 
        }
        else
        {
            $this->countries()->detach();
        }
    }

    public function hasEmail($email)
    {
        return $this->customers->contains('email', $email);
    }

    public static function getShop($shop)
    {
        if(is_int($shop))
        {
            return Shop::findOrFail($shop);
        }

        return Shop::where("name", $shop)->first();
    }

    public function getPayNLPayments()
    {
        if($this->paynl)
        {
            return $this->paynl->config()->payments();
        }

        return array();
    }

    public function payments()
    {
        $this->payments = array();

        if($this->paynl)
        {
            $this->payments = array_merge($this->payments, $this->getPayNLPayments());
        }

        return $this;
    }

    public function pay($provider, $transaction = array())
    {
        if($provider == "paynl")
        {
            return $this->paynl->config()->pay($transaction);
        }
    }

    public function confirmPayment($request)
    {
        if($request->provider == "paynl")
        {
            return $this->paynl->config()->confirm($request->order_id);
        }
    }

    public function setShop($request)
    {
        if($request->logo)
        {
            $filename = str_random(40) . "." . $request->logo->getClientOriginalExtension();
            Storage::put('shops/' . $filename, File::get($request->logo), 'public');

            $this->logo = $filename;
        }

        $this->name = $request->name;
        $this->link = $request->link;
        $this->tax_rule_id = ($request->tax_rule)? $request->tax_rule : null;
        $this->key = ($this->key)? $this->key : str_random(40);
        $this->save();

        if($request->paynl_active)
        {
            $paynl = $this->paynl;

            if(!$paynl)
            {
                $paynl = new Paynl;
                $paynl->shop_id = $this->id;
            }

            $paynl->test_mode = $request->paynl_mode;
            $paynl->service_id = $request->paynl_service_id;
            $paynl->api_token = $request->paynl_api_token;
            $paynl->save();
        }

        if(!$request->paynl_active && $this->paynl)
        {
            $this->paynl->delete();
        }

        $this->languages()->sync($request->languages);
        $this->currencies()->sync($request->currencies);
        $this->countries()->sync($request->countries);

        return $this;
    }
}




