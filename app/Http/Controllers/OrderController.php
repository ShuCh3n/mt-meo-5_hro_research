<?php

namespace App\Http\Controllers;

use App\Product;
use App\GeneratedOrderFile;

use App\Customer;
use App\Shop;
use App\OrderStatus;
use App\Payment;
use App\Order;
use File;
use Storage;

use App\Http\Requests;

class OrderController extends Controller
{
    public function orders(){
        $orders = Order::orderBy('created_at', 'desc')->get();

        return view('dashboard.orders', compact('orders'));
    }

    public function viewOrder()
    {
        $order = Order::findOrFail($this->request->id);
        $order_statuses = OrderStatus::oldest('percentage')->get();

        return view('dashboard.view_order', compact('order', 'order_statuses'));
    }

    public function newOrder()
    {
        $customers = Customer::all();
        $shops = Shop::all();
        $customer_id = $this->request->customer_id;
        $order_statusues = OrderStatus::all();
        $payments = Payment::all();

        return view('dashboard.new_order', compact('customers', 'customer_id', 'shops', 'order_statusues', 'payments'));
    }

    public function actionNewOrder()
    {
        $this->validate($this->request, [
            'shop' => 'required',
            'customer' => 'required',
            'customer_address' => 'required',
        ]);

        dd($this->request);
    }

    public function generatedOrders()
    {
        $histories = GeneratedOrderFile::orderBy('created_at', 'desc')->where('deleted_at', null)->paginate(20);
        return view('dashboard.generated_orders', compact('histories'));
    }

    public function uploadGeneratedOrderFile()
    {
        $filename = date('YmdHis') . '_' .str_random(10) . "." . $this->request->file->getClientOriginalExtension();
        $file = File::get($this->request->file);

        Storage::disk('local')->put('generated-orders/excel/' . $filename, $file);

        shell_exec(escapeshellcmd('python '.base_path('python/orderGeneratorExcelConverter.py').' '.$filename));

        $generatedOrderFile = new GeneratedOrderFile;
        $generatedOrderFile->user_id = auth()->user()->id;
        $generatedOrderFile->original_filename = $this->request->file->getClientOriginalName();
        $generatedOrderFile->filename = $filename;

        $generatedOrderFile->save();

        return array(
            "status" => true,
            "id"    => $generatedOrderFile->id,
            "user"  => auth()->user()->firstname,
            "date"  => date('d M Y', strtotime($generatedOrderFile->created_at)),
            "file_link" => route('review_generated_order_file', [$generatedOrderFile->id, $this->request->file->getClientOriginalName()])
        );
    }

    public function removeFile()
    {
        $file = GeneratedOrderFile::findOrFail($this->request->id);
        $file->deleted_at = date('Y-m-d H:i:s');
        $file->save();
    }

    public function reviewGeneratedOrder()
    {
        if(isset($_GET['settings']))
            OrderController::updateGeneratedOrderSettings();

        $source = GeneratedOrderFile::findOrFail($this->request->id);

        if($source->deleted_at != null)
            die('The requested file was removed!');

        $file = json_decode(file_get_contents(base_path('storage/app/generated-orders/json/'.$source->filename.'.json')), true);

        if(!file_exists(base_path('storage/app/generated-orders/settings.json')))
            die('Settings.json couldn\'t be found!');

        $settings = json_decode(file_get_contents(base_path('storage/app/generated-orders/settings.json')), true);

        $orders = array();
        $block = false;

        foreach($file as $row) {
            $product = Product::where('sku', $row['artikelcode'])->first();
            if(count($product) < 1)
                $product = Product::where('supplier_sku', $row['artikelcode'])->first();
                if(count($product) < 1)
                    $product = Product::where('ean', $row['artikelcode'])->first();
                    if(count($product) < 1)
                        $block = true;
            if($row['artikelcode'] != $product->sku)
                $row['artikelcode'] = $product->sku.' ('.$row['artikelcode'].')';

            if($block) {
                $row['naam'] = false;
                $row['merk'] = false;
            }
            else {
                $row['naam'] = $product->infos->where('id', $product->id)->first();
                $row['merk'] = $product->brand->where('id', $product->brand_id)->first();
                if(count($row['naam']) < 1 |count($row['merk']) < 1) {
                    $row['naam'] = false;
                    $row['merk'] = false;
                    $block = true;
                }
                else {
                    $row['naam'] = $row['naam']->name;
                    $row['merk'] = $row['merk']->name;
                }
            }



            $creditor = Customer::where('av_deb', $row['crediteurnummer'])->first();
            if(count($creditor) < 1)
                $row['crediteur'] = false;
            else
                $row['crediteur'] = $creditor->company_name;



            if(isset($orders[$row['bestelling id']]))
                array_push($orders[$row['bestelling id']]['regels'], $row);
            else {
                $orders[$row['bestelling id']] = array(
                    'inkoper' => OrderController::getNameFromAdministration($row['inkoop admin']),
                    'inkoop debiteur' => OrderController::getDebnoFromAdministration($row['inkoop admin']),
                    'inkoop admin' => $row['inkoop admin'],
                    'inkoop magazijn' => $row['inkoop magazijn'],
                    'inkoop locatie' => $row['inkoop locatie'],
                    'eigenaar' => OrderController::getNameFromAdministration($row['eigenaar admin']),
                    'eigenaar debiteur' => OrderController::getDebnoFromAdministration($row['eigenaar admin']),
                    'eigenaar admin' => $row['eigenaar admin'],
                    'eigenaar magazijn' => $row['eigenaar magazijn'],
                    'eigenaar locatie' => $row['eigenaar locatie'],
                    'crediteur' => $row['crediteur'],
                    'crediteurnummer' => $row['crediteurnummer'],
                    'besteldatum' => $row['besteldatum'],
                    'leverdatum' => $row['leverdatum'],
                    'valuta' => $row['valuta'],
                    'referentie' => $row['referentie'],
                    'regels' => array($row)
                );
            }
        }

        $file_id = $this->request->id;
        $processed = $source->processed;

        return view('dashboard.generate_order', compact('orders', 'file_id', 'processed', 'block', 'settings'));
    }

    public function processGeneratedOrder()
    {
        $file = GeneratedOrderFile::findOrFail($this->request->id);

        $output = shell_exec(escapeshellcmd('python '.base_path('python/orderGeneratorXmlConverter.py').' '.$file->filename.'.json').' 2>&1');

        $output = json_decode($output, true);

        if($output['success']) {
            $file->processed = 1;
            $file->save();
            return $output;
        }
        else
            return $output;
    }

    public function updateGeneratedOrderSettings()
    {
        Storage::disk('local')->put('generated-orders/settings.json', json_encode($_GET['settings']));
    }

    public function getNameFromAdministration($administration)
    {
        $settings = json_decode(file_get_contents(base_path('storage/app/generated-orders/settings.json')), true);
        $result = false;
        foreach($settings as $key=>$val)
            if($val['admin'] == $administration)
                $result = ucwords(str_replace('_', ' ', $key));
        return $result;
    }

    public function getDebnoFromAdministration($administration)
    {
        $settings = json_decode(file_get_contents(base_path('storage/app/generated-orders/settings.json')), true);
        $result = false;
        foreach($settings as $key=>$val)
            if($val['admin'] == $administration)
                $result = $val['debno'];
        return $result;
    }
}
