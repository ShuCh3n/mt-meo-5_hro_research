<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Weather;
use App\Country;

class UpdateWeather extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'weather:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update weather prices';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $client = guzzle()->get('http://api.openweathermap.org/data/2.5/weather', [
            'query' => [
                'q'     => 'NLD',
                'APPID' => '1658f8f778d791c44640b4401d7f8456',
                'units' => 'metric'
            ]
        ]);

        Weather::create([
            "country_id"    => Country::getCountry('NLD')->id,
            "degree"        => json_decode($client->getBody())->main->temp
        ]);
    }
}
