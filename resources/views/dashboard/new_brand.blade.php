@extends('dashboard.layout.main')

@section('stylesheet')
    <link href="/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="/css/plugins/select2/select2.min.css" rel="stylesheet">

    <link href="/css/plugins/summernote/summernote.css" rel="stylesheet">
    <link href="/css/plugins/summernote/summernote-bs3.css" rel="stylesheet">

    <style>
        .profile_picture .picture {
            display: block;
            overflow: hidden;
            width: 200px;
            height: 150px;
        }
        .profile_picture .picture img {
            width: 100%;
        }
        .profile_picture .after, .passport_picture .after {
            color: #fff;
            cursor: pointer;
            display: none;
            padding-top: 75px;
            position: absolute;
            top: 0;
            width: 200px;
            height: 150px;
        }
        .profile_picture:hover .after, .passport_picture:hover .after {
            background: rgba(0, 0, 0, 0.5) none repeat scroll 0 0;
            display: block;
        }
        input[type="file"] {
            display: none;
        }
    </style>
@stop

@section('javascript')
    <!-- iCheck -->
    <script src="/js/plugins/iCheck/icheck.min.js"></script>

    <!-- Select2 -->
    <script src="/js/plugins/select2/select2.full.min.js"></script>

    <!-- SUMMERNOTE -->
    <script src="/js/plugins/summernote/summernote.min.js"></script>

    <script>
        $(function(){
            $("#logo-file-input").change(function(){
                if (this.files && this.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('#logo-img').attr('src', e.target.result);
                    }

                    reader.readAsDataURL(this.files[0]);
                }
            });
        });
    </script>
@stop

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>{{ trans('pages.inquiry_detail_header') }}</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('home') }}">{{ trans('menu.home') }}</a>
                </li>
                <li>
                    <a href="">{{ trans('menu.inquiries') }}</a>
                </li>
                <li class="active">
                    <strong>{{ trans('page.inquiry') }}</strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">
        {!! session()->has('success')? '<div class="alert alert-success alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>' . session('success') . '</div>' : '' !!}

        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <form method="post" class="form-horizontal" enctype="multipart/form-data">
            <div class="row">
                <div class="col-md-8">
                    <div class="ibox">

                        <div class="ibox-title">
                            <h5>New Brand</h5>
                        </div>

                        <div class="ibox-content">
                            <div class="mb20"></div>

                            <div class="row">

                                <div class="col-sm-4 col-md-3">
                                    <div class="m-b-sm">
                                        <div class="row text-center">
                                            <div class="profile_picture">
                                                <label for="logo-file-input">
                                                    <div class="picture" v-else>
                                                        <img id="logo-img" src="http://static.independent.co.uk/s3fs-public/styles/article_large/public/thumbnails/image/2016/02/25/13/cat-getty_0.jpg" alt="image">
                                                        <div class="after">Change</div>
                                                    </div>
                                                </label>

                                                <input id="logo-file-input" type="file" name="logo" />
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-8 col-md-9">
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Brand Name</label>
                                        <div class="col-md-10"><input type="text" class="form-control" name="name" placeholder="Name"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="ibox">
                        <div class="ibox-title">
                            <h5>Action</h5>
                        </div>

                        <div class="ibox-content">
                            {{ csrf_field() }}
                            <button class="btn btn-block btn-outline btn-success" type="submit"><i class="fa fa-floppy-o"></i> Save</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>

        <div class="mb30"></div>
    </div>
@stop