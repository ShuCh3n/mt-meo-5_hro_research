@extends('dashboard.layout.main')


@section('content')
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <h2>{{ $search->total }} results found for: <span class="text-navy">“{{ request()->q }}”</span></h2>

                        <div class="search-form">
                            <form action="{{ route('search') }}" method="get">
                                <div class="input-group">
                                    <input type="text" placeholder="Admin Theme" name="q" class="form-control input-lg" value="{{ request()->q }}">
                                    <div class="input-group-btn">
                                        <button class="btn btn-lg btn-primary" type="submit">Search</button>
                                    </div>
                                </div>
                            </form>
                        </div>

                        @foreach($search->results as $result)
                            <div class="hr-line-dashed"></div>
                            <div class="search-result">
                                <h3><a href="{{ $result->link }}">{{ $result->title ?? '' }}</a></h3>
                                <a href="{{ $result->link }}" class="search-link">{{ $result->link ?? '' }}</a>
                                <p>{{ $result->description ?? '' }}</p>
                            </div>
                        @endforeach
                
                        <div class="text-center">
                            {{ $search->results->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop