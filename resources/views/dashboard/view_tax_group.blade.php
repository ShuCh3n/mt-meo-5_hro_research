@extends('dashboard.layout.main')

@section('stylesheet')
    <link href="/css/plugins/select2/select2.min.css" rel="stylesheet">
@stop

@section('javascript')
    <!-- Select2 -->
    <script src="/js/plugins/select2/select2.full.min.js"></script>

    <script>
        $(function(){
            $(".shop_list").select2();
        });
    </script>
@stop

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-6">
            <h2>{{ trans('pages.inquiry_detail_header') }}</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('home') }}">{{ trans('menu.home') }}</a>
                </li>
                <li>
                    <a href="">{{ trans('menu.inquiries') }}</a>
                </li>
                <li class="active">
                    <strong>{{ trans('page.inquiry') }}</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-6">
            <div class="btn-group pull-right mt30">
                <a class="btn btn-warning  btn-outline" href="{{ route('new_tax_group') }}"><i class="fa fa-plus-circle" aria-hidden="true"></i> New Group</a>
                <a class="btn btn-primary pull-right btn-outline" href="{{ route('new_tax', $tax_group->id) }}"><i class="fa fa-plus-circle" aria-hidden="true"></i> New Tax Rule</a>
            </div>
        </div>
    </div>


	<div class="wrapper wrapper-content animated fadeInRight ecommerce">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-content">

                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th data-toggle="true">Code</th>
                                    <th data-toggle="true">Tax Name</th>
                                    <th data-toggle="true">Fixed Price</th>
                                    <th data-toggle="true">Percentage</th>
                                    <th data-toggle="true">Enable</th>
                                    <th class="text-right" data-sort-ignore="true">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($tax_group->tax_rules as $rule)
                                    <tr>
                                        <td>{{ $rule->code }}</td>
                                        <td>{{ $rule->name }}</td>
                                        <td>{{ ($rule->fixed_price)? "€ " . $rule->fixed_price : '' }}</td>
                                        <td>{{ ($rule->percentage)? $rule->percentage . "%" : '' }}</td>
                                        <td>{!! ($rule->enable)? '<i class="fa fa-check text-navy" aria-hidden="true"></i>' : '<i class="fa fa-times text-danger" aria-hidden="true"></i>' !!}</td>
                                        <td class="text-right">
                                            <a class="btn-white btn btn-xs" href="{{ route('edit_tax', [$tax_group->id, $rule->id]) }}">Edit</a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop