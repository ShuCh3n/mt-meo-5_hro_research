<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductPrice extends Model
{
    use SoftDeletes;

    protected $hidden = [
        'created_at', 
        'deleted_at', 
        'updated_at',
        'purchasing_price'
    ];

    protected $fillable = ['product_id', 'purchasing_price', 'retail_price', 'list_price', 'unit_name', 'unit_price'];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
