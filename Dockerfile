FROM ubuntu:16.10
MAINTAINER Stenn Kool <stennkool@gmail.com>

RUN apt-get update && apt-get -y install supervisor git emacs-nox php-elisp nano nginx curl php-fpm php-mcrypt php-json php-curl unzip php-xml php-mbstring php-gd php-mysql composer && apt-get -y autoremove && apt-get clean && rm -rf /var/lib/apt/lists/*

COPY container-build/php.ini /etc/php/7.0/fpm/php.ini

ENV TERM=xterm

RUN /bin/rm /etc/nginx/sites-enabled/default

RUN /bin/mkdir /run/php

RUN /bin/echo 'catch_workers_output = yes' >> /etc/php/7.0/fpm/pool.d/www.conf

COPY . /var/www/laravel

WORKDIR /var/www/laravel

RUN locale-gen nl_NL

COPY container-build/laravel.conf /etc/nginx/sites-enabled/laravel

COPY container-build/nginx.conf /etc/nginx/nginx.conf

COPY container-build/supervisord.conf /etc/supervisor/conf.d/supervisord.conf

RUN /bin/chmod -R 777 /var/www/laravel/storage

RUN /bin/chmod -R 777 /var/www/laravel/bootstrap/cache

RUN /usr/bin/composer update

EXPOSE 80

CMD /usr/bin/supervisord
