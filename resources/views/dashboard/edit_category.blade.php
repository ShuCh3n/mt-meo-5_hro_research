@extends('dashboard.layout.main')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>{{ trans('pages.inquiry_detail_header') }}</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('home') }}">{{ trans('menu.home') }}</a>
                </li>
                <li>
                    <a href="">{{ trans('menu.inquiries') }}</a>
                </li>
                <li class="active">
                    <strong>{{ trans('page.inquiry') }}</strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">

        <form method="post" class="form-horizontal">
            <div class="row">
                <div class="col-md-8">
                    <div class="ibox">

                        <div class="ibox-title">
                            <h5>Parent Category</h5>
                        </div>

                        <div class="ibox-content">
                            <div class="mb20"></div>

                            <div class="row">
                                <div class="col-md-12">
                                
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Shops</label>

                                        <div class="col-md-10">
                                            @foreach($shops as $shop)
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <label>
                                                            <input class="i-checks" type="checkbox" name="shops[]" value="{{ $shop->id }}" {{ ($shop->id == $category->shop_id)? 'checked' : '' }} /> {{ $shop->name }}
                                                        </label>
                                                    </div>

                                                    <div class="col-md-9">
                                                        <select2 name="parent_category[{{ $shop->id }}]">
                                                            <option value="0">Root</option>
                                                            @foreach($shop->categories as $shop_category)
                                                                <option value="{{ $shop_category->id }}" {{ ($category->parent)? ($category->parent->id == $shop_category->id)? 'selected' : '' : '' }}>
                                                                    {{ $shop_category->info->name }}
                                                                </option>
                                                            @endforeach
                                                        </select2>
                                                    </div>
                                                </div>
                                                <div class="mb15"></div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="ibox">

                        <div class="ibox-title">
                            <h5>Category Info</h5>
                        </div>

                        <div class="ibox-content">
                            <div class="mb20"></div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Language</label>
                                        <div class="col-md-10">
                                            <language-select>
                                                @foreach($languages as $language)
                                                    <option value="{{ $language->id }}" icon="{{ $language->icon }}">{{ $language->name }}</option>
                                                @endforeach
                                            </language-select>
                                        </div>
                                    </div>

                                    @foreach($languages as $language)
                                        <div class="multi_language {{ $language->id }}">
                                            <div class="form-group">
                                                <label class="col-md-2 control-label">Category Name {!! flag_icon($language->icon) !!}</label>
                                                <div class="col-md-10">
                                                    <input type="text" class="form-control" data-input="name" name="category_info[{{ $language->id }}][category_name]" placeholder="Category Name" value="{{ @$category->info()->language($language->id)->first()->name }}">
                                                </div>
                                            </div>                                 

                                            <div class="form-group">
                                                <label class="col-md-2 control-label">Meta Description {!! flag_icon($language->icon) !!}</label>
                                                <div class="col-md-10"><input type="text" class="form-control" data-input="description" name="category_info[{{ $language->id }}][meta_description]" placeholder="Describe short (Max 160)" value="{{ @$category->info()->language($language->id)->first()->meta_description }}"></div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-2 control-label">Meta Link {!! flag_icon($language->icon) !!}</label>
                                                <div class="col-md-10"><input type="text" class="form-control" data-input="link" name="category_info[{{ $language->id }}][meta_link]" placeholder="Custom Link" value="{{ @$category->info()->language($language->id)->first()->link }}"></div>
                                            </div>                                    

                                            <div class="form-group">
                                                <label class="col-md-2 control-label">Description {!! flag_icon($language->icon) !!}</label>
                                                <div class="col-md-10">
                                                    <summernote name="category_info[{{ $language->id }}][description]" height="350">
                                                        {{ @$category->info()->language($language->id)->first()->description }}
                                                    </summernote>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach

                                </div>
                            </div>
                        </div>
                    </div> 
                </div>

                <div class="col-md-4">
                    <div class="ibox">
                        <div class="ibox-title">
                            <h5>Action</h5>
                        </div>

                        <div class="ibox-content">
                            {{ csrf_field() }}
                            <button class="btn btn-block btn-outline btn-success" type="submit"><i class="fa fa-floppy-o"></i> Save</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>

        <div class="mb30"></div>
    </div>
@stop