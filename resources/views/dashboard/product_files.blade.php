@extends('dashboard.layout.main')

@section('content')
      <h2>Material safety data sheets</h2>
	<form action="{{route('uploadFileAction', $product->id)}}"
      class="dropzone">
            <input name="file" type="file" multiple />
            {{ csrf_field() }}

            <div class="dz-preview dz-file-preview dz-processing dz-success dz-complete">
                  @foreach ($product->msds as $msds)
                        <div class="dz-image">
                              <img data-dz-thumbnail="">
                        </div>
                        <div class="dz-details">
                              <div class="dz-size"></div>
                              <div class="dz-filename"><span data-dz-name="">{{$msds->filename}}</span></div>
                        </div>
                  @endforeach
      </form>
@stop
