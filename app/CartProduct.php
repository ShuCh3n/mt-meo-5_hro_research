<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CartProduct extends Model
{
    protected $hidden = ["created_at", "updated_at"];

    public function product()
    {
    	return $this->belongsTo(Product::class);
    }
}
