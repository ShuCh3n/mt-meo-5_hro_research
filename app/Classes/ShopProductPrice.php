<?php

namespace App\Classes;

class ShopProductPrice {
	private $product;
	private $request;
	private $shop;

	public function __construct($product, $shop)
	{
		$this->product = $product;
		$this->shop = $shop;

		$this->retail_price = $this->calculateRetailPrice();
		$this->tax_price = $this->calculateTaxes();
		$this->list_price = $this->calculateListPrice();
		$this->discount_price = $this->calculateDiscount();
	}

	private function calculateRetailPrice()
	{
		if($this->product->shop_price && $this->product->shop_product)
		{
			$retail_price = $this->product->shop_price->retail_price;
			$retail_price += $this->product->shop_product->additional_fee_per_item;

			return $retail_price;
		}

		if($this->product->product_price)
		{
			return $this->product->product_price->retail_price;
		}
		
		return 0;
	}

	private function calculateListPrice()
	{
		if($this->product->shop_price)
		{
			$list_price = $this->product->shop_price->list_price;
		}
		elseif($this->product->product_price)
		{
			$list_price = $this->product->product_price->list_price;
		}else{
			$list_price = 0.0;
		}
		return (float)$list_price;
	}

	private function calculateDiscount()
	{
		$amount = 0;

		$catalog_price_rules = $this->product->catalog_price_rules;

		foreach($catalog_price_rules as $rule)
		{
			if($rule->add_or_subtract)
			{
				$amount += $this->getDiscountAmount($rule);
			}
			else
			{
				$amount -= $this->getDiscountAmount($rule);
			}

			$this->list_price = $this->retail_price;
			$this->retail_price += $amount;

			$this->insertDiscount($rule, $amount);
		}

		return $amount;
	}

	private function calculateTaxes()
	{
		$this->getProductTax();
		$shop_tax = $this->getShopTax();

		return $shop_tax;
	}

	private function getProductTax()
	{
		if($this->product->shop_product && $this->product->shop_product->tax_rule)
		{
			$tax = $this->getTaxPrice($this->product->shop_product->tax_rule);
			//$this->retail_price += $tax; ///TODO check

			$this->insertTax($this->product->shop_product->tax_rule, $tax);

			return $tax;
		}

		return 0;
	}

	private function getShopTax()
	{
		if($this->shop->tax_rule)
		{
			if(!$this->product->shop_info)
			{
				$this->retail_price += $this->retail_price;
				$tax_rule = collect();
				$tax_rule->name = 'Unknown tax rule';
				$tax = 0.21;

				$this->insertTax($tax_rule, 0);

				return 0;
			}

			$tax = $this->getTaxPrice($this->product->shop_info->shop->tax_rule);
			$this->retail_price += $tax;
			$this->insertTax($this->product->shop_info->shop->tax_rule, $tax);

			return $tax;
		}

		return 0;
	}

	private function getTaxPrice($tax_rule)
	{
		$tax = 0;

		if($tax_rule->fixed_price)
		{
			$tax += floatval($tax_rule->fixed_price);
		}

		if($tax_rule->percentage)
		{
			$tax += (float)(($this->retail_price / 100) * $tax_rule->percentage);
		}

		return $tax;
	}

	private function getDiscountAmount($rule)
	{
		if($rule->amount_or_percentage)
		{
			return $rule->amount;
		}
		
		return (float)(($this->retail_price / 100) * $rule->amount);
	}

	private function insertDiscount($rule, $amount)
	{
		$this->discounts[] = array(
			"name" => $rule->name,
			"amount" => (float)$amount
        );
	}

	private function insertTax($tax_rule, $tax)
	{
		$this->taxes[] = array(
			"name" => $tax_rule->name,
			"amount" => (float)$tax
        );
	}

	public static function calculatePrice($products, $shop)
	{
		foreach($products as $product)
		{
			$product->prices = new self($product, $shop);
		}

		return $products;
	}
}
