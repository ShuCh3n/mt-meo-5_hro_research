---
title: API Reference

language_tabs:
- bash
- javascript

includes:

search: true

toc_footers:
- <a href='http://github.com/mpociot/documentarian'>Documentation Powered by Documentarian</a>

---
<!-- START_INFO -->
# Info

Welcome to the generated API reference.
[Get Postman Collection](http://cms.dev/docs/public/collection.json)

<!-- END_INFO -->

#general
<!-- START_aebda4740c2da3d664c1f789cb81b497 -->
## public/v1/health

> Example request:

```bash
curl -X GET "http://cms.dev/public/v1/health" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://cms.dev/public/v1/health",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "status": "good"
}
```

### HTTP Request
`GET public/v1/health`

`HEAD public/v1/health`


<!-- END_aebda4740c2da3d664c1f789cb81b497 -->

<!-- START_e0538bd3f0e16d0eab9be4a25bf3a996 -->
## public/v1/thumbnail/{sku}

> Example request:

```bash
curl -X GET "http://cms.dev/public/v1/thumbnail/{sku}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://cms.dev/public/v1/thumbnail/{sku}",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
null
```

### HTTP Request
`GET public/v1/thumbnail/{sku}`

`HEAD public/v1/thumbnail/{sku}`


<!-- END_e0538bd3f0e16d0eab9be4a25bf3a996 -->

<!-- START_44e84c557b16f3d25f429901e77f73d9 -->
## public/v1/thumbnail/{sku}/shop/{shop_id}

> Example request:

```bash
curl -X GET "http://cms.dev/public/v1/thumbnail/{sku}/shop/{shop_id}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://cms.dev/public/v1/thumbnail/{sku}/shop/{shop_id}",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
null
```

### HTTP Request
`GET public/v1/thumbnail/{sku}/shop/{shop_id}`

`HEAD public/v1/thumbnail/{sku}/shop/{shop_id}`


<!-- END_44e84c557b16f3d25f429901e77f73d9 -->

<!-- START_6da3fc7ca400a452247895fe718ad51c -->
## public/v1/image/{sku}

> Example request:

```bash
curl -X GET "http://cms.dev/public/v1/image/{sku}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://cms.dev/public/v1/image/{sku}",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
[
    {
        "img": "https:\/\/storage.googleapis.com\/tti-cdn\/cdn\/images\/not_found.png",
        "thumb": "https:\/\/storage.googleapis.com\/tti-cdn\/cdn\/images\/not_found.png"
    }
]
```

### HTTP Request
`GET public/v1/image/{sku}`

`HEAD public/v1/image/{sku}`


<!-- END_6da3fc7ca400a452247895fe718ad51c -->

<!-- START_346ea74be08b20d96f6d4710ccb92c96 -->
## public/v1/image/{sku}/shop/{shop_id}

> Example request:

```bash
curl -X GET "http://cms.dev/public/v1/image/{sku}/shop/{shop_id}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://cms.dev/public/v1/image/{sku}/shop/{shop_id}",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
[
    {
        "img": "https:\/\/storage.googleapis.com\/tti-cdn\/cdn\/images\/not_found.png",
        "thumb": "https:\/\/storage.googleapis.com\/tti-cdn\/cdn\/images\/not_found.png"
    }
]
```

### HTTP Request
`GET public/v1/image/{sku}/shop/{shop_id}`

`HEAD public/v1/image/{sku}/shop/{shop_id}`


<!-- END_346ea74be08b20d96f6d4710ccb92c96 -->

<!-- START_e6c4e1c72d5dd94c904f24a46d24c93e -->
## public/v1/image/{sku}/shop/{shop_id}/available

> Example request:

```bash
curl -X GET "http://cms.dev/public/v1/image/{sku}/shop/{shop_id}/available" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://cms.dev/public/v1/image/{sku}/shop/{shop_id}/available",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
null
```

### HTTP Request
`GET public/v1/image/{sku}/shop/{shop_id}/available`

`HEAD public/v1/image/{sku}/shop/{shop_id}/available`


<!-- END_e6c4e1c72d5dd94c904f24a46d24c93e -->

