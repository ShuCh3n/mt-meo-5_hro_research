<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Product;
use App\Shop;
use App\Image;

class PublicController extends Controller
{
    public function getThumbnail($sku, $shop = null){
        $not_found = redirect(env('CDN_URI') . '/images/not_found.png');
        $product = Product::where('sku', $sku)->first();
        if(!$product){
            return $not_found;
        }
        $image = collect($product->allimages($shop))->first();
        if($image){
            return redirect(env('CDN_URI') . '/images/' . $image->filename . '_thumb.png');
        }
        return $not_found;
    }

    public function getImageAvailability($sku, $shop = null){
        $product = Product::where('sku', $sku)->first();
        if(!$product){
            return response('Not found', 404);
        }
        $image = collect($product->allimages($shop))->first();
        if(!$image){
            return response('Not found', 404);
        }
        return response('Found', 200);
    }

    public function getImage($sku, $shop = null){
        $not_found = [['img' => env('CDN_URI') . '/images/not_found.png', 'thumb' => env('CDN_URI') . '/images/not_found.png']];
        if($shop){
            $shopmodel = Shop::find($shop);
            if($shopmodel->default_image_id){
                $image = Image::find($shopmodel->default_image_id);
                $not_found = [
                    'img' => env('CDN_URI') . '/images/' . $image->filename . '.png',
                    'thumb' => env('CDN_URI') . '/images/' . $image->filename . '_thumb.png'
                ];
            }
        }

        $product = Product::where('sku', $sku)->first();
        if(!$product){
            return $not_found;
        }
        $imagesSource = Product::where('sku', $sku)->firstOrFail()->allimages($shop);
        $images = [];
        foreach($imagesSource as $image)
        {
            $images[] = [
                'img' => env('CDN_URI') . '/images/' . $image->filename . '.png',
                'thumb' => env('CDN_URI') . '/images/' . $image->filename . '_thumb.png'
            ];
        }
        if(count($images) > 0){
            return $images;
        }
        return $not_found;
    }
}
