@extends('dashboard.layout.main')

@section('stylesheet')
    <link href="/css/plugins/iCheck/custom.css" rel="stylesheet">
@stop

@section('javascript')
    <!-- iCheck -->
    <script src="/js/plugins/iCheck/icheck.min.js"></script>

    <script>
        $(function(){
            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });
        });
    </script>
@stop

@section('content')
	<div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>{{ trans('pages.inquiry_detail_header') }}</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('home') }}">{{ trans('menu.home') }}</a>
                </li>
                <li>
                    <a href="">{{ trans('menu.inquiries') }}</a>
                </li>
                <li class="active">
                    <strong>{{ trans('page.inquiry') }}</strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">
        {!! session()->has('success')? '<div class="alert alert-success alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>' . session('success') . '</div>' : '' !!}

        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <form id="profileForm" method="post" class="form-horizontal">
            <div class="row">
                <div class="col-md-8">
                    <div class="ibox">

                        <div class="ibox-title">
                            <h5>New Address</h5>
                        </div>

                        <div class="ibox-content">
                            <div class="mb20"></div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Customer</label>
                                        <div class="col-md-10">
                                            @if($customers->count() > 0)
                                                <select class="form-control select2" name="customer">
                                                    <option value=""></option>
                                                    @foreach($customers as $customer)
                                                        <option value="{{ $customer->id }}" {{ ($customer_id == $customer->id)? 'selected' : ''}}>{{ $customer->id }} {{ $customer->firstname }} {{ $customer->lastname }}</option>
                                                    @endforeach
                                                </select>
                                            @else
                                                <p class="form-control-static">
                                                    <a href="{{ route('new_customer') }}">Create first customer</a>
                                                </p>
                                            @endif
                                        </div>
                                    </div>

                                	<div class="form-group">
                                        <label class="col-md-2 control-label">Name</label>
                                        <div class="col-md-10"><input type="text" class="form-control" name="name" placeholder="Address Label Name"></div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Default Address</label>
                                        <div class="col-md-5">
                                            <label><input type="radio" class="form-control i-checks" name="default_address" value="1"> Yes</label>
                                        </div>
                                        <div class="mb5-sm"></div>
                                        <div class="col-md-5">
                                            <label><input type="radio" class="form-control i-checks" name="default_address" value="0"> No</label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Company</label>
                                        <div class="col-md-10">
                                            <input type="text" class="form-control" name="company" placeholder="Company" value="">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Name</label>
                                        <div class="col-md-5">
                                            <input type="text" class="form-control" name="firstname" placeholder="Firstname" value="">
                                        </div>
                                        <div class="mb5-sm"></div>
                                        <div class="col-md-5">
                                            <input type="text" class="form-control" name="lastname" placeholder="Lastname" value="">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Phonenumber</label>
                                        <div class="col-md-5">
                                            <input type="text" class="form-control" name="mobile_phone" placeholder="Mobile Phone" value="">
                                        </div>
                                        <div class="mb5-sm"></div>
                                        <div class="col-md-5">
                                            <input type="text" class="form-control" name="home_phone" placeholder="Home Phone" value="">
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="ibox">

                        <div class="ibox-title">
                            <h5>Shipping Address</h5>
                        </div>

                        <div class="ibox-content">
                            <div class="mb20"></div>

                            <div class="row">
                                <div class="col-md-12">
									<div class="form-group">
										<label class="col-md-2 control-label">Street</label>
										<div class="col-md-5">
											<input type="text" class="form-control" name="street" placeholder="Street" value="">
										</div>
										<div class="mb5-sm"></div>
										<div class="col-md-5">
											<input type="text" class="form-control" name="housenumber" placeholder="Housenumber" value="">
										</div>
									</div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">City / Zipcode</label>
                                        <div class="col-md-5">
                                            <input type="text" class="form-control" name="city" placeholder="City" value="">
                                        </div>
                                        <div class="mb5-sm"></div>
                                        <div class="col-md-5">
                                            <input type="text" class="form-control" name="zipcode" placeholder="Zipcode" value="">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Country</label>
                                        <div class="col-md-10">
                                            <select class="form-control countries" name="country">
                                                <option value=""></option>
                                                @foreach($countries as $country)
                                                    <option value="{{ $country->id }}" icon="{{ $country->icon }}">{{ $country->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">State</label>
                                        <div class="col-md-10">
                                            <input type="text" class="form-control" name="state" placeholder="State" value="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="ibox">
                        <div class="ibox-title">
                            <h5>Action</h5>
                        </div>

                        <div class="ibox-content">
                            {{ csrf_field() }}
                            <button class="btn btn-block btn-outline btn-success" type="submit"><i class="fa fa-floppy-o"></i> Save</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>

        <div class="mb30"></div>
    </div>
@stop