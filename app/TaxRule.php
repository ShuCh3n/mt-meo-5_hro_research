<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TaxRule extends Model
{
    protected $hidden = ['created_at', 'updated_at'];
    
    public static function getRule($input)
    {
    	if(is_string($input))
    	{
    		$country = Country::where('name', $input)->first();

    		if($country)
    		{
    			return $country->getDefaultTaxRule();
    		}
    	}

    	return false;
    }

    public function countries()
    {
        return $this->belongsToMany(Country::class);
    }

    public function tax_group()
    {
        return $this->belongsTo(TaxGroup::class);
    }

    public function states()
    {
        return $this->belongsToMany(State::class);
    }

    public function shop_tax_rule()
    {
        return $this->hasManyThrough(Product::class, Shop::class);
    }

    public function scopeActive($query)
    {
        return $query->where('enable', 1);
    }

    public function setTaxRule($request)
    {
        $this->enable = $request->enable;
        $this->name = $request->name;
        $this->fixed_price = (!empty($request->fixed_price))? $request->fixed_price : null;
        $this->percentage = (!empty($request->percentage))? $request->percentage : null;
        $this->save();

        $this->countries()->sync($request->countries);

        if($request->states)
        {
            $this->states()->sync($request->states);
        }
    }

    public function detachCountry()
    {
        $this->countries()->detach();
    }

    public function detachState()
    {
        $this->states()->detach();
    }
}
