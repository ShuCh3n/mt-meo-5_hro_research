<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Country;
use App\Shop;
use App\ProductFeatureType;
use App\CatalogPriceRule;
use App\CustomerGroup;
use App\CartRule;

class PriceRuleController extends Controller
{
    public function cartRules()
    {
    	$cart_rules = CartRule::all();
    	return view('dashboard.cart_rules', compact('cart_rules'));
    }

    public function cataglogPriceRules()
    {
    	$price_rules = CatalogPriceRule::all();
    	return view('dashboard.cataglog_price_rules', compact('price_rules'));
    }

    public function newCatalogPriceRule()
    {
        $countries = Country::all();
        $shops = Shop::all();
        $product_feature_types = ProductFeatureType::all();
        $customer_groups = CustomerGroup::all();

    	return view('dashboard.new_catalog_price_rule', compact('countries', 'shops', 'product_feature_types', 'customer_groups'));
    }

    public function actionNewCatalogPriceRule()
    {
    	$catalog_price_rule = new CatalogPriceRule;
    	$catalog_price_rule->setCatalogPriceRule($this->request);

        if($this->request->countries)
        {
            $catalog_price_rule->syncCountries($this->request->countries);
        }

        if($this->request->customer_groups)
        {
            $catalog_price_rule->syncCustomerGroups($this->request->customer_groups);
        }

        if($this->request->products)
        {
            $catalog_price_rule->syncProducts($this->request->products);
        }

        if($this->request->feature_types)
        {
            $catalog_price_rule->syncFeatureTypes($this->request->feature_types);
        }
    	
        if($this->request->categories)
        {
            $catalog_price_rule->syncCategories($this->request->categories);
        }
    	
        if($this->request->shops)
        {
            $catalog_price_rule->syncShops($this->request->shops);
        }

        return redirect()->route('catalog_price_rules');
    }

    public function editCatalogPriceRule()
    {
        $catalog_price_rule = CatalogPriceRule::findOrFail($this->request->id);
        $countries = Country::all();
        $shops = Shop::all();
        $product_feature_types = ProductFeatureType::all();
        $customer_groups = CustomerGroup::all();

        return view('dashboard.edit_catalog_price_rule', compact('catalog_price_rule', 'countries', 'shops', 'product_feature_types', 'customer_groups'));
    }

    public function actionEditCatalogPriceRule()
    {
        $catalog_price_rule = CatalogPriceRule::findOrFail($this->request->id);
        $catalog_price_rule->setCatalogPriceRule($this->request);

        if(!$this->request->shops)
        {
            $this->request->shops = array();
        }

        if(!$this->request->countries)
        {
            $this->request->countries = array();
        }

        if(!$this->request->customer_groups)
        {
            $this->request->customer_groups = array();
        }

        if(!$this->request->products)
        {
            $this->request->products = array();
        }

        if(!$this->request->feature_types)
        {
            $this->request->feature_types = array();
        }
        
        if(!$this->request->categories)
        {
            $this->request->categories = array();
        }

        $catalog_price_rule->syncCountries($this->request->countries);
        $catalog_price_rule->syncCustomerGroups($this->request->customer_groups);
        $catalog_price_rule->syncProducts($this->request->products);
        $catalog_price_rule->syncFeatureTypes($this->request->feature_types);
        $catalog_price_rule->syncCategories($this->request->categories);
        $catalog_price_rule->syncShops($this->request->shops);

        return redirect()->route('catalog_price_rules');
    }

    public function removeCatalogPriceRule()
    {
        $catalog_price_rule = CatalogPriceRule::findOrFail($this->request->id);
        $catalog_price_rule->delete();

        return array("status" => true);
    }

    public function newCartRules()
    {
        $countries = Country::all();
        $shops = Shop::all();
        $product_feature_types = ProductFeatureType::all();
        $customer_groups = CustomerGroup::all();

        return view('dashboard.new_cart_rule', compact('countries', 'shops', 'product_feature_types', 'customer_groups'));
    }

    public function actionNewCartRules()
    {
        $cart_rule = new CartRule;
        $cart_rule->setCartRule($this->request);

        if($this->request->countries)
        {
            $cart_rule->syncCountries($this->request->countries);
        }

        if($this->request->customer_groups)
        {
            $cart_rule->syncCustomerGroups($this->request->customer_groups);
        }

        if($this->request->customers)
        {
            $cart_rule->syncCustomers($this->request->customers);
        }

        if($this->request->products)
        {
            $cart_rule->syncProducts($this->request->products);
        }

        if($this->request->feature_types)
        {
            $cart_rule->syncFeatureTypes($this->request->feature_types);
        }
        
        if($this->request->categories)
        {
            $cart_rule->syncCategories($this->request->categories);
        }

        return redirect()->route('cart_rules');
    }

    public function bulkPriceRules()
    {
        $bulk_prices = array();
        return view('dashboard.bulk_price_rule', compact('bulk_prices'));
    }

    public function newBulkPriceRule()
    {
        $shops = Shop::all();
        return view('dashboard.new_bulk_price_rule', compact('shops'));
    }

    public function actionNewBulkPriceRule()
    {
        // if($this->request->shop == "all")
        // {
        //     foreach()
        //     {
                
        //     }
        // }
        // $bulk_price = new ProductBulkPrice;
        // $bulk_price->setBulkPrice($this->request);

        dd($this->request);
    }

    public function wholesalePriceRules()
    {
        return view('dashboard.wholesale_price_rule');
    }
}
