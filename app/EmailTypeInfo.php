<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmailTypeInfo extends Model
{
	public function email_type()
	{
		return $this->belongsTo(EmailType::class);
	}

    public function scopeBackOffice($query)
    {
    	$query->where('language_id', auth()->user()->preferenceLanguage()->id);
    }
}
