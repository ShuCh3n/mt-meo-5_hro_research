<?php

namespace App\Http\Middleware;

use Closure;
use App\CustomerSession;

class CheckCustomer
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->header('CustomerToken'))
        {
            $request->customer_session = CustomerSession::firstOrNew([
                'session_token' => $request->header('CustomerToken')
            ]);
            $request->customer_session->ip = $request->ip();
            $request->customer_session->checked_out = false;
            $request->customer_session->save();

            if($request->header('User'))
            {
                $request->customer_session->customer_id = $request->header('User');
                $request->customer_session->save();
            }

            return $next($request);
        }

        if(env('API_DEBUG'))
        {
            $request->customer_session = CustomerSession::where('session_token', env('DEBUG_CUSTOMER_SESSION'))->firstOrCreate(['session_token' => env('DEBUG_CUSTOMER_SESSION'), 'ip' => $request->ip(), 'checked_out' => false]);

            return $next($request);
        }

        return response(array('status' => false));
    }
}
