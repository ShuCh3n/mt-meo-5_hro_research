<?php
namespace App\Classes\SyncFactory;

use App\Classes\SyncFactory\Shops\Bandenbestellen;

interface iOrder
{
	public function setOrder(\App\Order $order);
	public function setShopRule(\App\Shop $shop);
}

class Order implements iOrder
{
	public function setOrder(\App\Order $order)
	{
		$this->order = $order;
		return $this;
	}

	public function setShopRule(\App\Shop $shop)
	{
		switch ($shop->name) {
			case 'Banden Bestellen':
				$this->shop = new Bandenbestellen($shop);
				break;
			default:
				# code...
				break;
		}

		return $this;
	}

	public function getFormatedOrder()
	{
		$this->order = $this->shop->setOrder($this->order)->getFormatedOrder();
		return $this;
	}
}