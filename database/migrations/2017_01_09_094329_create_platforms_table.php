<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlatformsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('platforms', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code')->unique();
            $table->string('name');
            $table->integer('priority');
            $table->boolean('b2b')->default(false);
            $table->text('settings');
            $table->timestamps();
        });

        DB::table('platforms')->insert([
            [
                'code'          => str_random(10),
                'name'          => '07zr',
                'priority'      => 10,
                'b2b'           => true,
                'settings'      => '{}'
            ],
            [
                'code'          => str_random(10),
                'name'          => 'Allopneus',
                'priority'      => 20,
                'b2b'           => true,
                'settings'      => '{}'
            ],
            [
                'code'          => str_random(10),
                'name'          => 'Boxstop',
                'priority'      => 30,
                'b2b'           => true,
                'settings'      => '{}'
            ],
            [
                'code'          => str_random(10),
                'name'          => 'Deldo',
                'priority'      => 40,
                'b2b'           => true,
                'settings'      => '{}'
            ],
            [
                'code'          => str_random(10),
                'name'          => 'Hanse',
                'priority'      => 50,
                'b2b'           => true,
                'settings'      => '{}'
            ],
            [
                'code'          => str_random(10),
                'name'          => 'Hoco',
                'priority'      => 60,
                'b2b'           => true,
                'settings'      => '{}'
            ],
            [
                'code'          => str_random(10),
                'name'          => 'Hohl',
                'priority'      => 70,
                'b2b'           => true,
                'settings'      => '{}'
            ],
            [
                'code'          => str_random(10),
                'name'          => 'Interpneus',
                'priority'      => 80,
                'b2b'           => true,
                'settings'      => '{}'
            ],
            [
                'code'          => str_random(10),
                'name'          => 'Krieg',
                'priority'      => 90,
                'b2b'           => true,
                'settings'      => '{}'
            ],
            [
                'code'          => str_random(10),
                'name'          => 'Krupp',
                'priority'      => 100,
                'b2b'           => true,
                'settings'      => '{}'
            ],
            [
                'code'          => str_random(10),
                'name'          => 'Seng',
                'priority'      => 110,
                'b2b'           => true,
                'settings'      => '{}'
            ],
            [
                'code'          => str_random(10),
                'name'          => 'Shtyre',
                'priority'      => 120,
                'b2b'           => true,
                'settings'      => '{}'
            ],
            [
                'code'          => str_random(10),
                'name'          => 'Straub',
                'priority'      => 130,
                'b2b'           => true,
                'settings'      => '{}'
            ],
            [
                'code'          => str_random(10),
                'name'          => 'Tyre24',
                'priority'      => 140,
                'b2b'           => true,
                'settings'      => '{}'
            ],
            [
                'code'          => str_random(10),
                'name'          => 'Tyredis',
                'priority'      => 150,
                'b2b'           => true,
                'settings'      => '{}'
            ],
            [
                'code'          => str_random(10),
                'name'          => 'Wolf',
                'priority'      => 160,
                'b2b'           => true,
                'settings'      => '{}'
            ]
        ]);

        Schema::create('platform_product_prices', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('platform_id')->unsigned();
            $table->integer('product_id')->unsigned();
            $table->decimal('price', 10, 2);
            $table->timestamps();

            $table->foreign('platform_id')
                ->references('id')
                ->on('platforms')
                ->onDelete('cascade');

            $table->foreign('product_id')
                ->references('id')
                ->on('products')
                ->onDelete('cascade');
        });

        Schema::create('platform_product', function (Blueprint $table) {
            $table->integer('platform_id')->unsigned();
            $table->integer('product_id')->unsigned();
            $table->string('article_code')->nullable();

            $table->foreign('platform_id')
                ->references('id')
                ->on('platforms')
                ->onDelete('cascade');

            $table->foreign('product_id')
                ->references('id')
                ->on('products')
                ->onDelete('cascade');

            $table->primary(['platform_id', 'product_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('platform_product');
        Schema::dropIfExists('platform_product_prices');
        Schema::dropIfExists('platforms');
    }
}
