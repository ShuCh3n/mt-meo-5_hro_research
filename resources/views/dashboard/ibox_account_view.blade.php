@extends('dashboard.layout.main')

@section('content')
	<div class="wrapper wrapper-content animated fadeInRight ecommerce">
        <form id="profileForm" method="post" class="form-horizontal">
            <div class="row">
            	<account-view></account-view>
            </div>
        </form>
    </div>
@stop

@section('vue_templates')
	<script type="text/x-template" id="acccount-view">
        <div>
            <div class="col-lg-8">
                @foreach($settings as $setting)
                    <account-view-formule-box 
                        countries="{{ $countries }}" 
                        platform_options="{{ $platforms }}"
                        platform_selected="{{ $setting->platform_id }}"
                        country_selected="{{ $setting->country_id }}"
                        settings="{{ $setting->settings }}"
                        box_id="{{ $setting->id }}"
                    ></account-view-formule-box>
                @endforeach

                <account-view-formule-box countries="{{ $countries }}" platform_options="{{ $platforms }}" v-for="y in x" key="{{ str_random(5) }}"></account-view-formule-box>
            </div>
            <div class="col-lg-4">
                <div class="ibox">
                    <div class="ibox-title">
                        <h5>Action</h5>
                    </div>
                    <div class="ibox-content">

                        <div class="row">
                            <div class="col-md-12">
                                {{ csrf_field() }}
                                <button class="btn btn-block btn-outline btn-success" type="submit"><i class="fa fa-floppy-o"></i> Save</button>
                                <button type="button" class="btn btn-block btn-outline btn-primary" @click='add'><i class="fa fa-plus"></i> Add Formule</button>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </script>
@stop