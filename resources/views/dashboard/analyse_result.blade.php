@extends('dashboard.layout.main')

@section('content')
	<div class="wrapper wrapper-content animated fadeInRight ecommerce">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Analist</h5>
                        <a class="btn btn-primary pull-right btn-xs" href="{{ route('analyze', request()->input()) }}"><i class="fa fa-file-excel-o"></i> Download Excel</a> 
                    </div>
                    <div class="ibox-content">
  
                        <table class="table table-striped table-hover">
                            <thead>
                            <tr>
                                <th>EAN</th>
                                <th>Product Code</th>
                                <th>Article Code</th>
                                <th>Name</th>
                                <th>Supplier Price</th>
                                <th>Target Price</th>
                                <th>Cheapest Price</th>
                                <th>Purchasing Price</th>
                                <th>Difference</th>
                                <th>Price Status</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($analyzer->items as $item)
                                    <tr>
                                        <td>{{ $item->product->ean ?? '-' }}</td>
                                        <td>{{ $item->product->supplier_sku ?? '-' }}</td>
                                        <td>{{ $item->product->sku ?? '-' }}</td>
                                        <td><a href="{{ route('view_product', $item->product->id) }}">{{ $item->name ?? $item->product->info->name }}</a></td>
                                        <td>{{ price($item->price ?? 0) }}</td>
                                        <td>{{ price($item->target_price ?? 0) }}</td>
                                        <td>{{ price($item->cheapest_price->price ?? 0) }}</td>
                                        <td class="text-warning">{{ price($item->product->product_price()->latest()->first()->purchasing_price ?? 0) }}</td>
                                        <td>{{ ($item->percentage_difference)? $item->percentage_difference . '%'  : '-' }}</td>
                                        <td>
                                            @if($item->deal == 1)
                                                <span class='label label-primary'>Deal</span>
                                            @endif

                                            @if($item->deal == 2)
                                                <span class='label label-danger'>No Deal</span>
                                            @endif

                                            @if($item->deal == 3)
                                                <span class='label label-warning'>No Info</span>
                                            @endif
                                        </td>
{{--                                         
                                      
          
                                        <td>{{ price($result->cheapest_platform['price']->price ?? 0) }}</td>
                                        <td class="text-warning">{{ price($result->purchasing_price ?? 0) }}</td>
                                        <td>{{ $result->percentage_difference ?? '-' }} %</td>
                                        <td>
                                            @if($result->status == 1)
                                                <span class='label label-primary'>Deal</span>
                                            @endif

                                            @if($result->status == 2)
                                                <span class='label label-danger'>No Deal</span>
                                            @endif

                                            @if($result->status == 3)
                                                <span class='label label-warning'>No Info</span>
                                            @endif
                                        </td> --}}
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop