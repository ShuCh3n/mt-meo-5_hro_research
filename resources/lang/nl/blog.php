<?php
/*************************************************************************
 Generated via "php artisan localization:missing" at 2017/04/10 17:07:06
*************************************************************************/

return array (
  'tags' => 'Tags',
  'action' => 'Actie',
  'active' => 'Actief',
  'blog_post' => 'Blog post',
  'body' => 'Bericht',
  'disabled' => 'Uitgeschakeld',
  'enabled' => 'Ingeschakeld',
  'inactive' => 'Inactief',
  'language' => 'Taal',
  'new_article' => 'Nieuw artikel',
  'new_blog' => 'Nieuw blog',
  'newblog_detail_header' => 'newblog_detail_header',
  'num_articles' => 'Aantal artikelen',
  'release' => 'Ingangsdatum',
  'release_date' => 'release_date',
  'save' => 'Opslaan',
  'shop' => 'Winkel',
  'short_body' => 'Korte omschrijving',
  'title' => 'Titel',
);
