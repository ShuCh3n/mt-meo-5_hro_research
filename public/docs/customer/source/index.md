---
title: API Reference

language_tabs:
- bash
- javascript

includes:

search: true

toc_footers:
- <a href='http://github.com/mpociot/documentarian'>Documentation Powered by Documentarian</a>
---
<!-- START_INFO -->
# Info

Welcome to the generated API reference.
[Get Postman Collection](http://cms.dev/docs/customer/collection.json)

<!-- END_INFO -->

#general
<!-- START_65b4e851ded7d0511f3e420100281529 -->
## customerapi/v1/health

> Example request:

```bash
curl -X GET "http://cms.dev/customerapi/v1/health" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://cms.dev/customerapi/v1/health",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "status": "good"
}
```

### HTTP Request
`GET customerapi/v1/health`

`HEAD customerapi/v1/health`


<!-- END_65b4e851ded7d0511f3e420100281529 -->

<!-- START_815334aca750ba8c9bc370ad859f82f1 -->
## customerapi/v1/product/{id}

> Example request:

```bash
curl -X GET "http://cms.dev/customerapi/v1/product/{id}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://cms.dev/customerapi/v1/product/{id}",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "id": 1,
    "shop_id": 3,
    "product_id": 1,
    "tax_rule_id": null,
    "minimum_qty": 1,
    "stock_management": "0",
    "active_when_out_of_stock": "0",
    "deny_order_when_out_of_stock": "0",
    "additional_fee_per_item": "0.00",
    "active": "1",
    "created_at": null,
    "updated_at": "2017-01-25 13:49:14",
    "images": [],
    "product": {
        "id": 1,
        "brand_id": null,
        "product_feature_type_id": 1,
        "product_group_id": 135,
        "tax_rule_id": null,
        "supplier_sku": null,
        "ean": null,
        "upc": "upc",
        "sku": "BSMONTLM",
        "minimum_qty": 1,
        "stock_management": "1",
        "active_when_out_of_stock": "1",
        "deny_order_when_out_of_stock": "1",
        "width": "0.00",
        "height": "0.00",
        "depth": "0.00",
        "weight": "0.00",
        "additional_fee_per_item": "0.00",
        "active": "0",
        "imported": "0",
        "prices": {
            "shop": {
                "id": 3,
                "tax_rule_id": 4,
                "logo": null,
                "name": "Banden Bestellen",
                "link": "http:\/\/bandenbestellen.nl",
                "tax_rule": {
                    "id": 4,
                    "tax_group_id": 1,
                    "enable": "1",
                    "code": "1N",
                    "name": "Te betalen BTW hoog VK-NL",
                    "fixed_price": null,
                    "percentage": "21.00"
                }
            },
            "retail_price": 0,
            "taxes": [
                {
                    "name": "Unknown tax rule",
                    "amount": 0
                }
            ],
            "tax_price": 0,
            "list_price": 0,
            "discount_price": 0
        },
        "logo": [],
        "category_id": 0,
        "brand": null,
        "shop_products": [
            {
                "id": 1,
                "shop_id": 3,
                "product_id": 1,
                "tax_rule_id": null,
                "minimum_qty": 1,
                "stock_management": "0",
                "active_when_out_of_stock": "0",
                "deny_order_when_out_of_stock": "0",
                "additional_fee_per_item": "0.00",
                "active": "1",
                "created_at": null,
                "updated_at": "2017-01-25 13:49:14",
                "tax_rule": null
            }
        ],
        "shop_price": null,
        "shop_infos": [],
        "catalog_price_rules": [],
        "product_feature_options": [],
        "images": [],
        "product_price": {
            "id": 186795,
            "product_id": 1,
            "retail_price": 0,
            "list_price": "0.00",
            "unit_name": "unit name",
            "unit_price": "0.00"
        },
        "groups": [],
        "categories": []
    }
}
```

### HTTP Request
`GET customerapi/v1/product/{id}`

`HEAD customerapi/v1/product/{id}`


<!-- END_815334aca750ba8c9bc370ad859f82f1 -->

<!-- START_834d992dd2de4adb223dc0ddae029ad7 -->
## customerapi/v1/products/search

> Example request:

```bash
curl -X POST "http://cms.dev/customerapi/v1/products/search" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://cms.dev/customerapi/v1/products/search",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST customerapi/v1/products/search`


<!-- END_834d992dd2de4adb223dc0ddae029ad7 -->

<!-- START_5b5f09a3dc0e6693e56a01a2ffdc1fad -->
## customerapi/v1/categories

> Example request:

```bash
curl -X GET "http://cms.dev/customerapi/v1/categories" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://cms.dev/customerapi/v1/categories",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
null
```

### HTTP Request
`GET customerapi/v1/categories`

`HEAD customerapi/v1/categories`


<!-- END_5b5f09a3dc0e6693e56a01a2ffdc1fad -->

<!-- START_f6ed2821363aeeb9f0f6f08d7e3fac05 -->
## customerapi/v1/category/{id}/products

> Example request:

```bash
curl -X GET "http://cms.dev/customerapi/v1/category/{id}/products" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://cms.dev/customerapi/v1/category/{id}/products",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
null
```

### HTTP Request
`GET customerapi/v1/category/{id}/products`

`HEAD customerapi/v1/category/{id}/products`


<!-- END_f6ed2821363aeeb9f0f6f08d7e3fac05 -->

<!-- START_48cf895fb86abb7c517f4c6fdbbef78e -->
## customerapi/v1/shop

> Example request:

```bash
curl -X GET "http://cms.dev/customerapi/v1/shop" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://cms.dev/customerapi/v1/shop",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "status": true,
    "shop": {
        "id": 3,
        "tax_rule_id": 4,
        "logo": null,
        "name": "Banden Bestellen",
        "link": "http:\/\/bandenbestellen.nl",
        "tax_rule": {
            "id": 4,
            "tax_group_id": 1,
            "enable": "1",
            "code": "1N",
            "name": "Te betalen BTW hoog VK-NL",
            "fixed_price": null,
            "percentage": "21.00"
        }
    }
}
```

### HTTP Request
`GET customerapi/v1/shop`

`HEAD customerapi/v1/shop`


<!-- END_48cf895fb86abb7c517f4c6fdbbef78e -->

<!-- START_ffcbf3fd5ed8b71cf3dd438d6b6c4b55 -->
## customerapi/v1/vehicle/{vehicle}

> Example request:

```bash
curl -X GET "http://cms.dev/customerapi/v1/vehicle/{vehicle}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://cms.dev/customerapi/v1/vehicle/{vehicle}",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
null
```

### HTTP Request
`GET customerapi/v1/vehicle/{vehicle}`

`HEAD customerapi/v1/vehicle/{vehicle}`


<!-- END_ffcbf3fd5ed8b71cf3dd438d6b6c4b55 -->

<!-- START_99d6815dea68262f52771d8e9050331a -->
## customerapi/v1/vehicle/{vehicle}/{type}

> Example request:

```bash
curl -X GET "http://cms.dev/customerapi/v1/vehicle/{vehicle}/{type}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://cms.dev/customerapi/v1/vehicle/{vehicle}/{type}",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
null
```

### HTTP Request
`GET customerapi/v1/vehicle/{vehicle}/{type}`

`HEAD customerapi/v1/vehicle/{vehicle}/{type}`


<!-- END_99d6815dea68262f52771d8e9050331a -->

<!-- START_2b89a4a794c808f95c5df5d538f82b34 -->
## customerapi/v1/vehicle/{vehicle}/{type}/{id}

> Example request:

```bash
curl -X GET "http://cms.dev/customerapi/v1/vehicle/{vehicle}/{type}/{id}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://cms.dev/customerapi/v1/vehicle/{vehicle}/{type}/{id}",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
null
```

### HTTP Request
`GET customerapi/v1/vehicle/{vehicle}/{type}/{id}`

`HEAD customerapi/v1/vehicle/{vehicle}/{type}/{id}`


<!-- END_2b89a4a794c808f95c5df5d538f82b34 -->

