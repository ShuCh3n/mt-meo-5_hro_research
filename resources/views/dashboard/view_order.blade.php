@extends('dashboard.layout.main')

@section('content')
	<div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>{{ trans('pages.inquiry_detail_header') }}</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('home') }}">{{ trans('menu.home') }}</a>
                </li>
                <li>
                    <a href="">{{ trans('menu.inquiries') }}</a>
                </li>
                <li class="active">
                    <strong>{{ trans('page.inquiry') }}</strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">
        <form method="post" class="form-horizontal">
            <div class="row">
                <div class="col-md-8">
                    <div class="ibox">

                        <div class="ibox-title">
                            <h5># {{ $order->id }}</h5>
                        </div>

                        <div class="ibox-content">
                            <div class="mb20"></div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Customer</label>
                                        <div class="col-md-5">
                                            <p class="form-control-static">
                                                <a href="{{ route('view_customer', $order->customer->id) }}">{{ $order->customer->firstname . ' ' . $order->customer->lastname }}</a>
                                            </p>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Reference</label>
                                        <div class="col-md-5">
                                            <p class="form-control-static">{{ $order->reference }}</p>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Order placed</label>
                                        <div class="col-md-5">
                                            <p class="form-control-static">{{ $order->created_at }}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="ibox">

                        <div class="ibox-title">
                            <h5>Order Status</h5>
                        </div>

                        <div class="ibox-content">
                            <div class="mb20"></div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div id="vertical-timeline" class="vertical-container">
                                        <div class="row">
                                            <div class="col-sm-5">
                                                <input type="text" name="search" value="" placeholder="# Trackingnumber" class="form-control">
                                            </div>
                                            <div class="mb5-sm"></div>
                                            <div class="col-sm-5">
                                                <select2 class="form-control">
                                                    @foreach($order_statuses as $status)
                                                        <option value="{{ $status->id }}">{{ $status->name }}</option>
                                                    @endforeach
                                                </select2>
                                            </div>
                                            <div class="mb5-sm"></div>
                                            <div class="col-sm-2">
                                                <button class="btn btn-w-m btn-block btn-primary" type="button">Update</button>
                                            </div>
                                        </div>
                                    </div>

                                	<div id="vertical-timeline" class="vertical-container dark-timeline">
                                		<div class="vertical-timeline-block">
                                			<div class="vertical-timeline-icon navy-bg">
                                				<i class="fa fa-briefcase"></i>
                                			</div>

                                			<div class="vertical-timeline-content">
                                				<h2>Dummy</h2>
                                			</div>
                                		</div>
                                	</div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="ibox">
                    	<div class="ibox-title">
                    		<span class="pull-right">(<strong>{{ $order->lines->count() }}</strong>) items</span>
                    		<h5>Products</h5>
                    	</div>

                        @foreach($order->lines as $order_line)
                            <div class="ibox-content">
                            	<div class="table-responsive">
                            		<table class="table shoping-cart-table">

                            			<tbody>
                            				<tr>
                            					<td width="90">
                            						<div class="cart-product-imitation">
                            						</div>
                            					</td>
                            					<td class="desc">
                            						<h3>
                            							<a href="#" class="text-navy">
                            								{{ $order_line->name }}
                            							</a>
                            						</h3>
                            						<p class="small">
                            							There are many variations of passages of Lorem Ipsum available
                            						</p>
                            						<dl class="small m-b-none">
                            							<dt>Description lists</dt>
                            							<dd>List is perfect for defining terms.</dd>
                            						</dl>
                            					</td>

                            					<td>
                            						{{ price($order_line->price) }}
                            					</td>
                            					<td width="65">
                            						QTY {{ $order_line->qty }}
                            					</td>
                            					<td>
                            						<h4>
                            							{{ price($order_line->price * $order_line->qty) }}
                            						</h4>
                            					</td>

                            				</tr>
                            			</tbody>
                            		</table>
                            	</div>
                            </div>
                        @endforeach
                    </div>

                </div>

                <div class="col-md-4">
                    <div class="ibox">
                        <div class="ibox-title">
                            <h5>Action</h5>
                        </div>

                        <div class="ibox-content">
                            <div class="clearfix mb20">
                                <select2>
                                    <option>Message 1</option>
                                    <option>Message 2</option>
                                    <option>Message 3</option>
                                    <option>Message 4</option>
                                </select2>
                            </div>

                            <strong class="mt20">Message</strong>

                            <textarea class="form-control" placeholder="Message to customer" style="height: 200px;"></textarea>

                            <div class="mb20"></div>

                            {{ csrf_field() }}

                            <button class="btn btn-block btn-outline btn-primary" type="submit"><i class="fa fa-comments-o"></i> Send</button>
                        </div>
                    </div>

                    <div class="ibox">
                        <div class="ibox-title">
                            <h5>Action</h5>
                        </div>

                        <div class="ibox-content">
                        	<strong>Note</strong>
                        	<textarea class="form-control" placeholder="Private notes" style="height: 200px;"></textarea>

                        	<div class="mb20"></div>

                            {{ csrf_field() }}

                            <button class="btn btn-block btn-outline btn-success" type="submit"><i class="fa fa-floppy-o"></i> Save</button>
                            <a class="btn btn-block btn-outline btn-warning" type="submit"><i class="fa fa-envelope-o" aria-hidden="true"></i> Send message</a>
                        </div>
                    </div>
                </div>
            </div>
        </form>    
    </div>
@stop