<?php
/*************************************************************************
 Generated via "php artisan localization:missing" at 2017/04/10 17:07:06 
*************************************************************************/

return array (
  //============================== New strings to translate ==============================//
  // Defined in file /var/www/cms/resources/views/dashboard/new_blog_article.blade.php
  'tags' => 'tags',
  //==================================== Translations ====================================//
  'action' => 'action',
  'active' => 'active',
  'blog_post' => 'blog_post',
  'body' => 'body',
  'disabled' => 'disabled',
  'enabled' => 'enabled',
  'inactive' => 'inactive',
  'language' => 'language',
  'new_article' => 'new_article',
  'new_blog' => 'new_blog',
  'newblog_detail_header' => 'newblog_detail_header',
  'num_articles' => 'num_articles',
  'release' => 'release',
  'release_date' => 'release_date',
  'save' => 'save',
  'shop' => 'shop',
  'short_body' => 'short_body',
  'title' => 'title',
);