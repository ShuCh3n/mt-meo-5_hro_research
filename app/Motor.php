<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Motor extends Model
{
    protected $fillable = ['code', 'vehicle_brand_id', 'name', 'model', 'type', 'cc', 'tyre_front_width', 'tyre_front_height', 'tyre_front_inch', 'tyre_rear_width', 'tyre_rear_height', 'tyre_rear_inch'];

    public function vehicle_brand()
    {
    	return $this->belongsTo(VehicleBrand::class);
    }

    public function vehicle_years()
    {
    	return $this->belongsToMany(VehicleYear::class);//->withPivot('tyre_front_width', 'tyre_front_height', 'tyre_front_inch', 'tyre_rear_width', 'tyre_rear_height', 'tyre_rear_inch');
    }

    public function products()
    {
    	return $this->belongsToMany(Product::class, 'category_motor_product_vehicle_year')
                ->withPivot('category_id', 'vehicle_year_id', 'month', 'sku', 'min_qty');
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class, 'category_motor_product_vehicle_year')
                ->withPivot('product_id', 'vehicle_year_id', 'month', 'sku', 'min_qty');
    }

    public function category()
    {
        return $this->belongsTo(VehicleCategory::class);
    }

    public function scopeFilterVehicleBrand($query, $brands)
    {
        $query->whereIn('code', $brands);
    }

    public function scopeFilterVehicleName($query, $name)
    {
        return $query->whereHas('vehicle_brand', function($query) use ($name){
            $query->whereIn('name', $name);
        });
    }

    public function scopeFilterVehicleYear($query, $years)
    {
        return $query->whereHas('vehicle_years', function($query) use ($years){
            $query->whereIn('year', $years);
        });
    }
}
