<?php

namespace App\Http\Controllers;

use App\webHook;
use Illuminate\Http\Request;

class WebHookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\webHook  $webHook
     * @return \Illuminate\Http\Response
     */
    public function show(webHook $webHook)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\webHook  $webHook
     * @return \Illuminate\Http\Response
     */
    public function edit(webHook $webHook)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\webHook  $webHook
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, webHook $webHook)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\webHook  $webHook
     * @return \Illuminate\Http\Response
     */
    public function destroy(webHook $webHook)
    {
        //
    }
}
