<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\ProductCombinationStock;

class ProductCombination extends Model
{
	public function product_images()
	{
		return $this->belongsToMany(ProductImage::class);
	}

	public function product_attribute_options()
	{
		return $this->belongsToMany(ProductAttributeOption::class);
	}

    public function product_combination_stocks()
    {
        return $this->hasMany(ProductCombinationStock::class);
    }

	public function syncProductImage($images)
    {
    	$this->product_images()->sync($images);
    }

    public function syncProductAttributeOption($options)
    {
    	$this->product_attribute_options()->sync($options);
    }

    public function setProductCombination($product, $combination)
    {
    	$this->product_id = $product->id;
    	$this->default = $combination->default;
    	$this->supplier_sku = $combination->supplier_sku;
    	$this->ean = $combination->ean;
    	$this->upc = $combination->upc;
    	$this->sku = $combination->sku;
    	$this->minimum_qty = $combination->minimum_qty;

    	if($combination->impact_price->type)
    	{
    		$this->impact_price_type = $combination->impact_price->type;
    		$this->impact_price = $combination->impact_price->value;
    	}

    	if($combination->impact_weight->type)
    	{
    		$this->impact_weight_type = $combination->impact_weight->type;
    		$this->impact_weight = $combination->impact_weight->value;
    	}

    	if($combination->impact_unit_price->type)
    	{
    		$this->impact_unit_price_type = $combination->impact_unit_price->type;
    		$this->impact_unit_price = $combination->impact_unit_price->value;
    	}

    	$this->save();

    	if($combination->warehouse_stock)
    	{
    		foreach($combination->warehouse_stock as $stock_info)
    		{
    			$this->newStockInfo($stock_info);
    		}
    	}

    	if($combination->options)
    	{
    		$this->syncProductAttributeOption($combination->options);
    	}

    	if($combination->images)
    	{
    		$this->syncProductImage($combination->images);
    	}
    }

    public function newStockInfo($stock_info)
    {
    	$new_stock_info = new ProductCombinationStock;
    	$new_stock_info->product_combination_id = $this->id;
    	$new_stock_info->warehouse_id = $stock_info->id;
    	$new_stock_info->amount = $stock_info->val;
    	$new_stock_info->save();
    }

    public function getInJson()
    {
        $result = array();
        $result['default'] = ($this->default)? '1' : '0';
        $result['options'] = array();
        $result['options_text'] = array();

        foreach($this->product_attribute_options as $option)
        {
            $result['options'][] = $option->id;
            $result['options_text'][] = $option->product_attribute->multilanguage->info()->name . " : " . $option->multilanguage->info()->name;
        }

        $result['supplier_sku'] = $this->supplier_sku;
        $result['ean'] = $this->ean;
        $result['upc'] = $this->upc;
        $result['sku'] = $this->sku;
        $result['minimum_qty'] = $this->minimum_qty;
        $result['impact_price'] = array("type" => $this->impact_price_type, "value" => $this->impact_price);
        $result['impact_weight'] = array("type" => $this->impact_weight_type, "value" => $this->impact_weight);
        $result['impact_unit_price'] = array("type" => $this->impact_unit_price_type, "value" => $this->impact_unit_price);

        $stocks = $this->product_combination_stocks()->orderBy('created_at', 'desc')->get()->groupBy('warehouse_id');
        $result["warehouse_stock"] = array();

        foreach($stocks as $stock)
        {
            $result["warehouse_stock"][] = array("id" => $stock[0]->id, "val" => $stock[0]->amount);
        }

        $result["images"] = array();
        foreach($this->product_images as $images)
        {
            $result["images"][] = $images->id;
        }

        return json_encode($result);
    }
}
