@php
$noVue = true;
@endphp
@extends('dashboard.layout.main')
@section('javascript')
<script src="/bower_components/angular/angular.min.js"></script>
<script src="/bower_components/angular-filter/dist/angular-filter.min.js"></script>
<script src="/bower_components/angular-upload/angular-upload.min.js"></script>
<script>
  var cmsApp = angular.module('cmsApp', ['angular.filter', 'lr.upload']);
  cmsApp.controller('ProductImages', function($scope, $http) {

    $scope.selectedshop = [0];
    $scope.selectedType = '0';
    $scope.selectedproduct = '';
    $scope.selectedbrands = '';
    $scope.selectedproductgroup = '';
    $scope.fetchGroupImages = function() {
      $http.get('/ajax/tyretypes/images/?filter=' + $scope.selectedproductgroup).then(
        function(data) {
          $scope.productgroups = data.data;
        },
        function(data) {
          console.debug(data);
        });
    };

    $scope.fetchBrandImages = function() {
      $http.get('/ajax/brands/images').then(
        function(data) {
          $scope.brands = data.data;
        },
        function(data) {
          console.debug(data);
        });
    };

    $scope.fetchProducts = function() {
      $http.get('/ajax/products/images?filter=' + $scope.selectedproduct).then(
        function(data) {
          $scope.products = data.data;
        },
        function(data) {
          console.debug(data);
        });
    };

    $scope.deleteGrpImg = function(id) {
      $http.delete('/ajax/grouppic/' + id + '/[' + $scope.selectedshop + ']').then(
        function(data) {
          $scope.fetchGroupImages();
        },
        function(data) {
          console.debug(data);
        });
    };

    $scope.deleteProdImg = function(id) {
      $http.delete('/ajax/productpic/' + id + '/[' + $scope.selectedshop + ']').then(
        function(data) {
          $scope.fetchProducts();
        },
        function(data) {
          console.debug(data);
        });
    };

    $scope.deleteBrandImg = function(id) {
      $http.delete('/ajax/brandpic/' + id + '/[' + $scope.selectedshop + ']').then(
        function(data) {
          $scope.fetchBrandImages();
        },
        function(data) {
          console.debug(data);
        });
    };

    $scope.updateImageType = function(image, imageType) {
      $http.put('/ajax/updateimagetype/' + image, {
        'imgType': imageType
      }).then(
        function(data) {
          console.debug(data);
          $scope.fetchProducts();
          $scope.fetchGroupImages();
        },
        function(data) {
          console.debug(data);
        });
    };

    $http.get('/ajax/imagetypes/').then(
      function(data) {
        $scope.imageTypes = data.data;
      },
      function(data) {
        console.debug(data);
      });

    $scope.fetchProducts();
    $scope.fetchGroupImages();
    $scope.fetchBrandImages();

    $scope.inShopArray = function(elm) {
      for (shop of $scope.selectedshop) {
        if (shop == elm.shop_id) {
          return elm;
        }
      }
    }
  });
</script>

@stop @section('content')
<div ng-app="cmsApp">
  <div ng-controller="ProductImages">
    <div class="form-group">
      <label for="selectedshop">@lang('images.select a shop')</label>
      <select id="selectedshop" ng-model="selectedshop" class="selectpicker" multiple data-live-search="true">
        @foreach ($shops as $shop)
        <option value="{{$shop->id}}">{{$shop->name}}</option>
        @endforeach
      </select>
      <label for="selectedType">@lang('images.image type')</label>
      <select id="selectedType" ng-model="selectedType" class="selectpicker">
        <option value="0">@lang('images.products')</option>
        <option value="1">@lang('images.groups')</option>
        <option value="2">@lang('images.brands')</option>
      </select>
    </div>
    <div ng-show="selectedType == '0'" class="form-group">
      <label for="products">@lang('images.products')</label>
      <input ng-model-options='{ debounce: 500 }' ng-change="fetchProducts()" ng-model="selectedproduct" type=text list=products class="form-control">
      <datalist id=products>
        <option ng-repeat="product in products">@{{product.sku}}</option>
      </datalist>
    </div>

    <div ng-show="selectedType == '2'" class="form-group">
      <label for="brands">@lang('images.brands')</label>
      <input ng-model="selectedbrands" type=text list=brands class="form-control">
      <datalist id=brands>
        <option ng-repeat="brand in brands">@{{brand.name}}</option>
      </datalist>
    </div>

    <div ng-show="selectedType == '1'" class="form-group">
      <label for="productGroup">@lang('images.groups')</label>
      <input ng-model-options='{ debounce: 500 }' ng-change="fetchGroupImages()" ng-model="selectedproductgroup" type=text list=productGroup class="form-control">
      <datalist id=productGroup>
        <option ng-repeat="productgroup in productgroups | first: 50">@{{productgroup.name}}</option>
      </datalist>
    </div>


    <button ng-click="fetchGroupImages(); fetchProducts(); fetchBrandImages();" class="btn btn-primary">@lang('images.refresh manually')</button>
    <div>
      <div ng-show="selectedType == '1'" ng-repeat="group in productgroups | filterBy: ['name']: selectedproductgroup | first: 20" class="row">
        <div class="col-xs-12">
          <h2 class="pull-left">@{{group.name}}</h2><i class="pull-right">@lang('images.drag and drop to upload')</i>
          <div class="btn btn-default col-xs-12 btn-upload pull-right" multiple="true" upload-button url="/ajax/tyretypepic/@{{group.id}}/@{{selectedshop}}" param="image" on-success="fetchGroupImages()">
            <div class="col-xs-2 well h200 uplImgTile" ng-repeat="image in group.images | pick: inShopArray  | unique: 'filename'"><i ng-click="deleteGrpImg(image.id)" class="fa fa-times pull-right"></i>
              <div class="row">
                <select ng-change="updateImageType(image.id, image.selectedImageType.id)" ng-model="image.selectedImageType" ng-init="image.selectedImageType.id = image.imagetype.id" ng-options="imageType.name for imageType in imageTypes track by imageType.id" required>
                </select>
              </div>
              <div class="row">
                <img class="center" ng-src="{{ENV('CDN_URI')}}/images/@{{image.filename}}_thumb.png"></img>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div ng-show="selectedType == '0'" ng-repeat="product in products | filterBy: ['sku']: selectedproduct | first: 20" class="row">
        <div class="col-xs-12">
          <h2 class="pull-left">@{{product.sku}}</h2><i class="pull-right">@lang('images.drag and drop to upload')</i>
          <div class="btn btn-default col-xs-12 btn-upload pull-right" multiple="true" upload-button url="/ajax/productpic/@{{product.id}}/@{{selectedshop}}" param="image" on-success="fetchProducts()">
            <div class="col-xs-2 well h200 uplImgTile" ng-repeat="image in product.images | pick: inShopArray | unique: 'filename'"><i ng-click="deleteProdImg(image.id)" class="fa fa-times pull-right"></i>
              <img class="center" ng-src="{{ENV('CDN_URI')}}/images/@{{image.filename}}_thumb.png"></img>
            </div>
          </div>
        </div>
      </div>
      <div ng-show="selectedType == '2'" ng-repeat="brand in brands | filterBy: ['name']: selectedbrands | first: 20" class="row">
        <div class="col-xs-12">
          <h2 class="pull-left">@{{brand.name}}</h2><i class="pull-right">@lang('images.drag and drop to upload')</i>
          <div class="btn btn-default col-xs-12 btn-upload pull-right" multiple="true" upload-button url="/ajax/brandpic/@{{brand.id}}/@{{selectedshop}}" param="image" on-success="fetchBrandImages()">
            <div class="col-xs-2 well h200 uplImgTile" ng-repeat="image in brand.image | pick: inShopArray | unique: 'filename'"><i ng-click="deleteProdImg(image.id)" class="fa fa-times pull-right"></i>
              <img class="center" ng-src="{{ENV('CDN_URI')}}/images/brands/@{{image.filename}}_large.png"></img>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@stop
