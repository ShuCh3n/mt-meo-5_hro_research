# coding=utf-8
import os
import sys
import json
import bcrypt
import requests
import MySQLdb


#########################

config = {
    'lamp' : {
        'host' : '10.0.159.235',  # BOUWLAMP
        'user' : 'av_cms_connector',
        'pass' : '8rUYBFrzpv06eLcK',
        'data' : 'av_tti_connector'
    },
    'cms' : {
        'host' : '127.0.0.1',  # CMS LOCAL
        'user' : 'av_cms_connector',
        'pass' : '8pwP31OtcrTuk52M',
        'data' : 'cms-dev'
    },
    'google' : {
        'geocoding' : {
            'endpoint' : 'https://maps.googleapis.com/maps/api/geocode/json',
            'token' : 'AIzaSyBpleXjdkXe_LUwSu40zT_NLFVu7MgdWo0'
        }
    }
}

#########################




### Record Checker
def rc(record):
    output = []
    i = 0
    for input in record:
        entry = ec(input, record, i)
        if entry is not False:
            output.append(entry)
        else:
            return False
        i += 1
    return output




### Entry Checker
def ec(entry, record, i = None):
    try:
        input = str(entry)
    except:
        return False


    # MAND
    if i is 1 and (input.find('00:00:00') is not -1 or input.find('00, 00, 00') is not -1):
        input = 'TTI'


    # Company names
    if i is 2 and (input is '' or input.find('00:00:00') is not -1 or input.find('00, 00, 00') is not -1):
        return False


    # Email addresses
    if i is 3 and (input is '' or input.lower() is 'geen' or input.find('@') is -1):
        return False


    # Phone numbers
    if i is 5 or i is 6:
        input = input.replace(' ', '')
        input = input.replace('-', '')


    # Address
    if i is 10 and input.find(', ,') != -1:
            return False


    # Incorrect encoded/decoded characters
    input = input.replace('Ãƒâ€°', 'Ã‰')
    input = input.replace('Ã¢â‚¬Å“', '"')
    input = input.replace('Ã¢â‚¬', '"')
    input = input.replace('Ãƒâ€¡', 'Ã‡')
    input = input.replace('ÃƒÂ©', 'Ã©')
    input = input.replace('ÃƒÆ’Ã‚Â©', 'Ã€Â©')
    input = input.replace('Ã€Â©', 'Ã¨')
    input = input.replace('ÃƒÂº', 'Ãº')
    input = input.replace('Ã¢â‚¬Â¢', '-')
    input = input.replace('ÃƒËœ', 'Ã˜')
    input = input.replace('ÃƒÂµ', 'Ãµ')
    input = input.replace('ÃƒÂ­', 'Ã­')
    input = input.replace('ÃƒÂ¢', 'Ã¢')
    input = input.replace('ÃƒÂ£', 'Ã£')
    input = input.replace('ÃƒÂª', 'Ãª')
    input = input.replace('ÃƒÂ¡', 'Ã¡')
    input = input.replace('ÃƒÂ³', 'Ã³')
    input = input.replace('Ã¢â‚¬â€œ', 'â€“')
    input = input.replace('ÃƒÂ§', 'Ã§')
    input = input.replace('Ã‚Âª', 'Âª')
    input = input.replace('Ã‚Âº', 'Âº')
    input = input.replace('ÃƒÆ’Ã…Â¸', 'ÃŸ')
    input = input.replace('ÃƒÅ¸', 'ÃŸ')
    input = input.replace('ÃƒÆ’Ã‚Â¯', 'Ã¯')
    input = input.replace('Ã¢â€šÂ¬', 'â‚¬')
    input = input.replace('ÃƒÆ’Ã‚Â¼', 'Ã¼')
    input = input.replace('ÃƒÂ¼', 'Ã¼')
    input = input.replace('ÃƒÆ’Ã‚Â¶', 'Ã¶')
    input = input.replace('ÃƒÂ¶', 'Ã¶')
    input = input.replace('ÃƒÂ´', 'Ã¶')
    input = input.replace('ÃƒÆ’Ã¢â‚¬â€œ', 'Ã–')
    input = input.replace('ÃƒÆ’Ã‚Â¤', 'Ã¤')
    input = input.replace('ÃƒÂ¤', 'Ã¤')
    input = input.replace('ÃƒÂ«', 'Ã«')
    input = input.replace('ÃƒÂ¨', 'Ã¨')
    input = input.replace('ÃƒÆ’', 'Ãƒ')
    input = input.replace('ÃƒÆ’Ã…â€œ', 'Ãœ')
    input = input.replace('Ã€Å“', 'Ãœ')
    input = input.replace('ÃƒÂ¯', 'Ã¯')
    input = input.replace('Ãƒâ€šÃ‚', '')
    input = input.replace('Ã‚Â²', 'Â²')
    input = input.replace('Ãƒ', 'Ã€')
    input = input.replace('Ã‚', '')
    input = input.replace('Â´', "'")

    # Characters to remove
    input = input.replace('#', '')  #35
    input = input.replace('$', '')  #36
    input = input.replace('%', '')  #37
    input = input.replace('&', '')  #38
    input = input.replace('*', '')  #42
    input = input.replace('+', '')  #43
    input = input.replace('<', '')  #60
    input = input.replace('=', '')  #61
    input = input.replace('>', '')  #62
    input = input.replace('?', '')  #63
    input = input.replace('[', '')  #91
    input = input.replace('\\', '') #92
    input = input.replace(']', '')  #93
    input = input.replace('^', '')  #94
    input = input.replace('{', '')  #123
    input = input.replace('}', '')  #125
    input = input.replace('~', '')  #126
    input = input.replace('¡', '')  #161
    input = input.replace('¢', '')  #162
    input = input.replace('£', '')  #163
    input = input.replace('¤', '')  #164
    input = input.replace('¥', '')  #165
    input = input.replace('¦', '')  #166
    input = input.replace('§', '')  #167
    input = input.replace('¨', '')  #168
    input = input.replace('©', '')  #169
    input = input.replace('ª', '')  #170
    input = input.replace('«', '')  #171
    input = input.replace('¬', '')  #172
    input = input.replace('®', '')  #174
    input = input.replace('°', '')  #176
    input = input.replace('±', '')  #177
    input = input.replace('²', '')  #178
    input = input.replace('³', '')  #179
    input = input.replace('µ', '')  #181
    input = input.replace('¶', '')  #182
    input = input.replace('·', '')  #183
    input = input.replace('¹', '')  #185
    input = input.replace('º', '')  #186
    input = input.replace('»', '')  #187
    input = input.replace('¼', '')  #188
    input = input.replace('½', '')  #189
    input = input.replace('¾', '')  #190
    input = input.replace('¿', '')  #191
    input = input.replace('×', '')  #215
    input = input.replace('÷', '')  #247
    input = input.replace('Œ', '')  #338
    input = input.replace('ƒ', '')  #402
    input = input.replace('†', '')  #8224
    input = input.replace('‡', '')  #8225
    input = input.replace('•', '')  #8226
    input = input.replace('‰', '')  #8240
    input = input.replace('€', '')  #8364
    input = input.replace('™', '')  #8482

    # Characters to replace
    input = input.replace('/', ' - ') #47
    input = input.replace(':', ', ')  #58
    input = input.replace(';', ', ')  #59
    input = input.replace('|', '-')   #124
    input = input.replace('…', '...') #8230
    input = input.replace('    ', ' ')
    input = input.replace('   ', ' ')
    input = input.replace('  ', ' ')

    # Characters to replace and convert
    input = input.replace('`', '&#39;') #96
    input = input.replace('¯', '&#45;') #175
    input = input.replace('´', '&#39;') #180
    input = input.replace('¸', '&#44;') #184
    input = input.replace('–', '&#45;') #8211
    input = input.replace('—', '&#45;') #8212
    input = input.replace('‘', '&#39;') #8216
    input = input.replace('’', '&#39;') #8217
    input = input.replace('‚', '&#39;') #8218
    input = input.replace('“', '&#34;') #8220
    input = input.replace('”', '&#34;') #8221
    input = input.replace('„', '&#34;') #8222

    # Characters to convert
    input = input.replace('"', '&#34;')
    input = input.replace("'", '&#39;')
    input = input.replace('(', '&#40;')
    input = input.replace(')', '&#41;')
    #input = input.replace(',', '&#44;')
    #input = input.replace('-', '&#45;')
    #input = input.replace('.', '&#46;')
    #input = input.replace('@', '&#64;')
    #input = input.replace('_', '&#95;')

    # Characters with accents
    input = input.replace('À', '&#192;')
    input = input.replace('Á', '&#193;')
    input = input.replace('Â', '&#194;')
    input = input.replace('Ã', '&#195;')
    input = input.replace('Ä', '&#196;')
    input = input.replace('Å', '&#197;')
    input = input.replace('Æ', '&#198;')
    input = input.replace('Ç', '&#199;')
    input = input.replace('È', '&#200;')
    input = input.replace('É', '&#201;')
    input = input.replace('Ê', '&#202;')
    input = input.replace('Ë', '&#203;')
    input = input.replace('Ì', '&#204;')
    input = input.replace('Í', '&#205;')
    input = input.replace('Î', '&#206;')
    input = input.replace('Ï', '&#207;')
    input = input.replace('Ð', '&#208;')
    input = input.replace('Ñ', '&#209;')
    input = input.replace('Ò', '&#210;')
    input = input.replace('Ó', '&#211;')
    input = input.replace('Ô', '&#212;')
    input = input.replace('Õ', '&#213;')
    input = input.replace('Ö', '&#214;')
    input = input.replace('Ø', '&#216;')
    input = input.replace('Ù', '&#217;')
    input = input.replace('Ú', '&#218;')
    input = input.replace('Û', '&#219;')
    input = input.replace('Ü', '&#220;')
    input = input.replace('Ý', '&#221;')
    input = input.replace('Þ', '&#222;')
    input = input.replace('ß', '&#223;')
    input = input.replace('à', '&#224;')
    input = input.replace('á', '&#225;')
    input = input.replace('â', '&#226;')
    input = input.replace('ã', '&#227;')
    input = input.replace('ä', '&#228;')
    input = input.replace('å', '&#229;')
    input = input.replace('æ', '&#230;')
    input = input.replace('ç', '&#231;')
    input = input.replace('è', '&#232;')
    input = input.replace('é', '&#233;')
    input = input.replace('ê', '&#234;')
    input = input.replace('ë', '&#235;')
    input = input.replace('ì', '&#236;')
    input = input.replace('í', '&#237;')
    input = input.replace('î', '&#238;')
    input = input.replace('ï', '&#239;')
    input = input.replace('ð', '&#240;')
    input = input.replace('ñ', '&#241;')
    input = input.replace('ò', '&#242;')
    input = input.replace('ó', '&#243;')
    input = input.replace('ô', '&#244;')
    input = input.replace('õ', '&#245;')
    input = input.replace('ö', '&#246;')
    input = input.replace('ø', '&#248;')
    input = input.replace('ù', '&#249;')
    input = input.replace('ú', '&#250;')
    input = input.replace('û', '&#251;')
    input = input.replace('ü', '&#252;')
    input = input.replace('ý', '&#253;')
    input = input.replace('þ', '&#254;')
    input = input.replace('ÿ', '&#255;')
    input = input.replace('œ', '&#339;')
    input = input.replace('Š', '&#352;')
    input = input.replace('š', '&#353;')
    input = input.replace('Ÿ', '&#376;')

    return input



### Method to get the correct country ID
def get_country(term):
    country_id = None
    term = term.lower()
    for country in countries:
        if country[1].lower() == term or country[2].lower() == term or country[3].lower() == term:
            country_id = country[0]
    if country_id is not None:
        return country_id
    else:
        exit(json.dumps({
            'success' : False,
            'reason' : 'Country couldn\'t be found!',
            'code' : '0x04'
        }))




def create(db, record):
    #print('\n  # Create new record')
    query  = "INSERT INTO "
    query += "`customers` "
    query += "(`av_deb`, `mand`, `company_name`, `shop_id`, `customer_group_id`, `gender`, `firstname`, `lastname`, `dob`, `email`, `password`, `mobile_phone`, `home_phone`, `billing_company`, `billing_vat_number`, `billing_firstname`, `billing_lastname`, `billing_street`, `billing_housenumber`, `billing_city`, `billing_zipcode`, `billing_state`, `billing_state_id`, `billing_country_id`, `tax_rule_id`, `optin`, `note`, `active`, `deleted_at`, `created_at`, `updated_at`) "
    query += "VALUES ("
    query += "'" + str(record['av_deb']) + "', "               # av_deb
    query += "'" + str(record['mand']) + "', "                 # mand
    query += "'" + str(record['company_name']) + "', "         # company_name
    query += "1, "                                             # shop_id
    query += "1, "                                             # customer_group_id
    query += "NULL, "                                          # gender
    query += "NULL, "                                          # firstname
    query += "NULL, "                                          # lastname
    query += "NULL, "                                          # dob
    query += "'" + str(record['email']) + "', "                # email
    query += "'" + str(record['password']) + "', "             # password
    if record['mobile_phone'] != '':
        query += "'" + str(record['mobile_phone']) + "', "     # mobile_phone
    else:
        query += "NULL, "
    if record['home_phone'] != '':
        query += "'" + str(record['home_phone']) + "', "       # home_phone
    else:
        query += "NULL, "
    query += "'" + str(record['billing_company']) + "', "      # billing_company
    if record['billing_vat_number'] != '':
        query += "'" + str(record['billing_vat_number']) + "', "   # billing_vat_number
    else:
        query += "NULL, "
    query += "NULL, "                                          # billing_firstname
    query += "NULL, "                                          # billing_lastname
    query += "'" + str(record['billing_street']) + "', "       # billing_street
    query += "'" + str(record['billing_housenumber']) + "', "  # billing_housenumber
    query += "'" + str(record['billing_city']) + "', "         # billing_city
    query += "'" + str(record['billing_zipcode']) + "', "      # billing_zipcode
    if record['billing_state'] != '':
        query += "'" + str(record['billing_state']) + "', "        # billing_state
    else:
        query += "NULL, "
    query += "NULL, "                                          # billing_state_id
    query += "'" + str(record['billing_country_id']) + "', "   # billing_country_id
    query += "NULL, "                                          # tax_rule_id
    query += "0, "                                             # optin
    query += "NULL, "                                          # note
    query += "1, "                                             # active
    query += "NULL, "                                          # deleted_at
    query += "NOW(), "                                         # created_at
    query += "NOW()"                                           # updated_at
    query += ")"
    customer_id = execute(db, query, True)
    #print('  # I' + str(customer_id))
    #print('  # ' + str(query))

    query  = "INSERT INTO "
    query += "`customer_addresses` "
    query += "(`customer_id`, `alias`, `company`, `firstname`, `lastname`, `mobile_phone`, `home_phone`, `street`, `housenumber`, `city`, `zipcode`, `state`, `state_id`, `country_id`, `default`, `note`, `deleted_at`, `created_at`, `updated_at`) "
    query += "VALUES ("
    query += "'" + str(customer_id) + "', "            # customer_id
    query += "'Default address', "                     # alias
    query += "'" + str(record['company']) + "', "      # company
    query += "NULL, "                                  # firstname
    query += "NULL, "                                  # lastname
    if record['mobile_phone'] != '':
        query += "'" + str(record['mobile_phone']) + "', "         # mobile_phone
    else:
        query += "NULL, "
    if record['home_phone'] != '':
        query += "'" + str(record['home_phone']) + "', "           # home_phone
    else:
        query += "NULL, "
    query += "'" + str(record['street']) + "', "       # street
    query += "'" + str(record['housenumber']) + "', "  # housenumber
    query += "'" + str(record['city']) + "', "         # city
    query += "'" + str(record['zipcode']) + "', "      # zipcode
    if record['state'] != '':
        query += "'" + str(record['state']) + "', "        # state
    else:
        query += "NULL, "
    query += "NULL, "                                  # state_id
    query += "'" + str(record['country_id']) + "', "   # country_id
    query += "1, "                                     # default
    query += "NULL, "                                  # note
    query += "NULL, "                                  # deleted_at
    query += "NOW(), "                                 # created_at
    query += "NOW()"                                   # updated_at
    query += ")"
    execute(db, query)
    #print('  # ' + str(query))

    exit(json.dumps({ 'success' : True, 'creditor' : record['company_name'] }))




### Method to fetch data from the CMS database
def execute(connection, query, return_id = False):
    cursor = connection.cursor()
    try:
        cursor.execute(query)
        connection.commit()
    except Exception, e:
        connection.rollback()
        exit(json.dumps({
            'success' : False,
            'reason' : 'Error while executing query!',
            'code' : '0x05',
            'exception' : str(e)
        }))
    if return_id:
        return cursor.lastrowid






if len(sys.argv) < 2:
    exit(json.dumps({
        'success' : False,
        'reason' : 'No creditor number!',
        'code' : '0x01'
    }))

connection = MySQLdb.connect(
    host = config['cms']['host'],
    user = config['cms']['user'],
    passwd = config['cms']['pass'],
    db = config['cms']['data'],
    charset = 'utf8'
)
cursor = connection.cursor()
cursor.execute("SELECT `av_deb` FROM `customers` WHERE `shop_id` = '1' AND `customer_group_id` = '1' AND `av_deb` = '" + str(sys.argv[1]) + "'")
results = cursor.fetchall()
if len(results) > 0:
    exit(json.dumps({
        'success' : False,
        'reason' : 'Already exists!',
        'code' : '0x02'
    }))




connection = MySQLdb.connect(
    host = config['lamp']['host'],
    user = config['lamp']['user'],
    passwd = config['lamp']['pass'],
    db = config['lamp']['data'],
    charset = 'utf8'
)
cursor = connection.cursor()
cursor.execute("SELECT `SUB_NR` as av_deb, `MAND_ID` as mand, `ACCT_NAME` as company_name, `MAIL_BUS` as email, `WEB_PASS` as password, `TEL_MOB` as mobile_phone, `TEL_BUS` as home_phone, `ACCT_NAME` as billing_company, `VAT_NR` as billing_vat_number, `CNT_CODE` as billing_country, CONCAT(`ADDRESS1`, ', ', `POST_CODE`, ', ', `CITY`, ' ', `CNT_CODE`) as address FROM `CONTACT` WHERE `SUB_NR` = '" + str(sys.argv[1]) + "'")
results = cursor.fetchall()

if len(results) < 1:
    exit(json.dumps({
        'success': False,
        'reason': 'Creditor couldn\'t be found!',
        'code' : '0x13'
    }))
elif len(results) > 1:
    exit(json.dumps({
        'success': False,
        'reason': 'Mutliple creditors were found!',
        'code' : '0x14'
    }))
else:
    creditor = results[0]
    del results, connection


    connection = MySQLdb.connect(
        host = config['cms']['host'],
        user = config['cms']['user'],
        passwd = config['cms']['pass'],
        db = config['cms']['data'],
        charset = 'utf8'
    )

    cursor = connection.cursor()

    cursor.execute("SELECT `id`, `name`, `iso_code_2`, `iso_code_3` FROM `countries` WHERE 1")
    results = cursor.fetchall()

    if len(results) < 1:
        exit(json.dumps({
            'success': False,
            'reason': 'Countries couldn\'t be fetched!',
            'code' : '0x03'
        }))
    else:
        countries = results
        del results


    stop = False
    response = requests.get(config['google']['geocoding']['endpoint'] + '?address=' + creditor[10].replace(' ', '+') + '&key=' + config['google']['geocoding']['token'])
    if response.status_code == 200:
        response = response.json()
        if response['status'] == 'OK' and len(response['results']) == 1:
            new = {}
            new['av_deb'] = creditor[0]
            new['mand'] = creditor[1]
            new['company_name'] = creditor[2]
            new['email'] = creditor[3]
            new['password'] = bcrypt.hashpw(creditor[4].encode('latin-1'), bcrypt.gensalt())
            new['mobile_phone'] = creditor[5]
            new['home_phone'] = creditor[6]
            new['company'] = creditor[7]
            new['billing_company'] = creditor[7]
            new['billing_vat_number'] = creditor[8]
            for component in response['results'][0]['address_components']:
                if 'route' in component['types']:
                    val = ec(component['long_name'], creditor)
                    if val is not False:
                        new['street'] = val
                        new['billing_street'] = val
                    else:
                        stop = True
                elif 'street_number' in component['types']:
                    val = ec(component['long_name'], creditor)
                    if val is not False:
                        new['housenumber'] = val
                        new['billing_housenumber'] = val
                    else:
                        stop = True
                elif 'postal_code' in component['types']:
                    val = ec(component['long_name'], creditor)
                    if val is not False:
                        new['zipcode'] = val
                        new['billing_zipcode'] = val
                    else:
                        stop = True
                elif 'locality' in component['types']:
                    val = ec(component['long_name'], creditor)
                    if val is not False:
                        new['city'] = val
                        new['billing_city'] = val
                    else:
                        stop = True
                elif 'administrative_area_level_1' in component['types']:
                    val = ec(component['long_name'], creditor)
                    if val is not False:
                        new['state'] = val
                        new['billing_state'] = val
                    else:
                        stop = True
                elif 'country' in component['types']:
                    val = get_country(component['short_name'])
                    new['country_id'] = val
                    new['billing_country_id'] = val
            if not stop and 'street' in new and 'billing_street' in new and 'housenumber' in new and 'billing_housenumber' in new and 'zipcode' in new and 'billing_zipcode' in new and 'city' in new and 'billing_city' in new and 'state' in new and 'billing_state' in new and 'billing_country_id' in new and 'country_id' in new:

                ### Extra verifier ###
                #print('  # ' + new['country_id'] + ' | ' + str(len(new['zipcode'])))
                if int(new['country_id']) is 144 and len(new['zipcode']) is not 7:
                    exit(json.dumps({
                        'success' : False,
                        'reason' : 'Incorrect Dutch zipcode!',
                        'code' : '0x09'
                    }))
                else:
                    create(connection, new)
                ######################

            else:
                exit(json.dumps({
                    'success' : False,
                    'reason' : 'Address components are missing!',
                    'code' : '0x08'
                }))
        elif response['status'] == 'OK' and len(response['results']) > 1:
            exit(json.dumps({
                'success' : False,
                'reason' : 'Multiple addresses were found!',
                'code' : '0x07'
            }))
        elif response['status'] == 'ZERO_RESULTS':
            exit(json.dumps({
                'success' : False,
                'reason' : 'Address couldn\'t be found!',
                'code' : '0x06'
            }))
        elif response['status'] == 'OVER_QUERY_LIMIT':
            exit(json.dumps({
                'success' : False,
                'reason' : 'Exceeded API query limit!',
                'code' : '0x10'
            }))
        else:
            exit(json.dumps({
                'success' : False,
                'reason' : 'API response error',
                'code' : '0x11',
                'exception' : str(response['status'])
            }))
    else:
        exit(json.dumps({
            'success' : False,
            'reason' : 'HTTP response error',
            'code' : '0x12',
            'exception' : str(response.status_code)
        }))