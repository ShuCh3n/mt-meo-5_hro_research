@extends('dashboard.layout.main')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>{{ trans('pages.inquiry_detail_header') }}</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('home') }}">{{ trans('menu.home') }}</a>
                </li>
                <li>
                    <a href="">{{ trans('menu.inquiries') }}</a>
                </li>
                <li class="active">
                    <strong>{{ trans('page.inquiry') }}</strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">

        <form method="post" class="form-horizontal">
            <div class="row">
                <div class="col-md-8">
                    <div class="ibox">

                        <div class="ibox-title">
                            <h5>Edit Feature</h5>
                        </div>

                        <div class="ibox-content">
                            <div class="mb20"></div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Language</label>
                                        <div class="col-md-10">
                                            <language-select id="multi-language" class="form-control countries-select">
                                                @foreach($languages as $language)
                                                    <option value="{{ $language->id }}" icon="{{ $language->icon }}">{{ $language->name }}</option>
                                                @endforeach
                                            </language-select>
                                        </div>
                                    </div>

                                    @foreach($languages as $language)
                                        <div class="multi_language {{ $language->id }}">
                                            <div class="form-group">
                                                <label class="col-md-2 control-label">Name {!! flag_icon($language->icon) !!}</label>
                                                <div class="col-md-10">
                                                    <input type="text" class="form-control feature-name-input" name="feature_name[{{ $language->id }}]" value="{{ @$product_feature->infos->where('language_id', $language->id)->first()->name }}" placeholder="Feature Name">
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div> 

                    <div class="ibox">

                        <div class="ibox-title">
                            <h5>Feature Options</h5>
                        </div>

                        <div class="ibox-content">
                            <div class="mb20"></div>

                            <div class="row">
                                <div class="col-md-12">
                                    @foreach($languages as $language)
                                        <div class="multi_language {{ $language->id }}">
                                            @foreach($product_feature->options as $option)
                                                <div class="form-group option_row_{{ $option->id }}">
                                                    @if($loop->first)
                                                        <label class="col-md-2 control-label">Option</label>
                                                        <div class="col-md-1 text-center">
                                                            {!! flag_icon($language->icon) !!}
                                                        </div>
                                                        <div class="col-md-5">
                                                            <input type="text" class="form-control feature-option-input" name="option_name[old][{{ $language->id }}][{{ $option->id }}]" data-option="{{ $option->id }}" placeholder="Option Name" value="{{ @$option->infos->where('language_id', $language->id)->first()->name }}">
                                                        </div>
                                                        <div class="col-md-2"><p class="form-control-static"><a href="{{ route('shop_products', ['all', 'filters' => ['options' => [$option->id]]]) }}">View Products ({{ $option->products->count() }})</a></p></div>
                                                        <div class="col-md-2">
                                                            <a class="btn btn-outline btn-warning btn-sm" href="{{ route('transfer_feature_option', $option->id) }}"><i class="fa fa-exchange" aria-hidden="true"></i> Transfer</a>
                                                            <button type="button" class="btn btn-outline btn-danger btn-sm remove-feature" option_id="{{ $option->id }}" href="{{ route('transfer_feature_option', $option->id) }}"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                                                        </div>
                                                    @else
                                                        <div class="col-md-1 col-md-offset-2 text-center">
                                                            {!! flag_icon($language->icon) !!}
                                                        </div>
                                                        <div class="col-md-5">
                                                            <input type="text" class="form-control feature-option-input" name="option_name[old][{{ $language->id }}][{{ $option->id }}]" data-option="{{ $option->id }}" placeholder="Option Name" value="{{ @$option->infos->where('language_id', $language->id)->first()->name }}">
                                                        </div>
                                                        <div class="col-md-2"><p class="form-control-static"><a href="{{ route('shop_products', ['all', 'filters' => ['options' => [$option->id]]]) }}">View Products ({{ $option->products->count() }})</a></p></div>
                                                        <div class="col-md-2">
                                                            <a class="btn btn-outline btn-warning btn-sm" href="{{ route('transfer_feature_option', $option->id) }}"><i class="fa fa-exchange" aria-hidden="true"></i> Transfer</a>
                                                            <button type="button" class="btn btn-outline btn-danger btn-sm remove-feature" option_id="{{ $option->id }}" href="{{ route('transfer_feature_option', $option->id) }}"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                                                        </div>
                                                    @endif
                                                </div>
                                            @endforeach

                                            <div class="more_options language-{{ $language->id }}" data-language="{{ $language->id }}"></div>
                                        </div>
                                    @endforeach

                                    <button class="btn btn-w-m btn-primary pull-right add-option" type="button">
                                        <i class="fa fa-plus" aria-hidden="true"></i> Add Option
                                    </button>

                                </div>
                            </div>
                        </div>
                    </div> 

                </div>

                <div class="col-md-4">
                    <div class="ibox">
                        <div class="ibox-title">
                            <h5>Action</h5>
                        </div>

                        <div class="ibox-content">
                            {{ csrf_field() }}
                            <button class="btn btn-block btn-outline btn-success" type="submit"><i class="fa fa-floppy-o"></i> Save</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>

        <div class="mb30"></div>
    </div>
@stop