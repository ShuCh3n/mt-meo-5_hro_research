import pandas
import numpy
import os
import ftpMinion as ftp_minion

class CSVMinion():
	def __init__(self, base_path):
		self.base_path = base_path

	def getData(self, seperator, filename, skip_rows = 0):
		ftp = ftp_minion.FTPMinion()
		ftp.sync_server().getFile(filename)

		return pandas.read_csv(os.path.dirname(__file__) + '/' + self.base_path + '/' + filename, sep=seperator, low_memory=False, encoding='ISO-8859-1', skiprows=skip_rows, error_bad_lines=False)