@extends('dashboard.layout.main')

@section('content')
	<div class="wrapper wrapper-content animated fadeInRight ecommerce">
            <div class="ibox-content m-b-sm border-bottom">
                <div class="row">
                    <div class="col-xs-12">
                        <a class="btn btn-primary pull-right" href="{{ route('new_cart_rule') }}"><i class="fa fa-plus-circle" aria-hidden="true"></i> New Cart Rule</a>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox">
                        <div class="ibox-content">

                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Coupon</th>
                                        <th>Giftcard</th>
                                        <th>Value</th>
                                        <th>Free Shipping</th>
                                        <th>Valid Date</th>
                                        <th>Active</th>
                                        <th class="text-right" data-sort-ignore="true">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($cart_rules as $rule)
                                        <tr>
                                            <td>
                                               {{ $rule->name }}
                                            </td>
                                            <td>
                                               {{ $rule->voucher_code }}
                                            </td>
                                            <td>
                                              {!! ($rule->giftcard)? '<i class="fa fa-check text-navy" aria-hidden="true"></i>' : '<i class="fa fa-times text-danger" aria-hidden="true"></i>' !!}
                                            </td>
                                            <td>
                                                <i class="fa fa-minus" aria-hidden="true"></i>

                                               @if($rule->amount_or_percentage)
                                                 €
                                               @endif

                                               {{ $rule->amount }}

                                               @if(!$rule->amount_or_percentage)
                                                %
                                               @endif
                                            </td>
                                            <td>
                                               {!! ($rule->free_shipping)? '<i class="fa fa-check text-navy" aria-hidden="true"></i>' : '<i class="fa fa-times text-danger" aria-hidden="true"></i>' !!}
                                            </td>
                                            <td>
                                               {{ date('d M Y', strtotime($rule->valid_from)) }} until {{ date('d M Y', strtotime($rule->valid_until)) }}
                                            </td>
                                            <td>
                                               {!! ($rule->active)? '<i class="fa fa-check text-navy" aria-hidden="true"></i>' : '<i class="fa fa-times text-danger" aria-hidden="true"></i>' !!}
                                            </td>
                                            <td class="text-right">
                                                <div class="btn-group">
                                                    <a href="{{ route('edit_product_feature_type', $rule->id) }}" class="btn-white btn btn-xs">Edit</a>
                                                    <a href="{{ route('remove_product_feature_type', $rule->id) }}" class="btn-white btn btn-xs">Delete</a>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@stop