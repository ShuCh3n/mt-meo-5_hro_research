<?php
namespace App\Classes\SyncFactory;

use App\Classes\SyncFactory\Order;
use App\Classes\SyncFactory\Product;

class SyncFactory
{
	public static function create($type)
	{
		switch ($type) {
			case 'order':
				return new Order;
				break;
			case 'product':
				return new Product;
				break;
			default:
				# code...
				break;
		}
	}
}