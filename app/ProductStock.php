<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductStock extends Model
{
    use SubFilter;
    use SoftDeletes;
    
    protected $fillable = ['warehouse_id', 'product_id', 'amount'];

    public function warehouse()
    {
        return $this->belongsTo(Warehouse::class);
    }
}
