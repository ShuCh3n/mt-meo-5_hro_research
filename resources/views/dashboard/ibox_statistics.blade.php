@extends('dashboard.layout.main')

@section('javascript')
    <script>
        $('.input-daterange').datepicker({
            format: "dd M yyyy",
            keyboardNavigation: false,
            forceParse: false,
            autoclose: true,
            orientation: 'bottom'
        });
    </script>
@stop

@section('content')
	<div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-md-12">

            	<div class="ibox">
                    <div class="ibox-title">
                        <h5>Statistic Date</h5>
                    </div>

                    <div class="ibox-content">
                        <div class="mb20"></div>

                        <form method="get">
                            <div class="row m-b-lg">

                                <div class="col-sm-12">

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Date</label>
                                        <div class="col-md-8">
                                            <div class="input-daterange input-group col-xs-12" id="datepicker">
                                                <input type="text" class="form-control" name="from" placeholder="From date" value="{{ request()->from }}" />
                                                <span class="input-group-addon">to</span>
                                                <input type="text" class="form-control" name="until" placeholder="Until date" value="{{ request()->until }}" />
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <button class="btn-primary btn">Select</button>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                @component('dashboard.components.flot_graph')
                	@slot('heading')
                		Rubber
                	@endslot

                    @slot('stats')
                        {{ $rubber }}
                    @endslot
                @endcomponent

                @component('dashboard.components.flot_graph')
                	@slot('heading')
                		Weather
                	@endslot

                    @slot('stats')
                        {{ $weather }}
                    @endslot
                @endcomponent

            </div>
        </div>

    </div>
@stop