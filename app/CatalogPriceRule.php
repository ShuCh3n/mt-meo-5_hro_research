<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CatalogPriceRule extends Model
{
    use SoftDeletes;

    protected $dates = ['valid_from', 'valid_until'];
    
	public function countries()
	{
		return $this->belongsToMany(Country::class);
	}

    public function customer_groups()
    {
        return $this->belongsToMany(CustomerGroup::class);
    }

	public function products()
	{
		return $this->belongsToMany(Product::class);
	}

	public function feature_types()
	{
		return $this->belongsToMany(ProductFeatureType::class);
	}

    public function shops()
    {
        return $this->belongsToMany(Shop::class);
    }

	public function categories()
	{
		return $this->belongsToMany(Category::class);
	}

    public function setCatalogPriceRule($request)
    {
    	$this->active = $request->active;
    	$this->name = $request->name;
    	$this->add_or_subtract = $request->add_or_subtract;
        $this->amount_or_percentage = $request->amount_or_percentage;
    	$this->amount = ($request->amount_or_percentage == 1)? $request->price_amount : $request->percentage_amount;
    	$this->priority = $request->priority;
        $this->valid_from = date("Y-m-d", strtotime($request->valid_from));
        $this->valid_until = date("Y-m-d", strtotime($request->valid_until));
    	$this->save();
    }

    public function syncCountries($countries)
    {
    	$this->countries()->sync($countries);
    }

    public function syncCustomerGroups($customer_groups)
    {
        $this->customer_groups()->sync($customer_groups);
    }

    public function syncProducts($products)
    {
    	$this->products()->sync($products);
    }

    public function syncFeatureTypes($feature_types)
    {
    	$this->feature_types()->sync($feature_types);
    }

    public function syncCategories($categories)
    {
    	$this->categories()->sync($categories);
    }

    public function syncShops($shops)
    {
        $this->shops()->sync($shops);
    }

    public function scopeActive($query)
    {
        return $query->where('active', 1);
    }

    
}
