@extends('dashboard.layout.main')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-6">
            <h2>{{ trans('pages.inquiry_detail_header') }}</h2>
            <ol class="breadcrumb">
                <li>
                    {{ trans('menu.home') }}
                </li>
                <li>
                    {{ trans('menu.inquiries') }}
                </li>
                <li class="active">
                    <strong>{{ trans('page.inquiry') }}</strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">
        <form method="post" class="form-horizontal" enctype="multipart/form-data">
            <div class="row">
                <div class="col-xs-8">
                    <div class="ibox">
                        <div class="ibox-title">
                            <h5>@lang('blog.blog_post')</h5>
                        </div>
                        <div class="ibox-content">

                            <div class="form-group">
                                <label class="col-sm-2 control-label">Cover</label>
                                <div class="col-sm-10">
                                    <div class="image_block text-center">
                                        <label for="cover-file-input">
                                            <div class="preview_image center-block hide">
                                                <img id="preview_image" src="#" />
                                            </div>
                                        
                                            <div class="choose_image_block" id="container">
                                                <i class="fa fa-picture-o"></i> Choose Cover Image
                                            </div>
                                        </label>
                                        <input id="cover-file-input" name="cover_image" type="file" />
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label">@lang('blog.language')</label>

                                <div class="col-md-10">
                                    <language-select class="form-control">
                                        @foreach($languages as $language)
                                            <option value="{{ $language->id }}" icon="{{ $language->icon }}">{{ $language->name }}</option>
                                        @endforeach
                                    </language-select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label">@lang('blog.category')</label>

                                <div class="col-md-8">
                                    <select2 name="category">
                                        @foreach($categories as $category)
                                            <option value="{{ $category->id }}" {{ ($blog->blog_category_id == $category->id)? 'selected' : ''}}>{{ $category->info()->first()->name }}</option>
                                        @endforeach
                                    </select2>
                                </div>

                                <div class="col-md-2">
                                    <a href="{{ route('new_blog_category') }}" class="btn btn-primary btn-block"><i class="fa fa-plus" aria-hidden="true"></i> New Category</a>
                                </div>
                            </div>

                            @foreach($languages as $language)
                                <div class="multi_language {{ $language->id }}">
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">@lang('blog.title') {!! flag_icon($language->icon) !!}</label>

                                        <div class="col-md-10">
                                            <input type="text" class="form-control" name="language[{{ $language->id }}][title]" value="{{ $blog->info()->language($language->id)->first()->title ?? '' }}" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">@lang('blog.short_body') {!! flag_icon($language->icon) !!}</label>

                                        <div class="col-md-10">
                                            <summernote name="language[{{ $language->id }}][short]" height="100">
                                                {!! $blog->info()->language($language->id)->first()->short ?? '' !!}
                                            </summernote>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                       <label class="col-md-2 control-label">@lang('blog.body') {!! flag_icon($language->icon) !!}</label>

                                        <div class="col-md-10">
                                            <summernote name="language[{{ $language->id }}][body]" height="300">
                                                {!! $blog->info()->language($language->id)->first()->body ?? '' !!}
                                            </summernote>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>

                </div>

                <div class="col-xs-4">
                    <div class="ibox">
                        <div class="ibox-title">
                            <h5>@lang('blog.action')</h5>
                        </div>
                        <div class="ibox-content">

                            <div class="form-group">
                                <div class="col-md-6">
                                    <label><input type="radio" class="form-control i-checks" name="active" value="1" checked> @lang('blog.enabled')</label>
                                </div>
                                <div class="col-md-6">
                                    <label><input type="radio" class="form-control i-checks" name="active" value="0"> @lang('blog.disabled')</label>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-12">@lang('blog.release_date')</label>

                                <div class="col-md-12">
                                    <datepicker name="releasedate" value="{{ $blog->published_at->format('d M Y') }}"></datepicker>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-12">
                                    <div class="input-group clockpicker" data-autoclose="true">
                                        <span class="input-group-addon">
                                            <span class="fa fa-clock-o"></span>
                                        </span>
                                        <input type="text" name="releasetime" class="form-control">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-12">@lang('blog.tags')</label>

                                <div class="col-md-12">
                                    <select2 multiple="multiple" name="tags[]">
                                        @foreach($allTags as $tag)
                                            <option @if(in_array($tag->id, $tags)) selected @endif value="{{ $tag->id }}">{{ $tag->tagContent->where('languages_id', 2)->first()->name ?? ''}}</option>
                                        @endforeach
                                    </select2>
                                </div>
                            </div>

                            {{ csrf_field() }}
                            <button class="btn btn-block btn-outline btn-success" type="submit"><i class="fa fa-floppy-o"></i> @lang('blog.save')</button>
                        </div>

                    </div>
                </div>


            </div>
        </form>
    </div>
@stop
