<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShopProduct extends Model
{
    protected $fillable = ['shop_id', 'product_id', 'tax_rule_id', 'minimum_qty', 'stock_management', 'active_when_out_of_stock', 'deny_order_when_out_of_stock', 'additional_fee_per_item', 'active'];

    public function product()
    {
    	return $this->belongsTo(Product::class);
    }

    public function shop()
    {
        return $this->belongsTo(Shop::class);
    }

    public function tax_rule()
    {
        return $this->belongsTo(TaxRule::class);
    }

    public function scopeProductPrice($query, $shop)
    {
        return $query->with(['product.shop_prices' => function($query) use ($shop){
            $query->where('shop_id', $shop->id)->latest()->first();
        }]);
    }

    public function scopeShop($query, $shop_id)
    {
        return $query->where('shop_id', $shop_id);
    }

    public function scopeProduct($query, $product_id)
    {
        return $query->where('product_id', $product_id);
    }

    public function setProduct($request)
    {
        //$this->featured = $request->featured;
        $this->tax_rule_id = ($request->tax_rule && !empty($request->tax_rule))? $request->tax_rule : null;
        $this->minimum_qty = $request->minimum_qty;
        $this->stock_management = intval($request->stock_management);
        $this->active_when_out_of_stock = intval($request->active_when_out_of_stock);
        $this->deny_order_when_out_of_stock = intval($request->deny_order_when_out_of_stock);
        $this->additional_fee_per_item = floatval($product->additional_fee_per_item ?? 0);
        $this->active = intval($request->active);
        $this->save();
    }

    public function scopeActive($query)
    {
        return $query->where('active', 1);
    }
}
