@extends('dashboard.layout.main')

@section('content')
	<div class="wrapper wrapper-content animated fadeInRight ecommerce">
            <div class="ibox-content m-b-sm border-bottom">
                <div class="row">
                    <div class="col-xs-12">
                        <a class="btn btn-primary pull-right" href="{{ route('new_page') }}"><i class="fa fa-plus-circle" aria-hidden="true"></i> New page</a>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox">
                        <div class="ibox-content">

                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th data-toggle="true">
                                            <a href="">
                                                Shop
                                            </a>
                                        </th>
                                        <th data-hide="phone">
                                            <a href="">
                                                Pages
                                            </a>
                                        </th>
                                        <th class="text-right" data-sort-ignore="true">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($shops as $shop)
                                        <tr>
                                            <td>
                                               {{ $shop->name }}
                                            </td>
                                            <td>
                                              {{ rand(1, 10) }}
                                            </td>
                                            <td class="text-right">
                                                <div class="btn-group">
                                                    <a href="{{ route('shop_pages', $shop->id) }}" class="btn-white btn btn-xs">View</a>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@stop