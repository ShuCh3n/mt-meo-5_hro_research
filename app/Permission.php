<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    public function infos()
    {
    	return $this->hasMany(PermissionLanguage::class);
    }

    public function info()
    {
        return $this->hasOne(PermissionLanguage::class);
    }
}
