<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Images extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('image_type', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->softDeletes();
            $table->timestamps();
        });

        DB::table('image_type')->insert([
            [
                "name"  => "Front"
            ],
            [
                "name"  => "Rear"
            ],
            [
                "name"  => "Front + Rear"
            ]
        ]);

        Schema::create('images', function(Blueprint $table) {
            $table->increments('id');
            $table->string('filename');
            $table->integer('imagable_id')->nullable();
            $table->string('imagable_type')->nullable();
            $table->integer('image_type_id')->unsigned()->nullable();
            $table->integer('shop_id')->unsigned()->nullable();
            $table->softDeletes();
            $table->timestamps();

            $table->index('imagable_id');
            $table->index('imagable_type');

            $table->foreign('image_type_id')
                ->references('id')
                ->on('image_type')
                ->onDelete('cascade');

            $table->foreign('shop_id')
                ->references('id')
                ->on('shops')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('images');
        Schema::dropIfExists('image_type');
    }
}
