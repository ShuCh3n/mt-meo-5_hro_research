<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Customer;
use App\Shop;
use App\Http\Requests\CustomerRequest;
use App\EmailAvatar;
use App\CustomerGroup;
use App\Country;
use App\CustomerAddress;
use App\TaxGroup;

use Storage;
use DB;

class CustomerController extends Controller
{
	public function customers()
    {
        $customers = Customer::orderBy('created_at', 'desc')->paginate(100);
        $request = $this->request;

        return view('dashboard.customers', compact('customers', 'request'));
    }

    public function newCustomer()
    {
    	$shops = Shop::all();
        $countries = Country::all();
        $customer_groups = CustomerGroup::all();
        $tax_groups = TaxGroup::all();

    	return view('dashboard.new_customer', compact('shops', 'customer_groups', 'countries', 'tax_groups'));
    }

    public function actionNewCustomer(CustomerRequest $request)
    {
        $this->request = $request;

        if($this->request->shop_object->hasEmail($this->request->email))
        {
            return redirect()->back()->withInput()->withErrors(["Email already exist for this shop"]);
        }
        
        $customer = new Customer;
        $customer->shop_id = $this->request->shop;
        $customer->customer_group_id = ($this->request->group)? CustomerGroup::findOrFail($this->request->group)->id : null;
        $customer->gender = $this->request->gender;
        $customer->firstname = $this->request->firstname;
        $customer->lastname = $this->request->lastname;
        $customer->dob = date("Y-m-d", strtotime($this->request->dob));
        $customer->email = $this->request->email;
        $customer->password = bcrypt($this->request->password);
        $customer->mobile_phone = $this->request->mobile_phone;
        $customer->home_phone = $this->request->phone;
        $customer->billing_company = $this->request->company;
        $customer->billing_vat_number = $this->request->vat_number;
        $customer->billing_firstname = $this->request->firstname;
        $customer->billing_lastname = $this->request->lastname;
        $customer->billing_street = $this->request->street;
        $customer->billing_housenumber = $this->request->housenumber;
        $customer->billing_city = $this->request->city;
        $customer->billing_zipcode = $this->request->zipcode;
        $customer->billing_state_id = $this->request->state;
        $customer->billing_country_id = $this->request->country;
        $customer->newsletter = $this->request->newsletter;
        $customer->optin = $this->request->optin;
        $customer->note = $this->request->note;
        $customer->active = $this->request->active;
        $customer->save();

        if($this->request->same_as_shipping)
        {
			$customer_address = new CustomerAddress;
			$customer_address->customer_id = $customer->id;
			$customer_address->default = 1;
			$customer_address->alias = "Address " . rand ( "10" , "1688" );
			$customer_address->company = $this->request->company;
			$customer_address->firstname = $this->request->firstname;
			$customer_address->lastname = $this->request->lastname;
			$customer_address->mobile_phone = $this->request->mobile_phone;
			$customer_address->home_phone = $this->request->phone;
			$customer_address->street = $this->request->street;
			$customer_address->housenumber = $this->request->housenumber;
			$customer_address->city = $this->request->city;
			$customer_address->zipcode = $this->request->zipcode;
			$customer_address->state = $this->request->state;
			$customer_address->country_id = $this->request->country;
			$customer_address->save();
        }

        EmailAvatar::retrieveGravatar($customer->email);

        return redirect()->route('customers');
    }

    public function actionEditCustomer(CustomerRequest $request)
    {
        
        $customer = Customer::findOrFail($this->request->id);

        if($customer->email != $this->request->email)
        {
            if($this->request->shop_object->hasEmail($this->request->email))
            {
                return redirect()->back()->withInput()->withErrors(["Email already exist for this shop"]);
            }
        }

        $customer->shop_id = $this->request->shop;
        $customer->customer_group_id = ($this->request->group)? CustomerGroup::findOrFail($this->request->group)->id : null;
        $customer->gender = $this->request->gender;
        $customer->firstname = $this->request->firstname;
        $customer->lastname = $this->request->lastname;
        $customer->dob = date("Y-m-d", strtotime($this->request->dob));
        $customer->email = $this->request->email;

        if($this->request->password)
        {
            $customer->password = bcrypt($this->request->password);
        }
        
        $customer->mobile_phone = $this->request->mobile_phone;
        $customer->home_phone = $this->request->phone;
        $customer->billing_company = $this->request->company;
        $customer->billing_vat_number = $this->request->vat_number;
        $customer->billing_street = $this->request->street;
        $customer->billing_housenumber = $this->request->housenumber;
        $customer->billing_city = $this->request->city;
        $customer->billing_zipcode = $this->request->zipcode;
        $customer->billing_state = $this->request->state;
        $customer->billing_country_id = $this->request->country;
        $customer->optin = $this->request->optin;
        $customer->note = $this->request->note;
        $customer->active = $this->request->active;
        $customer->save();

        //EmailAvatar::retrieveGravatar($customer->email);

        return redirect()->route('customers');
    }

    public function editCustomer()
    {
        $customer = Customer::findOrFail($this->request->id);
        $shops = Shop::all();
        $countries = Country::all();
        $customer_groups = CustomerGroup::all();

        return view('dashboard.edit_customer', compact('customer', 'shops', 'countries', 'customer_groups'));
    }

    public function viewCustomer()
    {
        $customer = Customer::findOrFail($this->request->id);
        return view('dashboard.view_customer', compact('customer'));
    }

    public function viewAvatar()
    {
        $customer = Customer::findOrFail($this->request->id);
        $size = "medium";

        if($this->request->size)
        {
            $size = $this->request->size;
        }

        $avatar = $customer->avatar();

        if($avatar)
        {
            $file = Storage::get('email_avatars/' . $size . '/' . $avatar->filename);
            return response($file)->header('Content-Type', 'image/jpg');
        }
    }

    public function customerGroups()
    {
        $customer_groups = CustomerGroup::all();

        for($i = 0; $i < count($customer_groups); $i++)
            $customer_groups[$i]->customers = Customer::where('customer_group_id', $customer_groups[$i]->id)->count();

        return view('dashboard.customer_groups', compact('customer_groups'));
    }

    public function newCustomerGroup()
    {
        return view('dashboard.new_customer_group');
    }

    public function actionNewCustomerGroup()
    {
        $customer_group = new CustomerGroup;
        $customer_group->name = $this->request->name;
        $customer_group->mandant = $this->request->mandant;
        $customer_group->discount = $this->request->discount;
        $customer_group->tax = $this->request->tax;
        $customer_group->show_prices = $this->request->show_prices;
        $customer_group->save();

        return redirect()->route('customer_groups');
    }

    public function editCustomerGroup()
    {
        $customer_group = CustomerGroup::findOrFail($this->request->id);
        return view('dashboard.edit_customer_group', compact('customer_group'));
    }

    public function modifyCustomerGroup()
    {
        $customer_group = CustomerGroup::findOrFail($this->request->id)->update([
            "name" => $this->request->name,
            "mandant" => $this->request->mandant,
            "discount" => $this->request->discount,
            "tax" => $this->request->tax,
            "show_prices" => $this->request->show_prices
        ]);

        return redirect()->route('customer_groups');
    }

    public function newCustomerAddress()
    {
        $customers = Customer::all();
        $countries = Country::all();
        $customer_id = $this->request->customer_id;

        return view('dashboard.new_customer_addresss', compact('customers', 'countries', 'customer_id'));
    }

    public function actionNewCustomerAddress()
    {
        $customer_address = new CustomerAddress;
        $customer_address->customer_id = Customer::findOrFail($this->request->customer)->id;
        $customer_address->alias = $this->request->name;
        $customer_address->company = $this->request->company;
        $customer_address->firstname = $this->request->firstname;
        $customer_address->lastname = $this->request->lastname;
        $customer_address->mobile_phone = $this->request->mobile_phone;
        $customer_address->home_phone = $this->request->home_phone;
        $customer_address->street = $this->request->street;
        $customer_address->housenumber = $this->request->housenumber;
        $customer_address->city = $this->request->city;
        $customer_address->zipcode = $this->request->zipcode;
        $customer_address->state = $this->request->state;
        $customer_address->country_id = $this->request->country;
        $customer_address->save();

        if($this->request->default_address)
        {
            $customer_address->makeDefault();
        }

        return redirect()->route('customer_addresses');
    }

    public function editCustomerAddress()
    {
        $customer_address = CustomerAddress::findOrFail($this->request->id);
        $countries = Country::all();

        return view('dashboard.edit_customer_address', compact('customer_address', 'countries'));
    }

    public function actionEditCustomerAddress()
    {
        $customer_address = CustomerAddress::findOrFail($this->request->id);
        $customer_address->alias = $this->request->name;
        $customer_address->company = $this->request->company;
        $customer_address->firstname = $this->request->firstname;
        $customer_address->lastname = $this->request->lastname;
        $customer_address->mobile_phone = $this->request->mobile_phone;
        $customer_address->home_phone = $this->request->home_phone;
        $customer_address->street = $this->request->street;
        $customer_address->housenumber = $this->request->housenumber;
        $customer_address->city = $this->request->city;
        $customer_address->zipcode = $this->request->zipcode;
        $customer_address->state = $this->request->state;
        $customer_address->country_id = $this->request->country;
        

        if($this->request->default_address)
        {
            $customer_address->makeDefault();
        }
        else
        {
            $customer_address->default = 0;
        }

        $customer_address->save();

        return redirect()->route('customer_addresses');
    }

    public function findCustomer()
    {
        $result['items'] = array();
        
        if($this->request->shop)
        {
            $shop = Shop::where('id', $this->request->shop)->with(['customers' => function($query){
                $query->where('id', 'like', '%' . $this->request->q .'%');
                $query->orWhere('firstname', 'like', '%' . $this->request->q .'%');
                $query->orWhere('lastname', 'like', '%' . $this->request->q .'%');
                $query->orWhere('email', 'like', '%' . $this->request->q .'%');
                $query->orWhere('mobile_phone', 'like', '%' . $this->request->q .'%');
                $query->orWhere('home_phone', 'like', '%' . $this->request->q .'%');
                $query->with('customer_addresses');
            }])->first();

            $result['items'] = $shop->customers;
        }
        else
        {
            $result['items'] = Customer::where(DB::raw('CAST(id as TEXT)'), 'like', '%' . intval($this->request->q) . '%' )
                        ->orWhere('firstname', 'like', '%' . $this->request->q .'%')
                        ->orWhere('lastname', 'like', '%' . $this->request->q .'%')
                        ->orWhere('email', 'like', '%' . $this->request->q .'%')
                        ->orWhere('mobile_phone', 'like', '%' . $this->request->q .'%')
                        ->orWhere('home_phone', 'like', '%' . $this->request->q .'%')
                        ->get();
        }

        return $result;
    }

    public function resolveCreditor()
    {
        $response = shell_exec('python '.base_path('python/resolveCreditor.py').' '.$this->request->code.' 2>&1');

        return $response;
    }
}
