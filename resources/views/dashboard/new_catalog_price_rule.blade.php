@extends('dashboard.layout.main')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>{{ trans('pages.inquiry_detail_header') }}</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('home') }}">{{ trans('menu.home') }}</a>
                </li>
                <li>
                    <a href="">{{ trans('menu.inquiries') }}</a>
                </li>
                <li class="active">
                    <strong>{{ trans('page.inquiry') }}</strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">
        {!! session()->has('success')? '<div class="alert alert-success alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>' . session('success') . '</div>' : '' !!}

        <form method="post" class="form-horizontal">
            <div class="row">
                <div class="col-md-8">
                    <div class="ibox">

                        <div class="ibox-title">
                            <h5>New Catalog Price Rule</h5>
                        </div>

                        <div class="ibox-content">
                            <div class="mb20"></div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Active</label>
                                        <div class="col-md-5">
                                            <label><input type="radio" class="form-control i-checks" name="active" value="1" checked> Yes</label>
                                        </div>
                                        <div class="col-md-5">
                                            <label><input type="radio" class="form-control i-checks" name="active" value="0"> No</label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Name</label>
                                        <div class="col-md-10"><input type="text" class="form-control" name="name" placeholder="Cataglog Price Rule Name"></div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Add / Subtract</label>
                                        <div class="col-md-5">
                                            <label><input type="radio" class="form-control i-checks" name="add_or_subtract" value="1" checked> Add</label>
                                        </div>
                                        <div class="col-md-5">
                                            <label><input type="radio" class="form-control i-checks" name="add_or_subtract" value="0"> Subtract</label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Amount / Percentage</label>
                                        <div class="col-md-5">
                                            <label><input type="radio" class="form-control i-checks check-amount" name="amount_or_percentage" value="1" checked> Amount</label>
                                        </div>
                                        <div class="col-md-5">
                                            <label><input type="radio" class="form-control i-checks check-percentage" name="amount_or_percentage" value="0"> Percentage</label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Amount</label>
                                        <div class="col-md-10 input-price-amount">
                                            <amount-input name="price_amount" placeholder="Price Amount"></amount-input>
                                        </div>
                                        <div class="col-md-10 input-percentage-amount">
                                            <percentage-input name="percentage_amount" placeholder="Percentage Amount"></percentage-input>
                                        </div>
                                    </div>

                                    <div class="form-group" id="data_1">
                                        <label class="col-md-2 control-label">Valid Date</label>
                                        <div class="col-md-4">
                                            <datepicker name="valid_from" placeholder="Valid from"></datepicker>
                                        </div>
                                        <div class="col-md-2 text-center">
                                            Until
                                        </div>
                                        <div class="col-md-4">
                                            <datepicker name="valid_until" placeholder="Valid until"></datepicker>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Priority</label>
                                        <div class="col-md-10">
                                            <integer-input name="priority" placeholder="Priority"></integer-input>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="ibox">

                        <div class="ibox-title">
                            <h5>Catalog Rule</h5>
                        </div>

                        <div class="ibox-content">
                            <div class="mb20"></div>

                            <div class="row">
                                <div class="col-md-12">

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Shops</label>
                                        <div class="col-md-10">
                                            <select2 multiple="" name="shops[]">
                                                @foreach($shops as $shop)
                                                    <option value="{{ $shop->id }}">{{ $shop->name }}</option>
                                                @endforeach
                                            </select2>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Country</label>
                                        <div class="col-md-10">
                                            <country-select class="form-control" multiple="" name="countries[]">
                                                @foreach($countries as $country)
                                                    <option value="{{ $country->id }}" icon="{{ $country->icon }}">{{ $country->name }}</option>
                                                @endforeach
                                            </country-select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Customer Group</label>
                                        <div class="col-md-10">
                                            <select2 class="form-control" multiple="" name="customer_groups[]">
                                                @foreach($customer_groups as $group)
                                                    <option value="{{ $group->id }}">{{ $group->name }}</option>
                                                @endforeach
                                            </select2>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Products</label>
                                        <div class="col-md-10">
                                            <product-search-select multiple="" name="products[]"></product-search-select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Feature Type</label>
                                        <div class="col-md-10">
                                            <select2 class="select2 form-control" multiple="" name="feature_types[]">
                                                @foreach($product_feature_types as $type)
                                                    <option value="{{ $type->id }}">{{ $type->info->name }}</option>
                                                @endforeach
                                            </select2>
                                        </div>
                                    </div>

                                    <hr />

                                    @foreach($shops as $shop)
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">{{ $shop->name }}</label>
                                            <div class="col-md-10">
                                                @if($shop->categories->count() > 0)
                                                    <select2 class="form-control" multiple="multiple" name="categories[]">
                                                        @foreach($shop->categories as $category)
                                                            <option value="{{ $category->id }}">{{ $category->info->name }}</option>
                                                        @endforeach
                                                    </select2>

                                                @else
                                                    <p class="form-control-static">
                                                        <a href="{{ route('new_category', ["shop_id" => $shop->id]) }}">Create first category</a>
                                                    </p>
                                                @endif
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>

                <div class="col-md-4">
                    <div class="ibox">
                        <div class="ibox-title">
                            <h5>Action</h5>
                        </div>

                        <div class="ibox-content">
                            {{ csrf_field() }}
                            <button class="btn btn-block btn-outline btn-success" type="submit"><i class="fa fa-floppy-o"></i> Save</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>

        <div class="mb30"></div>
    </div>
@stop