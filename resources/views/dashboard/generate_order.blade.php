@extends('dashboard.layout.main')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-6">
            <h2>@lang('orders.generate_orders_title')</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('home') }}">@lang('main.home')</a>
                </li>
                <li>
                    <a href="{{ route('generated_orders') }}">@lang('main.generated_orders')</a>
                </li>
                <li class="active">
                    <strong>@lang('orders.generate_orders_title')</strong>
                </li>
            </ol>
        </div>

        <div class="col-lg-6 text-right mt30">
            @if($processed == -1)
                <a class="btn btn-default" href="{{ route('generated_orders') }}"><i class="fa fa-arrow-left" aria-hidden="true"></i> @lang('main.go_back')</a>
                <a class="formulaModal btn btn-default" data-toggle="modal" data-target="#settingsModal"><i class="fa fa-cog" aria-hidden="true"></i> @lang('main.Settings')</a>
                <remove-file-button redir="{{ route('generated_orders') }}" item_id="{{ $file_id }}"><i class="fa fa-trash-o"></i> @lang('main.remove_file')</remove-file-button>
                @if(!$block)
                    <process-file-button redir="{{ route('generated_orders') }}" item_id="{{ $file_id }}"><i class="fa fa-files-o"></i> @lang('orders.generate_orders')</process-file-button>
                @else
                    <button class="btn btn-primary" style="cursor:;" title="@lang('main.errors_on_page')!" disabled><i class="fa fa-files-o"></i> @lang('orders.generate_orders')</button>
                @endif
            @else
                <a class="btn btn-default" href="{{ route('generated_orders') }}"><i class="fa fa-arrow-left" aria-hidden="true"></i> @lang('main.go_back')</a>
                <button class="btn btn-primary" title="@lang('orders.already_been_processed')!" disabled><i class="fa fa-files-o"></i> @lang('orders.generate_orders')</button>
            @endif
        </div>
    </div>

	<div class="wrapper wrapper-content animated fadeInRight ecommerce">

        @if($block)
            <div class="alert alert-danger">
                <ul>
                    <li>@lang('orders.products_not_found')!</li>
                </ul>
            </div>
        @endif

        <div class="row">
            <div class="col-lg-12">
                @foreach($orders as $order)
                <div class="ibox">
                    <div class="ibox-title">
                        @if(isset($order['inkoop admin']) && isset($order['eigenaar admin']) && isset($order['crediteurnummer']))
                            <?php $order['type'] = 'purchase_transfer'; ?>
                            <h5>@lang('orders.purchase_and_transfer')</h5>
                        @elseif(isset($order['inkoop admin']) && !isset($order['eigenaar admin']) && isset($order['crediteurnummer']))
                            <?php $order['type'] = 'purchase'; ?>
                            <h5>@lang('orders.purchase_order')</h5>
                        @elseif(isset($order['inkoop admin']) && isset($order['eigenaar admin']) && !isset($order['crediteurnummer']))
                            <?php $order['type'] = 'transfer'; ?>
                            <h5>@lang('orders.transfer_order')</h5>
                        @else
                            {{ die(trans('orders.error_in_file_format')) }}
                        @endif
                    </div>
                    <div class="ibox-content">
                        <ul class="list-unstyled" style="padding:0 8px;">
                            @if($order['type'] == 'purchase' || $order['type'] == 'purchase_transfer')
                                <li>
                                    <b>@lang('orders.seller'):</b>&nbsp;
                                    @if($order['crediteur'] != false)
                                        {{ $order['crediteur'].' ('.$order['crediteurnummer'].')' }}
                                    @else
                                        <abbr title="@lang('orders.creditor_not_found')">{{ strtoupper(trans('orders.unknown_creditor')) }}</abbr>&nbsp;
                                        ({{ $order['crediteurnummer'] }})&nbsp;
                                        <find-creditor class="btn-xs btn-default btn-outline" title="@lang('orders.search_for_creditor')" url="/customer/resolve-creditor/" code="{{ $order['crediteurnummer'] }}"><i class="fa fa-search"></i>&nbsp;@lang('main.search')</find-creditor>
                                    @endif
                                </li>
                                <li>
                                    <b>@lang('orders.buyer'):</b>&nbsp;
                                    {{ $order['inkoper'] != false ? $order['inkoper'].' ('.$order['inkoop debiteur'].')' : $order['inkoop admin'] }}
                                </li>
                                @if($order['type'] == 'purchase_transfer')
                                    <li>
                                        <b>@lang('orders.owner'):</b>&nbsp;
                                        {{ $order['eigenaar'] != false ? $order['eigenaar'].' ('.$order['eigenaar debiteur'].')' : $order['eigenaar admin'] }}
                                    </li>
                                @endif
                            @elseif($order['type'] == 'transfer')
                                <li><b>@lang('orders.seller'):</b> {{ $order['inkoper'] != false ? $order['inkoper'].' ('.$order['inkoop debiteur'].')' : $order['inkoop admin'] }}</li>
                                <li><b>@lang('orders.buyer'):</b> {{ $order['eigenaar'] != false ? $order['eigenaar'].' ('.$order['eigenaar debiteur'].')' : $order['eigenaar admin'] }}</li>
                            @endif
                            <li><b>@lang('orders.order_date'):</b> {{ date('d M Y', ($order['besteldatum'] / 1000)) }}</li>
                            <li><b>@lang('orders.delivery_date'):</b> {{ date('d M Y', ($order['leverdatum'] / 1000)) }}</li>
                            <li><b>@lang('orders.reference'):</b> {{ $order['referentie'] }}</li>
                        </ul>
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>@lang('main.article_code')</th>
                                <th>@lang('main.name')</th>
                                <th>@lang('main.brand')</th>
                                <th>@lang('main.quantity')</th>
                                <th>@lang('main.product_price')</th>
                                <th><abbr title="@lang('orders.average_purchase_price_abbr')">@lang('orders.average_purchase_price')</abbr> in %</th>
                                <th><abbr title="@lang('orders.average_purchase_price_abbr')">@lang('orders.average_purchase_price')</abbr> in €</th>
                                @if(strpos($order['type'], 'transfer') !== false)
                                    <th>@lang('orders.margin_in') %</th>
                                    <th>@lang('orders.margin_in') {{ $order['valuta'] == 'EUR' ? '&euro;' : '&#36;' }}</th>
                                    <th>@lang('orders.transfer_price')</th>
                                @endif
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($order['regels'] as $line)
                                    <tr{{ $line['naam'] == false ? ' class=danger' : '' }}>
                                        <td>{{ $line['artikelcode'] }}</td>
                                        <td>{!! $line['naam'] != false ? $line['naam'] : '<abbr title="'.trans('orders.product_not_found').'!"><i class="fa fa-exclamation-triangle"></i>&nbsp;'.strtoupper(trans('orders.unknown_product')).'</abbr>' !!}</td>
                                        <td>{!! $line['merk'] != false ? $line['merk'] : '<i class="fa fa-exclamation-triangle"></i>' !!}</td>
                                        <td>{{ $line['aantal'] }}</td>
                                        <td>{{ $order['valuta'] == 'EUR' ? '&euro;&nbsp;'.number_format($line['prijs'], 2, ',', '.') : '&#36;&nbsp;'.number_format($line['prijs'], 2, '.', ',') }}</td>
                                        <td>{{ $line['gip procent'] }}&nbsp;%</td>
                                        <td>{{ $order['valuta'] == 'EUR' ? '&euro;&nbsp;'.number_format($line['gip prijs'], 2, ',', '.') : '&#36;&nbsp;'.number_format($line['gip prijs'], 2, '.', ',') }}</td>
                                        @if(strpos($order['type'], 'transfer') !== false)
                                            <td>{{ $line['procent marge'] }}&nbsp;%</td>
                                            <td>{{ $order['valuta'] == 'EUR' ? '&euro;&nbsp;'.number_format($line['prijs marge'], 2, ',', '.') : '&#36;&nbsp;'.number_format($line['prijs marge'], 2, '.', ',') }}</td>
                                            <td>
                                                @if($order['valuta'] == 'EUR')
                                                    &euro;&nbsp;{{ number_format((($line['prijs'] + $line['gip prijs']) * (($line['gip procent'] / 100) + 1) + $line['prijs marge']) * (($line['procent marge'] / 100) + 1), 2, ',', '.') }}
                                                @else
                                                    &#36;&nbsp;{{ number_format((($line['prijs'] + $line['gip prijs']) * (($line['gip procent'] / 100) + 1) + $line['prijs marge']) * (($line['procent marge'] / 100) + 1), 2, '.', ',') }}
                                                @endif
                                            </td>
                                        @endif
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal inmodal fade" id="settingsModal" role="dialog"  aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">@lang('main.close')</span></button>
                    <h4 class="modal-title"><i class="fa fa-cog"></i> @lang('main.Settings')</h4>
                    <small class="font-bold">@lang('orders.connect_admin_to_debno')</small>
                </div>

                <form method="get" action="" id="formSettingsModal">
                    <div class="modal-body">
                        <div class="row" style="padding-bottom:12px;">
                            <div class="col-sm-6">
                            </div>
                            <div class="col-sm-3 text-center">
                                <b>@lang('main.administration')</b>
                            </div>
                            <div class="col-sm-3 text-center">
                                <b>@lang('main.debtor_number')</b>
                            </div>
                        </div>
                        @foreach($settings as $key => $val)
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-6" style="padding: 8px 12px;">{{ ucwords(str_replace('_', ' ', $key)) }}</label>
                                    <div class="col-sm-3">
                                        <div class="input-group m-b" style="width:100%;">
                                            <input type="text" class="form-control m-b" name="settings[{{ $key }}][admin]" placeholder="@lang('main.administration')" value="{{ $val['admin'] }}">
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="input-group m-b" style="width:100%;">
                                            <input type="text" class="form-control m-b" name="settings[{{ $key }}][debno]" placeholder="@lang('main.debtor_number')" value="{{ $val['debno'] }}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-white pull-left" data-dismiss="modal">@lang('main.close')</button>
                        <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o" aria-hidden="true"></i> @lang('main.save')</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop