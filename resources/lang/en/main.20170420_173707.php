<?php
/*************************************************************************
 Generated via "php artisan localization:missing" at 2017/04/10 17:38:45 
*************************************************************************/

return array (
  //==================================== Translations ====================================//
  'CMS' => 'CMS',
  'Settings' => 'Settings',
  'customers' => 'customers',
  'images' => 'images',
  'logout' => 'logout',
  'no' => 'no',
  'orders' => 'orders',
  'products' => 'products',
  'search' => 'search',
  'welcome' => 'welcome',
  'yes' => 'yes',
  //================================== Obsolete strings ==================================//
  'LLH:obsolete' => 
  array (
    'false' => 'false',
    'true' => 'true',
  ),
);