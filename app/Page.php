<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    public function page_infos()
    {
    	return $this->hasMany(PageInfo::class);
    }

    public function language()
    {
    	return $this->belongsTo(Language::class);
    }

    public function shop()
    {
        return $this->belongsTo(Shop::class);
    }
    
    public function info($language_id = false, $frontpage = false)
    {
    	$info = false;
    	
        if($language_id)
        {
            $info = $this->page_infos()->where("language_id", $language_id)->first();
        }

        if(!$info && !$frontpage && auth())
        {        
            $info = $this->page_infos()->where("language_id", json_decode(auth()->user()->options)->language_id)->first();
        }

        if(!$info)
        {
            $info = $this->page_infos()->whereNotNull("language_id")->first();
        }

        return $info;
    }
}
