<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRewardProgramsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reward_programs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('shop_id');
            $table->string('mandant');
            $table->json('rules');
            $table->boolean('active');
            $table->timestamps();

            $table->foreign('shop_id')
                ->references('id')
                ->on('shops')
                ->onDelete('cascade');
        });

        Schema::create('reward_program_orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order_id');
            $table->integer('reward_program_id')->unsigned();
            $table->string('company');
            $table->string('iban');
            $table->decimal('total_price');
            $table->boolean('payout')->default(false);
            $table->timestamps();

            $table->foreign('order_id')
                ->references('id')
                ->on('orders')
                ->onDelete('cascade');

            $table->foreign('reward_program_id')
                ->references('id')
                ->on('reward_programs')
                ->onDelete('cascade');
        });

        Schema::create('reward_program_order_lines', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('reward_program_order_id')->unsigned();
            $table->integer('product_id')->nullable();
            $table->string('name');
            $table->integer('qty');
            $table->boolean('credit');
            $table->decimal('price');
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('reward_program_order_id')
                ->references('id')
                ->on('reward_program_orders')
                ->onDelete('cascade');

            $table->foreign('product_id')
                ->references('id')
                ->on('products')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reward_program_order_lines');
        Schema::dropIfExists('reward_program_orders');
        Schema::dropIfExists('reward_programs');
    }
}
