<?php
namespace App\Classes\Orders;


/**
 * Class Order
 * @package App\Classes\Orders
 */
use App\Cart;
use App\Product;

/**
 * Class Order
 * @package App\Classes\Orders
 */
class Order
{
    /**
     * @var
     */
    private $delivery;

    /**
     * @var
     */
    protected $totalNumProducts = 0;

    /**
     * @var int
     */
    private $shipmentNodeInt = 4;

    /**
     * @var
     */
    public $order;

    /**
     * @var bool
     */
    private $freeShipment = false;

    /**
     * @var bool
     */
    private $dropship = false;

    /**
     * @var float
     */
    private $shippingCost = 0;

    /**
     * @var mixed
     */
    private $shop;

    /**
     * @return mixed
     */
    public function getShipmentMethod()
    {
        return $this->order->shipment_method;
    }

    /**
     * @return mixed
     */
    public function getShipmentNodeInt()
    {
        return $this->shipmentNodeInt;
    }

    /**
     * @param mixed $shipmentNodeInt
     */
    public function setShipmentNodeInt($shipmentNodeInt)
    {
        if($shipmentNodeInt < $this->shipmentNodeInt){
           $this->shipmentNodeInt = $shipmentNodeInt;
        }
    }

    /**
     * @return mixed
     */
    public function calculateTotal(){
        $total = 0.0;
        foreach ($this->order->lines()->get() as $orderLine) {
            $product = Product::findOrFail($orderLine->product_id);
            if ($product->product_feature_type_id == 2) {
                $total += $orderLine->price*$orderLine->no_products;
            } else {
                $total += (new ShopProductPrice($product, $this->shop))->retail_price*$orderLine->no_products;
            }
        }
        return $total;
    }


    public function setTotal($total){
        $this->order->total_price = $total;
        $this->order->save();
    }

    /**
     * @param mixed $shipmentMethod
     */
    public function setShipmentMethod($shipmentMethod)
    {
        $this->order->shipment_method = $shipmentMethod;
        $this->order->save();
    }


    public function getShipmentNode()
    {
        return $this->order->shipment_node;
    }

    public function setShipmentNode()
    {
        foreach ($this->order->lines()->get() as $orderLine){
            $product = Product::findOrFail($orderLine->product_id);
            if($orderLine->third_party_stock){
                if($product->product_feature_type_id == 1){
                    $this->setShipmentNodeInt(4);
                }else{
                    $this->setShipmentNodeInt(3);
                }
            }else{
                if($product->product_feature_type_id == 1){
                    $this->setShipmentNodeInt(2);
                }else{
                    $this->setShipmentNodeInt(1);
                }
            }
        }
        switch ($this->getShipmentNodeInt()) {
            case 1:
                $this->order->shipment_node = 'products';
                break;
            case 2:
                $this->order->shipment_node = 'tyres';
                break;
            case 3:
                $this->order->shipment_node = 'third_party_products';
                break;
            case 4:
                $this->order->shipment_node = 'third_party_tyres';
                break;
        }

        $this->order->save();
    }


    /**
     * @return mixed
     */
    public function getFreeShipment()
    {
        return $this->freeShipment;
    }

    /**
     * @param mixed $freeShipment
     */
    public function setFreeShipment($freeShipment)
    {
        $this->freeShipment = $freeShipment;
    }

    /**
     * @return mixed
     */
    public function getDelivery()
    {
        return $this->delivery;
    }

    /**
     * @param mixed $delivery
     */
    public function setDelivery(Collection $delivery)
    {
        $this->delivery = new Collection();
        $this->delivery->av_deb = $delivery->av_deb;
        $this->delivery->deliver_company  = $delivery->deliver_company ?? null;
        $this->delivery->deliver_street  = $delivery->deliver_street ?? null;
        $this->delivery->city  = $delivery->city ?? null;
        $this->delivery->zipcode  = $delivery->zipcode ?? null;
        $this->delivery->company  = $delivery->deliver_company ?? null;
        $this->delivery->deliver_housenumber  = $delivery->deliver_housenumber ?? null;
        $county  = $delivery->deliver_country ?? null;
        if($county){
            $county = \App\Country::where('iso_code_2', $county)->first()->id;
        }
        $this->delivery->deliver_country_id = $county;
    }


    /**
     * @param $orderdata
     */
    public function createFromJson($orderdata)
    {
        $this->shop = \App\Shop::findOrFail($orderdata->shop_id);
        $order = new \App\Order();
        $orderdata = json_decode($orderdata);
        $order->ref = $orderdata->ref;
        $order->placed_on = $orderdata->placed_on;
        $order->ip = $orderdata->ip;
        $order->customer_id = $orderdata->customer_id;
        $order->address = $orderdata->address;
        $order->housenumber = $orderdata->housenumber;
        $order->city = $orderdata->city;
        $order->postal_code = $orderdata->postal_code;
        $order->mand = $orderdata->mand;
        $order->dropship = $orderdata->dropship;
        $order->internal_comment = $orderdata->internal_comment;
        $order->customer_comment = $orderdata->customer_comment;
        $order->customer_refference = $orderdata->customer_refference;
        $order->country_id = $orderdata->country_id;
        $order->shipment_method = $orderdata->shipment_method;
        $order->shipment_node = $orderdata->shipment_node;
        $order->shipment_costs = $orderdata->shipment_costs;
        $order->total_price = 0.00;
        $order->bem = 0.00;
        $order->profit = $orderdata->profit;
        $order->profit_payed = $orderdata->profit_payed;
        $order->order_status_id = $orderdata->order_status_id;
        $order->payment_id = $orderdata->payment_id;
        $order->shop_id = $orderdata->shop_id;
        $order->checked = $orderdata->checked;
        $order->save();
        $this->order = $order;
    }

    /**
     * @param $orderid
     */
    public function createFromId($orderid)
    {
        $this->order = \App\Order::findOrFail($orderid);
        $this->shop = \App\Shop::findOrFail($this->order->shop_id);
        return $this->order;
    }


    /**
     * @param $shopRequest
     * @return \App\Order
     */
    public function createFromRequest($shopRequest)
    {
        $this->shop = \App\Shop::findOrFail($shopRequest->shop->id);
        $order = new \App\Order();
        $order->ref = strtoupper(str_random(9));
        $order->placed_on = Carbon::now()->toDateTimeString();
        $order->ip = $shopRequest->customer_session->ip;
        $order->customer_id = $shopRequest->customer_session->customer->id;
        $order->address = $shopRequest->customer_session->customer->billing_street;
        $order->housenumber = $shopRequest->customer_session->customer->billing_housenumber;
        $order->city = $shopRequest->customer_session->customer->billing_city;
        $order->postal_code = $shopRequest->customer_session->customer->billing_zipcode;
        $order->customer_code = $this->delivery->av_deb;
        $order->phone = $shopRequest->customer_session->customer->mobile_phone ?? null;
        $order->email = $shopRequest->customer_session->customer->email ?? null;
        $order->deliver_company = $this->delivery->deliver_company ?? null;
        $order->deliver_street = $this->delivery->deliver_street ?? null;
        $order->deliver_housenumber = $this->delivery->deliver_housenumber ?? null;
        $order->deliver_country_id = $this->delivery->deliver_country_id ?? null;
        $order->deliver_city = $this->delivery->city ?? null;
        $order->deliver_zipcode = $this->delivery->zipcode ?? null;
        $order->mand = $shopRequest->customer_session->customer->mand;
        $order->dropship = $this->dropship;
        $order->internal_comment = '';
        $order->customer_comment = '';
        $order->customer_refference = '';
        $order->country_id = $shopRequest->customer_session->customer->billing_country_id;
        $order->shipment_method = '**';
        $order->shipment_node = null;
        $order->shipment_costs = 0.00;
        $order->total_price = 0.00;
        $order->bem = 0.00;
        $order->profit = 0.00;
        $order->profit_payed = 0;
        $order->order_status_id = 3;
        $order->payment_id = $shopRequest->payment_method;
        $order->shop_id = $shopRequest->shop->id;
        $order->checked = false;
        if($shopRequest->customer_session->cart->additional_fee){
            $order->additional_costs = $shopRequest->customer_session->cart->additional_fee;
        }
        $order->save();

        $this->order = $order;
        foreach ($shopRequest->customer_session->cart->cart_products as $product) {
            $this->addProduct($product->product_id, $product->qty, $product->retail_price);
        }

        if($shopRequest->customer_session->cart->shipment_fee){
            $this->setShippingCost($shopRequest->customer_session->cart->shipment_fee);
        }else{
            $this->setShippingCost(0.00);
        }

        return $this->createFromRequestHook($order, $shopRequest->customer_session->cart->cart_products);
    }

    /**
     *
     */
    public function  createFromRequestHook($order, $products) {
        return $order;
    }

    /**
     *
     */
    public function createNew()
    {
        $this->order = new \App\Order;
        return $this->order;
    }

    /**
     *
     */
    public function createLast()
    {
        $order = \App\Order::where('checked', false)->whereHas('order_status', function($q){
            $q->where('can_ship', true);
        })
            ->orderBy('placed_on', 'ASC')
            ->first();

        if($order){
            return $this->order = $order;
        }else{
            return false;
        }
    }

    /**
     * @param $productID
     * @param $amount
     * @param $price
     * @param string $comment
     * @param bool $third_party
     */
    public function addProduct($productID, $amount, $price, $comment = '', $third_party = false)
    {
        $line = new \App\OrderLine();
        $line->order_id = $this->order->id;
        $line->product_id = $productID;

        $line->price = $price;
        $line->customer_comment = $comment;
        $product = Product::findOrFail($productID);
        $stock = $product->product_stocks->whereIn('warehouse_id', [1, 2])->first()->amount;
        $third_party_amount = ($stock?$amount%$stock:$amount);
        if ($third_party && $product->stock_management) {
            $line->third_party_stock = true;
            $line->no_products = $third_party_amount;
            $line->price = $price;//$amount * $price;
        } else {
            if ($amount > $stock && $product->stock_management) {

                if ($stock < 1) {
                    $line->third_party_stock = true;
                } else {
                    $line->third_party_stock = false;
                }
                if ($third_party_amount != 0) {
                    $this->addProduct($productID, $third_party_amount, $price, $comment, true);
                    return;
                }
            } else {
                $third_party_amount = 0;
                $line->third_party_stock = false;
            }
            $line->no_products = $amount - $third_party_amount;
            $line->price = $price;// * ($amount - $third_party_amount);
        }
        $line->save();

        if ($product->product_feature_type->infos->first()->name != 'Service') {
            $this->totalNumProducts += $amount;
            $this->adjustShipment();
            $this->addProductHook($productID, $amount, $price, $line->id);
        }else{
            $this->addServiceHook($productID, $amount, $price, $line->id);
        }

    }

    /**
     * @param $productid
     */
    public function addProductHook($productid, $amount, $price, $lineId)
    {
        $this->setTotal($this->calculateTotal());
    }

    /**
     * @param $productid
     */
    public function addServiceHook($productid, $amount, $price, $lineId)
    {

    }

    /**
     * @return mixed
     */
    public function getOrderFormatted()
    {
        $order = $this->order;
        unset($order->payment);
        $order->country_code = $order->country->iso_code_2;
        $order->country_name = $order->country->name;
        unset($order->country);
        $order->company_name = $order->customer->company_name;
        unset($order->customer);
        $order->shop_name = $order->shop->name;
        $order->AV_admin = $order->shop->AV_admin;
        unset($order->shop);
        $order->productCount = $this->countProducts($order);
        $tyres = $order->filter(true, false);
        $products = $order->filter(false, false);
        $third_party_tyres = $order->filter(true, true);
        $third_party_products = $order->filter(false, true);
        $service_products = $order->service_products;
        if (count($tyres)) $order->tyres = $tyres->merge($service_products);
        if (count($products)) $order->products = $products->merge($service_products);
        if (count($third_party_tyres)) $order->third_party_tyres = $third_party_tyres->merge($service_products);
        if (count($third_party_products)) $order->third_party_products = $third_party_products->merge($service_products);
        return $this->formattedOrderHook($order);
    }

    /**
     * @return mixed
     */
    public function countProducts($order){
        //dd($order->line_products());
        $result = [];
        foreach ($order->line_products() as $line_product){
            $productType = \App\ProductFeatureType::find($line_product->product->product_feature_type_id);
            if($productType){
                if(isset($result[str_slug($productType->info->name)])) {
                    $result[str_slug($productType->info->name)] = $line_product->no_products + $result[str_slug($productType->info->name)];
                }else{
                    $result[str_slug($productType->info->name)] = $line_product->no_products;
                }
            }
        }
        return $result;
    }

    /**
     * @return mixed
    */
    public function formattedOrderHook($order){
        return $order;
    }

    /**
     * @return mixed
     */
    public function checkOrder()
    {
        $this->order->checked = true;
        return $this->order->save();
    }

    /**
     * @return mixed
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @param mixed $order
     */
    public function setOrder($order)
    {
        $this->order = $order;
        $this->order->save();
    }
    
    /**
     * @return mixed
     */
    public function getProfit()
    {
        return $this->order->profit;
    }

    /**
     * @param mixed profit$
     */
    public function setProfit($profit)
    {
        $this->order->profit = $profit;
        $this->order->save();
    }

    /**
     *
     */
    public function adjustShipment()
    {
        if (!$this->freeShipment) {
            $this->order->shipment_costs = $this->shippingCost;
        }
    }

    /**
     * @return boolean
     */
    public function isDropship()
    {
        return $this->dropship;
    }

    /**
     * @param boolean $dropship
     */
    public function setDropship($dropship)
    {
        $this->dropship = $dropship;
    }

    /**
     * @return float
     */
    public function getShippingCost()
    {
        return $this->shopment_costs;
    }

    /**
     * @param float $shippingCost
     */
    public function setShippingCost($shippingCost)
    {
        $this->shopment_costs = $shippingCost;
    }


}
