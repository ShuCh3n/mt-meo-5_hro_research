import select2 from 'select2'
import flot from 'flot'
import metismenu from 'metismenu'
import slimscroll from 'jquery-slimscroll'
import icheck from 'icheck'
import bootstrap_select from 'bootstrap-select'
import datepicker from 'bootstrap-datepicker'

require('./bootstrap');
require('./theme');

import Select2 from './components/Select2.vue'
import FindProduct from './components/FindProduct.vue'
import LanguageSelect from './components/LanguageSelect.vue'
import Summernote from './components/Summernote.vue'
import AmountInput from './components/AmountInput.vue'
import PercentageInput from './components/PercentageInput.vue'
import IntegerInput from './components/IntegerInput.vue'
import MotorSkuInput from './components/MotorSkuInput.vue'
import MotorYearFilter from './components/MotorYearFilter.vue'
import MotorCategoryFilter from './components/MotorCategoryFilter.vue'
import TyreSizeInput from './components/TyreSizeInput.vue'
import ViewMotorPage from './components/ViewMotorPage.vue'
import MotorProfile from './components/MotorProfile.vue'
import ImageSelector from './components/ImageSelector.vue'
import CountrySelect from './components/CountrySelect.vue'
import AccountView from './components/AccountView.vue'
import ProductGraph from './components/ProductGraph.vue'
import ProductGraphOptions from './components/ProductGraphOptions.vue'
import Dropzone from './components/Dropzone.vue'
import AnalistHistory from './components/AnalistHistory.vue'
import RemoveButton from './components/RemoveButton.vue'
import RemoveFileButton from './components/RemoveFileButton.vue'
import ProcessFileButton from './components/ProcessFileButton.vue'
import FindCreditor from './components/FindCreditor.vue'
import AnalistUploader from './components/AnalistUploader.vue'
import Datepicker from './components/Datepicker.vue'
import ProductSearchSelect from './components/ProductSearchSelect.vue'
import PhoneInput from './components/PhoneInput.vue'
import PasswordInput from './components/PasswordInput.vue'
import TaxSelect from './components/TaxSelect.vue'
import ProductCsvImportButton from './components/ProductCsvImportButton.vue'
import ProductCsvImporter from './components/ProductCsvImporter.vue'

new Vue({
	el: '#app',
	data(){
		return {
            csrf_token: null
        }
	},
	components: {
		Select2,
		FindProduct,
		LanguageSelect,
		Summernote,
		AmountInput,
		PercentageInput,
		IntegerInput,
		MotorSkuInput,
		MotorYearFilter,
		MotorCategoryFilter,
		TyreSizeInput,
		ViewMotorPage,
		MotorProfile,
		ImageSelector,
		CountrySelect,
		AccountView,
		ProductGraph,
		ProductGraphOptions,
		Dropzone,
		AnalistHistory,
		RemoveButton,
		RemoveFileButton,
		ProcessFileButton,
		AnalistUploader,
		Datepicker,
		ProductSearchSelect,
		PhoneInput,
		PasswordInput,
		TaxSelect,
		ProcessFileButton,
		FindCreditor,
		ProductCsvImportButton,
		ProductCsvImporter
	},
	mounted: function(){
		this.csrf_token = document.querySelector('#_token').getAttribute('value')
	}
});
