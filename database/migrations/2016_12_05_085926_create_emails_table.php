<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('email_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('identifier');
            $table->boolean('newsletter');
            $table->timestamps();
        });

        Schema::create('email_type_infos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('language_id')->unsigned();
            $table->integer('email_type_id')->unsigned();
            $table->string('name');
            $table->timestamps();

            $table->foreign('language_id')
                ->references('id')
                ->on('languages')
                ->onDelete('cascade');

            $table->foreign('email_type_id')
                ->references('id')
                ->on('email_types')
                ->onDelete('cascade');
        });

        DB::table('email_types')->insert([
            [
                "identifier"    => "new_customer",
                "newsletter"    => false
            ],
            [
                "identifier"    => "contact_autoreply",
                "newsletter" => false
            ],
            [
                "identifier"    => "order_confirmation",
                "newsletter" => false
            ],
            [
                "identifier"    => "new_password",
                "newsletter" => false
            ],
        ]);

        DB::table('email_type_infos')->insert([
            [
                "language_id"   => 2,
                "email_type_id" => 1,
                "name"          => "Nieuwe Klant"
            ],
            [
                "language_id"   => 2,
                "email_type_id" => 2,
                "name"          => "Contact Autoreply"
            ],
            [
                "language_id"   => 2,
                "email_type_id" => 3,
                "name"          => "Orderbevestiging"
            ],
            [
                "language_id"   => 2,
                "email_type_id" => 4,
                "name"          => "Nieuw Wachtwoord Instellen"
            ],

        ]);

        Schema::create('emails', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('email_type_id')->unsigned();
            $table->integer('shop_id')->unsigned();
            $table->boolean('active');
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('email_type_id')
                ->references('id')
                ->on('email_types')
                ->onDelete('cascade');

            $table->foreign('shop_id')
                ->references('id')
                ->on('shops')
                ->onDelete('cascade');
        });

        Schema::create('email_infos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('email_id')->unsigned();
            $table->integer('language_id')->unsigned();
            $table->string('title');
            $table->string('view_file');
            $table->timestamps();

            $table->foreign('email_id')
                ->references('id')
                ->on('emails')
                ->onDelete('cascade');

            $table->foreign('language_id')
                ->references('id')
                ->on('languages')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('email_infos');
        Schema::dropIfExists('emails');
        Schema::dropIfExists('email_type_infos');
        Schema::dropIfExists('email_types');
    }
}
