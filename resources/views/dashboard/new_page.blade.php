@extends('dashboard.layout.main')

@section('stylesheet')
    <link href="/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="/css/plugins/select2/select2.min.css" rel="stylesheet">

    <link href="/css/plugins/summernote/summernote.css" rel="stylesheet">
    <link href="/css/plugins/summernote/summernote-bs3.css" rel="stylesheet">
@stop

@section('javascript')
    <!-- iCheck -->
    <script src="/js/plugins/iCheck/icheck.min.js"></script>

    <!-- Select2 -->
    <script src="/js/plugins/select2/select2.full.min.js"></script>

    <!-- SUMMERNOTE -->
    <script src="/js/plugins/summernote/summernote.min.js"></script>

    <script>
        $(function(){
            $(".select2").select2({
                placeholder: "Select"
            });

            $('.summernote').summernote();

            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });

        });
    </script>
@stop

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>{{ trans('pages.inquiry_detail_header') }}</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('home') }}">{{ trans('menu.home') }}</a>
                </li>
                <li>
                    <a href="">{{ trans('menu.inquiries') }}</a>
                </li>
                <li class="active">
                    <strong>{{ trans('page.inquiry') }}</strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">
        {!! session()->has('success')? '<div class="alert alert-success alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>' . session('success') . '</div>' : '' !!}

        <form id="profileForm" method="post" class="form-horizontal">
            <div class="row">
                <div class="col-md-8">
                    <div class="ibox">

                        <div class="ibox-title">
                            <h5>New Page</h5>
                        </div>

                        <div class="ibox-content">
                            <div class="mb20"></div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Active</label>
                                        <div class="col-md-5">
                                            <label><input type="radio" class="form-control i-checks" name="active" value="1" checked> Enabled</label>
                                        </div>
                                        <div class="col-md-5">
                                            <label><input type="radio" class="form-control i-checks" name="active" value="0"> Disabled</label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Shops</label>

                                        <div class="col-md-10">
                                            <select class="select2 form-control" multiple="multiple" name="shops[]">
                                                @foreach($shops as $shop)
                                                    <option value="{{ $shop->id }}">{{ $shop->name }}</option>
                                                @endforeach
                                            </select>
                                            <div class="mb15"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="tabs-container">
                        <ul class="nav nav-tabs">
                            @foreach($languages as $language)
                                <li class="{{ ($language->name != 'Nederlands')?: 'active' }}">
                                    <a data-toggle="tab" href="#lang_{{ $language->id }}">
                                        <img alt="flag" src="/img/flags/16/{{ $language->icon }}.png">
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                        <div class="tab-content">
                            @foreach($languages as $language)
                                <div id="lang_{{ $language->id }}" class="tab-pane {{ ($language->name != 'Nederlands')?: 'active' }}">
                                    <div class="panel-body">
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">Page Title</label>
                                            <div class="col-md-10"><input type="text" class="form-control" name="page_info[{{ $language->id }}][title]" placeholder="Title"></div>
                                        </div>                                 

                                        <div class="form-group">
                                            <label class="col-md-2 control-label">Meta Description</label>
                                            <div class="col-md-10"><input type="text" class="form-control" name="page_info[{{ $language->id }}][meta_description]" placeholder="Describe short"></div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-2 control-label">Meta Link</label>
                                            <div class="col-md-10"><input type="text" class="form-control" name="page_info[{{ $language->id }}][meta_link]" placeholder="Custom Link"></div>
                                        </div>                                    

                                        <div class="form-group">
                                            <label class="col-md-2 control-label">Description</label>
                                            <div class="col-md-10">
                                                <textarea class="summernote" name="page_info[{{ $language->id }}][body]"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>    
                </div>

                <div class="col-md-4">
                    <div class="ibox">
                        <div class="ibox-title">
                            <h5>Action</h5>
                        </div>

                        <div class="ibox-content">

                            {{ csrf_field() }}
                            <button class="btn btn-block btn-outline btn-success" type="submit"><i class="fa fa-floppy-o"></i> Save</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>

        <div class="mb30"></div>
    </div>
@stop