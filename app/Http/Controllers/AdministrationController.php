<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Product;

/**
 * @resource Administration endpoints
 *
 * For fetching data present in accountview administrations, the **AdminToken** header is required.
 */

class AdministrationController extends Controller
{
    public function searchProduct($sku)
    {
        $product = Product::where('sku', $sku)->with(['motorProduct.year' => function($q){
            $q->with('category');
        }])->with(['motors' => function($q){
             $q->with('vehicle_years')->distinct();
             $q->with('vehicle_brand');
		}])->firstOrFail();
        $product->msdsFiles = $product->msds->map(function($item, $key){
            return ENV('CDN_URI').'/files/'.$item->filename.'.'.$item->extension;
        });
        unset($product->msds);

        $imagesSource = $product->allimages();
        $images = [];

        foreach($imagesSource as $image)
        {
            $images[] = array(
                'img' => env('CDN_URI') . '/images/' . $image->filename . '.png',
                'thumb' => env('CDN_URI') . '/images/' . $image->filename . '_thumb.png'
            );
        }

        $product->productimages = $images;
        unset($product->images);
        unset($product->groups);

        return $product;
    }

    public function products()
    {
        $products = Product::with(['motorProduct.year' => function($q){
            $q->with('category');
        }])->with(['motors' => function($q){
             $q->with('vehicle_years')->distinct();
             $q->with('vehicle_brand');
		}])->paginate();
        foreach ($products as $product) {

            $product->msdsFiles = $product->msds->map(function($item, $key){
                return ['url' => ENV('CDN_URI').'/files/'.$item->filename.'.'.$item->extension];
            });
            unset($product->msds);

            $imagesSource = $product->allimages();
            $images = [];

            foreach($imagesSource as $image)
            {
                $images[] = array(
                    'img' => env('CDN_URI') . '/images/' . $image->filename . '.png',
                    'thumb' => env('CDN_URI') . '/images/' . $image->filename . '_thumb.png'
                );
            }

            $product->productimages = $images;
            unset($product->images);
            unset($product->groups);
        }
        return $products;
    }
}
