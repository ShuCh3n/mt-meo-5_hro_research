<?php

namespace App\Classes\API\Vehicle;

class Vehicle
{
	protected $request;
	protected $paginate;

    public function __construct($request)
    {
    	$this->request = $request;
    	$this->paginate = 100;

    	if($this->request->paginate && is_numeric($this->request->paginate) && $this->request->paginate < 500)
    	{
    		$this->paginate = $this->request->paginate;
    	}
    }
}