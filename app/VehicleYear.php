<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VehicleYear extends Model
{
    public function products()
    {
        return $this->belongsToMany(Product::class, 'motor_product_vehicle_year');
    }

	public function motors()
	{
		return $this->belongsToMany(Motor::class, 'motor_product_vehicle_year');
	}

    public function scopeThisMotor($query, $motor_id)
    {
    	$query->whereHas('motors', function($query) use ($motor_id){
    		$query->where('id', $motor_id);
    	});
    }

    public static function getByYear($year)
    {
        if(is_numeric($year))
        {
            return self::firstOrCreate(['year' => $year]);
        }
    }
}
