@extends('dashboard.layout.main')

@section('predefined-javascript')
    <script>
        function dropzone_callback(file, response)
        {
            html  = '<tr class="success" id="' + response.id + '">';
            html += '<td><i class="fa fa-times"></i></td>';
            html += '<td><a href="' + response.file_link + '">' + file.name + '</a></td>';
            html += '<td>' + response.user + '</td>';
            html += '<td class="hidden-xs">' + response.date + '</td>';
            html += '<td class="text-info text-right"><i class="fa fa-check-circle-o" aria-hidden="true"></i></td>';
            html += '</tr>';
            $("tbody").prepend(html);
            $(".dz-preview").remove();
            $(".dz-message").show();
        }
    </script>
@stop

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>@lang('main.generated_orders')</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('home') }}">@lang('main.home')</a>
                </li>
                <li class="active">
                    <strong>@lang('main.generated_orders')</strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight ecommerce">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>@lang('orders.import_excel')</h5>
                    </div>
                    <div class="ibox-content">
                        <dropzone action="{{ route('upload_generated_order_file') }}" max_files="10" max_files_size="15" accepted_files=".xlsx, .xls, .csv" success_callback="dropzone_callback"></dropzone>
                    </div>
                </div>

                <div class="ibox">
                    <div class="ibox-title">
                        <h5>@lang('orders.generate_from_file')</h5>
                    </div>
                    <div class="ibox-content">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>@lang('orders.processed')</th>
                                <th>@lang('main.filename')</th>
                                <th>@lang('main.user')</th>
                                <th class="hidden-xs">@lang('main.date')</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($histories as $history)
                                    <tr id="{{ $history->id }}">
                                        @if($history->processed == 1)
                                            <td><i class="fa fa-check"></i></td>
                                        @else
                                            <td><i class="fa fa-times"></i></td>
                                        @endif
                                        <td><a href="{{ route('review_generated_order_file', $history->id) }}">{{ $history->original_filename }}</a></td>
                                        <td>{{ $history->user->firstname }}</td>
                                        <td class="hidden-xs">{{ date('d M Y', strtotime($history->created_at)) }}</td>
                                        <td class="text-right">
                                            @if($history->processed == -1)
                                            <remove-button class="btn-outline btn-xs" url="/generated-order/remove/" item_id="{{ $history->id }}">
                                                <i class="fa fa-trash-o"></i>
                                            </remove-button>
                                            {{--@else
                                                <button class="btn btn-danger btn-outline btn-xs" title="These orders have already been processed!" disabled><i class="fa fa-trash"></i></button>--}}
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop