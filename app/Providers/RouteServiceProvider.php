<?php

namespace App\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //

        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapWebRoutes();

        $this->mapApiRoutes();

        $this->mapSyncRoutes();

        $this->mapAdministrationRoutes();

        $this->mapPublicRoutes();

        $this->mapHealthRoute();

        $this->mapCustomerAPIRoute();
    }

    protected function mapWebRoutes()
    {
        Route::group([
            'middleware' => 'web',
            'namespace' => $this->namespace,
        ], function ($router) {
            require base_path('routes/web.php');
        });
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::group([
            'middleware' => 'api',
            'namespace' => $this->namespace . '\API',
            'prefix' => 'api/v1',
            'name'  => 'api.',
        ], function ($router) {
            require base_path('routes/api.php');
        });
    }

    protected function mapAdministrationRoutes()
    {
        Route::group([
            'middleware' => 'administration',
            'namespace' => $this->namespace,
            'prefix' => 'admin/v1',
        ], function ($router) {
            require base_path('routes/administration.php');
        });
    }

    protected function mapPublicRoutes()
    {
        Route::group([
            'middleware' => 'public',
            'namespace' => $this->namespace,
            'prefix' => 'public/v1',
        ], function ($router) {
            require base_path('routes/public.php');
        });
    }

    protected function mapHealthRoute()
    {
        Route::group([
            'middleware' => 'publicNoRate',
            'namespace' => $this->namespace,
            'prefix' => 'public/health/',
        ], function ($router) {
            require base_path('routes/public.php');
        });
    }

    protected function mapCustomerAPIRoute()
    {
        Route::group([
            'middleware' => 'customerAPI',
            'namespace' => $this->namespace,
            'prefix' => 'customerapi/v1',
        ], function ($router) {
            require base_path('routes/customerAPI.php');
        });
    }

    protected function mapSyncRoutes()
    {
        Route::group([
            'middleware' => 'sync',
            'namespace' => $this->namespace . '\Sync',
            'prefix' => 'sync/v1',
        ], function ($router) {
            require base_path('routes/sync.php');
        });
    }
}
