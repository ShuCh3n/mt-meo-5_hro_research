<?php
/*************************************************************************
 Generated via "php artisan localization:missing" at 2017/04/10 16:46:28
*************************************************************************/

return array (
  'bulk_edit' => 'In bulk bewerken',
  'action' => 'Actie',
  'active' => 'Actief',
  'all' => 'Alles',
  'ean' => 'EAN',
  'edit' => 'Bewerk',
  'id' => 'ID',
  'new_product' => 'Nieuw product',
  'productname' => 'Productnaam',
  'products' => 'Producten',
  'retailprice' => 'Verkoopprijs',
  'shop' => 'Winkel',
  'sku' => 'Artikelcode',
  'view' => 'Bekijk',
);
