<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Email extends Model
{
    public function shop()
    {
    	return $this->belongsTo(Shop::class);
    }

    public function infos()
    {
    	return $this->hasMany(EmailInfo::class);
    }

    public function info()
    {
    	return $this->hasOne(EmailInfo::class);
    }

    public function email_type()
    {
        return $this->belongsTo(EmailType::class);
    }
}
