<?php
/**
 * Created by IntelliJ IDEA.
 * User: stenn
 * Date: 24-10-2016
 * Time: 15:00
 */

namespace App\Classes\Orders;


class OrderFactory
{
    public static function create($request, $order)
    {
        switch ($request->shop->id) {
        	case 3:
        		return new BandenBestellen($request, $order);
        		break;
        	
        	default:
        		return new Order;
        		break;
        }
    }
}