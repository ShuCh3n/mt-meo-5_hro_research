@extends('dashboard.layout.main')

@section('content')
	<div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>{{ trans('pages.inquiry_detail_header') }}</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('home') }}">{{ trans('menu.home') }}</a>
                </li>
                <li>
                    <a href="">{{ trans('menu.inquiries') }}</a>
                </li>
                <li class="active">
                    <strong>{{ trans('page.inquiry') }}</strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">
        {!! session()->has('success')? '<div class="alert alert-success alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>' . session('success') . '</div>' : '' !!}

        <form id="profileForm" method="post" class="form-horizontal">
            <div class="row">
                <div class="col-md-8">
                    <div class="ibox">

                        <div class="ibox-title">
                            <h5>New order</h5>
                        </div>

                        <div class="ibox-content">
                            <div class="mb20"></div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Shop</label>
                                        <div class="col-md-10">
                                            @if($shops->count() > 0)
                                                <select class="form-control select2" id="pick-shop" name="shop">
                                                    <option value=""></option>
                                                    @foreach($shops as $shop)
                                                        <option value="{{ $shop->id }}">{{ $shop->name }}</option>
                                                    @endforeach
                                                </select>
                                            @else
                                                <p class="form-control-static">
                                                    <a href="{{ route('new_shop') }}">Create first shop</a>
                                                </p>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Customer</label>
                                        <div class="col-md-10">
                                            <select class="form-control select2 find-customer" name="customer">
                                                <option value=""></option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Shipping Address</label>
                                        <div class="col-md-8">
                                            <select class="form-control select2 customer_addresses" name="customer_address">
                                            </select>
                                        </div>
                                        <div class="col-md-2">
                                            <a href="{{ route('new_customer_address') }}" class="btn btn-block btn-primary"><i class="fa fa-plus" aria-hidden="true"></i> Add Address</a>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Order Status</label>
                                        <div class="col-md-10">
                                            <select class="form-control select2" name="order_status">
                                                <option value=""></option>
                                                @foreach($order_statusues as $status)
                                                    <option value="{{ $status->id }}">{{ $status->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Payment</label>
                                        <div class="col-md-10">
                                            <select class="form-control select2" name="payment">
                                                <option value=""></option>
                                                @foreach($payments as $payment)
                                                    <option value="{{ $payment->id }}">{{ $payment->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="ibox">
                    	<div class="ibox-title">
                    		<span class="pull-right">(<strong id="total_products"></strong>) items</span>
                    		<h5>Products</h5>
                    	</div>

                        <div id="cart"></div>

                        <div class="ibox-content">
                            <div class="col-md-offset-6 col-md-6 text-right">
                                <div>Shipping: $ 5.00</div>
                                <div>Tax: $ 5.00</div>
                                <div>Total: $ 50000.00</div>
                            </div>
                        </div>

                        <button type="button" class="btn btn-block btn-primary" data-toggle="modal" data-target="#addProduct">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add Product
                        </button>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="ibox">
                        <div class="ibox-title">
                            <h5>Action</h5>
                        </div>

                        <div class="ibox-content">
                        	<strong>Note</strong>
                        	<textarea class="form-control" placeholder="Private notes" style="height: 200px;"></textarea>

                        	<div class="mb20"></div>

                            {{ csrf_field() }}

                            <button class="btn btn-block btn-outline btn-success" type="submit"><i class="fa fa-floppy-o"></i> Save</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>    
    </div>

	<div class="modal inmodal fade" id="addProduct" role="dialog"  aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
					<h4 class="modal-title">Add Product</h4>
				</div>
				<div class="modal-body">
					<div class="form-horizontal">
						<div class="form-group">
	                        <label class="col-md-2 control-label">Product</label>
	                        <div class="col-md-10">
                                <select class="add-product-modal form-control">
                                    <option value="" selected="selected">Find product</option>
                                </select>
                            </div>
	                    </div>
	                    <div class="form-group">
	                        <label class="col-md-2 control-label">Price</label>

                            <div class="col-sm-10">
                                <div class="input-group m-b">
                                    <span class="input-group-addon" id="modal-symbol">{{ auth()->user()->options()->symbol_icon }}</span> 
                                    <input id="modal-list-price" type="text" class="form-control" placeholder="Price">
                                </div>
                            </div>

	                    </div>
	                    <div class="form-group">
	                        <label class="col-md-2 control-label">QTY</label>
	                        <div class="col-md-10"><input type="text" id="modal-qty" class="form-control" name="category_info[][category_name]" placeholder="QTY" value="1"></div>
	                    </div>

                    </div>
				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary" id="add_to_order" data-dismiss="modal">Add</button>
				</div>
			</div>
		</div>
	</div>

@stop