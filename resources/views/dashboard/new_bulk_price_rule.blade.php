@extends('dashboard.layout.main')

@section('stylesheet')
    <link href="/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="/css/plugins/touchspin/jquery.bootstrap-touchspin.min.css" rel="stylesheet">
    <link href="/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
    <style>
        .select2-result-repository__avatar {
            float: left;
            margin-right: 10px;
            width: 100px;
        }
        .select2-result-repository__title {
            color: black;
            font-weight: bold;
            line-height: 1.1;
            margin-bottom: 4px;
            word-wrap: break-word;
        }
        .select2-result-repository__forks, .select2-result-repository__stargazers, .select2-result-repository__watchers {
            color: #ddd;
            display: inline-block;
            font-size: 11px;
            margin-right: 10px;
        }
    </style>
@stop

@section('javascript')
    <!-- iCheck -->
    <script src="/js/plugins/iCheck/icheck.min.js"></script>

    <!-- TouchSpin -->
    <script src="/js/plugins/touchspin/jquery.bootstrap-touchspin.min.js"></script>

    <!-- Data picker -->
   <script src="/js/plugins/datapicker/bootstrap-datepicker.js"></script>

    <script>
        $(function(){

            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });

            initTouchSpin();

            $(".find-product").select2({
                ajax: {
                    url: "/ajax/products/search",
                    headers: { 'X-CSRF-Token': csrf_token },
                    dataType: 'json',
                    type: "post",
                    delay: 250,
                    data: function (params) {
                        return {
                            q: params.term, // search term
                            shop: $("#pick-shop").val(),
                            page: params.page
                        };
                    },
                    processResults: function (data, params) {
                        params.page = params.page || 1;
                        return {
                            results: data.items,
                            pagination: {
                                more: (params.page * 30) < data.total_count
                            }
                        };
                    },
                        cache: true
                    },
                    escapeMarkup: function (markup) { return markup; },
                    minimumInputLength: 1,
                    templateResult: formatProduct,
                    templateSelection: formatProductSelection
            });

            $('.find-product').on("select2:selecting", function(e) { 
                product = e.params.args.data;

                for(var i = 0, len = product.shops.length; i < len; i++)
                {
                    $(".select2.shops").append($("<option>").val(product.shops[i].id).html(product.shops[i].name));
                }
            });

            $('.add-bulk-rule').click(function(){
                $.get('/snippets/bulk_rule_input', function(data){

                    var random_string = randomString(10);
                    
                    $('.more_bulk_rules').each(function( index ) {
                        var form_group = $(data).find('div.form-group');

                        if($(this).attr('data-shop'))
                        {
                            form_group.find('input.input-qty').attr('name', 'bulk_rule_qty[' + $(this).attr('data-shop') + '][]');
                            form_group.find('input.input-price').attr('name', 'bulk_rule_retail_price[' + $(this).attr('data-shop') + '][]');

                            $(form_group).appendTo(".more_bulk_rules.shop-" + $(this).attr('data-shop'));
                        }
                        else
                        {
                            form_group.find('input.input-qty').attr('name', 'bulk_rule_qty[all][]');
                            form_group.find('input.input-price').attr('name', 'bulk_rule_retail_price[all][]');

                            $(form_group).appendTo(".more_bulk_rules.more-bulk-price-all-shop");
                        }
                    });

                    initTouchSpin();
                });
            });

            $(document).on('change', '.more-bulk-price-all-shop .input-qty', function(){
                $('.input-qty[data-identifier="' + $(this).attr('data-identifier') + '"]').val($(this).val());
            });

            $(document).on('change', '.more-bulk-price-all-shop .input-price', function(){
                $('.input-price[data-identifier="' + $(this).attr('data-identifier') + '"]').val($(this).val());
            });

        });

        function formatProduct (product) {
            if (product.loading) return product.text;

            var markup = "<div class='select2-result-repository clearfix'>";

            if(product.images.length > 0)
            {
                markup += "<div class='select2-result-repository__avatar'><img src='/product/images/" + product.images[0].id + "/small/filename' width='100' /></div>";
            }
            else
            {
                markup += "<div class='select2-result-repository__avatar'><img src='/product/images/no_image/small/filename' width='100' /></div>";
            }

            markup += "<div class='select2-result-repository__meta'><div class='select2-result-repository__title'>" + product.info.name + "</div>";

            if (product.info.short_description) {
                markup += "<div class='select2-result-repository__description'>" + product.info.short_description + "</div>";
            }

            markup += "<div class='select2-result-repository__statistics'>" +
            "<div class='select2-result-repository__forks'><i class='fa fa-hashtag'></i> " + product.id + "</div>" +
            "<div class='select2-result-repository__stargazers'><i class='fa fa-qrcode'></i> " + product.ean + "</div>" +
            "<div class='select2-result-repository__watchers'><i class='fa fa-shopping-bag'></i> " + product.sku + "</div>" +
            "</div>" +
            "</div></div>";

            return markup;
        }

        function formatProductSelection (product) {
            modal_product = product;

            if(product.info)
            {
                return product.info.name;
            }
            
            //getProductInfo(product);

            return product;         
        }

        function initTouchSpin()
        {
            $(".input-price").TouchSpin({
                max: 1000000000,
                min: 0,
                step: 1,
                decimals: 2,
                boostat: 5,
                maxboostedstep: 10,
                prefix: '€',
                buttondown_class: 'btn btn-white',
                buttonup_class: 'btn btn-white'
            });

            $(".input-qty").TouchSpin({
                max: 1000000000,
                min: 0,
                step: 1,
                decimals: 0,
                boostat: 5,
                maxboostedstep: 10,
                buttondown_class: 'btn btn-white',
                buttonup_class: 'btn btn-white'
            });
        }

    </script>
@stop

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>{{ trans('pages.inquiry_detail_header') }}</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('home') }}">{{ trans('menu.home') }}</a>
                </li>
                <li>
                    <a href="">{{ trans('menu.inquiries') }}</a>
                </li>
                <li class="active">
                    <strong>{{ trans('page.inquiry') }}</strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">
        {!! session()->has('success')? '<div class="alert alert-success alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>' . session('success') . '</div>' : '' !!}

        <form id="profileForm" method="post" class="form-horizontal">
            <div class="row">
                <div class="col-md-8">
                    <div class="ibox">

                        <div class="ibox-title">
                            <h5>New Bulk Price Rule</h5>
                        </div>

                        <div class="ibox-content">
                            <div class="mb20"></div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Active</label>
                                        <div class="col-md-5">
                                            <label><input type="radio" class="form-control i-checks" name="active" value="1" checked> Yes</label>
                                        </div>
                                        <div class="col-md-5">
                                            <label><input type="radio" class="form-control i-checks" name="active" value="0"> No</label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Wholesale</label>
                                        <div class="col-md-5">
                                            <label><input type="radio" class="form-control i-checks" name="wholesale" value="1" checked> Yes</label>
                                        </div>
                                        <div class="col-md-5">
                                            <label><input type="radio" class="form-control i-checks" name="wholesale" value="0"> No</label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Products</label>
                                        <div class="col-md-10">
                                            <select class="find-product form-control" name="product"></select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Shop</label>
                                        <div class="col-md-10">
                                            <select id="multi-shops" class="select2 form-control shops" name="shop">
                                                <option value="all">All</option>
                                            </select>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="ibox">

                        <div class="ibox-title">
                            <h5>Product Rule</h5>
                        </div>

                        <div class="ibox-content">
                            <div class="mb20"></div>

                            <div class="row">
                                <div class="col-md-12">

                                    <div class="multi-shop-info-box multi-shop-all">
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">Bulk Price</label>
                                            <div class="col-md-5">
                                                <input type="text" class="form-control input-qty all-shop-input bulk-qty-input" placeholder="QTY" data-input="bulk-qty-input">
                                            </div>
                                            <div class="col-md-5">
                                                <input type="text" class="form-control input-price all-shop-input bulk-price-input" placeholder="List Price" data-input="bulk-price-input">
                                            </div>
                                        </div>

                                        <div class="more_bulk_rules more-bulk-price-all-shop"></div>
                                    </div>

                                    @foreach($shops as $shop)
                                        <div class="multi-shop-info-box multi-shop-{{ $shop->id }}">
                                            <div class="form-group">
                                                <label class="col-md-2 control-label">Bulk Price</label>
                                                <div class="col-md-5">
                                                    <input type="text" class="form-control input-qty all-shop-input bulk-qty-input" name="bulk_rule_qty[{{ $shop->id }}][]" placeholder="QTY" data-input="bulk-qty-input">
                                                </div>
                                                <div class="col-md-5">
                                                    <input type="text" class="form-control input-price bulk-price-input" name="bulk_rule_retail_price[{{ $shop->id }}][]" placeholder="Retail Price" data-input="bulk-price-input">
                                                </div>
                                            </div>

                                            <div class="more_bulk_rules shop-{{ $shop->id }}" data-shop="{{ $shop->id }}"></div>
                                        </div>
                                    @endforeach

                                    <div class="form-group">
                                        <div class="col-md-10 col-md-offset-2">
                                            <button class="btn btn-block btn-outline btn-primary add-bulk-rule" type="button"><i class="fa fa-plus-circle" aria-hidden="true"></i> Add Bulk Rule</button>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div> 
                </div>

                <div class="col-md-4">
                    <div class="ibox">
                        <div class="ibox-title">
                            <h5>Action</h5>
                        </div>

                        <div class="ibox-content">
                            {{ csrf_field() }}
                            <button class="btn btn-block btn-outline btn-success" type="submit"><i class="fa fa-floppy-o"></i> Save</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>

        <div class="mb30"></div>
    </div>
@stop