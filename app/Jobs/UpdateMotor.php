<?php

namespace App\Jobs;


use App\Motor;
use App\Classes\WebHooks\WebHookFactory;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class UpdateMotor implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $motor;
    protected $webhook;
    protected $event;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Motor $motor, $webhook, $event = 'create')
    {
        $this->motor = $motor;
        $this->webhook = $webhook;
        $this->event = $event;
    }
    /**

     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $webhook = WebHookFactory::create($this->motor, $this->webhook, $this->event);
        $webhook->execute();
    }
}