<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductFeatureLanguage extends Model
{
    protected $fillable = ['product_feature_id', 'language_id', 'name'];

    protected $hidden = [
        'created_at', 
        'updated_at'
    ];
    
    public function product_feature()
    {
    	return $this->belongsTo(ProductFeature::class);
    }

    public function scopeLanguage($query, $language_id)
    {
        return $query->where('language_id', $language_id);
    }

    public function scopeBackOffice($query)
    {
    	$query->where('language_id', auth()->user()->preferenceLanguage()->id);
    }
}
