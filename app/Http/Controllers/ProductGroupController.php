<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\ProductGroup;
use App\Product;

class ProductGroupController extends Controller
{
    public function index()
    {
        $product_groups = ProductGroup::all();
        return view('dashboard.product_groups', compact('product_groups'));
    }

    public function ProductGroupsWithImages()
    {   
        $productgroups = ProductGroup::with('images.ImageType');
        if($this->request->input('filter')){
            $productgroups->where('name', 'LIKE', '%'.$this->request->input('filter').'%');
        }

        return $productgroups
        ->limit(100)
        ->get();
    }

    public function newProductGroup()
    {
        return view('dashboard.new_product_group');
    }

    public function actionNewProductGroup()
    {
        $product_group = new ProductGroup;
        $product_group->setGroup($this->request);

        return redirect()->route('product_groups');
    }

    public function editProductGroup()
    {
        $product_group = ProductGroup::findOrFail($this->request->id);

        return view('dashboard.edit_product_group', compact('product_group'));
    }

    public function actionEditProductGroup()
    {
        $product_group = ProductGroup::findOrFail($this->request->id);
        $product_group->setGroup($this->request);

        return redirect()->route('product_groups');
    }

    public function viewProductGroup()
    {
        $products = Product::where('product_group_id', $this->request->id)->with('info')->paginate(100);
        return view('dashboard.view_product_group', compact('products'));
    }

    public function removeFromGroup()
    {
        $product = Product::findOrFail($this->request->id);
        $product->product_group_id = null;
        $product->save();

        return array("status" => true);
    }
}
