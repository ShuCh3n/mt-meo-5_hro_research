@extends('dashboard.layout.main')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-6">
            <h2>{{ trans('pages.inquiry_detail_header') }}</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('home') }}">{{ trans('menu.home') }}</a>
                </li>
                <li>
                    <a href="">{{ trans('menu.inquiries') }}</a>
                </li>
                <li class="active">
                    <strong>{{ trans('page.inquiry') }}</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-6">
            <a class="btn btn-outline btn-primary pull-right mt30" href="{{ route('new_customer_address') }}"><i class="fa fa-plus-circle" aria-hidden="true"></i> New address</a>
        </div>
    </div>

	<div class="wrapper wrapper-content animated fadeInRight ecommerce">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-content">

                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>Customer</th>
                                <th>Firstname</th>
                                <th>Lastname</th>
                                <th>Address</th>
                                <th>Postal Code</th>
                                <th>City</th>
                                <th>State</th>
                                <th>Country</th>
                                <th class="text-right">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($customer_addresses as $address)
                                    <tr>
                                        <td>
                                            <a href="{{ route('view_customer', $address->customer->id) }}">{{ $address->customer->firstname }} {{ $address->customer->lastname }}</a>
                                        </td>
                                        <td>
                                           {{ $address->firstname }}
                                        </td>
                                        <td>
                                          {{ $address->lastname }}
                                        </td>
                                        <td>
                                          {{ $address->street . ' ' . $address->housenumber }}
                                        </td>
                                        <td>
                                          {{ $address->zipcode }}
                                        </td>
                                        <td>
                                          {{ $address->city }}
                                        </td>
                                        <td>
                                          {{ $address->state }}
                                        </td>
                                        <td>
                                          <img src="/images/flags/16/{{ $address->country->icon }}.png" /> {{ $address->country->iso_code_3 }}
                                        </td>
                                        <td class="text-right">
                                            <a class="btn-white btn btn-xs" href="{{ route('edit_customer_address', $address->id) }}">Edit</a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <td colspan="6">
                                    <ul class="pagination pull-right"></ul>
                                </td>
                            </tr>
                            </tfoot>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
@stop